FROM node:8.9 as node

ARG ENV=prod
ARG APP=myarrow

ENV ENV ${ENV}
ENV APP ${APP}

WORKDIR /app
COPY ./ /app/

# Install Packaged and Build App
RUN yarn install
RUN yarn run build --env=${ENV} --app=${APP}
RUN mv /app/dist/${APP}/* /app/dist/

# Serve app, based on Nginx, to have only the compiled app ready for production with Nginx
FROM nginx:1.13.8-alpine

COPY --from=node /app/dist/ /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
