#!/usr/bin/env groovy
import jenkins.model.Jenkins
import hudson.EnvVars
import groovy.json.JsonSlurperClassic
import groovy.json.JsonBuilder
import groovy.json.JsonOutput
import java.net.URL

properties([
    buildDiscarder(
        logRotator(
            artifactDaysToKeepStr: "5",
            artifactNumToKeepStr: "5",
            daysToKeepStr: "5",
            numToKeepStr: "5"
        )
    ),
    disableConcurrentBuilds(),
    disableResume(),
    durabilityHint("PERFORMANCE_OPTIMIZED"),
    [
        $class: "RebuildSettings",
        autoRebuild: true,
        rebuildDisabled: false
    ],
    parameters([
        choice(
            choices: "MyArrow",
            description: "Application Theme",
            name: "APP"
        ),
        choice(
            choices: "LOCAL\nDEV\nQA\nUAT\nPROD",
            description: "Deploy Environment",
            name: "ENV"
        ),
        string(
            defaultValue: "",
            description: "Sprint Number e.g. 1",
            name: "SPRINT"
        )
    ])
])

def slurper = new groovy.json.JsonSlurperClassic()
def SLACK_CHANNEL = "#arrow-fusion-images"
def SLACK_TOKEN = "nYCc3BBYeTDN8GznRtX1MuGA"
def SLACK_TEAM_DOMAIN = "thebioagency"
def SLACK_COLOR_DANGER  = "#BD3200"
def SLACK_COLOR_SUCCESS = "#35CB1C"
def SLACK_COLOR_INFO    = "#1870B6"
def SLACK_COLOR_WARNING = "#FDCB00"
def CONTINUE = true

def restGET(resource, slurper) {
    println(resource)
    def get = new URL(resource).openConnection()
    def getRC = get.getResponseCode()
    if ( ! getRC.equals(200)) {
        return false;
    }

    def json = slurper.parseText(get.getInputStream().getText())
    if ( ! json.ok) {
        return false
    }

    return json.result
}

def restPUT(resource, version, slurper) {
    def post = new URL(resource).openConnection()
    def body = "{\"version\": ${version}}"
    post.setRequestMethod("POST")
    post.setDoOutput(true)
    post.setRequestProperty("Content-Type", "application/json")
    post.getOutputStream().write(body.getBytes("UTF-8"))
    def postRC = post.getResponseCode()
    if ( ! postRC.equals(201)) {
        return false
    }

    def json = slurper.parseText(post.getInputStream().getText())
    if ( ! json.ok) {
        return false
    }

    return true
}

try {
    def gitOrganisation = "arrowecommerce"
    def gitHostURL = "https://bitbucket.org"
    def gitRepo = "myarrow-frontend"
    def gitBranch = "master"
    def gitCredentials = "ac343ce9-7ed1-44e0-b5fa-6c298c950798"
    def dockerOrganisation = "arrowdigitalgroup"
    def dockerRegistry = "https://index.docker.io/v1/"
    def dockerHubCredentials = "0d446694-14e7-4f5d-a22a-2ea62f1d32dd"
    def gitRepoURL = "${gitHostURL}/${gitOrganisation}/${gitRepo}"
    def versionAPI = "https://www.jsonstore.io/3ec84fcda22a24d52b042be5d403f7513f2354e43227ad978c20ad03f421506a/${gitRepo}"
    def versionTag
    def version = 0
    def resp
    def image
    def tag = "${ENV}-SPRINT-${String.format("%02d", Integer.parseInt(SPRINT))}"
    def imageName = "${dockerOrganisation}/${gitRepo}"
    def dockerhubLink = "https://hub.docker.com/r/${imageName}/tags/"

    switch (ENV) {
        case "LOCAL":
            gitBranch = "develop"
            break
        case "DEV":
            gitBranch = "develop"
            break
        case "QA":
            gitBranch = "develop"
            break
        case "UAT":
            gitBranch = "uat"
            break
        case "PROD":
            gitBranch = "master"
            break
        default:
            gitBranch = "master"
    }

    timeout(120) {
        node {

            stage("Clone Repository") {

                if (CONTINUE) {
                    try {
                        checkout([
                            $class: "GitSCM",
                            branches: [[name: "*/${gitBranch}"]],
                            doGenerateSubmoduleConfigurations: false,
                            extensions: [[$class: "CleanBeforeCheckout"], [$class: 'CheckoutOption', timeout: 120]],
                            submoduleCfg: [],
                            userRemoteConfigs: [[
                                credentialsId: gitCredentials,
                                url: "${gitRepoURL}"]
                            ]
                        ])
                    } catch (exc) {
                        slackSend(
                            teamDomain: SLACK_TEAM_DOMAIN,
                            token: SLACK_TOKEN,
                            channel: SLACK_CHANNEL,
                            color: SLACK_COLOR_DANGER,
                            message: "FAILED Clone Repository:\n ${imageName}:${tag}\n ${env.BUILD_URL}console"
                        )
                        CONTINUE = false
                        currentBuild.result = 'FAILURE'
                    }
                }
            }

            stage("Generate Tag") {
                if (CONTINUE) {
                    try {
                        versionTag = tag.replaceAll("\\.", "")

                        resp = restGET( "${versionAPI}/${versionTag}", slurper);
                        if (resp) {
                            version = resp.version
                        }

                        version++

                        tag = "${tag}.${String.format("%03d", version)}"
                        echo tag
                    } catch (exc) {
                        slackSend(
                            teamDomain: SLACK_TEAM_DOMAIN,
                            token: SLACK_TOKEN,
                            channel: SLACK_CHANNEL,
                            color: SLACK_COLOR_DANGER,
                            message: "FAILED Generate Tag:\n ${imageName}:${tag}\n ${env.BUILD_URL}console"
                        )
                        echo "FAILED ${exc}"
                        CONTINUE = false
                        currentBuild.result = 'FAILURE'
                    }
                }
            }

            stage("Build Image") {
                if (CONTINUE) {
                    try {
                        image = docker.build("${imageName}:${tag}", "--build-arg APP=${APP.toLowerCase()} --build-arg ENV=${ENV.toLowerCase()} .")
                    } catch (exc) {
                        slackSend(
                            teamDomain: SLACK_TEAM_DOMAIN,
                            token: SLACK_TOKEN,
                            channel: SLACK_CHANNEL,
                            color: SLACK_COLOR_DANGER,
                            message: "FAILED Build:\n ${imageName}:${tag}\n ${env.BUILD_URL}console"
                        )
                        CONTINUE = false
                        currentBuild.result = 'FAILURE'
                    }
                }
            }

            stage("Push Image") {
                if (CONTINUE) {
                    try {
                        docker.withRegistry(dockerRegistry, dockerHubCredentials) {
                            image.push()
                        }
                    } catch (exc) {
                        slackSend(
                            teamDomain: SLACK_TEAM_DOMAIN,
                            token: SLACK_TOKEN,
                            channel: SLACK_CHANNEL,
                            color: SLACK_COLOR_DANGER,
                            message: "FAILED Push Image:\n ${imageName}:${tag}\n ${env.BUILD_URL}console"
                        )
                        CONTINUE = false
                        currentBuild.result = 'FAILURE'
                    }
                }
            }

            stage("Track Version") {
                if (CONTINUE) {
                    try {
                        resp = restPUT( "${versionAPI}/${versionTag}", version, slurper);
                    } catch (exc) {
                        slackSend(
                            teamDomain: SLACK_TEAM_DOMAIN,
                            token: SLACK_TOKEN,
                            channel: SLACK_CHANNEL,
                            color: SLACK_COLOR_DANGER,
                            message: "FAILED Track Version:\n ${env.BUILD_URL}console"
                        )
                        echo "FAILED ${exc}"
                    }
                }
            }

            stage("Prune Docker Images") {
                try {
                    sh "docker image prune -a --force"
                } catch (exc) {
                    slackSend(
                        teamDomain: SLACK_TEAM_DOMAIN,
                        token: SLACK_TOKEN,
                        channel: SLACK_CHANNEL,
                        color: SLACK_COLOR_DANGER,
                        message: "FAILED Prune images:\n ${env.BUILD_URL}console"
                    )
                }
            }

            if (CONTINUE) {
                slackSend(
                    teamDomain: SLACK_TEAM_DOMAIN,
                    token: SLACK_TOKEN,
                    channel: SLACK_CHANNEL,
                    color: SLACK_COLOR_SUCCESS,
                    message: "Build Successful\n ${imageName}:${tag}\n Docker Hub URL: ${dockerhubLink}"
                )
            }

        }
    }
} catch (exc) {
    jenkins = Jenkins.instance
    echo "Caught Exception: ${exc}"
    currentBuild.result = "ABORTED"
    slackSend(
        teamDomain: SLACK_TEAM_DOMAIN,
        token: SLACK_TOKEN,
        channel: SLACK_CHANNEL,
        color: SLACK_COLOR_DANGER,
        message: "JOB ABORTED Caught Exception:\n ${exc}\n ${env.BUILD_URL}console"
    )
    throw exc
}
