# Myarrow Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.3.

## Installation and Setup

1.  Install [Yarn](https://yarnpkg.com/en/docs/install)
2.  Clone the [MyArrow Frontend](https://bitbucket.org/arrowecommerce/myarrow-frontend) project from BitBucket
3.  Run `yarn` in the root directory

All dependencies should be installed.

## Development server

Run the following commands to start the Webpack Server with the APIs pointing to the following Environments:

- `yarn start:local` Local Docker Instances
- `yarn start:dev` AzureDEV
- `yarn start:qa` AzureQA
- `yarn start:uat` AzureUAT
- `yarn start:prod` AzurePROD

Navigate to `http://localhost:4200/` or `http://0.0.0.0:4200/`. The app will automatically reload if you change any of the source files.

## Git Hooks

[Local Git Hooks](https://www.atlassian.com/git/tutorials/git-hooks) are managed and maintained using [Husky](https://github.com/typicode/husky), we are using the [cosmiconfig](https://github.com/davidtheclark/cosmiconfig) file approach, placing our husky hooks config on a `.huskyrc` file.

There are two active githooks in the project.

1.  **Prettier:** Ensures that the code follows the same style independant of Developer. Prettier will restructure your code on every commit.
2.  **Unit Tests:** Every Push will require the Unit Tests to pass.

If the githooks aren't active then you will have to install them manually:

```
mkdir .git/hooks
node node_modules/husky/lib/installer/bin install
```

## Running Unit Tests manually

Run `yarn test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Build

Builds are automatically triggered once a Pull Request is merged into the `develop` branch. This is handled by [Concourse](https://10.251.172.207/)

## Coding Style

To ensure consistency across formatting of files, we use Prettier. Please ensure you setup your IDE using one of the following tutorials.

In case you haven't done this, there is also now a git-hook to ensure your code updates match the Prettier style:

- Webstorm: `https://prettier.io/docs/en/webstorm.html`
- VSCode: `https://github.com/prettier/prettier`
- Sublime: `https://github.com/danreeves/sublime-prettier`

## Adding a White Label Theme

1.  Create a new folder in `src/themes` with the name of the new theme (alphanumeric and lowercase) eg. _bigsportscompany_

2.  Copy and update the two SCSS files from `src/themes/myarrow` into a folder named `styles`.

3.  Add any additional assets that are needed to `src/themes/THEMENAME/assets` following the same structure as in the `myarrow` theme.

4.  Create a new entry in the `app` array in `angular-cli.json`. Make sure to change the `name`, `styles`, `stylePreprocessorOptions` and `assets` properties to point to the correct theme folder.

5.  Add the theme to `build.conf.json` and include add any locales that are required for that theme.

6.  Add the theme name as a choice for the `APP` parameter in `Jenkinsfile` and `Release.Jenkinsfile`. This can be in any capitilisation, with spaces, but needs to match up with the theme name in Step 1.
