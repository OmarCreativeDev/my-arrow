exports.config = {
  dev: {
    snippet: `<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl+ '&gtm_auth=pThnZE5z7aNhW0XYdaY1pg&gtm_preview=env-6&gtm_cookies_win=x';f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NXR2PTN');</script>`,
    noscript: `<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NXR2PTN&gtm_auth=pThnZE5z7aNhW0XYdaY1pg&gtm_preview=env-6&gtm_cookies_win=x"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>`,
  },
  qa: {
    snippet: `<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl+ '&gtm_auth=TvUKuQhJ6VpwtAnnbvmcNg&gtm_preview=env-7&gtm_cookies_win=x';f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NXR2PTN');</script>`,
    noscript: `<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NXR2PTN&gtm_auth=TvUKuQhJ6VpwtAnnbvmcNg&gtm_preview=env-7&gtm_cookies_win=x"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>`,
  },
  uat: {
    snippet: `<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl+ '&gtm_auth=2_pUx66GUDzlPfELq7y1Aw&gtm_preview=env-8&gtm_cookies_win=x';f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NXR2PTN');</script>`,
    noscript: `<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NXR2PTN&gtm_auth=2_pUx66GUDzlPfELq7y1Aw&gtm_preview=env-8&gtm_cookies_win=x"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>`,
  },
  prod: {
    snippet: `<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NXR2PTN');</script>`,
    noscript: `<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NXR2PTN"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>`,
  },
};
