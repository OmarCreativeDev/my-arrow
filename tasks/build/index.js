const fs = require('fs');
const path = require('path');
const argv = require('yargs')
  .default('app', 'myarrow')
  .default('env', 'prod').argv;
const chalk = require('chalk');
const spawn = require('child_process').spawnSync;
const buildConfig = require('../../build.conf.json');
const replaceGTM = require('./replace-gtm');

const app = argv.app.toLowerCase();
const env = argv.env.toLowerCase();

console.log(chalk.green(`[build task] Running i18n`));

spawn('ng', ['xi18n', '--i18n-locale=en-US'], { stdio: 'inherit' });

spawn('xliffmerge', ['--quiet', '--profile=i18n.config.json', ...buildConfig.locales], { stdio: 'inherit' });

console.log(chalk.green(`[build task] Completed i18n`));

buildConfig.bundles[app].locales.forEach(locale => {
  console.log(chalk.green(`[build task] Building ${app}/${locale}`));

  //run ng build with required args
  const args = [
    'build',
    `--project=${app}`,
    '--prod',
    `--configuration=${env}`,
    `--i18n-locale=${locale}`,
    '--i18n-format=xlf',
    `--i18n-file=src/i18n/messages.${locale}.xlf`,
    `--output-path=dist/${app}/${locale}`,
    `--deploy-url=${locale}/`,
  ];

  console.log(chalk.green(`[build task] Running ng ${args}`));
  spawn('ng', args, { stdio: 'inherit' });

  replaceGTM(`dist/${app}/${locale}/index.html`, env);
});
