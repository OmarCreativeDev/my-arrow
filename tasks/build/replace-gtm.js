const cheerio = require('cheerio');
const fs = require('fs');
const chalk = require('chalk');
const gtmConfig = require('../../gtm.conf').config;

/**
 * Replaces GTM script snippet and noscript tags on index.html
 */

module.exports = function(indexPath, env) {
  console.log(chalk.green(`[build task] Adding GTM snippets to index.html`));

  fs.readFile(indexPath, 'utf8', function(err, indexHtml) {
    if (err) {
      return console.log(chalk.red(`[build task] Error reading index.html - ${err}`));
    }

    // the GTM config (currently based on env)
    let gtm = gtmConfig[env];

    // if config not found use default
    if (!gtm) {
      console.log(chalk.yellow(`[build task] GTM config not found - using 'dev' config`));
      gtm = gtmConfig.dev;
    }

    // load cheerio object
    const $ = cheerio.load(indexHtml);

    // replace GTM scripts
    $('script#gtmsnippet').replaceWith(gtm.snippet);
    $('noscript#gtmnoscript').replaceWith(gtm.noscript);

    // write file back with updated cheerio object
    fs.writeFile(indexPath, $.html(), function(err) {
      if (err) return console.log(chalk.red(`[build task] Error replacing index.html - ${err}`));
    });
  });
};
