import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@app/core/auth/auth.guard';
import { PrivateFeaturesGuard } from '@app/core/features/private-features.guard';
import { NAGuard } from '@app/core/user/user-northAmerican.guard';
import { RoleGuard } from '@app/core/user/user-role.guard';
import { UserGuard } from '@app/core/user/user.guard';
import { ErrorsComponent } from '@app/features/errors/pages/errors/errors.component';
import { NotFoundComponent } from '@app/features/errors/pages/not-found/not-found.component';
import { InterstitialComponent } from '@app/layout/interstitial/interstitial.component';

import { LoggedInComponent } from './layout/logged-in/logged-in.component';
import { LoggedOutComponent } from './layout/logged-out/logged-out.component';
import { BomGuard } from './features/bom/bom.guard';

const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full',
  },
  {
    path: 'style-guide',
    loadChildren: './style-guide/style-guide.module#StyleGuideModule',
  },
  {
    path: '',
    component: InterstitialComponent,
    children: [
      {
        path: '',
        loadChildren: './features/public-interstitials/public-interstitials.module#PublicInterstitialsModule',
      },
    ],
  },
  {
    path: '',
    component: LoggedOutComponent,
    children: [{ path: '', loadChildren: './features/public/public.module#PublicModule' }],
  },
  {
    path: '',
    component: LoggedInComponent,
    children: [
      {
        path: 'dashboard',
        loadChildren: './features/dashboard/dashboard.module#DashboardModule',
        canActivate: [AuthGuard, UserGuard, PrivateFeaturesGuard],
      },
      {
        path: 'products',
        loadChildren: './features/products/products.module#ProductsModule',
        canActivate: [AuthGuard, UserGuard, PrivateFeaturesGuard],
      },
      {
        path: 'trade-compliance',
        loadChildren: './features/trade-compliance/trade-compliance.module#TradeComplianceModule',
        canActivate: [AuthGuard, UserGuard, NAGuard, PrivateFeaturesGuard],
      },
      {
        path: 'orders',
        loadChildren: './features/orders/orders.module#OrdersModule',
        canActivate: [AuthGuard, UserGuard, PrivateFeaturesGuard],
      },
      { path: 'cart', loadChildren: './features/cart/cart.module#CartModule', canActivate: [AuthGuard, UserGuard, PrivateFeaturesGuard] },
      {
        path: 'forecast',
        loadChildren: './features/forecast/forecast.module#ForecastModule',
        canActivate: [AuthGuard, UserGuard, RoleGuard, PrivateFeaturesGuard],
      },
      {
        path: 'bom',
        loadChildren: './features/bom/bom.module#BomModule',
        canActivate: [AuthGuard, UserGuard, PrivateFeaturesGuard, BomGuard],
      },
      {
        path: 'quotes',
        loadChildren: './features/quotes/quotes.module#QuotesModule',
        canActivate: [AuthGuard, UserGuard, PrivateFeaturesGuard],
      },
      {
        path: 'notifications',
        loadChildren: './features/product-notifications/product-notifications.module#ProductNotificationsModule',
        canActivate: [AuthGuard, UserGuard, PrivateFeaturesGuard],
      },
      {
        path: 'quotes/submitted-quotes',
        loadChildren: './features/submitted-quotes/submitted-quotes.module#SubmittedQuotesModule',
        canActivate: [AuthGuard, UserGuard, PrivateFeaturesGuard],
      },
      {
        path: 'error',
        component: ErrorsComponent,
        canActivate: [AuthGuard, UserGuard, PrivateFeaturesGuard],
      },
      {
        path: '**',
        component: NotFoundComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  providers: [BomGuard],
  exports: [RouterModule],
})
export class AppRoutingModule {}
