import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ApiService } from './api/api.service';
import { UserService } from './user/user.service';
import { UserGuard } from './user/user.guard';
import { LoginGuard } from './login/login.guard';
import { AuthService } from './auth/auth.service';
import { AuthTokenService } from './auth/auth-token.service';
import { AuthInterceptorService } from './auth/auth-interceptor.service';
import { AuthGuard } from './auth/auth.guard';
import { OrdersService } from './orders/orders.service';
import { CartService } from './cart/cart.service';
import { CheckoutService } from '@app/core/checkout/checkout.service';
import { BomsService } from '@app/core/boms/boms.service';
import { RoleGuard } from '@app/core/user/user-role.guard';
import { TradeComplianceService } from '@app/core/trade-compliance/trade-compliance.service';
import { EmailService } from '@app/core/email/email.service';
import { ProductNotificationsService } from '@app/core/product-notifications/product-notifications.service';
import { DialogModule } from '@app/core/dialog/dialog-module';
import { ModalService } from '@app/shared/services/modal.service';
import { DateService } from '@app/shared/services/date.service';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { ToastService } from '@app/core/toast/toast.service';
import { QuoteCartService } from '@app/core/quote-cart/quote-cart.service';
import { PublicFeaturesGuard } from '@app/core/features/public-features.guard';
import { PrivateFeaturesGuard } from '@app/core/features/private-features.guard';
import { LanguageService } from '@app/core/language/language.service';
import { InventoryService } from '@app/core/inventory/inventory.service';
import { PurchaseOrderService } from '@app/core/purchase-order/purchase-order.service';
import { PaymentService } from '@app/core/payment/payment.service';
import { WindowRefService } from '@app/core/window/window.service';
import { LiveChatService } from '@app/core/live-chat/live-chat.service';
import { OfflineLiveChatService } from '@app/core/offline-live-chat/offline-live-chat.service';
import { QuotesService } from '@app/core/quotes/quotes.service';
import { NAGuard } from '@app/core/user/user-northAmerican.guard';
import { RegistrationService } from '@app/core/registration/registration.service';
import { ForecastService } from '@app/core/forecast/forecast.service';
import { WINDOW_PROVIDERS } from '@app/shared/services/window.service';
import { PasswordResetGuard } from '@app/core/password-reset/password-reset.guard';
import { UserService as UserBomService } from '@arrow/bom/core';
import { CheckoutGuard } from '@app/core/checkout/checkout.guard';
import { PushPullOrderService } from '@app/core/push-pull-order/push-pull-order.service';
import { CustomerBroadcastService } from '@app/core/customer-broadcast/customer-broadcast.service';

@NgModule({
  imports: [HttpClientModule, DialogModule],
  providers: [
    ApiService,
    UserService,
    UserGuard,
    LoginGuard,
    RoleGuard,
    NAGuard,
    AuthService,
    AuthTokenService,
    AuthGuard,
    OrdersService,
    PushPullOrderService,
    BomsService,
    CartService,
    CheckoutService,
    TradeComplianceService,
    EmailService,
    BackdropService,
    ModalService,
    DateService,
    ProductNotificationsService,
    PublicFeaturesGuard,
    PrivateFeaturesGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true,
    },
    ToastService,
    QuoteCartService,
    LanguageService,
    InventoryService,
    PurchaseOrderService,
    PaymentService,
    WindowRefService,
    LiveChatService,
    OfflineLiveChatService,
    QuotesService,
    RegistrationService,
    ForecastService,
    WINDOW_PROVIDERS,
    PasswordResetGuard,
    UserBomService,
    CheckoutGuard,
    CustomerBroadcastService,
  ],
})
export class CoreModule {}
