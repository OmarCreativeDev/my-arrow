import { IListingState } from '@app/shared/shared.interfaces';
import { HttpErrorResponse } from '@angular/common/http';

/**
 * API responses
 */
export interface IBomResponse {
  payload: any;
  status?: {
    executionTime: number;
    statusCode: number;
    statusMsg: string;
    serverInfo: string;
  };
}

export interface IBomListingResponse extends IBomResponse {
  payload: Array<{
    bomId: string;
    bomName: string;
    partCount: number;
    lastEdited: string;
  }>;
}

export interface IBomCreateResponse extends IBomResponse {
  payload: {
    bomId: string;
  };
}

export interface IBomAddPartsResponse extends IBomResponse {
  payload: {
    rowsIds: Array<string>;
  };
}

/**
 * Store state
 */
export interface IBomState {
  listing: IListingState<IBom>;
  status: {
    busy: boolean;
    lastCreated: IBom;
    lastModified: IModifiedBom;
    error: HttpErrorResponse;
  };
}

export interface IBom {
  id: string;
  name: string;
  partCount?: number;
  partNumbers?: Array<string>;
  lastEdited?: string;
}

export interface IModifiedBom extends IBom {
  isNew: boolean;
}

export interface IAddToBomPart {
  manufacturerPartNumber: string;
  manufacturer: string;
  quantity: number;
  docId: string;
  itemId: number;
  warehouseId: number;
}
