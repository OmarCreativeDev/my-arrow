import {
  GetBomListingComplete,
  GetBomListingError,
  AddPartsToBomComplete,
  CreateBom,
  CreateBomComplete,
  CreateBomError,
  AddPartsToBom,
  AddPartsToBomError,
} from './boms.actions';
import { bomReducers, INITIAL_BOM_STATE } from './boms.reducers';
import { mockBoms } from '@app/core/boms/boms.service.spec';
import { IListingState } from '@app/shared/shared.interfaces';
import { IBom, IModifiedBom } from '@app/core/boms/boms.interface';
import { HttpErrorResponse } from '@angular/common/http';

describe('BOM Reducers', () => {
  it(`should return boms listing on GetBomsComplete action`, () => {
    const action = new GetBomListingComplete(mockBoms);
    const result = bomReducers(INITIAL_BOM_STATE, action);
    const expectedResultState: IListingState<IBom> = {
      loading: false,
      items: mockBoms,
      error: undefined,
    };
    expect(result.listing).toEqual(expectedResultState);
  });
  it(`should set error  GetBomsComplete action`, () => {
    const error = new HttpErrorResponse({ status: 500 });
    const action = new GetBomListingError(error);
    const result = bomReducers(INITIAL_BOM_STATE, action);
    const expectedResultState: IListingState<IBom> = {
      loading: false,
      items: undefined,
      error: error,
    };
    expect(result.listing).toEqual(expectedResultState);
  });

  it(`should update store on createBom action`, () => {
    const action = new CreateBom('Bom name');
    const result = bomReducers(INITIAL_BOM_STATE, action);
    expect(result.status.busy).toBeTruthy();
  });

  it(`should update store on createBomComplete action`, () => {
    const action = new CreateBomComplete({ id: 'aaa', name: 'Bom name' });
    const result = bomReducers(INITIAL_BOM_STATE, action);
    expect(result.status.lastCreated).toEqual({ id: 'aaa', name: 'Bom name' });
  });

  it(`should update store on createBomError action`, () => {
    const error = new HttpErrorResponse({ status: 500 });
    const action = new CreateBomError(error);
    const result = bomReducers(INITIAL_BOM_STATE, action);
    expect(result.status.error).toEqual(error);
  });

  it(`should update store on AddPartsToBom action`, () => {
    const bom = {
      id: 'aaa',
      name: 'Bom name',
      parts: [
        {
          docId: '1234',
          itemId: 1234,
          warehouseId: 1234,
          manufacturerPartNumber: 'aaa',
          quantity: 100,
          manufacturer: 'Electronics Industry Public Company Limited',
        },
        {
          docId: '1234',
          itemId: 1234,
          warehouseId: 1234,
          manufacturerPartNumber: 'bbb',
          quantity: 200,
          manufacturer: 'Electronics Industry Public Company Limited',
        },
      ],
      isNew: true,
    };
    const action = new AddPartsToBom(bom);
    const result = bomReducers(INITIAL_BOM_STATE, action);
    expect(result.status.busy).toBeTruthy();
  });

  it(`should update store on AddPartsToBomComplete action`, () => {
    const modifiedBom: IModifiedBom = { id: mockBoms[0].id, name: mockBoms[0].name, isNew: true };
    const action = new AddPartsToBomComplete(modifiedBom);
    const result = bomReducers(INITIAL_BOM_STATE, action);
    expect(result.status.lastModified).toEqual(modifiedBom);
  });

  it(`should update store on addPartsToBomError action`, () => {
    const error = new HttpErrorResponse({ status: 500 });
    const action = new AddPartsToBomError(error);
    const result = bomReducers(INITIAL_BOM_STATE, action);
    expect(result.status.error).toEqual(error);
  });
});
