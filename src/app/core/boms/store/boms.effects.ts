import { Effect, Actions, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';

import { map, switchMap, catchError, withLatestFrom } from 'rxjs/operators';
import { of, forkJoin } from 'rxjs';

import {
  BomActionTypes,
  GetBomListing,
  GetBomListingComplete,
  GetBomListingError,
  AddPartsToBom,
  AddPartsToBomComplete,
  AddPartsToBomError,
  CreateBom,
  CreateBomError,
  CreateBomComplete,
  OpenAddToBomModal,
} from './boms.actions';
import { BomsService } from '@app/core/boms/boms.service';
import { IBom } from '@app/core/boms/boms.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { getRegion } from '@app/core/user/store/user.selectors';
import { DialogService } from '@app/core/dialog/dialog.service';
import { AddToBomDialogComponent } from '@app/shared/components/add-to-bom-dialog/add-to-bom-dialog.component';

@Injectable()
export class BomEffects {
  @Effect()
  getBomListing = this.actions$.pipe(
    ofType<GetBomListing>(BomActionTypes.GET_BOM_LISTING),
    switchMap(() => {
      return this.bomsService.getBomListing().pipe(
        map(response => {
          if (response.status.statusCode !== 200) {
            return new GetBomListingError(
              new HttpErrorResponse({ status: response.status.statusCode, statusText: response.status.statusMsg })
            );
          } else {
            if (!response.payload) return new GetBomListingComplete([]);
            const boms: Array<IBom> = response.payload.map(bom => {
              return {
                id: bom.bomId,
                name: bom.bomName,
                partCount: bom.partCount,
                lastEdited: bom.lastEdited,
              };
            });
            return new GetBomListingComplete(boms);
          }
        }),
        catchError(error => of(new GetBomListingError(error)))
      );
    })
  );

  @Effect()
  CreateBom = this.actions$.pipe(
    ofType<CreateBom>(BomActionTypes.CREATE_BOM),
    withLatestFrom(this.store.pipe(select(getRegion))),
    switchMap(([action, region]) => {
      const regionMap = {
        arrowna: 'NAC',
        arrowce: 'EUROPE',
        arrowse: 'EUROPE',
        arrowne: 'EUROPE',
        arrowap: 'ASIA',
        arrownz: 'ASIA',
      };
      return this.bomsService.createBom(action.payload, regionMap[region]).pipe(
        map(response => {
          return new CreateBomComplete({ id: response.payload.bomId, name: action.payload });
        }),
        catchError(err => of(new CreateBomError(err)))
      );
    })
  );

  @Effect()
  AddPartsToBom = this.actions$.pipe(
    ofType(BomActionTypes.ADD_PARTS_TO_BOM),
    switchMap((action: AddPartsToBom) => {
      const addPartsObservables = action.payload.parts.map(part => {
        return this.bomsService.addPartToBom(action.payload.id, part.manufacturerPartNumber, part.quantity, part.manufacturer);
      });
      return forkJoin(addPartsObservables).pipe(
        map(() => {
          return new AddPartsToBomComplete({
            id: action.payload.id,
            name: action.payload.name,
            isNew: action.payload.isNew,
          });
        }),
        catchError(error => of(new AddPartsToBomError(error)))
      );
    })
  );

  @Effect({ dispatch: false })
  openAddToBomModal = this.actions$.pipe(
    ofType<OpenAddToBomModal>(BomActionTypes.OPEN_ADD_TO_BOM_MODAL),
    map(action => {
      this.dialogService.open(AddToBomDialogComponent, {
        data: action.payload.parts,
        size: 'medium',
      });
    })
  );

  constructor(
    private actions$: Actions,
    private bomsService: BomsService,
    private store: Store<IAppState>,
    private dialogService: DialogService
  ) {}
}
