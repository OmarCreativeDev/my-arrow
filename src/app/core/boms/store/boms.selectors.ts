import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IBomState } from '@app/core/boms/boms.interface';

export const getBomState = createFeatureSelector<IBomState>('boms');

export const getBomListing = createSelector(getBomState, (bomState: IBomState) => bomState.listing);

export const getLastCreatedBom = createSelector(getBomState, (bomState: IBomState) => bomState.status.lastCreated);

export const getLastModifiedBom = createSelector(getBomState, (bomState: IBomState) => bomState.status.lastModified);

export const getBomError = createSelector(getBomState, (bomState: IBomState) => bomState.status.error);

export const isBomBusy = createSelector(getBomState, (bomState: IBomState) => bomState.status.busy);
