import { Action } from '@ngrx/store';
import { IBom, IModifiedBom, IAddToBomPart } from '@app/core/boms/boms.interface';
import { HttpErrorResponse } from '@angular/common/http';

export enum BomActionTypes {
  GET_BOM_LISTING = '[BOM] Get BOM Listing',
  GET_BOM_LISTING_COMPLETE = '[BOM] Get BOM Lisitng Complete',
  GET_BOM_LISTING_ERROR = '[BOM] Get BOM Lisitng Error',
  OPEN_ADD_TO_BOM_MODAL = '[BOM] Open `Add To Bom` Modal',
  CREATE_BOM = '[BOM] Create BOM',
  CREATE_BOM_COMPLETE = '[BOM] Create BOM Complete',
  CREATE_BOM_ERROR = '[BOM] Create BOM Error',
  ADD_PARTS_TO_BOM = '[BOM] Add Parts To BOM',
  ADD_PARTS_TO_BOM_COMPLETE = '[BOM] Add Parts To BOM Complete',
  ADD_PARTS_TO_BOM_ERROR = '[BOM] Add Parts To BOM Error',
}

export class GetBomListing implements Action {
  readonly type = BomActionTypes.GET_BOM_LISTING;
  constructor() {}
}

export class GetBomListingComplete implements Action {
  readonly type = BomActionTypes.GET_BOM_LISTING_COMPLETE;
  constructor(public payload: Array<IBom>) {}
}

export class GetBomListingError implements Action {
  readonly type = BomActionTypes.GET_BOM_LISTING_ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export class OpenAddToBomModal implements Action {
  readonly type = BomActionTypes.OPEN_ADD_TO_BOM_MODAL;
  constructor(public payload: { url: string; parts: Array<IAddToBomPart> }) {}
}

export class CreateBom implements Action {
  readonly type = BomActionTypes.CREATE_BOM;
  constructor(public payload: string) {}
}

export class CreateBomComplete implements Action {
  readonly type = BomActionTypes.CREATE_BOM_COMPLETE;
  constructor(public payload: IBom) {}
}

export class CreateBomError implements Action {
  readonly type = BomActionTypes.CREATE_BOM_ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export class AddPartsToBom implements Action {
  readonly type = BomActionTypes.ADD_PARTS_TO_BOM;
  constructor(public payload: { id: string; name: string; parts: Array<IAddToBomPart>; isNew: boolean }) {}
}

export class AddPartsToBomComplete implements Action {
  readonly type = BomActionTypes.ADD_PARTS_TO_BOM_COMPLETE;
  constructor(public payload: IModifiedBom) {}
}

export class AddPartsToBomError implements Action {
  readonly type = BomActionTypes.ADD_PARTS_TO_BOM_ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export type BomActions =
  | GetBomListing
  | GetBomListingComplete
  | GetBomListingError
  | OpenAddToBomModal
  | CreateBom
  | CreateBomComplete
  | CreateBomError
  | AddPartsToBom
  | AddPartsToBomComplete
  | AddPartsToBomError;
