import { BomActions, BomActionTypes } from '@app/core/boms/store/boms.actions';
import { IBomState } from '@app/core/boms/boms.interface';

export const INITIAL_BOM_STATE: IBomState = {
  listing: {
    loading: true,
    error: undefined,
    items: undefined,
  },
  status: {
    lastModified: undefined,
    lastCreated: undefined,
    error: undefined,
    busy: false,
  },
};

export function bomReducers(state: IBomState = INITIAL_BOM_STATE, action: BomActions): IBomState {
  switch (action.type) {
    case BomActionTypes.GET_BOM_LISTING_COMPLETE: {
      return {
        ...state,
        listing: {
          loading: false,
          error: undefined,
          items: action.payload,
        },
      };
    }
    case BomActionTypes.GET_BOM_LISTING_ERROR: {
      return {
        ...state,
        listing: {
          loading: false,
          error: action.payload,
          items: undefined,
        },
      };
    }

    case BomActionTypes.CREATE_BOM: {
      return {
        ...state,
        status: {
          ...state.status,
          busy: true,
        },
      };
    }

    case BomActionTypes.CREATE_BOM_COMPLETE: {
      return {
        ...state,
        listing: {
          loading: false,
          error: undefined,
          items: [action.payload, ...state.listing.items],
        },
        status: {
          ...state.status,
          lastCreated: action.payload,
          busy: false,
        },
      };
    }

    case BomActionTypes.CREATE_BOM_ERROR: {
      return {
        ...state,
        status: {
          ...state.status,
          error: action.payload,
          busy: false,
        },
      };
    }

    case BomActionTypes.ADD_PARTS_TO_BOM: {
      return {
        ...state,
        status: {
          ...state.status,
          busy: true,
        },
      };
    }

    case BomActionTypes.ADD_PARTS_TO_BOM_COMPLETE: {
      return {
        ...state,
        status: {
          ...state.status,
          lastModified: action.payload,
          busy: false,
        },
      };
    }

    case BomActionTypes.ADD_PARTS_TO_BOM_ERROR: {
      return {
        ...state,
        status: {
          ...state.status,
          error: action.payload,
          busy: false,
        },
      };
    }
    default: {
      return state;
    }
  }
}
