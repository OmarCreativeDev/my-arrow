import { Injectable } from '@angular/core';
import { ApiService } from '@app/core/api/api.service';
import { Observable } from 'rxjs';
import { IBomListingResponse, IBomAddPartsResponse, IBomCreateResponse } from '@app/core/boms/boms.interface';
import { environment } from '@env/environment';

@Injectable()
export class BomsService {
  constructor(private apiService: ApiService) {}

  /**
   * Returns list of user BOMs
   */
  public getBomListing(): Observable<IBomListingResponse> {
    return this.apiService.get<IBomListingResponse>(`${environment.baseUrls.serviceBoms}/user`);
  }

  /**
   * Creates a new BOM
   * @param params
   */
  public createBom(name: string, region: string): Observable<IBomCreateResponse> {
    const params = { n: name, o: 'MYARROW', re: region };
    return this.apiService.get<IBomCreateResponse>(`${environment.baseUrls.serviceBoms}/new`, params);
  }

  /**
   * Add parts to a BOM
   * @param params
   */
  public addPartToBom(bomId: string, part: string, quantity: number, manufacturer: string): Observable<IBomAddPartsResponse> {
    return this.apiService.put<IBomAddPartsResponse>(
      `${environment.baseUrls.serviceBoms}/new/${bomId}?pn=${part}&q=${quantity}&mfr=${manufacturer}`,
      {}
    );
  }
}
