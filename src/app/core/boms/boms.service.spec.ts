import { TestBed, inject } from '@angular/core/testing';
import { BomsService } from './boms.service';
import { ApiService } from '../api/api.service';
import { of } from 'rxjs';
import { CoreModule } from '@app/core/core.module';
import { IBomListingResponse } from '@app/core/boms/boms.interface';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { StoreModule } from '@ngrx/store';
import { bomReducers } from '@app/core/boms/store/boms.reducers';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';

export const mockBomResponse: IBomListingResponse = {
  payload: [
    {
      bomId: '1236236',
      bomName: 'Fake name',
      lastEdited: '2018-09-10',
      partCount: 4503,
    },
  ],
};

export const mockBoms = [
  {
    id: '1236236',
    name: 'Fake name',
    lastEdited: '2018-09-10',
    partCount: 4503,
  },
];

describe('BomsService', () => {
  let apiService: ApiService;
  let bomService: BomsService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, CoreModule, StoreModule.forRoot({ boms: bomReducers })],
      providers: [BomsService, { provide: WSSService, useClass: MockStompWSSService }],
    });
    apiService = TestBed.get(ApiService);
    bomService = TestBed.get(BomsService);
    http = TestBed.get(HttpTestingController);
  });

  it('should be created', inject([BomsService], (service: BomsService) => {
    expect(service).toBeTruthy();
  }));

  it('should return user bom list', () => {
    spyOn(apiService, 'get').and.returnValue(of(mockBomResponse));
    bomService.getBomListing().subscribe(response => {
      expect(response.payload.length).toEqual(1);
    });
  });

  it('should send correct params with createBom', () => {
    bomService.createBom('New Bom', 'NAC').subscribe();
    http.expectOne(req => {
      return req.method === 'GET' && req.params.get('n') === 'New Bom' && req.params.get('re') === 'NAC';
    });
  });

  it('should send correct params with addPartsToBom', () => {
    bomService.addPartToBom('xyz', 'bav99', 200, 'Electronics Industry Public Company Limited').subscribe();
    http.expectOne(req => {
      return req.method === 'PUT' && req.responseType === 'json';
    });
  });
});
