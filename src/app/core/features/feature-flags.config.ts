export const PUBLIC_PATHS = [{ key: 'forgottenPassword', value: '/recover-password' }];

export const PRIVATE_PATHS = [
  { key: 'compliance', value: '/trade-compliance' },
  { key: 'forecast', value: '/forecast' },
  { key: 'quotes', value: '/quotes/submitted-quotes' },
];
