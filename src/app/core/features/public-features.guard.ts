import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Store, select } from '@ngrx/store';
import { combineLatest } from 'rxjs';

import { PUBLIC_PATHS } from '@app/core/features/feature-flags.config';
import { GetPublicProperties } from '@app/features/properties/store/properties.actions';
import { getPublicFeatureFlagsSelector, getErrorSelector } from '@app/features/properties/store/properties.selectors';
import { IAppState } from '@app/shared/shared.interfaces';
import { filter, map, tap } from 'rxjs/operators';
import { isEmpty } from 'lodash-es';

@Injectable()
export class PublicFeaturesGuard implements CanActivate {
  constructor(private router: Router, private store: Store<IAppState>) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return combineLatest(this.store.pipe(select(getPublicFeatureFlagsSelector)), this.store.pipe(select(getErrorSelector))).pipe(
      tap(([publicFeatureFlags, error]) => {
        if (isEmpty(publicFeatureFlags) && !error) {
          this.store.dispatch(new GetPublicProperties());
        }
      }),
      filter(([publicFeatureFlags, error]) => publicFeatureFlags || error),
      map(([publicFeatureFlags, error]) => this.handleResponse([publicFeatureFlags, error], state))
    );
  }

  private handleResponse([publicFeatureFlags, error], state: RouterStateSnapshot) {
    const pathInfo = PUBLIC_PATHS.find(path => state.url.startsWith(path.value));
    if (pathInfo && !publicFeatureFlags[pathInfo.key]) {
      this.router.navigateByUrl('/login');
      return false;
    } else {
      return true;
    }
  }
}
