import { inject, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { cloneDeep } from 'lodash-es';

import { ApiService } from '@app/core/api/api.service';
import { PublicFeaturesGuard } from './public-features.guard';
import { mockPublicProperties } from '@app/core/features/feature-flags.mock';
import { CoreModule } from '@app/core/core.module';

import { PropertiesService } from '@app/core/properties/properties.service';
import { propertiesReducers } from '@app/features/properties/store/properties.reducers';
import { PropertiesModule } from '@app/features/properties/properties.module';
import { PropertiesEffects } from '@app/features/properties/store/properties.effects';
import { GetPublicPropertiesSuccess } from '@app/features/properties/store/properties.actions';

class DummyComponent {}

describe('PublicFeaturesGuard', () => {
  const routes = [{ path: 'dashboard', component: DummyComponent }, { path: 'login', component: DummyComponent }];
  const mockSnapshot: RouterStateSnapshot = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);
  let store: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        CoreModule,
        PropertiesModule,
        StoreModule.forRoot({
          properties: propertiesReducers,
        }),
        EffectsModule.forRoot([PropertiesEffects]),
      ],
      providers: [PublicFeaturesGuard, PropertiesService, ApiService],
    });
    store = TestBed.get(Store);
  });

  it('should be created', inject([PublicFeaturesGuard], (guard: PublicFeaturesGuard) => {
    expect(guard).toBeTruthy();
  }));

  it(`should guard route if on /recover-password route and 'forgottenPassword' flag is set to false`, inject(
    [PublicFeaturesGuard],
    (guard: PublicFeaturesGuard) => {
      mockSnapshot.url = '/recover-password';
      const mockProperties = cloneDeep(mockPublicProperties);
      mockProperties.flags.forgottenPassword = false;
      store.dispatch(new GetPublicPropertiesSuccess(mockProperties));
      guard.canActivate(new ActivatedRouteSnapshot(), mockSnapshot).subscribe(canActivate => {
        expect(canActivate).toBeFalsy();
      });
    }
  ));

  it(`should guard route if on /recover-password route and 'forgottenPassword' flag is set to true`, inject(
    [PublicFeaturesGuard],
    (guard: PublicFeaturesGuard) => {
      mockSnapshot.url = '/recover-password';
      const mockProperties = cloneDeep(mockPublicProperties);
      mockProperties.flags.forgottenPassword = true;
      store.dispatch(new GetPublicPropertiesSuccess(mockProperties));
      guard.canActivate(new ActivatedRouteSnapshot(), mockSnapshot).subscribe(canActivate => {
        expect(canActivate).toBeTruthy();
      });
    }
  ));
});
