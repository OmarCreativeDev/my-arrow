export const mockPublicProperties: object = {
  flags: {
    i18n: true,
    forgottenPassword: true,
    registration: true,
  },
};

export const mockPrivateProperties: object = {
  featureFlags: {
    bom: true,
    addToBom: true,
    catalogue: true,
    compliance: true,
    forecast: true,
    orders: true,
    ordersRequestReturn: true,
    ordersPushPull: true,
    products: true,
    quotes: true,
    tariff: true,
    buyFromQuotes: true,
    customerBroadcast: true,
    quoteNotes: true,
    productCatalogCount: true,
  },
};
