import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Store, select } from '@ngrx/store';
import { combineLatest } from 'rxjs';

import { PRIVATE_PATHS } from '@app/core/features/feature-flags.config';
import { GetPrivateProperties } from '@app/features/properties/store/properties.actions';
import { getPrivateFeatureFlagsSelector, getErrorSelector } from '@app/features/properties/store/properties.selectors';
import { IAppState } from '@app/shared/shared.interfaces';
import { getUser } from '@app/core/user/store/user.selectors';
import { filter, map, tap } from 'rxjs/operators';
import { isEmpty } from 'lodash-es';

@Injectable()
export class PrivateFeaturesGuard implements CanActivate {
  constructor(private router: Router, private store: Store<IAppState>) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return combineLatest(
      this.store.pipe(select(getPrivateFeatureFlagsSelector)),
      this.store.pipe(select(getErrorSelector)),
      this.store.pipe(select(getUser))
    ).pipe(
      tap(([privateFeatureFlags, error, user]) => {
        if (isEmpty(privateFeatureFlags) && !error && user) {
          this.store.dispatch(new GetPrivateProperties(`private/${user.registrationInstance.toLocaleLowerCase()}`));
        }
      }),
      filter(([privateFeatureFlags, error, user]) => (privateFeatureFlags !== undefined && user !== undefined) || error !== undefined),
      map(([privateFeatureFlags]) => this.handleResponse(privateFeatureFlags, state))
    );
  }

  private handleResponse(privateFeatureFlags, state: RouterStateSnapshot) {
    const pathInfo = PRIVATE_PATHS.find(path => state.url.startsWith(path.value));
    if (pathInfo && ![pathInfo.key]) {
      this.router.navigateByUrl('/dashboard');
      return false;
    } else {
      return true;
    }
  }
}
