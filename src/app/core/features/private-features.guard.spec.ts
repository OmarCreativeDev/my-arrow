import { inject, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { cloneDeep } from 'lodash-es';

import { ApiService } from '@app/core/api/api.service';
import { PrivateFeaturesGuard } from './private-features.guard';
import { mockPrivateProperties } from '@app/core/features/feature-flags.mock';
import { CoreModule } from '@app/core/core.module';

import { PropertiesService } from '@app/core/properties/properties.service';
import { propertiesReducers } from '@app/features/properties/store/properties.reducers';
import { PropertiesModule } from '@app/features/properties/properties.module';
import { PropertiesEffects } from '@app/features/properties/store/properties.effects';
import { GetPrivatePropertiesSuccess } from '@app/features/properties/store/properties.actions';
import { IAppState } from '@app/shared/shared.interfaces';
import { userReducers } from '@app/core/user/store/user.reducers';

class DummyComponent {}

describe('PrivateFeaturesGuard', () => {
  const routes = [{ path: 'dashboard', component: DummyComponent }];
  const mockSnapshot: RouterStateSnapshot = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);
  let store: Store<IAppState>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        CoreModule,
        PropertiesModule,
        StoreModule.forRoot({
          properties: propertiesReducers,
          user: userReducers,
        }),
        EffectsModule.forRoot([PropertiesEffects]),
      ],
      providers: [PrivateFeaturesGuard, PropertiesService, ApiService],
    });
    store = TestBed.get(Store);
  });

  it('should be created', inject([PrivateFeaturesGuard], (guard: PrivateFeaturesGuard) => {
    expect(guard).toBeTruthy();
  }));

  it(`should guard route if on forecast route and 'forecast' flag is set to false`, inject(
    [PrivateFeaturesGuard],
    (guard: PrivateFeaturesGuard) => {
      mockSnapshot.url = '/forecast';
      const mockProperties = cloneDeep(mockPrivateProperties);
      mockProperties.featureFlags.forecast = false;
      store.dispatch(new GetPrivatePropertiesSuccess(mockProperties));
      guard.canActivate(new ActivatedRouteSnapshot(), mockSnapshot).subscribe(canActivate => {
        expect(canActivate).toBeFalsy();
      });
    }
  ));

  it(`should activate forecast route if 'forecast' flag is set to true`, inject([PrivateFeaturesGuard], (guard: PrivateFeaturesGuard) => {
    mockSnapshot.url = '/forecast';
    const mockProperties = cloneDeep(mockPrivateProperties);
    mockProperties.featureFlags.forecast = true;
    store.dispatch(new GetPrivatePropertiesSuccess(mockProperties));
    guard.canActivate(new ActivatedRouteSnapshot(), mockSnapshot).subscribe(canActivate => {
      expect(canActivate).toBeTruthy();
    });
  }));
});
