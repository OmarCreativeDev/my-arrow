import { Injectable } from '@angular/core';

@Injectable()
export class LanguageService {
  language: string = 'en_US';

  public getLanguage() {
    const localeCookie = document.cookie.match('(^|;) ?locale=([^;]*)(;|$)');
    /* istanbul ignore next */
    this.language = localeCookie ? localeCookie[2].replace('-', '_') : 'en_US';
    const headers = {
      'Accept-Language': this.language,
    };

    return headers;
  }
}
