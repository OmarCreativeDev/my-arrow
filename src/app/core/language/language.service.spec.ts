import { TestBed } from '@angular/core/testing';

import { LanguageService } from '@app/core/language/language.service';

describe('LanguageService', () => {
  let languageService: LanguageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LanguageService],
    });
    languageService = TestBed.get(LanguageService);
  });

  it('should be created', () => {
    expect(languageService).toBeTruthy();
  });

  it('should call getLanguage', () => {
    spyOn(languageService, 'getLanguage').and.callThrough();
    languageService.getLanguage();
    expect(languageService.getLanguage).toHaveBeenCalled();
  });
});
