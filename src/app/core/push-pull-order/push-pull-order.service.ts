import { Injectable } from '@angular/core';
import { ApiService } from '@app/core/api/api.service';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { IPushPullOrderRequest } from '@app/core/push-pull-order/push-pull-order.interfaces';

@Injectable()
export class PushPullOrderService {
  constructor(private apiService: ApiService) {}

  public changeOrderDate(data: IPushPullOrderRequest): Observable<{}> {
    return this.apiService.post<{}>(`${environment.baseUrls.pushPullOrder}/change-order`, data);
  }
}
