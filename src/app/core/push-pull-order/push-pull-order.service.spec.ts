import { TestBed, inject } from '@angular/core/testing';

import { PushPullOrderService } from './push-pull-order.service';
import { ApiService } from '@app/core/api/api.service';
import { ApiServiceMock } from '@app/core/api/api.service.spec';

describe('PushPullOrderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PushPullOrderService, { provide: ApiService, useClass: ApiServiceMock }],
    });
  });

  it('should be created', inject([PushPullOrderService], (service: PushPullOrderService) => {
    expect(service).toBeTruthy();
  }));
});
