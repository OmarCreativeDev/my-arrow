export interface IPushPullOrderRequest {
  poNumber: string;
  orderNumber: string;
  coItems?: Array<IPushPullOrderRequestItem>;
}

export interface IPushPullOrderRequestItem {
  currentDate: string;
  customerPartNumber: string;
  manufacturerName: string;
  manufacturerPartNumber: string;
  orderLineItemId: string;
  updatedDate: string;
}
