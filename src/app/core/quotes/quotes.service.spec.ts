import { TestBed, inject } from '@angular/core/testing';
import { QuotesService } from './quotes.service';
import { ApiService } from '@app/core/api/api.service';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { environment } from '@env/environment';
import { IFileTypeEnum, IQuoteDownloadCriteria } from '@app/shared/shared.interfaces';
import { IQuotedRequestToAdd } from './quotes.interfaces';

const mockRequest = {
  sorting: {
    order: '',
    field: '',
  },
  pagination: {
    page: 1,
    limit: 10,
  },
  search: {
    searchQuery: '',
    field: '',
  },
};

const mockQueryRequest = {
  id: '112',
  sorting: {
    order: '',
    field: '',
  },
  pagination: {
    page: 1,
    limit: 10,
  },
};

const mockQuotesToAddRequest: IQuotedRequestToAdd = {
  billToId: 12345,
  currency: 'USD',
  lineItems: [],
  shoppingCartId: '99876b9187',
  userId: '12345678',
};

const mockDownloadQuoteRequest: IQuoteDownloadCriteria = {
  quoteHeaderId: 2815898,
  fileType: IFileTypeEnum.XLS,
  columns: [
    'quoteLineNumber',
    'partNumber',
    'cpn',
    'quantity',
    'leadTime',
    'manufacturer',
    'availability',
    'targetPrice',
    'quotePrice',
    'totalPrice',
    'status',
  ],
};

const mockId = 111;

describe('QuotesService', () => {
  let apiService: ApiService;
  let quotesService: QuotesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuotesService, { provide: ApiService, useClass: ApiServiceMock }],
    });

    apiService = TestBed.get(ApiService);
    quotesService = TestBed.get(QuotesService);
  });

  it('should be created', inject([QuotesService], (service: QuotesService) => {
    expect(service).toBeTruthy();
  }));

  describe('QuotesService should call API service with the correct request method and payload', () => {
    beforeEach(() => {
      spyOn(apiService, 'post');
      spyOn(apiService, 'get');
    });

    it('API Service on `getQuotes()`', () => {
      quotesService.getQuotes(mockRequest);
      expect(apiService.post).toHaveBeenCalledWith(`${environment.baseUrls.serviceQuotes}/search`, mockRequest);
    });

    it('API Service on `getSubmittedQuotesCount()`', () => {
      quotesService.getSubmittedQuotesCount();
      expect(apiService.get).toHaveBeenCalledWith(`${environment.baseUrls.serviceQuotes}/count`);
    });

    it('API Service on `getQuoteLineItems()`', () => {
      quotesService.getQuoteLineItems(mockQueryRequest);
      expect(apiService.post).toHaveBeenCalledWith(
        `${environment.baseUrls.serviceQuotes}/details/${mockQueryRequest.id}/lineitems`,
        mockQueryRequest
      );
    });

    it('API Service on `getQuoteDetails()`', () => {
      quotesService.getQuoteDetails(mockId);
      expect(apiService.get).toHaveBeenCalledWith(`${environment.baseUrls.serviceQuotes}/details/${mockId}`);
    });

    it('API Service on `purchaseQuotes()`', () => {
      quotesService.purchaseQuotes(mockQuotesToAddRequest);
      expect(apiService.post).toHaveBeenCalledWith(`${environment.baseUrls.serviceQuotes}/purchase`, mockQuotesToAddRequest);
    });

    it('API Service on `downloadQuote()`', () => {
      spyOn(apiService, 'postBlob');
      quotesService.downloadQuote(mockDownloadQuoteRequest);
      expect(apiService.postBlob).toHaveBeenCalledWith(`${environment.baseUrls.serviceQuotes}/download`, mockDownloadQuoteRequest);
    });
  });
});
