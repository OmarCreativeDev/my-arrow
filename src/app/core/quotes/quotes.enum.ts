export enum QuoteStatus {
  CUSTOMER_ACCEPTED = 'Customer Accepted',
  PROCESSING = 'Processing',
  EXPIRED = 'Expired',
  PART_QUOTED = 'Partially Quoted',
  FULLY_QUOTED = 'Fully Quoted',
}

export enum QuoteLineItemStatus {
  QUOTED = 'Quoted',
  PROCESSING = 'Processing',
  EXPIRED = 'Expired',
  PURCHASED = 'Purchased',
  IN_CART = 'Added to Cart',
}

export enum QuoteLineItemCartsStatus {
  QUOTED = 'QUOTED',
  PURCHASED = 'PURCHASED',
  IN_CART = 'IN_CART',
}

export enum QuoteWSMessageEventCodes {
  DELETED = 'DELETED',
  FAILED = 'FAILED',
  PURCHASED = 'PURCHASED',
  SUCCESS = 'SUCCESS',
}
