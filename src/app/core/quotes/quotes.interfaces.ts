import { IPaginationState } from '@app/shared/shared.interfaces';
import { HttpErrorResponse } from '@angular/common/http';
import { ISortCriteron } from '@app/shared/shared.interfaces';
import { IProductDetails } from '../inventory/product-details.interface';

export interface IQuotesState {
  loading?: boolean;
  error?: HttpErrorResponse;
  quotes: Array<IQuote>;
  pagination?: IPaginationState;
  searchText: string;
  sort: Array<ISortCriteron>;
}

export interface IQuoteDetailsState {
  loading?: boolean;
  error?: HttpErrorResponse;
  pagination?: IPaginationState;
  searchText: string;
  sort: Array<ISortCriteron>;
  lineItems: Array<IQuoteLineItem>;
  quoteDetails: any;
  quotesPurchased: any;
  productInformationRequestedToLog: IQuotedProductsToAdd;
}

export interface IQuote {
  expiryDate: string;
  itemsQuoted: number;
  owner: string;
  quoteNumber: string;
  quoteHeaderId?: number;
  referenceNumber: string;
  status: string;
  submittedDate: string;
  totalItems: number;
  externalComments: string;
}

export interface IQuotesQueryRequest {
  searchText: string;
  pagination?: {
    limit: number;
    page: number;
  };
  sorting?: {
    order: string;
    field: string;
  };
  id?: number;
}

export interface IQuotesQueryResponse {
  pagination: {
    page: number;
    limit: number;
    total: number;
  };
  quotes?: Array<IQuote>;
  lineItems?: Array<IQuoteLineItem>;
}

export interface IQuoteLineItem {
  cpn: string;
  mpn: string;
  docId?: string;
  foh: number;
  leadtime: string;
  manufacturer: string;
  moq: number;
  multOrdQty: number;
  notes: string;
  partNumber: string;
  productDetail?: IProductDetails;
  quantity: number;
  quotedPrice: number;
  quoteLineId: number;
  quoteLineStatus: string;
  quoteLineNumber: string;
  status?: string;
  targetPrice: number;
  totalPrice: number;
  checked?: boolean;
  tariffAmount?: number;
  tariffApplicable?: boolean;
  externalComments?: string;
}

export interface IQuoteDetailsResponse {
  accountName: string;
  accountNumber: string;
  billTo: {
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
    city: string;
    country: string;
    id: number;
    name: string;
    postCode: string;
    state: string;
  };
  expiryDate: string;
  internalSalesRep: string;
  itemsQuoted: number;
  owner: string;
  quoteNumber: string;
  quoteType: string;
  quotedCurrency: string;
  referenceNumber: string;
  shipTo: {
    addressLine1: string;
    addressLine2: string;
    addressLine3: string;
    city: string;
    country: string;
    id: number;
    name: string;
    postCode: string;
    state: string;
  };
  status: string;
  submittedDate: string;
  terms: string;
  totalCost: number;
  totalItems: number;
}

export interface IQuoteCount {
  count: number;
}

export interface ICheckQuotedDetailLineItem {
  index: number;
  checked: boolean;
}

export interface IQuotedLineItemToAdd {
  quoteHeaderId: number;
  quoteNumber: string;
  quoteLineId: number;
}

export interface IQuotedRequestToAdd {
  billToId: number;
  currency: string;
  lineItems: Array<IQuotedLineItemToAdd>;
  shoppingCartId: string;
  userId: string;
}

export interface IQuotedProductsToAdd {
  quantity: number;
  totalPrice: number;
}

export interface IQuoteLineItemsToAddWSResponse {
  quoteHeaderId: number;
  quoteNumber: string;
  quoteLineId: number;
  quoteLineNumber: string;
  eventCode: string;
  eventMessage: string;
}
