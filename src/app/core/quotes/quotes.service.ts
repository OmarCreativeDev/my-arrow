import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs';
import { IQuotesQueryRequest, IQuoteDetailsResponse, IQuoteCount, IQuotesQueryResponse, IQuotedRequestToAdd } from './quotes.interfaces';
import { environment } from '@env/environment';
import { IQuoteDownloadCriteria } from '@app/shared/shared.interfaces';
import { HttpResponse } from '@angular/common/http';

@Injectable()
export class QuotesService {
  constructor(private apiService: ApiService) {}

  public getQuotes(data): Observable<IQuotesQueryRequest> {
    return this.apiService.post(`${environment.baseUrls.serviceQuotes}/search`, data);
  }

  public getQuoteLineItems(data): Observable<IQuotesQueryResponse> {
    return this.apiService.post(`${environment.baseUrls.serviceQuotes}/details/${data.id}/lineitems`, data);
  }

  public getQuoteDetails(id: number): Observable<IQuoteDetailsResponse> {
    return this.apiService.get<IQuoteDetailsResponse>(`${environment.baseUrls.serviceQuotes}/details/${id}`);
  }

  public getSubmittedQuotesCount(): Observable<IQuoteCount> {
    return this.apiService.get<IQuoteCount>(`${environment.baseUrls.serviceQuotes}/count`);
  }

  public purchaseQuotes(quotesToAddRequest: IQuotedRequestToAdd): Observable<any> {
    return this.apiService.post<IQuoteCount>(`${environment.baseUrls.serviceQuotes}/purchase`, quotesToAddRequest);
  }

  public downloadQuote(downloadCriteria: IQuoteDownloadCriteria): Observable<HttpResponse<Blob>> {
    return this.apiService.postBlob(`${environment.baseUrls.serviceQuotes}/download`, downloadCriteria);
  }
}
