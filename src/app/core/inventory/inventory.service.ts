import { ApiService } from '../api/api.service';
import { combineLatest, Observable } from 'rxjs';
import { environment } from '@env/environment';
import { getCurrencyCode, getUserBillToAccount } from '@app/core/user/store/user.selectors';
import { Injectable } from '@angular/core';
import { IAppState } from '@app/shared/shared.interfaces';
import { IProductCategory, IUserTaxonomy } from '@app/core/inventory/product-category.interface';
import { IProductDetails, IProductValidateResponse } from '@app/core/inventory/product-details.interface';
import { IProductPrice, IProductPriceRequest } from '@app/core/inventory/product-price.interface';
import { IProductSearch } from './product-search.interface';
import { IProductSearchQueryRequest, ISearchListing } from '@app/core/inventory/product-search.interface';
import { IReelPriceResponse, IReelPriceRequest } from '@app/core/reel/reel.interfaces';
import { Store, select } from '@ngrx/store';
import { switchMap } from 'rxjs/operators';
import { IAddToCartRequestItem } from '@app/core/cart/cart.interfaces';

@Injectable()
export class InventoryService {
  /**
   * todo - to remove/replace once angular 6 releases better i18n handling
   * @type {{key: string; value: string}[]}
   */
  public i18nMap = [
    {
      key: 'IN_STOCK',
      value: 'In stock',
    },
    {
      key: 'EU_RHC',
      value: 'Eu RoHS',
    },
    {
      key: 'CHINA_RHC',
      value: 'China RoHS',
    },
  ];

  constructor(private apiService: ApiService, private userStore: Store<IAppState>) {}

  /**
   * todo - to remove/replace once angular 6 releases better i18n handling
   * @param {string} filterName
   * @returns {string}
   */
  public localiseEnum(filterName: string): string {
    let friendlyLabel: string = '';

    this.i18nMap.filter(item => {
      if (item.key === filterName) {
        friendlyLabel = item.value;
      }
    });

    return friendlyLabel;
  }

  public search(searchRequest: IProductSearchQueryRequest): Observable<IProductSearch> {
    return this.apiService.post<IProductSearch>(`${environment.baseUrls.serviceProductSearch}/search`, searchRequest);
  }

  public categories(userTaxonomy: IUserTaxonomy): Observable<Array<IProductCategory>> {
    return this.apiService.get<Array<IProductCategory>>(
      `${environment.baseUrls.serviceProductsCatalog}/categories?io=${userTaxonomy.warehouseList.join()}&billTo=${
        userTaxonomy.billTo
      }&region=${userTaxonomy.region}`
    );
  }

  /**
   * Get billToId and currencyCode from store
   * Then retrieve single price / priceTiers
   * @param {number} itemId
   * @param {number} warehouseId
   * @param {string} cpn
   * @param {number} endCustomerSiteId
   * @returns {Observable<IProductPrice>}
   */
  public price(itemId: number, warehouseId: number, cpn?: string, endCustomerSiteId?: number): Observable<IProductPrice> {
    const billTo$ = this.userStore.pipe(select(getUserBillToAccount));
    const currency$ = this.userStore.pipe(select(getCurrencyCode));

    return combineLatest(billTo$, currency$).pipe(
      switchMap(results => {
        const billTo: number = results[0];
        const currency: string = results[1];
        const params = {
          itemId,
          warehouseId,
          billTo,
          currency,
          ...(cpn && { cpn }),
          ...(endCustomerSiteId && { endCustomerSiteId }),
        };
        return this.apiService.get<IProductPrice>(`${environment.baseUrls.serviceProductSearch}/prices`, params);
      })
    );
  }

  /**
   * Returns product details by product id.
   * @param id
   */
  public getProductDetails(id: string): Observable<IProductDetails> {
    return this.apiService.get<IProductDetails>(`${environment.baseUrls.serviceProductDetails}/${id}`);
  }

  /**
   * Returns cross reference by product id.
   * @param id
   */
  public getProductCrossReference(id: string): Observable<ISearchListing> {
    return this.apiService.get<ISearchListing>(`${environment.baseUrls.serviceProductDetails}/${id}/cross-reference`);
  }

  /**
   * Return the price for a reel item
   * @param reelPriceRequest
   */
  public getReelPrice(reelPriceRequest: IReelPriceRequest): Observable<IReelPriceResponse> {
    return this.apiService.post<IReelPriceResponse>(
      `${environment.baseUrls.serviceProductSearch}/arrowreelsprice`,
      reelPriceRequest,
      false
    );
  }

  /**
   * Return the price for a reel item
   * @param reelPriceRequest
   */
  public validateItems(addToCartRequestItems: Array<IAddToCartRequestItem>): Observable<IProductValidateResponse> {
    return this.apiService.post<IProductValidateResponse>(`${environment.baseUrls.serviceProductDetails}/validate`, {
      items: addToCartRequestItems,
    });
  }

  public getPriceByQuantity(productPriceRequest: IProductPriceRequest): Observable<IProductPrice> {
    const params = {
      itemId: productPriceRequest.itemId,
      warehouseId: productPriceRequest.warehouseId,
      billTo: productPriceRequest.billTo,
      currency: productPriceRequest.currencyCode,
      ...(productPriceRequest.cpn && { cpn: productPriceRequest.cpn }),
      ...(productPriceRequest.endCustomerSiteId && { endCustomerSiteId: productPriceRequest.endCustomerSiteId }),
      quantity: productPriceRequest.quantity,
      shipTo: productPriceRequest.shipTo,
    };
    return this.apiService.get<IProductPrice>(`${environment.baseUrls.serviceProductSearch}/prices`, params);
  }
}
