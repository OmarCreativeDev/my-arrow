import { IApiRequestState } from '@app/shared/shared.interfaces';

export interface ICatalogueState extends IApiRequestState {
  taxonomy: Array<IProductCategory>;
  isTaxonomyLoaded: boolean;
  category: string;
  level: number;
}

export interface IProductCategory {
  id: number;
  name: string;
  totalProducts: number;
  childCategories?: Array<IProductCategory>;
}

export interface IUserTaxonomy {
  warehouseList: Array<string>;
  billTo: number;
  region: string;
}

export interface ISelectedCategory {
  name: string;
  level: number;
}
