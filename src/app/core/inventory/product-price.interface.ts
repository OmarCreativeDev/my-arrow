export interface IProductPrice {
  bufferQuantity?: number;
  message?: string;
  price?: number;
  priceTiers?: Array<IProductPriceTiers>;
  tariffApplicable?: boolean;
  tariffValue?: number;
  purchasable?: boolean;
}

export interface IProductPriceTiers {
  maxQuantity: number;
  minQuantity: number;
  price: number;
}

export interface IProductPriceRequest {
  billTo: number;
  shipTo: number;
  docId: string;
  itemId: number;
  warehouseId: number;
  quantity: number;
  cpn?: string;
  endCustomerSiteId?: number;
  currencyCode: string;
}
