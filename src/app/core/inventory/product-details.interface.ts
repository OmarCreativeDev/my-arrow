import { HttpErrorResponse } from '@angular/common/http';
import { IProductPriceTiers } from '@app/core/inventory/product-price.interface';
import { IEndCustomerRecord, IPipeline, ISearchListing } from '@app/core/inventory/product-search.interface';
import { IApiRequestState, IComplianceState, IProductSpecification } from '@app/shared/shared.interfaces';

export interface IProductDetails {
  arrowReel: boolean;
  availableQuantity: number;
  bufferQuantity: number;
  category: string;
  checked?: boolean;
  compliance: IComplianceState;
  customerPartNumber: string;
  datasheet?: string;
  description: string;
  docId: string;
  endCustomerRecords?: Array<IEndCustomerRecord>;
  id: string;
  image: string;
  itemId: number;
  itemType: string;
  leadTime: string;
  manufacturer: string;
  minimumOrderQuantity: number;
  mpn: string;
  multipleOrderQuantity: number;
  ncnr: boolean;
  pipeline?: IPipeline;
  price: {
    priceTier: IProductPriceTiers[];
  };
  suppAlloc?: string;
  purchasable: boolean;
  specs: Array<IProductSpecification>;
  spq: number;
  warehouseId: number;
  quantity?: number;
  selectedCustomerPartNumber?: string;
  selectedEndCustomerSiteId?: number;
}

export interface IProductDetailsState extends IApiRequestState {
  product: IProductDetails;
  crossReference: ISearchListing;
}

export interface IProductValidateResponseItem {
  itemId: number;
  warehouseId: number;
  docId: string;
  descriptionText?: string;
  proposedQty?: number;
  minimumQty?: number;
  multiple?: number;
  mpn?: string;
}

export interface IProductValidateResponse {
  validItems: Array<IProductValidateResponseItem>;
  invalidItems: Array<IProductValidateResponseItem>;
}

export interface IProductDetailsState {
  loading: boolean;
  product: IProductDetails;
  error: HttpErrorResponse;
}
