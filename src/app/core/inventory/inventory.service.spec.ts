import { ApiService } from '@app/core/api/api.service';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { environment } from '@env/environment';
import { InventoryService } from './inventory.service';
import { IProduct } from '@app/shared/shared.interfaces';
import { IProductCategory, IUserTaxonomy } from '@app/core/inventory/product-category.interface';
import { IProductPrice, IProductPriceRequest } from '@app/core/inventory/product-price.interface';
import { IProductSearchQueryRequest } from '@app/core/inventory/product-search.interface';
import { Observable, of } from 'rxjs';
import { searchResultsMock } from '@app/features/products/components/product-search-listing/product-search-listing.component.spec.ts';
import { Store } from '@ngrx/store';
import { TestBed } from '@angular/core/testing';

const mockSearchRequest: IProductSearchQueryRequest = {
  searchQuery: '',
  paging: {
    limit: 10,
    page: 1,
  },
};

export const mockProduct: IProduct = {
  docId: 'xxxx_xxxx',
  arrowReel: true,
  availableQuantity: 0,
  bufferQuantity: 0,
  compliance: {},
  customerPartNumber: 'string',
  datasheet: 'http://www.sdgsgd.com/file.pdf',
  description: 'Diode Switching 70V 0.215A 3-Pin SOT-23',
  id: '1234_12345678',
  endCustomerRecords: [
    {
      cpn: 'string',
      endCustomers: [
        {
          endCustomerSiteId: 0,
          name: 'string',
        },
      ],
    },
  ],
  image: 'data:image/gif;base64,R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs=',
  itemId: 1,
  itemType: 'COMPONENTS',
  leadTime: 7,
  manufacturer: 'On Semiconductors',
  minimumOrderQuantity: 0,
  mpn: 'BAV99',
  multipleOrderQuantity: 0,
  pipeline: {
    deliveryDate: 'string',
    quantity: 0,
  },
  price: 0,
  quotable: true,
  specs: [
    {
      name: 'EU RoHS Compliant',
      value: 'ON OFF',
    },
  ],
  spq: 1.5,
  warehouseId: 0,
};

const mockProductCategories: Array<IProductCategory> = [
  {
    id: 1,
    name: 'Product Category',
    totalProducts: 100,
  },
];

const mockProductPrice: IProductPrice = {
  price: 1.99,
};

export class StoreMock {
  public select(): Observable<any> {
    return of(1111);
  }
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

describe('InventoryService', () => {
  let inventoryService: InventoryService;
  let apiService: ApiService;
  let userStore: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InventoryService, { provide: ApiService, useClass: ApiServiceMock }, { provide: Store, useClass: StoreMock }],
    });

    apiService = TestBed.get(ApiService);
    inventoryService = TestBed.get(InventoryService);
    userStore = TestBed.get(Store);
  });

  it('should be created', () => {
    expect(inventoryService).toBeTruthy();
  });

  it('should return localised filter labels on `localiseEnum()`', () => {
    expect(inventoryService.localiseEnum(inventoryService.i18nMap[0].key)).toEqual(inventoryService.i18nMap[0].value);
    expect(inventoryService.localiseEnum('')).toEqual('');
    expect(inventoryService.localiseEnum('KEY_THAT_DOESNT_EXIST')).toEqual('');
  });

  it('should return IProductSearch on `search()`', () => {
    spyOn(apiService, 'post').and.returnValue(of(searchResultsMock));
    inventoryService.search(mockSearchRequest).subscribe(() => {
      expect(searchResultsMock.productsTotal).toEqual(2);
      expect(searchResultsMock.products.length).toEqual(2);
    });
    expect(apiService.post).toHaveBeenCalled();
  });

  it('should return an Observable<Array<IProductCategory>> on `categories()`', () => {
    const taxonomyRequest: IUserTaxonomy = { warehouseList: ['V90', 'V91', 'V92'], billTo: 12345, region: 'AC' };
    spyOn(apiService, 'get').and.returnValue(of(mockProductCategories));
    inventoryService.categories(taxonomyRequest).subscribe(categories => {
      expect(categories instanceof Array).toBeTruthy();
      expect(categories[0].id === 1).toBeTruthy();
      expect(categories[0].name === 'Product Category');
    });
  });

  it('should return an Observable<IProductPrice> on `price()`', () => {
    spyOn(apiService, 'get').and.returnValue(of(mockProductPrice));
    spyOn(userStore, 'pipe').and.returnValues(of(1111), of('USD'));

    inventoryService.price(1, 123, '1365393').subscribe(() => {
      expect(apiService.get).toHaveBeenCalledWith(`${environment.baseUrls.serviceProductSearch}/prices`, {
        itemId: 1,
        warehouseId: 123,
        billTo: 1111,
        currency: 'USD',
        cpn: '1365393',
      });
    });
  });

  it('should return an Observable<IProductPrice> on `price()` with a CPN value', () => {
    spyOn(apiService, 'get').and.returnValue(of(mockProductPrice));
    spyOn(userStore, 'pipe').and.returnValues(of(1111), of('USD'));

    inventoryService.price(1, 123, '1365393', 1231131).subscribe(() => {
      expect(apiService.get).toHaveBeenCalledWith(`${environment.baseUrls.serviceProductSearch}/prices`, {
        itemId: 1,
        warehouseId: 123,
        billTo: 1111,
        currency: 'USD',
        cpn: '1365393',
        endCustomerSiteId: 1231131,
      });
    });
  });

  it('should return an Observable<IProductPrice> on `price()` with an End Customer value', () => {
    spyOn(apiService, 'get').and.returnValue(of(mockProductPrice));
    spyOn(userStore, 'pipe').and.returnValues(of(1111), of('USD'));

    inventoryService.price(1, 123, null, 456).subscribe(() => {
      expect(apiService.get).toHaveBeenCalledWith(`${environment.baseUrls.serviceProductSearch}/prices`, {
        itemId: 1,
        warehouseId: 123,
        billTo: 1111,
        currency: 'USD',
        endCustomerSiteId: 456,
      });
    });
  });

  it('should return an Observable<IProductPrice> on `price()` with a CPN and End Customer value', () => {
    spyOn(apiService, 'get').and.returnValue(of(mockProductPrice));
    spyOn(userStore, 'pipe').and.returnValues(of(1111), of('USD'));

    inventoryService.price(1, 123, '1365393', 456).subscribe(() => {
      expect(apiService.get).toHaveBeenCalledWith(`${environment.baseUrls.serviceProductSearch}/prices`, {
        itemId: 1,
        warehouseId: 123,
        billTo: 1111,
        currency: 'USD',
        cpn: '1365393',
        endCustomerSiteId: 456,
      });
    });
  });

  it('should return an Observable<IProduct> on `productDetails()`', () => {
    spyOn(apiService, 'get').and.returnValue(of(mockProduct));
    inventoryService.getProductDetails('1');
    expect(apiService.get).toHaveBeenCalledWith(`${environment.baseUrls.serviceProductDetails}/1`);
  });

  it('should return an Observable<IProduct> on `productDetails()`', () => {
    spyOn(apiService, 'post').and.returnValue(of({ price: 123 }));
    inventoryService.getReelPrice(undefined);
    expect(apiService.post).toHaveBeenCalled();
  });

  it('should return an Observable<IProductPrice>', () => {
    spyOn(apiService, 'get').and.returnValue(
      of({
        tariffApplicable: true,
        tariffValue: 1212,
      })
    );
    const productPriceRequest: IProductPriceRequest = {
      billTo: 11,
      shipTo: 11,
      docId: '11',
      itemId: 1234,
      warehouseId: 567,
      quantity: 50,
      currencyCode: 'USD',
    };
    inventoryService.getPriceByQuantity(productPriceRequest);
    expect(apiService.get).toHaveBeenCalled();
  });
});
