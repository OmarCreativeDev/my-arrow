import { IProduct, IPaginationState } from '@app/shared/shared.interfaces';

// api response
export interface IProductSearch {
  categories?: Array<IProductPrimaryCategory>;
  filters?: Array<IProductFilter>;
  globalInventory?: IGlobalInventory;
  manufacturers?: Array<IProductAdditionalFilter>;
  products: Array<ISearchListing>;
  productsTotal: number;
}

export interface ISearchListing extends IProduct {
  quantity?: number;
  selectedCustomerPartNumber?: string;
  selectedEndCustomerSiteId?: number;
}

export interface IProductSearchListingState extends IProductSearch {
  appliedCategory: string;
  appliedFilters: Array<IProductFilter>;
  appliedManufacturers: Array<IProductAdditionalFilter>;
  error?: Error;
  errorCodeMessage?: string;
  loading: boolean;
  searchQuery: string;
  selectedFilters: Array<string>;
  selectedManufacturers: Array<IProductAdditionalFilter>;
  selectedIds: Array<string>;
  pagination?: IPaginationState;
  sort?: ISortProductSearch;
  quoteOnlyItemsLength: number;
}

export interface IProductSearchQueryRequest {
  category?: string;
  filters?: Array<IProductFilter>;
  manufacturers?: Array<IProductAdditionalFilter>;
  searchQuery: string;
  sort?: ISortProductSearch;
  paging: {
    limit: number;
    page: number;
  };
}

export interface IProductSearchQueryState extends IProductSearch {
  query: string;
}

export interface IAppliedFilters {
  appliedCategory?: string;
  appliedFilters?: Array<IProductFilter>;
  appliedManufacturers?: Array<IProductAdditionalFilter>;
  clearSelectedFilters?: boolean;
}

export interface IProductFilter {
  name: string;
  checked?: boolean;
}

export interface IProductAdditionalFilter extends IProductFilter {
  id: string;
  productsTotal?: number;
}

export interface IProductPrimaryCategory {
  name: string;
  secondaryCategories: Array<IProductSecondaryCategory>;
  isOpen?: boolean;
}

export interface IProductSecondaryCategory {
  name: string;
  tertiaryCategories: Array<IProductAdditionalFilter>;
  isOpen?: boolean;
}

export interface IGlobalInventory {
  additionalPartsAvailable: boolean;
  additionalPartsCount: number;
}

export interface IEndCustomerRecord {
  cpn: string;
  endCustomers: Array<IEndCustomer>;
}

export interface IEndCustomer {
  name: string;
  endCustomerSiteId: number;
}

export interface IPipeline {
  deliveryDate: string;
  quantity: number;
}

export interface ISortProductSearch {
  sortField: string;
  sortDirection: string;
}
