import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { GetPrivateProperties } from '@app/features/properties/store/properties.actions';
import { getPrivateFeatureFlagsSelector, getErrorSelector } from '@app/features/properties/store/properties.selectors';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { getUser } from '@app/core/user/store/user.selectors';
import { isEmpty } from 'lodash-es';

@Injectable()
export class CatalogueGuard implements CanActivate {
  constructor(private router: Router, private store: Store<IAppState>) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return combineLatest(
      this.store.pipe(select(getPrivateFeatureFlagsSelector)),
      this.store.pipe(select(getErrorSelector)),
      this.store.pipe(select(getUser))
    ).pipe(
      tap(([featureFlags, error, user]) => {
        if (isEmpty(featureFlags) && !error) {
          this.store.dispatch(new GetPrivateProperties(`private/${user.registrationInstance.toLocaleLowerCase()}`));
        }
      }),
      filter(([featureFlags, error]) => featureFlags || error),
      map(([featureFlags, error]) => this.handleResponse(featureFlags))
    );
  }

  private handleResponse(featureFlags: object) {
    if (!featureFlags['catalogue']) {
      this.router.navigateByUrl('/dashboard');
      return false;
    } else {
      return true;
    }
  }
}
