export interface ICountry {
  code: String;
  name: String;
}

export interface ICountryList {
  countryList: Array<ICountry>;
}

export interface IProperties {
  public: any;
  private: any;
}

export interface IPropertiesState extends ICountryList, IProperties {
  error?: object;
  loading: boolean;
}
