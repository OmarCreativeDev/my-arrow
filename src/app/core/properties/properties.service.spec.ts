import { TestBed, inject } from '@angular/core/testing';
import { of } from 'rxjs';

import { PropertiesService } from './properties.service';
import { ICountryList } from '@app/core/properties/properties.interface';
import { ApiService } from '@app/core/api/api.service';
import { ApiServiceMock } from '@app/core/api/api.service.spec';

const mockCountryListResults: ICountryList = {
  countryList: [
    {
      code: 'US',
      name: 'United States',
    },
    {
      code: 'CR',
      name: 'Costa Rica',
    },
  ],
};

export const mockDomainProps = {
  arrowce: {
    custServicePhoneNum: '+49(0) 6102 5030-8808',
    custServiceEmail: 'webshop@arroweurope.com',
  },
};

describe('PropertiesService', () => {
  let apiService: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PropertiesService, { provide: ApiService, useClass: ApiServiceMock }],
    });

    apiService = TestBed.get(ApiService);
  });

  it('should be created', inject([PropertiesService], (service: PropertiesService) => {
    expect(service).toBeTruthy();
  }));

  it('getCountryList should return an Observable<ICountryList>', inject([PropertiesService], (service: PropertiesService) => {
    spyOn(apiService, 'get').and.returnValue(of(mockCountryListResults));
    service.getCountryList().subscribe(result => {
      expect(result).toEqual(mockCountryListResults);
    });
  }));

  it('getProperties should return an Observable<Map<string, any>>', inject([PropertiesService], (service: PropertiesService) => {
    spyOn(apiService, 'get').and.returnValue(of(mockDomainProps));
    service.getProperties('domains').subscribe(result => {
      expect(result).toEqual(mockDomainProps);
    });
  }));
});
