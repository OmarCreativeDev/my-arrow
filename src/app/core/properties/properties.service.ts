import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { ApiService } from '@app/core/api/api.service';
import { ICountryList } from '@app/core/properties/properties.interface';

@Injectable()
export class PropertiesService {
  constructor(private apiService: ApiService) {}

  public getCountryList(): Observable<ICountryList> {
    return this.apiService.get<ICountryList>(`${environment.baseUrls.serviceProperties}/countries/`);
  }

  public getProperties(path: string): Observable<any> {
    return this.apiService.get<any>(`${environment.baseUrls.serviceProperties}/${path}`);
  }
}
