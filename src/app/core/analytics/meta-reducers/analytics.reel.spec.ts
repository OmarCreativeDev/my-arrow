import { EventAction, EventCategory, EventType } from '@app/core/analytics/analytics.enums';
import { OpenReelModal } from '@app/core/reel/stores/reel.actions';
import { reelMetaReducers } from '@app/core/analytics/meta-reducers/analytics.reel';
import { INITIAL_REEL_STATE } from '@app/core/reel/stores/reel.reducers';

describe('Reel Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };

    spyOn(window['dataLayer'], 'push');
  });

  it(`should push correct props to DataLayer on REEL_OPEN_MODAL action`, () => {
    const url = '/Test';
    const action = new OpenReelModal({ url });

    reelMetaReducers(INITIAL_REEL_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      eventAction: EventAction.ARROW_REEL_MODAL,
      eventCategory: EventCategory.MODAL,
      eventLabel: url,
      event: EventType.EVENT,
    });
  });
});
