import { getAnalyticsMetaReducers, trackException, trackEvent } from '@app/core/analytics/analytics.utils';
import { EventsMap } from 'redux-beacon';
import { AnalyticsActionTypes, SendAnalyticsException, SendAnalyticsCustomEvent } from '../stores/analytics.actions';
import { googleAnalyticsReducers } from '../stores/analytics.reducer';

const eventsMap: EventsMap = {
  [AnalyticsActionTypes.SEND_EXCEPTION]: (action: SendAnalyticsException) => ({
    ...trackException(action.payload),
  }),

  [AnalyticsActionTypes.SEND_CUSTOM_EVENT]: (action: SendAnalyticsCustomEvent) => ({
    ...trackEvent(action.payload),
  }),
};

export function googleAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, googleAnalyticsReducers)(state, action);
}
