import { EventAction, EventCategory, EventType } from '@app/core/analytics/analytics.enums';
import { QuoteCartLimit, RequestQuote } from '@app/features/quotes/stores/quote-cart.actions';
import { quoteCartAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.quote-cart';
import { INITIAL_QUOTE_CART_STATE } from '@app/features/quotes/stores/quote-cart.reducers';

describe('Quote Cart Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };

    spyOn(window['dataLayer'], 'push');
  });

  it(`should push correct props to DataLayer on QUOTE_CART_LIMIT action`, () => {
    const requestedItemsCount = 101;
    const action = new QuoteCartLimit(requestedItemsCount);

    quoteCartAnalyticsMetaReducers(INITIAL_QUOTE_CART_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventAction: EventAction.QUOTE_CART_LIMIT,
      eventCategory: EventCategory.QUOTE,
      eventLabel: requestedItemsCount,
    });
  });

  it(`should push correct props to DataLayer on REQUEST_QUOTE action`, () => {
    const requestedQuote = 'Name- Price: $10 - Target: $0';
    const action = new RequestQuote(requestedQuote);

    quoteCartAnalyticsMetaReducers(INITIAL_QUOTE_CART_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventAction: EventAction.QUOTE_REQUEST,
      eventCategory: EventCategory.QUOTE,
      eventLabel: requestedQuote,
    });
  });
});
