import { EventAction, EventCategory, EventType } from '@app/core/analytics/analytics.enums';
import { registrationAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.registration';
import {
  RegistrationSubmit,
  RegistrationSubmitSuccess,
  RegistrationSubmitFailed,
} from '@app/features/public/components/registration-form/store/registration.actions';
import { INITIAL_REGISTRATION_STATE } from '@app/features/public/components/registration-form/store/registration.reducers';
import { IRegistration } from '@app/core/registration/registration.interfaces';
import { LabelValue } from '@app/core/analytics/analytics.enums';
import { HttpErrorResponse } from '@angular/common/http';

const userBody: IRegistration = {
  firstName: 'Analytics',
  lastName: 'Test',
  companyName: 'Test company',
  email: 'analyticstest@test.com',
  phone: '88888888',
  salesRepEmail: 'wolf@test.com',
  billingStreet: 'Test 123',
  billingCity: 'Test',
  billingState: 'CA',
  billingPostalCode: '20104',
  billingCountry: 'US',
  shippingStreet: 'Test 123',
  shippingCity: 'Test',
  shippingState: 'CA',
  shippingPostalCode: '20104',
  shippingCountry: 'US',
  assistanceRequiredBySalesRep: false,
  arrowAccountNumber: null,
  source: 'OneWeb',
  jobDescription: 'Systems Engineer',
  acceptedTermsAndConditions: true,
};

describe('Registration Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };
    spyOn(window['dataLayer'], 'push');
  });

  it(`should push correct props to DataLayer on REGISTRATION_SUBMIT action`, () => {
    const action = new RegistrationSubmit(userBody);

    registrationAnalyticsMetaReducers(INITIAL_REGISTRATION_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.REGISTER,
      eventAction: EventAction.REGISTRATION_REQUEST,
      eventLabel: LabelValue.NOT_SET,
    });
  });

  it(`should push correct props to DataLayer on REGISTRATION_SUBMIT_SUCCESS action`, () => {
    const userId = 'john@doe.com';
    const action = new RegistrationSubmitSuccess(userId);

    registrationAnalyticsMetaReducers(INITIAL_REGISTRATION_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.REGISTER,
      eventAction: EventAction.SUCCESS,
      eventLabel: LabelValue.NOT_SET,
    });
  });

  it(`should push correct props to DataLayer on REGISTRATION_SUBMIT_FAILED action`, () => {
    const errorMessage = 'Registration Failed';
    const error = new HttpErrorResponse({ error: { debugError: errorMessage } });
    const action = new RegistrationSubmitFailed(error);

    registrationAnalyticsMetaReducers(INITIAL_REGISTRATION_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.REGISTER,
      eventAction: EventAction.FAIL,
      eventLabel: action.payload.message,
    });
  });
});
