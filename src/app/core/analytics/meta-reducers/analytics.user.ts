import {
  UserActionTypes,
  UserLoggedInComplete,
  UpdateCurrencyCodeComplete,
  RequestUserComplete,
  UserLoggedInFailed,
  UserAfterLoggedInComplete,
  PasswordResetSuccess,
  UserLoggedOut,
} from './../../user/store/user.actions';
import { EventsMap } from 'redux-beacon';

import { userReducers } from '@app/core/user/store/user.reducers';
import { trackEvent, getAnalyticsMetaReducers, trackException } from '@app/core/analytics/analytics.utils';
import { EventCategory, EventAction, EventType } from '@app/core/analytics/analytics.enums';
import { ICustomDimensions } from '@app/core/analytics/analytics.interfaces';
import { HttpErrorResponse } from '@angular/common/http';

export enum UserErrorType {
  USER_ERROR_0000 = '- NOT PARSEABLE ERROR -',
  USER_ERROR_3001 = 'User Not Found',
  USER_ERROR_3002_L2 = 'L2 Issue',
  USER_ERROR_3002_L3 = 'L3 Issue',
  USER_ERROR_3003_ACC = 'Could not change account',
  USER_ERROR_3003_DB = 'Error Comunicating with Data Base',
  USER_ERROR_3004 = 'Authentication Error',
  USER_ERROR_3005 = 'Terms and Conditions could not be registered as accepted',
  USER_ERROR_3009 = 'Error Processing User Information',
  USER_ERROR_3012 = 'Bill-to sites Not Found',
}

function getErrorMessage(err: HttpErrorResponse) {
  const joinMessagePieces = err.error.messages;
  return joinMessagePieces && joinMessagePieces.length > 0 ? joinMessagePieces.join(', ') : UserErrorType.USER_ERROR_0000;
}

function ParseAnalyticError(err: HttpErrorResponse | string): string {
  if (typeof err === 'string') {
    return err;
  }

  const httpErrorResponse = <HttpErrorResponse>err;
  const httpErrorStatusCode = parseInt(httpErrorResponse.error.status, 10) || 0;
  const errorMessage = getErrorMessage(httpErrorResponse);

  switch (httpErrorStatusCode) {
    case 3002: {
      return /L2/i.test(errorMessage)
        ? UserErrorType.USER_ERROR_3002_L2
        : /L3/i.test(errorMessage)
        ? UserErrorType.USER_ERROR_3002_L3
        : errorMessage;
    }
    case 3003: {
      return /account/i.test(errorMessage)
        ? UserErrorType.USER_ERROR_3003_ACC
        : /(base|jdbc)/i.test(errorMessage)
        ? UserErrorType.USER_ERROR_3003_DB
        : errorMessage;
    }
    case 3001:
    case 3004:
    case 3005:
    case 3009:
    case 3012: {
      return UserErrorType['USER_ERROR_' + httpErrorStatusCode];
    }
    default: {
      return errorMessage;
    }
  }
}

/**
 * Map region codes to region names for user friendly GA reports
 */
const regionMap = {
  arrowna: 'Americas',
  arrowce: 'EMEA',
  arrowse: 'EMEA',
  arrowne: 'EMEA',
  arrowap: 'APAC',
  arrownz: 'APAC',
};

/**
 * Pushes the user object to the dataLayer
 * @param action
 */
function pushUser(action: UserLoggedInComplete | RequestUserComplete | UserAfterLoggedInComplete): ICustomDimensions {
  return {
    user: {
      id: action.payload.internalUserId,
      loggedinstate: 1,
      company: action.payload.company,
      region: action.payload.region,
      geo: regionMap[action.payload.region],
      language: action.payload.defaultLanguage,
      currency: action.payload.currencyCode,
      clientId: action.payload.clientId,
    },
    ecommerce: {
      currencyCode: action.payload.currencyCode,
    },
  };
}

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  /**
   * When user logs in push login event and user object
   */
  [UserActionTypes.USER_LOGGED_IN_COMPLETE]: (action: UserLoggedInComplete) => {
    return {
      ...trackEvent({ category: EventCategory.LOGIN, action: EventAction.SUCCESS, label: action.payload.internalUserId }),
      ...pushUser(action),
    };
  },
  /**
   * When user fails login (currently capturing both auth failure and profile errors)
   */
  [UserActionTypes.USER_LOGGED_IN_FAILED]: (action: UserLoggedInFailed) => {
    try {
      return {
        ...trackEvent({
          category: EventCategory.LOGIN,
          action: EventAction.FAIL,
          label: ParseAnalyticError(action.payload),
        }),
      };
    } catch (e) {
      return { ...trackException(e) };
    }
  },
  /**
   * When user is requested (this happens when user is returning to an authenticated session) just push the user
   */
  [UserActionTypes.REQUEST_USER_COMPLETE]: (action: RequestUserComplete) => pushUser(action),

  /**
   * When user RESET_USER action is called (when user logs out / tokens are cleared) we need to also reset the user object in the dataLayer
   */
  [UserActionTypes.RESET_USER]: () => ({
    user: {
      id: -1,
      loggedinstate: 0,
      company: undefined,
      region: undefined,
      geo: undefined,
      language: undefined,
      currency: undefined,
      clientId: undefined,
    },
    ecommerce: undefined,
  }),

  /**
   * When user changes currency code trigger currency event and update relevant currency props
   */
  [UserActionTypes.USER_UPDATE_CURRENCY_CODE_COMPLETE]: (action: UpdateCurrencyCodeComplete) => ({
    ...trackEvent({ category: EventCategory.CURRENCY, action: EventAction.SELECT, label: action.payload }),
    user: {
      currency: action.payload,
    },
    ecommerce: {
      currencyCode: action.payload,
    },
  }),

  /**
   * When Admin UI logs as user push login event and user object
   */
  [UserActionTypes.USER_AFTER_LOGGED_IN_COMPLETE]: (action: UserAfterLoggedInComplete) => ({
    ...trackEvent({ category: EventCategory.LOGIN, action: EventAction.SUCCESS, label: action.payload.internalUserId }),
    ...pushUser(action),
  }),

  /**
   * When user completes password reset process
   */
  [UserActionTypes.USER_PASSWORD_RESET]: (action: PasswordResetSuccess) => ({
    ...trackEvent({ category: EventCategory.LOGIN, action: EventAction.FORGOT_PASSWORD, label: action.payload }),
  }),

  /**
   * When user logs off automatically after a timeout
   */
  [UserActionTypes.USER_LOGGED_OUT]: (action: UserLoggedOut) => ({
    ...trackEvent({
      event: EventType.SESSION_EVENT,
      category: EventCategory.LOGOUT,
      action: EventAction.AUTOLOGOUT,
      label: action.payload,
    }),
  }),

  /**
   * Whenever a token refresh event triggers
   */
  [UserActionTypes.TOKEN_REFRESH]: () => ({
    ...trackEvent({ event: EventType.SESSION_EVENT, category: EventCategory.SESSION_EVENTS, action: EventAction.TOKEN_REFRESH }),
  }),

  /**
   * Whenever a web socket connection event happens
   */
  [UserActionTypes.WEB_SOCKET_CONNECT]: () => ({
    ...trackEvent({ event: EventType.SESSION_EVENT, category: EventCategory.SESSION_EVENTS, action: EventAction.WEB_SOCKET_CONNECT }),
  }),

  /**
   * Whenever a web socket reconnection event happens
   */
  [UserActionTypes.WEB_SOCKET_RECONNECT]: () => ({
    ...trackEvent({ event: EventType.SESSION_EVENT, category: EventCategory.SESSION_EVENTS, action: EventAction.WEB_SOCKET_RECONNECT }),
  }),

  /**
   * Whenever a web socket disconnection event happens
   */
  [UserActionTypes.WEB_SOCKET_DISCONNECT]: () => ({
    ...trackEvent({ event: EventType.SESSION_EVENT, category: EventCategory.SESSION_EVENTS, action: EventAction.WEB_SOCKET_DISCONNECT }),
  }),
};

/**
 * Export meta-reducer
 */
export function userAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, userReducers)(state, action);
}
