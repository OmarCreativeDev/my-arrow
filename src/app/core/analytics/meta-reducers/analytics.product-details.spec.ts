import { EventType, EventAction, EventCategory } from '@app/core/analytics/analytics.enums';
import { GetProductDetailsSuccess } from '@app/features/products/stores/product-details/product-details.actions';
import { mockProductDetails } from '@app/features/products/components/product-detail/product-detail.component.spec';
import { productDetailsAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.product-details';
import { INITIAL_PRODUCT_DETAILS_STATE } from '@app/features/products/stores/product-details/product-details.reducers';

describe('Product Details Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };
    spyOn(window['dataLayer'], 'push');
  });

  it(`should push correct props to DataLayer on GET_PRODUCT_DETAILS_SUCCESS action`, () => {
    const action = new GetProductDetailsSuccess(mockProductDetails);
    productDetailsAnalyticsMetaReducers(INITIAL_PRODUCT_DETAILS_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.ECOMMERCE,
      eventCategory: EventCategory.ECOMMERCE,
      eventAction: EventAction.VIEW_PRODUCT_DETAILS,
      eventLabel: 'BAV99',
      ecommerce: {
        detail: {
          products: [
            {
              name: 'BAV99',
              id: 1,
              brand: 'On Semiconductors',
            },
          ],
        },
      },
    });
  });
});
