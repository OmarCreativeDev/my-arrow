import {
  AddToCartSuccess,
  AddToQuoteSuccess,
  CartItemsDeletionSuccess,
  GetCartDataSuccess,
  ShoppingCartLimit,
} from '@app/features/cart/stores/cart/cart.actions';
import { INITIAL_CART_STATE } from '@app/features/cart/stores/cart/cart.reducers';
import { cartAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.cart';
import { EventType, EventAction, EventCategory, ListType } from '@app/core/analytics/analytics.enums';
import { IShoppingCartResponse, ICartSelectedProduct } from '@app/core/cart/cart.interfaces';

describe('Cart Analytics', () => {
  const mockAddToCart = {
    request: {
      billToId: 2143261,
      currency: 'EUR',
      lineItems: [
        {
          manufacturerPartNumber: 'BAV99',
          docId: '1989_00140804',
          itemId: 140804,
          warehouseId: 1989,
          quantity: 50,
          requestDate: '2018-06-20',
        },
      ],
      shoppingCartId: '5b244051eb8c7a0010ba1eaf',
      quoteOnlyItemsLength: 0,
    },
    path: '/products/search/bav99',
  };

  const mockAddToQuoteCart = {
    lineItems: [
      {
        docId: '1989_00140804',
        itemId: 140804,
        targetPrice: null,
        warehouseId: 1989,
        quantity: 50,
        requestDate: '2018-06-20',
        manufacturerPartNumber: 'BAV99',
      },
      {
        docId: '1985_00140805',
        itemId: 140805,
        targetPrice: null,
        warehouseId: 1985,
        quantity: 23,
        requestDate: '2018-06-20',
        manufacturerPartNumber: 'BAV99',
      },
    ],
  };

  const mockDeletedProducts: ICartSelectedProduct[] = [{ id: 1234, name: 'BAV99' }, { id: 5678, name: 'BAV100' }];

  const mockCartResponse: IShoppingCartResponse = {
    accountNumber: 12345,
    company: 'CISCO SYSTEMS, INC.',
    billToId: 2174252,
    currency: 'USD',
    id: '5b30e285e42c2d001257539a',
    userId: 'dghitton@cisco.com',
    status: 'IN_PROGRESS',
    createdDate: '2018-06-25T12:39:33.321',
    updatedDate: '2018-06-25T12:39:33.321',
    lineItems: [
      {
        itemId: 6297775,
        warehouseId: 1790,
        docId: '1790_06297775',
        requestDate: '2018-06-25',
        quantity: 3000,
        id: '5b30e404e42c2d001257539b',
        total: 95.75999999999999,
        priceTiers: [
          { quantityFrom: 1, quantityTo: 3000, pricePerItem: 0.03192 },
          { quantityFrom: 3001, quantityTo: 30000, pricePerItem: 0.02988 },
          { quantityFrom: 30001, quantityTo: 1500000, pricePerItem: 0.02742 },
        ],
        manufacturerPartNumber: 'BAV99',
        lastValidationDate: '2018-06-25T12:46:11.041',
        manufacturer: 'ON Semiconductor|ONSEMI',
        description: 'Diode Small Signal Switching 70V 0.2A 3-Pin SOT-23 T/R',
        image: '',
        inStock: false,
        arrowReel: false,
        minimumOrderQuantity: 3000,
        multipleOrderQuantity: 3000,
        availableQuantity: 0,
        leadTime: '42',
        endCustomerRecords: [],
        price: 0.03192,
        originalQuantity: 3000,
      },
    ],
    itemCount: 1,
  };

  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };
    spyOn(window['dataLayer'], 'push');
  });

  it(`should push correct props to DataLayer on ADD_TO_CART_SUCCESS action`, () => {
    const action = new AddToCartSuccess(mockAddToCart);
    cartAnalyticsMetaReducers(INITIAL_CART_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.ECOMMERCE,
      eventCategory: EventCategory.ECOMMERCE,
      eventAction: EventAction.ADD_TO_CART,
      eventLabel: 'BAV99',
      ecommerce: {
        currencyCode: 'EUR',
        add: {
          actionField: { list: ListType.SEARCH_PAGE },
          products: [{ id: 140804, name: 'BAV99', quantity: 50 }],
        },
      },
    });
  });

  it(`should push correct props to DataLayer on DELETE_ITEMS_SUCCESS action`, () => {
    const action = new CartItemsDeletionSuccess(mockDeletedProducts);
    cartAnalyticsMetaReducers(INITIAL_CART_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.ECOMMERCE,
      eventCategory: EventCategory.ECOMMERCE,
      eventAction: EventAction.REMOVE_FROM_CART,
      eventLabel: 'BAV99, BAV100',
      ecommerce: {
        remove: {
          products: mockDeletedProducts,
        },
      },
    });
  });

  it(`should push correct props to DataLayer on ADD_TO_QUOTE_SUCCESS action`, () => {
    const action = new AddToQuoteSuccess(mockAddToQuoteCart);
    cartAnalyticsMetaReducers(INITIAL_CART_STATE, action);
    const eventLabel = mockAddToQuoteCart.lineItems.map(lineItem => lineItem.manufacturerPartNumber).join(', ');
    const eventValue = mockAddToQuoteCart.lineItems.reduce((sum, lineItem) => {
      return sum + lineItem.quantity;
    }, 0);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventAction: EventAction.ADD_TO_QUOTE,
      eventCategory: EventCategory.QUOTE,
      eventLabel,
      eventValue,
    });
  });

  it(`should push correct props to DataLayer on GET_CART_DATA_SUCCESS action`, () => {
    const action = new GetCartDataSuccess(mockCartResponse);
    cartAnalyticsMetaReducers(INITIAL_CART_STATE, action);
    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.ECOMMERCE,
      eventCategory: EventCategory.ECOMMERCE,
      eventAction: EventAction.VIEW_CART,
      eventLabel: '5b30e285e42c2d001257539a',
      ecommerce: {
        checkout: {
          actionField: { step: 1 },
          products: [{ name: 'BAV99', id: 6297775, price: 0.03192, brand: 'ON Semiconductor|ONSEMI', quantity: 3000 }],
        },
      },
    });
  });

  it(`should push correct props to DataLayer on SHOPPING_CART_LIMIT action`, () => {
    const shoppingCartCAP = 100;
    const action = new ShoppingCartLimit(shoppingCartCAP);

    cartAnalyticsMetaReducers(INITIAL_CART_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventAction: EventAction.SHOPPING_CART_LIMIT,
      eventCategory: EventCategory.SHOPPING_CART,
      eventLabel: shoppingCartCAP,
    });
  });
});
