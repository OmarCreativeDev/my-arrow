import { INITIAL_CART_STATE } from '@app/features/cart/stores/cart/cart.reducers';
import {
  SubmitProductSearchQuery,
  SubmitProductSearchQueryComplete,
  ProductClicked,
} from '@app/features/products/stores/product-search/product-search.actions';
import { productSearchAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.product-search';
import { EventType, EventAction, ListType, EventCategory } from '@app/core/analytics/analytics.enums';

describe('Product Search Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };
    spyOn(window['dataLayer'], 'push');
  });

  const mockProduct = {
    customerPartNumber: 'x123',
    image: null,
    price: null,
    specs: null,
    arrowReel: false,
    availableQuantity: 0,
    bufferQuantity: 0,
    compliance: { EU_ROHS: 'EU_ROHS_RHC' },
    datasheet: 'http://download.siliconexpert.com/pdfs/2017/11/2/13/18/29/763/ons_/manual/3668116925726701bav21.pdf',
    description: 'Diode Small Signal Switching 200V 0.2A 2-Pin DO-35 Bulk',
    id: '1790_07703665',
    itemId: 7703665,
    itemType: 'COMPONENTS',
    leadTime: 99,
    manufacturer: 'ON Semiconductor',
    minimumOrderQuantity: 1,
    mpn: 'BAV20',
    multipleOrderQuantity: 1,
    quotable: true,
    spq: 2000,
    warehouseId: 1790,
    warehouseCode: 'V36',
    pkg: 'BULK',
    docId: '1790_07703665',
  };

  const mockQuery = { searchQuery: 'bav99', category: '', sort: { sortField: '', sortDirection: '' }, paging: { limit: 10, page: 1 } };

  const mockSearchResponse = {
    categories: [
      {
        id: '^2015',
        name: 'Semiconductor - Discrete',
        secondaryCategories: [{ name: 'Diodes', tertiaryCategories: [{ name: 'Rectifier', productsTotal: 2, id: '^2015/27331/2021' }] }],
      },
    ],
    filters: [{ name: 'IN_STOCK' }, { name: 'EU_RHC' }, { name: 'CHINA_RHC' }],
    globalInventory: { additionalPartsAvailable: true, additionalPartsCount: -1 },
    manufacturers: [{ id: 'on semiconductor|onsemi', name: 'on semiconductor', productsTotal: 2 }],
    products: [mockProduct],
    pagination: { limit: 10, current: 1, total: 1 },
    productsTotal: 2,
    query: 'bav99',
  };

  it(`should push correct props to DataLayer on QUERY action`, () => {
    const action = new SubmitProductSearchQuery(mockQuery);
    productSearchAnalyticsMetaReducers(INITIAL_CART_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.SEARCH,
      eventAction: EventAction.PRODUCT_SEARCH,
      eventLabel: 'bav99',
    });
  });

  it(`should push correct props to DataLayer on QUERY_COMPLETE action`, () => {
    const action = new SubmitProductSearchQueryComplete(mockSearchResponse);
    productSearchAnalyticsMetaReducers(INITIAL_CART_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.ECOMMERCE,
      eventCategory: EventCategory.ECOMMERCE,
      eventAction: EventAction.VIEW_PRODUCT_SEARCH_RESULTS,
      eventLabel: 'bav99',
      ecommerce: {
        impressions: [{ name: 'BAV20', id: 7703665, brand: 'ON Semiconductor', list: ListType.SEARCH_PAGE, position: 1 }],
      },
    });
  });

  it(`should push correct props to DataLayer on PRODUCT_CLICKED action`, () => {
    const action = new ProductClicked({ product: mockProduct, index: 0 });
    productSearchAnalyticsMetaReducers(INITIAL_CART_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.ECOMMERCE,
      eventCategory: EventCategory.ECOMMERCE,
      eventAction: EventAction.PRODUCT_CLICK,
      eventLabel: 'BAV20',
      ecommerce: {
        click: {
          actionField: { list: ListType.SEARCH_PAGE },
          products: [{ name: 'BAV20', id: 7703665, brand: 'ON Semiconductor', position: 1 }],
        },
      },
    });
  });
});
