import { EventAction, EventCategory, EventType } from '@app/core/analytics/analytics.enums';
import { LayoutAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.layout';
import { INITIAL_RESULTS_STATE } from '@app/layout/store/layout.reducers';
import { LanguageDropdownClick, LanguageSelect } from '@app/layout/store/layout.actions';

describe('Layout Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };

    spyOn(window['dataLayer'], 'push');
  });

  it(`should push correct props to DataLayer on LANGUAGE_DROPDOWN_CLICK action`, () => {
    const mockUserId = '1305827';
    const action = new LanguageDropdownClick(mockUserId);

    LayoutAnalyticsMetaReducers(INITIAL_RESULTS_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventAction: EventAction.DROP_DOWN_CLICK,
      eventCategory: EventCategory.SITE_LANGUAGE,
      eventLabel: mockUserId,
    });
  });
  it(`should push correct props to DataLayer on LANGUAGE_SELECT action`, () => {
    const mockLangunageSelected = 'de';
    const action = new LanguageSelect(mockLangunageSelected);

    LayoutAnalyticsMetaReducers(INITIAL_RESULTS_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventAction: EventAction.SELECT,
      eventCategory: EventCategory.SITE_LANGUAGE,
      eventLabel: mockLangunageSelected,
    });
  });
});
