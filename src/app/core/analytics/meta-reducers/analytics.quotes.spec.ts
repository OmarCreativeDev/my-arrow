import { EventAction, EventCategory, EventType } from '@app/core/analytics/analytics.enums';
import { quotesAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.quotes';
import { Query } from '@app/features/submitted-quotes/stores/quotes/quotes.actions';
import { INITIAL_QUOTE_STATE } from '@app/features/submitted-quotes/stores/quotes/quotes.reducers';

describe('Quotes Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };
    spyOn(window['dataLayer'], 'push');
  });

  const mockQuery = { searchText: 'Bav99', sorting: { order: '', field: '' }, pagination: { limit: 10, page: 1 } };

  it(`should push correct props to DataLayer on QUERY action`, () => {
    const action = new Query(mockQuery);

    quotesAnalyticsMetaReducers(INITIAL_QUOTE_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.SEARCH,
      eventAction: EventAction.QUOTE_SEARCH,
      eventLabel: 'bav99',
    });
  });
});
