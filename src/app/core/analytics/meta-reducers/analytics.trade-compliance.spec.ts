import { EventAction, EventCategory, EventType } from '@app/core/analytics/analytics.enums';
import {
  TradeComplianceSearch,
  ComplianceCheckUpload,
} from '@app/features/trade-compliance/components/search-results/store/search-results.actions';
import { tradeComplianceAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.trade-compliance';
import { INITIAL_RESULTS_STATE } from '@app/features/trade-compliance/components/search-results/store/search-results.reducers';

describe('Trade Compliance Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };

    spyOn(window['dataLayer'], 'push');
  });

  it(`should push correct props to DataLayer on TRADE_COMPLIANCE_SEARCH action`, () => {
    const requestedTradeComplianceSearchInfo = 'NAFTA: 2014: 10-527334-19R';
    const action = new TradeComplianceSearch(requestedTradeComplianceSearchInfo);

    tradeComplianceAnalyticsMetaReducers(INITIAL_RESULTS_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventAction: EventAction.COMPLIANCE_SEARCH,
      eventCategory: EventCategory.SEARCH,
      eventLabel: requestedTradeComplianceSearchInfo,
    });
  });

  it(`should push correct props to DataLayer on COMPLIANCE_CHECK_UPLOAD action`, () => {
    const requestedTradeComplianceCheckUpload = 'type';
    const action = new ComplianceCheckUpload('type');

    tradeComplianceAnalyticsMetaReducers(INITIAL_RESULTS_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventAction: EventAction.COMPLIANCE_CHECK_UPLOAD,
      eventCategory: EventCategory.COMPLIANCE,
      eventLabel: requestedTradeComplianceCheckUpload,
    });
  });
});
