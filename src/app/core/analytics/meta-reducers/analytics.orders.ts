import { EventAction, EventCategory } from '@app/core/analytics/analytics.enums';
import { getAnalyticsMetaReducers, trackEvent } from '@app/core/analytics/analytics.utils';
import {
  EmailRepresentative,
  OrdersActionTypes,
  PushPullOrder,
  SetFilter,
  SubmitSearch,
  RequestReturnOrder,
  SubmitRequestReturnOrder,
} from '@app/features/orders/stores/orders/orders.actions';
import { ordersReducers } from '@app/features/orders/stores/orders/orders.reducers';
import { EventsMap } from 'redux-beacon';

/**
 * GTM dataLayer mappings
 */

const eventsMap: EventsMap = {
  /**
   * When user submits a search
   */
  [OrdersActionTypes.SUBMIT_SEARCH]: (action: SubmitSearch) => {
    const search = action.payload;
    // we don't want to track RESET
    if (!search.value) {
      return;
    }
    return trackEvent({ category: EventCategory.SEARCH, action: EventAction.ORDER_SEARCH, label: `${search.type}: ${search.value}` });
  },

  /**
   * When user submits a filter
   */
  [OrdersActionTypes.SET_FILTER]: (action: SetFilter) => {
    if (!action.track) {
      return;
    }
    return trackEvent({ category: EventCategory.ORDERS, action: EventAction.ORDER_FILTER, label: action.label });
  },

  /**
   * When user emails representative push orders ids
   */
  [OrdersActionTypes.EMAIL_REPRESENTATIVE]: (action: EmailRepresentative) => {
    const ordersId = action.POnumbers.sort().join(', ');
    return trackEvent({
      category: EventCategory.ORDERS,
      action: EventAction.EMAIL_SALES_REPRESENTATIVE,
      label: ordersId,
    });
  },

  /**
   * When user edit requested date
   */
  [OrdersActionTypes.PUSH_PULL_ORDER]: (action: PushPullOrder) => {
    const products = action.ProductNames.sort().join(', ');
    return trackEvent({
      category: EventCategory.ORDERS,
      action: EventAction.PUSH_PULL_ORDER,
      label: products,
    });
  },

  /**
   * When user request to make a return
   */
  [OrdersActionTypes.REQUEST_RETURN_ORDER]: (action: RequestReturnOrder) => {
    return trackEvent({
      category: EventCategory.ORDERS,
      action: EventAction.REQUEST_RETURN,
      label: 'Request Return',
    });
  },

  /**
   * When user submits the form for request a return
   */
  [OrdersActionTypes.SUBMIT_REQUEST_RETURN_ORDER]: (action: SubmitRequestReturnOrder) => {
    return trackEvent({
      category: EventCategory.ORDERS,
      action: EventAction.RETURN_SUBMITTED,
      label: action.payload,
    });
  },
};

/**
 * Export meta-reducer
 */
export function ordersAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, ordersReducers)(state, action);
}
