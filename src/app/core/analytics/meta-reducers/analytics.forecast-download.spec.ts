import {
  OpenedForecastDownloadModal,
  SelectedForecastDownloadFileColumns,
  SelectedForecastDownloadFileDateRange,
  SelectedForecastDownloadFileType,
} from '@app/features/forecast/stores/forecast-download/forecast-download.actions';
import { IForecastDownload, IForecastDownloadModal } from '@app/core/forecast/forecast.interfaces';
import { forecastDownloadAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.forecast-download';
import { INITIAL_FORECAST_DOWNLOAD_STATE } from '@app/features/forecast/stores/forecast-download/forecast-download.reducers';
import { EventAction, EventCategory, EventType } from '@app/core/analytics/analytics.enums';

describe('Forecast Download Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };
    spyOn(window['dataLayer'], 'push');
  });

  const mockForecastDownloadModal: IForecastDownloadModal = {
    url: 'http://forecastdownload.url.test',
  };

  const mockForecastDownloadParams: IForecastDownload = {
    accountNumber: 1,
    billToNumber: 1,
    smrName: '',
    fileType: 'CSV',
    columns: ['manufacturerPartNumber', 'buffer', 'inventoryItemId'],
    weeksInHorizon: 8,
  };

  it(`should push correct props to DataLayer on OPENED_FORECAST_DOWNLOAD_MODAL action`, () => {
    const action = new OpenedForecastDownloadModal(mockForecastDownloadModal);
    forecastDownloadAnalyticsMetaReducers(INITIAL_FORECAST_DOWNLOAD_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.MODAL,
      eventAction: EventAction.FORECAST_DOWNLOAD_MODAL,
      eventLabel: mockForecastDownloadModal.url,
    });
  });

  it(`should push correct props to DataLayer on SELECTED_FORECAST_DOWNLOAD_FILE_TYPE action`, () => {
    const action = new SelectedForecastDownloadFileType(mockForecastDownloadParams);
    forecastDownloadAnalyticsMetaReducers(INITIAL_FORECAST_DOWNLOAD_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.FORECAST,
      eventAction: EventAction.MODAL_DOWNLOAD_TYPE,
      eventLabel: mockForecastDownloadParams.fileType,
    });
  });

  it(`should push correct props to DataLayer on SELECTED_FORECAST_DOWNLOAD_FILE_COLUMNS action`, () => {
    const action = new SelectedForecastDownloadFileColumns(mockForecastDownloadParams);
    forecastDownloadAnalyticsMetaReducers(INITIAL_FORECAST_DOWNLOAD_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.FORECAST,
      eventAction: EventAction.DOWNLOAD_COLUMNS,
      eventLabel: mockForecastDownloadParams.columns.sort().join(', '),
    });
  });

  it(`should push correct props to DataLayer on SELECTED_FORECAST_DOWNLOAD_FILE_DATE_RANGE action`, () => {
    const action = new SelectedForecastDownloadFileDateRange(mockForecastDownloadParams);
    forecastDownloadAnalyticsMetaReducers(INITIAL_FORECAST_DOWNLOAD_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.FORECAST,
      eventAction: EventAction.DOWNLOAD_DATE_RANGE,
      eventLabel: mockForecastDownloadParams.weeksInHorizon,
    });
  });
});
