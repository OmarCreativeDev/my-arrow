import { EventType, EventAction, EventCategory } from '@app/core/analytics/analytics.enums';
import { EmailRepresentative, SubmitSearch, SubmitRequestReturnOrder } from '@app/features/orders/stores/orders/orders.actions';
import { INITIAL_ORDERS_STATE } from '@app/features/orders/stores/orders/orders.reducers';
import { ordersAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.orders';
import { ISearchCriteria } from '@app/shared/shared.interfaces';

describe('Orders Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };
    spyOn(window['dataLayer'], 'push');
  });

  const mockQuery: ISearchCriteria = {
    value: 'xyz1234',
    type: 'purchaseOrderNumber',
  };

  it(`should push correct props to DataLayer on QUERY action`, () => {
    const action = new SubmitSearch(mockQuery);
    ordersAnalyticsMetaReducers(INITIAL_ORDERS_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.SEARCH,
      eventAction: EventAction.ORDER_SEARCH,
      eventLabel: 'purchaseOrderNumber: xyz1234',
    });
  });

  it(`should push correct props to DataLayer on EmailRepresentative action`, () => {
    const action = new EmailRepresentative(['PO123', 'PO4321']);
    ordersAnalyticsMetaReducers(INITIAL_ORDERS_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.ORDERS,
      eventAction: EventAction.EMAIL_SALES_REPRESENTATIVE,
      eventLabel: action.POnumbers.sort().join(', '),
    });
  });

  it(`should push correct props to DataLayer on SubmitRequestReturnOrder action`, () => {
    const returnReason: string = 'Damaged';
    const action = new SubmitRequestReturnOrder(returnReason);
    ordersAnalyticsMetaReducers(INITIAL_ORDERS_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.ORDERS,
      eventAction: EventAction.RETURN_SUBMITTED,
      eventLabel: 'Damaged',
    });
  });
});
