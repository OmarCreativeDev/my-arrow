import { POUploadSuccess, POUploadFailed } from '@app/features/dashboard/stores/dashboard.actions';
import { dashboardAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.dashboard';
import { EventCategory, EventAction } from '@app/core/analytics/analytics.enums';
import { IPOUpload } from '@app/core/purchase-order/purchase-order.interface';
import { INITIAL_DASHBOARD_STATE } from '@app/features/dashboard/stores/dashboard.reducers';
import { HttpErrorResponse } from '@angular/common/http';

describe('Dashboard Analytics', () => {
  const mockFormData: IPOUpload = {
    userEmail: 'first.oncorems@testl4.com',
    region: 'arrowna',
    uploadType: 'CONEXIOM',
    poNumber: '1234',
  };
  const file = `{
    lastModified: 1529929488154,
    lastModifiedDate: 'Mon Jun 25 2018 13:24:48 GMT+0100 (BST) {}',
    name: 'order-test.pdf',
    size: 359780,
    type: 'application/pdf',
    webkitRelativePath: '',
  }`;
  const formData: FormData = new FormData();

  formData.append('file', file, 'order-test.pdf');
  formData.append('data', JSON.stringify(mockFormData));

  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };
    spyOn(window['dataLayer'], 'push');
  });

  it(`should push correct props to DataLayer on PO_UPLOAD_SUCCESS action`, () => {
    const action = new POUploadSuccess(mockFormData.poNumber);
    dashboardAnalyticsMetaReducers(INITIAL_DASHBOARD_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: 'EVENT',
      eventCategory: EventCategory.PURCHASE_ORDER,
      eventAction: EventAction.UPLOAD_SUCCESS,
      eventLabel: '1234',
    });
  });

  it(`should push correct props to DataLayer on PO_UPLOAD_FAILED action`, () => {
    const action = new POUploadFailed(new HttpErrorResponse({ error: { error: 'Fail reason' } }));
    dashboardAnalyticsMetaReducers(INITIAL_DASHBOARD_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: 'EVENT',
      eventCategory: EventCategory.PURCHASE_ORDER,
      eventAction: EventAction.UPLOAD_FAILED,
      eventLabel: 'Fail reason',
    });
  });
});
