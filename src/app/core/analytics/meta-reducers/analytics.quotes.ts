import { EventsMap } from 'redux-beacon';

import { trackEvent, getAnalyticsMetaReducers } from '@app/core/analytics/analytics.utils';
import { EventAction, EventCategory } from '@app/core/analytics/analytics.enums';

import { quotesReducer } from '@app/features/submitted-quotes/stores/quotes/quotes.reducers';
import { QuotesActionTypes, Query } from '@app/features/submitted-quotes/stores/quotes/quotes.actions';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  /**
   * Quotes queries
   */
  [QuotesActionTypes.QUERY]: (action: Query) => {
    return action.payload.searchText.trim().toLowerCase().length === 0
      ? null
      : {
          ...trackEvent({
            category: EventCategory.SEARCH,
            action: EventAction.QUOTE_SEARCH,
            label: action.payload.searchText.toLowerCase(),
          }),
        };
  },
};

/**
 * Export meta-reducer
 */
export function quotesAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, quotesReducer)(state, action);
}
