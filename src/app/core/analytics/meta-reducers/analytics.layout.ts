import { EventsMap } from 'redux-beacon';

import { trackEvent, getAnalyticsMetaReducers } from '@app/core/analytics/analytics.utils';
import { EventAction, EventCategory } from '@app/core/analytics/analytics.enums';
import { LayoutActionTypes, LanguageDropdownClick, LanguageSelect } from '@app/layout/store/layout.actions';
import { layoutReducers } from '@app/layout/store/layout.reducers';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  /**
   * Layout queries
   */
  [LayoutActionTypes.LANGUAGE_DROPDOWN_CLICK]: (action: LanguageDropdownClick) => ({
    ...trackEvent({
      category: EventCategory.SITE_LANGUAGE,
      action: EventAction.DROP_DOWN_CLICK,
      label: action.payload,
    }),
  }),
  [LayoutActionTypes.LANGUAGE_SELECT]: (action: LanguageSelect) => ({
    ...trackEvent({
      category: EventCategory.SITE_LANGUAGE,
      action: EventAction.SELECT,
      label: action.payload,
    }),
  }),
};

/**
 * Export meta-reducer
 */
export function LayoutAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, layoutReducers)(state, action);
}
