import { PlaceOrderSuccess, GetCheckoutDataSuccess, ActivateNcnrModal } from '@app/features/cart/stores/checkout/checkout.actions';
import { checkoutAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.checkout';
import { INITIAL_CHECKOUT_STATE } from '@app/features/cart/stores/checkout/checkout.reducers';
import { EventType, EventAction, EventCategory } from '@app/core/analytics/analytics.enums';
import { mockedCheckout } from '@app/features/cart/stores/checkout/checkout.reducers.spec';

describe('Checkout Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };
    spyOn(window['dataLayer'], 'push');
  });

  const mockConfirmation = {
    accountInfo: { accountId: 2082245, accountName: 'ELC Lighting BV', accountNumber: 1062685, region: 'arrowce', orgId: 276 },
    cart: {
      company: 'ELC Lighting BV',
      billToId: 2131422,
      currency: 'EUR',
      accountNumber: null,
      terms: null,
      id: '5b2b8db3c383160029b84191',
      userId: 'testce1a@testl4.com',
      status: 'IN_PROGRESS',
      createdDate: '2018-06-21T11:36:19.016',
      updatedDate: '2018-06-21T11:36:19.016',
      shipping: null,
      purchaseOrderNumber: null,
      comments: null,
      bookOrderFlag: null,
      orderHeaderId: null,
      lineItems: [
        {
          id: '5b2b8e89c383160029b84192',
          total: 1.61095,
          ncnrAccepted: null,
          ncnrAcceptedBy: null,
          manufacturerPartNumber: 'BAV99',
          valid: null,
          lastValidationDate: '2018-06-21T11:42:36.028',
          itemType: null,
          warehouseCode: null,
          selectedEndCustomerSiteName: null,
          manufacturer: 'ON Semiconductor|ONSEMI',
          description: 'Diode Small Signal Switching 70V 0.2A 3-Pin SOT-23 T/R',
          image: 'http://download.siliconexpert.com/pdfs/2014/4/23/8/11/53/632/fsc_/manual/sot-23-3_1.jpg',
          inStock: false,
          quotable: null,
          arrowReel: false,
          ncnr: null,
          businessCost: null,
          minimumOrderQuantity: 1,
          multipleOrderQuantity: 1,
          availableQuantity: 0,
          bufferQuantity: 0,
          leadTime: '8',
          price: 0.02929,
          validation: 'VALID',
          itemId: 43394,
          warehouseId: 3489,
          docId: '3489_00043394',
          selectedCustomerPartNumber: null,
          enteredCustomerPartNumber: null,
          selectedEndCustomerSiteId: null,
          requestDate: '2018-06-21',
          quantity: 55,
          reel: null,
        },
      ],
      subTotal: 1.61095,
      itemCount: 1,
    },
    shippingAddress: {
      primaryFlag: true,
      name: 'ELC lighting BV',
      address1: '.',
      address2: 'Weerijs 8',
      address3: null,
      address4: null,
      city: 'Gemert',
      county: null,
      state: null,
      country: 'Netherlands',
      postCode: '5422 WV',
      id: 2131938,
    },
    orderDetails: {
      shippingMethod: 'Standard',
      shipComplete: false,
      poNumber: 'xyz',
      salesRepReview: false,
      bookOrder: true,
      checkoutWithCreditCard: false,
    },
    userInfo: { firstName: 'George', lastName: 'Michael' },
    paymentType: 'ONACCOUNT',
    contact: {
      customerServiceEmail: 'JSmall@arroweurope.com',
      customerServiceNumber: '+49(0) 6102 5030-8808',
      salesRepName: 'Abdisalam, Hamdia',
      salesRepEmail: 'habdisalam@arroweurope.com',
      salesRepPhoneNumber: '123',
    },
    billingAddress: {
      name: 'ELC lighting BV',
      accountId: 2082245,
      accountNumber: 1062685,
      ebsPersonId: 7732679,
      billToId: 2131422,
      orgId: 276,
      address1: '.',
      address2: 'Weerijs 8',
      address3: '',
      address4: '',
      city: 'Gemert',
      state: '',
      province: '',
      country: 'NL',
      countryDescription: 'Netherlands',
      postalCode: '5422 WV',
      selected: null,
      id: 2131422,
    },
    orderId: 13,
  };

  it(`should push correct props to DataLayer on GET_CHECKOUT_DATA_SUCCESS action`, () => {
    const action = new GetCheckoutDataSuccess(mockedCheckout.data);
    checkoutAnalyticsMetaReducers(INITIAL_CHECKOUT_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.ECOMMERCE,
      eventCategory: EventCategory.ECOMMERCE,
      eventAction: EventAction.VIEW_CHECKOUT,
      ecommerce: {
        checkout: {
          actionField: { step: 2 },
          products: [
            { name: 'BAV99WT1G', id: 7323176, price: 0.01156, brand: 'ON Semiconductor|ONSEMI', quantity: 3000 },
            { name: 'LM95071CIMFX/NOPB', id: 7283092, price: 0.75593, brand: 'Texas Instruments|TI', quantity: 3000 },
          ],
        },
      },
    });
  });

  it(`should push correct props to DataLayer on PLACE_ORDER_SUCCESS action`, () => {
    const action = new PlaceOrderSuccess(mockConfirmation);
    checkoutAnalyticsMetaReducers(INITIAL_CHECKOUT_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.ECOMMERCE,
      eventCategory: EventCategory.ECOMMERCE,
      eventAction: EventAction.PLACE_ORDER,
      eventLabel: 13,
      ecommerce: {
        currencyCode: 'EUR',
        purchase: {
          actionField: { id: 13, revenue: 1.61095 },
          products: [{ name: 'BAV99', id: 43394, price: 0.02929, brand: 'ON Semiconductor|ONSEMI', quantity: 55 }],
        },
        tariffValue: 0,
        quantityTariffItems: 0,
        paymentMethod: 'ONACCOUNT',
        shippingLocation: 'Netherlands',
      },
    });

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.ECOMMERCE,
      eventCategory: EventCategory.ECOMMERCE,
      eventAction: EventAction.VIEW_CHECKOUT_CONFIRMATION,
      eventLabel: 13,
      ecommerce: {
        orderLineCount: 1,
        shippingMethod: 'Standard',
        checkout: {
          actionField: { step: 3, option: 'Standard' },
          products: [{ name: 'BAV99', id: 43394, price: 0.02929, brand: 'ON Semiconductor|ONSEMI', quantity: 55 }],
        },
      },
    });
  });

  it(`should push prop 65 props to DataLayer on PLACE_ORDER_SUCCESS action of an order shipping to California`, () => {
    mockConfirmation.shippingAddress.country = 'United States';
    mockConfirmation.shippingAddress.state = 'CA';
    const action = new PlaceOrderSuccess(mockConfirmation);
    checkoutAnalyticsMetaReducers(INITIAL_CHECKOUT_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.ECOMMERCE,
      eventCategory: EventCategory.ECOMMERCE,
      eventAction: EventAction.PLACE_ORDER,
      eventLabel: 13,
      ecommerce: {
        currencyCode: 'EUR',
        purchase: {
          actionField: { id: 13, revenue: 1.61095 },
          products: [{ name: 'BAV99', id: 43394, price: 0.02929, brand: 'ON Semiconductor|ONSEMI', quantity: 55 }],
        },
        tariffValue: 0,
        quantityTariffItems: 0,
        paymentMethod: 'ONACCOUNT',
        shippingLocation: 'CA, United States',
      },
    });
  });

  it('it should push NCNR Modal Activation to DataLayer on ACTIVATE_NCNR_MODAL action when NCNR modal pops up', () => {
    const mockUrl = '/cart/checkout';
    const action = new ActivateNcnrModal(mockUrl);
    checkoutAnalyticsMetaReducers(INITIAL_CHECKOUT_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventAction: EventAction.ACTIVATE_NCNR_MODAL,
      eventCategory: EventCategory.MODAL,
      eventLabel: mockUrl,
    });
  });
});
