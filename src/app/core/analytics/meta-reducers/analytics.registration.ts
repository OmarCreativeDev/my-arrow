import { EventsMap } from 'redux-beacon';
import { trackEvent, getAnalyticsMetaReducers } from '@app/core/analytics/analytics.utils';
import { EventCategory, EventType, EventAction } from '@app/core/analytics/analytics.enums';
import {
  RegistrationActionTypes,
  RegistrationSubmitFailed,
} from '@app/features/public/components/registration-form/store/registration.actions';
import { registrationReducers } from '@app/features/public/components/registration-form/store/registration.reducers';
import { LabelValue } from '@app/core/analytics/analytics.enums';
import { HttpErrorResponse } from '@angular/common/http';

const parseErrorToLabel = (action: RegistrationSubmitFailed): string => {
  try {
    const errorResponse: HttpErrorResponse = <HttpErrorResponse>action.payload;
    const label: string =
      errorResponse.error && errorResponse.error.debugMessage ? errorResponse.error.debugMessage : errorResponse.message;
    return label;
  } catch (error) {
    return <string>error;
  }
};

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  [RegistrationActionTypes.REGISTRATION_SUBMIT_SUCCESS]: () => ({
    ...trackEvent({
      action: EventAction.SUCCESS,
      category: EventCategory.REGISTER,
      event: EventType.EVENT,
      label: LabelValue.NOT_SET,
    }),
  }),
  [RegistrationActionTypes.REGISTRATION_SUBMIT_FAILED]: (action: RegistrationSubmitFailed) => ({
    ...trackEvent({
      action: EventAction.FAIL,
      category: EventCategory.REGISTER,
      event: EventType.EVENT,
      label: parseErrorToLabel(action),
    }),
  }),
  [RegistrationActionTypes.REGISTRATION_SUBMIT]: () => ({
    ...trackEvent({
      action: EventAction.REGISTRATION_REQUEST,
      category: EventCategory.REGISTER,
      event: EventType.EVENT,
      label: LabelValue.NOT_SET,
    }),
  }),
};

/**
 * Export meta-reducer
 */
export function registrationAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, registrationReducers)(state, action);
}
