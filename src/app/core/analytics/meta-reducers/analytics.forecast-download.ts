import { EventsMap } from 'redux-beacon';
import { trackEvent, getAnalyticsMetaReducers } from '@app/core/analytics/analytics.utils';
import { EventAction, EventType, EventCategory } from '@app/core/analytics/analytics.enums';
import {
  ForecastDownloadActionTypes,
  OpenedForecastDownloadModal,
  SelectedForecastDownloadFileColumns,
  SelectedForecastDownloadFileDateRange,
  SelectedForecastDownloadFileType,
} from '@app/features/forecast/stores/forecast-download/forecast-download.actions';
import { forecastDownloadReducers } from '@app/features/forecast/stores/forecast-download/forecast-download.reducers';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  [ForecastDownloadActionTypes.OPENED_FORECAST_DOWNLOAD_MODAL]: (action: OpenedForecastDownloadModal) => ({
    ...trackEvent({
      event: EventType.EVENT,
      category: EventCategory.MODAL,
      action: EventAction.FORECAST_DOWNLOAD_MODAL,
      label: action.payload.url,
    })
  }),
  [ForecastDownloadActionTypes.SELECTED_FORECAST_DOWNLOAD_FILE_TYPE]: (action: SelectedForecastDownloadFileType) => ({
    ...trackEvent({
      event: EventType.EVENT,
      category: EventCategory.FORECAST,
      action: EventAction.MODAL_DOWNLOAD_TYPE,
      label: action.payload.fileType,
    })
  }),
  [ForecastDownloadActionTypes.SELECTED_FORECAST_DOWNLOAD_FILE_COLUMNS]: (action: SelectedForecastDownloadFileColumns) => ({
    ...trackEvent({
      event: EventType.EVENT,
      category: EventCategory.FORECAST,
      action: EventAction.DOWNLOAD_COLUMNS,
      label: action.payload.columns.length === 1 ? action.payload.columns[0] : action.payload.columns.sort().join(', '),
    })
  }),
  [ForecastDownloadActionTypes.SELECTED_FORECAST_DOWNLOAD_FILE_DATE_RANGE]: (action: SelectedForecastDownloadFileDateRange) => ({
    ...trackEvent({
      event: EventType.EVENT,
      category: EventCategory.FORECAST,
      action: EventAction.DOWNLOAD_DATE_RANGE,
      label: action.payload.weeksInHorizon,
    })
  }),
};

/**
 * Export meta-reducer
 */
export function forecastDownloadAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, forecastDownloadReducers) (state, action)
}
