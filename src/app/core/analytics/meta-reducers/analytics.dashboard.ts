import { EventsMap } from 'redux-beacon';

import { trackEvent, getAnalyticsMetaReducers } from '@app/core/analytics/analytics.utils';
import { EventCategory, EventAction } from '@app/core/analytics/analytics.enums';

import { DashboardActionTypes, POUploadSuccess, POUploadFailed } from '@app/features/dashboard/stores/dashboard.actions';
import { dashboardReducers } from '@app/features/dashboard/stores/dashboard.reducers';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  /**
   * Trigger a 'PO upload success' action
   */
  [DashboardActionTypes.PO_UPLOAD_SUCCESS]: (action: POUploadSuccess) => {
    return {
      ...trackEvent({
        category: EventCategory.PURCHASE_ORDER,
        action: EventAction.UPLOAD_SUCCESS,
        label: action.payload,
      }),
    };
  },
  [DashboardActionTypes.PO_UPLOAD_FAILED]: (action: POUploadFailed) => ({
    ...trackEvent({
      category: EventCategory.PURCHASE_ORDER,
      action: EventAction.UPLOAD_FAILED,
      label: action.payload.error.error,
    }),
  }),
};

/**
 * Export meta-reducer
 */
export function dashboardAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, dashboardReducers)(state, action);
}
