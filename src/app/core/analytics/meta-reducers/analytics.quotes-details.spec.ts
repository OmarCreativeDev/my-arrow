import { EventAction, EventCategory, EventType } from '@app/core/analytics/analytics.enums';
import { quotesDetailsAnalyticsMetaReducers } from './analytics.quotes-details';
import { Query, RequestProductsLoggedOnAnalytics } from '@app/features/submitted-quotes/stores/quote-details/quote-details.actions';
import { INITIAL_QUOTE_STATE } from '@app/features/submitted-quotes/stores/quotes/quotes.reducers';

describe('Quotes Details Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };
    spyOn(window['dataLayer'], 'push');
  });

  const mockQuery = { pagination: { page: 1, limit: 10 }, searchText: 'Microchip', sorting: { order: '', field: '' }, id: 2649759 };
  const mockProductsToBuyInfo = { quantity: 1, totalPrice: 1 };

  it(`should push correct props to DataLayer on QUERY action`, () => {
    const action = new Query(mockQuery);

    quotesDetailsAnalyticsMetaReducers(INITIAL_QUOTE_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.SEARCH,
      eventAction: EventAction.QUOTE_DETAIL_SEARCH,
      eventLabel: 'microchip',
    });
  });

  it(`should return null if searchText has no content on QUERY action`, () => {
    mockQuery.searchText = '';
    const action = new Query(mockQuery);

    quotesDetailsAnalyticsMetaReducers(INITIAL_QUOTE_STATE, action);

    expect(action.payload.searchText.trim().toLowerCase().length).toEqual(0);
  });

  it(`should push correct props to DataLayer on RequestProductsLoggedOnAnalytics action`, () => {
    const action = new RequestProductsLoggedOnAnalytics(mockProductsToBuyInfo);

    quotesDetailsAnalyticsMetaReducers(INITIAL_QUOTE_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.QUOTE,
      eventAction: EventAction.QUOTED_PRODUCTS_ADDED,
      eventLabel: mockProductsToBuyInfo.quantity,
      eventValue: mockProductsToBuyInfo.totalPrice,
      metricType: 'set',
      metricName: 'metric1',
      metricValue: mockProductsToBuyInfo.quantity,
    });
  });
});
