import { EventsMap } from 'redux-beacon';
import {
  CartActionTypes,
  AddToCartSuccess,
  CartItemsDeletionSuccess,
  AddToQuoteSuccess,
  GetCartDataSuccess,
  ShoppingCartLimit,
} from '@app/features/cart/stores/cart/cart.actions';

import { cartReducers } from '@app/features/cart/stores/cart/cart.reducers';
import { trackEvent, getAnalyticsMetaReducers, getListType } from '@app/core/analytics/analytics.utils';
import { EventCategory, EventType, EventAction } from '@app/core/analytics/analytics.enums';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  /**
   * GA Enhanced Ecommerce 'add'
   */
  [CartActionTypes.ADD_TO_CART_SUCCESS]: (action: AddToCartSuccess) => ({
    ...trackEvent({
      event: EventType.ECOMMERCE,
      category: EventCategory.ECOMMERCE,
      action: EventAction.ADD_TO_CART,
      label: action.payload.request.lineItems.map(product => product.manufacturerPartNumber).join(', '),
    }),
    ecommerce: {
      currencyCode: action.payload.request.currency,
      add: {
        actionField: { list: getListType(action.payload.path) },
        products: action.payload.request.lineItems.map(product => ({
          id: product.itemId,
          name: product.manufacturerPartNumber,
          quantity: product.quantity,
        })),
      },
    },
  }),
  /**
   * GA Enhanced Ecommerce 'remove'
   */
  [CartActionTypes.ITEMS_DELETION_SUCCESS]: (action: CartItemsDeletionSuccess) => ({
    ...trackEvent({
      event: EventType.ECOMMERCE,
      category: EventCategory.ECOMMERCE,
      action: EventAction.REMOVE_FROM_CART,
      label: action.payload.map(product => product.name).join(', '),
    }),
    ecommerce: {
      remove: {
        products: action.payload,
      },
    },
  }),
  /**
   * GA Enhanced Ecommerce 'add to quote'
   */
  [CartActionTypes.ADD_TO_QUOTE_SUCCESS]: (action: AddToQuoteSuccess) => ({
    ...trackEvent({
      action: EventAction.ADD_TO_QUOTE,
      category: EventCategory.QUOTE,
      label: action.payload.lineItems
        .sort()
        .map(product => product.manufacturerPartNumber)
        .join(', '),
      value: action.payload.lineItems.sort().reduce((sum, product) => {
        return sum + product.quantity;
      }, 0),
    }),
  }),
  /**
   * Enhanced E-commerce event 'checkout' Step 1 (Cart)
   */
  [CartActionTypes.GET_CART_DATA_SUCCESS]: (action: GetCartDataSuccess) => ({
    ...trackEvent({
      event: EventType.ECOMMERCE,
      category: EventCategory.ECOMMERCE,
      action: EventAction.VIEW_CART,
      label: action.payload.id,
    }),
    ecommerce: {
      checkout: {
        actionField: { step: 1 },
        products: action.payload.lineItems.map(product => ({
          name: product.manufacturerPartNumber,
          id: product.itemId,
          price: product.price,
          brand: product.manufacturer,
          quantity: product.quantity,
        })),
      },
    },
  }),

  [CartActionTypes.SHOPPING_CART_LIMIT]: (action: ShoppingCartLimit) => ({
    ...trackEvent({
      category: EventCategory.SHOPPING_CART,
      action: EventAction.SHOPPING_CART_LIMIT,
      label: action.payload,
    }),
  }),
};

/**
 * Export meta-reducer
 */
export function cartAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, cartReducers)(state, action);
}
