import { EventCategory } from '@app/core/analytics/analytics.enums';
import { getAnalyticsMetaReducers, trackEvent } from '@app/core/analytics/analytics.utils';
import { CatalogueActionTypes, SelectCategory } from '@app/features/products/stores/catalogue/catalogue.actions';
import { EventsMap } from 'redux-beacon';
import { catalogueReducers } from '@app/features/products/stores/catalogue/catalogue.reducers';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  [CatalogueActionTypes.SELECT_CATEGORY]: (action: SelectCategory) => {
    return {
      ...trackEvent({
        category: EventCategory.CATALOGUE,
        action: action.payload.name,
        label: action.payload.level,
      }),
    };
  },
};

/**
 * Export meta-reducer
 */
export function catalogueAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, catalogueReducers)(state, action);
}
