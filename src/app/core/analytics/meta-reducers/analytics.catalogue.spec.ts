import { SelectCategory } from '@app/features/products/stores/catalogue/catalogue.actions';
import { INITIAL_CATALOGUE_STATE } from '@app/features/products/stores/catalogue/catalogue.reducers';
import { catalogueAnalyticsMetaReducers } from './analytics.catalogue';
import { EventCategory, EventType } from '../analytics.enums';
import { ISelectedCategory } from '@app/core/inventory/product-category.interface';

describe('Catalogue Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };
    spyOn(window['dataLayer'], 'push');
  });

  it('should push correct props to DataLayer on SELECT_CATEGORY action', () => {
    const mockData: ISelectedCategory = { name: 'ELECTRONICS', level: 2 };
    const action = new SelectCategory(mockData);
    catalogueAnalyticsMetaReducers(INITIAL_CATALOGUE_STATE, action);
    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.CATALOGUE,
      eventAction: mockData.name,
      eventLabel: mockData.level,
    });
  });
});
