import { EventsMap } from 'redux-beacon';

import { trackEvent, getAnalyticsMetaReducers } from '@app/core/analytics/analytics.utils';
import { EventAction, EventCategory } from '@app/core/analytics/analytics.enums';
import {
  GenerateCertificateActionTypes,
  GenerateCertificate,
} from '@app/features/trade-compliance/components/generate-certificate/store/generate-certificate.actions';
import { generateCertificateReducers } from '@app/features/trade-compliance/components/generate-certificate/store/generate-certificate.reducers';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  /**
   * Trade Compliance Certificate queries
   */
  [GenerateCertificateActionTypes.GENERATE_CERTIFICATE]: (action: GenerateCertificate) => ({
    ...trackEvent({
      category: EventCategory.COMPLIANCE,
      action: EventAction.CERTIFICATION_REQUESTED,
      label: action.payload.certificate.parts,
    }),
  }),
};

/**
 * Export meta-reducer
 */
export function tradeComplianceCertificateAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, generateCertificateReducers)(state, action);
}
