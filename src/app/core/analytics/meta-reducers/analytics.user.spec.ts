import {
  RequestUserComplete,
  UserLoggedInComplete,
  UpdateCurrencyCodeComplete,
  ResetUser,
  UserLoggedInFailed,
  PasswordResetSuccess,
  UserLoggedOut,
  TokenRefresh,
  WebSocketConnect,
  WebSocketReconnect,
  WebSocketDisconnect,
} from '@app/core/user/store/user.actions';
import { mockUser } from '@app/core/user/user.service.spec';
import { userAnalyticsMetaReducers, UserErrorType } from '@app/core/analytics/meta-reducers/analytics.user';
import { INITIAL_USER_STATE } from '@app/core/user/store/user.reducers';
import { EventCategory, EventType, EventAction } from '@app/core/analytics/analytics.enums';
import { ClientId } from '@app/core/user/user.interface';
import { HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ReasonsForLogout } from './analytics.user.enum';

const mockHttpError: HttpErrorResponse = {
  headers: new HttpHeaders({}),
  type: null,
  status: 404,
  statusText: 'Not Found',
  url: 'https://dev-myarrow-api.arrow.com/users/profile/login',
  ok: false,
  name: 'HttpErrorResponse',
  message: 'Http failure response for https://dev-myarrow-api.arrow.com/users/profile/login: 404 Not Found',
  error: {
    timestamp: '2018-11-23T16:48:21.965',
    status: '3001',
    error: 'User Not Found',
    exception: 'com.arrow.myarrow.service.user.exception.UserServiceException',
    messages: ['User Not Found'],
    path: '/users/profile/login',
  },
};

describe('User Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };
    spyOn(window['dataLayer'], 'push');
  });

  it(`should push correct props to DataLayer window.dataLayer on USER_LOGGED_IN_COMPLETE action`, () => {
    const initialState = { profile: undefined, redirectUrl: 'dashboard' };
    const action = new UserLoggedInComplete(mockUser);
    userAnalyticsMetaReducers(initialState, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.LOGIN,
      eventAction: EventAction.SUCCESS,
      eventLabel: 254554,
      user: {
        id: 254554,
        loggedinstate: 1,
        company: 'ONXORE MANUFACTURING SERVICES, LTD',
        region: 'arrowna',
        geo: 'Americas',
        language: 'en',
        currency: 'USD',
        clientId: ClientId.MYARROWUI,
      },
      ecommerce: {
        currencyCode: 'USD',
      },
    });
  });

  describe('should push correct props to window.dataLayer when the error is a HttpErrorResponse', () => {
    const testCases = [
      {
        status: '3001',
        message: UserErrorType.USER_ERROR_3001,
      },
      {
        status: '3002',
        message: UserErrorType.USER_ERROR_3002_L2,
      },
      {
        status: '3002',
        message: UserErrorType.USER_ERROR_3002_L3,
      },
      {
        status: '3002',
        message: '- EXAMPLE NOT LISTED 3002 EVENT -',
      },
      {
        status: '3003',
        message: UserErrorType.USER_ERROR_3003_ACC,
      },
      {
        status: '3003',
        message: UserErrorType.USER_ERROR_3003_DB,
      },
      {
        status: '3003',
        message: '- EXAMPLE NOT LISTED 3003 EVENT -',
      },
      {
        status: '3004',
        message: UserErrorType.USER_ERROR_3004,
      },
      {
        status: '3005',
        message: UserErrorType.USER_ERROR_3005,
      },
      {
        status: '3009',
        message: UserErrorType.USER_ERROR_3009,
      },
      {
        status: '3012',
        message: UserErrorType.USER_ERROR_3012,
      },
      {
        status: '3000',
        message: '- EXAMPLE ERROR NOT LISTED IN ENUM -',
      },
    ];

    for (const testCase of testCases) {
      it(`for error ${testCase.status} should dispatch a dataLayer label: '${testCase.message}'`, () => {
        const initialState = { profile: undefined, redirectUrl: 'dashboard' };
        const builtError = { ...mockHttpError, error: { ...mockHttpError.error, status: testCase.status, messages: [testCase.message] } };
        const action = new UserLoggedInFailed(builtError);
        userAnalyticsMetaReducers(initialState, action);

        expect(window['dataLayer'].push).toHaveBeenCalledWith({
          event: EventType.EVENT,
          eventCategory: EventCategory.LOGIN,
          eventAction: EventAction.FAIL,
          eventLabel: testCase.message,
        });
      });
    }
  });

  it(`should push correct props to  window.dataLayer on USER_LOGGED_IN_FAILED action and error is a String`, () => {
    const initialState = { profile: undefined, redirectUrl: 'dashboard' };
    const action = new UserLoggedInFailed('User Not Found');
    userAnalyticsMetaReducers(initialState, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.LOGIN,
      eventAction: EventAction.FAIL,
      eventLabel: 'User Not Found',
    });
  });

  it(`should push user object to window.dataLayer on REQUEST_USER_COMPLETE action`, () => {
    const action = new RequestUserComplete(mockUser);
    userAnalyticsMetaReducers(INITIAL_USER_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      user: {
        id: 254554,
        loggedinstate: 1,
        company: 'ONXORE MANUFACTURING SERVICES, LTD',
        region: 'arrowna',
        geo: 'Americas',
        language: 'en',
        currency: 'USD',
        clientId: ClientId.MYARROWUI,
      },
      ecommerce: {
        currencyCode: 'USD',
      },
    });
  });

  it(`should push user object to window.dataLayer on RESET action`, () => {
    const action = new ResetUser();
    userAnalyticsMetaReducers(INITIAL_USER_STATE, action);
    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      user: {
        id: -1,
        loggedinstate: 0,
        company: undefined,
        region: undefined,
        geo: undefined,
        language: undefined,
        currency: undefined,
        clientId: undefined,
      },
      ecommerce: undefined,
    });
  });

  it(`should push correct props to DataLayer window.dataLayer on USER_UPDATE_CURRENCY_CODE_COMPLETE action`, () => {
    const action = new UpdateCurrencyCodeComplete('USD');
    userAnalyticsMetaReducers(INITIAL_USER_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.CURRENCY,
      eventAction: EventAction.SELECT,
      eventLabel: 'USD',
      user: {
        currency: 'USD',
      },
      ecommerce: {
        currencyCode: 'USD',
      },
    });
  });

  it(`should push correct props to DataLayer on USER_PASSWORD_RESET action`, () => {
    const action = new PasswordResetSuccess('Reset Completed');
    userAnalyticsMetaReducers(INITIAL_USER_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.LOGIN,
      eventAction: EventAction.FORGOT_PASSWORD,
      eventLabel: 'Reset Completed',
    });
  });

  it(`should push correct props to DataLayer on USER_LOGGED_OUT action`, () => {
    const action = new UserLoggedOut(ReasonsForLogout.USER_INACTIVITY);
    userAnalyticsMetaReducers(INITIAL_USER_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.SESSION_EVENT,
      eventCategory: EventCategory.LOGOUT,
      eventAction: EventAction.AUTOLOGOUT,
      eventLabel: ReasonsForLogout.USER_INACTIVITY,
    });
  });

  it(`should push correct props to DataLayer on TOKEN_REFRESH action`, () => {
    const action = new TokenRefresh();
    userAnalyticsMetaReducers(INITIAL_USER_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.SESSION_EVENT,
      eventCategory: EventCategory.SESSION_EVENTS,
      eventAction: EventAction.TOKEN_REFRESH,
    });
  });

  it(`should push correct props to DataLayer on WEB_SOCKET_CONNECT action`, () => {
    const action = new WebSocketConnect();
    userAnalyticsMetaReducers(INITIAL_USER_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.SESSION_EVENT,
      eventCategory: EventCategory.SESSION_EVENTS,
      eventAction: EventAction.WEB_SOCKET_CONNECT,
    });
  });

  it(`should push correct props to DataLayer on WEB_SOCKET_RECONNECT action`, () => {
    const action = new WebSocketReconnect();
    userAnalyticsMetaReducers(INITIAL_USER_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.SESSION_EVENT,
      eventCategory: EventCategory.SESSION_EVENTS,
      eventAction: EventAction.WEB_SOCKET_RECONNECT,
    });
  });

  it(`should push correct props to DataLayer on WEB_SOCKET_DISCONNECT action`, () => {
    const action = new WebSocketDisconnect();
    userAnalyticsMetaReducers(INITIAL_USER_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.SESSION_EVENT,
      eventCategory: EventCategory.SESSION_EVENTS,
      eventAction: EventAction.WEB_SOCKET_DISCONNECT,
    });
  });
});
