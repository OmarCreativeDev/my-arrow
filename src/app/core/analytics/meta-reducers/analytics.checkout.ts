import { EventsMap } from 'redux-beacon';

import { trackEvent, getAnalyticsMetaReducers } from '@app/core/analytics/analytics.utils';
import { EventType, EventAction, EventCategory } from '@app/core/analytics/analytics.enums';
import {
  CheckoutActionTypes,
  PlaceOrderSuccess,
  GetCheckoutDataSuccess,
  ActivateNcnrModal,
} from '@app/features/cart/stores/checkout/checkout.actions';
import { checkoutReducers } from '@app/features/cart/stores/checkout/checkout.reducers';
import { ICheckoutCartItem } from '@app/core/checkout/checkout.interfaces';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  /**
   * Enhanced E-commerce event 'checkout' Step 2 (Checkout)
   */
  [CheckoutActionTypes.GET_CHECKOUT_DATA_SUCCESS]: (action: GetCheckoutDataSuccess) => ({
    ...trackEvent({ event: EventType.ECOMMERCE, category: EventCategory.ECOMMERCE, action: EventAction.VIEW_CHECKOUT }),
    ecommerce: {
      checkout: {
        actionField: { step: 2 },
        products: action.payload.cart.lineItems.map(product => ({
          name: product.manufacturerPartNumber,
          id: product.itemId,
          price: product.price,
          brand: product.manufacturer,
          quantity: product.quantity,
        })),
      },
    },
  }),
  /**
   * Triggers an E-Commerce actions
   * 'purchase' action
   * 'checkout_option'
   * 'checkout' Step 3 (Confirmation)
   */
  [CheckoutActionTypes.PLACE_ORDER_SUCCESS]: (action: PlaceOrderSuccess) => {
    const { cart } = action.payload;
    const { country, state } = action.payload.shippingAddress;

    return [
      {
        ...trackEvent({
          event: EventType.ECOMMERCE,
          category: EventCategory.ECOMMERCE,
          action: EventAction.PLACE_ORDER,
          label: action.payload.orderId,
        }),
        ecommerce: {
          currencyCode: cart.currency,
          purchase: {
            actionField: {
              id: action.payload.orderId,
              revenue: cart.subTotal,
            },
            products: cart.lineItems.map(product => ({
              name: product.manufacturerPartNumber,
              id: product.itemId,
              price: product.price,
              brand: product.manufacturer,
              quantity: product.quantity,
            })),
          },
          tariffValue: cart.tariffTotal || 0,
          quantityTariffItems: cart.lineItems.filter((lineItem: ICheckoutCartItem) => lineItem.tariffValue && lineItem.tariffValue > 0)
            .length,
          paymentMethod: action.payload.paymentType,
          shippingLocation: state ? `${state}, ${country}` : country,
        },
      },
      {
        ...trackEvent({
          event: EventType.ECOMMERCE,
          category: EventCategory.ECOMMERCE,
          action: EventAction.VIEW_CHECKOUT_CONFIRMATION,
          label: action.payload.orderId,
        }),
        ecommerce: {
          orderLineCount: action.payload.cart.lineItems.length,
          shippingMethod: action.payload.orderDetails.shippingMethod,
          checkout: {
            actionField: { step: 3, option: action.payload.orderDetails.shippingMethod },
            products: action.payload.cart.lineItems.map(product => ({
              name: product.manufacturerPartNumber,
              id: product.itemId,
              price: product.price,
              brand: product.manufacturer,
              quantity: product.quantity,
            })),
          },
        },
      },
    ];
  },

  /**
   * Modal event 'NCNCR Modal Pops Up'
   */
  [CheckoutActionTypes.ACTIVATE_NCNR_MODAL]: (action: ActivateNcnrModal) => ({
    ...trackEvent({
      event: EventType.EVENT,
      action: EventAction.ACTIVATE_NCNR_MODAL,
      category: EventCategory.MODAL,
      label: action.payload,
    }),
  }),
};

/**
 * Export meta-reducer
 */
export function checkoutAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, checkoutReducers)(state, action);
}
