import { EventAction, EventCategory } from '@app/core/analytics/analytics.enums';
import { getAnalyticsMetaReducers, trackEvent } from '@app/core/analytics/analytics.utils';

import { EventsMap } from 'redux-beacon';
import { bomReducers } from '@app/core/boms/store/boms.reducers';
import { BomActionTypes, AddPartsToBom, OpenAddToBomModal } from '@app/core/boms/store/boms.actions';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  /**
   * When user Opens BOM modal
   */
  [BomActionTypes.OPEN_ADD_TO_BOM_MODAL]: (action: OpenAddToBomModal) => {
    return trackEvent({
      category: EventCategory.MODAL,
      action: EventAction.BOM_MODAL,
      label: action.payload.url,
    });
  },

  /**
   * When user adds parts to a BOM
   */
  [BomActionTypes.ADD_PARTS_TO_BOM]: (action: AddPartsToBom) => {
    return trackEvent({
      category: EventCategory.BOM,
      action: action.payload.isNew ? EventAction.ADD_TO_NEW_BOM : EventAction.ADD_TO_EXISTING_BOM,
      label: action.payload.parts
        .map(part => {
          return part.manufacturerPartNumber;
        })
        .join(', '),
    });
  },
};

/**
 * Export meta-reducer
 */
export function bomAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, bomReducers)(state, action);
}
