import { EventsMap } from 'redux-beacon';

import { trackEvent, getAnalyticsMetaReducers } from '@app/core/analytics/analytics.utils';
import { EventAction, EventCategory, EventType } from '@app/core/analytics/analytics.enums';
import { reelReducers } from '@app/core/reel/stores/reel.reducers';
import { ReelActionTypes, OpenReelModal } from '@app/core/reel/stores/reel.actions';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  /**
   * Reel Open Modal
   */
  [ReelActionTypes.REEL_OPEN_MODAL]: (action: OpenReelModal) => ({
    ...trackEvent({
      event: EventType.EVENT,
      category: EventCategory.MODAL,
      action: EventAction.ARROW_REEL_MODAL,
      label: action.payload.url,
    }),
  }),
};

/**
 * Export meta-reducer
 */
export function reelMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, reelReducers)(state, action);
}
