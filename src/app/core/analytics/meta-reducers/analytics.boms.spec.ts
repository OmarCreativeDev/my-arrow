import { EventType, EventAction, EventCategory } from '@app/core/analytics/analytics.enums';
import { bomAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.boms';
import { INITIAL_BOM_STATE } from '@app/core/boms/store/boms.reducers';
import { OpenAddToBomModal, AddPartsToBom } from '@app/core/boms/store/boms.actions';
import { IAddToBomPart } from '@app/core/boms/boms.interface';

describe('BOM Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };
    spyOn(window['dataLayer'], 'push');
  });

  const mockParts: Array<IAddToBomPart> = [
    {
      docId: '1234',
      itemId: 1234,
      warehouseId: 1234,
      manufacturerPartNumber: 'aaa',
      quantity: 100,
      manufacturer: 'Electronics Industry Public Company Limited',
    },
    {
      docId: '1234',
      itemId: 1234,
      warehouseId: 1234,
      manufacturerPartNumber: 'bbb',
      quantity: 200,
      manufacturer: 'Electronics Industry Public Company Limited',
    },
  ];

  it(`should push correct props to DataLayer on OpenAddToBomModal action`, () => {
    const action = new OpenAddToBomModal({ url: '/products/123', parts: mockParts });
    bomAnalyticsMetaReducers(INITIAL_BOM_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.MODAL,
      eventAction: EventAction.BOM_MODAL,
      eventLabel: '/products/123',
    });
  });

  it(`should push correct props to DataLayer on AddPartsToBom action with a new BOM`, () => {
    const action = new AddPartsToBom({
      id: 'aaa',
      name: 'Bom name',
      parts: mockParts,
      isNew: true,
    });
    bomAnalyticsMetaReducers(INITIAL_BOM_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.BOM,
      eventAction: EventAction.ADD_TO_NEW_BOM,
      eventLabel: 'aaa, bbb',
    });
  });

  it(`should push correct props to DataLayer on AddPartsToBom action with an existing BOM`, () => {
    const action = new AddPartsToBom({
      id: 'aaa',
      name: 'Bom name',
      parts: mockParts,
      isNew: false,
    });
    bomAnalyticsMetaReducers(INITIAL_BOM_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventCategory: EventCategory.BOM,
      eventAction: EventAction.ADD_TO_EXISTING_BOM,
      eventLabel: 'aaa, bbb',
    });
  });
});
