import { EventsMap } from 'redux-beacon';

import { trackEvent, getAnalyticsMetaReducers, trackMetric } from '@app/core/analytics/analytics.utils';
import { EventAction, EventCategory } from '@app/core/analytics/analytics.enums';

import {
  QuoteDetailsActionTypes,
  Query,
  RequestProductsLoggedOnAnalytics,
} from '@app/features/submitted-quotes/stores/quote-details/quote-details.actions';
import { quoteDetailsReducer } from '@app/features/submitted-quotes/stores/quote-details/quote-details.reducers';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  /**
   * Quotes Details queries
   */
  [QuoteDetailsActionTypes.QUERY]: (action: Query) => {
    return action.payload.searchText.trim().toLowerCase().length === 0
      ? null
      : {
          ...trackEvent({
            category: EventCategory.SEARCH,
            action: EventAction.QUOTE_DETAIL_SEARCH,
            label: action.payload.searchText.toLowerCase(),
          }),
        };
  },
  /**
   * Buy from Quotes
   */
  [QuoteDetailsActionTypes.REQUEST_PRODUCTS_LOGGED_ON_ANALYTICS]: (action: RequestProductsLoggedOnAnalytics) => ({
    ...trackEvent({
      category: EventCategory.QUOTE,
      action: EventAction.QUOTED_PRODUCTS_ADDED,
      label: action.payload.quantity,
      value: action.payload.totalPrice,
    }),
    ...trackMetric(action.payload.quantity),
  }),
};

/**
 * Export meta-reducer
 */
export function quotesDetailsAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, quoteDetailsReducer)(state, action);
}
