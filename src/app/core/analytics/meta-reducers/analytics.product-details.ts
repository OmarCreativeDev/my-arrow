import { EventsMap } from 'redux-beacon';

import { trackEvent, getAnalyticsMetaReducers } from '@app/core/analytics/analytics.utils';
import { EventType, EventAction, EventCategory } from '@app/core/analytics/analytics.enums';

import { ProductDetailsActionTypes, GetProductDetailsSuccess } from '@app/features/products/stores/product-details/product-details.actions';
import { productDetailsReducers } from '@app/features/products/stores/product-details/product-details.reducers';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  /**
   * Enhanced E-commerce event 'details'
   */
  [ProductDetailsActionTypes.GET_PRODUCT_DETAILS_SUCCESS]: (action: GetProductDetailsSuccess) => ({
    ...trackEvent({
      event: EventType.ECOMMERCE,
      category: EventCategory.ECOMMERCE,
      action: EventAction.VIEW_PRODUCT_DETAILS,
      label: action.payload.mpn,
    }),
    ecommerce: {
      detail: {
        products: [
          {
            name: action.payload.mpn,
            id: action.payload.itemId,
            brand: action.payload.manufacturer,
          },
        ],
      },
    },
  }),
};

/**
 * Export meta-reducer
 */
export function productDetailsAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, productDetailsReducers)(state, action);
}
