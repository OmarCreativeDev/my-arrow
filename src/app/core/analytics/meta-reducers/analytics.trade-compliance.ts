import { EventsMap } from 'redux-beacon';

import { trackEvent, getAnalyticsMetaReducers } from '@app/core/analytics/analytics.utils';
import { EventAction, EventCategory } from '@app/core/analytics/analytics.enums';
import {
  TradeComplianceActionTypes,
  TradeComplianceSearch,
  ComplianceCheckUpload,
} from '@app/features/trade-compliance/components/search-results/store/search-results.actions';
import { tradeComplianceSearchReducers } from '@app/features/trade-compliance/components/search-results/store/search-results.reducers';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  /**
   * Trade Compliance queries
   */
  [TradeComplianceActionTypes.TRADE_COMPLIANCE_SEARCH]: (action: TradeComplianceSearch) => ({
    ...trackEvent({
      category: EventCategory.SEARCH,
      action: EventAction.COMPLIANCE_SEARCH,
      label: action.payload,
    }),
  }),
  [TradeComplianceActionTypes.COMPLIANCE_CHECK_UPLOAD]: (action: ComplianceCheckUpload) => ({
    ...trackEvent({
      category: EventCategory.COMPLIANCE,
      action: EventAction.COMPLIANCE_CHECK_UPLOAD,
      label: action.payload,
    }),
  }),
};

/**
 * Export meta-reducer
 */
export function tradeComplianceAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, tradeComplianceSearchReducers)(state, action);
}
