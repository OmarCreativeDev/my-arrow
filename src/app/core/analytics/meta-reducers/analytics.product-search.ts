import { EventsMap } from 'redux-beacon';

import { trackEvent, getAnalyticsMetaReducers } from '@app/core/analytics/analytics.utils';
import { EventAction, EventType, ListType, EventCategory } from '@app/core/analytics/analytics.enums';

import { productSearchReducer } from '@app/features/products/stores/product-search/product-search.reducers';
import {
  ProductSearchActionTypes,
  SubmitProductSearchQuery,
  SubmitProductSearchQueryComplete,
  ProductClicked,
} from '@app/features/products/stores/product-search/product-search.actions';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  /**
   * Product search queries
   */
  [ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH_QUERY]: (action: SubmitProductSearchQuery) => ({
    ...trackEvent({
      category: EventCategory.SEARCH,
      action: EventAction.PRODUCT_SEARCH,
      label: action.payload.searchQuery,
    }),
  }),
  /**
   * Enhanced E-commerce 'impressions'
   */
  [ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH_QUERY_COMPLETE]: (action: SubmitProductSearchQueryComplete) => ({
    ...trackEvent({
      event: EventType.ECOMMERCE,
      category: EventCategory.ECOMMERCE,
      action: EventAction.VIEW_PRODUCT_SEARCH_RESULTS,
      label: action.payload.query,
    }),
    ecommerce: {
      impressions: action.payload.products.map((product, index) => ({
        name: product.mpn,
        id: product.itemId,
        brand: product.manufacturer,
        list: ListType.SEARCH_PAGE,
        position: index + 1,
      })),
    },
  }),
  /**
   * Enhanced E-commerce 'click'
   */
  [ProductSearchActionTypes.PRODUCT_CLICKED]: (action: ProductClicked) => ({
    ...trackEvent({
      event: EventType.ECOMMERCE,
      category: EventCategory.ECOMMERCE,
      action: EventAction.PRODUCT_CLICK,
      label: action.payload.product.mpn,
    }),
    ecommerce: {
      click: {
        actionField: { list: ListType.SEARCH_PAGE },
        products: [
          {
            name: action.payload.product.mpn,
            id: action.payload.product.itemId,
            brand: action.payload.product.manufacturer,
            position: action.payload.index + 1,
          },
        ],
      },
    },
  }),
};

/**
 * Export meta-reducer
 */
export function productSearchAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, productSearchReducer)(state, action);
}
