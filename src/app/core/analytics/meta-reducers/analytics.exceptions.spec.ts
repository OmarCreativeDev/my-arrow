import { EventType, EventAction, EventCategory } from '@app/core/analytics/analytics.enums';
import { UserLoggedIn } from '@app/core/user/store/user.actions';
import { TestBed } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { googleAnalyticsMetaReducers } from './analytics.exceptions';
import { UserService } from '@app/core/user/user.service';
import { EffectsModule } from '@ngrx/effects';
import { UserEffects } from '@app/core/user/store/user.effects';
import { AnalyticsEffects } from '../stores/analytics.effects';
import { AuthService } from '@app/core/auth/auth.service';
import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { ApiService } from '@app/core/api/api.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';
import { RouterTestingModule } from '@angular/router/testing';
import { CheckoutService } from '@app/core/checkout/checkout.service';
import { userAnalyticsMetaReducers } from './analytics.user';
import * as AnalyticsUtils from '../analytics.utils';
import { of } from 'rxjs';

class DummyComponent {}
const routes = [{ path: 'user-error', component: DummyComponent }];

describe('Google Analytics Exceptions', () => {
  let store: Store<IAppState>;
  let userService: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(routes),
        StoreModule.forRoot({ user: userAnalyticsMetaReducers, googleAnalytics: googleAnalyticsMetaReducers }),
        EffectsModule.forRoot([UserEffects, AnalyticsEffects]),
      ],
      providers: [
        AuthTokenService,
        AuthService,
        ApiService,
        CheckoutService,
        UserService,
        { provide: WSSService, useClass: MockStompWSSService },
      ],
    });

    store = TestBed.get(Store);
    userService = TestBed.get(UserService);
    window['dataLayer'] = { push: function(e) {} };
    spyOn(window['dataLayer'], 'push');
  });

  it(`should push correct props to DataLayer on Error trown`, () => {
    const mockError = {
      event: EventType.EXCEPTION,
      eventCategory: EventCategory.SYSTEM_EXCEPTION,
      eventAction: EventAction.ERROR_TRACK,
      eventLabel: 'THIS IS AN ERROR',
    };

    spyOn(userService, 'getUserAfterLogin').and.returnValue(of(new HttpErrorResponse({ error: { status: 404 } })));
    spyOn(AnalyticsUtils, 'trackException').and.returnValue(mockError);
    const action = new UserLoggedIn();

    store.dispatch(action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith(mockError);
  });
});
