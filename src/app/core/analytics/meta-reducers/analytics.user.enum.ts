export enum ReasonsForLogout {
  TOKEN_EXPIRED = 'Token expired',
  USER_INACTIVITY = 'User inactivity',
  WEB_SOCKET_INACTIVITY = 'Web sockets inactivity',
}
