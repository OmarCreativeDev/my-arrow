import { EventAction, EventCategory, EventType } from '@app/core/analytics/analytics.enums';
import { tradeComplianceCertificateAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.trade-compliance-certificate';
import { INITIAL_RESULTS_STATE } from '@app/features/trade-compliance/components/generate-certificate/store/generate-certificate.reducers';
import { GenerateCertificate } from '@app/features/trade-compliance/components/generate-certificate/store/generate-certificate.actions';

describe('Trade Compliance Certificate Analytics', () => {
  beforeEach(() => {
    window['dataLayer'] = { push: function() {} };

    spyOn(window['dataLayer'], 'push');
  });

  it(`should push correct props to DataLayer on GENERATE_CERTIFICATE action`, () => {
    const requestedTradeComplianceCertificate = ['10-527334-19R'];
    const mockCertificate = {
      certificate: {
        certificateNameType: 'nafta',
        parts: ['10-527334-19R'],
        to: 'david.lane@oncorems.com',
        userInformation: {
          email: 'david.lane@oncorems.com',
          companyName: '334',
          attentionOf: 'Test',
          address: 'Kissimmee',
          city: 'Orlando',
        },
        year: 2014,
      },
    };
    const action = new GenerateCertificate(mockCertificate);

    tradeComplianceCertificateAnalyticsMetaReducers(INITIAL_RESULTS_STATE, action);

    expect(window['dataLayer'].push).toHaveBeenCalledWith({
      event: EventType.EVENT,
      eventAction: EventAction.CERTIFICATION_REQUESTED,
      eventCategory: EventCategory.COMPLIANCE,
      eventLabel: requestedTradeComplianceCertificate,
    });
  });
});
