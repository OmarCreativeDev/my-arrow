import { getAnalyticsMetaReducers, trackException } from '@app/core/analytics/analytics.utils';
import { EventsMap } from 'redux-beacon';
import { AnalyticsActionTypes, SendAnalyticsException } from '../stores/analytics.actions';
import { googleAnalyticsReducers } from '../stores/analytics.reducer';
const eventsMap: EventsMap = {
  [AnalyticsActionTypes.SEND_EXCEPTION]: (action: SendAnalyticsException) => {
    return {
      ...trackException(action.payload),
    };
  },
};

export function googleAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, googleAnalyticsReducers)(state, action);
}
