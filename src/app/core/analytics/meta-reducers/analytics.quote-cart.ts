import { EventsMap } from 'redux-beacon';

import { trackEvent, getAnalyticsMetaReducers } from '@app/core/analytics/analytics.utils';
import { EventAction, EventCategory } from '@app/core/analytics/analytics.enums';
import { QuoteCartActionTypes, QuoteCartLimit, RequestQuote } from '@app/features/quotes/stores/quote-cart.actions';
import { quoteCartReducers } from '@app/features/quotes/stores/quote-cart.reducers';

/**
 * GTM dataLayer mappings
 */
const eventsMap: EventsMap = {
  /**
   * Quotes queries
   */
  [QuoteCartActionTypes.QUOTE_CART_LIMIT]: (action: QuoteCartLimit) => ({
    ...trackEvent({
      category: EventCategory.QUOTE,
      action: EventAction.QUOTE_CART_LIMIT,
      label: action.payload,
    }),
  }),

  [QuoteCartActionTypes.REQUEST_QUOTE]: (action: RequestQuote) => ({
    ...trackEvent({
      category: EventCategory.QUOTE,
      action: EventAction.QUOTE_REQUEST,
      label: action.payload,
    }),
  }),
};

/**
 * Export meta-reducer
 */
export function quoteCartAnalyticsMetaReducers(state, action) {
  return getAnalyticsMetaReducers(eventsMap, quoteCartReducers)(state, action);
}
