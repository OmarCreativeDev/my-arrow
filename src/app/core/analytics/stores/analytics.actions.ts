import { Action } from '@ngrx/store';
import { IEventProps } from '../analytics.interfaces';
export enum AnalyticsActionTypes {
  SEND_EXCEPTION = '[GA] SEND_EXCEPTION',
  SEND_EXCEPTION_COMPLETE = '[GA] SEND_EXCEPTION_COMPLETE',
  SEND_CUSTOM_EVENT = '[GA] SEND_CUSTOM_EVENT',
  SEND_CUSTOM_EVENT_COMPLETE = '[GA] SEND_CUSTOM_EVENT_COMPLETE',
}

export class SendAnalyticsException implements Action {
  readonly type = AnalyticsActionTypes.SEND_EXCEPTION;
  constructor(public payload: Error) {}
}

export class SendAnalyticsExceptionComplete implements Action {
  readonly type = AnalyticsActionTypes.SEND_EXCEPTION_COMPLETE;
  constructor() {}
}

export class SendAnalyticsCustomEvent implements Action {
  readonly type = AnalyticsActionTypes.SEND_CUSTOM_EVENT;
  constructor(public payload: IEventProps) {}
}

export class SendAnalyticsCustomEventComplete implements Action {
  readonly type = AnalyticsActionTypes.SEND_CUSTOM_EVENT_COMPLETE;
  constructor() {}
}

export type AnalyticsActions =
  | SendAnalyticsException
  | SendAnalyticsExceptionComplete
  | SendAnalyticsCustomEvent
  | SendAnalyticsCustomEventComplete;
