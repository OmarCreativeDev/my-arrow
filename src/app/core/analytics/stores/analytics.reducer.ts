import { AnalyticsActionTypes, AnalyticsActions } from './analytics.actions';
export interface IGaState {
  loading: boolean;
}

export const INITIAL_GA_STATE = {
  loading: false,
};

export function googleAnalyticsReducers(state = INITIAL_GA_STATE, action: AnalyticsActions): IGaState {
  switch (action.type) {
    case AnalyticsActionTypes.SEND_CUSTOM_EVENT:
    case AnalyticsActionTypes.SEND_EXCEPTION: {
      return {
        ...state,
        loading: true,
      };
    }

    case AnalyticsActionTypes.SEND_EXCEPTION_COMPLETE:
    case AnalyticsActionTypes.SEND_CUSTOM_EVENT_COMPLETE: {
      return {
        ...state,
        loading: false,
      };
    }
    default: {
      return state;
    }
  }
}
