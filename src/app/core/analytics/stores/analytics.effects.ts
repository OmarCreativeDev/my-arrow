import { Observable, of } from 'rxjs';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import {
  SendAnalyticsException,
  AnalyticsActionTypes,
  SendAnalyticsExceptionComplete,
  SendAnalyticsCustomEvent,
  SendAnalyticsCustomEventComplete,
} from './analytics.actions';
import { switchMap } from 'rxjs/operators';

@Injectable()
export class AnalyticsEffects {
  @Effect()
  sendAnalyticsException: Observable<any> = this.actions$.pipe(
    ofType<SendAnalyticsException>(AnalyticsActionTypes.SEND_EXCEPTION),
    switchMap(() => of(new SendAnalyticsExceptionComplete()))
  );

  @Effect()
  sendAnalyticsCustomEvent: Observable<any> = this.actions$.pipe(
    ofType<SendAnalyticsCustomEvent>(AnalyticsActionTypes.SEND_CUSTOM_EVENT),
    switchMap(() => of(new SendAnalyticsCustomEventComplete()))
  );

  constructor(private actions$: Actions) {}
}
