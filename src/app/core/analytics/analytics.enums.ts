/**
 * The type of event (GTM will use this for identifying triggers)
 * - EVENT: A GTM trigger that is recognised as a standard custom event
 * - ECOMMERCE: A GTM trigger that is recognised as a GA Enhanced Ecommerce and will require an 'ecommerce' dataLayer update
 */
export enum EventType {
  EVENT = 'EVENT',
  SESSION_EVENT = 'sessionEvent',
  ECOMMERCE = 'ECOMMERCE',
  EXCEPTION = 'EXCEPTION',
}

/**
 * The event category that will be sent to GA
 */
export enum EventCategory {
  LOGIN = 'Log In',
  LOGOUT = 'Log Out',
  CURRENCY = 'Currency',
  PURCHASE_ORDER = 'Purchase Order',
  QUOTE = 'Quote',
  DOWNLOADS = 'Downloads',
  CONTACT = 'Contact',
  ORDERS = 'Orders',
  ECOMMERCE = 'Ecommerce',
  SEARCH = 'Search',
  BOM = 'BOM',
  MODAL = 'Modal',
  REGISTER = 'Register',
  COMPLIANCE = 'Compliance',
  SITE_LANGUAGE = 'Site Language',
  CATALOGUE = 'Product Catalogue',
  FORECAST = 'Forecast',
  SESSION_EVENTS = 'Session Events',
  SYSTEM_EXCEPTION = 'System Exception',
  SHOPPING_CART = 'Shopping Cart',
}

/**
 * The event action that will be sent to GA
 */
export enum EventAction {
  SUCCESS = 'Success',
  FAIL = 'Fail',
  SELECT = 'Select',
  ERROR_TRACK = 'Error Track',
  UPLOAD_SUCCESS = 'Upload Success',
  UPLOAD_FAILED = 'Upload Failed',
  ADD_TO_QUOTE = 'Add To Quote',
  ADD_TO_CART = 'Add To Cart',
  REMOVE_FROM_CART = 'Remove From Cart',
  PRODUCT_CLICK = 'Product Click',
  PRODUCT_SEARCH = 'Product Search',
  ORDER_SEARCH = 'Order Search',
  ORDER_FILTER = 'Order Filter',
  VIEW_CART = 'View Cart',
  VIEW_QUOTES = 'View Quotes',
  VIEW_CHECKOUT = 'View Checkout',
  VIEW_CHECKOUT_CONFIRMATION = 'Checkout Confirmation',
  PLACE_ORDER = 'Place Order Success',
  VIEW_PRODUCT_DETAILS = 'View Product Details',
  VIEW_PRODUCT_SEARCH_RESULTS = 'View Product Search Results',
  EMAIL_SALES_REPRESENTATIVE = 'Email Sales Representative',
  PUSH_PULL_ORDER = 'Edit Request Date',
  QUOTE_SEARCH = 'Quote List Search',
  QUOTE_DOWNLOAD = 'Quote Download',
  QUOTE_DETAIL_SEARCH = 'Quote Detail Search',
  QUOTE_CART_LIMIT = 'Quote Cart Limit',
  SHOPPING_CART_LIMIT = 'Shopping Cart Limit',
  FORGOT_PASSWORD = 'Forgot Password',
  ACTIVATE_NCNR_MODAL = 'Activate NCNR Modal',
  COMPLIANCE_SEARCH = 'Compliance Search',
  ARROW_REEL_MODAL = 'Arrow Reel Modal',
  QUOTE_REQUEST = 'Quote Request',
  QUOTED_PRODUCTS_ADDED = 'Quoted Products Added',
  COMPLIANCE_CHECK_UPLOAD = 'Compliance Check Upload',
  CERTIFICATION_REQUESTED = 'Certification Requested',
  BOM_MODAL = 'BOM Modal',
  ADD_TO_NEW_BOM = 'Add to new BOM',
  ADD_TO_EXISTING_BOM = 'Add to existing BOM',
  DROP_DOWN_CLICK = 'Drop Down Click',
  FORECAST_DOWNLOAD_MODAL = 'Forecast Download Modal',
  MODAL_DOWNLOAD_TYPE = 'Modal Download Type',
  DOWNLOAD_COLUMNS = 'Download Columns',
  DOWNLOAD_DATE_RANGE = 'Download Date Range',
  REQUEST_RETURN = 'Request Return',
  AUTOLOGOUT = 'Auto Log Out',
  TOKEN_REFRESH = 'Token Refresh',
  WEB_SOCKET_CONNECT = 'Web Socket Connect',
  WEB_SOCKET_RECONNECT = 'Web Socket Reconnect',
  WEB_SOCKET_DISCONNECT = 'Web Socket Disconnect',
  REGISTRATION_REQUEST = 'Registration Request',
  RETURN_SUBMITTED = 'Return Submitted',
}

/**
 * The type of listing e.g search page / category page etc
 */
export enum ListType {
  SEARCH_PAGE = 'Product Search',
}

/**
 * Default value for eventLabel
 */
export enum LabelValue {
  NOT_SET = '(not set)',
}
