import { createMetaReducer } from 'redux-beacon';
import GoogleTagManager from '@redux-beacon/google-tag-manager';
import { IEventProps, ICustomMetrics } from '@app/core/analytics/analytics.interfaces';
import { EventType, ListType, EventCategory } from '@app/core/analytics/analytics.enums';

/**
 * Pushes an Event to the DataLayer
 * GTM will then read the category, action, label and value props on the DataLayer and pass event to GA
 */
export function trackEvent(props: IEventProps) {
  return {
    event: props.event || EventType.EVENT,
    eventAction: props.action,
    eventCategory: props.category,
    ...(props.label && { eventLabel: props.label }),
    ...(props.value && { eventValue: props.value }),
  };
}

export function trackException(error: Error) {
  return {
    event: EventType.EXCEPTION,
    eventCategory: EventCategory.SYSTEM_EXCEPTION,
    eventAction: error.message,
    ...(error.message && { eventLabel: error.stack }),
  };
}

export function trackMetric(value: any): ICustomMetrics {
  return {
    metricType: 'set',
    metricName: 'metric1',
    metricValue: value,
  };
}

/**
 * Returns the type of list from a given path or null if no match found
 * @param path
 */
export function getListType(path: string) {
  return path.includes('/products/search') ? ListType.SEARCH_PAGE : null;
}

/**
 * Wrapper method to return GTM meta-reducers
 * @param eventsMap
 * @param reducers
 */
/* istanbul ignore next */
export function getAnalyticsMetaReducers(eventsMap, reducers) {
  const gtmMetaReducer = createMetaReducer(eventsMap, GoogleTagManager());
  return gtmMetaReducer(reducers);
}
