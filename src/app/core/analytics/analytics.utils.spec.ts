import { ListType } from '@app/core/analytics/analytics.enums';
import { IEventProps } from '@app/core/analytics/analytics.interfaces';

import { getListType, trackEvent } from './analytics.utils';

describe('Analytics Utilities', () => {
  describe('#trackEvent()', () => {
    it('should return the default event parameters', () => {
      const props: IEventProps = {
        action: 'ACTION',
        category: 'SEARCH',
      };
      const event = trackEvent(props);
      expect(event).toEqual(
        jasmine.objectContaining({
          event: 'EVENT',
          eventAction: 'ACTION',
          eventCategory: 'SEARCH',
        })
      );
    });

    it('should return the custom event parameters', () => {
      const props: IEventProps = {
        event: 'CUSTOM_EVENT',
        category: 'CATEGORY',
        action: 'ACTION',
        label: 'LABEL',
        value: 'VALUE',
      };
      expect(trackEvent(props)).toEqual(
        jasmine.objectContaining({
          event: 'CUSTOM_EVENT',
          eventAction: 'ACTION',
          eventCategory: 'CATEGORY',
          eventLabel: 'LABEL',
          eventValue: 'VALUE',
        })
      );
    });
  });

  describe('#getListType()', () => {
    it('should return null if `path` is not from the search path', () => {
      expect(getListType('/not/search/path/')).toBeNull();
    });

    it('should return null if `path` is not from the search path', () => {
      expect(getListType('/products/search/path/match')).toEqual(ListType.SEARCH_PAGE);
    });
  });
});
