export interface IEventProps {
  event?: string;
  category: string;
  action: string;
  label?: string | number;
  value?: string | number;
}

export interface ICustomDimensions {
  user?: {
    id: number;
    loggedinstate: number;
    company: string;
    region: string;
    geo: string;
    language: string;
    currency: string;
    clientId: string;
  };
  ecommerce?: {
    currencyCode: string;
  };
}

export interface ICustomMetrics {
  metricType: string;
  metricName: string;
  metricValue: number;
}
