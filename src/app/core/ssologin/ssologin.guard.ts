import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { Observable, of } from 'rxjs';

@Injectable()
export class SSOLoginGuard implements CanActivate {
  constructor(private router: Router) {}

  public canActivate(route: ActivatedRouteSnapshot): Observable<boolean> {
    if (route.queryParams.cookieval && route.queryParams.accessid) {
      return of(true);
    } else {
      this.router.navigateByUrl('/login');
      return of(false);
    }
  }
}
