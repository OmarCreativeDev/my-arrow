import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { ApiService } from './api.service';

export class ApiServiceMock {
  public getPublic() {
    return of([]);
  }

  public get() {
    return of([]);
  }

  public put() {
    return of([]);
  }

  public post() {
    return of([]);
  }

  public delete() {
    return of([]);
  }

  public postBase64String() {
    return of([]);
  }

  public patch() {
    return of([]);
  }

  public postBlob() {
    return of([]);
  }
}

describe('ApiService', () => {
  let apiService: ApiService;
  let authTokenService: AuthTokenService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule],
      providers: [ApiService, AuthTokenService],
    });

    apiService = TestBed.get(ApiService);
    authTokenService = TestBed.get(AuthTokenService);
    http = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(apiService).toBeTruthy();
  });

  it('should send token in Authentication Header with a GET request', () => {
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue('tokenWithGet');
    apiService.get('/dummy-service').subscribe(() => {
      http.expectOne(req => {
        return req.url === '/dummy-service' && req.method === 'GET' && req.headers.get('Authorization') === 'Bearer tokenWithGet';
      });
    });
  });

  it('should send params with a GET request', () => {
    apiService.get('/dummy-service', { param1: 'paramValue1', param2: 'paramValue2' }).subscribe();
    http.expectOne(req => {
      return (
        req.url === '/dummy-service' &&
        req.method === 'GET' &&
        req.params.get('param1') === 'paramValue1' &&
        req.params.get('param2') === 'paramValue2'
      );
    });
  });

  it('getPublic() should `NOT` send token in Authentication Header with a GET request', () => {
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue('tokenWithGet');
    apiService.getPublic('/dummy-service').subscribe(() => {
      http.expectOne(req => {
        return req.url === '/dummy-service' && req.method === 'GET' && req.headers.has('Authorization') === false;
      });
    });
  });

  it('getPublic() should send params with a GET request', () => {
    apiService.getPublic('/dummy-service', { param1: 'paramValue1', param2: 'paramValue2' }).subscribe();
    http.expectOne(req => {
      return (
        req.url === '/dummy-service' &&
        req.method === 'GET' &&
        req.params.get('param1') === 'paramValue1' &&
        req.params.get('param2') === 'paramValue2'
      );
    });
  });

  it('should send JSON payload and token in Authentication Header with a POST request', () => {
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue('tokenWithPost');
    apiService.post('/dummy-service', { propKey: 'propValue' }).subscribe(() => {
      http.expectOne(req => {
        return (
          req.url === '/dummy-service' &&
          req.method === 'POST' &&
          req.body.propKey === 'propValue' &&
          req.headers.get('Authorization') === 'Bearer tokenWithPost'
        );
      });
    });
  });

  it('should send formData when formData flag set to TRUE with a POST request', () => {
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue('tokenWithPost');
    apiService.post('/dummy-service', { propKey: 'propValue' }, true).subscribe(() => {
      http.expectOne(req => {
        return (
          req.url === '/dummy-service' &&
          req.method === 'POST' &&
          req.body.get('propKey') === 'propValue' &&
          req.headers.get('Authorization') === 'Bearer tokenWithPost'
        );
      });
    });
  });

  it(`should return 'blob' responseType with the postBlob() method`, () => {
    apiService.postBlob('/dummy-service', {}).subscribe();
    expect(http.expectOne('/dummy-service').request.responseType).toEqual('blob');
  });

  it(`should return 'text' responseType with the postBase64String() method`, () => {
    apiService.postBase64String('/dummy-service', {}).subscribe();
    expect(http.expectOne('/dummy-service').request.responseType).toEqual('text');
  });

  it('should send token in Authentication Header and params in body with a PATCH request', () => {
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue('tokenWithPatch');
    const params = { propName: 'propValue' };
    apiService.patch('/dummy-service', params).subscribe(() => {
      http.expectOne(req => {
        return (
          req.url === '/dummy-service' &&
          req.method === 'PATCH' &&
          req.body.get('propName') === 'propValue' &&
          req.headers.get('Authorization') === 'Bearer tokenWithPatch'
        );
      });
    });
  });

  it('should send payload as JSON in body with a PATCH request when sendAsJSON is set to true', () => {
    const payload = { propName: 'propValue' };
    apiService.patch('/dummy-service', payload, true).subscribe();
    http.expectOne(req => {
      return req.url === '/dummy-service' && req.method === 'PATCH' && req.body === payload;
    });
  });

  it('should send token in Authentication Header and params with a PUT request', () => {
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue('tokenWithPut');
    const params = { propName: 'propValue' };
    apiService.put('/dummy-service', params).subscribe(() => {
      http.expectOne(req => {
        return (
          req.url === '/dummy-service' &&
          req.method === 'PUT' &&
          req.body.get('propName') === 'propValue' &&
          req.headers.get('Authorization') === 'Bearer tokenWithPut'
        );
      });
    });
  });

  it('should send payload as JSON in body with a PUT request when sendAsJSON is set to true', () => {
    const payload = { propName: 'propValue' };
    apiService.put('/dummy-service', payload, true).subscribe();
    http.expectOne(req => {
      return req.url === '/dummy-service' && req.method === 'PUT' && req.body === payload;
    });
  });

  it('should send token in Authentication Header with a DELETE request', () => {
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue('tokenWithDelete');
    apiService.delete('/dummy-service').subscribe(() => {
      http.expectOne(req => {
        return req.url === '/dummy-service' && req.method === 'DELETE' && req.headers.get('Authorization') === 'Bearer tokenWithDelete';
      });
    });
  });

  it('should not send any headers when access token is null', () => {
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue(null);
    apiService.get('/dummy-service').subscribe(() => {
      http.expectOne(req => {
        return req.url === '/dummy-service' && req.method === 'GET' && req.headers.get('Authorization') === null;
      });
    });
  });
});
