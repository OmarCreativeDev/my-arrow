import { IContact } from '@app/core/user/user.interface';
import { IPriceTier } from '@app/shared/components/price-tiers/price-tiers.interface';
import { QuoteCartErrorTypes, QuoteCartLineItemStatus } from '@app/core/quote-cart/quote-cart.enum';

export interface IFieldMap {
  fieldName: string;
  value: any;
  index: number;
}

export interface IQuoteCartLineItemCount {
  lineItemCount: number;
  maxLineItems: number;
  remainingLineItems: number;
}

export interface IQuoteCartLineItem {
  itemId: number;
  manufacturerPartNumber: string;
  selectedCustomerPartNumber: string;
  selectedEndCustomerSiteId: number;
  selectedEndCustomerName?: string;
  requestDate: string;
  quantity: number;
  id: string;
  total: number;
  priceTiers: Array<IPriceTier>;
  manufacturer: string;
  description: string;
  price?: number;
  checked?: boolean;
  changed?: boolean;
  targetPrice?: number;
  validation?: QuoteCartLineItemStatus;
  ncnrAccepted?: boolean;
  ncnrAcceptedBy?: boolean;
  image?: string;
  inStock?: boolean;
  quotable?: boolean;
  minimumOrderQuantity?: number;
  multipleOrderQuantity?: number;
  availableQuantity?: number;
  bufferQuantity?: number;
  leadTime?: string;
  datasheet?: string;
  multiple?: number;
  ncnr?: boolean;
  endCustomerRecords?: Array<IEndCustomerRecord>;
  enteredCustomerPartNumber?: string;
  tariffApplicable: boolean;
  tariffValue?: number;
}

export interface IQuoteCart {
  id: string;
  total: number;
  status: string;
  lineItems: Array<IQuoteCartLineItem>;
  metadata: object;
  additionalInfo?: string;
  valid?: boolean;
  quoteNumber?: string;
  quoteHeader?: string;
  submittedDate?: string;
}

export interface IQuoteCartInfo {
  id: string;
  userId: string;
  status: string;
}

export interface IQuoteCarts {
  userId: string;
  quoteCarts: Array<IQuoteCartInfo>;
}

export interface IQuoteCartState {
  lineItemCount: number;
  maxLineItems: number;
  remainingLineItems: number;
  submittedQuotesCount: number;
  quoteCart: IQuoteCart;
  carts: IQuoteCarts;
  loading: boolean;
  error?: object;
  errorType?: QuoteCartErrorTypes;
}

export interface ICheckQuoteCartLineItem {
  index: number;
  checked: boolean;
}

export interface IAddToQuoteCartRequestItem {
  docId: string;
  enteredCustomerPartNumber?: string;
  itemId: number;
  quantity: number;
  manufacturerPartNumber: string;
  requestDate?: string;
  selectedCustomerPartNumber?: string;
  selectedEndCustomerSiteId?: number;
  targetPrice?: number;
  warehouseId?: number;
}

export interface IAddToQuoteCartRequest {
  quoteCartId?: string;
  lineItems: Array<IAddToQuoteCartRequestItem>;
}

export interface IQuoteCartLineItemsDeleteRequest {
  quoteCartId: string;
  lineItemIds: Array<string>;
}

export interface IQuoteCartCompleteLineItemsDeletion {
  deleteRequestId: string;
  lineItemIds?: Array<string>;
  quoteCartId: string;
}

export interface IQuoteCartDeleteLineItemsResponse {
  id: string;
}

export interface IQuoteCartLineItemsUpdateRequest {
  id: string;
  lineItems: Array<IQuoteCartLineItem>;
}

export interface IQuoteCartSubmitRequest {
  accountId: number;
  accountNumber: number;
  additionalInfo: string;
  cartId: string;
  contact: IContact;
  currencyCode: string;
  email: string;
  firstName: string;
  lastName: string;
  orgId: number;
  phoneNumber: string;
  billToId: number;
  shipToId: number;
  region: string;
}

export interface IQuoteCartSubmitResponse {
  id: string;
  quoteNumber?: string;
  quoteHeader?: string;
  status: string;
  submittedDate: string;
  userId: string;
}

export interface IQuoteCartUpdateRequest {
  cartId: string;
  currency: string;
  billToId: number;
}

export interface IEndCustomer {
  endCustomerSiteId: number;
  name: string;
}

export interface IEndCustomerRecord {
  cpn: string;
  endCustomers?: Array<IEndCustomer>;
}

export interface ISubmittedQuotesCountResponse {
  count: number;
}

export interface IQuoteCartValidateLineItemsRequest {
  lineItemsIds: Array<string>;
  quoteCartId: string;
}
