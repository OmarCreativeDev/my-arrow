import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { takeWhile } from 'rxjs/operators';

import { getCartSelector } from '@app/features/quotes/stores/quote-cart.selectors';
import { IAppState } from '@app/shared/shared.interfaces';
import { IQuoteCart } from '@app/core/quote-cart/quote-cart.interfaces';

@Injectable()
export class QuoteCartGuard implements CanActivate {
  constructor(private store: Store<IAppState>, private router: Router) {}
  private isAlive = true;

  public getQuoteCartFromStore(): IQuoteCart {
    let cart$: Observable<IQuoteCart>;
    let cart: IQuoteCart;
    cart$ = this.store.pipe(select(getCartSelector));
    cart$.pipe(takeWhile(() => this.isAlive)).subscribe(quoteCart => (cart = quoteCart));

    return cart;
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const quoteCart = this.getQuoteCartFromStore();
    if (!quoteCart.quoteNumber || !quoteCart.quoteHeader) {
      this.router.navigateByUrl('/quotes');
      return of(false);
    } else {
      return of(true);
    }
  }
}
