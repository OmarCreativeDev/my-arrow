export enum QuoteCartLineItemStatus {
  INVALID = 'INVALID',
  PENDING = 'PENDING',
  VALID = 'VALID',
}

export enum QuoteCartErrorTypes {
  SUBMIT_ERROR = 'SUBMIT_ERROR',
}
