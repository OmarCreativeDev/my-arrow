import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';

import { ApiService } from '../api/api.service';

import {
  IQuoteCarts,
  IQuoteCart,
  IQuoteCartLineItemCount,
  IQuoteCartLineItem,
  IQuoteCartLineItemsUpdateRequest,
  IQuoteCartLineItemsDeleteRequest,
  IQuoteCartSubmitRequest,
  IQuoteCartSubmitResponse,
  IAddToQuoteCartRequest,
  IQuoteCartUpdateRequest,
  IQuoteCartValidateLineItemsRequest,
  IQuoteCartDeleteLineItemsResponse,
  IQuoteCartCompleteLineItemsDeletion,
} from './quote-cart.interfaces';

@Injectable()
export class QuoteCartService {
  constructor(private apiService: ApiService) {}

  public getQuoteCarts(): Observable<IQuoteCarts> {
    return this.apiService.get<IQuoteCarts>(`${environment.baseUrls.serviceQuoteCart}/carts`);
  }

  public getQuoteCartLineItemCount(quoteCartId: string): Observable<IQuoteCartLineItemCount> {
    return this.apiService.get<IQuoteCartLineItemCount>(`${environment.baseUrls.serviceQuoteCart}/carts/${quoteCartId}/lineItemCount`);
  }

  public getQuoteCart(quoteCartId: string): Observable<IQuoteCart> {
    return this.apiService.get<IQuoteCart>(`${environment.baseUrls.serviceQuoteCart}/carts/${quoteCartId}`);
  }

  public updateQuoteCart(updateRequest: IQuoteCartUpdateRequest) {
    const { cartId, billToId, currency } = updateRequest;
    return this.apiService.patch<void>(
      `${environment.baseUrls.serviceQuoteCart}/carts/${cartId}`,
      {
        billToId,
        currency,
      },
      true
    );
  }

  public addLineItems(request: IAddToQuoteCartRequest) {
    return this.apiService.post<void>(`${environment.baseUrls.serviceQuoteCart}/carts/${request.quoteCartId}/lineItems`, {
      lineItems: request.lineItems,
    });
  }

  /**
   * Requests the deletion of a list of line items IDs
   * @param deleteQuoteCartRequest
   */
  public requestQuoteCartLineItemsDeletion(
    deleteQuoteCartRequest: IQuoteCartLineItemsDeleteRequest
  ): Observable<IQuoteCartDeleteLineItemsResponse> {
    const { quoteCartId, lineItemIds } = deleteQuoteCartRequest;
    return this.apiService.post<IQuoteCartDeleteLineItemsResponse>(
      `${environment.baseUrls.serviceQuoteCart}/carts/${quoteCartId}/lineItems/deleteRequest`,
      lineItemIds
    );
  }

  public deleteQuoteCartLineItems(quoteCartIdAndRequestId: IQuoteCartCompleteLineItemsDeletion): Observable<IQuoteCartLineItem[]> {
    const { deleteRequestId, quoteCartId } = quoteCartIdAndRequestId;
    return this.apiService.delete<IQuoteCartLineItem[]>(
      `${environment.baseUrls.serviceQuoteCart}/carts/${quoteCartId}/lineItems/deleteRequest/${deleteRequestId}`
    );
  }

  public updateQuoteCartLineItems(updateRequest: IQuoteCartLineItemsUpdateRequest): Observable<IQuoteCartLineItem[]> {
    return this.apiService.put<IQuoteCartLineItem[]>(
      `${environment.baseUrls.serviceQuoteCart}/carts/${updateRequest.id}/lineItems`,
      updateRequest.lineItems,
      true
    );
  }

  public submitQuoteCart(quoteCartSubmit: IQuoteCartSubmitRequest): Observable<IQuoteCartSubmitResponse> {
    const {
      accountId,
      accountNumber,
      additionalInfo,
      cartId,
      contact,
      currencyCode,
      email,
      firstName,
      lastName,
      orgId,
      phoneNumber,
      billToId,
      shipToId,
      region,
    } = quoteCartSubmit;

    return this.apiService.post<IQuoteCartSubmitResponse>(`${environment.baseUrls.serviceQuoteCart}/quotes/${cartId}`, {
      accountId,
      accountNumber,
      additionalInfo,
      contact,
      currencyCode,
      email,
      firstName,
      lastName,
      orgId,
      phoneNumber,
      billToId,
      shipToId,
      region,
    });
  }

  public validateQuoteCartLineItems(request: IQuoteCartValidateLineItemsRequest): Observable<any> {
    return this.apiService.put<any>(
      `${environment.baseUrls.serviceQuoteCart}/carts/${request.quoteCartId}/lineitems/validate`,
      request.lineItemsIds,
      true
    );
  }
}
