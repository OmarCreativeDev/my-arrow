import { inject, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';

import { ApiService } from '@app/core/api/api.service';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { QuoteCartService } from './quote-cart.service';
import { CoreModule } from '@app/core/core.module';
import {
  IQuoteCart,
  IQuoteCarts,
  IQuoteCartSubmitResponse,
  IQuoteCartUpdateRequest,
  IQuoteCartValidateLineItemsRequest,
  IQuoteCartCompleteLineItemsDeletion,
} from './quote-cart.interfaces';

import mockedQuoteCartLineItems from '@app/features/quotes/pages/quotes/quotes-model-data-mock';

const deleteIds: IQuoteCartCompleteLineItemsDeletion = {
  deleteRequestId: '1234567890',
  lineItemIds: [mockedQuoteCartLineItems[0].id],
  quoteCartId: '23g14h32g4jh32g432',
};

const mockDeletedItems = mockedQuoteCartLineItems[0];
export class StoreMock {
  public select(): Observable<any> {
    return of({});
  }
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

describe('QuoteCartService', () => {
  let apiService: ApiService;
  let quoteCartService: QuoteCartService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule],
      providers: [QuoteCartService, { provide: ApiService, useClass: ApiServiceMock }, { provide: Store, useClass: StoreMock }],
    });
    apiService = TestBed.get(ApiService);
    quoteCartService = TestBed.get(QuoteCartService);
  });

  it('should be created', inject([QuoteCartService], (service: QuoteCartService) => {
    expect(service).toBeTruthy();
  }));

  it('getQuoteCartLineItemCount() should get line items count', inject([QuoteCartService], (service: QuoteCartService) => {
    const quoteCardId = '23g14h32g4jh32g432';
    const expected = { lineItemCount: 100, maxLineItems: 100, remainingLineItems: 0 };
    spyOn(apiService, 'get').and.returnValue(of(expected));
    service.getQuoteCartLineItemCount(quoteCardId).subscribe(result => {
      expect(result).toEqual(expected);
    });
  }));

  it('`getQuoteCarts()` should return `IQuoteCarts`', () => {
    const quoteCartsResponseMock: IQuoteCarts = {
      userId: 'usr-001',
      quoteCarts: [
        {
          id: '123',
          userId: 'usr-001',
          status: '',
        },
      ],
    };

    spyOn(apiService, 'get').and.returnValue(of(quoteCartsResponseMock));

    quoteCartService.getQuoteCarts().subscribe((response: IQuoteCarts) => {
      expect(response.userId).toBe(quoteCartsResponseMock.userId);
      expect(response.quoteCarts.length).toBe(1);
      expect(response.quoteCarts).toBe(quoteCartsResponseMock.quoteCarts);
    });
  });

  it('`getQuoteCart()` should return `Observable<IQuoteCart>`', () => {
    const quoteCardId = '23g14h32g4jh32g432';
    const quoteCartResponseMock: IQuoteCart = {
      id: '23g14h32g4jh32g432',
      total: undefined,
      status: undefined,
      lineItems: [],
      metadata: {},
      additionalInfo: undefined,
    };

    spyOn(apiService, 'get').and.returnValue(of(quoteCartResponseMock));

    quoteCartService.getQuoteCart(quoteCardId).subscribe((response: IQuoteCart) => {
      expect(response).toBe(quoteCartResponseMock);
    });
  });

  it('`deleteQuoteCartLineItems()` should delete the selected LineItems', () => {
    spyOn(apiService, 'delete').and.returnValue(of([mockDeletedItems]));

    quoteCartService.deleteQuoteCartLineItems(deleteIds).subscribe(quoteCartLineItems => {
      expect(quoteCartLineItems[0].id).toEqual(deleteIds.lineItemIds[0]);
    });
  });

  it('`submitQuoteCart()` should return `Observable<IQuoteCartSubmitResponse>`', () => {
    const quoteCartRequestMock = {
      accountId: 1305827,
      accountNumber: 1067767,
      additionalInfo: undefined,
      cartId: '5a8701382c1724bf4003132c',
      contact: {
        customerServiceEmail: 'ggustus@arrow.com',
        customerServiceNumber: '+1 877 237 8621',
        salesRepName: "O'Brien-Meek, Patrice",
        salesRepPhoneNumber: '+1 303-824-6459',
        salesRepEmail: 'patty.obrienmeek@arrow.com',
      },
      currencyCode: 'USD',
      email: 'david.lane@oncorems.com',
      firstName: 'David',
      lastName: 'Lane',
      orgId: 241,
      phoneNumber: undefined,
      billToId: 2174374,
      shipToId: 2175315,
      region: 'arrowna',
    };
    const quoteCartResponseMock: IQuoteCartSubmitResponse = {
      id: '5a8701382c1724bf4003132c',
      quoteNumber: '8b64aa73-c9db-4e59-ad09-e8396943677e',
      quoteHeader: '0b61b61e-aa01-48eb-a7fd-be2588fbfe3d',
      status: 'SUBMITTED',
      submittedDate: '2018-05-30',
      userId: '17xvi0kpizcnds4s7mtn3g01',
    };

    spyOn(apiService, 'post').and.returnValue(of(quoteCartResponseMock));

    quoteCartService.submitQuoteCart(quoteCartRequestMock).subscribe((response: IQuoteCartSubmitResponse) => {
      expect(response).toBe(quoteCartResponseMock);
    });
  });

  it('`updateQuoteCart()` should update quote cart', () => {
    const updateMock: IQuoteCartUpdateRequest = {
      cartId: '5a8701382c1724bf4003132c',
      billToId: 12345,
      currency: 'USD',
    };
    spyOn(apiService, 'patch').and.returnValue(of([]));
    quoteCartService.updateQuoteCart(updateMock).subscribe(response => {
      expect(response).toBeTruthy();
    });
  });

  it('`addLineItems()` should call `apiService.post()`', () => {
    spyOn(apiService, 'post');

    const quoteCartId = '23g14h32g4jh32g432';
    const lineItems = mockedQuoteCartLineItems.map(
      ({ enteredCustomerPartNumber, itemId, quantity, requestDate, selectedCustomerPartNumber, selectedEndCustomerSiteId }) => {
        const docId = '123';
        const intendedPrice = 100;
        const warehouseId = 123;
        const manufacturerPartNumber = 'BAV99';

        return {
          docId,
          enteredCustomerPartNumber,
          intendedPrice,
          itemId,
          quantity,
          requestDate,
          selectedCustomerPartNumber,
          selectedEndCustomerSiteId,
          warehouseId,
          manufacturerPartNumber,
        };
      }
    );

    quoteCartService.addLineItems({
      quoteCartId,
      lineItems,
    });

    expect(apiService.post).toHaveBeenCalled();
  });

  it('`updateQuoteCartLineItems()` should return `Observable<Array<IQuoteCartLineItem>>`', () => {
    spyOn(apiService, 'put').and.returnValue(of(mockedQuoteCartLineItems));

    const mockQuoteCartLineItemsUpdateRequest = {
      id: '123',
      lineItems: mockedQuoteCartLineItems,
    };

    quoteCartService.updateQuoteCartLineItems(mockQuoteCartLineItemsUpdateRequest).subscribe(quoteCartLineItems => {
      expect(quoteCartLineItems[0].id).toEqual(mockQuoteCartLineItemsUpdateRequest.lineItems[0].id);
    });
  });

  it('`validateQuoteCartLineItems()` should return `Observable<any>`', () => {
    spyOn(apiService, 'put').and.returnValue(of());

    const mockQuoteCartValidateLineItemsRequest: IQuoteCartValidateLineItemsRequest = {
      lineItemsIds: mockedQuoteCartLineItems.slice().map(lineItem => lineItem.id),
      quoteCartId: '123',
    };

    quoteCartService.validateQuoteCartLineItems(mockQuoteCartValidateLineItemsRequest).subscribe(response => {
      expect(response).toBeTruthy();
    });
  });
});
