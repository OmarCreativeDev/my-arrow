import { TestBed, inject } from '@angular/core/testing';

import { ApiService } from '@app/core/api/api.service';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { OrdersService } from './orders.service';
import { environment } from '@env/environment';
import { mockSearchRequest, mockRmaFormRequest, mockDownloadOrderRequest } from '@app/core/orders/orders.mock';
import { IViewOrderRequest } from '@app/core/orders/orders.interfaces';

describe('OrdersService', () => {
  let ordersService: OrdersService;
  let apiService: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OrdersService, { provide: ApiService, useClass: ApiServiceMock }],
    });

    apiService = TestBed.get(ApiService);
    ordersService = TestBed.get(OrdersService);
  });

  it('should be created', inject([OrdersService], (service: OrdersService) => {
    expect(service).toBeTruthy();
  }));

  it('should call API Service on `getAllOrderLines()`', () => {
    spyOn(apiService, 'get');
    ordersService.getAllOrderLines();
    expect(apiService.get).toHaveBeenCalledWith(`${environment.baseUrls.serviceOrderHistory}/order-lines`);
  });

  it('should call API Service on `orderLinesSearch()`', () => {
    spyOn(apiService, 'post');
    ordersService.orderLinesSearch(mockSearchRequest);
    expect(apiService.post).toHaveBeenCalledWith(`${environment.baseUrls.serviceOrderHistory}/order-lines/search`, mockSearchRequest);
  });

  it('should call API Service with an id on `getOrderForId()`', () => {
    const params: IViewOrderRequest = {
      orderId: 'PO-123',
      salesHeaderId: 1234,
      billToSiteUseId: 1234,
    };

    spyOn(apiService, 'get');
    ordersService.getOrderForId(params);
    expect(apiService.get).toHaveBeenCalledWith(`${environment.baseUrls.serviceOrderHistory}/orders/PO-123`, params);
  });

  it('should call submit for order request return ', () => {
    spyOn(apiService, 'post');
    ordersService.requestReturnOrder(mockRmaFormRequest);
    expect(apiService.post).toHaveBeenCalledWith(`${environment.baseUrls.serviceOrderHistory}/rma`, mockRmaFormRequest);
  });

  it('API Service on `downloadOrderLines()`', () => {
    spyOn(apiService, 'postBlob');
    ordersService.downloadOrderLines(mockDownloadOrderRequest);
    expect(apiService.postBlob).toHaveBeenCalledWith(
      `${environment.baseUrls.serviceOrderHistory}/order-lines/download`,
      mockDownloadOrderRequest
    );
  });
});
