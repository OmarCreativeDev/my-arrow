import { IFilterRule, IOrderLine, ISearchCriteria, ISortCriteron } from '@app/shared/shared.interfaces';

export interface IOrderLineQueryResponse {
  orderLines: Array<IOrderLine>;
  paging?: {
    currentPage: number;
    totalPages: number;
  };
}

export interface IOrderLineQueryRequest {
  search: ISearchCriteria;
  filter: Array<IFilterRule>;
  paging: {
    limit: number;
    page: number;
  };
  sort?: Array<ISortCriteron>;
}

export interface IPlaceOrderResponse {
  orderId: number;
}

export enum OrderServiceHttpExceptionStatusEnum {
  BAD_REQUEST = '4002',
  DATABASE = '4003',
  EXTERNAL_SERVICE = '4004',
}

export interface IRmaRequest {
  problemDescription: string;
  lineItems: Array<IRmaRequestLineItem>;
  isCreditRequired?: boolean;
  isRepairReplacement?: boolean;
  isFailureAnalysisRequired?: boolean;
  isCapaRequired: boolean;
  customerCapaReference?: string;
  customerQualityContactName?: string;
  contactPhone?: string;
  contactEmail: string;
  salesOrderNumber: string;
  customerPONumber: string;
  arrowSalesContactEmail: string;
  oracleAccountNumber: string;
  customerName: string;
  enteringBranch?: string;
  billToAccount: string;
  shipToAccount: string;
  invoiceNumber: string;
  isBranchTransferOrder: boolean;
  issueType: string;
  specialHandlingCode?: string;
  operatingUnit?: string;
  problemCategory: string;
  isReturningParts: boolean;
}

export interface IRmaRequestLineItem {
  quantityAffected?: number;
  quantityReceived?: number;
  partNumberReceived?: string;
  dateCodeSystem?: string;
  dateCodeUser: string;
  deliveryId?: string;
  lineItem: string;
  quantityOrdered: number;
  partNumberOrdered: string;
  customerPartNumber: string;
  carrier?: string;
  shipDate?: Date;
  trackingNumber?: string;
  warehouseCode?: string;
  stockNumber?: string;
  countryOfOrigin?: string;
  orderLineType?: string;
  manufacturerName: string;
  manufacturerPartNumber: string;
  attachments?: Array<IRmaAttachment>;
}

export enum RmaFileTypeEnum {
  JPG = '.JPG',
  PNG = '.PNG',
  GIF = '.GIF',
  DOC = '.DOC',
  DOCX = '.DOCX',
  XLS = '.XLS',
  XLSX = '.XLSX',
  PPT = '.PPT',
  PPTX = '.PPTX',
  PDF = '.PDF',
  RTF = '.RTF',
  TXT = '.TXT',
  MSG = '.MSG',
  MPEG = '.MPEG',
  MP4 = '.MP4',
  MOV = '.MOV',
  WMV = '.WMV',
}

export interface IRmaAttachment {
  id: string;
  name: string;
}

export interface IRmaRequestAttachment {
  orderId: string;
  lineItemId: string;
  type: string;
  value: string;
  size: number;
}

export interface IRmaLineItemFile {
  file: File;
  uploaded: boolean;
}

export interface IViewOrderRequest {
  orderId: string;
  salesHeaderId: number;
  billToSiteUseId: number;
}
