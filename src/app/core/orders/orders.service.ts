import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../api/api.service';
import { environment } from '@env/environment';

import {
  IOrderLineQueryResponse,
  IOrderLineQueryRequest,
  IPlaceOrderResponse,
  IRmaRequestAttachment,
  IRmaAttachment,
  IViewOrderRequest,
} from '@app/core/orders/orders.interfaces';
import { IOrder, IDownloadCriteria, IRmaSubmitRequest } from '@app/shared/shared.interfaces';
import { HttpResponse } from '@angular/common/http';

@Injectable()
export class OrdersService {
  constructor(private apiService: ApiService) {}

  /**
   * Creates a new order
   */
  public createOrder(data): Observable<IPlaceOrderResponse> {
    return this.apiService.post<IPlaceOrderResponse>(`${environment.baseUrls.serviceOrders}/`, data);
  }

  /**
   * Gets all lines for the user
   */
  public getAllOrderLines(): Observable<IOrderLineQueryResponse> {
    return this.apiService.get<IOrderLineQueryResponse>(`${environment.baseUrls.serviceOrderHistory}/order-lines`);
  }

  public orderLinesSearch(searchRequest: IOrderLineQueryRequest): Observable<IOrderLineQueryResponse> {
    return this.apiService.post<IOrderLineQueryResponse>(`${environment.baseUrls.serviceOrderHistory}/order-lines/search`, searchRequest);
  }

  /**
   * Retrieves the Order for a given ID
   * @param orderId number
   */
  public getOrderForId(params: IViewOrderRequest): Observable<IOrder> {
    return this.apiService.get<IOrder>(`${environment.baseUrls.serviceOrderHistory}/orders/${params.orderId}`, params);
  }

  public downloadOrderLines(downloadCriteria: IDownloadCriteria): Observable<HttpResponse<Blob>> {
    return this.apiService.postBlob(`${environment.baseUrls.serviceOrderHistory}/order-lines/download`, downloadCriteria);
  }

  /**
   * Submits a request to return an order
   */
  public requestReturnOrder(rmaSubmitRequest: IRmaSubmitRequest): Observable<void> {
    return this.apiService.post<void>(`${environment.baseUrls.serviceOrderHistory}/rma`, rmaSubmitRequest);
  }

  public loadRmaAttachmentOrderLines(loadAttachmentRequest: IRmaRequestAttachment): Observable<IRmaAttachment> {
    return this.apiService.post(`${environment.baseUrls.serviceOrderHistory}/rma/attachment`, loadAttachmentRequest);
  }

  public removeRmaAttachmentOrderLines(attachment: IRmaAttachment): Observable<any> {
    return this.apiService.delete(`${environment.baseUrls.serviceOrderHistory}/rma/attachment/${attachment.id}`);
  }

  public removeRmaAttachmentsByLineItemId(orderId: number, lineItemId: string): Observable<any> {
    return this.apiService.delete(`${environment.baseUrls.serviceOrderHistory}/rma/${orderId}/lineItem/${lineItemId}/attachments`);
  }

  public removeAttachmentsByOrderId(orderId: number): Observable<any> {
    return this.apiService.delete(`${environment.baseUrls.serviceOrderHistory}/rma/${orderId}/attachments`);
  }
}
