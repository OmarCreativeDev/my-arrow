import { NgForm } from '@angular/forms';
import { DialogData } from '@app/features/orders/components/order-returns-submit-dialog/order-returns-submit-dialog.component';

import { IOrderLineQueryRequest } from '@app/core/orders/orders.interfaces';
import {
  IOrder,
  IOrderDetails,
  IOrderLine,
  IRmaSubmitRequest,
  IRmaLineItemRequest,
  IOrderReturnFormHeader,
  IOrderReturnFormLineItem,
  IDownloadCriteria,
  IFilterCriteronOperatorEnum,
  IFileTypeEnum,
  ISortCriteronOrderEnum,
  IDownloadColumnTypeEnum,
  IRmaAttachmentRequest,
} from '@app/shared/shared.interfaces';

export const mockSearchRequest: IOrderLineQueryRequest = {
  search: {
    value: 'PO-123',
    type: '',
  },
  filter: [],
  paging: {
    limit: 25,
    page: 1,
  },
};

export const mockRmaFormRequest: IRmaSubmitRequest = {
  creditRequired: false,
  repairReplacement: true,
  failureAnalysisRequired: false,
  capaRequired: true,
  branchTransferOrder: false,
  returningParts: true,
  problemDescription: 'Damage',
  region: 'AC',
  customerCapaReference: 'REF-123',
  customerQualityContactName: '',
  contactPhone: '(123)456-7890',
  contactEmail: 'joe@mail.com',
  salesOrderNumber: '',
  customerPONumber: '',
  arrowSalesContactEmail: 'contact@arrow.com',
  oracleAccountNumber: '123456789',
  customerName: 'CustomerABC',
  enteringBranch: '',
  billToAccount: '1234',
  shipToAccount: '1234',
  issueType: 'Damage',
  operatingUnit: '',
  problemCategory: 'RMA',
  lineItems: [
    {
      quantityAffected: 5,
      quantityReceived: 10,
      quantityOrdered: 20,
      partNumberReceived: '',
      dateCodeUser: '',
      lineItem: '1.0',
      partNumberOrdered: '',
      customerPartNumber: '',
      warehouseCode: 'VA105',
      stockNumber: '',
      orderLineType: '',
      manufacturerName: 'MN-U123',
      manufacturerPartNumber: 'MN-U123-ABC',
      facilityCode: '',
      shipToAccount: '5432',
      specialHandlingCode: '',
      deliveries: [],
      attachments: [{ id: '', name: '' } as IRmaAttachmentRequest],
    } as IRmaLineItemRequest,
  ],
};

export const mockOrder = {
  details: {
    salesOrderId: 1234,
  } as IOrderDetails,
  orderLines: [
    {
      salesHeaderId: 456,
      itemId: 987,
      shipments: [],
    } as IOrderLine,
  ],
} as IOrder;

export const ngFormMock = {
  value: {
    header: {
      capa: true,
      capaNumber: 'CAPA-123',
      description: 'Some order description',
      partsYes: true,
    } as IOrderReturnFormHeader,
    'orderLine-987': {
      quantityAffected: 1,
      quantityReceived: 1,
      attachments: [],
    } as IOrderReturnFormLineItem,
  },
  invalid: true,
} as NgForm;

export const orderReturnsSubmitDialogData: DialogData = {
  submitFailed: false,
  orderId: 123,
  salesHeaderId: 456,
};

export const mockDownloadOrderRequest: IDownloadCriteria = {
  search: { value: '', type: 'purchaseOrderNumber' },
  filter: [
    { criteria: [{ key: 'status', value: 'OPEN', operator: IFilterCriteronOperatorEnum.Equals }] },
    {
      criteria: [
        { key: 'status', value: 'SHIPPED', operator: IFilterCriteronOperatorEnum.Equals },
        { key: 'shipmentTrackingDate', value: '2018-12-31T00:00:00-06:00', operator: IFilterCriteronOperatorEnum.LessThanOrEqualTo },
        { key: 'shipmentTrackingDate', value: '2018-12-24T00:00:00-06:00', operator: IFilterCriteronOperatorEnum.GreaterThanOrEqualTo },
      ],
    },
  ],
  sort: [{ key: 'purchaseOrderNumber', order: ISortCriteronOrderEnum.DESC }, { key: 'lineItem', order: ISortCriteronOrderEnum.DESC }],
  fileType: IFileTypeEnum.XLS,
  columns: [
    { id: 0, label: 'Customer P.O Number', properties: ['purchaseOrderNumber'], type: IDownloadColumnTypeEnum.String, format: '' },
    { id: 1, label: 'Buyer', properties: ['buyerName'], type: IDownloadColumnTypeEnum.String, format: '' },
    { id: 2, label: 'P.O. Status', properties: ['purchaseOrderStatus'], type: IDownloadColumnTypeEnum.String, format: '' },
    { id: 3, label: 'Customer Part Number', properties: ['customerPartNumber'], type: IDownloadColumnTypeEnum.String, format: '' },
    { id: 4, label: 'Manufacturer Part Number', properties: ['manufacturerPartNumber'], type: IDownloadColumnTypeEnum.String, format: '' },
    { id: 5, label: 'Manufacturer Name', properties: ['manufacturerName'], type: IDownloadColumnTypeEnum.String, format: '' },
    { id: 6, label: 'Date Entered', type: IDownloadColumnTypeEnum.Date, properties: ['entered'], format: 'YYYY-MM-DD' },
    { id: 7, label: 'Request Date', type: IDownloadColumnTypeEnum.Date, properties: ['requested'], format: 'YYYY-MM-DD' },
    { id: 8, label: 'Commit Date', type: IDownloadColumnTypeEnum.Date, properties: ['committed'], format: 'YYYY-MM-DD' },
    { id: 9, label: 'Quantity Ordered', type: IDownloadColumnTypeEnum.String, properties: ['qtyOrdered'], format: '' },
    { id: 10, label: 'Quantity Shipped', type: IDownloadColumnTypeEnum.String, properties: ['qtyShipped'], format: '' },
    { id: 11, label: 'Quantity Ready to Ship', type: IDownloadColumnTypeEnum.String, properties: ['qtyReadyToShip'], format: '' },
    { id: 12, label: 'Quantity Remaining', type: IDownloadColumnTypeEnum.String, properties: ['qtyRemaining'], format: '' },
    { id: 13, label: 'Item Status', type: IDownloadColumnTypeEnum.String, properties: ['status'], format: '' },
    { id: 14, label: 'Ship Date', type: IDownloadColumnTypeEnum.Date, properties: ['shipmentTrackingDate'], format: 'YYYY-MM-DD' },
    { id: 15, label: 'Tracking Number', type: IDownloadColumnTypeEnum.Url, value: 'shipmentTrackingReference', url: 'shipmentTrackingUrl' },
    { id: 16, label: 'Ship Method', type: IDownloadColumnTypeEnum.String, properties: ['carrier'], format: '' },
    {
      id: 17,
      label: 'Sales Order / Line Item',
      type: IDownloadColumnTypeEnum.String,
      properties: ['salesOrderId', 'lineItem'],
      format: '{0} / {1}',
    },
    { id: 18, label: 'Unit Resale', type: IDownloadColumnTypeEnum.String, properties: ['unitResale'], format: '' },
    { id: 19, label: 'Extended Resale', type: IDownloadColumnTypeEnum.String, properties: ['extResale'], format: '' },
  ],
  title: 'Order Summary',
};
