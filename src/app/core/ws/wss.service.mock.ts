// MockStompClient requires the following line in the Unit Test implementation in order to override Stomp Client
// Check wss.service.spec.ts
/*
@ts-ignore
Client = MockStompClient;
*/
import { WS_DEBUG, WS_HEARTBEAT_INCOMING, WS_HEARTBEAT_OUTGOING, WS_RECONNECT_DELAY } from '@env/environment';
import { Client } from '@stomp/stompjs';
import { WSSState } from '@app/core/ws/wss.interface';
import { Subject, of } from '../../../../node_modules/rxjs';

export class MockStompClient {
  brokerURL: '';
  stompVersions: '';
  connected: false;
  shouldDebug: false;
  activate = () => { };
  onConnect = () => {
    this.debug();
  };
  onDisconnect = () => { };
  onWebSocketClose = () => { };
  deactivate = () => { };
  debug = () => {
    if (this.shouldDebug) {
      console.log('Log');
    }
  };
}

const event: Event = null;
export const mockCloseEvent: CloseEvent = {
  ...event,
  wasClean: false,
  code: 2,
  reason: 'Forced Close',
  initEvent: function () { },
  initCloseEvent: function () { },
};

export class MockStompWSSService {
  public client: Client;
  public reconnectTries: number = 0;
  public state$ = new Subject<WSSState>();

  getClientConfig() {
    return {
      brokerURL: '/demo',
      debug: WS_DEBUG ? str => console.log(str) : () => { },
      reconnectDelay: WS_RECONNECT_DELAY,
      heartbeatIncoming: WS_HEARTBEAT_INCOMING,
      heartbeatOutgoing: WS_HEARTBEAT_OUTGOING,
    };
  }
  requestConnect() {
    this.state$.next(WSSState.CONNECTED);
  }
  handleClientStates() { }
  requestDisconnect() { }
  requestReconnect() { }
  emitBeforeConnect() { }
  emitOnConnect() { }
  emitOnDisconnect() { }
  isClientConnected = () => true;
  isClientInitialized = () => true;
  subscribe = () => of();
}
