export interface IWSConfig {
  headers?: Object;
  heartbeat_in?: number;
  heartbeat_out?: number;
  reconnect_delay?: number;
  debug?: boolean;
}

export enum WSSState {
  INITIAL = 'INITIAL',
  CONNECTED = 'CONNECTED',
  DISCONNECTED = 'DISCONNECTED',
}
