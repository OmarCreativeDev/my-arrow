import { TestBed } from '@angular/core/testing';
import { Client, StompConfig } from '@stomp/stompjs';

import { environment, WS_HEARTBEAT_INCOMING, WS_HEARTBEAT_OUTGOING, WS_RECONNECT_DELAY } from '@env/environment';

import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { WSSService } from '@app/core/ws/wss.service';
import { WSSState } from '@app/core/ws/wss.interface';
import { MockStompClient } from '@app/core/ws/wss.service.mock';
import { StoreModule } from '@ngrx/store';
import { userReducers } from '../user/store/user.reducers';
import { of } from 'rxjs';

describe('WSSService', () => {
  let authTokenService: AuthTokenService;
  let wssService: WSSService;
  let fakeAccessToken: string;
  let brokerURL: string;
  let securedBrokerURL: string;
  const tmpStompClient = Client;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          user: userReducers,
        }),
      ],
      providers: [AuthTokenService, WSSService],
    });

    authTokenService = TestBed.get(AuthTokenService);
    wssService = TestBed.get(WSSService);

    wssService.publicFeatureFlags$ = of({ webSocketSecurity: true });
    wssService.setPublicFeatureFlags({ webSocketSecurity: false });

    // @ts-ignore
    Client = MockStompClient;
    wssService.initStompClient();

    fakeAccessToken = 'asd123';
    brokerURL = `${environment.baseUrls.serviceEventNotifier}/ws/websocket`;
    securedBrokerURL = `${environment.baseUrls.serviceEventNotifier}/ws/websocket?access_token=${fakeAccessToken}`;
    spyOn(authTokenService, 'getAccessToken').and.returnValue(fakeAccessToken);
  });

  afterEach(() => {
    // @ts-ignore
    Client = tmpStompClient;
  });

  it('should be created', () => {
    expect(wssService).toBeTruthy();
  });

  it('getClientConfig should return a StompConfig object without web socket security', () => {
    const actual: StompConfig = wssService.getClientConfig();
    const expected: StompConfig = {
      brokerURL,
      debug: actual.debug,
      reconnectDelay: WS_RECONNECT_DELAY,
      heartbeatIncoming: WS_HEARTBEAT_INCOMING,
      heartbeatOutgoing: WS_HEARTBEAT_OUTGOING,
    };

    expect(actual).toEqual(expected);
  });

  it('getClientConfig should return a StompConfig object with web socket security', () => {
    wssService.setPublicFeatureFlags({ webSocketSecurity: true });

    const actual: StompConfig = wssService.getClientConfig();
    const expected: StompConfig = {
      brokerURL: securedBrokerURL,
      debug: actual.debug,
      reconnectDelay: WS_RECONNECT_DELAY,
      heartbeatIncoming: WS_HEARTBEAT_INCOMING,
      heartbeatOutgoing: WS_HEARTBEAT_OUTGOING,
    };

    expect(actual).toEqual(expected);
  });

  it("connect should set a new client when the client wasn't initialized", () => {
    spyOn(wssService, 'isClientConnected').and.returnValue(false);
    spyOn(wssService, 'handleClientStates').and.returnValue(() => { });

    wssService.requestConnect();

    expect(wssService.client).toBeTruthy();
  });

  it("connect should call handleClientStates when the client wasn't initialized", () => {
    spyOn(wssService, 'isClientConnected').and.returnValue(false);
    spyOn(wssService, 'handleClientStates').and.returnValue(() => { });

    wssService.requestConnect();

    expect(wssService.handleClientStates).toHaveBeenCalled();
  });

  it('connect should NOT set a new Client and call handleClientStates when the client was initialized', () => {
    spyOn(wssService, 'isClientConnected').and.returnValue(true);
    spyOn(wssService, 'handleClientStates').and.returnValue(() => { });

    wssService.requestConnect();

    expect(wssService.client).not.toBeNull();
    expect(wssService.handleClientStates).not.toHaveBeenCalled();
  });

  it('handleClientStates should set the onConnect, onDisconnect and onWebSocketClose handle methods', () => {
    wssService.client.onConnect = undefined;
    wssService.client.onDisconnect = undefined;

    wssService.handleClientStates();

    expect(wssService.client.onConnect).toBeDefined();
    expect(wssService.client.onDisconnect).toBeDefined();
  });

  it('onConnect should call emitOnConnect when client connects', () => {
    spyOn(wssService, 'emitOnConnect');
    wssService.handleClientStates();
    // @ts-ignore
    wssService.client.onConnect();

    expect(wssService.emitOnConnect).toHaveBeenCalled();
  });

  it('onDisconnect should call emitOnDisconnect when client disconnects', () => {
    spyOn(wssService, 'emitOnDisconnect');
    wssService.handleClientStates();
    // @ts-ignore
    wssService.client.onDisconnect();

    expect(wssService.emitOnDisconnect).toHaveBeenCalled();
  });

  it('disconnect should call client.deactivate when the client is connected', () => {
    spyOn(wssService, 'isClientInitialized').and.returnValue(true);
    spyOn(wssService.client, 'deactivate');

    wssService.requestDisconnect();

    expect(wssService.client.deactivate).toHaveBeenCalled();
  });

  it('disconnect should NOT call client.deactivate when the client is disconnected', () => {
    spyOn(wssService, 'isClientInitialized').and.returnValue(false);
    spyOn(wssService.client, 'deactivate');

    wssService.requestDisconnect();

    expect(wssService.client.deactivate).not.toHaveBeenCalled();
  });

  it('emitBeforeConnect should increase the reconnectRetries when the socket is NOT able to connect', () => {
    wssService.reconnectTries = 1;
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue(fakeAccessToken);
    spyOn(wssService.store, 'dispatch');

    wssService.emitBeforeConnect();

    expect(authTokenService.getValidAccessToken()).toBeDefined();
    expect(wssService.store.dispatch).not.toHaveBeenCalled();
    expect(wssService.reconnectTries).toEqual(2);
  });

  it('emitBeforeConnect should call dispatch because reconnectRetries is greater than WS_RECONNECT_TRIES when the socket is NOT able to connect', () => {
    wssService.reconnectTries = 3;
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue(fakeAccessToken);
    spyOn(wssService.store, 'dispatch');

    wssService.emitBeforeConnect();

    expect(authTokenService.getValidAccessToken()).toBeDefined();
    expect(wssService.store.dispatch).toHaveBeenCalled();
    expect(wssService.reconnectTries).toEqual(4);
  });

  it('emitBeforeConnect should call dispatch because Token is not longer valid when the socket is NOT able to connect, no matter if reconnectRetries is NOT greater than WS_RECONNECT_TRIES', () => {
    wssService.reconnectTries = 1;
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue(null);
    spyOn(wssService.store, 'dispatch');

    wssService.emitBeforeConnect();

    expect(authTokenService.getValidAccessToken()).toBeNull();
    expect(wssService.store.dispatch).toHaveBeenCalled();
    expect(wssService.reconnectTries).toEqual(2);
  });

  it('emitOnConnect should emit the WSSState.CONNECTED state', () => {
    spyOn(wssService.state$, 'next');

    wssService.emitOnConnect();

    expect(wssService.state$.next).toHaveBeenCalledWith(WSSState.CONNECTED);
  });

  it('emitOnConnect should reset to 0 the reconnectRetries on WSSState.CONNECTED state', () => {
    wssService.reconnectTries = 1;
    spyOn(wssService.state$, 'next');
    spyOn(wssService.store, 'dispatch');

    wssService.emitOnConnect();

    expect(wssService.state$.next).toHaveBeenCalledWith(WSSState.CONNECTED);
    expect(wssService.store.dispatch).toHaveBeenCalled();
    expect(wssService.reconnectTries).toEqual(0);
  });

  it('emitOnDisconnect should emit the WSSState.DISCONNECTED state', () => {
    spyOn(wssService.state$, 'next');
    spyOn(wssService.store, 'dispatch');
    wssService.emitOnDisconnect();

    expect(wssService.state$.next).toHaveBeenCalledWith(WSSState.DISCONNECTED);
  });

  it('isClientConnected should return TRUE when client was initialized and connected', () => {
    // @ts-ignore
    wssService.client = {
      ...wssService.client,
      connected: true,
    };

    expect(wssService.isClientConnected()).toEqual(true);
  });

  it("isClientConnected should NOT return TRUE when client wasn't initialized and connected", () => {
    // @ts-ignore
    wssService.client = {
      ...wssService.client,
      connected: false,
    };

    expect(wssService.isClientConnected()).toEqual(false);
  });

  it('isClientInitialized should return TRUE when client was initialized', () => {
    // @ts-ignore
    wssService.client = {
      ...wssService.client,
    };

    expect(wssService.isClientInitialized()).toEqual(true);
  });

  it("isClientInitialized should NOT return TRUE when client wasn't initialized", () => {
    wssService.client = undefined;

    expect(wssService.isClientInitialized()).toEqual(false);
  });

  it('requestReconnect should call requestDisconnect and requestConnect', () => {
    spyOn(wssService, 'requestDisconnect');
    spyOn(wssService, 'requestConnect');

    wssService.requestReconnect();

    expect(wssService.requestDisconnect).toHaveBeenCalled();
    expect(wssService.requestConnect).toHaveBeenCalled();
  });

  it('invalidateRetry should return FALSE when token is valid and reconnectRetries is NOT greater than WS_RECONNECT_TRIES', () => {
    wssService.reconnectTries = 1;
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue(fakeAccessToken);

    expect(wssService.invalidateRetry()).toEqual(false);
  });

  it('invalidateRetry should return TRUE when token is NOT valid', () => {
    wssService.reconnectTries = 1;
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue(null);

    expect(wssService.invalidateRetry()).toEqual(true);
  });

  it('invalidateRetry should return TRUE when reconnectRetries is greater than WS_RECONNECT_TRIES', () => {
    wssService.reconnectTries = 4;
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue(fakeAccessToken);

    expect(wssService.invalidateRetry()).toEqual(true);
  });

  it('invalidateRetry should return TRUE when when token is NOT valid OR reconnectRetries is greater than WS_RECONNECT_TRIES', () => {
    wssService.reconnectTries = 4;
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue(null);

    expect(wssService.invalidateRetry()).toEqual(true);
  });

});
