import './textEncoder.polyfill';
import { Injectable, Injector } from '@angular/core';
import { Client, StompConfig } from '@stomp/stompjs';

import {
  environment,
  WS_DEBUG,
  WS_HEARTBEAT_INCOMING,
  WS_HEARTBEAT_OUTGOING,
  WS_RECONNECT_DELAY,
  WS_RECONNECT_TRIES,
} from '@env/environment';
import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { WSSState } from '@app/core/ws/wss.interface';
import { Subject, Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { WebSocketConnect, WebSocketDisconnect } from '../user/store/user.actions';
import { UserLoggedOut } from '@app/core/user/store/user.actions';
import { ReasonsForLogout } from '@app/core/analytics/meta-reducers/analytics.user.enum';
import { getPublicFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';
import { isEmpty } from 'lodash-es';

@Injectable()
export class WSSService {
  public client: Client;
  public reconnectTries: number = 0;
  public state$ = new Subject<WSSState>();
  public publicFeatureFlags: object;
  public publicFeatureFlags$: Observable<object>;
  public subscription: Subscription;

  constructor(public store: Store<IAppState>, public injector: Injector) {
    this.publicFeatureFlags$ = this.store.pipe(select(getPublicFeatureFlagsSelector));
    this.state$.next(WSSState.INITIAL);
  }

  public setPublicFeatureFlags(flags: object): void {
    this.publicFeatureFlags = flags;
  }

  private buildBrokerURL(): string {
    const authTokenService = this.injector.get(AuthTokenService);
    const brokerURL: string = `${environment.baseUrls.serviceEventNotifier}/ws/websocket`;

    return this.publicFeatureFlags['webSocketSecurity']
      ? `${brokerURL}?access_token=${authTokenService.getAccessToken() || ''}`
      : brokerURL;
  }

  public getClientConfig(): StompConfig {
    return {
      brokerURL: this.buildBrokerURL(),
      debug: WS_DEBUG ? str => console.log(str) : () => { },
      reconnectDelay: WS_RECONNECT_DELAY,
      heartbeatIncoming: WS_HEARTBEAT_INCOMING,
      heartbeatOutgoing: WS_HEARTBEAT_OUTGOING,
    };
  }

  public initStompClient(): void {
    const clientConfig = this.getClientConfig();
    this.client = new Client(clientConfig);
  }

  public handleClientStates() {
    this.client.beforeConnect = () => this.emitBeforeConnect();
    this.client.onConnect = () => this.emitOnConnect();
    this.client.onDisconnect = () => this.emitOnDisconnect();
  }

  public emitBeforeConnect(): void {
    this.reconnectTries++;
    if (this.invalidateRetry()) {
      this.emitLogOutUser();
    }
  }

  public emitOnConnect(): void {
    this.reconnectTries = 0;
    this.store.dispatch(new WebSocketConnect());
    this.state$.next(WSSState.CONNECTED);
  }

  public emitOnDisconnect(): void {
    this.store.dispatch(new WebSocketDisconnect());
    this.state$.next(WSSState.DISCONNECTED);
  }

  public emitLogOutUser(): void {
    this.store.dispatch(new UserLoggedOut(ReasonsForLogout.WEB_SOCKET_INACTIVITY));
  }

  public requestConnect(): void {
    if (!this.isClientConnected()) {
      this.subscription = this.subscribeToProperties();
    }
  }

  public initConnection(): void {
    this.reconnectTries = 0;
    this.initStompClient();
    this.handleClientStates();
    this.client.activate();
  }

  public requestDisconnect(): void {
    if (this.isClientInitialized()) {
      this.client.deactivate();
    }
  }

  public requestReconnect(): void {
    this.requestDisconnect();
    this.requestConnect();
  }

  public isClientConnected(): boolean {
    return this.client && this.client.connected;
  }

  public isClientInitialized(): boolean {
    return this.client ? true : false;
  }

  public invalidateRetry(): boolean {
    const authTokenService = this.injector.get(AuthTokenService);
    return !authTokenService.getValidAccessToken() || this.reconnectTries > WS_RECONNECT_TRIES;
  }

  private subscribeToProperties(): Subscription {
    return this.publicFeatureFlags$.subscribe(flags => this.handlePropertiesSubscription(flags));
  }

  private handlePropertiesSubscription(flags: object): void {
    if (!isEmpty(flags)) {
      this.setPublicFeatureFlags(flags);
      this.initConnection();
      this.unsubscribeFromProperties();
    }
  }

  private unsubscribeFromProperties(): void {
    if (this.subscription && !this.subscription.closed) {
      this.subscription.unsubscribe();
    }
  }
}
