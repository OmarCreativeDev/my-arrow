import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';

import { ApiService } from '@app/core/api/api.service';
import { environment } from '@env/environment';
import { IForecast, IForecastDownload, IForecastSearchDetails } from '@app/core/forecast/forecast.interfaces';

@Injectable()
export class ForecastService {
  constructor(private apiService: ApiService) {}

  public getForecastDetails(accountNumber: number, billToNumber: number): Observable<IForecast> {
    const params = { accountNumber, billToNumber };

    return this.apiService.get<IForecast>(`${environment.baseUrls.serviceForecasts}`, params);
  }

  public getForecastSummary(accountNumber: number, billToNumber: number): Observable<IForecast> {
    const params = { accountNumber, billToNumber };

    return this.apiService.get<IForecast>(`${environment.baseUrls.serviceForecasts}/summary`, params);
  }

  public getForecastFile(preferences: IForecastDownload): Observable<HttpResponse<Blob>> {
    return this.apiService.postBlob(`${environment.baseUrls.serviceForecasts}/download`, preferences);
  }

  public getForecastSearchDetails({
    accountNumber,
    billToNumber,
    page = 1,
    size = 10,
    sortColumn = 'customerPartNumber',
    sortDirection = 'ASC',
    searchText = null,
    searchBy = 'any',
    shortagesOnly = false,
    shortageDateRange = 0,
  }: IForecastSearchDetails): Observable<IForecast> {
    const params = {
      accountNumber,
      billToNumber,
      page,
      size,
      sortColumn,
      sortDirection,
      searchText,
      searchBy,
      shortagesOnly,
      shortageDateRange,
    };
    if (!searchText) delete params.searchText;

    return this.apiService.get<IForecast>(`${environment.baseUrls.serviceForecasts}/search`, params);
  }
}
