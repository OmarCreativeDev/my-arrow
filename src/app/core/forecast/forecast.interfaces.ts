export interface IForecastGetParams {
  accountNumber: number;
  billToNumber: number;
}

export interface IForecastSearchGetParams {
  accountNumber: number;
  billToNumber: number;
  searchText?: string;
  page?: number;
  size?: number;
  sortColumn?: string;
  sortDirection?: string;
  searchBy?: string;
}

export interface IForecastDetails {
  companyName: string;
  received: string;
  lastUpdate: string;
  partsInFilteredView: number;
  forcastHorizon: string;
  partsCovered: number;
  potentialShortages: number;
}

export interface IForecastHorizon {
  id: number;
  date: string;
  quantity: number;
  cumulativeShort: number;
}

export interface IForecastOrderList {
  orderDate: string;
  quantity: number;
}

export interface IForecastOrderPipeLine {
  leadtime: number;
  onOrderList: Array<IForecastOrderList>;
}

export interface IForecastAvailableInventory {
  publicQty: number;
  bufferQty: number;
  onSiteQty: number;
}

export interface IForecastPartList {
  custItemId: string;
  mfrItemId: string;
  mfrName: string;
  quantityPublic: number;
  mult: number;
  itemId: number;
  whsId: number;
  itemStatus: string;
  factoryLeadTime: number;
  restrictedWhCode: string;
  onOrderPipeline: IForecastOrderPipeLine;
  firstShortDate: string;
  inventoryAvailableToSell: IForecastAvailableInventory;
  horizonList: Array<IForecastHorizon>;
  selected?: boolean;
}

export interface IForecast {
  name: string;
  status: string;
  receivedDate: string;
  lastArrowUpdateDate: string;
  billTo: number;
  shipTo: number;
  custAccId: number;
  linesSubmitted: number;
  partsCovered: number;
  potentialShortages: number;
  weeksInHorizon: number;
  content?: Array<IForecastPartList>;
  paging?: any;
  totalPages?: any;
  number?: any;
  pageable?: any;
  shortagesOnly?: boolean;
}

export interface IForecastDownloadOptions {
  dates: Array<string>;
  types: Array<string>;
  columns: Array<string>;
}

export interface IForecastDownloadPreferences {
  dateRange: string;
  fileType: string;
  columns: Array<string>;
}

export interface IForecastDownloadState {
  loading: boolean;
  error?: object;
  status: number;
}

export type IForecastDownloadFields =
  | 'customerPartNumber'
  | 'manufacturerPartNumber'
  | 'buffer'
  | 'mult'
  | 'itemStatus'
  | 'leadTime'
  | 'pipeline'
  | 'billToLocationId'
  | 'shipToLocationId'
  | 'inventoryItemId'
  | 'firstShort'
  | 'manufacturerName'
  | 'posInTransit'
  | 'publicInventory';

export interface IForecastDownload {
  accountNumber: number;
  billToNumber: number;
  fileType: IForecastDownloadFileTypes;
  smrName: string;
  columns: Array<IForecastDownloadFields>;
  weeksInHorizon: number;
}

export interface IForecastSearchDetails {
  accountNumber: number;
  billToNumber: number;
  page: number;
  size: number;
  sortColumn?: string;
  sortDirection?: string;
  searchText?: string;
  searchBy?: string;
  shortagesOnly?: boolean;
  shortageDateRange?: number;
}

export interface IForecastDownloadModal {
  url: string,
}

export type IForecastDownloadFileTypes = 'XLS' | 'XLSX' | 'CSV';
