import { TestBed, inject } from '@angular/core/testing';
import { ApiService } from '@app/core/api/api.service';
import { CoreModule } from '@app/core/core.module';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { ForecastService } from './forecast.service';
import mockedForecastDetails from '@app/core/forecast/forecast-mock-response';
import { of } from 'rxjs';

describe('ForecastService', () => {
  let apiService: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule],
      providers: [ForecastService, { provide: ApiService, useClass: ApiServiceMock }],
    });
    apiService = TestBed.get(ApiService);
  });

  it('should be created', inject([ForecastService], (service: ForecastService) => {
    expect(service).toBeTruthy();
  }));

  it('should return an Observable<IForecast> with the mockedForecastDetails() method ', inject(
    [ForecastService],
    (service: ForecastService) => {
      spyOn(apiService, 'get').and.returnValue(of(mockedForecastDetails));
      service.getForecastDetails(1, 2).subscribe(result => {
        expect(result).toEqual(mockedForecastDetails);
      });
    }
  ));

  it('should return an Observable<IForecast> with the mockedForecastDetails() method ', inject(
    [ForecastService],
    (service: ForecastService) => {
      spyOn(apiService, 'get').and.returnValue(of(mockedForecastDetails));
      service.getForecastSearchDetails({ accountNumber: 1, billToNumber: 2, page: 3, size: 4 }).subscribe(result => {
        expect(result).toEqual(mockedForecastDetails);
      });
    }
  ));

  it('should return an Observable<IForecast> with the getForecastSummary() method ', inject(
    [ForecastService],
    (service: ForecastService) => {
      spyOn(apiService, 'get').and.returnValue(of(mockedForecastDetails));
      service.getForecastSummary(1, 2).subscribe(result => {
        expect(result).toEqual(mockedForecastDetails);
      });
    }
  ));
});
