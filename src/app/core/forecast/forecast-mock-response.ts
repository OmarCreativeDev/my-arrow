export default <any>{
  name: '3M Company',
  status: null,
  receivedDate: '2018-04-09T19:00:00',
  lastArrowUpdateDate: '2018-04-17T22:23:54',
  billTo: 2182504,
  shipTo: 2269269,
  custAccId: 1305343,
  linesSubmitted: 256,
  totalElements: 256,
  partsCovered: 29,
  potentialShortages: 7,
  weeksInHorizon: 38,
  content: [
    {
      custItemId: '26-1016-0006-7',
      mfrItemId: 'OP184ESZ-REEL7',
      mfrName: 'ADI',
      quantityPublic: 0,
      mult: 1000,
      itemId: 6228106,
      whsId: 1790,
      itemStatus: 'Active',
      factoryLeadTime: 9,
      restrictedWhCode: null,
      onOrderPipeline: {
        leadtime: 9,
        onOrderList: [
          {
            orderDate: null,
            quantity: null,
          },
        ],
      },
      firstShortDate: null,
      inventoryAvailableToSell: {
        publicQty: 0,
        bufferQty: 0,
        onSiteQty: 0,
      },
      horizonList: [
        {
          id: 1,
          date: '2018-04-15T19:00:00',
          quantity: 740,
          cumulativeShort: 0,
        },
        {
          id: 2,
          date: '2018-04-22T19:00:00',
          quantity: 1440,
          cumulativeShort: 0,
        },
        {
          id: 3,
          date: '2018-04-29T19:00:00',
          quantity: 180,
          cumulativeShort: 0,
        },
        {
          id: 4,
          date: '2018-05-06T19:00:00',
          quantity: 360,
          cumulativeShort: 0,
        },
        {
          id: 5,
          date: '2018-05-13T19:00:00',
          quantity: 180,
          cumulativeShort: 0,
        },
        {
          id: 6,
          date: '2018-05-20T19:00:00',
          quantity: 480,
          cumulativeShort: 0,
        },
        {
          id: 7,
          date: '2018-05-27T19:00:00',
          quantity: 720,
          cumulativeShort: 0,
        },
        {
          id: 8,
          date: '2018-06-03T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 9,
          date: '2018-06-10T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 11,
          date: '2018-06-24T19:00:00',
          quantity: 180,
          cumulativeShort: 0,
        },
        {
          id: 11,
          date: '2018-06-24T19:00:00',
          quantity: 180,
          cumulativeShort: 0,
        },
        {
          id: 12,
          date: '2018-07-01T19:00:00',
          quantity: 3556,
          cumulativeShort: 0,
        },
        {
          id: 13,
          date: '2018-07-08T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 14,
          date: '2018-07-15T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 15,
          date: '2018-07-22T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 16,
          date: '2018-07-29T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 17,
          date: '2018-08-05T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 18,
          date: '2018-08-12T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 19,
          date: '2018-08-19T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 21,
          date: '2018-09-02T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 21,
          date: '2018-09-02T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 22,
          date: '2018-09-09T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 23,
          date: '2018-09-16T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 24,
          date: '2018-09-23T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 25,
          date: '2018-09-30T19:00:00',
          quantity: 3277,
          cumulativeShort: 0,
        },
        {
          id: 26,
          date: '2018-10-07T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 27,
          date: '2018-10-14T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 28,
          date: '2018-10-21T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 29,
          date: '2018-10-28T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 31,
          date: '2018-11-11T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 31,
          date: '2018-11-11T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 32,
          date: '2018-11-18T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 33,
          date: '2018-11-25T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 34,
          date: '2018-12-02T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 35,
          date: '2018-12-09T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 36,
          date: '2018-12-16T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 37,
          date: '2018-12-23T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 38,
          date: '2018-12-30T18:00:00',
          quantity: 970,
          cumulativeShort: 0,
        },
        {
          id: 39,
          date: '2019-01-06T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 41,
          date: '2019-01-20T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 41,
          date: '2019-01-20T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 42,
          date: '2019-01-27T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 43,
          date: '2019-02-03T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 44,
          date: '2019-02-10T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 45,
          date: '2019-02-17T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 46,
          date: '2019-02-24T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 47,
          date: '2019-03-03T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 48,
          date: '2019-03-10T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 49,
          date: '2019-03-17T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 50,
          date: '2019-03-24T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 51,
          date: '2019-03-31T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 52,
          date: '2019-04-07T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
      ],
      documentId: '1790_06228106',
      ubtInventory: 0,
      custNumber: 1305343,
      primaryWrhse: 1790,
    },
    {
      custItemId: '26-1016-0007-5',
      mfrItemId: 'ADG711BRUZ-REEL7',
      mfrName: 'ADI',
      quantityPublic: 0,
      mult: 1000,
      itemId: 6219284,
      whsId: 1790,
      itemStatus: 'Active',
      factoryLeadTime: 20,
      restrictedWhCode: null,
      onOrderPipeline: {
        leadtime: 20,
        onOrderList: [
          {
            orderDate: null,
            quantity: null,
          },
        ],
      },
      firstShortDate: '2018-05-13T19:00:00',
      inventoryAvailableToSell: {
        publicQty: 0,
        bufferQty: 2000,
        onSiteQty: 0,
      },
      horizonList: [
        {
          id: 1,
          date: '2018-04-15T19:00:00',
          quantity: 685,
          cumulativeShort: 0,
        },
        {
          id: 2,
          date: '2018-04-22T19:00:00',
          quantity: 1680,
          cumulativeShort: 0,
        },
        {
          id: 3,
          date: '2018-04-29T19:00:00',
          quantity: 210,
          cumulativeShort: 0,
        },
        {
          id: 4,
          date: '2018-05-06T19:00:00',
          quantity: 420,
          cumulativeShort: 0,
        },
        {
          id: 5,
          date: '2018-05-13T19:00:00',
          quantity: 210,
          cumulativeShort: 205,
        },
        {
          id: 6,
          date: '2018-05-20T19:00:00',
          quantity: 570,
          cumulativeShort: 0,
        },
        {
          id: 7,
          date: '2018-05-27T19:00:00',
          quantity: 840,
          cumulativeShort: 0,
        },
        {
          id: 8,
          date: '2018-06-03T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 9,
          date: '2018-06-10T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 11,
          date: '2018-06-24T19:00:00',
          quantity: 210,
          cumulativeShort: 0,
        },
        {
          id: 11,
          date: '2018-06-24T19:00:00',
          quantity: 210,
          cumulativeShort: 0,
        },
        {
          id: 12,
          date: '2018-07-01T19:00:00',
          quantity: 4154,
          cumulativeShort: 0,
        },
        {
          id: 13,
          date: '2018-07-08T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 14,
          date: '2018-07-15T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 15,
          date: '2018-07-22T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 16,
          date: '2018-07-29T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 17,
          date: '2018-08-05T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 18,
          date: '2018-08-12T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 19,
          date: '2018-08-19T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 21,
          date: '2018-09-02T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 21,
          date: '2018-09-02T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 22,
          date: '2018-09-09T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 23,
          date: '2018-09-16T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 24,
          date: '2018-09-23T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 25,
          date: '2018-09-30T19:00:00',
          quantity: 3818,
          cumulativeShort: 0,
        },
        {
          id: 26,
          date: '2018-10-07T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 27,
          date: '2018-10-14T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 28,
          date: '2018-10-21T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 29,
          date: '2018-10-28T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 31,
          date: '2018-11-11T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 31,
          date: '2018-11-11T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 32,
          date: '2018-11-18T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 33,
          date: '2018-11-25T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 34,
          date: '2018-12-02T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 35,
          date: '2018-12-09T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 36,
          date: '2018-12-16T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 37,
          date: '2018-12-23T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 38,
          date: '2018-12-30T18:00:00',
          quantity: 1130,
          cumulativeShort: 0,
        },
        {
          id: 39,
          date: '2019-01-06T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 41,
          date: '2019-01-20T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 41,
          date: '2019-01-20T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 42,
          date: '2019-01-27T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 43,
          date: '2019-02-03T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 44,
          date: '2019-02-10T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 45,
          date: '2019-02-17T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 46,
          date: '2019-02-24T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 47,
          date: '2019-03-03T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 48,
          date: '2019-03-10T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 49,
          date: '2019-03-17T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 50,
          date: '2019-03-24T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 51,
          date: '2019-03-31T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 52,
          date: '2019-04-07T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
      ],
      documentId: '1790_06219284',
      ubtInventory: 0,
      custNumber: 1305343,
      primaryWrhse: 1790,
    },
    {
      custItemId: '26-1017-6961-5',
      mfrItemId: 'LTC2630ISC6-LZ8#TRMPBF',
      mfrName: 'LTC',
      quantityPublic: 1000,
      mult: 500,
      itemId: 6450800,
      whsId: 1790,
      itemStatus: 'Active',
      factoryLeadTime: 6,
      restrictedWhCode: null,
      onOrderPipeline: {
        leadtime: 6,
        onOrderList: [
          {
            orderDate: null,
            quantity: null,
          },
        ],
      },
      firstShortDate: null,
      inventoryAvailableToSell: {
        publicQty: 1000,
        bufferQty: 500,
        onSiteQty: 0,
      },
      horizonList: [
        {
          id: 1,
          date: '2018-04-15T19:00:00',
          quantity: 89,
          cumulativeShort: 0,
        },
        {
          id: 2,
          date: '2018-04-22T19:00:00',
          quantity: 330,
          cumulativeShort: 0,
        },
        {
          id: 3,
          date: '2018-04-29T19:00:00',
          quantity: 90,
          cumulativeShort: 0,
        },
        {
          id: 4,
          date: '2018-05-06T19:00:00',
          quantity: 30,
          cumulativeShort: 0,
        },
        {
          id: 5,
          date: '2018-05-13T19:00:00',
          quantity: 30,
          cumulativeShort: 0,
        },
        {
          id: 6,
          date: '2018-05-20T19:00:00',
          quantity: 120,
          cumulativeShort: 0,
        },
        {
          id: 7,
          date: '2018-05-27T19:00:00',
          quantity: 150,
          cumulativeShort: 0,
        },
        {
          id: 8,
          date: '2018-06-03T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 9,
          date: '2018-06-10T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 11,
          date: '2018-06-24T19:00:00',
          quantity: 120,
          cumulativeShort: 0,
        },
        {
          id: 11,
          date: '2018-06-24T19:00:00',
          quantity: 120,
          cumulativeShort: 0,
        },
        {
          id: 12,
          date: '2018-07-01T19:00:00',
          quantity: 720,
          cumulativeShort: 0,
        },
        {
          id: 13,
          date: '2018-07-08T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 14,
          date: '2018-07-15T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 15,
          date: '2018-07-22T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 16,
          date: '2018-07-29T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 17,
          date: '2018-08-05T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 18,
          date: '2018-08-12T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 19,
          date: '2018-08-19T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 21,
          date: '2018-09-02T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 21,
          date: '2018-09-02T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 22,
          date: '2018-09-09T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 23,
          date: '2018-09-16T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 24,
          date: '2018-09-23T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 25,
          date: '2018-09-30T19:00:00',
          quantity: 750,
          cumulativeShort: 0,
        },
        {
          id: 26,
          date: '2018-10-07T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 27,
          date: '2018-10-14T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 28,
          date: '2018-10-21T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 29,
          date: '2018-10-28T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 31,
          date: '2018-11-11T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 31,
          date: '2018-11-11T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 32,
          date: '2018-11-18T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 33,
          date: '2018-11-25T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 34,
          date: '2018-12-02T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 35,
          date: '2018-12-09T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 36,
          date: '2018-12-16T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 37,
          date: '2018-12-23T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 38,
          date: '2018-12-30T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 39,
          date: '2019-01-06T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 41,
          date: '2019-01-20T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 41,
          date: '2019-01-20T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 42,
          date: '2019-01-27T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 43,
          date: '2019-02-03T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 44,
          date: '2019-02-10T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 45,
          date: '2019-02-17T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 46,
          date: '2019-02-24T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 47,
          date: '2019-03-03T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 48,
          date: '2019-03-10T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 49,
          date: '2019-03-17T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 50,
          date: '2019-03-24T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 51,
          date: '2019-03-31T18:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
        {
          id: 52,
          date: '2019-04-07T19:00:00',
          quantity: 0,
          cumulativeShort: 0,
        },
      ],
      documentId: '1790_06450800',
      ubtInventory: 0,
      custNumber: 1305343,
      primaryWrhse: 1790,
    },
  ],
};
