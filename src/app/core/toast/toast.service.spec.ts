import { TestBed } from '@angular/core/testing';

import { ToastService } from './toast.service';
import { ToastrService } from 'ngx-toastr';
class MockToastrService {
  public show() {}
  public remove() {}
  public clear() {}
}

describe('ToastService', () => {
  let service: ToastService;
  let toastrService: ToastrService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ToastService, { provide: ToastrService, useClass: MockToastrService }],
    }).compileComponents();

    toastrService = TestBed.get(ToastrService);
    service = TestBed.get(ToastService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Should call toastrService.show() with the correct parameters', () => {
    it('`good` on showToast()', () => {
      spyOn(toastrService, 'show');
      service.showToast('good');
      expect(toastrService.show).toHaveBeenCalledWith('good', '', {}, 'success');
    });

    it('`bad` on showErrorToast()', () => {
      spyOn(toastrService, 'show');
      service.showErrorToast('bad');
      expect(toastrService.show).toHaveBeenCalledWith('bad', '', {}, 'error');
    });
  });

  it('should call toastrService.clear() on hideToast()', () => {
    spyOn(toastrService, 'clear');
    service.hideToast();
    expect(toastrService.clear).toHaveBeenCalled();
  });
});
