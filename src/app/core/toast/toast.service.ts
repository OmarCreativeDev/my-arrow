import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class ToastService {
  constructor(private toastrService: ToastrService) {}

  public showToast(message: string) {
    this.toastrService.show(message, '', {}, 'success');
  }

  public hideToast() {
    this.toastrService.clear();
  }

  public showErrorToast(message: string) {
    this.toastrService.show(message, '', {}, 'error');
  }
}
