import { inject, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';

import { ApiService } from '@app/core/api/api.service';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { CoreModule } from '@app/core/core.module';
import { OfflineLiveChatService } from '@app/core/offline-live-chat/offline-live-chat.service';
import { IOfflineLiveChat } from '@app/core/offline-live-chat/offline-live-chat.interface';
import { environment } from '@env/environment';

const formparams: IOfflineLiveChat = {
  orgid: environment.liveChatSettings.orgId,
  firstName: 'Software',
  origin: 'MyArrow - Offline Chat',
  subject: 'MyArrow 2.0 Offline Chat Request - ' + 'Software' + ' ' + 'Developer',
  email: 'test@test.com',
  description: 'Arrow Offline chat test',
  Website__c: 'MyArrow.com',
  Communication_Type__c: 'Web',
  elqFormName: 'MyarrowNaEnDesignCenterEngineerProfile',
  elqSiteId: '600830862',
  sfdcUrl: environment.liveChatSettings.sfdcUrl,
  lastName: 'Developer',
  company: 'Arrow',
  LeadSource: 'ecommerce',
  Lead_Source_Detail__c: 'MyArrow',
  Lead_Source_Reference__c: 'Design Center',
  uploadType: 'Form Submission',
  Campaign_ID: '70170000001645b',
  Category__c: 'Engineering',
};
export class StoreMock {
  public select(): Observable<any> {
    return of({});
  }
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

describe('OfflineLiveChatService', () => {
  let apiService: ApiService;
  let offlineLiveChatService: OfflineLiveChatService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule],
      providers: [OfflineLiveChatService, { provide: ApiService, useClass: ApiServiceMock }, { provide: Store, useClass: StoreMock }],
    });
    apiService = TestBed.get(ApiService);
    offlineLiveChatService = TestBed.get(OfflineLiveChatService);
  });

  it('should be created', inject([OfflineLiveChatService], (service: OfflineLiveChatService) => {
    expect(service).toBeTruthy();
  }));

  it('should create a case', () => {
    spyOn(apiService, 'post');
    offlineLiveChatService.createCase(formparams);
    expect(apiService.post).toHaveBeenCalledWith(
      `${environment.baseUrls.serviceProperties + environment.liveChatSettings.offlinePostUrl}`,
      formparams
    );
  });
});
