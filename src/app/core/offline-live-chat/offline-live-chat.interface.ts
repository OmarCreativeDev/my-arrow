export interface IOfflineLiveChat {
  orgid: string;
  origin: string;
  subject: string;
  firstName: string;
  lastName: string;
  email: string;
  description: string;
  company: string;
  Website__c: string;
  Communication_Type__c: string;
  elqFormName: string;
  elqSiteId: string;
  sfdcUrl: string;
  LeadSource: string;
  Lead_Source_Detail__c: string;
  Lead_Source_Reference__c: string;
  uploadType: string;
  Campaign_ID: string;
  Category__c: string;
}
