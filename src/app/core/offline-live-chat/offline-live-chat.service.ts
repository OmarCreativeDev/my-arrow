import { Injectable } from '@angular/core';
import { ApiService } from '@app/core/api/api.service';
import { IOfflineLiveChat } from '@app/core/offline-live-chat/offline-live-chat.interface';
import { environment } from '@env/environment';

@Injectable()
export class OfflineLiveChatService {
  constructor(private apiService: ApiService) {}

  public createCase(data: IOfflineLiveChat) {
    return this.apiService.post(`${environment.baseUrls.serviceProperties + environment.liveChatSettings.offlinePostUrl}`, data);
  }
}
