import { TestBed, inject } from '@angular/core/testing';

import { EmailService } from '@app/core/email/email.service';
import { IEmail } from '@app/core/email/email.interfaces';
import { ApiService } from '@app/core/api/api.service';
import { CoreModule } from '@app/core/core.module';
import { environment } from '@env/environment';

describe('EmailService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule],
      providers: [EmailService],
    });
  });

  it('should be created', inject([EmailService], (service: EmailService) => {
    expect(service).toBeTruthy();
  }));

  it('should sendEmail', inject([EmailService, ApiService], (service: EmailService, apiService: ApiService) => {
    spyOn(apiService, 'post');
    const email: IEmail = {
      from: 'from@email.com',
      to: ['to@email.com'],
      subject: 'Email subject',
      content: 'Email body',
    };
    service.sendEmail(email);
    expect(apiService.post).toHaveBeenCalledWith(`${environment.baseUrls.serviceEmail}/send`, email);
  }));

  it('should sendTemplatedEmail', inject([EmailService, ApiService], (service: EmailService, apiService: ApiService) => {
    spyOn(apiService, 'post');
    const email: IEmail = {
      from: 'from@email.com',
      to: ['to@email.com'],
      subject: 'Email subject',
      content: 'Email body - should be ignored since using template',
      firstname: 'Testname',
      salutation: 'Mr.',
    };
    service.sendTemplatedEmail(email, 'basic');
    expect(apiService.post).toHaveBeenCalledWith(`${environment.baseUrls.serviceEmail}/sendTemplatedEmail/basic`, email);
  }));
});
