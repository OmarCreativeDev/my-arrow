import { Injectable } from '@angular/core';
import { ApiService } from '@app/core/api/api.service';
import { environment } from '@env/environment';
import { IEmail, EmailTemplates } from '@app/core/email/email.interfaces';

@Injectable()
export class EmailService {
  constructor(private apiService: ApiService) {}

  public sendEmail(email: IEmail) {
    return this.apiService.post<any>(`${environment.baseUrls.serviceEmail}/send`, email);
  }

  public sendTemplatedEmail(email: IEmail, templateName: EmailTemplates) {
    return this.apiService.post<any>(`${environment.baseUrls.serviceEmail}/sendTemplatedEmail/${templateName}`, email);
  }
}
