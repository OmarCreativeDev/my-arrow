export interface IEmail {
  from?: string;
  to: Array<string>;
  cc?: Array<string>;
  subject: string;
  content?: string; // not used when using a template
  firstname?: string; // passed on to templating engine
  salutation?: string; // passed on to templating engine
  payload?: any; // TODO: This should be a generic type
}

export type EmailTemplates = 'basic' | 'order-history'; // new items can be added with a |
