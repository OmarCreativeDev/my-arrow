import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { ApiService } from '@app/core/api/api.service';
import { CoreModule } from '@app/core/core.module';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { CheckoutService } from './checkout.service';
import { mockedCheckout } from '@app/features/cart/stores/checkout/checkout.reducers.spec';
import { IShipTo } from './checkout.interfaces';
import { StoreModule, Store } from '@ngrx/store';
import { userReducers } from '../user/store/user.reducers';
import { mockUser } from '@app/core/user/user.service.spec';
import { RequestUserComplete } from '../user/store/user.actions';

export const mockShippingAddressList: IShipTo[] = [
  {
    id: 123,
    address1: 'MILPITAS SITE 335',
    address2: '5802 BOB BULLOCK C1',
    address3: 'PMB 014-507',
    address4: '',
    city: 'LAREDO',
    county: '',
    state: 'TX',
    country: 'US',
    name: 'FLEXTRONICS INTERNATIONAL USA',
    postCode: '78041',
    primaryFlag: true,
    selected: true,
  },
  {
    id: 24667766,
    address1: 'SITE 998',
    address2: '5902 ROB BULLOCK C1',
    address3: 'PMB 014-537',
    address4: '',
    city: 'DENVER',
    county: '',
    state: 'COL',
    country: 'US',
    name: 'FLEXTRONICS INTERNATIONAL INC',
    postCode: '78061',
    primaryFlag: true,
    selected: false,
  },
];

describe('CheckoutService', () => {
  let service: CheckoutService;
  let apiService: ApiService;
  let store: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule, StoreModule.forRoot({ user: userReducers })],
      providers: [CheckoutService, { provide: ApiService, useClass: ApiServiceMock }],
    });
    store = TestBed.get(Store);
    apiService = TestBed.get(ApiService);
    service = TestBed.get(CheckoutService);
    spyOn(store, 'dispatch').and.callThrough();
  });

  beforeEach(() => {
    store.dispatch(new RequestUserComplete(mockUser));
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('getCheckout() should return an Observable<ICheckout>', () => {
    spyOn(apiService, 'get').and.returnValue(of(mockedCheckout.data));
    service.getCheckout().subscribe(checkout => {
      expect(checkout.accountInfo).toEqual(mockedCheckout.data.accountInfo);
      expect(checkout.cart.lineItems).toEqual(mockedCheckout.data.cart.lineItems);
      expect(checkout.shippingAddress).toEqual(mockedCheckout.data.shippingAddress);
      expect(checkout.shippingOptions).toEqual(mockedCheckout.data.shippingOptions);
    });
  });

  it('#acceptNcnrItems', () => {
    spyOn(apiService, 'put').and.returnValue(of([mockedCheckout.data.cart.lineItems[0]]));

    service
      .acceptNcnrItems({
        items: [mockedCheckout.data.cart.lineItems[0]],
        cartId: '0jsjda2131232',
      })
      .subscribe(response => {
        expect(response).toEqual([mockedCheckout.data.cart.lineItems[0]]);
      });
    expect(apiService.put).toHaveBeenCalled();
  });

  it('getAddressList() should return an Observable<IShipTo[]>', () => {
    spyOn(apiService, 'get').and.returnValue(of(mockShippingAddressList));
    const galSpy = spyOn(service, 'getAddressList').and.callThrough();

    service.getAddressList().subscribe(shippingAddressList => {
      expect(galSpy).toHaveBeenCalled();
      expect(shippingAddressList[0].address1).toEqual(mockShippingAddressList[0].address1);
    });
  });

  it('getAddress() should return an Observable<IShipTo>', () => {
    const galSpy = spyOn(service, 'getAddress').and.callThrough();

    const testShippingAddressId = 24667766;
    const filteredMockShippingAddress = mockShippingAddressList.filter(value => value.id === testShippingAddressId);

    spyOn(apiService, 'get').and.returnValue(of(filteredMockShippingAddress[0]));

    service.getAddress(testShippingAddressId).subscribe(shippingAddress => {
      expect(galSpy).toHaveBeenCalled();
      expect(shippingAddress.id).toEqual(testShippingAddressId);
    });
  });
});
