import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { getCheckoutSelector } from '@app/features/cart/stores/checkout/checkout.selectors';
import { ICheckout, ICheckoutCart } from '@app/core/checkout/checkout.interfaces';
import { ClearCheckoutData, GetCheckoutData } from '@app/features/cart/stores/checkout/checkout.actions';
import { ICartStatus, ICartValidationStatus } from '@app/core/cart/cart.interfaces';
import { filter, take, switchMap, catchError } from 'rxjs/operators';

@Injectable()
export class CheckoutGuard implements CanActivate {
  constructor(private store: Store<IAppState>, private router: Router) {}

  areCartAndItemsValid(cart: ICheckoutCart): boolean {
    const { lineItems } = cart;
    const validItems = lineItems.filter(lineItem => lineItem.validation === ICartValidationStatus.VALID);
    return cart.status === ICartStatus.IN_PROGRESS && lineItems.length && lineItems.length === validItems.length;
  }

  getFromAPI(): Observable<ICheckout> {
    this.store.dispatch(new ClearCheckoutData());
    this.store.dispatch(new GetCheckoutData());
    return this.store.pipe(
      select(getCheckoutSelector),
      filter(checkout => checkout.cart !== undefined),
      take(1)
    );
  }

  redirectToCart(): void {
    this.router.navigate(['/cart']);
  }

  canActivate(): Observable<boolean> {
    return this.getFromAPI().pipe(
      switchMap(checkout => {
        if (this.areCartAndItemsValid(checkout.cart)) {
          return of(true);
        }
        this.redirectToCart();
        return of(false);
      }),
      catchError(() => {
        this.redirectToCart();
        return of(false);
      })
    );
  }
}
