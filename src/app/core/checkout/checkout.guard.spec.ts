import { Location } from '@angular/common';
import { TestBed, inject, tick, fakeAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { StoreModule, Store } from '@ngrx/store';
import { cloneDeep } from 'lodash-es';
import { CoreModule } from '@app/core/core.module';
import { ICheckoutCart } from '@app/core/checkout/checkout.interfaces';
import { ICartStatus, ICartValidationStatus } from '@app/core/cart/cart.interfaces';
import { checkoutReducers } from '@app/features/cart/stores/checkout/checkout.reducers';
import { CheckoutGuard } from '@app/core/checkout/checkout.guard';
import { HttpErrorResponse } from '@angular/common/http';

class DummyComponent {}

const mockedCart: ICheckoutCart = {
  company: 'B & C ELECTRONIC ENGINEERING',
  billToId: 2171487,
  currency: 'USD',
  accountNumber: null,
  id: '5b0e8f35d93ddf000128c9db',
  userId: 'seamless_test@gmail.com',
  status: ICartStatus.IN_PROGRESS,
  lineItems: [
    {
      id: '5b16a3cb768ae20001304291',
      total: 34.68,
      ncnrAccepted: null,
      ncnrAcceptedBy: null,
      manufacturerPartNumber: 'BAV99WT1G',
      valid: null,
      itemType: null,
      selectedEndCustomerSiteName: null,
      manufacturer: 'ON Semiconductor|ONSEMI',
      description: 'Diode Switching 100V 0.715A 3-Pin SC-70 T/R',
      image: 'http://download.siliconexpert.com/pdfs/2017/5/11/9/7/8/409/ons_/manual/3sc70.jpg',
      inStock: true,
      quotable: null,
      arrowReel: null,
      ncnr: true,
      businessCost: null,
      minimumOrderQuantity: 3000,
      multipleOrderQuantity: 3000,
      availableQuantity: 731999,
      bufferQuantity: 0,
      leadTime: '15',
      price: 0.01156,
      validation: ICartValidationStatus.VALID,
      itemId: 7323176,
      warehouseId: 1790,
      docId: '1790_07323176',
      selectedCustomerPartNumber: null,
      enteredCustomerPartNumber: null,
      selectedEndCustomerSiteId: null,
      requestDate: '2018-06-08',
      quantity: 3000,
      reel: null,
    },
    {
      id: '5b16a418768ae20001304295',
      total: 2267.79,
      ncnrAccepted: null,
      ncnrAcceptedBy: null,
      manufacturerPartNumber: 'LM95071CIMFX/NOPB',
      valid: null,
      itemType: null,
      selectedEndCustomerSiteName: null,
      manufacturer: 'Texas Instruments|TI',
      description: 'Temp Sensor Digital Serial (3-Wire) 5-Pin SOT-23 T/R',
      image: 'http://download.siliconexpert.com/pdfs/2017/3/5/5/40/26/834/txn_/manual/dbv0005a.jpg',
      inStock: false,
      quotable: null,
      arrowReel: null,
      ncnr: true,
      businessCost: null,
      minimumOrderQuantity: 3000,
      multipleOrderQuantity: 3000,
      availableQuantity: 3000,
      bufferQuantity: 0,
      leadTime: '21',
      price: 0.75593,
      validation: ICartValidationStatus.VALID,
      itemId: 7283092,
      warehouseId: 1801,
      docId: '1801_07283092',
      selectedCustomerPartNumber: null,
      enteredCustomerPartNumber: null,
      selectedEndCustomerSiteId: null,
      requestDate: '2018-06-08',
      quantity: 3000,
      reel: null,
    },
  ],
  subTotal: 2302.47,
  itemCount: 2,
};

describe('CheckoutGuard', () => {
  const routes = [
    { path: 'cart', component: DummyComponent },
    { path: 'cart/checkout', component: DummyComponent },
    { path: 'cart/checkout/confirmation', component: DummyComponent },
  ];
  let store: Store<any>;
  let location: Location;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        StoreModule.forRoot({
          checkout: checkoutReducers,
        }),
        RouterTestingModule.withRoutes(routes),
      ],
      providers: [CheckoutGuard],
    });
    location = TestBed.get(Location);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', inject([CheckoutGuard], (guard: CheckoutGuard) => {
    expect(guard).toBeTruthy();
  }));

  it('should return true if cart is IN_PROGRESS and lineItems are VALID', inject([CheckoutGuard], (guard: CheckoutGuard) => {
    const valid = guard.areCartAndItemsValid(mockedCart);
    expect(valid).toBeTruthy();
  }));

  it('should return false if cart is not IN_PROGRESS', inject([CheckoutGuard], (guard: CheckoutGuard) => {
    const checkoutCart = cloneDeep(mockedCart);
    checkoutCart.status = ICartStatus.SUBMITTED;
    const valid = guard.areCartAndItemsValid(checkoutCart);
    expect(valid).toBeFalsy();
  }));

  it('should return false if any item is not VALID', inject([CheckoutGuard], (guard: CheckoutGuard) => {
    const checkoutCart = cloneDeep(mockedCart);
    checkoutCart.lineItems[0].validation = ICartValidationStatus.PENDING;
    const valid = guard.areCartAndItemsValid(checkoutCart);
    expect(valid).toBeFalsy();
  }));

  it("should redirect to '/cart' if redirectToCart() is called", fakeAsync(
    inject([CheckoutGuard], (guard: CheckoutGuard) => {
      guard.redirectToCart();
      tick(20);
      expect(location.path()).toBe('/cart');
    })
  ));

  it("should guard route and redirect to '/cart' if there are no items", inject([CheckoutGuard], (guard: CheckoutGuard) => {
    guard.canActivate().subscribe(canActivate => {
      expect(canActivate).toBeFalsy();
      tick(20);
      expect(location.path()).toBe('/cart');
    });
  }));

  it('should guard route and allow navigation if receives valid checkout cart', inject([CheckoutGuard], (guard: CheckoutGuard) => {
    const checkoutCart = cloneDeep(mockedCart);
    checkoutCart.status = ICartStatus.IN_PROGRESS;
    spyOn(guard, 'getFromAPI').and.returnValue(of({ cart: checkoutCart }));
    guard.canActivate().subscribe(canActivate => {
      expect(canActivate).toBeTruthy();
    });
  }));

  it("should guard route and redirect to '/cart' if receives invalid checkout cart", inject([CheckoutGuard], (guard: CheckoutGuard) => {
    const checkoutCart = cloneDeep(mockedCart);
    checkoutCart.status = ICartStatus.SUBMITTED;
    spyOn(guard, 'getFromAPI').and.returnValue(of({ cart: checkoutCart }));
    guard.canActivate().subscribe(canActivate => {
      expect(canActivate).toBeFalsy();
    });
  }));

  it("should guard route and redirect to '/cart' if receives an error", inject([CheckoutGuard], (guard: CheckoutGuard) => {
    spyOn(guard, 'getFromAPI').and.returnValue(of(new HttpErrorResponse({ status: 500 })));
    guard.canActivate().subscribe(canActivate => {
      expect(canActivate).toBeFalsy();
    });
  }));
});
