import { IContact, IBillTo } from '@app/core/user/user.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { INcnrStatus } from '@app/features/cart/components/ncnr-dialog/ncnr-dialog.interfaces';

export interface ICheckoutAccountInfo {
  accountName: string;
  accountNumber: number;
  terms: string;
  registrationInstance: string;
  region: string;
  paymentsTermId: number;
  paymentsTermName: string;
  currencyCode: string;
  orgId: number;
  selectedBillTo: number;
  selectedShipTo: number;
  accountId: number;
}

export interface IShippingCollectOption {
  carrierAccountNumber: string;
  carrierCode: string;
  carrierId: string;
  shipMethodCode: string;
}

export interface IShippingOption {
  code: string;
  name: string;
  options?: IShippingCollectOption[];
}

export interface IShipTo {
  id?: number;
  address1: string;
  address2: string;
  address3: string;
  address4: string;
  city: string;
  county: string;
  state: string;
  country: string;
  name: string;
  postCode: string;
  primaryFlag: boolean;
  selected?: boolean;
}

export interface ICreditCardInfo {
  firstName: string;
  lastName: string;
  address1: string;
  address2?: string;
  city: string;
  country: string;
  zip: string;
  phone: number;
  ccNumber: number;
  cvv: number;
  expMonth: string;
  expYear: number;
}

export interface ICheckout {
  accountInfo: ICheckoutAccountInfo;
  cart: ICheckoutCart;
  shippingAddress: IShipTo;
  shippingOptions: IShippingOption[];
  creditCard?: ICreditCardInfo;
}

export interface ICheckoutOptions {
  shippingMethod: string;
  shipComplete: boolean;
  poNumber: string;
  salesRepReview: boolean;
  checkoutWithCreditCard: boolean;
  externalComments?: string;
}

export interface ICheckoutState {
  data: ICheckout;
  options: ICheckoutOptions;
  confirmedOrderId: number;
  error?: Error;
  loading: boolean;
  orderPlacementInProgress: boolean;
  orderPlacementError: HttpErrorResponse;
  ncnrUpdateStatus: INcnrStatus;
}

// To replace IShoppingCartReel in future sprint
export interface ICheckoutReel {
  cost: number;
  itemsPerReel: number;
  margin: number;
  numberOfReels: number;
  resale: number;
}

// To replace IShoppingCartItem in future sprint
export interface ICheckoutCartItem {
  arrowReel: boolean;
  availableQuantity: number;
  bufferQuantity: number;
  businessCost: number;
  description: string;
  docId: string;
  enteredCustomerPartNumber: string;
  expiryDate?: string;
  id: string;
  image: string;
  inStock: boolean;
  itemId: number;
  itemType: string;
  leadTime: string;
  manufacturer: string;
  manufacturerPartNumber: string;
  minimumOrderQuantity?: number;
  multipleOrderQuantity?: number;
  ncnr?: boolean;
  price?: number;
  quantity: number;
  quotable: boolean;
  quoteHeaderId?: number;
  quoteLineId?: number;
  quoteNumber?: string;
  quoted?: boolean;
  quotedPrice?: number;
  reel: ICheckoutReel;
  requestDate: string;
  selectedCustomerPartNumber: string;
  selectedEndCustomerSiteId: number;
  selectedEndCustomerSiteName: string;
  total: number;
  warehouseId: number;
  valid?: boolean;
  changed?: boolean;
  ncnrAccepted?: boolean;
  ncnrAcceptedBy?: string;
  validation: string;
  tariffValue?: number;
  hts?: string;
  htsFlag?: boolean;
  tariffApplicable?: boolean;
  basePrice?: number;
  originalQuantity?: number;
  quantityChanged?: boolean;
}

// To replace IShoppingCart in future sprint
export interface ICheckoutCart {
  company: string;
  accountNumber: number;
  billToId: number;
  currency: string;
  id: string;
  itemCount: number;
  lineItems: ICheckoutCartItem[];
  status: string;
  subTotal: number;
  userId: string;
  tariffTotal?: number;
  total?: number;
}

export interface ICheckoutPlaceOrderRequest {
  accountInfo: {
    accountId: number;
    accountName: string;
    accountNumber: number;
    orgId: number;
    region: string;
  };
  cart: ICheckoutCart;
  orderDetails: ICheckoutOrderDetails;
  creditCardInfo?: {
    authorizationCode: string;
    merchantId: string;
    paymentProductId: string;
    paymentReference: string;
  };
  userInfo: {
    firstName: string;
    lastName: string;
  };
  contact: IContact;
  paymentType: string;
  shippingAddress: IOrderCheckoutShipTo;
  billingAddress: IOrderCheckoutBillTo;
}

export interface ICheckoutOrderDetails extends ICheckoutOptions {
  bookOrder: boolean;
}

export interface IOrderCheckoutBillTo extends IBillTo {
  id: number;
}

export interface IOrderCheckoutShipTo extends IShipTo {
  id: number;
}

export interface ICheckoutConfirmation extends ICheckoutPlaceOrderRequest {
  orderId: number;
}
