import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { ApiService } from '../api/api.service';
import { ICheckout, ICheckoutCartItem, IShipTo } from './checkout.interfaces';
import { IAcceptNcnrItemsRequest } from '@app/features/cart/components/ncnr-dialog/ncnr-dialog.interfaces';
import { getUser } from '../user/store/user.selectors';
import { switchMap } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';

@Injectable()
export class CheckoutService {
  constructor(private apiService: ApiService, private store: Store<IAppState>) {}

  public getCheckout(): Observable<ICheckout> {
    return this.apiService.get<ICheckout>(`${environment.baseUrls.serviceCheckout}/`);
  }

  /**
   * Retrieves Address list info for authenticated user
   */
  public getAddressList(billToID?: number): Observable<IShipTo[]> {
    return this.store.pipe(
      select(getUser),
      switchMap(user => {
        const billTo = billToID || user.selectedBillTo;
        return this.apiService.get<IShipTo[]>(`${environment.baseUrls.serviceCheckout}/billing-addresses/${billTo}/`, {
          registrationInstance: user.registrationInstance,
          ouID: user.orgId,
        });
      })
    );
  }

  /**
   * Retrieves ShipTo Address from certain Billing Address info for authenticated user
   */
  public getAddress(shipToId: number, billToID?: number): Observable<IShipTo> {
    return this.store.pipe(
      select(getUser),
      switchMap(user => {
        const billTo = billToID || user.selectedBillTo;
        return this.apiService.get<IShipTo>(
          `${environment.baseUrls.serviceCheckout}/billing-addresses/${billTo}/shipping-addresses/${shipToId}`,
          {
            registrationInstance: user.registrationInstance,
            ouID: user.orgId,
          }
        );
      })
    );
  }

  public acceptNcnrItems(acceptNcnrItemsRequest: IAcceptNcnrItemsRequest): Observable<Array<ICheckoutCartItem>> {
    return this.apiService.put<Array<ICheckoutCartItem>>(
      `${environment.baseUrls.serviceShoppingCart}/${acceptNcnrItemsRequest.cartId}/lineItems`,
      acceptNcnrItemsRequest.items,
      true
    );
  }
}
