import { Injectable } from '@angular/core';
import { endsWith, get } from 'lodash-es';
import { environment } from '@env/environment';
import { WindowRefService } from '@app/core/window/window.service';
import { IPrechatData } from '@app/layout/components/header/chat-button/chat-button.interface';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class LiveChatService {
  private liveagentScriptId: string = 'liveagent-script';
  private nativeWindow: Window;
  private messageEventHandler: (event: any) => void;

  constructor(private windowRefService: WindowRefService) {
    this.nativeWindow = this.windowRefService.nativeWindow;
  }

  private addMessageListener(callback): void {
    const eventMethod = this.nativeWindow.addEventListener ? 'addEventListener' : 'attachEvent';
    const eventer = this.nativeWindow[eventMethod];
    const messageEvent = eventMethod === 'attachEvent' ? 'onmessage' : 'message';
    eventer(messageEvent, callback, false);
  }

  private removeMessageListener(): void {
    const eventMethod = this.nativeWindow.removeEventListener ? 'removeEventListener' : 'detachEvent';
    const eventer = this.nativeWindow[eventMethod];
    const messageEvent = eventMethod === 'detachEvent' ? 'onmessage' : 'message';
    eventer(messageEvent, this.messageEventHandler, false);
  }

  public getLiveChatStarter(prechatData: IPrechatData, onlineButtonRef: any, offlineButtonRef: any): Observable<any> {
    const { chatButtonId, deploymentId, deploymentUrl, initUrl, orgId } = environment.liveChatSettings;
    const subject: Subject<any> = new Subject();
    const deploymentScript = this.nativeWindow.document.createElement('script');
    deploymentScript.setAttribute('type', 'text/javascript');
    deploymentScript.setAttribute('id', this.liveagentScriptId);
    deploymentScript.onload = () => {
      const liveagent = this.nativeWindow['liveagent'];
      if (!this.nativeWindow['_laq']) {
        this.nativeWindow['_laq'] = [];
      }
      this.nativeWindow['_laq'].push(function() {
        liveagent.showWhenOnline(chatButtonId, onlineButtonRef);
        liveagent.showWhenOffline(chatButtonId, offlineButtonRef);
      });
      this.messageEventHandler = function(event) {
        if (
          get(event.data, 'type', null) === 'prechatDataRequest' &&
          (endsWith(event.origin, 'arrow.com') || endsWith(event.origin, 'force.com'))
        ) {
          const prechatFields = {};
          for (const field in prechatData) {
            if (prechatData.hasOwnProperty(field)) {
              prechatFields['liveagent.prechat:' + field] = prechatData[field] ? prechatData[field] : '';
            }
          }
          event.source.postMessage(
            {
              type: 'prechatDataReponse',
              payload: prechatFields,
            },
            event.origin
          );
        }
      };
      this.addMessageListener(this.messageEventHandler);
      liveagent.init(initUrl, deploymentId, orgId);
      subject.next(liveagent.startChat);
      subject.complete();
    };
    this.nativeWindow.document.body.appendChild(deploymentScript);
    deploymentScript.src = deploymentUrl;
    return subject.asObservable();
  }

  public destroyLiveChat(): void {
    const liveagent = this.nativeWindow['liveagent'];
    const scriptTag = this.nativeWindow.document.getElementById(this.liveagentScriptId);
    scriptTag.parentNode.removeChild(scriptTag);
    if (liveagent && liveagent.disconect) {
      liveagent.disconect();
    }
    this.removeMessageListener();
    this.nativeWindow['liveAgentDeployment'] = undefined;
    this.nativeWindow['liveagent'] = undefined;
    this.nativeWindow['_laq'] = undefined;
    try {
      delete this.nativeWindow['liveAgentDeployment'];
      delete this.nativeWindow['liveagent'];
      delete this.nativeWindow['_laq'];
    } catch (e) {}
  }
}
