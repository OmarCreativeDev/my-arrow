import { TestBed, inject } from '@angular/core/testing';
import { WindowRefService } from '@app/core/window/window.service';
import { LiveChatService } from './live-chat.service';

describe('LiveChatService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LiveChatService, WindowRefService],
    });
  });

  it('should be created', inject([LiveChatService], (service: LiveChatService) => {
    expect(service).toBeTruthy();
  }));
});
