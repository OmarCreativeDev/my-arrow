import { FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { IPriceTier } from '@app/shared/components/price-tiers/price-tiers.interface';
import { IProductPriceRequest, IProductPrice } from '@app/core/inventory/product-price.interface';
import { CartDeletionStatus } from '@app/features/cart/components/delete-dialog/delete-dialog.enum';

export interface ICartLineItemCount {
  lineItemCount: number;
  lineItemMax: number;
  remainingLineItems: number;
}

export interface IEndCustomer {
  endCustomerSiteId: number;
  name: string;
}

export interface IEndCustomerRecord {
  cpn: string;
  endCustomers?: IEndCustomer[];
}

// To be replaced by ICheckoutReel in future sprint
export interface IShoppingCartReel {
  itemsPerReel: number;
  numberOfReels: number;
  cost?: number;
  resale?: number;
  margin?: number;
}

// To be replaced by ICheckoutCartItem in future sprint
export interface IShoppingCartItem {
  arrowReel: boolean;
  availableQuantity?: number;
  basePrice?: number;
  bufferQuantity?: number;
  businessCost?: number;
  changed?: boolean;
  description: string;
  docId: string;
  endCustomerRecords?: IEndCustomerRecord[];
  enteredCustomerPartNumber?: string;
  expiryDate?: string;
  expirationTimer?: Subscription;
  hts?: string;
  htsFlag?: boolean;
  id: string;
  image: string;
  inStock: boolean;
  itemId: number;
  itemType?: string;
  lastValidationDate?: string;
  leadTime: string;
  manufacturer: string;
  manufacturerPartNumber: string;
  minimumOrderQuantity?: number;
  multipleOrderQuantity?: number;
  ncnr?: boolean;
  ncnrAccepted?: boolean;
  ncnrAcceptedBy?: string;
  price?: number;
  priceTiers?: IPriceTier[];
  priceTiersMissing?: boolean;
  quantity: number;
  quotable?: boolean;
  quoteHeaderId?: number;
  quoteLineId?: number;
  quoteNumber?: string;
  quotedPrice?: number;
  reel?: IShoppingCartReel;
  requestDate: string;
  selectedCustomerPartNumber?: string;
  selectedEndCustomerSiteId?: number;
  tariffApplicable?: boolean;
  tariffValue?: number;
  total?: number;
  valid?: boolean;
  validation?: ICartValidationStatus;
  quoted?: boolean;
  warehouseCode?: string;
  warehouseId: number;
  originalQuantity?: number;
  isQuantityEdited?: boolean;
}

export interface ICartInfo {
  company: string;
  accountNumber: number;
  billToId: number;
  createdDate: string;
  currency: string;
  id: string;
  status: string;
  terms?: string;
  updatedDate: string;
  userId: string;
}

// To be replaced by ICheckoutCart in future sprint
export interface IShoppingCart extends ICartInfo {
  lineItems: IShoppingCartItem[];
  itemCount: number;
  valid?: boolean;
}

export interface IShoppingCartResponse extends IShoppingCart {
  total?: number;
}

export interface ICartsResponse {
  shoppingCarts: Array<ICartInfo>;
  userId: string;
}

export interface ICartState extends IShoppingCart {
  cartDeletionStatus: CartDeletionStatus;
  error?: Error;
  itemCount: number;
  quotedItemCount: number;
  loading: boolean;
  maxLineItems: number;
  remainingLineItems: number;
  selectedIds?: Array<string>;
  selectedProducts?: Array<ICartSelectedProduct>;
}

export interface ICartStoreField {
  fieldName: string;
  value: any;
  index: number;
}

export interface IPriceTiersRequest {
  itemId: number;
  customerPartNumber?: string;
  endCustomerSiteId?: number;
  quantity?: number;
  reel?: boolean;
  itemsPerReel?: number;
  numberOfReels?: number;
}

export interface IPriceTiersResponse {
  itemId: number;
  customerPartNumber: string;
  endCustomerSiteId: number;
  quantity: number;
  reel: boolean;
  itemsPerReel: number;
  numberOfReels: number;
  total: number;
  priceTiers: IPriceTier[];
}

export interface IShoppingCartReelModalData {
  lineItem?: IShoppingCartItem;
  modalForm?: FormGroup;
  pricePerItem?: number;
  showPriceError?: boolean;
  showReelModal: boolean;
  subtotal?: number;
}

export interface IShoppingCartReelUpdate {
  lineItem: IShoppingCartItem;
  shoppingCartId: string;
}

export interface IShoppingCartReelPatchRequest {
  lineItemId: string;
  price: number;
  quantity: number;
  reel: IShoppingCartReel;
  selectedCustomerPartNumber?: string;
  selectedEndCustomerSiteId?: number;
  shoppingCartId: string;
}

export interface ICartStoreFields {
  fieldName: string;
  value: any;
  indexes: Array<number>;
}

export interface ICartSelectedProduct {
  id: number;
  name: string;
}

// TODO waiting on GL to annotate http://localhost:9093/shoppingcarts/swagger-ui.html#/ShoppingCart/insertLineItemsUsingPOST
// TODO so that it's possible to illustrate mandatory/optional fields in data model below
export interface IAddToCartRequestItem {
  manufacturerPartNumber: string;
  docId: string;
  itemId: number;
  warehouseId: number;
  quantity: number;
  requestDate: string;
  enteredCustomerPartNumber?: string;
  selectedCustomerPartNumber?: string;
  selectedEndCustomerSiteId?: number;
  description?: string;
  manufacturer?: string;
  reel?: {
    itemsPerReel: number;
    numberOfReels: number;
  };
}

export interface IAddToCartRequest {
  billToId: number;
  currency: string;
  lineItems: Array<IAddToCartRequestItem>;
  shoppingCartId: string;
}

export interface IDeleteLineItemsRequest {
  lineItemsIds: Array<string>;
  products?: Array<ICartSelectedProduct>;
  shoppingCartId: string;
}

export interface IDeleteLineItemsResponse {
  id: string;
}

export interface ICompleteLineItemsDeletion {
  deleteRequestId: string;
  products?: Array<ICartSelectedProduct>;
  shoppingCartId: string;
}

export interface IUpdateLineItemsRequest {
  lineItems: Array<IShoppingCartItem>;
  shoppingCartId: string;
}

export interface IShoppingCartPatchRequest {
  shoppingCartId: string;
  currency: string;
  billToId: number;
  validation: string;
  status: ICartStatus;
}

export interface IShoppingCartPatchResponse {
  currency: string;
  billToId: number;
  status: ICartStatus;
}

export enum ICartStatus {
  CLOSED = 'CLOSED',
  IN_PROGRESS = 'IN_PROGRESS',
  ON_HOLD = 'ON_HOLD',
  SUBMITTED = 'SUBMITTED',
}

export enum ICartValidationStatus {
  VALID = 'VALID',
  INVALID = 'INVALID',
  PENDING = 'PENDING',
  QUOTE_EXPIRED = 'QUOTE_EXPIRED',
}

export enum ICartCheckoutValidRegions {
  NA = 'arrowna',
}

export interface IGetCartRequest {
  shoppingCartId: string;
  requestValidation?: boolean;
}

export interface IGetLineItemPriceRequest {
  id: string;
  request: IProductPriceRequest;
}

export interface IGetLineItemPriceResponse {
  id: string;
  response: IProductPrice;
}

export interface IValidatePendingItemsRequest {
  lineItemsIds: string[];
  shoppingCartId: string;
}
