import mockedLineItems from '@app/features/cart/pages/cart-details/cart-model-data-mock';
import { ApiService } from '@app/core/api/api.service';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { CartService } from './cart.service';
import { CoreModule } from '@app/core/core.module';
import { inject, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { ICartsResponse, IDeleteLineItemsResponse } from '@app/core/cart/cart.interfaces';
import { Store } from '@ngrx/store';
import { AuthTokenService } from '@app/core/auth/auth-token.service';

const deleteIds: string[] = [mockedLineItems[0].id];
const mockDeletedItems = mockedLineItems[0];

export class StoreMock {
  public select(): Observable<any> {
    return of({});
  }
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

describe('CartService', () => {
  let apiService: ApiService;
  let cartService: CartService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule],
      providers: [
        CartService,
        AuthTokenService,
        { provide: ApiService, useClass: ApiServiceMock },
        { provide: Store, useClass: StoreMock },
      ],
    });
    apiService = TestBed.get(ApiService);
    cartService = TestBed.get(CartService);
  });

  it('should be created', inject([CartService], (service: CartService) => {
    expect(service).toBeTruthy();
  }));

  it('should get line items count', inject([CartService], (service: CartService) => {
    spyOn(apiService, 'get').and.returnValue(of({ lineItemCount: 100, lineItemMax: 100, remainingLineItems: 0 }));
    service.getLineItemCount('001').subscribe(result => {
      expect(result).toEqual({ lineItemCount: 100, lineItemMax: 100, remainingLineItems: 0 });
    });
  }));

  it('`getShoppingCarts()` should return `ICartsResponse`', () => {
    const cartsResponseMock: ICartsResponse = {
      shoppingCarts: [
        {
          company: 'main account',
          accountNumber: 1,
          billToId: 1,
          createdDate: '',
          currency: '',
          id: '5abd70cf8ef5631815b1b76b',
          status: 'IN_PROGRESS',
          terms: '',
          updatedDate: '',
          userId: 'david.foo@isawesome.com',
        },
      ],
      userId: 'david.foo@isawesome.com',
    };

    spyOn(apiService, 'get').and.returnValue(of(cartsResponseMock));

    cartService.getShoppingCarts().subscribe(response => {
      expect(response.shoppingCarts.length).toBe(1);
      expect(response.shoppingCarts[0].company).toBe(cartsResponseMock.shoppingCarts[0].company);
      expect(response.shoppingCarts[0].billToId).toBe(cartsResponseMock.shoppingCarts[0].billToId);
      expect(response.shoppingCarts[0].id).toBe(cartsResponseMock.shoppingCarts[0].id);
      expect(response.shoppingCarts[0].status).toBe(cartsResponseMock.shoppingCarts[0].status);
      expect(response.shoppingCarts[0].userId).toBe(cartsResponseMock.shoppingCarts[0].userId);
      expect(response.userId).toBe(cartsResponseMock.userId);
    });
  });

  it('should return an Observable<IShoppingCartItem[]>', inject([CartService], (service: CartService) => {
    spyOn(apiService, 'get').and.returnValue(of({ lineItems: mockedLineItems }));
    service.getShoppingCart({ shoppingCartId: '001', requestValidation: true }).subscribe(shoppingCart => {
      const { lineItems } = shoppingCart;
      expect(lineItems.length).toEqual(mockedLineItems.length);
      expect(lineItems[0].manufacturerPartNumber).toEqual(mockedLineItems[0].manufacturerPartNumber);
      expect(lineItems[0].quantity).toEqual(mockedLineItems[0].quantity);
      expect(lineItems[0].requestDate).toEqual(mockedLineItems[0].requestDate);
      expect(lineItems[1].manufacturerPartNumber).toEqual(mockedLineItems[1].manufacturerPartNumber);
      expect(lineItems[1].quantity).toEqual(mockedLineItems[1].quantity);
      expect(lineItems[1].requestDate).toEqual(mockedLineItems[1].requestDate);
    });
  }));

  it('should request line items deletion and return deleteRequestId', inject([CartService], (service: CartService) => {
    const fakeDeleteResponse: IDeleteLineItemsResponse = { id: '123' };
    spyOn(apiService, 'post').and.returnValue(of(fakeDeleteResponse));
    service.requestLineItemsDeletion({ lineItemsIds: deleteIds, shoppingCartId: '001' }).subscribe(deleteResponse => {
      expect(deleteResponse).toEqual(fakeDeleteResponse);
    });
  }));

  it('should complete deletion request and return the delected item', inject([CartService], (service: CartService) => {
    spyOn(apiService, 'delete').and.returnValue(of([mockDeletedItems]));
    service.completeLineItemsDeletion({ deleteRequestId: '123', shoppingCartId: '001' }).subscribe(cartItems => {
      expect(cartItems[0].id).toEqual(deleteIds[0]);
    });
  }));

  it('#updateLineItems', inject([CartService], (service: CartService) => {
    spyOn(apiService, 'put').and.returnValue(of([mockedLineItems[0]]));
    service
      .updateLineItems({
        lineItems: [mockedLineItems[0]],
        shoppingCartId: '0jsjda2131232',
      })
      .subscribe(response => {
        expect(response).toEqual([mockedLineItems[0]]);
      });
    expect(apiService.put).toHaveBeenCalled();
  }));

  it('#validateItems', inject([CartService], (service: CartService) => {
    spyOn(apiService, 'put').and.returnValue(of());
    service.validateItems({ lineItemsIds: [mockedLineItems[0].id], shoppingCartId: '123' }).subscribe(response => {
      expect(response).toBeTruthy();
    });
    expect(apiService.put).toHaveBeenCalled();
  }));
});
