import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { ApiService } from '@app/core/api/api.service';
import {
  IShoppingCartItem,
  ICartLineItemCount,
  ICartsResponse,
  IShoppingCartReelPatchRequest,
  IShoppingCartResponse,
  IUpdateLineItemsRequest,
  IDeleteLineItemsRequest,
  IShoppingCartPatchRequest,
  IGetCartRequest,
  IAddToCartRequest,
  IValidatePendingItemsRequest,
  ICompleteLineItemsDeletion,
  IDeleteLineItemsResponse,
} from './cart.interfaces';
import { environment } from '@env/environment';
import { getAccountNumber, getCompany, getCurrencyCode, getUserBillToAccount } from '@app/core/user/store/user.selectors';
import { IAppState } from '@app/shared/shared.interfaces';

@Injectable()
export class CartService {
  constructor(private apiService: ApiService, private store: Store<IAppState>) {}

  public getLineItemCount(shoppingCartId: string, quoted?: boolean): Observable<ICartLineItemCount> {
    const quotedParam = quoted ? `/?quoted=${quoted}` : '';
    return this.apiService.get<ICartLineItemCount>(
      `${environment.baseUrls.serviceShoppingCart}/${shoppingCartId}/lineItemCount${quotedParam}`
    );
  }

  /**
   * Gets the shopping cart information with line Items
   * @returns {Observable<IShoppingCartResponse>}
   */
  public getShoppingCart(request: IGetCartRequest): Observable<IShoppingCartResponse> {
    return this.apiService.get<IShoppingCartResponse>(
      `${environment.baseUrls.serviceShoppingCart}/${request.shoppingCartId}/?requestValidation=${request.requestValidation}`
    );
  }

  /**
   * Gets all shopping carts by user
   * @returns {Observable<ICartsResponse>}
   */
  public getShoppingCarts(): Observable<ICartsResponse> {
    return this.apiService.get<ICartsResponse>(`${environment.baseUrls.serviceShoppingCart}/`);
  }

  /**
   * Requests the deletion of a list of line items IDs
   * @param deleteLineItemsRequest
   */
  public requestLineItemsDeletion(deleteLineItemsRequest: IDeleteLineItemsRequest): Observable<IDeleteLineItemsResponse> {
    const { lineItemsIds, shoppingCartId } = deleteLineItemsRequest;
    return this.apiService.post<IDeleteLineItemsResponse>(
      `${environment.baseUrls.serviceShoppingCart}/${shoppingCartId}/lineItems/deleteRequest`,
      lineItemsIds
    );
  }

  /**
   * Completes the deletion process of a previously requested line items deletion
   * @param completeLineItemsDeletion
   */
  public completeLineItemsDeletion(completeLineItemsDeletion: ICompleteLineItemsDeletion): Observable<Array<IShoppingCartItem>> {
    const { deleteRequestId, shoppingCartId } = completeLineItemsDeletion;
    return this.apiService.delete<Array<IShoppingCartItem>>(
      `${environment.baseUrls.serviceShoppingCart}/${shoppingCartId}/lineItems/deleteRequest/${deleteRequestId}`
    );
  }

  public updateLineItemReel(reelInfo: IShoppingCartReelPatchRequest): Observable<void> {
    const { lineItemId, price, quantity, reel, selectedCustomerPartNumber, selectedEndCustomerSiteId } = reelInfo;
    return this.apiService.patch<void>(
      `${environment.baseUrls.serviceShoppingCart}/${reelInfo.shoppingCartId}/lineItems/${lineItemId}`,
      {
        price,
        quantity,
        reel,
        selectedCustomerPartNumber,
        selectedEndCustomerSiteId,
      },
      true
    );
  }

  public updateLineItems(
    updateLineItemsRequest: IUpdateLineItemsRequest,
    requestValidation: boolean = false
  ): Observable<Array<IShoppingCartItem>> {
    return this.apiService.put<Array<IShoppingCartItem>>(
      `${environment.baseUrls.serviceShoppingCart}/${
        updateLineItemsRequest.shoppingCartId
      }/lineItems?requestValidation=${requestValidation}`,
      updateLineItemsRequest.lineItems,
      true
    );
  }

  public addLineItems(cartRequestData: IAddToCartRequest) {
    return this.apiService.post<void>(
      `${environment.baseUrls.serviceShoppingCart}/${cartRequestData.shoppingCartId}/lineItems`,
      cartRequestData
    );
  }

  /**
   * Create new shopping cart
   * Invoked only if user does not have a shopping cart
   * @returns {Observable<void>}
   */
  public createCart(): Observable<void> {
    // Get data slices
    const accountNumber$ = this.store.pipe(select(getAccountNumber));
    const billToId$ = this.store.pipe(select(getUserBillToAccount));
    const company$ = this.store.pipe(select(getCompany));
    const currencyCode$ = this.store.pipe(select(getCurrencyCode));

    return combineLatest(accountNumber$, billToId$, company$, currencyCode$).pipe(
      switchMap(results => {
        return this.apiService.post<void>(`${environment.baseUrls.serviceShoppingCart}/`, {
          accountNumber: results[0],
          billToId: results[1],
          company: results[2],
          currency: results[3],
        });
      })
    );
  }

  public partialUpdateCart(request: IShoppingCartPatchRequest) {
    const { shoppingCartId, validation, currency, billToId, status } = request;
    return this.apiService.patch<void>(
      `${environment.baseUrls.serviceShoppingCart}/${shoppingCartId}/`,
      {
        validation,
        currency,
        billToId,
        status,
      },
      true
    );
  }

  public validateItems(request: IValidatePendingItemsRequest): Observable<void> {
    return this.apiService.put<void>(
      `${environment.baseUrls.serviceShoppingCart}/${request.shoppingCartId}/lineItems/validate`,
      request.lineItemsIds,
      true
    );
  }
}
