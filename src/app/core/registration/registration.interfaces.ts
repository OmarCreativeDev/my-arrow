export enum IJobType {
  ENGINEER = 'Engineer',
  PURCHASER = 'Purchaser',
  SUPPLIER = 'Supplier',
}

export enum IRegistrationProcess {
  ACCOUNT_NUMBER = 'ACCOUNT_NUMBER',
  SMR_EMAIL = 'SMR_EMAIL',
  UNKNOWN = 'UNKNOWN',
}

export interface IRegistration {
  firstName: string;
  lastName: string;
  companyName: string;
  email: string;
  phone: string;
  salesRepEmail?: string;
  jobDescription: string;
  billingStreet: string;
  billingCity: string;
  billingState: string;
  billingPostalCode: string;
  billingCountry: string;
  shippingStreet: string;
  shippingCity: string;
  shippingState: string;
  shippingPostalCode: string;
  shippingCountry: string;
  assistanceRequiredBySalesRep: boolean;
  arrowAccountNumber?: string;
  source: string;
  acceptedTermsAndConditions: boolean;
}

export interface IRegistrationState {
  error?: Error | string;
  loading?: boolean;
  loadingEmail?: boolean;
  userExists?: boolean;
  userId?: string;
}
