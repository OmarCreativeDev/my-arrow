import { inject, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';

import { ApiService } from '@app/core/api/api.service';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { CoreModule } from '@app/core/core.module';
import { environment } from '@env/environment';
import { IRegistration } from '@app/core/registration/registration.interfaces';
import { RegistrationService } from '@app/core/registration/registration.service';

const body: IRegistration = {
  firstName: 'Falcon',
  lastName: 'Test',
  companyName: 'Test company',
  email: 'falconpunch@test.com',
  phone: '88993802',
  salesRepEmail: 'wolf@test.com',
  billingStreet: 'Test 123',
  billingCity: 'Test',
  billingState: 'CA',
  billingPostalCode: '20104',
  billingCountry: 'US',
  shippingStreet: 'Test 123',
  shippingCity: 'Test',
  shippingState: 'CA',
  shippingPostalCode: '20104',
  shippingCountry: 'US',
  assistanceRequiredBySalesRep: false,
  arrowAccountNumber: null,
  source: 'OneWeb',
  jobDescription: 'Systems Engineer',
  acceptedTermsAndConditions: true,
};
export class StoreMock {
  public select(): Observable<any> {
    return of({});
  }
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

describe('RegistrationService', () => {
  let apiService: ApiService;
  let registrationService: RegistrationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule],
      providers: [RegistrationService, { provide: ApiService, useClass: ApiServiceMock }, { provide: Store, useClass: StoreMock }],
    });
    apiService = TestBed.get(ApiService);
    registrationService = TestBed.get(RegistrationService);
  });

  it('should be created', inject([RegistrationService], (service: RegistrationService) => {
    expect(service).toBeTruthy();
  }));

  it('should create a submission from registration form', () => {
    spyOn(apiService, 'post');
    registrationService.postRegistration(body);
    expect(apiService.post).toHaveBeenCalledWith(`${environment.baseUrls.serviceSecurity}/account/`, body);
  });

  it('should verify if user exits', () => {
    const bodyParams = {
      email: 'test@test.com',
    };
    spyOn(apiService, 'post');
    registrationService.postUserExists('test@test.com');
    expect(apiService.post).toHaveBeenCalledWith(`${environment.baseUrls.serviceSecurity}/account/email`, bodyParams);
  });
});
