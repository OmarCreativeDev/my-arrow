import { Injectable } from '@angular/core';
import { ApiService } from '@app/core/api/api.service';
import { IRegistration } from './registration.interfaces';
import { environment } from '@env/environment';
import { Observable } from 'rxjs';

@Injectable()
export class RegistrationService {
  constructor(private apiService: ApiService) {}

  public postRegistration(body: IRegistration) {
    return this.apiService.post(`${environment.baseUrls.serviceSecurity}/account/`, body);
  }

  public postUserExists(email: string): Observable<boolean> {
    const bodyParams = {
      email: email,
    };
    return this.apiService.post(`${environment.baseUrls.serviceSecurity}/account/email`, bodyParams);
  }
}
