export enum CertificateNameType {
  nafta = 'nafta',
  coo = 'coo',
  htc = 'htc',
}
export interface IUserInformation {
  companyName: string;
  attentionOf: string;
  address: string;
  city: string;
  state: string;
  postalCode: string;
  countryCode: string;
  email: string;
  taxId: string;
}

export interface ICertificate {
  parts: Array<string>;
  to: string;
  userInformation?: IUserInformation;
  year?: number;
  isrEmail?: string;
  certificateNameType?: CertificateNameType;
}

export interface ICertificateState {
  loading: boolean;
  error?: Error;
  results: any;
  certificate?: ICertificate;
  isModalMessageOpen: boolean;
}
