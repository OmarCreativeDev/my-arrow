import { TestBed } from '@angular/core/testing';
import { GenerateCertificateService } from '@app/core/generate-certificate/generate-certificate.service';
import { ApiService } from '@app/core/api/api.service';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { CertificateNameType } from '@app/core/generate-certificate/generate-certificate.interface';

describe('GenerateCertificateService', () => {

  let generateCertificateService: GenerateCertificateService;
  let apiService: ApiService;

  beforeEach(() => {

    TestBed.configureTestingModule({
      providers: [
        GenerateCertificateService,
        { provide: ApiService, useClass: ApiServiceMock },
      ],
    });

    generateCertificateService = TestBed.get(GenerateCertificateService);
    apiService = TestBed.get(ApiService);

  });

  it('should be created', () => {
    expect(generateCertificateService).toBeTruthy();
  });

  it ('should use the ApiService to post the certificate', () => {
    spyOn(apiService, 'postBase64String');
    generateCertificateService.generate({certificateNameType: CertificateNameType.htc, parts: [], to: ''});
    expect(apiService.postBase64String).toHaveBeenCalled();
  });

  it ('should use the ApiService to get user information', () => {
    spyOn(apiService, 'get');
    generateCertificateService.getUserInformation();
    expect(apiService.get).toHaveBeenCalled();
  });

});
