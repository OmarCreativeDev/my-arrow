import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { ApiService } from '../api/api.service';

import { ICertificate } from './generate-certificate.interface';

@Injectable()
export class GenerateCertificateService {
  constructor(private apiService: ApiService) {}

  public generate(certificate: ICertificate): Observable<any> {
    const { certificateNameType, ...body } = certificate;
    return this.apiService.postBase64String(
      `${environment.baseUrls.serviceTradeCompliance}/certificates/email/${certificateNameType}/`,
      body
    );
  }

  public getUserInformation(): Observable<any> {
    return this.apiService.get(`${environment.baseUrls.serviceTradeCompliance}/userInformation/`);
  }
}
