import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { cloneDeep } from 'lodash-es';

import { CoreModule } from '../core.module';
import { LoginGuard } from './login.guard';
import { AuthTokenService } from '../auth/auth-token.service';
import { Store, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { mockPrivateProperties } from '@app/core/features/feature-flags.mock';
import { propertiesReducers } from '@app/features/properties/store/properties.reducers';
import { PropertiesEffects } from '@app/features/properties/store/properties.effects';
import { GetPrivatePropertiesSuccess } from '@app/features/properties/store/properties.actions';
import { PropertiesModule } from '@app/features/properties/properties.module';

class DummyComponent {}

describe('LoginGuard', () => {
  const mockSnapshot: RouterStateSnapshot = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);

  const routes = [{ path: 'dashboard', component: DummyComponent }];

  let loginGuard: LoginGuard;
  let authTokenService: AuthTokenService;
  let store: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        PropertiesModule,
        RouterTestingModule.withRoutes(routes),
        StoreModule.forRoot({
          properties: propertiesReducers,
        }),
        EffectsModule.forRoot([PropertiesEffects]),
      ],
      providers: [LoginGuard],
    });
    authTokenService = TestBed.get(AuthTokenService);
    loginGuard = TestBed.get(LoginGuard);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(loginGuard).toBeTruthy();
  });

  it('should redirect to the dashboard if access token is still active', () => {
    const mockProperties = cloneDeep(mockPrivateProperties);
    store.dispatch(new GetPrivatePropertiesSuccess(mockProperties));
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue('token');
    expect(loginGuard.canActivate(new ActivatedRouteSnapshot(), mockSnapshot)).toBeFalsy();
  });
});
