import { Injectable, Injector } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { GetPublicProperties } from '@app/features/properties/store/properties.actions';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';
import { first } from 'rxjs/operators';
import { isEmpty } from 'lodash-es';

@Injectable()
export class LoginGuard {
  constructor(private injector: Injector, private router: Router, private store: Store<IAppState>) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.fetchFeatureFlags();
    return this.redirectIfAuthValid();
  }

  public fetchFeatureFlags() {
    this.store
      .pipe(
        select(getPrivateFeatureFlagsSelector),
        first()
      )
      .subscribe(featureFlags => {
        if (isEmpty(featureFlags)) {
          this.store.dispatch(new GetPublicProperties());
        }
      });
  }

  public redirectIfAuthValid(): boolean {
    const authTokenService = this.injector.get(AuthTokenService);
    if (authTokenService.getValidAccessToken()) {
      this.router.navigateByUrl('dashboard');
      return false;
    }
    return true;
  }
}
