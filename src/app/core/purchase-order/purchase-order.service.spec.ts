import { TestBed, inject } from '@angular/core/testing';

import { PurchaseOrderService } from '@app/core/purchase-order/purchase-order.service';

import { CoreModule } from '@app/core/core.module';

describe('PurchaseOrderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule],
      providers: [PurchaseOrderService],
    });
  });

  it('should be created', inject([PurchaseOrderService], (service: PurchaseOrderService) => {
    expect(service).toBeTruthy();
  }));
});
