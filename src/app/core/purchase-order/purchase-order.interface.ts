export type POUploadType = 'MANUAL' | 'CONEXIOM';

export interface IPOUpload {
  userEmail: string;
  region: string;
  uploadType: POUploadType;
  poNumber: string;
}
