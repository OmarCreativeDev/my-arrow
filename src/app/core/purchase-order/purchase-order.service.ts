import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from '../api/api.service';

import { environment } from '@env/environment';

@Injectable()
export class PurchaseOrderService {
  constructor(private apiService: ApiService) {}

  public uploadPO(params: FormData): Observable<any> {
    return this.apiService.post<any>(`${environment.baseUrls.servicePOUpload}`, params, false);
  }
}
