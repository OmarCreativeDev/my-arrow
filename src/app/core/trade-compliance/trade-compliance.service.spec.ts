import { TestBed, inject } from '@angular/core/testing';
import { of } from 'rxjs';
import { ApiService } from '@app/core/api/api.service';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { TradeComplianceService } from './trade-compliance.service';
import { CertificateType } from './trade-compliance.interfaces';

const mockSearchResults: any = {
  NMC1210NPO472J50TRPLPF: [
    {
      partNumber: 'NMC1210NPO472J50TRPLPF',
    },
  ],
};

describe('TradeComplianceService', () => {
  let apiService: ApiService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [TradeComplianceService, { provide: ApiService, useClass: ApiServiceMock }],
    });
    apiService = TestBed.get(ApiService);
  });

  it('should be created', inject([TradeComplianceService], (service: TradeComplianceService) => {
    expect(service).toBeTruthy();
  }));

  it('should return an Observable<ITradeComplianceSearchResults>', inject([TradeComplianceService], (service: TradeComplianceService) => {
    service.setData('parts', 'NMC1210NPO472J50TRPLPF');
    spyOn(apiService, 'post').and.returnValue(of(mockSearchResults));
    service.getSearchResults().subscribe(parts => {
      expect(parts['NMC1210NPO472J50TRPLPF'][0].partNumber).toEqual('NMC1210NPO472J50TRPLPF');
    });
  }));

  it('should return nafta, coo or htc according to each filter', inject([TradeComplianceService], (service: TradeComplianceService) => {
    expect(service.getEndpoint(0)).toBe(CertificateType[0]);
    expect(service.getEndpoint(1)).toBe(CertificateType[1]);
    expect(service.getEndpoint(2)).toBe(CertificateType[2]);
  }));

  it('should return fields values when calling `getData`', inject([TradeComplianceService], (service: TradeComplianceService) => {
    const parts = '009,020,090';
    service.setData('parts', parts);
    expect(service.getData('parts')).toBe(parts);
  }));
});
