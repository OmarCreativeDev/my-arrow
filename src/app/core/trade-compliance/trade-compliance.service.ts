import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { ApiService } from '../api/api.service';
import { CertificateType, ITradeComplianceSearchResults } from './trade-compliance.interfaces';

@Injectable()
export class TradeComplianceService {
  public parts: string;
  public certificateType: number;
  public year: string;

  constructor(private apiService: ApiService) {}

  public getSearchResults(): Observable<ITradeComplianceSearchResults> {
    const parts = this.parts.replace(/ /g, '');
    const formattedParts = parts.split(',');
    const body = {
      year: this.year,
      parts: formattedParts,
    };

    return this.apiService.post<ITradeComplianceSearchResults>(
      `${environment.baseUrls.serviceTradeCompliance}/certificates/${this.getEndpoint(this.certificateType)}/`,
      body
    );
  }

  getEndpoint(certificateType) {
    switch (certificateType) {
      case CertificateType.nafta:
        return 'nafta';
      case CertificateType.coo:
        return 'coo';
      case CertificateType.htc:
        return 'htc';
    }
  }

  setData(field, data) {
    this[field] = data;
  }

  getData(field) {
    return this[field];
  }
}
