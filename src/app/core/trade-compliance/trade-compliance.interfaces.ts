export enum CertificateType {
  nafta = 0,
  coo = 1,
  htc = 2,
}

export enum CertificateTypeNames {
  nafta = 'NAFTA',
  coo = 'COO',
  htc = 'HTC',
}

export interface ITradeComplianceSearchType {
  label: string;
  value: number;
}

export interface ITradeComplianceSearchState<ResultType> {
  loading: boolean;
  error?: Error;
  results: ResultType;
  searchType: ITradeComplianceSearchType;
  naftaYear?: number;
  partNumbersFound?: Array<INAFTAPart | ICOOPart | IHTCPart>;
  partNumbersNotFound?: string[];
  cooPartNumbersFound?: Array<ICOOPart>;
}

export interface IPart {
  partNumber: string;
  checked?: boolean;
  id?: string;
  itemType?: string;
}

export interface INAFTAPart extends IPart {
  producer?: string;
  partDescription?: string;
  harmonizedTariffCode?: string;
  countryOfOrigin?: string;
}

export interface ICOOPart extends IPart {
  manufacturer?: string;
  countryOfOrigin?: string;
}

export interface IHTCPart extends IPart {
  partDescription?: string;
  manufacturer?: string;
  htsCode?: string;
  htsDescription?: string;
  htsCountry?: string;
  eccn?: string;
}

export interface ITradeComplianceSearchResults {
  [key: string]: Array<INAFTAPart | ICOOPart | IHTCPart>;
}

export interface ICheckPartNumber {
  index: number;
  checked: boolean;
}
