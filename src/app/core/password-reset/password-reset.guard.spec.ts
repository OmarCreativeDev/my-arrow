import { PasswordResetGuard } from '@app/core/password-reset/password-reset.guard';
import { TestBed, inject } from '@angular/core/testing';
import { CoreModule } from '@app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Store, StoreModule } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';
import { AuthService } from '@app/core/auth/auth.service';
import { of, throwError } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';

class DummyComponent {}

class MockActivatedRouteSnapshot {
  private _queryParams: any = {
    rid: '5b8458d030b4670001129b93',
    key: '17097b79-275d-4661-9043-b7e94407bc11',
  };
  get queryParams() {
    return this._queryParams;
  }
}

describe('PasswordResetGuard', () => {
  const routes = [{ path: 'password-reset', component: DummyComponent }];
  const mockSnapshot: any = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);
  let passwordResetGuard: PasswordResetGuard;
  let authService: AuthService;
  let store: Store<any>;
  let route: ActivatedRouteSnapshot;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({ user: userReducers }), CoreModule, RouterTestingModule.withRoutes(routes)],
      providers: [
        PasswordResetGuard,
        AuthService,
        {
          provide: ActivatedRouteSnapshot,
          useClass: MockActivatedRouteSnapshot,
        },
        { provide: WSSService, useClass: MockStompWSSService },
      ],
    });
    passwordResetGuard = TestBed.get(PasswordResetGuard);
    authService = TestBed.get(AuthService);
    store = TestBed.get(Store);
    route = TestBed.get(ActivatedRouteSnapshot);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(passwordResetGuard).toBeTruthy();
  });

  it('should allow routing to password-reset', () => {
    spyOn(authService, 'validateResetToken').and.returnValue(of([]));
    passwordResetGuard.canActivate(route, mockSnapshot).subscribe(result => expect(result).toBeTruthy());
  });

  it('should redirect to link-expired when status error 403 is resturned', inject([Router], (router: Router) => {
    const spy = spyOn(router, 'navigateByUrl');
    spyOn(authService, 'validateResetToken').and.returnValue(throwError(new HttpErrorResponse({ status: 403 })));
    passwordResetGuard.canActivate(route, mockSnapshot).subscribe(result => {
      expect(result).toBeFalsy();
      expect(spy.calls.first().args[0]).toBe('link-expired');
    });
  }));

  it('should redirect to request-error when status error 500 is resturned', inject([Router], (router: Router) => {
    const spy = spyOn(router, 'navigateByUrl');
    spyOn(authService, 'validateResetToken').and.returnValue(throwError(new HttpErrorResponse({ status: 500 })));
    passwordResetGuard.canActivate(route, mockSnapshot).subscribe(result => {
      expect(result).toBeFalsy();
      expect(spy.calls.first().args[0]).toBe('request-error');
    });
  }));
});
