import { Injectable, Injector } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from '@app/core/auth/auth.service';
import { map, catchError } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable()
export class PasswordResetGuard {
  constructor(private injector: Injector, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    const authService = this.injector.get(AuthService);
    const resetId = route.queryParams.rid;
    const key = route.queryParams.key;
    return authService.validateResetToken(resetId, key).pipe(
      map(e => {
        return true;
      }),
      catchError(exception => {
        if (exception.status === 403) {
          this.router.navigateByUrl('link-expired');
        } else {
          this.router.navigateByUrl('request-error');
        }
        return of(false);
      })
    );
  }
}
