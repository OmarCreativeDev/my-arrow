export interface IReelState {
  noOfReels: number;
  partsInReel: number;
  customerPartNumber: string;
  endCustomerSiteId: number;
  calculatedPrice: number;
  error: Error;
  loading: boolean;
  tariffValue?: number;
}

export interface IReelPriceRequest {
  billToId: number;
  cpn?: string;
  currency: string;
  endCustomerSiteId?: number;
  itemId: number;
  noOfReels: number;
  partsInReel: number;
  warehouseId: number;
}

export interface IReelPriceResponse {
  price: number;
  tariffValue?: number;
}

export interface IReelPrice {
  customerPartNumber?: string;
  tariffValue?: number;
  endCustomerSiteId?: number;
  noOfReels: number;
  partsInReel: number;
  totalParts: number;
  price: {
    unit: number;
    total: number;
  };
}

export interface IOpenReeelModalRequest {
  url: string;
}
