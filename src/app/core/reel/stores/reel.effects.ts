import { Injectable } from '@angular/core';
import { InventoryService } from '@app/core/inventory/inventory.service';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { ReelActionTypes, RequestReelPrice, RequestReelPriceFailed, RequestReelPriceSuccess } from './reel.actions';

@Injectable()
export class ReelEffects {
  constructor(private actions$: Actions, private inventoryService: InventoryService) {}

  /**
   * Side-effect that combines all side effects for reel price request.
   */
  @Effect()
  requestCartItemReelPrice$ = this.actions$.pipe(
    ofType(ReelActionTypes.REEL_REQUEST_PRICE),
    switchMap((action: RequestReelPrice) => {
      return this.inventoryService.getReelPrice(action.payload).pipe(
        map(response => {
          const rawPrice = response.price;
          const rawTariffValue = response.tariffValue;
          const price = isNaN(rawPrice) ? undefined : typeof rawPrice === 'string' ? parseFloat(rawPrice) : rawPrice;
          const tarrifValue = isNaN(rawTariffValue)
            ? undefined
            : typeof rawTariffValue === 'string'
            ? parseFloat(rawTariffValue)
            : rawTariffValue;

          return new RequestReelPriceSuccess({ price: price, tariffValue: tarrifValue });
        }),
        catchError(err => of(new RequestReelPriceFailed(err)))
      );
    })
  );
}
