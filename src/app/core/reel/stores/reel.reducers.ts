import { IReelState } from '@app/core/reel/reel.interfaces';
import { ReelActions, ReelActionTypes } from './reel.actions';

export const INITIAL_REEL_STATE: IReelState = {
  noOfReels: undefined,
  partsInReel: undefined,
  customerPartNumber: undefined,
  endCustomerSiteId: undefined,
  calculatedPrice: undefined,
  error: undefined,
  loading: undefined,
};

export function reelReducers(state: IReelState = INITIAL_REEL_STATE, action: ReelActions): IReelState {
  switch (action.type) {
    case ReelActionTypes.REEL_REQUEST_PRICE: {
      const { cpn, noOfReels, partsInReel, endCustomerSiteId } = action.payload;

      return {
        ...state,
        noOfReels,
        partsInReel,
        customerPartNumber: cpn ? cpn : undefined,
        endCustomerSiteId: endCustomerSiteId ? endCustomerSiteId : undefined,
        calculatedPrice: undefined,
        error: undefined,
        loading: true,
      };
    }

    case ReelActionTypes.REEL_REQUEST_PRICE_SUCCESS: {
      const price = action.payload.price;
      const tarrifValue = action.payload.tariffValue;
      return {
        ...state,
        calculatedPrice: price,
        tariffValue: tarrifValue,
        error: undefined,
        loading: undefined,
      };
    }

    case ReelActionTypes.REEL_REQUEST_PRICE_FAILED: {
      return {
        ...state,
        calculatedPrice: undefined,
        error: action.payload,
        loading: undefined,
      };
    }

    case ReelActionTypes.REEL_CLEAR_PRICE: {
      return {
        ...state,
        calculatedPrice: undefined,
      };
    }

    case ReelActionTypes.REEL_CLEAR_STATE: {
      return {
        ...INITIAL_REEL_STATE,
      };
    }

    default: {
      return state;
    }
  }
}
