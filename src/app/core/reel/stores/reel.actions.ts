import { Action } from '@ngrx/store';
import { IReelPriceRequest, IReelPriceResponse, IOpenReeelModalRequest } from '@app/core/reel/reel.interfaces';

export enum ReelActionTypes {
  REEL_CLEAR_STATE = 'REEL_CLEAR_STATE',
  REEL_CLEAR_PRICE = 'REEL_CLEAR_PRICE',
  REEL_REQUEST_PRICE = 'REEL_REQUEST_PRICE',
  REEL_REQUEST_PRICE_FAILED = 'REEL_REQUEST_PRICE_FAILED',
  REEL_REQUEST_PRICE_SUCCESS = 'REEL_REQUEST_PRICE_SUCCESS',
  REEL_OPEN_MODAL = 'REEL_OPEN_MODAL',
}

/**
 * Request reel price.
 */
export class RequestReelPrice implements Action {
  readonly type = ReelActionTypes.REEL_REQUEST_PRICE;
  constructor(public payload: IReelPriceRequest) {}
}

/**
 * Request reel price failed.
 */
export class RequestReelPriceFailed implements Action {
  readonly type = ReelActionTypes.REEL_REQUEST_PRICE_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Request reel price success.
 */
export class RequestReelPriceSuccess implements Action {
  readonly type = ReelActionTypes.REEL_REQUEST_PRICE_SUCCESS;
  constructor(public payload: IReelPriceResponse) {}
}

/**
 * Clear reel state
 */
export class ClearReelState implements Action {
  readonly type = ReelActionTypes.REEL_CLEAR_STATE;
}

export class ClearReelPrice implements Action {
  readonly type = ReelActionTypes.REEL_CLEAR_PRICE;
}

export class OpenReelModal implements Action {
  readonly type = ReelActionTypes.REEL_OPEN_MODAL;
  constructor(public payload: IOpenReeelModalRequest) {}
}

export type ReelActions =
  | RequestReelPrice
  | RequestReelPriceFailed
  | RequestReelPriceSuccess
  | ClearReelState
  | ClearReelPrice
  | OpenReelModal;
