import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IReelState, IReelPrice } from '@app/core/reel/reel.interfaces';
import * as math from 'mathjs';

/**
 * Selects cart state from the root state object
 */
export const getReelState = createFeatureSelector<IReelState>('reel');

/**
 * Get Reel Price
 */
export const getReelPrice = createSelector(getReelState, reelState => {
  const { noOfReels, partsInReel, calculatedPrice, customerPartNumber, endCustomerSiteId, tariffValue } = reelState;

  if (calculatedPrice) {
    const perUnit =
      noOfReels && partsInReel
        ? math
            .chain(reelState.calculatedPrice)
            .divide(math.multiply(reelState.partsInReel, reelState.noOfReels))
            .done()
        : undefined;

    return {
      noOfReels,
      partsInReel,
      customerPartNumber,
      endCustomerSiteId,
      totalParts: noOfReels * partsInReel,
      price: {
        unit: perUnit,
        total: calculatedPrice,
      },
      tariffValue: tariffValue,
    } as IReelPrice;
  }
});

/**
 * Get reel calculating status
 */
export const getReelLoadingStatus = createSelector(getReelState, reelState => reelState.loading);

/**
 * Get reel error message
 */
export const getReelError = createSelector(getReelState, reelState => reelState.error);
