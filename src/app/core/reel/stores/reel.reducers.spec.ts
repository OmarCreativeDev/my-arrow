import { IReelState, IReelPriceResponse, IReelPriceRequest } from '@app/core/reel/reel.interfaces';
import { INITIAL_REEL_STATE, reelReducers } from '@app/core/reel/stores/reel.reducers';
import { RequestReelPrice, RequestReelPriceFailed, RequestReelPriceSuccess, ClearReelState } from '@app/core/reel/stores/reel.actions';
import { cloneDeep } from 'lodash-es';

describe('Shopping cart Reducers', () => {
  let initialState: IReelState;

  beforeEach(() => {
    initialState = INITIAL_REEL_STATE;
  });

  it(`REEL_REQUEST_PRICE should set loading in true`, () => {
    const request: IReelPriceRequest = {
      billToId: 123456,
      currency: 'USD',
      itemId: 34567,
      noOfReels: 12,
      partsInReel: 6599,
      warehouseId: 9807,
    };
    const action = new RequestReelPrice(request);
    const result = reelReducers(initialState, action);
    expect(result.loading).toBeTruthy();
  });

  it(`REEL_REQUEST_PRICE_FAILED should have an error property`, () => {
    const error = new Error('foo');
    const action = new RequestReelPriceFailed(error);
    const result = reelReducers(initialState, action);
    expect(result.error).toBeDefined();
  });

  it(`REEL_REQUEST_PRICE_SUCCESS should return the price for the reel parameters`, () => {
    const clonedInitialState = cloneDeep(initialState);
    const price = 123;
    const reelPriceResponse: IReelPriceResponse = {
      price,
    };

    const action = new RequestReelPriceSuccess(reelPriceResponse);
    const result = reelReducers(clonedInitialState, action);

    expect(result.calculatedPrice).toEqual(price);
    expect(result.error).toBeUndefined();
    expect(result.loading).toBeUndefined();
  });

  it(`REEL_CLEAR_STATE should clean the state`, () => {
    const action = new ClearReelState();
    const result = reelReducers(initialState, action);
    expect(result.calculatedPrice).toBeUndefined();
    expect(result.error).toBeUndefined();
    expect(result.loading).toBeUndefined();
  });
});
