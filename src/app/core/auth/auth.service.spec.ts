import { TestBed, inject, fakeAsync } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as jwt from 'jsrsasign';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClient } from '@angular/common/http';

import { CoreModule } from '../../core/core.module';
import { AuthService } from './auth.service';
import { AuthTokenService } from './auth-token.service';
import { of, throwError } from 'rxjs';
import { userReducers } from '@app/core/user/store/user.reducers';
import { StoreModule, Store } from '@ngrx/store';
import { UserService } from '@app/core/user/user.service';
import { ResetUser, RequestUserComplete } from '@app/core/user/store/user.actions';
import { certificatePrivateKey } from './mockCertificates.js';
import { ClientId } from '@app/core/user/user.interface';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';
import { ApiService } from '@app/core/api/api.service';
import { mockCertificateResponse, foreverCurrentAccessToken } from './auth-token.service.spec';
import { mockUser } from '../user/user.service.spec';
import { IAuthResult } from './auth.interface';

class DummyComponent {}

describe('AuthService', () => {
  let apiService: ApiService;
  let store: Store<any>;

  const mockTokenHeader = {
    alg: 'RS256',
    typ: 'JWT',
  };

  const mockTokenPayload = {
    user_name: 'david',
    scope: ['read', 'write'],
    exp: 3100657707,
    authorities: ['ROLE_USER', 'ROLE_ADMIN'],
    ati: 'a9448f77-98f1-4061-a934-8f7895c4c176',
    jti: 'a9448f77-98f1-4061-a934-8f7895c4c146',
    email: 'david.sane@onxorems.com',
    client_id: ClientId.MYARROWUI,
  };

  const foreverCurrentRefreshToken = jwt.KJUR.jws.JWS.sign(
    null,
    JSON.stringify(mockTokenHeader),
    JSON.stringify(mockTokenPayload),
    certificatePrivateKey
  );

  const routes = [{ path: 'login', component: DummyComponent }, { path: 'dashboard', component: DummyComponent }];

  let authService: AuthService;
  let authTokenService: AuthTokenService;
  let http: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({ user: userReducers }), HttpClientTestingModule, RouterTestingModule.withRoutes(routes), CoreModule],
      providers: [AuthService, UserService, { provide: WSSService, useClass: MockStompWSSService }],
    });

    store = TestBed.get(Store);
    authService = TestBed.get(AuthService);
    apiService = TestBed.get(ApiService);
    authTokenService = TestBed.get(AuthTokenService);
    http = TestBed.get(HttpTestingController);
    httpClient = TestBed.get(HttpClient);

    spyOn(authTokenService, 'setTokens');
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(authService).toBeTruthy();
  });

  it('should login (authenticate user and set token)', () => {
    spyOn(authService, 'authenticate').and.returnValue(of(foreverCurrentAccessToken));
    spyOn(httpClient, 'get').and.returnValue(of(mockCertificateResponse));

    authService.login('username', 'password').subscribe(() => {
      expect(authTokenService.setTokens).toHaveBeenCalled();
    });
  });

  it('should set correct headers when authenticating user', () => {
    authService.SERVICE_URL = '/mock-service-url';

    authService.authenticate('username', 'password', 'clientId', 'accessId').subscribe(() => {
      expect(authTokenService.setTokens).toHaveBeenCalled();
    });

    http.expectOne(req => {
      return (
        req.url === '/mock-service-url' && req.method === 'POST' && req.headers.get('Content-Type') === 'application/x-www-form-urlencoded'
      );
    });
  });

  it('should dispath ResetUser() action on logout', inject(
    [AuthService, AuthTokenService, WSSService],
    (service: AuthService, authToken: AuthTokenService) => {
      spyOn(httpClient, 'delete').and.returnValue(of({}));
      const authTokenSpy = spyOn(authToken, 'resetLocalStorage');
      service.logout();
      expect(authTokenSpy).toHaveBeenCalled();
      expect(store.dispatch).toHaveBeenCalledWith(new ResetUser());
    }
  ));

  it('should dispath ResetUser() action on logout error', inject(
    [AuthService, AuthTokenService, WSSService],
    (service: AuthService, authToken: AuthTokenService) => {
      spyOn(httpClient, 'delete').and.returnValue(throwError(new Error()));
      const authTokenSpy = spyOn(authToken, 'resetLocalStorage');
      service.logout();
      expect(authTokenSpy).toHaveBeenCalled();
      expect(store.dispatch).toHaveBeenCalledWith(new ResetUser());
    }
  ));

  it('should request new tokens when refresh token is valid', () => {
    authService.SERVICE_URL = '/mock-service-url';

    authService.requestNewTokens(foreverCurrentRefreshToken).subscribe();

    http.expectOne(req => {
      return (
        req.url === '/mock-service-url' && req.method === 'POST' && req.headers.get('Content-Type') === 'application/x-www-form-urlencoded'
      );
    });
  });

  it('should request And Set Tokens In LocalStorage when refresh token is valid', fakeAsync(() => {
    authService.SERVICE_URL = '/mock-service-url';

    authService.requestAndSetTokensInLocalStorage().subscribe(() => {
      expect(authTokenService.setTokens).toHaveBeenCalled();
      expect(store.dispatch).toHaveBeenCalledWith(new ResetUser());
    });

    http.expectOne(req => {
      return (
        req.url === '/mock-service-url' && req.method === 'POST' && req.headers.get('Content-Type') === 'application/x-www-form-urlencoded'
      );
    });
  }));

  it('should request And Set Tokens In LocalStorage when access token is valid', fakeAsync(() => {
    spyOn(authService, 'requestNewTokens').and.returnValue(of(foreverCurrentAccessToken));
    authService.requestAndSetTokensInLocalStorage().subscribe(() => {
      expect(authTokenService.setTokens).toHaveBeenCalled();
    });
  }));

  it('should return null on checkAndRenewTokens() if both access and refresh tokens are valid', fakeAsync(() => {
    authService.SERVICE_URL = '/mock-service-url';
    authService.checkAndRenewTokens().subscribe(response => {
      expect(response).toBeNull();
    });
  }));

  it('should return null on checkAndRenewTokens() if access token is invalid and refresh tokens is valid', fakeAsync(() => {
    authService.SERVICE_URL = '/mock-service-url';
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue(null);
    spyOn(authTokenService, 'getValidRefreshToken').and.returnValue(foreverCurrentRefreshToken);
    spyOn(httpClient, 'post').and.returnValue(of({ access_token: foreverCurrentAccessToken }));

    authService.checkAndRenewTokens().subscribe((response: IAuthResult) => {
      expect(response.access_token).toBe(foreverCurrentAccessToken);
    });
  }));

  it('should request the Revoke tokens when logout', fakeAsync(() => {
    authService.REVOKE_TOKEN_URL = '/mock-service-url';

    authService.revokeRefreshToken(foreverCurrentRefreshToken).subscribe();

    http.expectOne(req => {
      return (
        req.url === '/mock-service-url' &&
        req.method === 'DELETE' &&
        req.headers.get('Content-Type') === 'application/x-www-form-urlencoded'
      );
    });
  }));

  it('should request token validation', () => {
    spyOn(apiService, 'get').and.returnValue(of([]));
    authService.validateResetToken('1234567890', '123a4567-c89d-42d3-c456-223342110000').subscribe(response => {
      expect(response).toBeDefined();
    });
  });

  it('should recoverPassword()', () => {
    spyOn(apiService, 'post').and.returnValue(of([]));
    authService.recoverPassword('email@email.com').subscribe(response => {
      expect(response).toBeDefined();
    });
  });

  it('should resetPassword()', () => {
    spyOn(apiService, 'post').and.returnValue(of([]));
    authService.resetPassword('1234567890', '123a4567-c89d-42d3-c456-223342110000', 'Pwd12345').subscribe(response => {
      expect(response).toBeDefined();
    });
  });

  it('should unsubscribe ngOnDestroy()', () => {
    const unsubSpy = spyOn(authService.subscription, 'unsubscribe');
    authService.ngOnDestroy();
    expect(unsubSpy).toHaveBeenCalled();
  });

  it('should start and set token timer on user login', () => {
    jasmine.clock().uninstall();
    jasmine.clock().install();
    const fakeTime = new Date().getTime() + 100;
    spyOn(authTokenService, 'getExpiresAccessTime').and.returnValue(fakeTime);
    spyOn(authService, 'requestAndSetTokensInLocalStorage').and.returnValue(of({}));
    store.dispatch(new RequestUserComplete(mockUser));
    jasmine.clock().tick(150);
    expect(authService.tokenInterval).toBeDefined();
    jasmine.clock().uninstall();
  });

  it('should NOT start token timer if token is expired already', () => {
    jasmine.clock().uninstall();
    jasmine.clock().install();
    const fakeTime = new Date().getTime() - 100;
    spyOn(authTokenService, 'getExpiresAccessTime').and.returnValue(fakeTime);
    store.dispatch(new RequestUserComplete(mockUser));
    jasmine.clock().tick(150);
    expect(authService.tokenInterval).toBeUndefined();
    jasmine.clock().uninstall();
  });
});
