import { camelCase, mapKeys } from 'lodash-es';
import * as jwt from 'jsrsasign';

import { Injectable } from '@angular/core';
import { IAuthResult, IDecodedToken, ITokenHeader, IToken, IPublicCertificate } from './auth.interface';
import { CLIENT_ID } from '@env/environment';

@Injectable()
export class AuthTokenService {
  constructor() {}

  private ACCESS_TOKEN_STORAGE_KEY = 'accessToken';
  private EXPIRES_AT_STORAGE_KEY = 'accessExpiresAt';
  private REFRESH_TOKEN_STORAGE_KEY = 'refreshToken';
  private EXPIRES_AT_REFRESH_KEY = 'refreshExpiresAt';
  private CLIENT_ID_KEY = 'clientId';
  public PUBLIC_CERTIFICATE_KEY = 'publicCertificate';
  public PUBLIC_CERTIFICATE_ALG = 'publicCertificateAlg';
  public TOKEN_REQUEST_TIME = 'tokenRequestTime';

  public getAccessToken() {
    return localStorage.getItem(this.ACCESS_TOKEN_STORAGE_KEY);
  }

  public getRefreshToken() {
    return localStorage.getItem(this.REFRESH_TOKEN_STORAGE_KEY);
  }

  public getClientId() {
    return localStorage.getItem(this.CLIENT_ID_KEY) || CLIENT_ID;
  }

  public isValidToken(token: string) {
    if (!token) {
      return false;
    }
    if (!this.getPublicCertificate()) {
      return false;
    }
    const pubkey = jwt.KEYUTIL.getKey(this.getPublicCertificate());
    return jwt.KJUR.jws.JWS.verifyJWT(token, pubkey, {
      alg: ['RS256', this.getPublicCertificateAlg()],
    });
  }

  /**
   * Gets the value of the tokens unless it has expired (in which case return null)
   */
  public getValidAccessToken() {
    const accessToken = this.getAccessToken();
    return this.isValidToken(accessToken) ? accessToken : null;
  }

  public getValidRefreshToken() {
    const refreshToken = this.getRefreshToken();
    return this.isValidToken(refreshToken) ? refreshToken : null;
  }

  public getPublicCertificate() {
    return localStorage.getItem(this.PUBLIC_CERTIFICATE_KEY) || null;
  }

  public getPublicCertificateAlg() {
    return localStorage.getItem(this.PUBLIC_CERTIFICATE_ALG) || null;
  }

  public getTokenRequestTime() {
    return localStorage.getItem(this.TOKEN_REQUEST_TIME) || null;
  }

  /**
   * Transform the value obtained from the parameter into its camelCase version
   */

  private camelCaseObj(obj: object) {
    return mapKeys(obj, (value: string, key: string) => camelCase(key));
  }

  /**
   * Decodes the token given as a parameter, with the formated version
   */

  public decodeToken(token: string): IDecodedToken {
    const jwtParse = jwt.KJUR.jws.JWS.parse;
    const { payloadObj, headerObj } = jwtParse(token);

    const formattedToken = <IToken>this.camelCaseObj(payloadObj);
    const formattedHeader = <ITokenHeader>this.camelCaseObj(headerObj);

    return {
      token: formattedToken,
      header: formattedHeader,
    };
  }

  /**
   * Stores access token in local storage
   * @param token
   */
  public setTokens(authResult: IAuthResult) {
    const decodedRefreshToken = this.decodeToken(authResult.refresh_token);

    localStorage.setItem(this.ACCESS_TOKEN_STORAGE_KEY, authResult.access_token);
    localStorage.setItem(this.EXPIRES_AT_STORAGE_KEY, this.handleExpirationTime(authResult));
    localStorage.setItem(this.REFRESH_TOKEN_STORAGE_KEY, authResult.refresh_token);
    localStorage.setItem(this.EXPIRES_AT_REFRESH_KEY, JSON.stringify(decodedRefreshToken.token.exp * 1000));
  }

  public handleExpirationTime(authResult: IAuthResult) {
    const tokenRequestTime = parseInt(this.getTokenRequestTime(), 10);
    const expirationTime = isNaN(tokenRequestTime)
      ? this.getExpirationTimeFromToken(authResult.access_token)
      : this.getExpirationTime(tokenRequestTime, authResult.expires_in);

    return JSON.stringify(expirationTime);
  }

  public getExpirationTime(tokenRequestTime: number, expiresIn: number) {
    return tokenRequestTime + expiresIn * 1000;
  }

  public getExpirationTimeFromToken(accessToken: string) {
    const decodedAccessToken = this.decodeToken(accessToken);

    return decodedAccessToken.token.exp * 1000;
  }

  /**
   * Sets the respective public certificate
   */

  public setPublicCertificate(certificate: IPublicCertificate) {
    localStorage.setItem(this.PUBLIC_CERTIFICATE_KEY, certificate.value);
    localStorage.setItem(this.PUBLIC_CERTIFICATE_ALG, certificate.alg);
  }

  /**
   * Returns the expiry time of the token
   */
  public getExpiresAccessTime() {
    return JSON.parse(localStorage.getItem(this.EXPIRES_AT_STORAGE_KEY));
  }

  /**
   * Purges the session (removes token and expiry time)
   */
  public resetLocalStorage() {
    localStorage.clear();
  }

  public setClientId(clientId: string) {
    localStorage.setItem(this.CLIENT_ID_KEY, clientId);
  }

  public setTokenRequestTime() {
    const date = new Date();

    localStorage.setItem(this.TOKEN_REQUEST_TIME, `${date.getTime()}`);
  }
}
