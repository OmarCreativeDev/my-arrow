import { Injectable, Injector } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthTokenService } from './auth-token.service';
import { Store } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { ResetUser } from '@app/core/user/store/user.actions';

@Injectable()
export class AuthGuard {
  constructor(private store: Store<IAppState>, private injector: Injector) {}

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    const authTokenService = this.injector.get(AuthTokenService);
    if (authTokenService.getValidAccessToken() === null && authTokenService.getValidRefreshToken() === null) {
      this.store.dispatch(new ResetUser(state.url));
      return false;
    }
    return true;
  }
}
