export interface IAuthResult {
  access_token: string;
  token_type?: string;
  refresh_token?: string;
  expires_in?: number;
}

export interface IToken {
  authorities: Array<string>;
  clientId: string;
  email: string;
  exp: number;
  jti: string;
  ati?: string;
  scope: Array<string>;
  userName: string;
}

export interface ITokenHeader {
  alg: string;
  typ: string;
}

export interface IDecodedToken {
  token: IToken;
  header: ITokenHeader;
}

export interface IPublicCertificate {
  alg: string;
  value: string;
}
