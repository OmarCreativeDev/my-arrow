import { TestBed, fakeAsync } from '@angular/core/testing';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { StoreModule, Store } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';
import { ResetUser } from '@app/core/user/store/user.actions';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@app/core/core.module';

import { AuthInterceptorService } from './auth-interceptor.service';
import { AuthService } from './auth.service';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';

describe('AuthInterceptorService', function() {
  let authService: AuthService;
  let http: HttpTestingController;
  let httpClient: HttpClient;
  let store: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({ user: userReducers }), RouterTestingModule, HttpClientTestingModule, CoreModule],
      providers: [
        {
          provide: HTTP_INTERCEPTORS,
          useClass: AuthInterceptorService,
          multi: true,
        },
        { provide: WSSService, useClass: MockStompWSSService },
      ],
    });

    authService = TestBed.get(AuthService);
    http = TestBed.get(HttpTestingController);
    httpClient = TestBed.get(HttpClient);

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should dispaych ResetUser() action with a 401 error', fakeAsync(() => {
    httpClient.get('/dummy-service').subscribe(
      () => {},
      () => {
        expect(store.dispatch).toHaveBeenCalledWith(new ResetUser());
      }
    );

    http.expectOne({ url: '/dummy-service', method: 'GET' }).error(new ErrorEvent('error'), { status: 401 });
  }));

  it('should pass other errors through to app', fakeAsync(() => {
    httpClient.get('/dummy-service').subscribe(
      () => {},
      () => {
        expect(store.dispatch).toHaveBeenCalledTimes(0);
      }
    );

    http.expectOne({ url: '/dummy-service', method: 'GET' }).error(new ErrorEvent('error'), { status: 500 });
  }));

  it('should verify if checkAndRenewTokens gets called', fakeAsync(() => {
    httpClient.get('/dummy-service').subscribe(() => {
      expect(authService.checkAndRenewTokens()).toHaveBeenCalled();
    });

    http.expectOne({ url: '/dummy-service', method: 'GET' });
  }));

  it('should verify if checkAndRenewTokens doesnt get called', fakeAsync(() => {
    httpClient.get('/security').subscribe(() => {
      expect(authService.checkAndRenewTokens()).toHaveBeenCalledTimes(0);
    });

    http.expectOne({ url: '/security', method: 'GET' });
  }));
});
