import { Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Store, ActionsSubject, Action } from '@ngrx/store';

import { Observable, forkJoin, of, Subscription } from 'rxjs';
import { shareReplay, tap, filter } from 'rxjs/operators';

import { environment, CLIENT_ID } from '@env/environment';
import { ApiService } from '../api/api.service';
import { AuthTokenService } from './auth-token.service';
import { IAppState } from '@app/shared/shared.interfaces';
import { IAuthResult, IPublicCertificate } from './auth.interface';
import { ResetUser, UserActionTypes, TokenRefresh } from '@app/core/user/store/user.actions';
import { IUser } from '@app/core/user/user.interface';
import { WSSService } from '@app/core/ws/wss.service';
import { includes } from 'lodash';

@Injectable()
export class AuthService implements OnDestroy {
  public SERVICE_URL = `${environment.baseUrls.serviceSecurity}/oauth/token`;
  public PUBLIC_CERTIFICATE_URL = `${environment.baseUrls.serviceSecurity}/oauth/token_key`;
  public REVOKE_TOKEN_URL = `${environment.baseUrls.serviceSecurity}/oauth/token/revoke`;
  public RESET_PWD_URL = `${environment.baseUrls.serviceSecurity}/password`;

  public subscription: Subscription = new Subscription();
  public tokenInterval;

  constructor(
    private actionsSubj: ActionsSubject,
    private http: HttpClient,
    private authTokenService: AuthTokenService,
    private apiService: ApiService,
    private store: Store<IAppState>,
    private wssService: WSSService
  ) {
    /**
     * THIS SUBSCRIPTION SHOULD NOT BE MOVED TO ngOnInit
     * AS WILL FAIL TO LISTEN THE DESIRED ACTION
     */
    try {
      this.subscription.add(this.subscribeUserActions());
    } catch (e) { }
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  /**
   * Login and authenticate user then set api token on success
   * @param email
   * @param password
   */
  public login(username: string, password: string) {
    return this.loginWithClientId(username, password, CLIENT_ID);
  }

  /**
   * Login and authenticate user with Client ID then set api token on success
   * @param email
   * @param password
   * @param clientId
   */
  public loginWithClientId(username: string, password: string, clientId: string, accessId: string = '') {
    const authenticate$ = this.authenticate(username.toLocaleLowerCase().trim(), password, clientId, accessId);
    const certificate$ = this.getPublicCertificate();

    return forkJoin(certificate$, authenticate$).pipe(
      tap(([certificate, authenticate]) => {
        this.authTokenService.setPublicCertificate(certificate);
        this.authTokenService.setTokens(authenticate);
      }),
      shareReplay()
    );
  }

  /**
   * Logout user from server and the dispath ResetUser action.
   * If error on server then fail silently and dispatch ResetUser.
   */
  public logout() {
    this.wssService.requestDisconnect();
    const refreshToken = this.authTokenService.getRefreshToken();
    const request = this.revokeRefreshToken(refreshToken).pipe(shareReplay());
    request.subscribe(() => this.resetAuthTokensAndUserProfile(), err => this.resetAuthTokensAndUserProfile());
    return request;
  }

  public resetAuthTokensAndUserProfile() {
    this.authTokenService.resetLocalStorage();
    return this.store.dispatch(new ResetUser());
  }
  /**
   * Get the certificate
   */
  public getPublicCertificate() {
    return this.http.get<IPublicCertificate>(this.PUBLIC_CERTIFICATE_URL);
  }

  /**
   * Authenticate user
   * @param username
   * @param password
   * @param clientId
   * @param accessId
   */
  public authenticate(username, password, clientId, accessId) {
    const body = new HttpParams()
      .set('grant_type', 'password')
      .set('username', username)
      .set('password', password)
      .set('client_id', clientId)
      .set('access_id', accessId);

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

    this.authTokenService.setClientId(clientId);
    this.authTokenService.setTokenRequestTime();

    return this.http.post<IAuthResult>(this.SERVICE_URL, body, { headers });
  }

  public revokeRefreshToken(refreshToken) {
    const clientId = this.authTokenService.getClientId();
    const body = new HttpParams().set('refresh_token', refreshToken).set('client_id', clientId);

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

    return this.http.delete<IAuthResult>(this.REVOKE_TOKEN_URL, {
      headers,
      params: body,
    });
  }

  public requestNewTokens(refreshToken) {
    const clientId = this.authTokenService.getClientId();
    const body = new HttpParams()
      .set('grant_type', 'refresh_token')
      .set('refresh_token', refreshToken)
      .set('client_id', clientId);

    const headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded');

    return this.http.post<IAuthResult>(this.SERVICE_URL, body, { headers });
  }

  /**
   * Request and Store the new access and refresh token in local storage
   */
  public requestAndSetTokensInLocalStorage() {
    const refreshToken = this.authTokenService.getValidRefreshToken();
    this.store.dispatch(new TokenRefresh());

    this.authTokenService.setTokenRequestTime();

    return this.requestNewTokens(refreshToken).pipe(
      tap(newTokens => {
        this.authTokenService.setTokens(newTokens);
        return newTokens;
      }),
      shareReplay()
    );
  }

  public checkAndRenewTokens(): Observable<any> {
    const accessToken = this.authTokenService.getValidAccessToken();
    const refreshToken = this.authTokenService.getValidRefreshToken();

    if (accessToken === null && refreshToken !== null) {
      return this.requestAndSetTokensInLocalStorage();
    }

    return of(null);
  }

  /**
   * Sends forgot password request (the service will send email reset url to user)
   * @param email
   */
  public validateResetToken(rid: string, key: string): Observable<any> {
    return this.apiService.get<any>(`${this.RESET_PWD_URL}/tokens/${rid}/${key}`);
  }

  /**
   * Sends forgot password request (the service will send email reset url to user)
   * @param email
   */
  public recoverPassword(email: string): Observable<any> {
    return this.apiService.post<any>(`${this.RESET_PWD_URL}/tokens`, { email });
  }

  /**
   * Resets ths users password
   * @param rid
   * @param key
   * @param newPassword
   */
  public resetPassword(rid: string, key: string, newPassword: string): Observable<IUser> {
    return this.apiService.post<IUser>(this.RESET_PWD_URL, { rid, key, newPassword });
  }

  private subscribeUserActions(): Subscription {
    return this.actionsSubj
      .pipe(
        filter(action =>
          includes(
            [UserActionTypes.REQUEST_USER_COMPLETE, UserActionTypes.USER_LOGGED_IN_COMPLETE, UserActionTypes.RESET_USER],
            action.type
          )
        )
      )
      .subscribe((action: Action) => {
        this.handleUserActions(action);
      });
  }

  private handleUserActions(action: Action) {
    switch (action.type) {
      case UserActionTypes.REQUEST_USER_COMPLETE:
      case UserActionTypes.USER_LOGGED_IN_COMPLETE: {
        this.startTokenRefreshTask();
        break;
      }
      default: {
        this.stopTokenRefreshTimer();
        break;
      }
    }
  }

  private startTokenRefreshTask() {
    const tokenExpirityTime = this.authTokenService.getExpiresAccessTime();
    const timeRemaining = Math.round((tokenExpirityTime - new Date().getTime()) * 0.9);
    if (timeRemaining > 0) {
      this.startTokenInterval(timeRemaining);
    }
  }

  private startTokenInterval(timeout) {
    this.tokenInterval = setInterval(() => {
      this.requestAndSetTokensInLocalStorage().subscribe(() => {
        this.stopTokenRefreshTimer();
        this.startTokenRefreshTask();
        this.reconnectWebSocket();
      });
    }, timeout);
  }

  private stopTokenRefreshTimer() {
    if (this.tokenInterval) {
      clearInterval(this.tokenInterval);
    } else {
      this.tokenInterval = undefined;
    }
  }

  private reconnectWebSocket() {
    this.wssService.requestReconnect();
  }
}
