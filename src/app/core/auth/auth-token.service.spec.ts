import { TestBed, inject } from '@angular/core/testing';
import { StoreModule } from '@ngrx/store';
import * as jwt from 'jsrsasign';

import { AuthTokenService } from './auth-token.service';
import { AuthService } from './auth.service';
import { ApiService } from '@app/core/api/api.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { certificatePublicKey, certificatePrivateKey } from './mockCertificates.js';

import { CLIENT_ID } from '@env/environment';
import { ClientId } from '@app/core/user/user.interface';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';

const expiredAccessToken =
  'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJkYXZpZCIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJleHAiOjE1MjI2ODgyMzEsImF1dGhvcml0aWVzIjpbIlJPTEVfVVNFUiIsIlJPTEVfQURNSU4iXSwianRpIjoiM2VhMzY2OGYtNWFkZS00MmVkLTkwNjctNzQ3NmZhNDRhNjljIiwiZW1haWwiOiJkYXZpZC5zYW5lQG9ueG9yZW1zLmNvbSIsImNsaWVudF9pZCI6Ik15QXJyb3dVSSJ9.CnZrdXjJyChYT1_Zlbpo86UQhGvTznoR0-Pzbcn28S8SWQ3WonIQ7YZQF75mphywSMrBrXzGhKvV2F1XZ4AQJChFRrvi-ldpNjfYkzUn-ZiJ21BTzVsoifipvilsuaxeq2il7B6yFgfjOtsFN3IIMjXieRlMINd7XO0L_MBg8egvAJH1dnu2IZFVNAdZa2eyJ9VV_od9CgOp0IBkyj72FP_Q3FTrEITr6IVsDlbGZLTC5wCfMBb_ZO8TNiVhaAY2TnAflT0OZCIHcLUWFSKt9gdZTPmJMPqTn_TzUx7MrSC1rc4HqpbM8aztaVDwOI1esO-Fw-iVcwMadjNentojqQ';
const expiredRefreshToken =
  'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJkYXZpZCIsInNjb3BlIjpbInJlYWQiLCJ3cml0ZSJdLCJhdGkiOiIzZWEzNjY4Zi01YWRlLTQyZWQtOTA2Ny03NDc2ZmE0NGE2OWMiLCJleHAiOjE1MjUyNzY2MzEsImF1dGhvcml0aWVzIjpbIlJPTEVfVVNFUiIsIlJPTEVfQURNSU4iXSwianRpIjoiZjE4ZjQwMWQtMjYxMy00YTUyLWJiMzctZTYzN2U2Y2I5MzlhIiwiZW1haWwiOiJkYXZpZC5zYW5lQG9ueG9yZW1zLmNvbSIsImNsaWVudF9pZCI6Ik15QXJyb3dVSSJ9.grl67sXs5UV9DzDz9xKecSOz9hob9AeVeq8PDN67OYGjLMDcnu8gmCixdyaqUu2DJUKIjNxsR1G-q3QzGTdkFKuzp9ntacvNQd5a4SE3ryTkZ38dRw4dHoJAY4tqDi6wO9HlRbZ14hIsG9JTk9iraWu0J5EbA8cDDKmjFYgnzadnNQn8OaSoI4rbZYbWKmxugumy3iJCDArQ9NAWFhAASTA_-6vqgukbGVK571PxFvLEw8_cLZIdDkSrv7bUpoRD5dSmOcBufMOm6uIOb_a-JS7GFXR4Tf2IcvxqIgQZcZe1cWI9ingj4xVQMeufH6EGH_z3PqqZu1fzm3e3RTbeFw';

const mockTokenHeader = {
  alg: 'RS256',
  typ: 'JWT',
};

const mockTokenPayload = {
  user_name: 'david',
  scope: ['read', 'write'],
  exp: 3100657707,
  authorities: ['ROLE_USER', 'ROLE_ADMIN'],
  jti: 'a9448f77-98f1-4061-a934-8f7895c4c146',
  email: 'david.sane@onxorems.com',
  client_id: ClientId.MYARROWUI,
};

export const foreverCurrentAccessToken = jwt.KJUR.jws.JWS.sign(
  null,
  JSON.stringify(mockTokenHeader),
  JSON.stringify(mockTokenPayload),
  certificatePrivateKey
);

export const mockCertificateResponse = {
  alg: 'Sha256WithRSAEncryption',
  value: certificatePublicKey,
};

export const foreverCurrentRefreshToken = jwt.KJUR.jws.JWS.sign(
  null,
  JSON.stringify(mockTokenHeader),
  JSON.stringify(mockTokenPayload),
  certificatePrivateKey
);

describe('AuthTokenService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}), HttpClientTestingModule],
      providers: [AuthTokenService, AuthService, ApiService, { provide: WSSService, useClass: MockStompWSSService }],
    });
  });

  it('should be created', inject([AuthTokenService], (service: AuthTokenService) => {
    expect(service).toBeTruthy();
  }));

  it('should return null all relevant gets if the token has not been set', inject([AuthTokenService], (service: AuthTokenService) => {
    expect(service.getValidAccessToken()).toEqual(null);
    expect(service.getValidRefreshToken()).toEqual(null);
  }));

  it('should return null if the token has expired', inject([AuthTokenService], (service: AuthTokenService) => {
    const mockResponse = {
      access_token: expiredAccessToken,
      refresh_token: expiredRefreshToken,
      expires_in: 0,
    };
    service.setTokens(mockResponse);

    spyOn(service, 'isValidToken').and.returnValue(false);

    const accessToken = service.getValidAccessToken();
    expect(accessToken).toEqual(null);
  }));

  it(`should return false for isValidToken() if there is no valid public certificate`, inject(
    [AuthTokenService],
    (service: AuthTokenService) => {
      const response = service.isValidToken(foreverCurrentAccessToken);
      expect(response).toEqual(false);
    }
  ));

  it(`should return true for isValidToken() when all tokens are valid`, inject([AuthTokenService], (service: AuthTokenService) => {
    spyOn(jwt.KEYUTIL, 'getKey').and.returnValue('---VALID---');
    spyOn(jwt.KJUR.jws.JWS, 'verifyJWT').and.returnValue(true);

    const mockResponse = {
      access_token: foreverCurrentAccessToken,
      refresh_token: foreverCurrentRefreshToken,
      expires_in: 0,
    };
    service.setTokens(mockResponse);
    service.setPublicCertificate(mockCertificateResponse);

    const response = service.isValidToken(foreverCurrentAccessToken);
    expect(response).toEqual(true);
  }));

  it('should get value of access token for un-expired tokens', inject([AuthTokenService], (service: AuthTokenService) => {
    const mockResponse = {
      access_token: foreverCurrentAccessToken,
      refresh_token: expiredRefreshToken,
      expires_in: 50,
    };
    service.PUBLIC_CERTIFICATE_KEY = 'publicCertificate';
    service.PUBLIC_CERTIFICATE_ALG = 'publicCertificateAlg';
    service.setPublicCertificate(mockCertificateResponse);
    service.setTokens(mockResponse);
    expect(service.getAccessToken()).toEqual(foreverCurrentAccessToken);

    spyOn(service, 'isValidToken').and.returnValue(true);
    expect(service.getValidAccessToken()).toEqual(foreverCurrentAccessToken);
  }));

  it('should get value of refresh token for un-expired tokens', inject([AuthTokenService], (service: AuthTokenService) => {
    const mockResponse = {
      access_token: foreverCurrentAccessToken,
      refresh_token: expiredRefreshToken,
      expires_in: 50,
    };
    service.PUBLIC_CERTIFICATE_KEY = 'publicCertificate';
    service.PUBLIC_CERTIFICATE_ALG = 'publicCertificateAlg';
    service.setPublicCertificate(mockCertificateResponse);
    service.setTokens(mockResponse);
    expect(service.getRefreshToken()).toEqual(expiredRefreshToken);

    spyOn(service, 'isValidToken').and.returnValue(true);
    expect(service.getValidRefreshToken()).toEqual(expiredRefreshToken);
  }));

  it('should get proper public certificate algorithm ', inject([AuthTokenService], (service: AuthTokenService) => {
    service.PUBLIC_CERTIFICATE_KEY = 'publicCertificate';
    service.PUBLIC_CERTIFICATE_ALG = 'publicCertificateAlg';
    const algorithm = service.getPublicCertificateAlg();
    expect(algorithm).toEqual(mockCertificateResponse.alg);
  }));

  it('resetLocalStorage should reset tokens', inject([AuthTokenService], (service: AuthTokenService) => {
    service.resetLocalStorage();
    expect(service.getValidAccessToken()).toEqual(null);
    expect(service.getExpiresAccessTime()).toEqual(null);
  }));

  it('getClientId should get clientId from Local Storage', inject([AuthTokenService], (service: AuthTokenService) => {
    expect(service.getClientId()).toEqual(CLIENT_ID);
  }));

  it('setClientId should set clientId inside Local Storage', inject([AuthTokenService], (service: AuthTokenService) => {
    const mockResponse = '123';

    service.setClientId(mockResponse);
    expect(service.getClientId()).toEqual(mockResponse);
  }));

  it('getTokenRequestTime should return null', inject([AuthTokenService], (service: AuthTokenService) => {
    const tokenRequestTime = service.getTokenRequestTime();
    expect(tokenRequestTime).toBeNull();
  }));

  it('setTokenRequestTime should set tokenRequestTime inside Local Storage', inject([AuthTokenService], (service: AuthTokenService) => {
    service.setTokenRequestTime();

    const tokenRequestTime = service.getTokenRequestTime();

    expect(tokenRequestTime).not.toBeNull();
  }));

  it('handleExpirationTime should call getExpirationTime to handle expiration time', inject(
    [AuthTokenService],
    (service: AuthTokenService) => {
      const authResult = {
        access_token: foreverCurrentAccessToken,
        token_type: 'bearer',
        refresh_token: foreverCurrentRefreshToken,
        expires_in: 599,
      };

      service.setTokenRequestTime();

      const tokenRequestTime = parseInt(service.getTokenRequestTime(), 10);
      const expirationTime = service.handleExpirationTime(authResult);
      const expectedExpirationTime = JSON.stringify(service.getExpirationTime(tokenRequestTime, authResult.expires_in));

      expect(expirationTime).toEqual(expectedExpirationTime);
    }
  ));

  it("handleExpirationTime should call getExpirationTimeFromToken to handle expiration time if there's no tokenRequestTime on Local Storage", inject(
    [AuthTokenService],
    (service: AuthTokenService) => {
      const authResult = {
        access_token: foreverCurrentAccessToken,
        token_type: 'bearer',
        refresh_token: foreverCurrentRefreshToken,
        expires_in: 599,
      };

      service.resetLocalStorage();

      const expirationTime = service.handleExpirationTime(authResult);
      const expectedExpirationTime = JSON.stringify(service.getExpirationTimeFromToken(authResult.access_token));

      expect(expirationTime).toEqual(expectedExpirationTime);
    }
  ));

  it('getExpirationTime should return a time in the future', inject([AuthTokenService], (service: AuthTokenService) => {
    const tokenRequestTime = new Date().getTime();
    const expiresIn = 599;
    const expirationTime = service.getExpirationTime(tokenRequestTime, expiresIn);
    const expectedExpirationTime = tokenRequestTime + expiresIn * 1000;

    expect(expirationTime).toEqual(expectedExpirationTime);
  }));

  it('getExpirationTimeFromToken should return token expiration time', inject([AuthTokenService], (service: AuthTokenService) => {
    const expirationTime = service.getExpirationTimeFromToken(foreverCurrentAccessToken);
    const decodedAccessToken = service.decodeToken(foreverCurrentAccessToken);
    const expectedExpirationTime = decodedAccessToken.token.exp * 1000;

    expect(expirationTime).toEqual(expectedExpirationTime);
  }));
});
