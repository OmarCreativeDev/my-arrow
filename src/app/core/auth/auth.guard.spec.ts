import { TestBed, fakeAsync } from '@angular/core/testing';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store, StoreModule } from '@ngrx/store';
import { CoreModule } from '../core.module';
import { AuthGuard } from './auth.guard';
import { AuthTokenService } from './auth-token.service';
import { userReducers } from '@app/core/user/store/user.reducers';
import { ResetUser } from '@app/core/user/store/user.actions';

describe('AuthGuard', () => {
  const mockSnapshot: RouterStateSnapshot = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);
  let authTokenService: AuthTokenService;
  let authGuard: AuthGuard;
  let store: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({ user: userReducers }), CoreModule],
      providers: [AuthGuard],
    });
    authTokenService = TestBed.get(AuthTokenService);
    authGuard = TestBed.get(AuthGuard);

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(authGuard).toBeTruthy();
  });

  it('should activate route if user has a token', () => {
    spyOn(authTokenService, 'getValidAccessToken').and.returnValue('token');
    expect(authGuard.canActivate(new ActivatedRouteSnapshot(), mockSnapshot)).toBeTruthy();
  });

  it(
    'should guard route and dispatch ResetUser action if access token is null',
    fakeAsync(() => {
      spyOn(authTokenService, 'getValidAccessToken').and.returnValue(null);
      mockSnapshot.url = '/orders';
      expect(authGuard.canActivate(new ActivatedRouteSnapshot(), mockSnapshot)).toBeFalsy();
      expect(store.dispatch).toHaveBeenCalledWith(new ResetUser('/orders'));
    })
  );
});
