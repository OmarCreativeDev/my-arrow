import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpErrorResponse, HttpHandler, HttpRequest } from '@angular/common/http';
import { mergeMap, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { includes, get } from 'lodash-es';

import { environment } from '@env/environment';
import { AuthService } from '@app/core/auth/auth.service.ts';
import { IAppState } from '@app/shared/shared.interfaces';
import { ResetUser, UserLoggedOut } from '@app/core/user/store/user.actions';
import { ReasonsForLogout } from '../analytics/meta-reducers/analytics.user.enum';

@Injectable()
export class AuthInterceptorService implements HttpInterceptor {
  constructor(private store: Store<IAppState>, private injector: Injector) {}

  public redirectToDashboard(error) {
    if (error instanceof HttpErrorResponse && error.status === 401) {
      this.store.dispatch(new UserLoggedOut(ReasonsForLogout.TOKEN_EXPIRED));
      this.store.dispatch(new ResetUser());
    }
    return null;
  }

  public handleNext(req, next) {
    return next.handle(req).pipe(tap(() => {}, (error: any) => this.redirectToDashboard(error)));
  }

  /**
   * Intercepts authorization error from apis
   * - if there is an 401 dispatch ResetUser() action, otherwise pass through and handle in app
   */

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (includes(req.url, environment.baseUrls.serviceSecurity)) {
      return this.handleNext(req, next);
    }

    const authService = this.injector.get(AuthService);

    return authService.checkAndRenewTokens().pipe(
      mergeMap(newTokens => {
        const newAccessToken = get(newTokens, 'access_token');
        const _req = newAccessToken
          ? req.clone({
              headers: req.headers.set('Authorization', `Bearer ${newAccessToken}`),
            })
          : req;

        return this.handleNext(_req, next);
      })
    );
  }
}
