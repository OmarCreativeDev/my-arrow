export interface IProductNotificationField {
  value: boolean;
  description: string;
  name: string;
}

export type ProductNotificationsFrequency = 'Daily' | 'Weekly' | 'Monthly';

export interface IProductNotifications {
  critical: Array<IProductNotificationField>;
  nonCritical: Array<IProductNotificationField>;
  custAccountId: number;
  custAcctSiteId: number;
  custContactId: number;
  custPartyId: number;
  pcnNotifReq: boolean;
  frequency: ProductNotificationsFrequency;
  pcnFileType: string;
}

export interface IProductNotificationsSelectedPreferences {
  critical: Array<IProductNotificationField>;
  nonCritical: Array<IProductNotificationField>;
  notificationsRequired: string;
  pcnNotifReq: boolean;
  custContactId: number;
  frequency: ProductNotificationsFrequency;
}

export interface IProductNotificationsPreferences {
  alertRecall?: boolean;
  assemblyProcess?: boolean;
  assemblySite?: boolean;
  custAccountId: number;
  custAcctSiteId: number;
  custContactId: number;
  custPartyId: number;
  endOfLife?: boolean;
  environmentalData?: boolean;
  eolReversal?: boolean;
  formFitFun?: boolean;
  frequency: ProductNotificationsFrequency;
  labellingPacking?: boolean;
  marking?: boolean;
  molding?: boolean;
  nomenclatureChg?: boolean;
  notForDesign?: boolean;
  other: boolean;
  pcnFileType: string;
  pcnNotifReq: boolean;
  recall?: boolean;
  region?: string;
  rfcbNcr?: boolean;
  rfcbNcrReversal?: boolean;
  shippingPacking?: boolean;
  testProcess?: boolean;
  testSite?: boolean;
  waferProcess?: boolean;
  waferSite?: boolean;
}

export type IProductNotificationFileTypes = 'XLS' | 'XLSX' | 'CSV';

export type IProductNotificationColumnTypes =
  | 'date'
  | 'customerPartNumber'
  | 'manufacturerPartNumber'
  | 'manufacturer'
  | 'type'
  | 'description';

export type ProductNotificationCategory = 'nonfunctional' | 'manufacturing' | 'status';

export interface IProductNotification {
  dateIssued: string;
  cpn: string;
  apn: string;
  manufacturer: string;
  type: string;
  description: string;
  ecpnNumber: string;
  docUrl?: string;
  category?: ProductNotificationCategory;
}

export interface IProductNotificationsDownloadUserPreferences {
  dateRange: number;
  fileType: IProductNotificationFileTypes;
  columns: Array<IProductNotificationColumnTypes>;
  userId?: string;
}

export interface IProductNotificationsColumnField {
  column: string;
  text: string;
}

export interface IProductNotificationsUserPreferences {
  dates: Array<number>;
  types: Array<IProductNotificationFileTypes>;
  columns: Array<IProductNotificationsColumnField>;
}

export interface IProductNotificationsDownloadParams {
  accountId: number;
  companyName: string;
  fileType: IProductNotificationFileTypes;
  columns: Array<IProductNotificationColumnTypes>;
  startDate?: string;
  endDate?: string;
}

export interface IProductNotificationsState {
  notifications: Array<IProductNotification>;
  loading: boolean;
  error?: object;
  notificationPreferences: IProductNotificationsSelectedPreferences;
  setNotificationPreferences: IProductNotificationsPreferences;
  downloadOptions: IProductNotificationsUserPreferences;
  downloadPreferences: IProductNotificationsDownloadUserPreferences;
  notificationPreferencesError: object;
  notificationPreferencesSuccess: boolean;
  downloadError: object;
  downloadStatus: number;
}

export interface IProductNotificationsSorted {
  pmProductNotifications: Array<IProductNotification>;
  pnfProductNotifications: Array<IProductNotification>;
  pscProductNotifications: Array<IProductNotification>;
}

export enum NotificationTypes {
  MANUFACTURING = 'manufacturing',
  NON_FUNCTIONAL = 'nonfunctional',
  STATUS = 'status',
}
