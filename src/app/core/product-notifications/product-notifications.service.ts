import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpResponse } from '@angular/common/http';

import { ApiService } from '../api/api.service';

import {
  IProductNotifications,
  IProductNotificationsPreferences,
  IProductNotificationsDownloadUserPreferences,
  IProductNotificationsUserPreferences,
  IProductNotification,
  IProductNotificationsDownloadParams,
} from '@app/core/product-notifications/product-notifications.interfaces';
import { environment } from '@env/environment';
import { LanguageService } from '@app/core/language/language.service';

@Injectable()
export class ProductNotificationsService {
  constructor(private apiService: ApiService, private languageService: LanguageService) {}

  public getProductNotificatonPreferences(
    custAccountId: number,
    custAcctSiteId: number,
    custPartyId: number
  ): Observable<IProductNotifications> {
    const params = { custAccountId, custAcctSiteId, custPartyId };
    const headers = this.languageService.getLanguage();

    return this.apiService.get<IProductNotifications>(`${environment.baseUrls.serviceProductNotification}/preferences`, params, headers);
  }

  public setProductNotificatonPreferences(params?: {}): Observable<IProductNotificationsPreferences> {
    return this.apiService.put<IProductNotificationsPreferences>(
      `${environment.baseUrls.serviceProductNotification}/preferences`,
      params,
      true
    );
  }

  public getProductNotifications(custAccountId: number, startDate: string, endDate: string): Observable<IProductNotification[]> {
    const params = { startDate, endDate };
    const headers = this.languageService.getLanguage();

    return this.apiService.get<IProductNotification[]>(
      `${environment.baseUrls.serviceProductNotification}/pcns/view/${custAccountId}`,
      params,
      headers
    );
  }

  public getProductNotificationsDownloadOptions(): Observable<IProductNotificationsUserPreferences> {
    const headers = this.languageService.getLanguage();

    return this.apiService.get<IProductNotificationsUserPreferences>(
      `${environment.baseUrls.serviceProductNotification}/pcns/download/preference/options`,
      null,
      headers
    );
  }

  public setProductNotificationsDownloadOptions(preferences): Observable<void> {
    return this.apiService.put<void>(`${environment.baseUrls.serviceProductNotification}/pcns/download/preference`, preferences, true);
  }

  public getProductNotificationsDownloadUserPreferences(): Observable<IProductNotificationsDownloadUserPreferences> {
    const headers = this.languageService.getLanguage();

    return this.apiService.get<IProductNotificationsDownloadUserPreferences>(
      `${environment.baseUrls.serviceProductNotification}/pcns/download/preference`,
      null,
      headers
    );
  }

  public getProductNotificationsFile(preferences: IProductNotificationsDownloadParams): Observable<HttpResponse<Blob>> {
    const headers = this.languageService.getLanguage();

    return this.apiService.postBlob(`${environment.baseUrls.serviceProductNotification}/pcns/download`, preferences, headers);
  }
}
