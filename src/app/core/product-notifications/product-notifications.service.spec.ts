import { TestBed, inject } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { ApiService } from '@app/core/api/api.service';
import { CoreModule } from '@app/core/core.module';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { ProductNotificationsService } from './product-notifications.service';
import {
  IProductNotification,
  IProductNotificationsDownloadUserPreferences,
  IProductNotificationsUserPreferences,
} from '@app/core/product-notifications/product-notifications.interfaces';
import mockedProductNotifications from './product-notifications-mock-response';
import { environment } from '@env/environment';

export const MockedProductNotifications: Array<IProductNotification> = [
  {
    dateIssued: '2018-01-11',
    cpn: 'test',
    apn: 'test',
    manufacturer: 'test',
    type: 'test',
    description: 'test',
    ecpnNumber: 'test',
    category: 'nonfunctional',
  },
  {
    dateIssued: '2017-01-11',
    cpn: 'atest',
    apn: 'atest',
    manufacturer: 'atest',
    type: 'atest',
    description: 'atest',
    ecpnNumber: 'atest',
    category: 'nonfunctional',
  },
];

const mockedPreferenceOptions: IProductNotificationsUserPreferences = {
  types: ['XLS', 'XLSX', 'CSV'],
  dates: [6, 12, 24],
  columns: [
    {
      column: 'date',
      text: 'Date',
    },
    {
      column: 'customerPartNumber',
      text: 'Cust Part #',
    },
    {
      column: 'manufacturerPartNumber',
      text: 'MFG Part Number',
    },
    {
      column: 'manufacturer',
      text: 'Manufacturer',
    },
    {
      column: 'type',
      text: 'Type',
    },
    {
      column: 'description',
      text: 'Description',
    },
  ],
};

const mockedUserPreferences: IProductNotificationsDownloadUserPreferences = {
  dateRange: 12,
  fileType: 'CSV',
  columns: ['date', 'customerPartNumber', 'manufacturerPartNumber', 'manufacturer', 'type', 'description'],
  userId: 'david.lane@oncorems.com',
};

const mockedPreferences: IProductNotificationsDownloadUserPreferences = {
  dateRange: 6,
  fileType: 'CSV',
  columns: ['date', 'customerPartNumber', 'manufacturerPartNumber', 'manufacturer', 'type', 'description'],
};

export class ProductNotificationsServiceMock {
  setProductNotificatonPreferences() {
    return of([]);
  }
  getProductNotificatonPreferences() {
    return of([]);
  }
  getProductNotifications() {
    return of(MockedProductNotifications);
  }
  getProductNotificationsDownloadOptions() {
    return of(mockedPreferenceOptions);
  }
  setProductNotificationsDownloadOptions() {
    return of([]);
  }
  getProductNotificationsDownloadUserPreferences() {
    return of(mockedPreferences);
  }
  getProductNotificationsFile() {
    return of([]);
  }
}

describe('ProductNotificationsService', () => {
  let apiService: ApiService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule],
      providers: [ProductNotificationsService, { provide: ApiService, useClass: ApiServiceMock }],
    });
    apiService = TestBed.get(ApiService);
  });

  it('should be created', inject([ProductNotificationsService], (service: ProductNotificationsService) => {
    expect(service).toBeTruthy();
  }));

  it('should get notification preferences', inject([ProductNotificationsService], (service: ProductNotificationsService) => {
    spyOn(apiService, 'get').and.returnValue(of(mockedProductNotifications));
    service.getProductNotificatonPreferences(1365356, 10128307, 6239544).subscribe(result => {
      expect(apiService.get).toHaveBeenCalledWith(
        `${environment.baseUrls.serviceProductNotification}/preferences`,
        { custAccountId: 1365356, custAcctSiteId: 10128307, custPartyId: 6239544 },
        { 'Accept-Language': 'en_US' }
      );
      expect(result).toEqual(mockedProductNotifications);
    });
  }));

  it('should get notifications', inject([ProductNotificationsService], (service: ProductNotificationsService) => {
    spyOn(apiService, 'get');
    service.getProductNotifications(0, '2018-01-01', '2018-03-30');
    expect(apiService.get).toHaveBeenCalled();
  }));

  it('should set notification preferences', inject([ProductNotificationsService], (service: ProductNotificationsService) => {
    spyOn(apiService, 'put');
    service.setProductNotificatonPreferences(0);
    expect(apiService.put).toHaveBeenCalled();
  }));

  it('should get notification download options', inject([ProductNotificationsService], (service: ProductNotificationsService) => {
    spyOn(apiService, 'get').and.returnValue(of(mockedPreferenceOptions));
    service.getProductNotificationsDownloadOptions().subscribe(result => {
      expect(apiService.get).toHaveBeenCalledWith(
        `${environment.baseUrls.serviceProductNotification}/pcns/download/preference/options`,
        null,
        {
          'Accept-Language': 'en_US',
        }
      );
      expect(result).toEqual(mockedPreferenceOptions);
    });
  }));

  it('should set notification download options', inject([ProductNotificationsService], (service: ProductNotificationsService) => {
    spyOn(apiService, 'put').and.returnValue(Observable);
    service.setProductNotificationsDownloadOptions(mockedUserPreferences);
    expect(apiService.put).toHaveBeenCalledWith(
      `${environment.baseUrls.serviceProductNotification}/pcns/download/preference`,
      mockedUserPreferences,
      true
    );
  }));

  it('should get user download preferences', inject([ProductNotificationsService], (service: ProductNotificationsService) => {
    spyOn(apiService, 'get').and.returnValue(of(mockedPreferences));
    service.getProductNotificationsDownloadUserPreferences().subscribe(result => {
      expect(apiService.get).toHaveBeenCalledWith(`${environment.baseUrls.serviceProductNotification}/pcns/download/preference`, null, {
        'Accept-Language': 'en_US',
      });
      expect(result).toEqual(mockedPreferences);
    });
  }));
});
