import { IProductNotifications } from './product-notifications.interfaces';

export default <IProductNotifications>{
  critical: [
    {
      description: 'End of Life',
      value: true,
      name: 'endOfLife',
    },
    {
      description: 'Alert',
      value: true,
      name: 'alertRecall',
    },
    {
      description: 'EOL Reversal',
      value: true,
      name: 'eolReversal',
    },
    {
      description: 'Recall',
      value: true,
      name: 'recall',
    },
    {
      description: 'Not for Design',
      value: true,
      name: 'notForDesign',
    },
    {
      description: 'Removed From Cost Book - NCNR',
      value: true,
      name: 'rfcbNcr',
    },
    {
      description: 'Removed From Cost Book - NCNR Renewal',
      value: true,
      name: 'rfcbNcrReversal',
    },
  ],
  nonCritical: [
    {
      description: 'Shipping / Packing',
      value: true,
      name: 'shippingPacking',
    },
    {
      description: 'Nomenclature Change',
      value: true,
      name: 'nomenclatureChg',
    },
    {
      description: 'Test Process',
      value: true,
      name: 'testProcess',
    },
    {
      description: 'Wafer Site',
      value: true,
      name: 'waferSite',
    },
    {
      description: 'Labeling and Packaging',
      value: true,
      name: 'labellingPacking',
    },
    {
      description: 'Molding',
      value: true,
      name: 'molding',
    },
    {
      description: 'Assembly Process',
      value: true,
      name: 'assemblyProcess',
    },
    {
      description: 'From / Fit Function',
      value: true,
      name: 'formFitFun',
    },
    {
      description: 'Marking',
      value: true,
      name: 'marking',
    },
    {
      description: 'Wafer Process',
      value: true,
      name: 'waferProcess',
    },
    {
      description: 'Test Site',
      value: true,
      name: 'testSite',
    },
    {
      description: 'Environment Data',
      value: true,
      name: 'environmentalData',
    },
  ],
  custContactId: 24822,
  custAccountId: 1365356,
  custAcctSiteId: 10128307,
  custPartyId: 6239544,
  frequency: 'Daily',
  pcnFileType: 'PDF',
  pcnNotifReq: true,
};
