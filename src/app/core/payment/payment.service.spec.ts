import { TestBed, inject } from '@angular/core/testing';
import { ApiService } from '@app/core/api/api.service';
import { CoreModule } from '@app/core/core.module';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { PaymentService } from './payment.service';

export const mockedPayment = {
  decision: 'ACCEPT',
  merchantReferenceCode: 'test',
  missingFields: [],
  profileId: '99001234',
  reasonCode: '100',
  requestId: '1234',
};

export const mockedPaymentRequest = {
  cardNumber: '4111111111111111',
  cardExpMM: '01',
  cardExpYYYY: '2020',
  cardCVV: '123',
  billToFirstName: 'Mr. Cool',
  billToLastName: 'Tester',
  billToCity: 'Testville',
  billToStateProvince: 'CA',
  billToPostalCode: '12345',
  billToCountry: 'USA',
  billToEmail: 'test@arrow.com',
  currencyCode: 'USD',
  cardType: 'VISA',
  billToStreet1: '123 Test St.',
};

describe('PaymentService', () => {
  let apiService: ApiService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule],
      providers: [PaymentService, { provide: ApiService, useClass: ApiServiceMock }],
    });
    apiService = TestBed.get(ApiService);
  });

  it('should be created', inject([PaymentService], (service: PaymentService) => {
    expect(service).toBeTruthy();
  }));

  it('should return an Observable<IPaymentResponse>', inject([PaymentService], (service: PaymentService) => {
    spyOn(apiService, 'post').and.returnValue(mockedPayment);
    service.validateCreditCard(mockedPaymentRequest);
    expect(apiService.post).toHaveBeenCalled();
  }));
});
