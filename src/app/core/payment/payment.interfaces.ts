export enum CreditCardTypes {
  AMEX = 'AMEX',
  MASTERCARD = 'MASTERCARD',
  VISA = 'VISA',
}

export interface IPaymentRequest {
  cardNumber: string;
  cardExpMM: string;
  cardExpYYYY: string;
  cardCVV: string;
  billToFirstName: string;
  billToLastName: string;
  billToCity: string;
  billToStateProvince: string;
  billToPostalCode: string;
  billToCountry: string;
  billToEmail: string;
  currencyCode: string;
  cardType: string;

  billToStreet1: string;
  billToStreet2?: string;
  freightAmount?: string;
  grandTotalAmount?: string;
  maskedCardNumber?: string;
  merchantRefCode?: string;
  requestType?: string;
  taxAmount?: string;
}

export interface IPaymentResponse {
  decision: string;
  merchantReferenceCode: string;
  missingFields: Array<string>;
  profileId: string;
  reasonCode: string;
  requestId: string;
}

export interface IPaymentState {
  error?: Error;
  loading: boolean;
  requestId: string;
  profileId: string;
  decision: string;
}
