import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../api/api.service';
import { IPaymentRequest, IPaymentResponse } from './payment.interfaces';
import { environment } from '@env/environment';

@Injectable()
export class PaymentService {
  constructor(private apiService: ApiService) {}

  public validateCreditCard(payload: IPaymentRequest): Observable<IPaymentResponse> {
    return this.apiService.post<IPaymentResponse>(`${environment.baseUrls.servicePayment}/getProfileId`, payload);
  }
}
