import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Location } from '@angular/common';
import { StoreModule, Store } from '@ngrx/store';
import { CoreModule } from '@app/core/core.module';
import { userReducers } from '@app/core/user/store/user.reducers';
import { RequestUserComplete } from '@app/core/user/store/user.actions';
import { mockUser } from '@app/core/user/user.service.spec';
import { RoleGuard } from '@app/core/user/user-role.guard';

class DummyComponent {}

describe('RoleGuard', () => {
  const routes = [{ path: 'forecast', component: DummyComponent }, { path: 'dashboard', component: DummyComponent }];
  const mockSnapshot: RouterStateSnapshot = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);
  mockSnapshot.url = '/forecast';

  let roleGuard: RoleGuard;
  let store: Store<any>;
  let location: Location;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        StoreModule.forRoot({
          user: userReducers,
        }),
        RouterTestingModule.withRoutes(routes),
      ],
      providers: [RoleGuard],
    });
    location = TestBed.get(Location);
    roleGuard = TestBed.get(RoleGuard);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', inject([RoleGuard], (service: RoleGuard) => {
    expect(service).toBeTruthy();
  }));

  it('should activate route if user has permission in their permission list', fakeAsync(() => {
    const action = new RequestUserComplete(mockUser);
    store.dispatch(action);
    roleGuard.canActivate(new ActivatedRouteSnapshot(), mockSnapshot).subscribe(canActivate => {
      expect(canActivate).toBeTruthy();
    });
  }));

  it('should guard route and redirect to "/dashboard" if user has not got the right permissions to view the page', fakeAsync(() => {
    const action = new RequestUserComplete(
      Object.assign(mockUser, {
        permissionList: [],
      })
    );
    store.dispatch(action);
    roleGuard.canActivate(new ActivatedRouteSnapshot(), mockSnapshot).subscribe(canActivate => {
      expect(canActivate).toBeFalsy();
      tick(20);
      expect(location.path()).toBe('/dashboard');
    });
  }));
});
