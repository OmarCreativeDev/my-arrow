import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Location } from '@angular/common';
import { StoreModule, Store } from '@ngrx/store';
import { CoreModule } from '@app/core/core.module';
import { UserGuard } from './user.guard';
import { AuthService } from '@app/core/auth/auth.service';
import { userReducers } from '@app/core/user/store/user.reducers';
import { RequestUserComplete, UserError } from '@app/core/user/store/user.actions';
import { mockUser } from '@app/core/user/user.service.spec';

class DummyComponent {}

describe('UserGuard', () => {
  const routes = [
    { path: 'login', component: DummyComponent },
    { path: 'terms-of-use', component: DummyComponent },
    { path: 'user-error', component: DummyComponent },
  ];
  const mockSnapshot: RouterStateSnapshot = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);

  let userGuard: UserGuard;
  let store: Store<any>;
  let location: Location;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        StoreModule.forRoot({
          user: userReducers,
        }),
        RouterTestingModule.withRoutes(routes),
      ],
      providers: [UserGuard, AuthService],
    });
    location = TestBed.get(Location);
    userGuard = TestBed.get(UserGuard);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', inject([UserGuard], (service: UserGuard) => {
    expect(service).toBeTruthy();
  }));

  it('should activate route if user has accepted T&Cs', fakeAsync(() => {
    const action = new RequestUserComplete(mockUser);
    store.dispatch(action);
    userGuard.canActivate(new ActivatedRouteSnapshot(), mockSnapshot).subscribe(canActivate => {
      expect(canActivate).toBeTruthy();
    });
  }));

  it('should guard route and redirect to "/terms-of-use" if user has NOT accepted T&Cs', fakeAsync(() => {
    const action = new RequestUserComplete(Object.assign(mockUser, { isTsAndCsAccepted: false }));
    store.dispatch(action);
    userGuard.canActivate(new ActivatedRouteSnapshot(), mockSnapshot).subscribe(canActivate => {
      expect(canActivate).toBeFalsy();
      tick(20);
      expect(location.path()).toBe('/terms-of-use');
    });
  }));

  it('should pass error to app', fakeAsync(() => {
    const action = new UserError(new HttpErrorResponse({ status: 500 }));
    store.dispatch(action);
    userGuard.canActivate(new ActivatedRouteSnapshot(), mockSnapshot).subscribe(canActivate => {
      expect(canActivate).toBeTruthy();
    });
  }));
});
