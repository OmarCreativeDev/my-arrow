import { UserServiceHttpExceptionStatusEnum, UserServiceHttpExceptionMessage } from './user.interface';
import { get } from 'lodash';

export function isUserNotReady(error) {
  const { FORBIDDEN_EXCEPTION, INTERNAL_SERVER_ERROR, USER_LEGACY_NOT_FOUND, REPOSITORY_EXCEPTION, SERVICE_NOT_FOUND_EXCEPTION, SERVICE_UNAVAILABLE } = UserServiceHttpExceptionStatusEnum;
  const { USER_NOT_FOUND_IN_LEGACY_DB, USER_NOT_FOUND } = UserServiceHttpExceptionMessage;
  const { status, message } = get(error, 'error', {});
  const registrationRequiredStatuse = [FORBIDDEN_EXCEPTION, USER_LEGACY_NOT_FOUND, REPOSITORY_EXCEPTION, SERVICE_NOT_FOUND_EXCEPTION];

  return registrationRequiredStatuse.includes(status) || (message === USER_NOT_FOUND_IN_LEGACY_DB && status === INTERNAL_SERVER_ERROR) || (message === USER_NOT_FOUND && status === SERVICE_UNAVAILABLE);
}

