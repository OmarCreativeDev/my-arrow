import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from '@angular/router';
import { getUser } from '@app/core/user/store/user.selectors';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import { from } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Injectable()
export class RoleGuard {
  constructor(private store: Store<IAppState>, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return from(this.store.pipe(select(getUser))).pipe(
      filter(user => user !== undefined),

      // handle the reponse
      map(user => this.handleResponse(user, state))
    );
  }

  private handleResponse(user, state: RouterStateSnapshot) {
    const permissions = {
      forecast: 'Forecast',
    };

    const permissionToTest = permissions[state.url.split('/')[1]];
    if (!user.permissionList.includes(permissionToTest)) {
      this.router.navigateByUrl('/dashboard');
      return false;
    }

    return true;
  }
}
