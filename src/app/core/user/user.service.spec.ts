import { ApiService } from '@app/core/api/api.service';
import { CoreModule } from '@app/core/core.module';
import { inject, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { IUser, ClientId, UserCustomerType } from './user.interface';
import { Store, StoreModule } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';
import { UserService } from './user.service';

export const mockUser: IUser = {
  internalUserId: 254554,
  organizationPartyID: 6160345,
  accountSiteID: 10125599,
  registrationInstance: 'AC',
  region: 'arrowna',
  firstName: 'David',
  lastName: 'Lane',
  email: 'david.sane@onxorems.com',
  company: 'ONXORE MANUFACTURING SERVICES, LTD',
  country: 'US',
  isTsAndCsAccepted: true,
  userType: 4,
  customerType: UserCustomerType.CEM,
  paymentsTermId: 2171,
  paymentsTermName: '2% 30 Net 60',
  paymentsTermDescription: '2% disc within 30 days, Net 60 days',
  currencyCode: 'USD',
  currencyList: ['EUR', 'GBP', 'USD'],
  defaultLanguage: 'en',
  permissionList: ['POUpload', 'Forecast', 'ArrowReel', 'Backorder', 'Compliance'],
  orgId: 241,
  selectedBillTo: 2171627,
  selectedShipTo: 2172033,
  accountNumber: 1067767,
  accountId: 1305827,
  contact: {
    customerServiceEmail: 'gxustus@xrrow.com',
    customerServiceNumber: '+1 877 237 6721',
    salesRepName: 'Gutierrez, Kala',
    salesRepPhoneNumber: '+1 303-600-1453',
    salesRepEmail: 'kxutierrez@xrrxx.com',
  },
  warehouses: [
    {
      id: 1410,
      code: 'V11',
      arrowReel: false,
    },
    {
      id: 1411,
      code: 'V13',
      arrowReel: false,
    },
  ],
  userPartyID: 7173135,
  clientId: ClientId.MYARROWUI,
};

export const mockUserInHouse: IUser = {
  internalUserId: 254554,
  organizationPartyID: 6160345,
  accountSiteID: 10125599,
  registrationInstance: 'AC',
  region: 'arrowna',
  firstName: 'David',
  lastName: 'Lane',
  email: 'david.sane@onxorems.com',
  company: 'ONXORE MANUFACTURING SERVICES, LTD',
  country: 'US',
  isTsAndCsAccepted: true,
  userType: 4,
  customerType: UserCustomerType.CEM,
  paymentsTermId: 2171,
  paymentsTermName: '2% 30 Net 60',
  paymentsTermDescription: '2% disc within 30 days, Net 60 days',
  currencyCode: 'USD',
  currencyList: ['EUR', 'GBP', 'USD'],
  defaultLanguage: 'en',
  permissionList: ['POUpload', 'Forecast', 'ArrowReel', 'Backorder', 'Compliance'],
  orgId: 241,
  selectedBillTo: 2171627,
  selectedShipTo: 2172033,
  accountNumber: 1067767,
  accountId: 1305827,
  contact: {
    customerServiceEmail: 'gxustus@xrrow.com',
    customerServiceNumber: '+1 877 237 6721',
    salesRepName: 'IN HOUSE,',
    salesRepPhoneNumber: undefined,
    salesRepEmail: undefined,
  },
  warehouses: [
    {
      id: 1410,
      code: 'V11',
      arrowReel: false,
    },
    {
      id: 1411,
      code: 'V13',
      arrowReel: false,
    },
  ],
  userPartyID: 7173135,
  clientId: ClientId.MYARROWUI,
};

const mockBillToAccounts = [
  {
    name: 'FLEXTRONICS INTERNATIONAL USA',
    organizationPartyID: 6160345,
    accountSiteID: 10125599,
    accountId: 150061,
    ebsPersonId: 6223503,
    billToId: 1365393,
    orgId: 241,
    address1: 'MILPITAS SITE 335',
    address2: '5802 BOB BULLOCK C1',
    address3: 'PMB 014-507',
    address4: '',
    city: 'LAREDO',
    state: 'TX',
    province: '',
    country: 'US',
    countryDescription: 'United States',
    postalCode: '78041',
    selected: true,
  },
  {
    name: 'FLEXTRONICS INTERNATIONAL USA',
    organizationPartyID: 6160345,
    accountSiteID: 10125599,
    accountId: 150061,
    ebsPersonId: 6223503,
    billToId: 1365393,
    orgId: 241,
    address1: 'MILPITAS SITE 335',
    address2: '5802 BOB BULLOCK C1',
    address3: 'PMB 014-507',
    address4: '',
    city: 'LAREDO',
    state: 'TX',
    province: '',
    country: 'US',
    countryDescription: 'United States',
    postalCode: '78041',
    selected: false,
  },
];

describe('UserService', () => {
  let apiService: ApiService;
  let store: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule, StoreModule.forRoot({ user: userReducers })],
      providers: [UserService],
    });

    apiService = TestBed.get(ApiService);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  afterEach(() => {
    document.cookie = 'locale=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  });

  it('should be created', inject([UserService], (service: UserService) => {
    expect(service).toBeTruthy();
  }));

  it('should return an Observable<Array<IUser>>', inject([UserService], (service: UserService) => {
    spyOn(apiService, 'get').and.returnValue(of(mockUser));
    service.getUser().subscribe(user => {
      expect(user.firstName).toEqual('David');
      expect(user.selectedBillTo).toEqual(2171627);
    });
  }));

  it('should return an Observable<Array<IBillto>>', inject([UserService], (service: UserService) => {
    spyOn(apiService, 'get').and.returnValue(of(mockBillToAccounts));
    service.getBillToAccounts().subscribe(account => {
      expect(account[0].accountId).toEqual(150061);
      expect(account[1].billToId).toEqual(1365393);
    });
  }));

  it('should acceptTerms()', inject([UserService], (service: UserService) => {
    spyOn(apiService, 'patch').and.returnValue(of({ accepted: true }));
    service.updateProfile({ isTsAndCsAccepted: true }).subscribe(response => {
      expect(response.accepted).toBeTruthy();
    });
  }));

  it('should change language and reload app if users default lang is not en', inject([UserService], (service: UserService) => {
    spyOn(service, 'reloadApp').and.callFake(() => {});
    service.localeCookie = null;
    service.checkLanguage('de');
    expect(document.cookie.indexOf('locale=de') !== -1).toBeTruthy();
    expect(service.reloadApp).toHaveBeenCalled();
  }));

  it('should do nothing if default lang is en', inject([UserService], (service: UserService) => {
    spyOn(service, 'changeLanguage');
    service.localeCookie = null;
    service.checkLanguage('en');
    expect(service.changeLanguage).toHaveBeenCalledTimes(0);
  }));

  it('should update selected Bill to account', inject([UserService], (service: UserService) => {
    const user: IUser = { ...mockUser, selectedBillTo: 1234 };
    spyOn(apiService, 'put').and.returnValue(of(user));
    service.updateBillTo(1234).subscribe(response => {
      expect(response.firstName).toEqual('David');
      expect(response.selectedBillTo).toEqual(1234);
    });
  }));
});
