import { TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Location } from '@angular/common';
import { StoreModule, Store } from '@ngrx/store';
import { CoreModule } from '@app/core/core.module';
import { userReducers } from '@app/core/user/store/user.reducers';
import { RequestUserComplete } from '@app/core/user/store/user.actions';
import { mockUser } from '@app/core/user/user.service.spec';
import { NAGuard } from '@app/core/user/user-northAmerican.guard';

class DummyComponent {}

describe('NAGuard', () => {
  const routes = [{ path: 'dashboard', component: DummyComponent }];
  const mockSnapshot: RouterStateSnapshot = jasmine.createSpyObj<RouterStateSnapshot>('RouterStateSnapshot', ['toString']);

  let naGuard: NAGuard;
  let store: Store<any>;
  let location: Location;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        StoreModule.forRoot({
          user: userReducers,
        }),
        RouterTestingModule.withRoutes(routes),
      ],
      providers: [NAGuard],
    });
    location = TestBed.get(Location);
    naGuard = TestBed.get(NAGuard);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', inject([NAGuard], (service: NAGuard) => {
    expect(service).toBeTruthy();
  }));

  it(
    'should activate route if user region is NA',
    fakeAsync(() => {
      const action = new RequestUserComplete(mockUser);
      store.dispatch(action);
      naGuard.canActivate(new ActivatedRouteSnapshot(), mockSnapshot).subscribe(canActivate => {
        expect(canActivate).toBeTruthy();
      });
    })
  );

  it(
    'should guard route and redirect to "/dashboard" if user region is not NA',
    fakeAsync(() => {
      const action = new RequestUserComplete(
        Object.assign(mockUser, {
          region: 'arrowse',
        })
      );
      store.dispatch(action);
      naGuard.canActivate(new ActivatedRouteSnapshot(), mockSnapshot).subscribe(canActivate => {
        expect(canActivate).toBeFalsy();
        tick(20);
        expect(location.path()).toBe('/dashboard');
      });
    })
  );
});
