import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { combineLatest } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { RequestUser } from '@app/core/user/store/user.actions';
import { getUser, getUserError } from '@app/core/user/store/user.selectors';

import { IAppState } from '@app/shared/shared.interfaces';

@Injectable()
export class UserGuard {
  constructor(private store: Store<IAppState>, private router: Router) {}

  /**
   * The user is only able to access routes if they have accepted terms and conditions
   * We can also handle user error (if we can not get a user we can not display the page)
   */

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return combineLatest(
      // we are expecting to receive either a user or a user error
      this.store.pipe(select(getUser)),
      this.store.pipe(select(getUserError))
    ).pipe(
      // if user not in store dispatch action
      tap(([user, error]) => {
        if (!user && !error) {
          this.store.dispatch(new RequestUser());
        }
      }),
      // wait for either a user / error
      filter(([user, error]) => user !== undefined || error !== undefined),
      // handle the reponse
      map(([user, error]) => this.handleResponse([user, error], state))
    );
  }

  /**
   * Handles the response from the store (either an error or a user object)
   * If user has not accepted terms redirect to /terms-of-use
   * Otherwise pass through
   * @param userResponse
   */
  private handleResponse([user, error], state: RouterStateSnapshot) {
    if (user && !user.isTsAndCsAccepted && state.url !== '/terms-of-use') {
      this.router.navigateByUrl('/terms-of-use');
      return false;
    }
    return true;
  }
}
