import { LOCALE_ID, Inject } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Injectable } from '@angular/core';
import { IUser, IBillTo, IUpdateProfileParams } from './user.interface';
import { Observable } from 'rxjs';
import { environment } from '@env/environment';
import { IAcceptanceResponse } from '@app/shared/shared.interfaces';
import { AuthTokenService } from '@app/core/auth/auth-token.service';

@Injectable()
export class UserService {
  public AVAILABLE_LOCALES = ['en-US', 'de', 'fr'];
  public localeCookie = document.cookie.match('(^|;) ?locale=([^;]*)(;|$)');

  constructor(@Inject(LOCALE_ID) protected localeId: string, private apiService: ApiService, private authTokenService: AuthTokenService) {}

  /**
   * Returns a clean profile from db (called when a user peforms a login)
   */
  public getUserAfterLogin(): Observable<IUser> {
    return this.apiService.get<IUser>(`${environment.baseUrls.serviceUser}/profile`);
  }

  /**
   * Returns the profile of the authenticated user from cache
   */
  public getUser(): Observable<IUser> {
    return this.apiService.get<IUser>(`${environment.baseUrls.serviceUser}/profile`);
  }

  /**
   * Updated required params of user profile
   */
  public updateProfile(params: IUpdateProfileParams): Observable<IAcceptanceResponse> {
    return this.apiService.patch<IAcceptanceResponse>(`${environment.baseUrls.serviceUser}/profile`, params);
  }

  /**
   * Update selected bill-to account
   */
  public updateBillTo(ebsPersonId: number): Observable<IUser> {
    return this.apiService.put<IUser>(`${environment.baseUrls.serviceUser}/profile/${ebsPersonId}`, {});
  }

  /**
   * Retrieves BillTo info for authenticated user
   */
  public getBillToAccounts(): Observable<IBillTo[]> {
    return this.apiService.get<IBillTo[]>(`${environment.baseUrls.serviceUser}/billtosites`);
  }

  /**
   * Change user language (currently a new app needs to be loaded)
   * @param lang
   */
  public changeLanguage(lang: string) {
    const cookieExpiry = new Date();
    cookieExpiry.setMonth(cookieExpiry.getMonth() + 6);
    document.cookie = `locale=${lang}; expires=${cookieExpiry}; path=/`;
    this.reloadApp();
  }

  /**
   * Check if users needs to change language
   * - locale stored in cookie takes highest priority (this needs to be cookie rather than local storage as read by server)
   * - only change language if language is in the config and does not match the locale of the app
   */
  public checkLanguage(lang: string) {
    if (!this.localeCookie && lang !== 'en' && lang !== this.localeId && this.AVAILABLE_LOCALES.includes(lang)) {
      this.changeLanguage(lang);
    }
  }

  public addClientId(user: IUser) {
    user.clientId = this.authTokenService.getClientId();
  }

  public reloadApp() {
    location.reload();
  }
}
