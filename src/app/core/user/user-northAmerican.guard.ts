import { Injectable } from '@angular/core';
import { IAppState } from '@app/shared/shared.interfaces';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { getUser } from '@app/core/user/store/user.selectors';
import { from } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { RegionUserEnum } from '@app/core/user/user.interface';

@Injectable()
export class NAGuard {
  constructor(private store: Store<IAppState>, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return from(this.store.pipe(select(getUser))).pipe(
      filter(user => user !== undefined),
      map(user => this.handleResponse(user, state))
    );
  }

  private handleResponse(user, state: RouterStateSnapshot) {
    if (user.region !== RegionUserEnum.NAUSER) {
      this.router.navigateByUrl('/dashboard');
      return false;
    }

    return true;
  }
}
