import { IUserState } from '@app/core/user/user.interface';
import { createFeatureSelector, createSelector } from '@ngrx/store';

export const getUserState = createFeatureSelector<IUserState>('user');

export const getUser = createSelector(
  getUserState,
  (userState: IUserState) => userState.profile
);

export const getTermsAccepted = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.isTsAndCsAccepted : false)
);

export const getRedirectUrl = createSelector(
  getUserState,
  (userState: IUserState) => userState.redirectUrl
);

export const getUserError = createSelector(
  getUserState,
  (userState: IUserState) => userState.error
);

export const getUserBillToAccount = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.selectedBillTo : 0)
);

export const getUserShipToAccount = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.selectedShipTo : 0)
);

export const getCurrencyCode = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.currencyCode : null)
);

export const getCurrencyList = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.currencyList : [])
);

export const getUserType = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.userType : 0)
);

export const getUserLoggedIn = createSelector(
  getUserState,
  (userState: IUserState) => {
    return !!userState && !!userState.profile;
  }
);

/**
 * Get company name
 * @type {MemoizedSelector<object, string>}
 */
export const getCompany = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.company : null)
);

/**
 * Get account number
 * @type {MemoizedSelector<object, string>}
 */
export const getAccountNumber = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.accountNumber : null)
);

export const getUserEmail = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile && userState.profile.email ? userState.profile.email : null)
);

export const getSalesRepEmails = createSelector(
  getUserState,
  (userState: IUserState) => {
    const emails: string[] = [];
    // for IN-HOUSE salesRepEmail is blank
    if (userState.profile.contact.salesRepEmail) {
      emails.push(userState.profile.contact.salesRepEmail);
    }
    if (userState.profile.contact.customerServiceEmail) {
      emails.push(userState.profile.contact.customerServiceEmail);
    }
    return emails;
  }
);

/**
 * Get account ID
 * @type {MemoizedSelector<object, number>}
 */
export const getAccountId = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.accountId : null)
);

export const getOrganizationPartyID = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.organizationPartyID : null)
);

export const getUserPartyID = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.userPartyID : null)
);

export const getAccountSiteID = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.accountSiteID : null)
);

export const getUserBillToAccounts = createSelector(
  getUserState,
  (userState: IUserState) => userState.billToAccounts
);

export const getAvailableShipTo = createSelector(
  getUserState,
  (userState: IUserState) => userState.shipToAddress
);

export const getUserAccountTerms = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.paymentsTermName : null)
);

export const getUserAccountName = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.company : null)
);

export const getRegion = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.region : null)
);

export const getCustomerType = createSelector(
  getUserState,
  (userState: IUserState) => (userState.profile ? userState.profile.customerType : null)
);
