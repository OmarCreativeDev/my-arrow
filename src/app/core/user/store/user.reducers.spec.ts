import { HttpErrorResponse } from '@angular/common/http';
import {
  AcceptedTermsComplete,
  RequestUserComplete,
  UserError,
  UpdateBillToComplete,
  UpdateBillToFailed,
  UserLoggedInComplete,
} from './user.actions';
import { userReducers, INITIAL_USER_STATE } from './user.reducers';
import { IUser } from '@app/core/user/user.interface';
import { mockUser } from '@app/core/user/user.service.spec';

describe('User Reducers', () => {
  it(`should set user in store on REQUEST_USER_COMPLETE action`, () => {
    const action = new RequestUserComplete(mockUser);
    const result = userReducers(INITIAL_USER_STATE, action);
    expect(result.profile).toEqual(mockUser);
  });

  it(`should updates T&Cs flag on ACCEPTED_TERMS_COMPLETE action`, () => {
    const initialState = {
      ...INITIAL_USER_STATE,
      profile: mockUser,
    };
    const action = new AcceptedTermsComplete(true);
    const result = userReducers(initialState, action);
    expect(result.profile.isTsAndCsAccepted).toBeTruthy();
  });

  it(`should set error on USER_ERROR action`, () => {
    const action = new UserError(new HttpErrorResponse({ error: { status: 3001 } }));
    const result = userReducers(INITIAL_USER_STATE, action);
    expect(result.error.error.status).toEqual(3001);
  });

  it(`should return the state without transforming it by default `, () => {
    const result = userReducers(undefined, {} as any);
    expect(result).toEqual(INITIAL_USER_STATE);
  });

  it(`should updates users selected Bill-To account on USER_UPDATE_BILL_TO_COMPLETE action`, () => {
    const initialState = {
      ...INITIAL_USER_STATE,
      profile: mockUser,
    };
    const user: IUser = { ...mockUser, selectedBillTo: 123456 };
    const action = new UpdateBillToComplete(user);
    const result = userReducers(initialState, action);
    expect(result.profile.selectedBillTo).toEqual(123456);
  });

  it(`should set error on USER_UPDATE_BILL_TO_FAILED action`, () => {
    const initialState = {
      ...INITIAL_USER_STATE,
      profile: mockUser,
    };
    const action = new UpdateBillToFailed(new HttpErrorResponse({ error: { status: 3002 } }));
    const result = userReducers(initialState, action);
    expect(result.error.error.status).toEqual(3002);
  });

  it(`should set user in store on USER_LOGGED_IN_COMPLETE action`, () => {
    const action = new UserLoggedInComplete(mockUser);
    const result = userReducers(INITIAL_USER_STATE, action);
    expect(result.profile).toEqual(mockUser);
  });
});
