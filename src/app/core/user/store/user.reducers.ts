import { UserActions, UserActionTypes } from './user.actions';
import { IUserState } from '../user.interface';

export const INITIAL_USER_STATE: IUserState = {
  error: undefined,
  profile: undefined,
  billToAccounts: undefined,
  shipToAddress: undefined,
  redirectUrl: 'dashboard',
  analyticsInformation: undefined,
};

export function userReducers(state: IUserState = INITIAL_USER_STATE, action: UserActions): IUserState {
  switch (action.type) {
    case UserActionTypes.REQUEST_USER_COMPLETE: {
      return {
        ...state,
        error: undefined,
        profile: action.payload,
      };
    }

    case UserActionTypes.USER_LOGGED_IN_COMPLETE: {
      return {
        ...state,
        error: undefined,
        profile: action.payload,
      };
    }

    case UserActionTypes.USER_AFTER_LOGGED_IN_COMPLETE: {
      return {
        ...state,
        error: undefined,
        profile: action.payload,
      };
    }

    case UserActionTypes.ACCEPTED_TERMS_COMPLETE: {
      return {
        ...state,
        profile: {
          ...state.profile,
          isTsAndCsAccepted: action.payload,
        },
      };
    }

    case UserActionTypes.USER_UPDATE_BILL_TO_COMPLETE: {
      return {
        ...state,
        profile: action.payload,
      };
    }

    case UserActionTypes.USER_UPDATE_BILL_TO_FAILED: {
      return {
        ...state,
        error: action.payload,
      };
    }

    case UserActionTypes.USER_UPDATE_SHIP_TO_COMPLETE: {
      return {
        ...state,
        profile: {
          ...state.profile,
          selectedShipTo: action.payload,
        },
      };
    }

    case UserActionTypes.USER_UPDATE_CURRENCY_CODE_COMPLETE: {
      return {
        ...state,
        profile: {
          ...state.profile,
          currencyCode: action.payload,
        },
      };
    }

    case UserActionTypes.RESET_USER: {
      return {
        ...INITIAL_USER_STATE,
        error: state.error,
        redirectUrl: action.payload,
      };
    }

    case UserActionTypes.USER_ERROR: {
      return {
        ...state,
        error: action.payload,
      };
    }

    case UserActionTypes.REQUEST_BILL_TO_ACCOUNTS_SUCCESS: {
      return {
        ...state,
        billToAccounts: {
          error: undefined,
          items: action.payload,
        },
      };
    }

    case UserActionTypes.REQUEST_BILL_TO_ACCOUNTS_FAILED: {
      return {
        ...state,
        billToAccounts: {
          error: action.payload,
          items: undefined,
        },
      };
    }

    case UserActionTypes.REQUEST_AVAILABLE_SHIPTO: {
      return {
        ...state,
        shipToAddress: {
          error: undefined,
          items: undefined,
          loading: true,
        },
      };
    }

    case UserActionTypes.REQUEST_AVAILABLE_SHIPTO_SUCCESS: {
      return {
        ...state,
        shipToAddress: {
          error: undefined,
          items: action.payload,
          loading: false,
        },
      };
    }

    case UserActionTypes.REQUEST_AVAILABLE_SHIPTO_FAILED: {
      return {
        ...state,
        billToAccounts: {
          error: action.payload,
          items: undefined,
          loading: false,
        },
      };
    }

    case UserActionTypes.CLEAR_USER_ERROR: {
      return {
        ...state,
        error: undefined,
      };
    }

    case UserActionTypes.USER_LOGGED_OUT_COMPLETE: {
      return {
        ...state,
        analyticsInformation: { ...state.analyticsInformation, logOutReason: action.payload },
      };
    }
    case UserActionTypes.USER_LOGGED_OUT_FAILED:
    case UserActionTypes.USER_AFTER_LOGGED_IN_FAILED:
    case UserActionTypes.TOKEN_REFRESH:
    case UserActionTypes.WEB_SOCKET_CONNECT:
    case UserActionTypes.WEB_SOCKET_RECONNECT:
    case UserActionTypes.WEB_SOCKET_DISCONNECT:

    default: {
      return state;
    }
  }
}
