import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { IUser, IBillTo } from '@app/core/user/user.interface';
import { IShipTo } from '@app/core/checkout/checkout.interfaces';

export enum UserActionTypes {
  ACCEPTED_TERMS = 'ACCEPTED_TERMS',
  ACCEPTED_TERMS_COMPLETE = 'ACCEPTED_TERMS_COMPLETE',
  REDIRECT = 'REDIRECT',
  REQUEST_USER = 'REQUEST_USER',
  REQUEST_USER_COMPLETE = 'REQUEST_USER_COMPLETE',
  RESET_USER = 'RESET USER',
  USER_ERROR = 'USER_ERROR',
  USER_UPDATE_BILL_TO = 'USER_UPDATE_BILL_TO',
  USER_UPDATE_BILL_TO_COMPLETE = 'USER_UPDATE_BILL_TO_COMPLETE',
  USER_UPDATE_BILL_TO_FAILED = 'USER_UPDATE_BILL_TO_FAILED',
  USER_UPDATE_CURRENCY_CODE = 'USER_UPDATE_CURRENCY_CODE',
  USER_UPDATE_CURRENCY_CODE_COMPLETE = 'USER_UPDATE_CURRENCY_CODE_COMPLETE',
  USER_UPDATE_SHIP_TO = 'USER_UPDATE_SHIP_TO',
  USER_UPDATE_SHIP_TO_COMPLETE = 'USER_UPDATE_SHIP_TO_COMPLETE',
  USER_LOGGED_IN = 'USER_LOGGED_IN',
  USER_LOGGED_IN_COMPLETE = 'USER_LOGGED_IN_COMPLETE',
  USER_LOGGED_IN_FAILED = 'USER_LOGGED_IN_FAILED',
  USER_AFTER_LOGGED_IN = 'USER_AFTER_LOGGED_IN',
  USER_AFTER_LOGGED_IN_COMPLETE = 'USER_AFTER_LOGGED_IN_COMPLETE',
  USER_AFTER_LOGGED_IN_FAILED = 'USER_AFTER_LOGGED_IN_FAILED',
  REQUEST_BILL_TO_ACCOUNTS = 'REQUEST_BILL_TO_ACCOUNTS',
  REQUEST_BILL_TO_ACCOUNTS_SUCCESS = 'REQUEST_BILL_TO_ACCOUNTS_SUCCESS',
  REQUEST_BILL_TO_ACCOUNTS_FAILED = 'REQUEST_BILL_TO_ACCOUNTS_FAILED',
  REQUEST_AVAILABLE_SHIPTO = 'REQUEST_AVAILABLE_SHIPTO',
  REQUEST_AVAILABLE_SHIPTO_SUCCESS = 'REQUEST_AVAILABLE_SHIPTO_SUCCESS',
  REQUEST_AVAILABLE_SHIPTO_FAILED = 'REQUEST_AVAILABLE_SHIPTO_FAILED',
  USER_PASSWORD_RESET = 'USER_PASSWORD_RESET',
  CLEAR_USER_ERROR = 'CLEAR_USER_ERROR',
  USER_LOGGED_OUT = 'USER_LOGGED_OUT',
  USER_LOGGED_OUT_COMPLETE = 'USER_LOGGED_OUT_COMPLETE',
  USER_LOGGED_OUT_FAILED = 'USER_LOGGED_OUT_FAILED',
  TOKEN_REFRESH = 'TOKEN_REFRESH',
  WEB_SOCKET_CONNECT = 'WEB_SOCKET_CONNECT',
  WEB_SOCKET_RECONNECT = 'WEB_SOCKET_RECONNECT',
  WEB_SOCKET_DISCONNECT = 'WEB_SOCKET_DISCONNECT',
}

export class RequestUser implements Action {
  readonly type = UserActionTypes.REQUEST_USER;
  constructor() { }
}

export class RequestUserComplete implements Action {
  readonly type = UserActionTypes.REQUEST_USER_COMPLETE;
  constructor(public payload: IUser) { }
}

export class ResetUser implements Action {
  readonly type = UserActionTypes.RESET_USER;
  constructor(public payload = 'dashboard') { }
}

export class AcceptedTerms implements Action {
  readonly type = UserActionTypes.ACCEPTED_TERMS;
  constructor(public payload: boolean) { }
}

export class AcceptedTermsComplete implements Action {
  readonly type = UserActionTypes.ACCEPTED_TERMS_COMPLETE;
  constructor(public payload: boolean) { }
}

export class UserError implements Action {
  readonly type = UserActionTypes.USER_ERROR;
  constructor(public payload: HttpErrorResponse) { }
}

export class Redirect implements Action {
  readonly type = UserActionTypes.REDIRECT;
  constructor() { }
}

export class UpdateBillTo implements Action {
  readonly type = UserActionTypes.USER_UPDATE_BILL_TO;
  constructor(public payload: number) { }
}

export class UpdateBillToFailed implements Action {
  readonly type = UserActionTypes.USER_UPDATE_BILL_TO_FAILED;
  constructor(public payload: HttpErrorResponse) { }
}

export class UpdateBillToComplete implements Action {
  readonly type = UserActionTypes.USER_UPDATE_BILL_TO_COMPLETE;
  constructor(public payload: IUser) { }
}

export class UpdateShipTo implements Action {
  readonly type = UserActionTypes.USER_UPDATE_SHIP_TO;
  constructor(public payload: number) { }
}

export class UpdateShipToComplete implements Action {
  readonly type = UserActionTypes.USER_UPDATE_SHIP_TO_COMPLETE;
  constructor(public payload: number) { }
}

export class UpdateCurrencyCode implements Action {
  readonly type = UserActionTypes.USER_UPDATE_CURRENCY_CODE;
  constructor(public payload: string) { }
}

export class UpdateCurrencyCodeComplete implements Action {
  readonly type = UserActionTypes.USER_UPDATE_CURRENCY_CODE_COMPLETE;
  constructor(public payload: string) { }
}

export class UserLoggedIn implements Action {
  readonly type = UserActionTypes.USER_LOGGED_IN;
  constructor() { }
}

export class UserLoggedInComplete implements Action {
  readonly type = UserActionTypes.USER_LOGGED_IN_COMPLETE;
  constructor(public payload: IUser) { }
}

export class UserLoggedInFailed implements Action {
  readonly type = UserActionTypes.USER_LOGGED_IN_FAILED;
  constructor(public payload: string | HttpErrorResponse) { }
}

export class UserAfterLoggedIn implements Action {
  readonly type = UserActionTypes.USER_AFTER_LOGGED_IN;
  constructor() { }
}

export class UserAfterLoggedInComplete implements Action {
  readonly type = UserActionTypes.USER_AFTER_LOGGED_IN_COMPLETE;
  constructor(public payload: IUser) { }
}

export class UserAfterLoggedInFailed implements Action {
  readonly type = UserActionTypes.USER_AFTER_LOGGED_IN_FAILED;
  constructor(public payload: string) { }
}

export class RequestBillToAccounts implements Action {
  readonly type = UserActionTypes.REQUEST_BILL_TO_ACCOUNTS;
}

export class RequestBillToAccountsSuccess implements Action {
  readonly type = UserActionTypes.REQUEST_BILL_TO_ACCOUNTS_SUCCESS;
  constructor(public payload: IBillTo[]) { }
}

export class RequestBillToAccountsFailed implements Action {
  readonly type = UserActionTypes.REQUEST_BILL_TO_ACCOUNTS_FAILED;
  constructor(public payload: HttpErrorResponse) { }
}

export class RequestAvailableShipTo implements Action {
  readonly type = UserActionTypes.REQUEST_AVAILABLE_SHIPTO;
  constructor(public billToId?: number) { }
}

export class RequestAvailableShipToSuccess implements Action {
  readonly type = UserActionTypes.REQUEST_AVAILABLE_SHIPTO_SUCCESS;
  constructor(public payload: IShipTo[]) { }
}

export class RequestAvailableShipToFailed implements Action {
  readonly type = UserActionTypes.REQUEST_AVAILABLE_SHIPTO_FAILED;
  constructor(public payload: HttpErrorResponse) { }
}

export class PasswordResetSuccess implements Action {
  readonly type = UserActionTypes.USER_PASSWORD_RESET;
  constructor(public payload: string) { }
}

export class ClearUserError implements Action {
  readonly type = UserActionTypes.CLEAR_USER_ERROR;
  constructor() { }
}

export class UserLoggedOut implements Action {
  readonly type = UserActionTypes.USER_LOGGED_OUT;
  constructor(public payload: string) { }
}

export class UserLoggedOutSuccess implements Action {
  readonly type = UserActionTypes.USER_LOGGED_OUT_COMPLETE;
  constructor(public payload: string) { }
}

export class UserLoggedOutFailed implements Action {
  readonly type = UserActionTypes.USER_LOGGED_OUT_FAILED;
  constructor(public error: Error) { }
}

export class TokenRefresh implements Action {
  readonly type = UserActionTypes.TOKEN_REFRESH;
  constructor() { }
}

export class WebSocketConnect implements Action {
  readonly type = UserActionTypes.WEB_SOCKET_CONNECT;
  constructor() { }
}

export class WebSocketReconnect implements Action {
  readonly type = UserActionTypes.WEB_SOCKET_RECONNECT;
  constructor() { }
}

export class WebSocketDisconnect implements Action {
  readonly type = UserActionTypes.WEB_SOCKET_DISCONNECT;
  constructor() { }
}

export type UserActions =
  | AcceptedTerms
  | AcceptedTermsComplete
  | Redirect
  | RequestUser
  | RequestUserComplete
  | ResetUser
  | UpdateBillTo
  | UpdateBillToFailed
  | UpdateBillToComplete
  | UpdateCurrencyCode
  | UpdateCurrencyCodeComplete
  | UpdateShipTo
  | UpdateShipToComplete
  | UserError
  | UserLoggedIn
  | UserLoggedInComplete
  | UserLoggedInFailed
  | UserAfterLoggedIn
  | UserAfterLoggedInComplete
  | UserAfterLoggedInFailed
  | RequestBillToAccounts
  | RequestBillToAccountsSuccess
  | RequestBillToAccountsFailed
  | RequestAvailableShipTo
  | RequestAvailableShipToSuccess
  | RequestAvailableShipToFailed
  | PasswordResetSuccess
  | ClearUserError
  | UserLoggedOut
  | UserLoggedOutSuccess
  | UserLoggedOutFailed
  | TokenRefresh
  | WebSocketConnect
  | WebSocketReconnect
  | WebSocketDisconnect;
