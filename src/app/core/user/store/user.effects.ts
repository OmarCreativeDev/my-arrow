import {
  AcceptedTerms,
  AcceptedTermsComplete,
  Redirect,
  RequestUser,
  RequestUserComplete,
  ResetUser,
  UpdateBillTo,
  UpdateBillToComplete,
  UpdateBillToFailed,
  UpdateCurrencyCode,
  UpdateCurrencyCodeComplete,
  UpdateShipTo,
  UpdateShipToComplete,
  UserActionTypes,
  UserError,
  UserLoggedIn,
  UserLoggedInComplete,
  RequestBillToAccountsSuccess,
  RequestBillToAccountsFailed,
  UserLoggedInFailed,
  UserAfterLoggedIn,
  UserAfterLoggedInComplete,
  UserAfterLoggedInFailed,
  RequestAvailableShipTo,
  RequestAvailableShipToSuccess,
  RequestAvailableShipToFailed,
  UserLoggedOut,
  UserLoggedOutSuccess,
  UserLoggedOutFailed,
} from './user.actions';

import { AuthService } from '@app/core/auth/auth.service';
import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { getRedirectUrl } from '@app/core/user/store/user.selectors';
import { Injectable } from '@angular/core';
import { IAppState } from '@app/shared/shared.interfaces';
import { IUser, UserServiceHttpExceptionStatusEnum } from '@app/core/user/user.interface';
import { map, switchMap, catchError, withLatestFrom } from 'rxjs/operators';
import { from, Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { UserService } from '@app/core/user/user.service';
import { isUserNotReady } from '../user.utils';
import { CheckoutService } from '@app/core/checkout/checkout.service';
import { GetPrivateProperties, GetPublicProperties } from '@app/features/properties/store/properties.actions';

@Injectable()
export class UserEffects {
  @Effect()
  getUser: Observable<any> = this.actions$.pipe(
    ofType<RequestUser>(UserActionTypes.REQUEST_USER),
    switchMap(() => {
      return this.userService.getUser().pipe(
        switchMap((user: IUser) => {
          this.userService.checkLanguage(user.defaultLanguage);
          this.userService.addClientId(user);
          return [
            new GetPrivateProperties(`private/${user.registrationInstance.toLocaleLowerCase()}`),
            new GetPublicProperties(),
            new RequestUserComplete(user),
          ];
        }),
        catchError(err => of(new UserError(err)))
      );
    })
  );

  @Effect({ dispatch: false })
  resetUser: Observable<any> = this.actions$.pipe(
    ofType<ResetUser>(UserActionTypes.RESET_USER),
    map(action => {
      this.authTokenService.resetLocalStorage();
      this.router.navigateByUrl('login');
    })
  );

  @Effect()
  isTermsAccepted: Observable<any> = this.actions$.pipe(
    ofType<AcceptedTerms>(UserActionTypes.ACCEPTED_TERMS),
    switchMap(() => {
      return this.userService.updateProfile({ isTsAndCsAccepted: true }).pipe(
        map((result: any) => {
          return new AcceptedTermsComplete(true);
        }),
        catchError(err => of(new UserError(err)))
      );
    })
  );

  @Effect()
  updateBillTo: Observable<any> = this.actions$.pipe(
    ofType<UpdateBillTo>(UserActionTypes.USER_UPDATE_BILL_TO),
    switchMap((action: UpdateBillTo) => {
      return this.userService.updateBillTo(action.payload).pipe(
        map((user: IUser) => {
          this.userService.checkLanguage(user.defaultLanguage);
          return new UpdateBillToComplete(user);
        }),
        catchError(err => of(new UpdateBillToFailed(err)))
      );
    })
  );

  @Effect()
  updateShipTo: Observable<any> = this.actions$.pipe(
    ofType<UpdateShipTo>(UserActionTypes.USER_UPDATE_SHIP_TO),
    switchMap((action: UpdateShipTo) => {
      return this.userService.updateProfile({ selectedShipTo: action.payload }).pipe(
        map(() => {
          return new UpdateShipToComplete(action.payload);
        }),
        catchError(err => of(new UserError(err)))
      );
    })
  );

  @Effect()
  updateCurrencyCode: Observable<any> = this.actions$.pipe(
    ofType<UpdateCurrencyCode>(UserActionTypes.USER_UPDATE_CURRENCY_CODE),
    switchMap((action: UpdateCurrencyCode) => {
      return this.userService.updateProfile({ currencyCode: action.payload }).pipe(
        map(() => {
          return new UpdateCurrencyCodeComplete(action.payload);
        }),
        catchError(err => of(new UserError(err)))
      );
    })
  );

  @Effect({ dispatch: false })
  redirect: Observable<any> = this.actions$.pipe(
    ofType<Redirect>(UserActionTypes.REDIRECT),
    withLatestFrom(this.store.pipe(select(getRedirectUrl))),
    map(([action, redirectUrl]) => {
      this.router.navigateByUrl(redirectUrl);
    })
  );

  @Effect()
  loggedIn: Observable<any> = this.actions$.pipe(
    ofType<UserLoggedIn>(UserActionTypes.USER_LOGGED_IN),
    switchMap((action: UserLoggedIn) => {
      return this.userService.getUserAfterLogin().pipe(
        switchMap((user: IUser) => {
          this.userService.addClientId(user);
          return [
            new GetPrivateProperties(`private/${user.registrationInstance.toLocaleLowerCase()}`),
            new UserLoggedInComplete(user),
            new GetPublicProperties(),
          ];
        }),
        catchError(err => {
          return from([new UserError(err), new UserLoggedInFailed(err)]);
        })
      );
    })
  );

  @Effect()
  loggedInComplete: Observable<any> = this.actions$.pipe(
    ofType<UserLoggedInComplete>(UserActionTypes.USER_LOGGED_IN_COMPLETE),
    map(() => new Redirect())
  );

  @Effect()
  afterLoggedIn: Observable<any> = this.actions$.pipe(
    ofType<UserAfterLoggedIn>(UserActionTypes.USER_AFTER_LOGGED_IN),
    switchMap(() => {
      return this.userService.getUserAfterLogin().pipe(
        switchMap((user: IUser) => {
          this.userService.addClientId(user);
          return [
            new GetPrivateProperties(`private/${user.registrationInstance.toLocaleLowerCase()}`),
            new GetPublicProperties(),
            new UserAfterLoggedInComplete(user),
          ];
        }),
        catchError(err => {
          return from([new UserError(err), new UserAfterLoggedInFailed(`User Error (${err.error.status})`)]);
        })
      );
    })
  );

  @Effect()
  userAfterLoggedInComplete: Observable<any> = this.actions$.pipe(
    ofType<UserAfterLoggedInComplete>(UserActionTypes.USER_AFTER_LOGGED_IN_COMPLETE),
    map(() => new Redirect())
  );

  @Effect({ dispatch: false })
  catchError: Observable<any> = this.actions$.pipe(
    ofType<UserError>(UserActionTypes.USER_ERROR),
    map(action => {
      if (action.payload.status === UserServiceHttpExceptionStatusEnum.UNAUTHORIZED_ERROR) {
        return;
      }
      if (isUserNotReady(action.payload)) {
        this.authService.logout();
      } else {
        this.router.navigateByUrl('user-error');
      }
    })
  );

  @Effect()
  getBillToAccounts = this.actions$.pipe(
    ofType<RequestUser>(UserActionTypes.REQUEST_BILL_TO_ACCOUNTS),
    switchMap(() => {
      return this.userService.getBillToAccounts().pipe(
        map(billToAccounts => {
          return new RequestBillToAccountsSuccess(billToAccounts);
        }),
        catchError(err => of(new RequestBillToAccountsFailed(err)))
      );
    })
  );

  @Effect()
  requestAvailableShipTo = this.actions$.pipe(
    ofType<RequestAvailableShipTo>(UserActionTypes.REQUEST_AVAILABLE_SHIPTO),
    switchMap((action: RequestAvailableShipTo) => {
      return this.checkoutService.getAddressList(action.billToId).pipe(
        map(shipToAddresses => {
          return new RequestAvailableShipToSuccess(shipToAddresses);
        }),
        catchError(err => of(new RequestAvailableShipToFailed(err)))
      );
    })
  );

  @Effect()
  requestLogout$ = this.actions$.pipe(
    ofType<UserLoggedOut>(UserActionTypes.USER_LOGGED_OUT),
    switchMap((action: UserLoggedOut) => {
      return this.authService.logout().pipe(
        map(() => {
          return new UserLoggedOutSuccess(action.payload);
        }),
        catchError(err => of(new UserLoggedOutFailed(err)))
      );
    })
  );

  constructor(
    private actions$: Actions,
    private authService: AuthService,
    private authTokenService: AuthTokenService,
    private router: Router,
    private store: Store<IAppState>,
    private checkoutService: CheckoutService,
    private userService: UserService,
  ) { }
}
