import { TestBed } from '@angular/core/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { RequestUserComplete, AcceptedTermsComplete, UserError, UpdateBillToComplete } from './user.actions';
import { userReducers } from './user.reducers';
import { IUser } from '@app/core/user/user.interface';
import { Store, StoreModule, select } from '@ngrx/store';
import { mockUser } from '@app/core/user/user.service.spec';
import {
  getUser,
  getTermsAccepted,
  getUserError,
  getRedirectUrl,
  getUserBillToAccount,
  getAccountId,
  getUserAccountTerms,
  getUserAccountName,
  getRegion,
  getSalesRepEmails,
} from '@app/core/user/store/user.selectors';

describe('User Selectors', () => {
  let store: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          user: userReducers,
        }),
      ],
    });

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it(`should select getUser slice`, () => {
    store.dispatch(new RequestUserComplete(mockUser));
    store.pipe(select(getUser)).subscribe(user => {
      expect(user).toEqual(mockUser);
    });
  });

  it(`should select getTermsAccepted slice`, () => {
    store.dispatch(new AcceptedTermsComplete(true));
    store.pipe(select(getTermsAccepted)).subscribe(termsAccepted => {
      expect(termsAccepted).toBeTruthy();
    });
  });

  it(`should select getRedirectUrl slice`, () => {
    store.pipe(select(getRedirectUrl)).subscribe(redirectUrl => {
      expect(redirectUrl).toEqual('dashboard');
    });
  });

  it(`should select getUserBillToAccount slice`, () => {
    const user: IUser = { ...mockUser, selectedBillTo: 12345 };
    store.dispatch(new UpdateBillToComplete(user));
    store.pipe(select(getUserBillToAccount)).subscribe(billToAccount => {
      expect(billToAccount).toBeTruthy(12345);
    });
  });

  it(`should select getUserError slice`, () => {
    store.dispatch(new UserError(new HttpErrorResponse({ error: { status: 3001 } })));
    store.pipe(select(getUserError)).subscribe(error => {
      expect(error.error.status).toEqual(3001);
    });
  });

  it(`should select getAccountId slice`, () => {
    store.dispatch(new RequestUserComplete(mockUser));
    store.pipe(select(getAccountId)).subscribe(accountId => {
      expect(accountId).toEqual(mockUser.accountId);
    });
  });

  it(`should select getUserAccountTerms slice`, () => {
    store.dispatch(new RequestUserComplete(mockUser));
    store.pipe(select(getUserAccountTerms)).subscribe(accountTerm => {
      expect(accountTerm).toEqual(mockUser.paymentsTermName);
    });
  });

  it(`should select getUserAccountName slice`, () => {
    store.dispatch(new RequestUserComplete(mockUser));
    store.pipe(select(getUserAccountName)).subscribe(company => {
      expect(company).toEqual(mockUser.company);
    });
  });

  it(`should select getRegion slice`, () => {
    store.dispatch(new RequestUserComplete(mockUser));
    store.pipe(select(getRegion)).subscribe(region => {
      expect(region).toEqual(mockUser.region);
    });
  });

  it(`should select getRegion slice`, () => {
    store.dispatch(new RequestUserComplete(mockUser));
    store.pipe(select(getSalesRepEmails)).subscribe(emails => {
      expect(emails).toEqual([mockUser.contact.salesRepEmail, mockUser.contact.customerServiceEmail]);
    });
  });
});
