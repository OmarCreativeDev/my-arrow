import { IException, IListingState } from '@app/shared/shared.interfaces';
import { IShipTo } from '../checkout/checkout.interfaces';

export interface IUser {
  accountId: number;
  accountSiteID: number;
  organizationPartyID: number;
  accountNumber: number;
  company: string;
  contact: IContact;
  country: string;
  currencyCode: string;
  currencyList: Array<string>;
  customerType: string;
  defaultLanguage: string;
  email: string;
  firstName: string;
  internalUserId: number;
  isTsAndCsAccepted: boolean;
  lastName: string;
  orgId: number;
  paymentsTermDescription: string;
  paymentsTermId: number;
  paymentsTermName: string;
  permissionList: Array<string>;
  region: string;
  registrationInstance: string;
  selectedBillTo: number;
  selectedShipTo: number;
  userType: number;
  warehouses: Array<IWarehouse>;
  phoneNumber?: string;
  userPartyID: number;
  clientId?: ClientId | string;
}

export interface IWarehouse {
  id: number;
  code: string;
  arrowReel: boolean;
}

export interface IContact {
  customerServiceEmail: string;
  customerServiceNumber: string;
  salesRepName: string;
  salesRepPhoneNumber: string;
  salesRepEmail: string;
}

export interface IBillTo {
  name: string;
  accountId: number;
  accountNumber: number;
  ebsPersonId: number;
  billToId: number;
  orgId: number;
  address1: string;
  address2: string;
  address3: string;
  address4: string;
  city: string;
  state: string;
  postalCode: string;
  province: string;
  country: string;
  countryDescription: string;
  selected: boolean;
}

export interface IUserState {
  error?: IException;
  profile: IUser;
  billToAccounts: IListingState<IBillTo>;
  shipToAddress: IListingState<IShipTo>;
  redirectUrl: string;
  analyticsInformation: IAnalyticsInformation;
}

export interface IAnalyticsInformation {
  logOutReason: string;
}

export interface IUpdateProfileParams {
  isTsAndCsAccepted?: boolean;
  selectedBillTo?: number;
  selectedShipTo?: number;
  currencyCode?: string;
}

export enum UserServiceHttpExceptionStatusEnum {
  RESET_PASSWORD_LINK_EXPIRED = '1007',
  RESET_PASSWORD_LINK_USED = '1008',
  SERVICE_NOT_FOUND_EXCEPTION = '3001',
  FORBIDDEN_EXCEPTION = '3002',
  REPOSITORY_EXCEPTION = '3003',
  USER_LEGACY_NOT_FOUND = '3004',
  LEGACY_INTEGRATION_FAILED = '3005',
  INVALID_REQUEST_PARAMETER = '3006',
  FEATURE_NOT_IMPLEMENTED = '3007',
  INVALID_REQUEST = '3008',
  TRANSFORMING_EXCEPTION = '3009',
  BAD_REQUEST_EXCEPTION = '3010',
  INTERNAL_SERVER_ERROR = 500,
  SERVICE_UNAVAILABLE = 503,
  UNAUTHORIZED_ERROR = 401,
}

export enum RegionUserEnum {
  NAUSER = 'arrowna',
}

export enum ClientId {
  MYARROWUI = 'MyArrowUI',
  MYADMINUI = 'MyAdminUI',
}

export enum UserServiceHttpExceptionMessage {
  USER_NOT_FOUND_IN_LEGACY_DB = 'User not found in Legacy DB',
  USER_NOT_FOUND = 'User Not Found',
}

export enum UserCustomerType {
  BROKER = 'BROKER',
  CEM = 'CEM',
  OEM = 'OEM',
  OED = 'OED',
  INT = 'INT',
}
