import { Injectable } from '@angular/core';
import { ApiService } from '@app/core/api/api.service';
import { Observable } from 'rxjs';
import { ICustomerBroadcastMessage } from '@app/core/customer-broadcast/customer-broadcast.interface';
import { environment } from '@env/environment';
import { IUser } from '@app/core/user/user.interface';

@Injectable()
export class CustomerBroadcastService {
  constructor(private apiService: ApiService) {}

  public getMessage(user: IUser): Observable<ICustomerBroadcastMessage> {
    const { region, defaultLanguage } = user;
    return this.apiService.get<ICustomerBroadcastMessage>(
      `${environment.baseUrls.serviceCustomerBroadcast}/message/${region}/${defaultLanguage}`
    );
  }

  public muteMessage(messageId: string): Observable<void> {
    return this.apiService.post<void>(`${environment.baseUrls.serviceCustomerBroadcast}/message/mute`, {
      messageId: messageId,
    });
  }
}
