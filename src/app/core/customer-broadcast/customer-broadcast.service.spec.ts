import { TestBed, inject } from '@angular/core/testing';
import { CustomerBroadcastService } from './customer-broadcast.service';
import { ApiService } from '@app/core/api/api.service';
import { CoreModule } from '@app/core/core.module';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { ICustomerBroadcastMessage } from '@app/core/customer-broadcast/customer-broadcast.interface';
import { IUser } from '../user/user.interface';
import { of } from 'rxjs';

describe('CustomerBroadcastService', () => {
  let apiService: ApiService;
  const mockedCustomerBroadcastMessage: ICustomerBroadcastMessage = {
    id: 'id',
    title: 'title',
    content: 'test content',
  };

  const user = {
    defaultLanguage: 'en',
    region: 'arrowna',
  } as IUser;

  const messageId = 'testMessageId';

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule],
      providers: [CustomerBroadcastService, { provide: ApiService, useClass: ApiServiceMock }],
    });
    apiService = TestBed.get(ApiService);
  });

  it('should be created', inject([CustomerBroadcastService], (service: CustomerBroadcastService) => {
    expect(service).toBeTruthy();
  }));

  it('getMessage() method should return an Observable<ICustomerBroadcastMessage>', inject(
    [CustomerBroadcastService],
    (service: CustomerBroadcastService) => {
      spyOn(apiService, 'get').and.returnValue(of(mockedCustomerBroadcastMessage));
      service.getMessage(user).subscribe(customerBroadcastMessage => {
        for (const key of Object.keys(customerBroadcastMessage)) {
          expect(customerBroadcastMessage[key]).toEqual(mockedCustomerBroadcastMessage[key]);
        }
      });
    }
  ));

  it('muteMessage() method should return an Observable<void>', inject([CustomerBroadcastService], (service: CustomerBroadcastService) => {
    spyOn(apiService, 'post').and.returnValue(of());
    service.muteMessage(messageId).subscribe(data => {
      expect(data).toBeNull();
    });
  }));
});
