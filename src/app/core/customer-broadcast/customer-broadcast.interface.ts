export interface ICustomerBroadcastMessage {
  id: string;
  title: string;
  content: string;
}
