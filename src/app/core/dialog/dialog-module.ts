import { DialogService } from '@app/core/dialog/dialog.service';
import { OVERLAY_CONTAINER_PROVIDER } from '@app/core/dialog/overlay-container';
import { NgModule } from '@angular/core';

@NgModule({
  providers: [
    DialogService,
    OVERLAY_CONTAINER_PROVIDER,
  ],
})
export class DialogModule {}
