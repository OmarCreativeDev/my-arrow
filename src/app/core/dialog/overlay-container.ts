import { Inject, Injectable, InjectionToken, OnDestroy, Optional, SkipSelf } from '@angular/core';
import { DOCUMENT } from '@angular/common';

@Injectable()
export class OverlayContainer implements OnDestroy {

  private _containerElement: HTMLElement;
  private _elements: HTMLElement[] = [];

  constructor(@Inject(DOCUMENT) private _document: any) {
    const element = this._document.createElement('div');
    element.classList.add('app-overlay-container');
    this._document.body.appendChild(element);
    this._containerElement = element;
  }

  ngOnDestroy() {
    /* istanbul ignore else */
    if (this._containerElement && this._containerElement.parentNode) {
      this._containerElement.parentNode.removeChild(this._containerElement);
    }
  }

  createAndAddElement(tag: string, classes: string[] = []): HTMLElement {
    const element = this._document.createElement(tag);
    // IE11 only accepts a single parameter to classList.add(className), so call it once for each className:
    classes.forEach(className => element.classList.add(className));
    this._containerElement.appendChild(element);

    this._elements.push(element);

    /* istanbul ignore else */
    if (this._elements.length === 1) {
      // Use a setTimeout, in case another container framework is calling this one,
      // and it removes the class as it finishes.
      setTimeout(() => this._document.body.classList.add('backdrop-open'));
    }

    return element;
  }

  removeElement(element: HTMLElement) {
    const index = this._elements.indexOf(element);

    if (index === -1) { return; }

    this._containerElement.removeChild(element);

    this._elements.splice(index, 1);

    /* istanbul ignore else */
    if (!this._elements.length) {
      this._document.body.classList.remove('backdrop-open');
    }
  }

}

export function OVERLAY_CONTAINER_PROVIDER_FACTORY(
  parentContainer: OverlayContainer,
  _document: any
  ) {
  return parentContainer || new OverlayContainer(_document);
}

export const OVERLAY_CONTAINER_PROVIDER = {
  // If there is already an OverlayContainer available, use that. Otherwise, provide a new one.
  provide: OverlayContainer,
  deps: [
    [new Optional(), new SkipSelf(), OverlayContainer],
    DOCUMENT as InjectionToken<any> // We need to use the InjectionToken somewhere to keep TS happy
  ],
  useFactory: OVERLAY_CONTAINER_PROVIDER_FACTORY
};
