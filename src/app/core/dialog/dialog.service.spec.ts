import { async, TestBed } from '@angular/core/testing';
import { Dialog, DialogService } from '@app/core/dialog/dialog.service';
import { OVERLAY_CONTAINER_PROVIDER } from '@app/core/dialog/overlay-container';
import { of } from 'rxjs';
import { Component } from '@angular/core';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';

export class DialogMock {
  close() {}
  get afterClosed() {
    return of();
  }
}

export class DialogServiceMock {
  open() {
    return new DialogMock();
  }
}

// TestComponent to be used for creating a Dialog
@Component({ template: '' })
class TestComponent {}

describe('DialogService', () => {
  let dialogService: DialogService;
  let dialog: Dialog<TestComponent>;

  let backdropElement: HTMLElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestComponent],
      providers: [DialogService, OVERLAY_CONTAINER_PROVIDER],
    })
      // Must set TestComponent as an entryComponent
      // See https://stackoverflow.com/questions/41483841/providing-entrycomponents-for-a-testbed
      .overrideModule(BrowserDynamicTestingModule, {
        set: {
          entryComponents: [TestComponent],
        },
      });

    dialogService = TestBed.get(DialogService);
    dialog = dialogService.open(TestComponent, { closable: true });

    backdropElement = (dialog as any)._backdropElement;
  });

  it('should be created', () => {
    expect(dialogService).toBeTruthy();
  });

  it('should return a Dialog when open is called', () => {
    expect(dialog).toBeTruthy();
  });

  it('should emit afterClosed event when a Dialog is closed', async(() => {
    const testResult = {};
    dialog.afterClosed.subscribe(result => expect(result).toEqual(testResult));
    dialog.close(testResult);
  }));

  it('should dismiss the Dialog when the backdrop is clicked', () => {
    dialog.afterClosed.subscribe(result => expect(result).toBeUndefined());
    backdropElement.dispatchEvent(new Event('click'));
  });
});
