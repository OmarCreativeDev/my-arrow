export type DialogSize = 'small' | 'medium' | 'large' | 'x-large' | 'large-x-large';

export class DialogConfig<D = any> {
  /** Whether the dialog has a backdrop. */
  hasBackdrop?: boolean = true;

  /** Whether the user can click outside to onDismiss a modal. */
  closable?: boolean = false;

  /** Position overrides. */
  size?: DialogSize = 'medium';

  /** Data being injected into the child component. */
  data?: D | null = null;
}
