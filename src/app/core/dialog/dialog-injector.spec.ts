import { DialogInjector } from '@app/core/dialog/dialog-injector';

describe('DialogInjector', () => {

  let dialogInjector: DialogInjector;

  const token = {};
  const result = 'result';

  beforeEach(() => {
    const customTokens = new WeakMap();
    customTokens.set(token, result);

    dialogInjector = new DialogInjector({get: () => null}, customTokens);
  });

  it('should create', () => {
    expect(dialogInjector).toBeTruthy();
  });

  it('should return a set token', () => {
    expect(dialogInjector.get(token)).toEqual(result);
  });

  it('should return null for any token that has not been set', () => {
    expect(dialogInjector.get({})).toBeNull();
  });

});
