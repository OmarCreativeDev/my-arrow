import { TestBed } from '@angular/core/testing';
import { OVERLAY_CONTAINER_PROVIDER, OverlayContainer } from '@app/core/dialog/overlay-container';

describe('OverlayContainer', () => {

  let overlayContainer: OverlayContainer;
  let containerElement: HTMLElement;

  beforeEach(() => {

    TestBed.configureTestingModule({
      providers: [
       OVERLAY_CONTAINER_PROVIDER,
      ],
    });

    overlayContainer = TestBed.get(OverlayContainer);
    containerElement = (overlayContainer as any)._containerElement;

  });

  it('should be created', () => {
    expect(overlayContainer).toBeTruthy();
  });

  it ('should add the container element when created', () => {
    expect(containerElement).toBeTruthy();
  });

  it('should remove the container element when destroyed', () => {
    overlayContainer.ngOnDestroy();
    expect(containerElement.parentNode).toBeFalsy();
  });

  it ('should add elements to the container elements', () => {
    overlayContainer.createAndAddElement('div');
    expect(containerElement.childNodes.length).toEqual(1);
    overlayContainer.createAndAddElement('div', ['test-class']);
    expect(containerElement.childNodes.length).toEqual(2);
  });

  it ('should remove elements', () => {
    overlayContainer.removeElement(null);
    expect(containerElement.childNodes.length).toEqual(0);
    const divElement1 = overlayContainer.createAndAddElement('div');
    const divElement2 = overlayContainer.createAndAddElement('div', ['test-class']);
    expect(containerElement.childNodes.length).toEqual(2);
    overlayContainer.removeElement(divElement1);
    overlayContainer.removeElement(divElement2);
    expect(containerElement.childNodes.length).toEqual(0);
  });

});
