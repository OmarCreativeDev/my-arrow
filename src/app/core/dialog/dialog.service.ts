import {
  ApplicationRef,
  ComponentFactoryResolver,
  ComponentRef,
  EmbeddedViewRef,
  Injectable,
  InjectionToken,
  Injector,
} from '@angular/core';
import { OverlayContainer } from './overlay-container';
import { DialogConfig } from './dialog-config';
import { Observable, Subject } from 'rxjs';
import { DialogInjector } from './dialog-injector';

/** Interface that can be used to generically type a class. */
interface ComponentType<T> {
  new (...args: any[]): T;
}

/** Injection token that can be used to access the data that was passed in to a dialog-panel. */
export const APP_DIALOG_DATA = new InjectionToken<any>('AppDialogData');

export class Dialog<T, D = any, R = any> {
  private _backdropElement: HTMLElement;
  private _wrapperElement: HTMLElement;

  private _componentRef: ComponentRef<T>;

  private readonly _afterClosed = new Subject<R | undefined>();

  constructor(
    componentType: ComponentType<T>,
    private _overlayContainer: OverlayContainer,
    componentFactoryResolver: ComponentFactoryResolver,
    injector: Injector,
    private _applicationRef: ApplicationRef,
    config: DialogConfig<D>
  ) {
    this._addBackdropAndWrapper(config);

    // Get a factory to create the Component:
    const componentFactory = componentFactoryResolver.resolveComponentFactory(componentType);

    // Create a custom injector:
    const customInjector = this._createInjector(injector, config.data);

    // Create the component, then attach it to the App:
    this._componentRef = componentFactory.create(customInjector);
    this._applicationRef.attachView(this._componentRef.hostView);

    // Get the component's HTMLElement, add CSS, and append it to the wrapper:
    const componentElement = (this._componentRef.hostView as EmbeddedViewRef<any>).rootNodes[0] as HTMLElement;
    const classes = ['dialog-panel', `dialog-panel--${config.size}`];
    // IE11 only accepts a single parameter to classList.add(className), so call it once for each className:
    classes.forEach(className => componentElement.classList.add(className));
    this._wrapperElement.appendChild(componentElement);
  }

  close(result?: R) {
    this._applicationRef.detachView(this._componentRef.hostView);
    this._componentRef.destroy();

    this._removeBackdropAndWrapper();

    this._afterClosed.next(result);
    this._afterClosed.complete();
  }

  get afterClosed(): Observable<R | undefined> {
    return this._afterClosed.asObservable();
  }

  private _createInjector(parentInjector: Injector, data: D): DialogInjector {
    const injectionTokens = new WeakMap();

    injectionTokens.set(APP_DIALOG_DATA, data).set(Dialog, this);

    return new DialogInjector(parentInjector, injectionTokens);
  }

  private _addBackdropAndWrapper(config: DialogConfig) {
    /* istanbul ignore else */
    if (config.hasBackdrop) {
      // Add the backdrop:
      this._backdropElement = this._overlayContainer.createAndAddElement('div', ['backdrop', 'backdrop--modal-open']);
      /* istanbul ignore else */
      if (config.closable) {
        this._backdropElement.addEventListener('click', (event: MouseEvent) => this.close());
      }
    }

    // Add the wrapper:
    this._wrapperElement = this._overlayContainer.createAndAddElement('div', ['dialog-wrapper']);
  }

  private _removeBackdropAndWrapper() {
    /* istanbul ignore else */
    if (this._backdropElement) {
      this._overlayContainer.removeElement(this._backdropElement);
    }
    this._overlayContainer.removeElement(this._wrapperElement);
  }
}

@Injectable()
export class DialogService {
  constructor(
    private _overlayContainer: OverlayContainer,
    private _componentFactoryResolver: ComponentFactoryResolver,
    private _applicationRef: ApplicationRef,
    private _injector: Injector
  ) {}

  open<T, D = any>(componentType: ComponentType<T>, config?: DialogConfig<D>): Dialog<T> {
    const defaultConfig = new DialogConfig();
    config = { ...defaultConfig, ...config };

    const dialog = new Dialog<T, D>(
      componentType,
      this._overlayContainer,
      this._componentFactoryResolver,
      this._injector,
      this._applicationRef,
      config
    );

    return dialog;
  }
}
