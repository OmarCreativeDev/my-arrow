import { bomAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.boms';
import { cartAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.cart';
import { catalogueAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.catalogue';
import { checkoutAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.checkout';
import { dashboardAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.dashboard';
import { LayoutAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.layout';
import { productDetailsAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.product-details';
import { quoteCartAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.quote-cart';
import { reelMetaReducers } from '@app/core/analytics/meta-reducers/analytics.reel';
import { tradeComplianceAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.trade-compliance';
import { tradeComplianceCertificateAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.trade-compliance-certificate';
import { userAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.user';
import { paymentReducers } from '@app/features/cart/stores/payment/payment.reducers';
import { forecastDownloadReducers } from '@app/features/forecast/stores/forecast-download/forecast-download.reducers';
import { forecastReducers } from '@app/features/forecast/stores/forecast/forecast.reducers';
import { productNotificationsReducers } from '@app/features/product-notifications/stores/product-notifications.reducers';
import { propertiesReducers } from '@app/features/properties/store/properties.reducers';
import { registrationAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.registration';
import { googleAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.custom-events';

export const reducers = {
  user: userAnalyticsMetaReducers,
  cart: cartAnalyticsMetaReducers,
  checkout: checkoutAnalyticsMetaReducers,
  tradeCompliancePartNumbers: tradeComplianceAnalyticsMetaReducers,
  tradeComplianceCertificate: tradeComplianceCertificateAnalyticsMetaReducers,
  properties: propertiesReducers,
  googleAnalytics: googleAnalyticsMetaReducers,
  productNotifications: productNotificationsReducers,
  productDetails: productDetailsAnalyticsMetaReducers,
  quoteCart: quoteCartAnalyticsMetaReducers,
  reel: reelMetaReducers,
  dashboard: dashboardAnalyticsMetaReducers,
  payment: paymentReducers,
  forecast: forecastReducers,
  forecastDownload: forecastDownloadReducers,
  boms: bomAnalyticsMetaReducers,
  layout: LayoutAnalyticsMetaReducers,
  registration: registrationAnalyticsMetaReducers,
  catalogue: catalogueAnalyticsMetaReducers,
};
