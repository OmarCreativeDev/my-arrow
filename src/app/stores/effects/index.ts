import { CartEffects } from '@app/features/cart/stores/cart/cart.effects';
import { CheckoutEffects } from '@app/features/cart/stores/checkout/checkout.effects';
import { ProductNotificationsEffects } from '@app/features/product-notifications/stores/product-notifications.effects';
import { QuoteCartEffects } from '@app/features/quotes/stores/quote-cart.effects';
import { ReelEffects } from '@app/core/reel/stores/reel.effects';
import { UserEffects } from '@app/core/user/store/user.effects';
import { DashboardEffects } from '@app/features/dashboard/stores/dashboard.effects';
import { ProductDetailsEffects } from '@app/features/products/stores/product-details/product-details.effects';
import { PaymentEffects } from '@app/features/cart/stores/payment/payment.effects';
import { ForecastEffects } from '@app/features/forecast/stores/forecast/forecast.effects';
import { ForecastDownloadEffects } from '@app/features/forecast/stores/forecast-download/forecast-download.effects';
import { PropertiesEffects } from '@app/features/properties/store/properties.effects';
import { BomEffects } from '@app/core/boms/store/boms.effects';
import { RegistrationEffects } from '@app/features/public/components/registration-form/store/registration.effects';
import { AnalyticsEffects } from '@app/core/analytics/stores/analytics.effects';
import { CatalogueEffects } from '@app/features/products/stores/catalogue/catalogue.effects';

export const effects = [
  CartEffects,
  CatalogueEffects,
  CheckoutEffects,
  ProductNotificationsEffects,
  ProductDetailsEffects,
  QuoteCartEffects,
  ReelEffects,
  UserEffects,
  DashboardEffects,
  PaymentEffects,
  ForecastEffects,
  ForecastDownloadEffects,
  PropertiesEffects,
  BomEffects,
  RegistrationEffects,
  AnalyticsEffects,
];
