import { MetaReducer } from '@ngrx/store';

import { resetStoreMetaReducers } from '@app/stores/metareducers/reset-store.metareducers';

export const metaReducers: MetaReducer<any>[] = [resetStoreMetaReducers];
