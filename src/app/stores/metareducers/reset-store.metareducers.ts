import { UserActionTypes } from '@app/core/user/store/user.actions';
import { isUserNotReady } from '@app/core/user/user.utils';
import { getPublicFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

export function resetStoreMetaReducers(reducer) {
  return (state, action) => {
    switch (action.type) {
      case UserActionTypes.RESET_USER:
        const error = state ? state.user.error : undefined;
        const userNotReady = error ? isUserNotReady(error) : false;
        const resetState = userNotReady ? { user: { error } } : undefined;
        let backedUpPublicFeatureFlags = {};

        if (state && state.properties) {
          backedUpPublicFeatureFlags = getPublicFeatureFlagsSelector(state);
        }
        return reducer({ ...resetState, properties: { public: { flags: backedUpPublicFeatureFlags } } }, action);

      default:
        return reducer(state, action);
    }
  };
}
