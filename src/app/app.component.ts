import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { ToastrService, ToastContainerDirective } from 'ngx-toastr';
import { TitleService } from '@app/shared/services/title.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  @ViewChild(ToastContainerDirective)
  toastContainer: ToastContainerDirective;

  constructor(private router: Router, private toastrService: ToastrService, private titleService: TitleService) {}

  ngOnInit() {
    this.titleService.init();

    this.router.events.subscribe(evt => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      this.scrollToTop();
    });

    this.toastrService.overlayContainer = this.toastContainer;
  }

  public scrollToTop(): void {
    window.scrollTo(0, 0);
  }
}
