import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { cloneDeep } from 'lodash-es';
import { IAppState } from '@app/shared/shared.interfaces';
import { INcnrDialogData, INcnrStatus, IAcceptNcnrItemsRequest } from './ncnr-dialog.interfaces';
import { ICheckoutCartItem } from '@app/core/checkout/checkout.interfaces';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { getNcnrUpdateStatus } from '@app/features/cart/stores/checkout/checkout.selectors';
import { UpdateNCNRAgreement, CleanNCNRState } from '@app/features/cart/stores/checkout/checkout.actions';

@Component({
  selector: 'app-ncnr-dialog',
  templateUrl: './ncnr-dialog.component.html',
  styleUrls: ['./ncnr-dialog.component.scss'],
})
export class NcnrDialogComponent implements OnInit, OnDestroy {
  public acceptedBy: string = '';
  public acceptCheckbox: boolean = false;
  public acceptanceStatus$: Observable<INcnrStatus>;
  public isUpdating: boolean = false;
  public updateFailed: boolean = false;
  public shouldRedirect: boolean = true;
  public subscription: Subscription;

  constructor(
    @Inject(APP_DIALOG_DATA) public data: INcnrDialogData,
    public dialog: Dialog<NcnrDialogComponent>,
    private store: Store<IAppState>
  ) {
    this.subscription = new Subscription();
    this.store.dispatch(new CleanNCNRState());
    this.acceptanceStatus$ = store.pipe(select(getNcnrUpdateStatus));
  }

  ngOnInit() {
    this.subscription.add(this.subscribeToChanges());
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public subscribeToChanges(): Subscription {
    return this.acceptanceStatus$.subscribe(status => {
      this.isUpdating = status === INcnrStatus.UPDATING;
      if (status === INcnrStatus.SUCCESSFUL) {
        this.updateFailed = false;
        this.onClose(false);
      } else if (status === INcnrStatus.FAILED) {
        this.updateFailed = true;
      }
    });
  }

  public nameValidator(event) {
    const pattern = /^[a-zA-Z\s_.'-]+$/;
    const inputChar = String.fromCharCode(event.charCode || event.keyCode);
    if (!pattern.test(inputChar)) {
      event.preventDefault();
    }
  }

  public isNcnrItem(lineItem: ICheckoutCartItem) {
    return lineItem.ncnr && !lineItem.ncnrAccepted;
  }

  public onClose(willRedirect: boolean): void {
    this.dialog.close(willRedirect);
  }

  public acceptTerms(): void {
    if (!this.denyActions) {
      const clonedItems: ICheckoutCartItem[] = cloneDeep(this.data.items);
      const acceptedLineItems: ICheckoutCartItem[] = clonedItems.filter(item => this.isNcnrItem(item));
      acceptedLineItems.forEach(item => {
        item.ncnrAccepted = true;
        item.ncnrAcceptedBy = this.acceptedBy;
      });
      const updateRequest: IAcceptNcnrItemsRequest = {
        items: acceptedLineItems,
        cartId: this.data.cartId,
      };
      this.store.dispatch(new UpdateNCNRAgreement(updateRequest));
    }
  }

  get denyActions(): boolean {
    return !this.acceptCheckbox || !!!this.acceptedBy || this.isUpdating;
  }
}
