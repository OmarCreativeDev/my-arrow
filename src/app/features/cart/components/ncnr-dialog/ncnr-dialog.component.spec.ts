import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Store, StoreModule } from '@ngrx/store';
import { CurrencyPipe } from '@angular/common';
import { FormattedPricePipe } from '@app/shared/pipes/formatted-price/formatted-price.pipe';
import { Dialog, APP_DIALOG_DATA } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { checkoutReducers } from '@app/features/cart/stores/checkout/checkout.reducers';
import { NcnrDialogComponent } from './ncnr-dialog.component';
import { mockedCheckout } from '@app/features/cart/stores/checkout/checkout.reducers.spec';
import { CleanNCNRState } from '@app/features/cart/stores/checkout/checkout.actions';

const dialogData = {
  currencyCode: 'USD',
  items: mockedCheckout.data.cart.lineItems,
  cartId: '001',
};

describe('NcnrDialogComponent', () => {
  let store: Store<any>;
  let component: NcnrDialogComponent;
  let fixture: ComponentFixture<NcnrDialogComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [NcnrDialogComponent, FormattedPricePipe],
      imports: [
        FormsModule,
        StoreModule.forRoot({
          checkout: checkoutReducers,
        }),
      ],
      providers: [CurrencyPipe, { provide: Dialog, useClass: DialogMock }, { provide: APP_DIALOG_DATA, useValue: { data: dialogData } }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    fixture = TestBed.createComponent(NcnrDialogComponent);
    component = fixture.componentInstance;
    component.data = dialogData;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call CleanNCRState on init', () => {
    component.ngOnInit();
    expect(store.dispatch).toHaveBeenCalledWith(new CleanNCNRState());
  });

  it('should check if a line item is NCNR or not', () => {
    const lineItem = dialogData.items[0];
    let isNcnr = component.isNcnrItem(lineItem);
    expect(isNcnr).toBeTruthy();
    lineItem.ncnrAccepted = true;
    isNcnr = component.isNcnrItem(lineItem);
    expect(isNcnr).toBeFalsy();
    lineItem.ncnr = false;
    isNcnr = component.isNcnrItem(lineItem);
    expect(isNcnr).toBeFalsy();
  });

  it('should deny actions by default', () => {
    expect(component.denyActions).toBeTruthy();
    component.acceptCheckbox = true;
    expect(component.denyActions).toBeTruthy();
    component.acceptedBy = 'John Doe';
    expect(component.denyActions).toBeFalsy();
  });

  it('should validate names with nameValidator()', () => {
    const event: any = document.createEvent('CustomEvent');
    event.charCode = 88; // '88' represents letter x
    event.preventDefault = () => {};
    spyOn(event, 'preventDefault');
    component.nameValidator(event);
    expect(event.preventDefault).toHaveBeenCalledTimes(0);
    event.charCode = undefined;
    event.keyCode = 50; // '50' represents a number
    component.nameValidator(event);
    expect(event.preventDefault).toHaveBeenCalledTimes(1);
  });

  it('should call dialog close method when invoking onClose', () => {
    const closeEvent = spyOn(component.dialog, 'close');
    component.onClose(false);
    expect(closeEvent).toHaveBeenCalled();
  });

  it('should dispatch action when invoking acceptTerms denyActions is false', () => {
    component.acceptCheckbox = true;
    component.acceptedBy = 'John Doe';
    component.acceptTerms();
    expect(store.dispatch).toHaveBeenCalled();
  });
});
