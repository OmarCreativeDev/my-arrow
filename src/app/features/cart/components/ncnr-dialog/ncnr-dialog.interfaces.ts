import { ICheckoutCartItem } from '@app/core/checkout/checkout.interfaces';

export interface INcnrDialogData {
  currencyCode: string;
  items: ICheckoutCartItem[];
  cartId: string;
}

export enum INcnrStatus {
  NONE = 'NONE',
  UPDATING = 'UPDATING',
  SUCCESSFUL = 'SUCCESSFUL',
  FAILED = 'FAILED',
}

export interface IAcceptNcnrItemsRequest {
  items: ICheckoutCartItem[];
  cartId: string;
}
