import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuantityUpdateDialogComponent } from './quantity-update-dialog.component';
import { Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { SharedModule } from '@app/shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('QuantityUpdateDialogComponent', () => {
  let component: QuantityUpdateDialogComponent;
  let fixture: ComponentFixture<QuantityUpdateDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot({ user: userReducers }), HttpClientTestingModule, HttpClientModule],
      declarations: [QuantityUpdateDialogComponent],
      providers: [{ provide: Dialog, useClass: DialogMock }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuantityUpdateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call dialog close method when invoking onClose', () => {
    const closeEvent = spyOn(component.dialog, 'close');
    component.onClose(false);
    expect(closeEvent).toHaveBeenCalled();
  });
});
