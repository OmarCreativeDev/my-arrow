import { Component } from '@angular/core';
import { Dialog } from '@app/core/dialog/dialog.service';

@Component({
  selector: 'app-quantity-dialog',
  templateUrl: './quantity-update-dialog.component.html',
})
export class QuantityUpdateDialogComponent {
  constructor(public dialog: Dialog<QuantityUpdateDialogComponent>) {}

  public onClose(quantityChangeAccepted: boolean): void {
    this.dialog.close({
      quantityChangeAccepted,
    });
  }
}
