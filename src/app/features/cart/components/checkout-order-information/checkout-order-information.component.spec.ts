import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckoutOrderInformationComponent } from './checkout-order-information.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { SharedModule } from '@app/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Store, StoreModule } from '@ngrx/store';
import { FormattedPricePipe } from '@app/shared/pipes/formatted-price/formatted-price.pipe';
import { mockedCheckout } from '@app/features/cart/stores/checkout/checkout.reducers.spec';
import { UpdateShippingAddressInStore } from '@app/features/cart/stores/checkout/checkout.actions';
import { IShipTo } from '@app/core/checkout/checkout.interfaces';
import { checkoutReducers } from '@app/features/cart/stores/checkout/checkout.reducers';
import { INITIAL_USER_STATE, userReducers } from '@app/core/user/store/user.reducers';
import { mockUser } from '@app/core/user/user.service.spec';
import { mockBillToAccounts } from '@app/shared/components/bill-to-accounts/bill-to-accounts.component.spec';

const shipTo: IShipTo = {
  name: 'B & C ELECTRONIC ENGINEERING',
  address1: ' ',
  address2: '1419 Westwood Blvd',
  address3: '',
  address4: '',
  city: 'Los Angeles',
  state: 'CA',
  country: 'United States',
  county: 'US',
  postCode: '111111',
  primaryFlag: true,
  selected: null,
};

const initialState = {
  checkout: mockedCheckout,
  user: {
    ...INITIAL_USER_STATE,
    profile: mockUser,
    billToAccounts: mockBillToAccounts,
  },
};

describe('CheckoutOrderInformationComponent', () => {
  let component: CheckoutOrderInformationComponent;
  let fixture: ComponentFixture<CheckoutOrderInformationComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CheckoutOrderInformationComponent],
      imports: [
        SharedModule,
        HttpClientTestingModule,
        StoreModule.forRoot({ checkout: checkoutReducers, user: userReducers }, { initialState: initialState }),
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, FormattedPricePipe],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutOrderInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return selectedPartNumber if defined', () => {
    const item = {
      ...mockedCheckout.data.cart.lineItems[0],
      selectedCustomerPartNumber: 'abcd',
      enteredCustomerPartNumber: 'efgh',
    };
    expect(component.getCustomerPartNumber(item)).toEqual('abcd');
  });

  it('should return enteredCustomerPartNumber if selectedPartNumber is undefined', () => {
    const item = {
      ...mockedCheckout.data.cart.lineItems[0],
      selectedCustomerPartNumber: null,
      enteredCustomerPartNumber: 'efgh',
    };
    expect(component.getCustomerPartNumber(item)).toEqual('efgh');
  });

  it(`should the prop65Flag be TRUE when ShipCountry is United States and ShipState is CA`, () => {
    store.dispatch(new UpdateShippingAddressInStore(shipTo));
    expect(component.prop65Flag).toEqual(true);
  });
});
