import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { IShoppingCartItem } from '@app/core/cart/cart.interfaces';
import { ICheckoutCartItem } from '@app/core/checkout/checkout.interfaces';
import { IAppState } from '@app/shared/shared.interfaces';
import { getShippingAddressSelector } from '@app/features/cart/stores/checkout/checkout.selectors';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import * as math from 'mathjs';
import { isProp65 } from '@app/features/cart/checkout.utils';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-checkout-order-information',
  templateUrl: './checkout-order-information.component.html',
  styleUrls: ['./checkout-order-information.component.scss'],
})
export class CheckoutOrderInformationComponent implements OnInit, OnDestroy {
  @Input()
  displayOnly: boolean = false;
  @Input()
  items: IShoppingCartItem[] | ICheckoutCartItem[] = [];
  @Input()
  displayTariffInfo: boolean = false;
  @Input()
  currencyCode$: Observable<string>;

  public shippingAddressSub: Subscription;
  public prop65Flag: boolean;

  constructor(private store: Store<IAppState>) {}

  ngOnInit() {
    this.shippingAddressSub = this.store
      .pipe(
        select(getShippingAddressSelector),
        filter(shippingAddress => shippingAddress !== undefined)
      )
      .subscribe(shippingAddress => {
        this.prop65Flag = shippingAddress.country && shippingAddress.state && isProp65(shippingAddress.country, shippingAddress.state);
      });
  }

  public getCustomerPartNumber(item: ICheckoutCartItem) {
    return item.selectedCustomerPartNumber ? item.selectedCustomerPartNumber : item.enteredCustomerPartNumber;
  }

  public multiply(price, quantity) {
    return math
      .chain(price ? math.bignumber(price) : 0)
      .multiply(quantity ? math.bignumber(quantity) : 0)
      .done();
  }

  ngOnDestroy() {
    if (this.shippingAddressSub && !this.shippingAddressSub.closed) this.shippingAddressSub.unsubscribe();
  }
}
