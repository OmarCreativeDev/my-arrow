import { Component, Input, OnInit } from '@angular/core';
import { DialogService } from '@app/core/dialog/dialog.service';
import { ShipToAccountsComponent } from '@app/shared/components/ship-to-accounts/ship-to-accounts.component';
import { BillToAccountsComponent } from '@app/shared/components/bill-to-accounts/bill-to-accounts.component';
import { IAppState } from '@app/shared/shared.interfaces';
import { ICheckoutAccountInfo, IShipTo } from '@app/core/checkout/checkout.interfaces';
import { UpdateShippingAddressInStore } from '@app/features/cart/stores/checkout/checkout.actions';
import { Store } from '@ngrx/store';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-checkout-account-information',
  templateUrl: './checkout-account-information.component.html',
  styleUrls: ['./checkout-account-information.component.scss'],
})
export class CheckoutAccountInformationComponent implements OnInit {
  @Input()
  displayOnly: boolean = false;
  @Input()
  accountInfo: ICheckoutAccountInfo;
  @Input()
  shippingAddress: IShipTo;

  constructor(private dialogService: DialogService, private store: Store<IAppState>) {}

  public ngOnInit() {}

  public openBillToDialog(): void {
    this.dialogService.open(BillToAccountsComponent);
  }

  public openShipToDialog() {
    const dialog = this.dialogService.open(ShipToAccountsComponent);

    if (typeof dialog !== 'undefined') {
      dialog.afterClosed.pipe(take(1)).subscribe(address => {
        if (address) {
          this.store.dispatch(new UpdateShippingAddressInStore(address));
        }
      });
    }
  }
}
