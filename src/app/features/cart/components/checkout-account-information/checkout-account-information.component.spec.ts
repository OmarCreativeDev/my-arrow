import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ShipToAccountsComponent } from '@app/shared/components/ship-to-accounts/ship-to-accounts.component';
import { CheckoutAccountInformationComponent } from './checkout-account-information.component';
import { SharedModule } from '@app/shared/shared.module';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { StoreModule } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';
import { checkoutReducers } from '@app/features/cart/stores/checkout/checkout.reducers';

describe('CheckoutAccountInformationComponent', () => {
  let component: CheckoutAccountInformationComponent;
  let fixture: ComponentFixture<CheckoutAccountInformationComponent>;

  let dialogService: DialogService;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [CheckoutAccountInformationComponent],
        imports: [
          StoreModule.forRoot({
            user: userReducers,
            checkout: checkoutReducers,
          }),
          SharedModule,
        ],
        providers: [{ provide: DialogService, useClass: DialogServiceMock }, ShipToAccountsComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckoutAccountInformationComponent);
    component = fixture.componentInstance;
    dialogService = TestBed.get(DialogService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should use the dialog service to open a ShipToAccounts dialog', () => {
    spyOn(dialogService, 'open');
    component.shippingAddress = {
      id: 24667766,
      address1: 'SITE 998',
      address2: '5902 ROB BULLOCK C1',
      address3: 'PMB 014-537',
      address4: '',
      city: 'DENVER',
      county: '',
      state: 'COL',
      country: 'US',
      name: 'FLEXTRONICS INTERNATIONAL INC',
      postCode: '78061',
      primaryFlag: true,
    };
    component.openShipToDialog();
    expect(dialogService.open).toHaveBeenCalled();
  });
});
