export enum CartDeletionStatus {
  DELETING = 'DELETING',
  FAILED = 'FAILED',
  NONE = 'NONE',
  SUCCESSFUL = 'SUCCESSFUL',
}
