import { ICartSelectedProduct } from '@app/core/cart/cart.interfaces';

export interface IDeleteDialogData {
  lineItemsIds: string[];
  products: ICartSelectedProduct[];
  shoppingCartId: string;
}
