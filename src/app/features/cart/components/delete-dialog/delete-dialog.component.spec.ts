import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from '@app/shared/shared.module';
import { StoreModule, Store } from '@ngrx/store';
import { of } from 'rxjs';
import { cartReducers } from '@app/features/cart/stores/cart/cart.reducers';
import { userReducers } from '@app/core/user/store/user.reducers';
import { Dialog, APP_DIALOG_DATA } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { DeleteDialogComponent } from './delete-dialog.component';
import { IDeleteDialogData } from '@app/features/cart/components/delete-dialog/delete-dialog.interface';
import { CartDeletionStatus } from '@app/features/cart/components/delete-dialog/delete-dialog.enum';

const dialogData: IDeleteDialogData = {
  lineItemsIds: ['123', '456'],
  products: [{ id: 123, name: 'Product 1' }, { id: 456, name: 'Product 2' }],
  shoppingCartId: '123',
};

describe('DeleteDialogComponent', () => {
  let store: Store<any>;
  let component: DeleteDialogComponent;
  let fixture: ComponentFixture<DeleteDialogComponent>;
  let dialog: Dialog<DeleteDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeleteDialogComponent],
      imports: [
        SharedModule,
        HttpClientTestingModule,
        StoreModule.forRoot({
          cart: cartReducers,
          user: userReducers,
        }),
      ],
      providers: [{ provide: Dialog, useClass: DialogMock }, { provide: APP_DIALOG_DATA, useValue: { data: dialogData } }],
    }).compileComponents();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    dialog = TestBed.get(Dialog);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the dialog', () => {
    spyOn(dialog, 'close');
    component.onClose();
    expect(dialog.close).toHaveBeenCalled();
  });

  it('should dispatch DeleteCartItems() when calling deleteItems()', () => {
    component.deleteItems();
    expect(store.dispatch).toHaveBeenCalled();
  });

  it('should set deleteFailed to false and call onClose when status is success', () => {
    spyOn(component, 'onClose');
    component.cartDeletionStatus$ = of(CartDeletionStatus.SUCCESSFUL);
    component.subscribeToChanges();
    expect(component.deleteFailed).toBeFalsy();
    fixture.detectChanges();
    expect(component.onClose).toHaveBeenCalled();
  });

  it('should set deleteFailed to true when status is failed', () => {
    component.cartDeletionStatus$ = of(CartDeletionStatus.FAILED);
    component.subscribeToChanges();
    expect(component.deleteFailed).toBeTruthy();
  });
});
