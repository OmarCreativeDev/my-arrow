import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { Dialog, APP_DIALOG_DATA } from '@app/core/dialog/dialog.service';
import { IDeleteDialogData } from '@app/features/cart/components/delete-dialog/delete-dialog.interface';
import { CleanDeleteCartStatus, RequestCartItemsDeletion } from '@app/features/cart/stores/cart/cart.actions';
import { getCartDeletionStatus } from '@app/features/cart/stores/cart/cart.selectors';
import { CartDeletionStatus } from '@app/features/cart/components/delete-dialog/delete-dialog.enum';

@Component({
  selector: 'app-delete-dialog',
  templateUrl: './delete-dialog.component.html',
})
export class DeleteDialogComponent implements OnInit, OnDestroy {
  public isDeleting: boolean = false;
  public cartDeletionStatus$: Observable<CartDeletionStatus>;
  public deleteFailed: boolean = false;
  private subscription: Subscription = new Subscription();

  constructor(
    @Inject(APP_DIALOG_DATA) public data: IDeleteDialogData,
    public dialog: Dialog<DeleteDialogComponent>,
    private store: Store<IAppState>
  ) {
    this.store.dispatch(new CleanDeleteCartStatus());
    this.cartDeletionStatus$ = store.pipe(select(getCartDeletionStatus));
  }

  ngOnInit() {
    this.subscription.add(this.subscribeToChanges());
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public deleteItems(): void {
    const { lineItemsIds, products, shoppingCartId } = this.data;
    this.store.dispatch(new RequestCartItemsDeletion({ lineItemsIds, products, shoppingCartId }));
  }

  public subscribeToChanges(): Subscription {
    return this.cartDeletionStatus$.subscribe(status => {
      this.isDeleting = status === CartDeletionStatus.DELETING;
      this.deleteFailed = false;
      if (status === CartDeletionStatus.SUCCESSFUL) {
        this.onClose();
      } else if (status === CartDeletionStatus.FAILED) {
        this.deleteFailed = true;
      }
    });
  }

  public onClose(): void {
    this.dialog.close();
  }
}
