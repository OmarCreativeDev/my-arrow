import { getPrice } from './cart.utils';
import { IPriceTier } from '@app/shared/components/price-tiers/price-tiers.interface';

describe('cart utils', () => {
  it('should return the right price', () => {
    const priceTiers: IPriceTier[] = [
      {
        pricePerItem: 2,
        quantityFrom: 1,
        quantityTo: 5,
      },
      {
        pricePerItem: 1.5,
        quantityFrom: 6,
        quantityTo: null,
      },
    ];
    expect(getPrice(7, 1.99, priceTiers)).toBe(1.5);
    expect(getPrice(7, 1.99, [])).toBe(1.99);
  });
});
