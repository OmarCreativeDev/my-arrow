const prop65Country = 'United States';
const prop65State = 'CA';

export const isProp65 = (country: string, state: string): boolean => country === prop65Country && state === prop65State;
