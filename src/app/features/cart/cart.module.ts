import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared/shared.module';
import { CartDetailsComponent } from './pages/cart-details/cart-details.component';
import { CartRoutingModule } from './cart-routing.module';
import { CheckoutComponent } from './pages/checkout/checkout.component';
import { OrderConfirmationComponent } from './pages/order-confirmation/order-confirmation.component';
import { CheckoutAccountInformationComponent } from './components/checkout-account-information/checkout-account-information.component';
import { CheckoutOrderInformationComponent } from './components/checkout-order-information/checkout-order-information.component';
import { RouterModule } from '@angular/router';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DeleteDialogComponent } from './components/delete-dialog/delete-dialog.component';
import { NcnrDialogComponent } from './components/ncnr-dialog/ncnr-dialog.component';
import { QuantityUpdateDialogComponent } from './components/quantity-update-dialog/quantity-update-dialog.component';

@NgModule({
  imports: [CommonModule, SharedModule, FormsModule, ReactiveFormsModule, CartRoutingModule, RouterModule],
  providers: [DialogService],
  declarations: [
    CartDetailsComponent,
    CheckoutComponent,
    OrderConfirmationComponent,
    CheckoutAccountInformationComponent,
    CheckoutOrderInformationComponent,
    DeleteDialogComponent,
    NcnrDialogComponent,
    QuantityUpdateDialogComponent,
  ],
  entryComponents: [DeleteDialogComponent, NcnrDialogComponent, QuantityUpdateDialogComponent],
})
export class CartModule {}
