import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { of } from 'rxjs';
import { Store, StoreModule } from '@ngrx/store';

import { SharedModule } from '@app/shared/shared.module';
import { OrderConfirmationComponent } from './order-confirmation.component';
import { checkoutReducers } from '@app/features/cart/stores/checkout/checkout.reducers';
import { quoteCartReducers } from '@app/features/quotes/stores/quote-cart.reducers';

class MockStore {
  select() {
    return of();
  }
  dispatch() {}
  pipe() {
    return of({});
  }
}

describe('OrderConfirmationComponent', () => {
  let component: OrderConfirmationComponent;
  let fixture: ComponentFixture<OrderConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderConfirmationComponent],
      providers: [{ provide: Store, useClass: MockStore }],
      imports: [
        SharedModule,
        HttpClientTestingModule,
        StoreModule.forRoot({
          checkout: checkoutReducers,
          quoteCart: quoteCartReducers,
        }),
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call print', () => {
    spyOn(component, 'print').and.callFake(() => true);
    component.print();
    fixture.detectChanges();
    expect(component.print).toHaveBeenCalled();
  });
});
