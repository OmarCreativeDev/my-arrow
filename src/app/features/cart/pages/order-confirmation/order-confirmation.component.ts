import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';

import { find } from 'lodash-es';

import { IAppState } from '@app/shared/shared.interfaces';
import { ICheckoutAccountInfo, IShipTo } from '@app/core/checkout/checkout.interfaces';
import {
  getShippingAccountInfoSelector,
  getShippingAddressSelector,
  getConfirmedOrderId,
  getCheckoutSelector,
  getCheckoutOptions,
} from '@app/features/cart/stores/checkout/checkout.selectors';
import { getLineItemCountSelector } from '@app/features/quotes/stores/quote-cart.selectors';
import { IShoppingCartItem } from '@app/core/cart/cart.interfaces';
import { PlaceOrderReset } from '@app/features/cart/stores/checkout/checkout.actions';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: 'app-order-confirmation',
  templateUrl: './order-confirmation.component.html',
  styleUrls: ['./order-confirmation.component.scss'],
})
export class OrderConfirmationComponent implements OnInit, OnDestroy {
  public shippingAccountInfo$: Observable<ICheckoutAccountInfo>;
  public shippingAddress$: Observable<IShipTo>;
  public state$: Observable<any>;
  public cart$: Observable<any>;
  public options$: Observable<any>;
  public poNumber: string;
  public externalComments: string;
  public confirmedOrderId$: Observable<number>;
  public quoteCartItemCount$: Observable<Number>;
  public lineItems$: Observable<IShoppingCartItem[]>;
  public cart: any;
  public privateFeatureFlags$: Observable<object>;
  public privateFeatureFlags: object;
  public displayTariffInfo: boolean = false;
  private subscription: Subscription = new Subscription();

  constructor(private store: Store<IAppState>) {
    this.quoteCartItemCount$ = this.store.pipe(select(getLineItemCountSelector));
  }

  ngOnInit() {
    this.confirmedOrderId$ = this.store.pipe(select(getConfirmedOrderId));
    this.shippingAccountInfo$ = this.store.pipe(select(getShippingAccountInfoSelector));
    this.shippingAddress$ = this.store.pipe(select(getShippingAddressSelector));
    this.cart$ = this.store.pipe(select(getCheckoutSelector));
    this.options$ = this.store.pipe(select(getCheckoutOptions));
    this.privateFeatureFlags$ = this.store.pipe(select(getPrivateFeatureFlagsSelector));
    this.startSubscriptions();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeCart());
    this.subscription.add(this.subscribeOptions());
    this.subscription.add(this.subscribePrivateFeatureFlags());
  }

  private subscribeCart(): Subscription {
    return this.cart$.subscribe(cart => {
      /* istanbul ignore next */
      if (cart.cart) {
        this.cart = cart.cart;
        if (cart.cart.lineItems) {
          this.lineItems$ = cart.cart.lineItems;
          this.displayTariffInfo = find(cart.cart.lineItems, item => item.tariffApplicable);
        }
      }
    });
  }

  private subscribeOptions(): Subscription {
    return this.options$.subscribe(options => {
      /* istanbul ignore next */
      if (options) {
        this.poNumber = options.poNumber;
        this.externalComments = options.salesRepReview ? options.externalComments : null;
      }
    });
  }

  private subscribePrivateFeatureFlags(): Subscription {
    return this.options$.subscribe(options => {
      /* istanbul ignore next */
      if (options) {
        this.poNumber = options.poNumber;
        this.externalComments = options.salesRepReview ? options.externalComments : null;
      }
    });
  }

  print(): void {
    window.print();
  }

  ngOnDestroy(): void {
    this.store.dispatch(new PlaceOrderReset());
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }
}
