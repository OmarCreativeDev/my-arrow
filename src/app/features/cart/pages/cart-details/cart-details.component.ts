import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { cloneDeep, find, findIndex, forOwn, get, has, isNumber, map, omit, reduce, includes, isEmpty } from 'lodash-es';
import { chain, flow, differenceBy, orderBy } from 'lodash';
import { Client, Message, StompSubscription } from '@stomp/stompjs';
import * as math from 'mathjs';
import * as moment from 'moment';
import { combineLatest, Observable, Subscription, timer } from 'rxjs';
import { map as rxMap, filter, take, debounceTime, distinctUntilChanged, skip } from 'rxjs/operators';

import { Store, select } from '@ngrx/store';
import { CustomValidators } from '@app/shared/classes/custom-validators';
import { WSSService } from '@app/core/ws/wss.service';
import { WSSState } from '@app/core/ws/wss.interface';
import { DialogService, Dialog } from '@app/core/dialog/dialog.service';
import {
  GetCartData,
  ToggleLineItems,
  UpdateCartItemReel,
  UpdateCartItems,
  UpdateCartItemsField,
  UpdateCartStore,
  RefreshCartItem,
  GetCarts,
  GetLineItemPrice,
  ValidateItems,
} from '@app/features/cart/stores/cart/cart.actions';
import { UpdateCheckoutOptions } from '@app/features/cart/stores/checkout/checkout.actions';
import {
  getCartItemsSelector,
  getError,
  getLoading,
  getSelectedIds,
  getShoppingCartId,
  getSelectedProducts,
} from '@app/features/cart/stores/cart/cart.selectors';
import {
  getCurrencyCode,
  getUserBillToAccount,
  getUserAccountName,
  getUserAccountTerms,
  getUser,
  getUserShipToAccount,
} from '@app/core/user/store/user.selectors';
import { IUser } from '@app/core/user/user.interface';
import { IAppState } from '@app/shared/shared.interfaces';
import {
  ICartCheckoutValidRegions,
  ICartSelectedProduct,
  ICartValidationStatus,
  IGetLineItemPriceRequest,
  IShoppingCartItem,
} from '@app/core/cart/cart.interfaces';
import { IDeleteDialogData } from '@app/features/cart/components/delete-dialog/delete-dialog.interface';
import { IReelDialog, IReelCallbackResponse } from '@app/shared/components/reel-dialog/reel-dialog.interfaces';
import { IDateInputConfig } from '@app/shared/components/date-picker/date-picker.interfaces';
import { ILineItemCpnInfo } from '@app/shared/components/line-item-cpn/line-item-cpn.interfaces';
import { BillToAccountsComponent } from '@app/shared/components/bill-to-accounts/bill-to-accounts.component';
import { DeleteDialogComponent } from '@app/features/cart/components/delete-dialog/delete-dialog.component';
import { ReelDialogComponent } from '@app/shared/components/reel-dialog/reel-dialog.component';
import { SESSION_TIMEOUT_MILLISECONDS } from '@env/environment';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';
import { QuantityUpdateDialogComponent } from '@app/features/cart/components/quantity-update-dialog/quantity-update-dialog.component';

@Component({
  selector: 'app-cart-details',
  templateUrl: './cart-details.component.html',
  styleUrls: ['./cart-details.component.scss'],
})
export class CartDetailsComponent implements OnInit, OnDestroy {
  public DATE_FORMAT: string = 'YYYY-MM-DD';

  public shoppingCart: IShoppingCartItem[];
  public cartForm: FormGroup;
  public cartItems: FormArray;
  public shoppingCartUpdated: IShoppingCartItem[];
  public minDate = moment(new Date())
    .startOf('day')
    .toDate();
  public selectedIds$: Observable<Array<string>>;
  public selectedProducts$: Observable<Array<ICartSelectedProduct>>;
  public cartItemFormGroupMap: Map<string, FormGroup> = new Map<string, FormGroup>();
  public requestDateConfiguration: IDateInputConfig[] = [
    {
      placeholder: 'Select Date',
    },
  ];

  public multipleRequestDateConfiguration: IDateInputConfig[] = [
    {
      placeholder: 'Change Selected Dates',
    },
  ];
  public showTiers = [];
  public showNcnrWarning: boolean = false;

  public requestDatePickerDates = [undefined];

  public accountName$: Observable<string>;
  public accountTerms$: Observable<string>;
  public loading$: Observable<boolean>;
  public error$: Observable<Error>;
  public shoppingCartId$: Observable<string>;
  public billToId$: Observable<number>;
  public shipToId$: Observable<number>;
  public currencyCode$: Observable<string>;

  public wsLineItems: Client;
  public shoppingCartWSTopic: StompSubscription;

  private lineItems$: Observable<IShoppingCartItem[]>;
  public wasCartValidated: boolean = false;

  public userProfile$: Observable<IUser>;
  public cpn: string;
  public endCustomerSiteId: number;
  public billToId: number;
  public shipToId: number;
  public currencyCode: string;
  public userRegion: string;
  public shoppingCartId;
  public privateFeatureFlags$: Observable<any>;
  public privateFeatureFlags: any;
  public subscription: Subscription = new Subscription();
  public deleteDialog: Dialog<DeleteDialogComponent>;
  public quantityUpdateDialog: Dialog<QuantityUpdateDialogComponent>;
  public quoteExpiredStatus: string = ICartValidationStatus.QUOTE_EXPIRED;
  public editedQuotedItems: boolean;

  constructor(
    private dialogService: DialogService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private store: Store<IAppState>,
    private wssService: WSSService
  ) {
    this.lineItems$ = store.pipe(select(getCartItemsSelector));
    this.loading$ = store.pipe(select(getLoading));
    this.error$ = store.pipe(select(getError));
    this.selectedIds$ = store.pipe(select(getSelectedIds));
    this.selectedProducts$ = store.pipe(select(getSelectedProducts));
    this.shoppingCartId$ = store.pipe(select(getShoppingCartId));
    this.billToId$ = store.pipe(select(getUserBillToAccount));
    this.currencyCode$ = store.pipe(select(getCurrencyCode));
    this.accountName$ = store.pipe(select(getUserAccountName));
    this.accountTerms$ = store.pipe(select(getUserAccountTerms));
    this.userProfile$ = store.pipe(select(getUser));
    this.privateFeatureFlags$ = store.pipe(select(getPrivateFeatureFlagsSelector));
    this.shipToId$ = store.pipe(select(getUserShipToAccount));
  }

  get cartSubtotal() {
    let subtotal = 0;
    if (this.cartItems && this.cartItems.controls) {
      this.cartItems.controls.forEach(cartItem => {
        subtotal = math
          .chain(this.getCartItemPrice(cartItem.value))
          .multiply(cartItem.get('quantity').value ? cartItem.get('quantity').value : 0)
          .add(subtotal)
          .done();
      });
    }
    return subtotal;
  }

  get tariffTotal() {
    let tariffSubtotal = 0;
    if (this.cartItems && this.cartItems.controls) {
      this.cartItems.controls.forEach(cartItem => {
        tariffSubtotal = math
          .chain(cartItem.value.tariffValue ? cartItem.value.tariffValue : 0)
          .add(tariffSubtotal)
          .done();
      });
    }
    return tariffSubtotal;
  }

  get cartTotal() {
    return has(this.privateFeatureFlags, 'tariff') && this.privateFeatureFlags.tariff
      ? this.cartSubtotal + this.tariffTotal
      : this.cartSubtotal;
  }

  get readyForCheckout() {
    let ready: boolean = this.cartForm.valid;
    if (ready && this.shoppingCart && this.shoppingCart.length > 0) {
      const shoppingCartNotReady = reduce(
        this.shoppingCart,
        (acc, lineItem) => {
          if (lineItem.changed || !lineItem.price) {
            acc.push(lineItem);
          }
          return acc;
        },
        []
      );
      ready = !shoppingCartNotReady || shoppingCartNotReady.length === 0;
    }
    return ready;
  }

  get lineItemsChanged() {
    if (this.shoppingCart && this.shoppingCart.length) {
      this.shoppingCartUpdated = new Array<IShoppingCartItem>();
      this.shoppingCartUpdated = reduce(
        this.shoppingCart,
        (acc, lineItem) => {
          if (lineItem.changed) {
            acc.push(lineItem);
          }
          return acc;
        },
        []
      );
    }
    return this.shoppingCartUpdated && this.shoppingCartUpdated.length ? true : false;
  }

  ngOnInit(): void {
    if (this.route.queryParams) {
      this.route.queryParams.pipe(filter(params => params.warnNcnr)).subscribe(params => {
        if (params.warnNcnr) {
          this.showNcnrWarning = true;
        }
      });
    }
    this.subscription.add(this.combineDataSourcesAndInitValues());
    this.subscription.add(this.wsClientStateSubscription());
    this.subscription.add(this.getCartItems());
    this.subscription.add(this.findEditedQuotedItems());
    this.store.dispatch(new GetCarts());
  }

  ngOnDestroy(): void {
    this.unsubscribeToWSTopic();
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
    if (this.deleteDialog) this.deleteDialog.close();
  }

  public getItemsIdsThatRequireValidation(): string[] {
    return this.shoppingCart && this.shoppingCart.length
      ? this.shoppingCart.filter(item => item.validation !== ICartValidationStatus.VALID).map(item => item.id)
      : [];
  }

  public validateLineItems(): void {
    const itemsIdsThatRequireValidation = this.getItemsIdsThatRequireValidation();
    if (itemsIdsThatRequireValidation.length && !this.wasCartValidated) {
      this.store.dispatch(new ValidateItems({ lineItemsIds: itemsIdsThatRequireValidation, shoppingCartId: this.shoppingCartId }));
      this.wasCartValidated = true;
    }
  }

  public wsClientStateSubscription() {
    return this.wssService.state$.subscribe((state: WSSState) => this.handleWSClientState(state));
  }

  public handleWSClientState(state: WSSState) {
    switch (state) {
      case WSSState.CONNECTED:
        this.onWSClientConnects(this.shoppingCartId);
        break;
      case WSSState.DISCONNECTED:
        this.onWSClientDisconnects();
        break;
    }
  }

  public onWSClientConnects(shoppingCartId: string): void {
    this.subscribeToWSTopic(shoppingCartId);
  }

  public onWSClientDisconnects(): void {
    this.shoppingCartWSTopic = undefined;
  }

  public subscribeToWSTopic(shoppingCartId: string) {
    if (this.wssService.isClientConnected() && shoppingCartId && !this.shoppingCartWSTopic) {
      this.wsLineItems = this.wssService.client;
      this.shoppingCartWSTopic = this.wssService.client.subscribe(`/shoppingCart/queue/${shoppingCartId}/lineItems`, (message: Message) => {
        const shoppingCartLineItem: IShoppingCartItem = JSON.parse(message.body);
        this.store.dispatch(new RefreshCartItem(shoppingCartLineItem));
      });
    }
    this.checkWSTopicSubAndValidateItems(this.shoppingCartWSTopic);
  }

  public checkWSTopicSubAndValidateItems(scWSTopic: StompSubscription) {
    const scSubscriptionId = get(scWSTopic, 'id');
    if (!isEmpty(scSubscriptionId)) {
      this.validateLineItems();
    }
  }

  public unsubscribeToWSTopic() {
    if (this.wssService.isClientConnected() && this.shoppingCartWSTopic) {
      this.shoppingCartWSTopic.unsubscribe();
    }
  }

  public combineDataSourcesAndInitValues() {
    return combineLatest(
      this.billToId$,
      this.currencyCode$,
      this.shoppingCartId$,
      this.userProfile$,
      this.privateFeatureFlags$,
      this.shipToId$
    ).subscribe(latestValues => {
      const [billToId, currencyCode, shoppingCartId, profile, featureFlags, shipToId] = latestValues;
      this.billToId = billToId;
      this.currencyCode = currencyCode;
      this.userRegion = profile && profile.region;
      this.privateFeatureFlags = featureFlags;
      this.shipToId = shipToId;
      if (shoppingCartId && !this.shoppingCartId) {
        this.shoppingCartId = shoppingCartId;
        this.store.dispatch(new GetCartData({ shoppingCartId, requestValidation: true }));
      }
    });
  }

  public isSelected(lineItem: IShoppingCartItem, selectedIds: Array<string>): boolean {
    return selectedIds.includes(lineItem.id);
  }

  public toggleLineItems(isSelected: boolean, lineItems: Array<IShoppingCartItem>): void {
    const lineItemIds = lineItems.map(lineItem => lineItem.id);
    const products = lineItems.map(lineItem => {
      return {
        id: lineItem.itemId,
        name: lineItem.manufacturerPartNumber,
      };
    });
    this.store.dispatch(new ToggleLineItems({ lineItemIds, products }, isSelected));
  }

  public updateCartField(fieldName: string, value: any, id: string): void {
    const index: number = findIndex(this.shoppingCart, { id });
    // Only update properties if they have a different value
    // This prevents an infinite loop
    if (index >= 0 && this.shoppingCart[index][fieldName] !== value) {
      this.store.dispatch(new UpdateCartStore({ fieldName, value, index }));
    }
  }

  public openDeleteDialog(): void {
    combineLatest(this.selectedIds$, this.selectedProducts$)
      .pipe(take(1))
      .subscribe(([lineItemsIds, products]) => {
        const deleteDialogData: IDeleteDialogData = {
          lineItemsIds,
          products,
          shoppingCartId: this.shoppingCartId,
        };
        this.deleteDialog = this.dialogService.open(DeleteDialogComponent, {
          data: deleteDialogData,
          size: 'large',
        });
      });
  }

  public changeLineItemsDate(dates: Array<string>) {
    this.selectedIds$.pipe(take(1)).subscribe(lineItemsIds => {
      this.changeLineItemsField(lineItemsIds, 'requestDate', dates[0]);
    });
  }

  private changeLineItemsField(lineItemids: Array<String>, fieldName: string, value: any) {
    const dateString = moment(value).format(this.DATE_FORMAT);
    const indexes: Array<number> = chain(lineItemids)
      .map(row => findIndex(this.shoppingCart, { id: row }))
      .value();
    this.store.dispatch(new UpdateCartItemsField({ fieldName, value: dateString, indexes }));
  }

  public changeLineItemDate(datesArray: Array<string>, lineItem: IShoppingCartItem) {
    if (!isNaN(Date.parse(datesArray[0]))) {
      const formattedDate = moment(datesArray[0]).format(this.DATE_FORMAT);
      if (formattedDate !== lineItem.requestDate) {
        this.updateCartField('requestDate', formattedDate, lineItem.id);
      }
    }
  }

  public isReelSelected(lineItem: IShoppingCartItem) {
    return get(lineItem.reel, 'itemsPerReel', 0) > 0 && get(lineItem.reel, 'numberOfReels', 0) > 0;
  }

  public openReelModal(lineItem: IShoppingCartItem): void {
    const { cost, itemsPerReel, margin, numberOfReels, resale } = get(lineItem, 'reel', {});
    const reelItem: IReelDialog = {
      readOnlyCpn: true,
      reelItem: {
        cartLineId: lineItem.id,
        availableQuantity: lineItem.availableQuantity,
        billToId: this.billToId,
        cpn: lineItem.selectedCustomerPartNumber,
        currency: this.currencyCode,
        endCustomerId: lineItem.selectedEndCustomerSiteId,
        endCustomerRecords: lineItem.endCustomerRecords,
        itemId: lineItem.itemId,
        partNumber: lineItem.manufacturerPartNumber,
        reel: {
          itemsPerReel: itemsPerReel || null,
          numberOfReels: numberOfReels || null,
          ...(cost && { cost }),
          ...(margin && { margin }),
          ...(resale && { resale }),
        },
        warehouseId: lineItem.warehouseId,
      },
    };
    const dialog = this.dialogService.open(ReelDialogComponent, {
      data: reelItem,
      size: 'x-large',
    });
    dialog.afterClosed.pipe(take(1)).subscribe((reelData: IReelCallbackResponse) => {
      if (reelData) {
        this.applyReelToItem(reelData);
      }
    });
  }

  public applyReelToItem(data: IReelCallbackResponse): void {
    const lineItem = find(this.shoppingCart, (item: IShoppingCartItem) => item.id === data.cartLineId);
    const clonedLineItem = cloneDeep(lineItem);

    clonedLineItem.price = data.pricePerItem;
    clonedLineItem.quantity = math.multiply(data.reel.itemsPerReel, data.reel.numberOfReels);
    clonedLineItem.reel = data.reel;
    clonedLineItem.tariffValue = data.tariffValue;
    clonedLineItem.selectedCustomerPartNumber = data.selectedCustomerPartNumber;
    clonedLineItem.selectedEndCustomerSiteId = data.selectedEndCustomerSiteId;

    this.store.dispatch(
      new UpdateCartItemReel({
        lineItem: clonedLineItem,
        shoppingCartId: this.shoppingCartId,
      })
    );

    /* istanbul ignore else */
    if (this.cartItemFormGroupMap.size) {
      this.cartItemFormGroupMap
        .get(clonedLineItem.id)
        .get('quantity')
        .disable();
    }
  }

  public updateItems(): void {
    if (this.editedQuotedItems) {
      this.openQuantityUpdateDialog();
    } else {
      this.store.dispatch(new UpdateCartItems({ lineItems: this.shoppingCartUpdated, shoppingCartId: this.shoppingCartId }));
    }
  }

  public findEditedQuotedItems(): Subscription {
    return this.lineItems$.subscribe(lineItems => {
      this.editedQuotedItems = lineItems.some(item => item.quoted && item.isQuantityEdited);
    });
  }

  public openQuantityUpdateDialog(): void {
    this.quantityUpdateDialog = this.dialogService.open(QuantityUpdateDialogComponent, { size: 'small' });
    this.quantityUpdateDialog.afterClosed.pipe(take(1)).subscribe(selection => {
      if (selection.quantityChangeAccepted) {
        this.store.dispatch(new UpdateCartItems({ lineItems: this.shoppingCartUpdated, shoppingCartId: this.shoppingCartId }));
      } else {
        this.restoreQuantityValue();
      }
    });
  }

  public checkoutWithTermsAccount(): void {
    const checkoutOptions = {
      shippingMethod: 'Standard',
      shipComplete: false,
      poNumber: '',
      salesRepReview: false,
      checkoutWithCreditCard: false,
    };
    this.store.dispatch(new UpdateCheckoutOptions(checkoutOptions));

    this.router.navigateByUrl('/cart/checkout');
  }

  public checkOutWithCreditCard(): void {
    const checkoutOptions = {
      shippingMethod: 'Standard',
      shipComplete: false,
      poNumber: '',
      salesRepReview: false,
      checkoutWithCreditCard: true,
    };
    this.store.dispatch(new UpdateCheckoutOptions(checkoutOptions));

    this.router.navigateByUrl('/cart/checkout');
  }

  private getCartItems(): Subscription {
    return this.lineItems$
      .pipe(
        rxMap((lineItems: IShoppingCartItem[]) => this.buildCartForm(lineItems)),
        rxMap(() => this.subscribeToWSTopic(this.shoppingCartId))
      )
      .subscribe();
  }

  private buildCartForm(lineItems: IShoppingCartItem[]) {
    const removedLineItemsIndexes = flow(
      _lineItems => differenceBy(_lineItems, lineItems, 'id'),
      _lineItems => map(_lineItems, row => findIndex(this.shoppingCart, { id: row.id })),
      _lineItems => orderBy(_lineItems, Number, ['desc'])
    )(this.shoppingCart);
    if (removedLineItemsIndexes.length && this.cartForm) {
      map(removedLineItemsIndexes, lineItemIndex => (<FormArray>this.cartForm.get('cartItems')).removeAt(lineItemIndex));
      this.shoppingCart = lineItems;
      return;
    }
    this.shoppingCart = lineItems;
    this.cartItems = this.buildCartItems(this.shoppingCart);
    this.cartForm = this.formBuilder.group({
      cartItems: this.cartItems,
    });
  }

  public buildCartItems(cartItems: IShoppingCartItem[]): FormArray {
    const cartItemsGroup: FormGroup[] = cartItems.map(cartItem => this.patchOrBuildCartItem(cartItem));
    return this.formBuilder.array(cartItemsGroup);
  }

  public patchOrBuildCartItem(cartItem: IShoppingCartItem): FormGroup {
    let cartItemFormGroup = this.cartItemFormGroupMap.get(cartItem.id);
    const isReelItem = this.isReelSelected(cartItem);

    if (cartItem.quoted) {
      const expirationDate = new Date(cartItem.expiryDate); // This assumes that the fetched date is in UTC
      const currentDate = new Date(new Date().toUTCString()); // Get current date in UTC
      const millisecondsUntilExpiration: number = expirationDate.getTime() - currentDate.getTime();
      if (cartItem.expirationTimer) {
        cartItem.expirationTimer.unsubscribe();
      }
      if (millisecondsUntilExpiration <= 0) {
        cartItem.validation = ICartValidationStatus.QUOTE_EXPIRED;
      } else if (millisecondsUntilExpiration < SESSION_TIMEOUT_MILLISECONDS) {
        cartItem.expirationTimer = timer(millisecondsUntilExpiration).subscribe(() => {
          cartItem.validation = ICartValidationStatus.QUOTE_EXPIRED;
          cartItem.expirationTimer.unsubscribe();
        });
        this.subscription.add(cartItem.expirationTimer);
      }
    }

    if (cartItemFormGroup) {
      cartItemFormGroup = this.patchCartItem(cartItemFormGroup, cartItem);
    } else {
      cartItemFormGroup = this.buildCartItem(cartItem);
      // Required to prevent property corruption
      /* istanbul ignore else */
      if (get(cartItem, 'endCustomerRecords', []).length) {
        cartItemFormGroup.controls['endCustomerRecords'].patchValue(cartItem.endCustomerRecords);
      }
    }

    if (cartItem.validation === ICartValidationStatus.PENDING) {
      cartItemFormGroup.get('requestDate').disable();
      cartItemFormGroup.get('quantity').disable();
    } else {
      cartItemFormGroup.get('requestDate').enable();
      /* istanbul ignore else */
      if (!isReelItem) {
        cartItemFormGroup.get('quantity').enable();
      }
    }

    /* istanbul ignore else */
    if (isReelItem) {
      cartItemFormGroup.get('quantity').disable();
    }

    return cartItemFormGroup;
  }

  private patchCartItem(cartItemFormGroup: FormGroup, cartItem: IShoppingCartItem): FormGroup {
    // * Create any missing controls before patching them

    forOwn(cartItem, (val, key) => {
      if (typeof cartItemFormGroup.controls[key] === 'undefined') {
        cartItemFormGroup.addControl(key, new FormControl());
      }
    });

    const partialItem = omit(cartItem, [
      'endCustomerRecords',
      'ncnr',
      'requestDate',
      'selectedCustomerPartNumber',
      'selectedEndCustomerSiteId',
    ]);

    cartItemFormGroup.patchValue({
      ...partialItem,
      manufacturerPartNumber: partialItem.manufacturerPartNumber,
      manufacturer: partialItem.manufacturer,
      description: partialItem.description,
    });

    const requestDateArray = [moment(cartItem.requestDate, this.DATE_FORMAT).toDate()];

    if (!isNaN(Date.parse(<any>requestDateArray[0]))) {
      cartItemFormGroup.controls['requestDate'].patchValue(requestDateArray);
    }

    /*
     * Update selectedCustomerPartNumber for form controls.
     */
    if (get(cartItem, 'selectedCustomerPartNumber') && cartItemFormGroup.controls['selectedCustomerPartNumber']) {
      cartItemFormGroup.controls['selectedCustomerPartNumber'].patchValue(cartItem.selectedCustomerPartNumber);
    }

    /*
     * Update selectedEndCustomerSiteId for form controls.
     */
    if (get(cartItem, 'selectedEndCustomerSiteId') && cartItemFormGroup.controls['selectedEndCustomerSiteId']) {
      cartItemFormGroup.controls['selectedEndCustomerSiteId'].patchValue(cartItem.selectedEndCustomerSiteId);
    }

    /*
     * Update endCustomerRecords for form controls.
     */
    if (get(cartItem, 'endCustomerRecords') && cartItemFormGroup.controls['endCustomerRecords']) {
      cartItemFormGroup.controls['endCustomerRecords'].patchValue(cartItem.endCustomerRecords);
    }

    /*
     * Sets NCNR to false if it is not present in the item
     */
    const ncnr = typeof cartItem.ncnr === 'undefined' ? false : cartItem.ncnr;
    if (cartItemFormGroup.controls['ncnr']) {
      cartItemFormGroup.controls['ncnr'].patchValue(ncnr);
    }

    cartItemFormGroup.controls['quantity'].setValidators([
      Validators.required,
      Validators.min(1),
      Validators.pattern('[0-9]+$'),
      CustomValidators.greaterThan(cartItem.minimumOrderQuantity, 'MOQ'),
      CustomValidators.multipleOf(cartItem.multipleOrderQuantity, 'multiple'),
    ]);
    this.cartItemFormGroupMap.set(cartItem.id, cartItemFormGroup);
    return cartItemFormGroup;
  }

  private buildCartItem(cartItem: IShoppingCartItem): FormGroup {
    const requestDateArray = [moment(cartItem.requestDate, this.DATE_FORMAT).toDate()];
    const cartItemFormGroup: FormGroup = this.formBuilder.group({
      ...cartItem,
      ...(cartItem.selectedCustomerPartNumber
        ? { selectedCustomerPartNumber: cartItem.selectedCustomerPartNumber }
        : { enteredCustomerPartNumber: cartItem.enteredCustomerPartNumber || null }),
      requestDate: [
        {
          value: requestDateArray,
          disabled: false,
        },
        [CustomValidators.date, Validators.required],
      ],
      quantity: [
        {
          value: cartItem.quantity,
          disabled: cartItem.arrowReel && this.isReelSelected(cartItem),
        },
        [
          Validators.required,
          Validators.min(1),
          Validators.pattern('[0-9]+$'),
          CustomValidators.greaterThan(cartItem.minimumOrderQuantity, 'MOQ'),
          CustomValidators.multipleOf(cartItem.multipleOrderQuantity, 'multiple'),
        ],
      ],
    });

    this.subscription.add(this.createObserverQuantityChanges(cartItemFormGroup, cartItem));
    this.cartItemFormGroupMap.set(cartItem.id, cartItemFormGroup);
    return cartItemFormGroup;
  }

  public createObserverQuantityChanges(cartItemFormGroup: FormGroup, cartItem: IShoppingCartItem) {
    return cartItemFormGroup
      .get('quantity')
      .valueChanges.pipe(
        debounceTime(2000),
        distinctUntilChanged(),
        skip(1)
      )
      .subscribe(quantity => {
        const freshCartItem = this.shoppingCart.find(item => item.id === cartItem.id);
        this.updateCartField('quantity', quantity, cartItem.id);
        this.getTariffValueWhenQuantityChanged(freshCartItem, quantity, cartItemFormGroup);
      });
  }

  public getTariffValueWhenQuantityChanged(cartItem: IShoppingCartItem, quantity: number, cartItemFormGroup: FormGroup) {
    if (cartItemFormGroup.get('quantity').valid && cartItem && cartItem.tariffApplicable) {
      const priceRequest: IGetLineItemPriceRequest = {
        id: cartItem.id,
        request: {
          billTo: this.billToId,
          shipTo: this.shipToId,
          docId: cartItem.docId,
          itemId: cartItem.itemId,
          warehouseId: cartItem.warehouseId,
          quantity: quantity,
          currencyCode: this.currencyCode,
        },
      };
      this.store.dispatch(new GetLineItemPrice(priceRequest));
    }
  }

  public getQuantityErrorMessage(quantity: FormControl, lineItem: IShoppingCartItem): string {
    let errorMessage: string;
    if (quantity.errors.required) {
      errorMessage = 'Please enter quantity.';
    } else if (quantity.errors.min || quantity.errors.MOQ) {
      errorMessage = `Invalid MOQ: ${lineItem.minimumOrderQuantity}`;
    } else if (quantity.errors.multiple) {
      errorMessage = `Invalid Multiple: ${lineItem.multipleOrderQuantity}`;
    }
    return errorMessage;
  }

  public toggle(i) {
    this.showTiers[i] = !this.showTiers[i];
  }

  public multiply(price, quantity) {
    return math
      .chain(price ? math.bignumber(price) : 0)
      .multiply(quantity ? math.bignumber(quantity) : 0)
      .done();
  }

  public isNCNRAgreementPending() {
    return this.cartItems.value.filter(cartItem => cartItem.ncnr && !cartItem.ncnrAccepted).length;
  }

  public isQuotedValid(cartItem: FormGroup) {
    const quantity = cartItem.controls.quantity.value;
    const { minimumOrderQuantity, multipleOrderQuantity } = cartItem.value;
    return (
      isNumber(quantity) &&
      isNumber(minimumOrderQuantity) &&
      isNumber(multipleOrderQuantity) &&
      quantity >= 1 &&
      quantity >= minimumOrderQuantity &&
      quantity % multipleOrderQuantity === 0
    );
  }

  public isItemNotValid(cartItem: IShoppingCartItem) {
    return cartItem.validation && cartItem.validation === ICartValidationStatus.INVALID;
  }

  public isItemPending(cartItem: IShoppingCartItem) {
    return cartItem.validation && cartItem.validation === ICartValidationStatus.PENDING;
  }

  public canEditCPNField(quoted: boolean): boolean {
    return quoted;
  }

  public isQuotedItemExpired(cartItem: IShoppingCartItem) {
    return cartItem.validation && cartItem.validation === ICartValidationStatus.QUOTE_EXPIRED;
  }

  public hasInvalidItems() {
    return !!this.cartItems.value.filter(cartItem => this.isItemNotValid(cartItem)).length;
  }

  public hasPendingItems() {
    return !!this.cartItems.value.filter(cartItem => this.isItemPending(cartItem)).length;
  }

  public hasValidationErrors() {
    return this.hasInvalidItems() || this.hasPendingItems() || this.hasInvalidOrExpiredQuotes();
  }

  public hasInvalidOrExpiredQuotes() {
    return !!this.cartItems.controls.filter(
      (cartItem: FormGroup) => cartItem.value.quoted && (!this.isQuotedValid(cartItem) || this.isQuotedItemExpired(cartItem.value))
    ).length;
  }

  public getLineItemCpnData(lineItem: IShoppingCartItem): ILineItemCpnInfo {
    return {
      ...(lineItem.endCustomerRecords && { endCustomerRecords: lineItem.endCustomerRecords }),
      ...(lineItem.enteredCustomerPartNumber && { enteredCpn: lineItem.enteredCustomerPartNumber }),
      ...(lineItem.selectedEndCustomerSiteId && { selectedEndCustomer: lineItem.selectedEndCustomerSiteId }),
      ...(lineItem.selectedCustomerPartNumber && { selectedCustomerPartNumber: lineItem.selectedCustomerPartNumber }),
    };
  }

  public onCustomCpnChange(value: string, control: FormControl, itemId: string): void {
    if (control) {
      control.setValue(value || null);
      this.updateCartField('enteredCustomerPartNumber', control.value, itemId);
    }
  }

  public openBillToDialog(e): void {
    e.preventDefault();
    this.dialogService.open(BillToAccountsComponent);
  }

  public hasAValidRegion(): boolean {
    return includes(ICartCheckoutValidRegions, this.userRegion);
  }

  public getCartItemPrice(cartItem: IShoppingCartItem): number {
    return has(this.privateFeatureFlags, 'quotes') && this.privateFeatureFlags.quotes && cartItem.quoted
      ? cartItem.quotedPrice || 0
      : cartItem.price || 0;
  }

  public shouldShowQuantityErrors(carItem: FormGroup): boolean {
    return carItem.controls.quantity.errors && !this.isItemNotValid(carItem.value);
  }

  private restoreQuantityValue(): void {
    this.shoppingCartUpdated
      .filter(cartItem => cartItem.quoted)
      .map(cartItem => {
        this.updateCartField('quantity', cartItem.originalQuantity, cartItem.id);
      });
  }
}
