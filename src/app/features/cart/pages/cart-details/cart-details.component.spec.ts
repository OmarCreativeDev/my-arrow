import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import * as math from 'mathjs';
import * as moment from 'moment';
import { Store, StoreModule } from '@ngrx/store';
import { of, Subscription } from 'rxjs';
import { CartService } from '@app/core/cart/cart.service';
import { CartDetailsComponent } from './cart-details.component';
import { SharedModule } from '@app/shared/shared.module';
import {
  ToggleLineItems,
  UpdateCartItems,
  UpdateCartItemsField,
  GetCartsSuccess,
  GetCarts,
  UpdateCartStore,
  UpdateCartItemReel,
} from '../../stores/cart/cart.actions';
import cartModelData, {
  invalidPriceList,
  modifiedItemList,
  validPriceList,
  mockCarts,
  mockCart,
  itemsWithErrors,
  quotedItemList,
} from './cart-model-data-mock';
import { cartReducers } from '@app/features/cart/stores/cart/cart.reducers';
import { propertiesReducers } from '@app/features/properties/store/properties.reducers';
import { userReducers } from '@app/core/user/store/user.reducers';
import { IShoppingCartItem } from '@app/core/cart/cart.interfaces';
import { BackdropServiceMock, ModalServiceMock } from '@app/app.component.spec';
import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { ModalService } from '@app/shared/services/modal.service';
import { DialogService, Dialog } from '@app/core/dialog/dialog.service';
import { DialogServiceMock, DialogMock } from '@app/core/dialog/dialog.service.spec';
import { findIndex } from 'lodash';
import { cloneDeep } from 'lodash-es';
import { IReelCallbackResponse } from '@app/shared/components/reel-dialog/reel-dialog.interfaces';
import { mockUser } from '@app/core/user/user.service.spec';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';
import { mockPrivateProperties } from '@app/core/features/feature-flags.mock';
import { PropertiesModule } from '@app/features/properties/properties.module';
import { PropertiesEffects } from '@app/features/properties/store/properties.effects';
import { PropertiesService } from '@app/core/properties/properties.service';
import { EffectsModule } from '@ngrx/effects';

class ActivatedRouteMock {
  public get params() {
    return of([]);
  }
}

class MockRouter {
  navigateByUrl(url: string) {
    return url;
  }
}

class MockCartService {
  public addToCart() {
    return of({});
  }
  public getShoppingCarts() {
    return of(mockCarts);
  }
  public getShoppingCart(id: string) {
    return of(mockCart);
  }
}

class DummyComponent {}

const mockPropertiesReducers = () => ({
  ...propertiesReducers,
  private: mockPrivateProperties,
});

class MockPropertiesService {
  public getProperties() {
    return of(mockPropertiesReducers);
  }
}

describe('CartDetailsComponent', () => {
  let component: CartDetailsComponent;
  let fixture: ComponentFixture<CartDetailsComponent>;
  let store: Store<any>;
  let router: Router;
  let formBuilder: FormBuilder;
  let dialogService: DialogService;
  const routes = [{ path: '/cart/checkout', component: DummyComponent }];

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CartDetailsComponent],
      imports: [
        SharedModule,
        PropertiesModule,
        StoreModule.forRoot({
          cart: cartReducers,
          properties: mockPropertiesReducers,
          user: userReducers,
        }),
        EffectsModule.forRoot([PropertiesEffects]),
        HttpClientTestingModule,
        RouterTestingModule.withRoutes(routes),
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        AuthTokenService,
        FormBuilder,
        { provide: PropertiesService, useClass: MockPropertiesService },
        { provide: ActivatedRoute, useClass: ActivatedRouteMock },
        { provide: BackdropService, useClass: BackdropServiceMock },
        { provide: CartService, useClass: MockCartService },
        { provide: ModalService, useClass: ModalServiceMock },
        { provide: Router, useClass: MockRouter },
        { provide: DialogService, useClass: DialogServiceMock },
        { provide: Dialog, useClass: DialogMock },
        { provide: WSSService, useClass: MockStompWSSService },
      ],
    });
    fixture = TestBed.createComponent(CartDetailsComponent);
    component = fixture.componentInstance;
    dialogService = TestBed.get(DialogService);

    fixture.detectChanges();
    component.privateFeatureFlags = mockPrivateProperties;
    component.shoppingCartId = '001';
    router = TestBed.get(Router);
    store = TestBed.get(Store);
    formBuilder = TestBed.get(FormBuilder);
    spyOn(store, 'dispatch').and.callThrough();
    spyOn(store, 'select').and.callThrough();
    spyOn(router, 'navigateByUrl').and.callThrough();
    spyOn(component, 'subscribeToWSTopic').and.returnValue(() => {});
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Init Test', () => {
    spyOn(component, 'combineDataSourcesAndInitValues').and.returnValue(of({}));

    store.dispatch(new GetCartsSuccess(mockCarts));
    component.ngOnInit();
    const action = new GetCarts();

    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('#buildCartItems should return array form with all the cart items', () => {
    const result = component.buildCartItems(cartModelData);
    expect(result.length === cartModelData.length).toBeTruthy();
    for (let index = 0; index < result.length; index++) {
      expect(result.at(index)['controls']['id']['value']).toBe(cartModelData[index].id);
    }
  });

  it('should return true when toggled', () => {
    spyOn(component, 'toggle').and.callThrough();
    component.toggle(1);
    expect(component.toggle).toHaveBeenCalled();
    expect(component.showTiers[1]).toBeTruthy();
  });

  describe('#patchOrBuildCartItem', () => {
    it('should create a FormGroup', () => {
      // Given
      const testCartItem: IShoppingCartItem = cartModelData[0];
      // When
      const result: FormGroup = component.patchOrBuildCartItem(testCartItem);
      // Then
      expect(result).toBeDefined();
      expect(result.controls.id.value).toBe(testCartItem.id);
    });

    it('should retrieve a FormGroup and patch its value', () => {
      // Given
      component.shoppingCart = cartModelData;
      const testCartItem: IShoppingCartItem = cartModelData[0];
      component.patchOrBuildCartItem(testCartItem);
      const testRequestDate = new Date();
      const testRequestDateString = moment(testRequestDate).format(component.DATE_FORMAT);
      testCartItem.requestDate = testRequestDateString;
      // When
      const result: FormGroup = component.patchOrBuildCartItem(testCartItem);
      result.controls['validation'].setValue('VALID');
      result.controls.requestDate.value[0] = testRequestDateString;
      // Then
      expect(result).toBeDefined();
      expect(component.cartItemFormGroupMap.get(testCartItem.id)).toBeDefined();
      expect(moment(result.controls.requestDate.value[0]).format(component.DATE_FORMAT)).toBe(
        moment(testRequestDate).format(component.DATE_FORMAT)
      );
      expect(component.cartItemFormGroupMap.get(testCartItem.id).get('quantity')).toBeDefined();
    });
  });

  it('#combineDataSourcesAndInitValues should init the values', () => {
    component.billToId$ = of(12);
    component.currencyCode$ = of('USD');
    component.shoppingCartId$ = of('12');
    component.userProfile$ = of(mockUser);
    component.privateFeatureFlags$ = of(true);
    component.shipToId$ = of(12);
    component.combineDataSourcesAndInitValues();
    expect(component.billToId).toEqual(12);
    expect(component.currencyCode).toEqual('USD');
    expect(component.shipToId).toEqual(12);
  });

  it('#createObserverQuantityChanges should return a subscription', () => {
    const subscription = new Subscription();
    spyOn(component, 'createObserverQuantityChanges').and.returnValue(subscription);
    component.shoppingCart = cartModelData;
    const testCartItem: IShoppingCartItem = cloneDeep(cartModelData[0]);
    testCartItem.tariffApplicable = true;
    testCartItem.quantity = 25;
    testCartItem.minimumOrderQuantity = 25;
    testCartItem.multipleOrderQuantity = 25;
    testCartItem.reel = null;
    const formGroup = component.patchOrBuildCartItem(testCartItem);
    const returnedSubscription = component.createObserverQuantityChanges(formGroup, testCartItem);
    expect(returnedSubscription).toEqual(subscription);
  });

  it('#getTariffValueWhenQuantityChanged should dispatch GetLineItemPrice', () => {
    component.shoppingCart = cartModelData;
    const testCartItem: IShoppingCartItem = cloneDeep(cartModelData[0]);
    testCartItem.tariffApplicable = true;
    testCartItem.quantity = 25;
    testCartItem.minimumOrderQuantity = 25;
    testCartItem.multipleOrderQuantity = 25;
    testCartItem.reel = null;
    const formGroup = component.patchOrBuildCartItem(testCartItem);
    component.getTariffValueWhenQuantityChanged(testCartItem, 100, formGroup);
    expect(store.dispatch).toHaveBeenCalled();
  });

  describe('#isSelected', () => {
    it('should return true if the provided lineItem is in the selection', () => {
      const lineItemId = '1.2.3';
      const lineItem = { id: lineItemId };
      const result = component.isSelected(lineItem as any, [lineItemId]);
      expect(result).toBeTruthy();
    });
    it('should return false if the provided lineItem is not in the selection', () => {
      const lineItemId = '1.2.3';
      const lineItem = { id: lineItemId };
      const result = component.isSelected(lineItem as any, []);
      expect(result).toBeFalsy();
    });
  });

  it('#toggleLineItems should map the provided lineItems to an array of Ids and dispatch a ToggleLineItems action to the store', () => {
    const product = { id: 1234, name: 'BAV99' };
    const lineItemId = '1.2.3';
    const lineItem = { id: lineItemId, itemId: 1234, manufacturerPartNumber: 'BAV99' };
    const lineItems = [lineItem];
    const operation = false;
    const action = new ToggleLineItems({ lineItemIds: [lineItemId], products: [product] }, operation);
    component.toggleLineItems(operation, lineItems as Array<any>);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('#openReelModal open the reel modal', () => {
    spyOn(dialogService, 'open').and.callThrough();
    const lineItem = {
      availableQuantity: 999,
      businessCost: 12,
      selectedCustomerPartNumber: 'XCV',
      selectedEndCustomerSiteId: 123,
      endCustomerRecords: [],
      itemId: 12345,
      manufacturerPartNumber: 'XCV-123',
      warehouseId: 123,
      reel: {
        itemsPerReel: 5,
        numberOfReels: 5,
      },
    };
    component.openReelModal(lineItem as any);
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should open the bill to account modal', () => {
    spyOn(dialogService, 'open').and.callThrough();
    const event = new MouseEvent('click');
    component.openBillToDialog(event);
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('#updateItems should dispatch an update action to the store', () => {
    const shoppingCartId = '001';
    component.shoppingCartUpdated = modifiedItemList;
    component.shoppingCartId = shoppingCartId;
    const action = new UpdateCartItems({ lineItems: modifiedItemList, shoppingCartId: shoppingCartId });
    component.updateItems();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
  describe('#readyForCheckout', () => {
    it('should return true if all of the line items have a price', () => {
      component.cartForm = formBuilder.group({});
      component.shoppingCart = validPriceList;
      const result = component.readyForCheckout;
      expect(result).toBeTruthy();
    });
    it('should return false if one of the line items does not have a price', () => {
      component.cartForm = formBuilder.group({});
      component.shoppingCart = invalidPriceList;
      const result = component.readyForCheckout;
      expect(result).toBeFalsy();
    });
    it('should return true if the the shopping cart is absenced', () => {
      component.cartForm = formBuilder.group({});
      component.shoppingCart = null;
      const result = component.readyForCheckout;
      expect(result).toBeTruthy();
    });
  });

  describe('#lineItemsChanged', () => {
    it('should return true at least one item has been modified', () => {
      component.shoppingCart = modifiedItemList;
      const result = component.lineItemsChanged;
      expect(result).toBeTruthy();
    });
    it('should return false if none item has been modified', () => {
      component.shoppingCart = cartModelData;
      const result = component.lineItemsChanged;
      expect(result).toBeFalsy();
    });

    it('should return false if the shopping cart is absenced', () => {
      component.shoppingCart = null;
      const result = component.lineItemsChanged;
      expect(result).toBeFalsy();
    });
  });

  describe('#reelIsNotSelected', () => {
    it('should be false if reel is not selected', () => {
      const testCartItem: IShoppingCartItem = cartModelData[0];
      const result =
        testCartItem.endCustomerRecords.length &&
        (!testCartItem.arrowReel ||
          !testCartItem.reel ||
          !testCartItem.reel.itemsPerReel ||
          testCartItem.reel.itemsPerReel === 0 ||
          !testCartItem.reel.numberOfReels ||
          testCartItem.reel.numberOfReels === 0);
      fixture.detectChanges();

      expect(result).toBeTruthy();
    });
  });
  describe('#getQuantityErrorMessage', () => {
    it('should return Please enter quantity when required error appears', () => {
      const quantity = new FormControl();
      const lineItem: IShoppingCartItem = cartModelData[0];

      quantity.setErrors({ required: true, MOQ: true, multiple: true });
      const result = component.getQuantityErrorMessage(quantity, lineItem);
      expect(result).toBe('Please enter quantity.');
    });
    it('should return Invalid MOQ: when MOQ error appears', () => {
      const quantity = new FormControl();
      const lineItem: IShoppingCartItem = cartModelData[0];

      quantity.setErrors({ MOQ: true, multiple: true });
      const result = component.getQuantityErrorMessage(quantity, lineItem);
      expect(result).toBe(`Invalid MOQ: ${lineItem.minimumOrderQuantity}`);
    });
    it('should return Invalid Multiple: when multiple error appears', () => {
      const quantity = new FormControl();
      const lineItem: IShoppingCartItem = cartModelData[0];

      quantity.setErrors({ multiple: true });
      const result = component.getQuantityErrorMessage(quantity, lineItem);
      expect(result).toBe(`Invalid Multiple: ${lineItem.multipleOrderQuantity}`);
    });
  });
  describe('#getTotal', () => {
    it('Should return the total of the line items', () => {
      const cartItems = component.buildCartItems(cartModelData);
      component.cartItems = cartItems;
      const result = component.cartTotal;
      const expectedTotal = cartModelData.map(a => math.multiply(a.quantity, a.price)).reduce((a, b) => math.sum(a, b), 0);
      expect(result).toBe(expectedTotal);
    });
    it('Should return the total of the line items when the price is absenced', () => {
      const mockdata = cartModelData.map(item => ({ ...item, price: null }));
      const cartItems = component.buildCartItems(mockdata);
      component.cartItems = cartItems;
      const result = component.cartTotal;
      expect(result).toBe(0);
    });

    it('Should return 0 when the cartItems are absenced', () => {
      component.cartItems = null;
      const result = component.cartTotal;
      expect(result).toBe(0);
    });
  });
  describe('#changeLineItemsDate', () => {
    it('Should call the store for updating the selected line items request date', () => {
      component.shoppingCart = cartModelData;
      const lineItemIds = ['001', '003'];
      const requestDate = '2099-12-12';
      const fieldName = 'requestDate';
      const indexes = [0, 2];
      component.selectedIds$ = of(lineItemIds);
      component.changeLineItemsDate([requestDate]);

      const action = new UpdateCartItemsField({ indexes: indexes, fieldName: fieldName, value: requestDate });
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });
  });

  describe('#updateCartField', () => {
    it('Should call the store for updating the request date', () => {
      component.shoppingCart = cartModelData;
      const fieldName = 'requestDate';
      const shoppingCartId = cartModelData[0].id;
      const value: string = moment(new Date()).format(this.DATE_FORMAT);
      const index: number = findIndex(cartModelData, { id: shoppingCartId });
      component.updateCartField('requestDate', value, shoppingCartId);

      const action = new UpdateCartStore({ fieldName, value, index });
      expect(store.dispatch).toHaveBeenCalled();
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });
  });

  it('should dialogService.open when openDeleteDialog is invoked', () => {
    component.shoppingCart = cartModelData;
    const lineItemsIds = ['787', '003'];
    const products = [{ id: 1234, name: 'BAV99' }, { id: 5678, name: 'BAV100' }];
    component.selectedIds$ = of(lineItemsIds);
    component.selectedProducts$ = of(products);
    spyOn(dialogService, 'open');
    component.openDeleteDialog();
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('isReelSelected', () => {
    component.shoppingCart = cartModelData;
    const result = component.isReelSelected(cartModelData[0]);
    expect(result).toBeTruthy();
  });

  describe('#applyReelToItem', () => {
    it('Should be called the store dispatch on applyReelToItem', () => {
      component.shoppingCart = cartModelData;
      const lineItem = cloneDeep(cartModelData[0]);

      const iReelCallbackResponse: IReelCallbackResponse = {
        cartLineId: lineItem.id,
        itemId: lineItem.itemId,
        pricePerItem: lineItem.price,
        reel: lineItem.reel,
        selectedCustomerPartNumber: lineItem.selectedCustomerPartNumber,
        selectedEndCustomerSiteId: lineItem.selectedEndCustomerSiteId,
        tariffValue: 20.34,
      };

      component.applyReelToItem(iReelCallbackResponse);
      lineItem.tariffValue = 20.34;
      lineItem.quantity = math.multiply(lineItem.reel.itemsPerReel, lineItem.reel.numberOfReels);

      const action = new UpdateCartItemReel({
        lineItem: lineItem,
        shoppingCartId: '001',
      });
      expect(store.dispatch).toHaveBeenCalled();
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });
  });

  describe('#multiply', () => {
    it('Should return a result when the price and quantity are available', () => {
      component.shoppingCart = cartModelData;
      const lineItem = cartModelData[0];
      const result = component.multiply(lineItem.price, lineItem.quantity);
      const expectedTotal = math
        .chain(lineItem.price ? math.bignumber(lineItem.price) : 0)
        .multiply(lineItem.quantity ? math.bignumber(lineItem.quantity) : 0)
        .done();
      expect(result).toEqual(expectedTotal);
    });

    it('Should return 0 when the price or quantity are absenced', () => {
      component.shoppingCart = cartModelData;
      const result = component.multiply(null, null);
      expect(result).toEqual(0);
    });
  });

  describe('#getCartItemPrice', () => {
    it('Should return quoted price for an item if its a quoted item', () => {
      const mockProperties = cloneDeep(mockPrivateProperties);
      mockProperties.featureFlags.quotes = true;
      component.privateFeatureFlags = mockProperties.featureFlags;
      const lineItem = quotedItemList[0];
      const result = component.getCartItemPrice(lineItem);
      expect(result).toEqual(quotedItemList[0].quotedPrice);
    });

    it('Should not return quoted price for an item if it is no a quoted item', () => {
      const mockProperties = cloneDeep(mockPrivateProperties);
      mockProperties.featureFlags.quotes = false;
      component.privateFeatureFlags = mockProperties.featureFlags;
      const lineItem = quotedItemList[1];
      const result = component.getCartItemPrice(lineItem);
      expect(result).toEqual(quotedItemList[1].price);
    });
  });

  describe('getLineItemCpnData', () => {
    it('should return a reduced object of type ILineItemCpnInfo', () => {
      const cpnInfo = component.getLineItemCpnData(cartModelData[0]);
      expect(cpnInfo).toEqual({
        endCustomerRecords: cartModelData[0].endCustomerRecords,
        selectedCustomerPartNumber: cartModelData[0].selectedCustomerPartNumber,
        selectedEndCustomer: cartModelData[0].selectedEndCustomerSiteId,
      });
    });
  });

  describe('#hasValidationErrors', () => {
    it('should return a number if there are errors in one or more lineItems', () => {
      const cartItems = component.buildCartItems(itemsWithErrors);
      component.cartItems = cartItems;
      const errors = component.hasValidationErrors();
      expect(errors).toBeTruthy();
    });
  });

  describe('#hasValidRegion', () => {
    it('should return validate it user has a valid region', () => {
      component.userRegion = 'arrowna';
      const validRegion = component.hasAValidRegion();
      expect(validRegion).toBeTruthy();
    });
  });

  describe('#shouldShowQuantityErrors', () => {
    it('should return true if line item has errors in the quantity field', () => {
      const cartItem: IShoppingCartItem = cartModelData[0];
      const cartItemForm: FormGroup = component.patchOrBuildCartItem(cartItem);
      cartItemForm.controls.quantity.setErrors({ required: true, MOQ: true, multiple: true });
      const result = component.shouldShowQuantityErrors(cartItemForm);
      expect(result).toBe(true);
    });

    it('should return null if line item has no errors in the quantity field', () => {
      const cartItem: IShoppingCartItem = cartModelData[0];
      const cartItemForm: FormGroup = component.patchOrBuildCartItem(cartItem);
      const result = component.shouldShowQuantityErrors(cartItemForm);
      expect(!!result).toBe(false);
    });
  });

  describe('#editedQuotedItemsPrice', () => {
    it('#findEditedQuotedItems should return a subscription', () => {
      const subscription = new Subscription();
      spyOn(component, 'findEditedQuotedItems').and.returnValue(subscription);
      component.shoppingCart = cartModelData;
      const cartItems = component.buildCartItems(quotedItemList);
      component.cartItems = cartItems;
      const returnedSubscription = component.findEditedQuotedItems();
      expect(returnedSubscription).toEqual(subscription);
    });

    it('Should dialogService.open when openQuantityUpdateDialog is invoked', () => {
      spyOn(dialogService, 'open').and.callThrough();
      component.shoppingCart = cartModelData;
      component.openQuantityUpdateDialog();
      expect(dialogService.open).toHaveBeenCalled();
    });
  });
});
