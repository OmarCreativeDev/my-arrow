const cartErrorMessages = {
  quantityZeroError: 'Please enter at least the MOQ',
  quantityMultipleError: 'Incorrect Multiple',
};
export default cartErrorMessages;
