import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule } from '@ngrx/store';
import { SharedModule } from '@app/shared/shared.module';
import { CheckoutComponent } from './checkout.component';

import { GetCheckoutDataSuccess, UpdateCheckoutOptions, PlaceOrder } from '@app/features/cart/stores/checkout/checkout.actions';
import { UpdateBillToComplete, RequestBillToAccounts, UpdateCurrencyCodeComplete } from '@app/core/user/store/user.actions';
import { checkoutReducers } from '@app/features/cart/stores/checkout/checkout.reducers';
import { userReducers, INITIAL_USER_STATE } from '@app/core/user/store/user.reducers';

import { mockUser } from '@app/core/user/user.service.spec';
import { mockBillToAccounts } from '@app/shared/components/bill-to-accounts/bill-to-accounts.component.spec';
import { mockedCheckout, mockPlaceOrderRequest } from '@app/features/cart/stores/checkout/checkout.reducers.spec';
import { of } from 'rxjs';

import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { mockPrivateProperties } from '@app/core/features/feature-flags.mock';
import { cloneDeep } from 'lodash';

class MockRouter {
  navigate(commands: any[], extras: NavigationExtras = { skipLocationChange: false }) {
    return commands;
  }
}

const initialState = {
  checkout: mockedCheckout,
  user: {
    ...INITIAL_USER_STATE,
    profile: mockUser,
    billToAccounts: mockBillToAccounts,
  },
};

describe('CheckoutComponent', () => {
  let component: CheckoutComponent;
  let fixture: ComponentFixture<CheckoutComponent>;
  let store: Store<any>;
  let router: Router;

  let shippingMethodControl: AbstractControl;
  let poNumberControl: AbstractControl;
  let termsAcceptedControl: AbstractControl;
  let exportAcceptedControl: AbstractControl;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CheckoutComponent],
      imports: [
        FormsModule,
        SharedModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        StoreModule.forRoot({ checkout: checkoutReducers, user: userReducers }, { initialState: initialState }),
        RouterTestingModule.withRoutes([]),
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{ provide: Router, useClass: MockRouter }, { provide: DialogService, useClass: DialogServiceMock }],
    }).compileComponents();
    fixture = TestBed.createComponent(CheckoutComponent);
    component = fixture.componentInstance;
    router = fixture.debugElement.injector.get(Router);
    component.privateFeatureFlags$ = of(mockPrivateProperties['featureFlags']);
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    shippingMethodControl = component.checkoutForm.get('shippingMethod');
    poNumberControl = component.checkoutForm.get('poNumber');
    termsAcceptedControl = component.checkoutForm.get('termsAccepted');
    exportAcceptedControl = component.checkoutForm.get('exportAccepted');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should set lineItems, currencyCode, subTotal and shippingOptions`, () => {
    store.dispatch(new GetCheckoutDataSuccess(mockedCheckout.data));
    component.ngOnInit();
    component.checkout$.subscribe(checkout => {
      expect(component.checkout.cart.lineItems[0].description).toBe('Diode Switching 100V 0.715A 3-Pin SC-70 T/R');
      expect(component.checkout.cart.currency).toBe('USD');
      expect(component.checkout.cart.subTotal).toBe(4062.9431);
      expect(component.checkout.shippingOptions[0].code).toBe('Standard');
    });
  });

  it('should disable the Accept button if exportAccepted is false', () => {
    shippingMethodControl.patchValue('Standard');
    poNumberControl.patchValue('po123');
    termsAcceptedControl.patchValue(true);
    exportAcceptedControl.patchValue(false);
    expect(component.checkoutForm.invalid).toBeTruthy();
  });

  it('should disable the Accept button if termsAccepted is false', () => {
    shippingMethodControl.patchValue('Standard');
    poNumberControl.patchValue('po123');
    termsAcceptedControl.patchValue(false);
    exportAcceptedControl.patchValue(true);
    expect(component.checkoutForm.invalid).toBeTruthy();
  });

  it('should disable the Accept button if purchaseOrderNumber is not set', () => {
    shippingMethodControl.patchValue('Standard');
    poNumberControl.patchValue('');
    termsAcceptedControl.patchValue(true);
    exportAcceptedControl.patchValue(true);
    expect(component.checkoutForm.invalid).toBeTruthy();
  });

  it('should enable the Accept button if criteria are met', () => {
    shippingMethodControl.patchValue('Standard');
    poNumberControl.patchValue('po123');
    termsAcceptedControl.patchValue(true);
    exportAcceptedControl.patchValue(true);
    expect(component.checkoutForm.valid).toBeTruthy();
  });

  it('should navigate to the cart on billTo update', () => {
    const spy = spyOn(router, 'navigate');
    store.dispatch(new UpdateBillToComplete({} as any));
    const url = spy.calls.first().args[0][0];
    expect(url).toBe('/cart');
  });

  it('should navigate to the cart on currency update', () => {
    const spy = spyOn(router, 'navigate');
    store.dispatch(new UpdateCurrencyCodeComplete('EUR'));
    const url = spy.calls.first().args[0][0];
    expect(url).toBe('/cart');
  });

  it(`should dispatch UpdateCheckoutOptions to store after a form value change`, () => {
    component.ngOnInit();
    shippingMethodControl.patchValue('Express');
    component.checkoutForm.valueChanges.subscribe(value => {
      expect(store.dispatch).toHaveBeenCalledWith(new UpdateCheckoutOptions(component.checkoutForm.value));
    });
  });

  it(`should place an order`, () => {
    component.placeOrder();
    expect(store.dispatch).toHaveBeenCalledWith(new PlaceOrder(mockPlaceOrderRequest));
  });

  it(`should dispatch RequestBillToAccounts$ action if billToAccounts are undefined`, () => {
    component.billToAccounts$ = of(undefined);
    component.placeOrder();
    expect(store.dispatch).toHaveBeenCalledWith(new RequestBillToAccounts());
  });

  it(`should open an NCNR dialog`, () => {
    component.openNCNRDialog([mockedCheckout.data.cart.lineItems[0]], mockedCheckout.data.cart.id, 'USD');
    expect(component.ncnrDialog).toBeDefined();
  });

  it(`should close NCNR dialog on ngOnDestroy`, () => {
    component.openNCNRDialog([mockedCheckout.data.cart.lineItems[0]], mockedCheckout.data.cart.id, 'USD');
    expect(component.ncnrDialog).toBeDefined();
    const spy = spyOn(component.ncnrDialog, 'close');
    component.ngOnDestroy();
    expect(spy).toHaveBeenCalled();
  });

  it(`should navigate to shopping cart when canceling NCNR acceptance`, () => {
    const spy = spyOn(router, 'navigate');
    component.cancelNCNRAcceptance();
    const url = spy.calls.first().args[0][0];
    const extras = spy.calls.first().args[1];
    expect(url).toBe('/cart');
    expect(extras.queryParams.warnNcnr).toBeTruthy();
  });

  it('should return false if there are errors', () => {
    poNumberControl.setErrors({ incorrect: true });
    poNumberControl.markAsTouched();
    const errors = component.hasErrors(poNumberControl);
    expect(errors).toBeTruthy();
  });

  it('should return tariff total', () => {
    const tariffTotal = 100;

    const cart = cloneDeep(component.checkout.cart);

    cart.tariffTotal = tariffTotal;

    const result = component.getCartTariffTotal(cart);

    expect(result).toEqual(tariffTotal);
  });

  it("should return 0 if there's no tariff total", () => {
    const tariffTotal = 0;
    const result = component.getCartTariffTotal(component.checkout.cart);

    expect(result).toEqual(tariffTotal);
  });

  it('should return cart total', () => {
    const cartTotal = 100;

    const cart = cloneDeep(component.checkout.cart);

    cart.total = cartTotal;

    const result = component.getCartTotal(cart);

    expect(result).toEqual(cartTotal);
  });

  it("should return subtotal if there's no cart total", () => {
    const subTotal = 100;

    const cart = cloneDeep(component.checkout.cart);

    cart.subTotal = subTotal;
    delete cart.total;

    const result = component.getCartTotal(cart);

    expect(result).toEqual(subTotal);
  });
  it('Should return true when a quantity has changed for a quoted item', () => {
    const cart = cloneDeep(component.checkout.cart);
    cart.lineItems[0].quoted = true;
    cart.lineItems[0].quantityChanged = true;
    cart.lineItems[0].originalQuantity = 2500;
    const result = component.hasQuotedItemAndQuantityChanged(cart.lineItems);
    expect(result).toBeTruthy();
  });
  it('Should return false when a quantity has not changed for a quoted item', () => {
    const cart = cloneDeep(component.checkout.cart);
    cart.lineItems[0].quoted = true;
    cart.lineItems[0].quantityChanged = false;
    cart.lineItems[0].originalQuantity = 3000;
    const result = component.hasQuotedItemAndQuantityChanged(cart.lineItems);
    expect(result).toBeFalsy();
  });
});
