import { Component, OnInit, OnDestroy, ChangeDetectorRef, AfterViewChecked } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AbstractControl } from '@angular/forms';

import { ActionsSubject, Store, select } from '@ngrx/store';
import { Observable, Subscription, combineLatest } from 'rxjs';
import { filter, map, skip, take, tap } from 'rxjs/operators';

import { find, get, has } from 'lodash-es';

import { IAppState, IListingState, IFormFieldStatus } from '@app/shared/shared.interfaces';
import {
  ICheckout,
  ICheckoutOptions,
  ICheckoutPlaceOrderRequest,
  ICheckoutCartItem,
  ICheckoutCart,
} from '@app/core/checkout/checkout.interfaces';

import {
  getCheckoutSelector,
  getLoading,
  getCheckoutOptions,
  getOrderPlacementInProgress,
  getOrderPlacementError,
  getCheckoutError,
} from '@app/features/cart/stores/checkout/checkout.selectors';
import { getCurrencyCode, getUser, getUserBillToAccounts, getUserShipToAccount } from '@app/core/user/store/user.selectors';
import { UpdateCheckoutOptions, PlaceOrder, ActivateNcnrModal } from '@app/features/cart/stores/checkout/checkout.actions';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { IUser, IBillTo } from '@app/core/user/user.interface';
import { RequestBillToAccounts } from '@app/core/user/store/user.actions';
import { INcnrDialogData } from '@app/features/cart/components/ncnr-dialog/ncnr-dialog.interfaces';
import { Dialog, DialogService } from '@app/core/dialog/dialog.service';
import { NcnrDialogComponent } from '@app/features/cart/components/ncnr-dialog/ncnr-dialog.component';

import { countryList, monthList, stateList, provinceList } from '@app/features/cart/pages/checkout/checkout.component.dropdown-data';
import { CreditCardValidator } from 'angular-cc-library';

import { ValidateCreditCard } from '@app/features/cart/stores/payment/payment.actions';
import { IPaymentRequest, CreditCardTypes } from '@app/core/payment/payment.interfaces';
import { getPaymentSelector } from '@app/features/cart/stores/payment/payment.selectors';
import { IPaymentState } from '@app/core/payment/payment.interfaces';
import { ICartStatus, ICartValidationStatus } from '@app/core/cart/cart.interfaces';
import { PartialUpdateShoppingCart, CartActionTypes } from '../../stores/cart/cart.actions';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss'],
})
export class CheckoutComponent implements OnInit, OnDestroy, AfterViewChecked {
  public checkout: ICheckout;
  public checkout$: Observable<ICheckout>;
  public checkoutError$: Observable<Error>;
  public loading$: Observable<boolean>;
  public currencyCode$: Observable<string>;
  public currencyCode: string;
  public checkoutOptions$: Observable<ICheckoutOptions>;
  public user$: Observable<IUser>;
  public billToAccounts$: Observable<IListingState<IBillTo>>;
  public orderPlacementInProgress$: Observable<boolean>;
  public orderPlacementError$: Observable<HttpErrorResponse>;
  public payment$: Observable<IPaymentState>;
  public shipToAccount$: Observable<number>;
  public changeCheckoutOptionsSub: Subscription;
  public ncnrDialog: Dialog<NcnrDialogComponent>;
  public checkoutForm: FormGroup;
  public states: Array<any>;
  public countryList: Array<any>;
  public provinces: Array<any>;
  public stateList: Array<any>;
  public yearList: Array<any>;
  public monthList: Array<any>;
  public currentYear: number;
  public ccExpYear: number;
  public cvv: number;
  public ccExpMonth: string;
  public country: string;
  public state: string;
  public province: string;
  public ccType: string;
  public stateLabel: string;
  public checkoutWithCredit: boolean;
  public userEmail: string;
  public paymentType: string = 'ONACCOUNT';
  public creditCardFailed: boolean = false;
  public profileId: string = null;
  public order: ICheckoutPlaceOrderRequest;
  public privateFeatureFlags$: Observable<any>;
  public privateFeatureFlags: object;
  public displayTariffInfo: boolean = false;
  public combinedCheckoutCurrencyCode: Subscription;
  public combinedSubscriptionThatRedirect: Subscription;
  public actionsSub: Subscription = new Subscription();

  constructor(
    private actionsSubj: ActionsSubject,
    private store: Store<IAppState>,
    private formBuilder: FormBuilder,
    private router: Router,
    private dialogService: DialogService,
    private cdRef: ChangeDetectorRef
  ) {
    /**
     * Store slices
     */
    this.user$ = this.store.pipe(select(getUser));
    this.billToAccounts$ = this.store.pipe(select(getUserBillToAccounts));
    this.checkout$ = this.store.pipe(select(getCheckoutSelector));
    this.checkoutError$ = this.store.pipe(select(getCheckoutError));
    this.currencyCode$ = this.store.pipe(select(getCurrencyCode));
    this.checkoutOptions$ = this.store.pipe(select(getCheckoutOptions));
    this.orderPlacementInProgress$ = this.store.pipe(select(getOrderPlacementInProgress));
    this.orderPlacementError$ = this.store.pipe(select(getOrderPlacementError));
    this.loading$ = this.store.pipe(select(getLoading));
    this.payment$ = this.store.pipe(select(getPaymentSelector));
    this.privateFeatureFlags$ = this.store.pipe(select(getPrivateFeatureFlagsSelector));
    this.shipToAccount$ = this.store.pipe(select(getUserShipToAccount));
  }

  ngAfterViewChecked() {
    this.cdRef.detectChanges();
  }

  ngOnInit() {
    /**
     * Initialise dropdowns
     */
    this.countryList = countryList;
    this.monthList = monthList;
    this.states = stateList;
    this.provinces = provinceList;

    this.currentYear = new Date().getFullYear();
    this.yearList = [
      {
        label: 'Select Year',
        value: '',
      },
    ];
    for (let year = this.currentYear; year <= this.currentYear + 14; year++) {
      this.yearList.push({
        label: year,
        value: year,
      });
    }

    /**
     * Set up form
     * - Build checkout form and set default values
     * - Listen to form updates and updates values in the store
     */
    this.checkoutOptions$.pipe(take(1)).subscribe(checkoutOptions => {
      this.checkoutWithCredit = checkoutOptions.checkoutWithCreditCard;
      const fields = {
        shippingMethod: [checkoutOptions.shippingMethod, Validators.required],
        shipComplete: [checkoutOptions.shipComplete, Validators.required],
        poNumber: [checkoutOptions.poNumber, Validators.required],
        salesRepReview: [checkoutOptions.salesRepReview || false, Validators.required],
        termsAccepted: [false, Validators.requiredTrue],
        exportAccepted: [false, Validators.requiredTrue],
        externalComments: [null],
      };
      const ccFields = {
        firstName: [null, Validators.required],
        lastName: [null, Validators.required],
        address1: [null, Validators.required],
        address2: [null],
        city: [null, Validators.required],
        postalCode: [null, Validators.required],
        phone: [null, Validators.required],

        country: [this.countryList[0].value, Validators.required],
        state: ['', Validators.required],
        ccNumber: [null, [<any>CreditCardValidator.validateCCNumber]],
        cvv: [null, Validators.required],
        expirationMonth: [this.monthList[0].value, Validators.required],
        expirationYear: [this.yearList[0].value, Validators.required],
      };
      const allFields = checkoutOptions.checkoutWithCreditCard ? { ...fields, ...ccFields } : fields;
      this.checkoutForm = this.formBuilder.group(allFields);

      this.changeCheckoutOptionsSub = this.checkoutForm.valueChanges.subscribe(value => {
        const externalComments = this.checkoutForm.get('externalComments');

        if (value.salesRepReview) {
          if (externalComments.status === IFormFieldStatus.Disabled) {
            externalComments.enable();
          }
          externalComments.setValidators([Validators.required]);
        } else if (externalComments.enabled === true) {
          externalComments.disable();
          externalComments.setValidators([]);
        }

        const cc = value.ccNumber + '';

        if (cc && cc.length && cc.length >= 2) {
          const arr = cc.split('');
          switch (arr[0]) {
            case '4':
              this.ccType = CreditCardTypes.VISA;
              break;
            case '5':
              this.ccType = arr[1] >= '1' && arr[1] <= '5' ? CreditCardTypes.MASTERCARD : null;
              break;
            case '2':
              this.ccType = arr[1] >= '2' && arr[1] <= '7' ? CreditCardTypes.MASTERCARD : null;
              break;
            case '3':
              this.ccType = arr[1] === '4' || arr[1] === '7' ? CreditCardTypes.AMEX : null;
              break;
            default:
              this.ccType = null;
          }
        } else {
          this.ccType = null;
        }

        if (this.checkoutForm.get('termsAccepted').status === 'VALID' || this.checkoutForm.get('exportAccepted').status === 'VALID') {
          Object.keys(this.checkoutForm.controls).map(formControl => this.checkoutForm.controls[formControl].markAsTouched());
        }

        this.store.dispatch(new UpdateCheckoutOptions(value));
      });
    });

    /**
     * Subscribe to getCheckout slice
     */
    this.combinedCheckoutCurrencyCode = combineLatest(this.checkout$, this.currencyCode$, this.user$)
      .pipe(filter(([checkout, currencyCode]) => checkout.cart !== undefined && currencyCode !== undefined))
      .subscribe(([checkout, currencyCode, user]) => {
        const prevCountry = get(this.checkout, 'shippingAddress.country');
        const newCountry = get(checkout, 'shippingAddress.country');
        const tariffCountry = 'United States';
        this.checkout = checkout;
        this.currencyCode = currencyCode;
        if (has(checkout, 'cart.lineItems')) {
          this.checkForPendingNCNRAcceptance();
          this.displayTariffInfo = !!find(checkout.cart.lineItems, item => item.tariffApplicable && item.tariffValue);
        }
        if (prevCountry && newCountry && prevCountry !== newCountry && (prevCountry === tariffCountry || newCountry === tariffCountry)) {
          this.invalidateShoppingCart(user.selectedBillTo, this.currencyCode, this.checkout.cart.id);
        }
      });

    /**
     * Must redirect to shopping cart when currency, bill to account or shipping address changes
     */
    this.combinedSubscriptionThatRedirect = combineLatest(this.currencyCode$, this.billToAccounts$)
      .pipe(skip(1))
      .subscribe(() => {
        this.navigateToCart();
      });

    /**
     * Get feature flags
     */
    this.privateFeatureFlags$.pipe(take(1)).subscribe(featureFlags => {
      this.privateFeatureFlags = featureFlags;
    });

    /**
     * Listen for NGRX actions (required for cart invalidation)
     */
    this.actionsSub.add(this.listenForActions());
  }

  private listenForActions(): Subscription {
    return this.actionsSubj.pipe(filter(action => action.type === CartActionTypes.PARTIAL_UPDATE_CART_SUCCESS)).subscribe(() => {
      this.navigateToCart();
    });
  }

  private invalidateShoppingCart(billToId: number, currency: string, shoppingCartId: string): void {
    const updateShoppingCart = new PartialUpdateShoppingCart({
      billToId: billToId,
      currency: currency,
      shoppingCartId,
      validation: ICartValidationStatus.INVALID,
      status: ICartStatus.IN_PROGRESS,
    });
    this.store.dispatch(updateShoppingCart);
  }

  private navigateToCart() {
    this.router.navigate(['/cart']);
  }

  public updateYear($event) {
    this.ccExpYear = $event;
  }

  public updateMonth($event) {
    this.ccExpMonth = $event;
  }

  public updateCountry($event) {
    this.country = $event;
    this.stateList = $event === 'US' ? this.states : this.provinces;
    this.stateLabel = $event === 'US' ? 'State' : 'Province';
    this.updateState(this.stateList[0].value);
  }

  public updateState($event) {
    this.state = $event;
  }

  ngOnDestroy() {
    if (this.changeCheckoutOptionsSub && !this.changeCheckoutOptionsSub.closed) this.changeCheckoutOptionsSub.unsubscribe();
    if (this.combinedCheckoutCurrencyCode && !this.combinedCheckoutCurrencyCode.closed) this.combinedCheckoutCurrencyCode.unsubscribe();
    if (this.combinedSubscriptionThatRedirect && !this.combinedSubscriptionThatRedirect)
      this.combinedSubscriptionThatRedirect.unsubscribe();
    if (this.actionsSub) this.actionsSub.unsubscribe();
    if (this.ncnrDialog) this.ncnrDialog.close();
  }

  /**
   * Checks for NCNR items.
   * If they exist then the NCNR acceptance dialog is opened
   */
  private checkForPendingNCNRAcceptance(): void {
    const ncnrItems = this.checkout.cart.lineItems.filter(item => item.ncnr && !item.ncnrAccepted);
    if (ncnrItems.length) {
      setTimeout(() => this.openNCNRDialog(this.checkout.cart.lineItems, this.checkout.cart.id, this.currencyCode), 1);
    }
  }

  /**
   * Opens the NCNR dialog for user acceptance of the terms
   * @param items Line items in the checkout
   * @param cartId ID of the cart, required for later update
   * @param currencyCode The current currency code
   */
  public openNCNRDialog(items: ICheckoutCartItem[], cartId: string, currencyCode: string): void {
    const ncnrDialogData: INcnrDialogData = {
      currencyCode,
      items,
      cartId,
    };
    this.ncnrDialog = this.dialogService.open(NcnrDialogComponent, {
      data: ncnrDialogData,
      size: 'x-large',
    });
    this.ncnrDialog.afterClosed.pipe(take(1)).subscribe(willRedirect => {
      if (willRedirect) this.cancelNCNRAcceptance();
    });

    if (this.ncnrDialog) {
      this.store.dispatch(new ActivateNcnrModal(this.router.url));
    }
  }

  /**
   * Redirects the user to the shopping cart if the terms for NCNR items are not accepted
   */
  public cancelNCNRAcceptance() {
    if (this.ncnrDialog) {
      this.ncnrDialog.close();
    }
    this.router.navigate(['/cart'], { queryParams: { warnNcnr: true } });
  }

  /**
   * Dispatches PlaceOrder action once neccessary mappings have been undertaken
   */
  public placeOrder() {
    if (this.checkoutWithCredit) {
      this.paymentType = 'CREDITCARD';
      this.user$.subscribe(user => {
        this.userEmail = user.email;
      });
      const paymentRequest: IPaymentRequest = {
        cardNumber: String(this.checkoutForm.value.ccNumber),
        cardExpMM: String(this.checkoutForm.value.expirationMonth),
        cardExpYYYY: String(this.checkoutForm.value.expirationYear),
        cardCVV: String(this.checkoutForm.value.cvv),
        billToFirstName: this.checkoutForm.value.firstName,
        billToLastName: this.checkoutForm.value.lastName,
        billToCity: this.checkoutForm.value.city,
        billToStateProvince: this.checkoutForm.value.state,
        billToPostalCode: String(this.checkoutForm.value.postalCode),
        billToEmail: this.userEmail,
        billToCountry: String(this.checkoutForm.value.country),
        currencyCode: this.currencyCode,
        cardType: this.ccType,
        billToStreet1: String(this.checkoutForm.value.address1),
      };
      this.store.dispatch(new ValidateCreditCard(paymentRequest));

      this.payment$.subscribe(response => {
        if (response.loading === false && response.decision && response.decision === 'ACCEPT' && response.profileId && response.requestId) {
          this.profileId = response.profileId;
          this.creditCardFailed = false;
          this.sendOrder();
        } else if (response.loading === false && response.decision && response.decision !== 'ACCEPT') {
          this.creditCardFailed = true;
        }
      });
    } else {
      this.sendOrder();
    }
  }

  private sendOrder() {
    combineLatest(this.billToAccounts$, this.checkout$, this.checkoutOptions$, this.user$)
      .pipe(
        tap(([billToAccounts]) => {
          if (!billToAccounts) {
            this.store.dispatch(new RequestBillToAccounts());
          }
        }),
        filter(([billToAccounts]) => billToAccounts !== undefined),
        map(([billToAccounts, checkoutData, checkoutOptions, user]) =>
          this.getPlaceOrderRequestMappings(billToAccounts, checkoutData, checkoutOptions, user)
        ),
        take(1)
      )
      .subscribe(results => {
        this.store.dispatch(new PlaceOrder(results));
      });
  }

  /**
   * Map available data to expected place order request
   */
  private getPlaceOrderRequestMappings(billToAccounts, checkoutData, checkoutOptions, user): ICheckoutPlaceOrderRequest {
    const hasQuotedItemAndQuantityChanged = this.hasQuotedItemAndQuantityChanged(this.checkout.cart.lineItems);
    const salesRepReview = checkoutOptions.salesRepReview || hasQuotedItemAndQuantityChanged;
    const externalComments = !hasQuotedItemAndQuantityChanged ? checkoutOptions.externalComments : 'Quantity changed for quoted item(s)';

    this.order = {
      accountInfo: {
        accountId: checkoutData.accountInfo.accountId,
        accountName: checkoutData.accountInfo.accountName,
        accountNumber: checkoutData.accountInfo.accountNumber,
        region: checkoutData.accountInfo.region,
        orgId: checkoutData.accountInfo.orgId,
      },
      cart: {
        ...checkoutData.cart,
        currency: user.currencyCode,
      },
      shippingAddress: {
        ...checkoutData.shippingAddress,
        id: user.selectedShipTo,
      },
      orderDetails: {
        ...checkoutOptions,
        ...(salesRepReview && { salesRepReview: salesRepReview }),
        bookOrder: salesRepReview ? false : true,
        ...(externalComments && { externalComments: externalComments }),
      },
      userInfo: {
        firstName: user.firstName,
        lastName: user.lastName,
      },
      paymentType: this.paymentType,
      contact: user.contact,
      billingAddress: {
        ...find(billToAccounts.items, { billToId: user.selectedBillTo }),
        id: user.selectedBillTo,
      },
    };

    /* istanbul ignore next */
    if (this.checkoutWithCredit) {
      const creditCardInfo = {
        creditCardInfo: {
          authorizationCode: this.profileId,
          merchantId: '0',
          paymentProductId: '0',
          paymentReference: this.profileId,
        },
      };

      this.order = { ...this.order, ...creditCardInfo };
    }

    return this.order;
  }

  clearCVV() {
    this.checkoutForm.controls.cvv.setValue('');
  }

  public hasErrors(field: AbstractControl) {
    return field.errors && field.touched;
  }

  public getCartTariffTotal(cart: ICheckoutCart): number {
    return cart.tariffTotal || 0;
  }

  public getCartTotal(cart: ICheckoutCart): number {
    return cart.total || cart.subTotal;
  }

  public hasQuotedItemAndQuantityChanged(lineItems: ICheckoutCartItem[]): boolean {
    return lineItems.some(lineItem => lineItem.quoted && lineItem.quantityChanged);
  }
}
