import { isProp65 } from './checkout.utils';

describe('checkout utils', () => {
  it(`should return prop65 false when country is not 'USA' or state is not 'CA'`, () => {
    expect(isProp65('Canada', 'AL')).toBeFalsy();
  });
  it(`should return prop65 true when country is 'USA' and state is 'CA'`, () => {
    expect(isProp65('United States', 'CA')).toBeTruthy();
  });
});
