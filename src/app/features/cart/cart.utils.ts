import { IPriceTier } from '@app/shared/components/price-tiers/price-tiers.interface';

export const getPrice = (quantity: number, originalPrice: number, priceTiers: IPriceTier[]) => {
  if (priceTiers) {
    for (let index = 0; index < priceTiers.length; index++) {
      const { quantityFrom: from, quantityTo: to, pricePerItem } = priceTiers[index];
      if (from <= quantity && quantity <= (to || quantity + 1)) {
        return pricePerItem;
      }
    }
  }
  return originalPrice;
};
