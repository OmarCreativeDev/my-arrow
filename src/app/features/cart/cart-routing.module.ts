import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CartDetailsComponent } from './pages/cart-details/cart-details.component';
import { CheckoutComponent } from '@app/features/cart/pages/checkout/checkout.component';
import { OrderConfirmationComponent } from '@app/features/cart/pages/order-confirmation/order-confirmation.component';
import { CheckoutGuard } from '@app/core/checkout/checkout.guard';

const cartRoutes: Routes = [
  { path: '', component: CartDetailsComponent, data: { title: 'Shopping Cart' } },
  { path: 'checkout', canActivate: [CheckoutGuard], component: CheckoutComponent, data: { title: 'Shopping Cart - Checkout' } },
  {
    path: 'checkout/confirmation',
    component: OrderConfirmationComponent,
    data: { title: 'Shopping Cart - Confirmation' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(cartRoutes)],
  exports: [RouterModule],
})
export class CartRoutingModule {}
