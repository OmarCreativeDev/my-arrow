import * as math from 'mathjs';
import { CartActions, CartActionTypes } from './cart.actions';
import { cloneDeep, findIndex, uniq, without } from 'lodash-es';
import { getPrice } from '../../cart.utils';
import { ICartInfo, IShoppingCartItem, ICartState, ICartValidationStatus } from '@app/core/cart/cart.interfaces';
import { CartDeletionStatus } from '@app/features/cart/components/delete-dialog/delete-dialog.enum';

export const INITIAL_CART_STATE: ICartState = {
  accountNumber: undefined,
  billToId: undefined,
  cartDeletionStatus: CartDeletionStatus.NONE,
  company: undefined,
  createdDate: undefined,
  currency: undefined,
  error: undefined,
  id: undefined,
  itemCount: 0,
  quotedItemCount: 0,
  maxLineItems: 0,
  remainingLineItems: 0,
  lineItems: [],
  loading: false,
  selectedIds: [],
  selectedProducts: [],
  status: undefined,
  updatedDate: undefined,
  userId: undefined,
};

export function cartReducers(state: ICartState = INITIAL_CART_STATE, action: CartActions): ICartState {
  switch (action.type) {
    case CartActionTypes.GET_CARTS: {
      return {
        ...state,
        loading: true,
      };
    }

    case CartActionTypes.GET_CARTS_SUCCESS: {
      const inProgressCart: ICartInfo = action.payload.shoppingCarts.find(cart => {
        return cart.status === 'IN_PROGRESS';
      });

      return {
        ...state,
        id: inProgressCart ? inProgressCart.id : state.id,
      };
    }

    case CartActionTypes.GET_CARTS_FAILED: {
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    }

    case CartActionTypes.GET_CART_DATA: {
      return {
        ...state,
        loading: true,
        selectedIds: [],
      };
    }
    case CartActionTypes.GET_CART_DATA_SUCCESS: {
      const { company, accountNumber, id, lineItems, status, billToId, currency, updatedDate, createdDate } = action.payload;
      const reducedLineItems = lineItems.map(lineItem => {
        const item: IShoppingCartItem = lineItem;
        const priceTiers = lineItem.priceTiers;
        item.priceTiersMissing = !priceTiers;

        if (lineItem.reel && lineItem.reel.itemsPerReel > 0 && lineItem.reel.numberOfReels > 0) {
          item.quantity = math.multiply(lineItem.reel.itemsPerReel, lineItem.reel.numberOfReels);
        } else {
          item.price = !item.priceTiersMissing
            ? getPrice(item.quantity, item.price, priceTiers)
            : lineItem.total
            ? math.divide(lineItem.total, lineItem.quantity)
            : null;
        }

        return item;
      });
      return {
        ...state,
        company,
        accountNumber,
        error: undefined,
        id,
        lineItems: reducedLineItems,
        loading: false,
        selectedIds: [],
        status,
        billToId,
        currency,
        updatedDate,
        createdDate,
      };
    }
    case CartActionTypes.GET_CART_DATA_FAILED: {
      return {
        ...state,
        company: undefined,
        accountNumber: undefined,
        error: action.payload,
        id: undefined,
        loading: false,
        selectedIds: [],
        status: undefined,
      };
    }
    case CartActionTypes.GET_CART_ITEM_COUNT_SUCCESS: {
      return {
        ...state,
        itemCount: action.payload.lineItemCount,
        maxLineItems: action.payload.lineItemMax,
        remainingLineItems: action.payload.remainingLineItems,
      };
    }
    case CartActionTypes.GET_CART_ITEM_COUNT_FAILED: {
      return {
        ...state,
        itemCount: 0,
      };
    }
    case CartActionTypes.GET_QUOTED_ITEM_COUNT_SUCCESS: {
      return {
        ...state,
        quotedItemCount: action.payload.lineItemCount,
      };
    }
    case CartActionTypes.GET_QUOTED_ITEM_COUNT_FAILED: {
      return {
        ...state,
        quotedItemCount: 0,
      };
    }
    case CartActionTypes.SELECT_LINE_ITEMS: {
      let selectedIds = [...state.selectedIds];
      let selectedProducts = [...state.selectedProducts];
      if (action.select) {
        selectedIds.push(...action.payload.lineItemIds);
        selectedProducts.push(...action.payload.products);
      } else {
        selectedIds = without(selectedIds, ...action.payload.lineItemIds);
        selectedProducts = without(selectedProducts, ...action.payload.products);
      }
      return {
        ...state,
        selectedProducts: uniq(selectedProducts),
        selectedIds: uniq(selectedIds),
      };
    }

    case CartActionTypes.CLEAN_DELETE_STATUS: {
      return {
        ...state,
        cartDeletionStatus: CartDeletionStatus.NONE,
      };
    }

    case CartActionTypes.REQUEST_ITEMS_DELETION:
    case CartActionTypes.COMPLETE_ITEMS_DELETION: {
      return {
        ...state,
        cartDeletionStatus: CartDeletionStatus.DELETING,
      };
    }

    case CartActionTypes.ITEMS_DELETION_SUCCESS: {
      let reducedLineItems = cloneDeep(state.lineItems);
      if (state.selectedIds && state.selectedIds.length) {
        reducedLineItems = reducedLineItems.filter(lineItem => !state.selectedIds.includes(lineItem.id));
      }
      return {
        ...state,
        cartDeletionStatus: CartDeletionStatus.SUCCESSFUL,
        lineItems: reducedLineItems,
        selectedIds: [],
        selectedProducts: [],
      };
    }

    case CartActionTypes.ITEMS_DELETION_REQUEST_FAILED:
    case CartActionTypes.ITEMS_DELETION_FAILED: {
      return {
        ...state,
        cartDeletionStatus: CartDeletionStatus.FAILED,
      };
    }

    case CartActionTypes.UPDATE_ITEMS_SUCCESS: {
      return {
        ...state,
        lineItems: action.payload,
        error: undefined,
        selectedIds: [],
      };
    }
    case CartActionTypes.UPDATE_ITEMS_FAILED: {
      return {
        ...state,
        error: action.payload,
      };
    }
    case CartActionTypes.UPDATE_CART_STORE: {
      const { fieldName, value, index } = action.payload;
      const cloneLineItems = cloneDeep(state.lineItems);
      const toUpdateItem: IShoppingCartItem = {
        ...cloneLineItems[index],
        [fieldName]: value,
        changed: true,
      };

      const { originalQuantity } = toUpdateItem;
      const isQuantityEdited = originalQuantity && originalQuantity !== toUpdateItem.quantity;
      toUpdateItem.isQuantityEdited = Boolean(isQuantityEdited);

      const priceTiers = toUpdateItem.priceTiers;
      if (toUpdateItem.reel && toUpdateItem.reel.itemsPerReel > 0 && toUpdateItem.reel.numberOfReels > 0) {
        // Reel items have just one price tier
        toUpdateItem.price = priceTiers && priceTiers.length ? priceTiers[0].pricePerItem : toUpdateItem.price;
      } else {
        toUpdateItem.price =
          priceTiers && priceTiers.length ? getPrice(toUpdateItem.quantity, toUpdateItem.price, priceTiers) : toUpdateItem.price;
      }
      const updateLineItems: IShoppingCartItem[] = [...cloneLineItems.slice(0, index), toUpdateItem, ...cloneLineItems.slice(index + 1)];
      return {
        ...state,
        lineItems: updateLineItems,
      };
    }
    case CartActionTypes.UPDATE_CART_CURRENCY: {
      return {
        ...state,
        currency: action.payload,
      };
    }
    case CartActionTypes.UPDATE_CART_ITEM_REEL_FAILED: {
      return {
        ...state,
        error: action.payload,
      };
    }
    case CartActionTypes.UPDATE_CART_ITEM_REEL_SUCCESS: {
      const { lineItems } = state;
      const { lineItem } = action;
      const lineItemIdx = findIndex(lineItems, item => item.id === lineItem.id);
      const reducedLineItems: IShoppingCartItem[] = [...lineItems.slice(0, lineItemIdx), lineItem, ...lineItems.slice(lineItemIdx + 1)];

      return {
        ...state,
        lineItems: reducedLineItems,
      };
    }

    case CartActionTypes.UPDATE_CART_ITEMS_FIELD: {
      const { fieldName, value, indexes } = action.payload;
      const cloneLineItems = cloneDeep(state.lineItems);
      if (indexes && indexes.length && cloneLineItems && cloneLineItems.length) {
        indexes.forEach(index => {
          cloneLineItems[index][fieldName] = value;
          cloneLineItems[index].changed = true;
        });
      }
      return {
        ...state,
        lineItems: cloneLineItems,
      };
    }

    case CartActionTypes.REFRESH_CART_ITEM_SUCCESS: {
      return {
        ...state,
        lineItems: action.payload,
        loading: false,
      };
    }

    case CartActionTypes.REFRESH_CART_ITEM_FAILED: {
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    }

    case CartActionTypes.PARTIAL_UPDATE_CART_SUCCESS: {
      const { billToId: oldBillToId, currency: oldCurrency } = state;
      const { billToId: newBillToId, currency: newCurrency } = action.payload;
      const clearCpnInfo = oldBillToId !== newBillToId;
      const markAsPending = clearCpnInfo || oldCurrency !== newCurrency;
      const clonedLineItems = cloneDeep(state.lineItems);
      if (clonedLineItems != null && clonedLineItems.length) {
        clonedLineItems.forEach(lineItem => {
          if (clearCpnInfo) {
            lineItem.selectedCustomerPartNumber = null;
            lineItem.selectedEndCustomerSiteId = null;
            lineItem.enteredCustomerPartNumber = null;
          }
          if (markAsPending) {
            lineItem.validation = ICartValidationStatus.PENDING;
          }
        });
      }
      return {
        ...state,
        currency: action.payload.currency,
        billToId: action.payload.billToId,
        status: action.payload.status,
        lineItems: clonedLineItems,
      };
    }

    case CartActionTypes.ADD_TO_CART_FAILED: {
      return {
        ...state,
        error: action.payload,
      };
    }

    case CartActionTypes.ADD_TO_QUOTE_FAILED: {
      return {
        ...state,
        error: action.payload,
      };
    }

    case CartActionTypes.GET_LINE_ITEM_PRICE_SUCCESS: {
      return {
        ...state,
        lineItems: action.payload,
        error: undefined,
      };
    }

    case CartActionTypes.GET_LINE_ITEM_PRICE_FAILED: {
      return {
        ...state,
        error: action.payload,
      };
    }

    case CartActionTypes.REMOVE_QUOTED_CART_ITEM_SUCCESS: {
      return {
        ...state,
        quotedItemCount: 0,
        lineItems: action.payload,
        error: undefined,
      };
    }

    case CartActionTypes.REMOVE_QUOTED_CART_ITEM_FAILED: {
      return {
        ...state,
        error: action.payload,
      };
    }

    case CartActionTypes.REMOVE_QUOTED_CART_ITEM:
    case CartActionTypes.REFRESH_CART_ITEM:
    case CartActionTypes.VALIDATE_ITEMS:
    case CartActionTypes.VALIDATE_ITEMS_SUCCESS:
    case CartActionTypes.VALIDATE_ITEMS_FAILED:
    default:
      return state;
  }
}
