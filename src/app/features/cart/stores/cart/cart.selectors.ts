import { createFeatureSelector, createSelector } from '@ngrx/store';

import { ICartState } from '@app/core/cart/cart.interfaces';

/**
 * Selects cart state from the root state object
 */
export const getCartState = createFeatureSelector<ICartState>('cart');

/**
 * Retrieves the search
 */
export const getCartItemsSelector = createSelector(
  getCartState,
  cartState => cartState.lineItems
);

export const getNcnrCartItemsSelector = createSelector(
  getCartState,
  cartState => cartState.lineItems.filter(item => item.ncnr && !item.ncnrAccepted)
);

/**
 * Retrieves the cart item count
 */
export const getCartItemCountSelector = createSelector(
  getCartState,
  cartState => cartState.itemCount
);

/**
 * Retrieves the quoted item count
 */
export const getQuotedItemCountSelector = createSelector(
  getCartState,
  cartState => cartState.quotedItemCount
);

/**
 * Retrieve the loading state of the page
 */
export const getLoading = createSelector(
  getCartState,
  cartState => cartState.loading
);

/**
 * Retrieve the error state of the page
 */
export const getError = createSelector(
  getCartState,
  cartState => cartState.error
);

/**
 * Get selected lines
 */
export const getSelectedIds = createSelector(
  getCartState,
  cartState => cartState.selectedIds
);

/**
 * Get selected products
 */
export const getSelectedProducts = createSelector(
  getCartState,
  cartState => cartState.selectedProducts
);

/**
 * Get in progress shopping cart id
 */
export const getShoppingCartId = createSelector(
  getCartState,
  cartState => cartState.id
);

/**
 * Retrieves the count of line items
 */
export const getCartItemsCount = createSelector(
  getCartState,
  cartState => cartState.itemCount
);

/**
 * Retrieve the number of max line items
 */
export const getCartMaxLineItems = createSelector(
  getCartState,
  cartState => cartState.maxLineItems
);

/**
 * Retrieve the number of remaining line items
 */
export const getCartRemainingLineItems = createSelector(
  getCartState,
  cartState => cartState.remainingLineItems
);

/**
 * Retrieve the cart deletion status
 */
export const getCartDeletionStatus = createSelector(
  getCartState,
  cartState => cartState.cartDeletionStatus
);

/**
 * Retrieve the cart currency
 */
export const getCartCurrency = createSelector(
  getCartState,
  cartState => cartState.currency
);
