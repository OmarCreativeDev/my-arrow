import {
  ICartLineItemCount,
  ICartsResponse,
  ICartStoreField,
  ICartStoreFields,
  IDeleteLineItemsRequest,
  IShoppingCartItem,
  IShoppingCartPatchRequest,
  IShoppingCartPatchResponse,
  IShoppingCartResponse,
  IUpdateLineItemsRequest,
  IGetCartRequest,
  IAddToCartRequest,
  IShoppingCartReelUpdate,
  ICartSelectedProduct,
  IGetLineItemPriceRequest,
  IValidatePendingItemsRequest,
  ICompleteLineItemsDeletion,
} from '@app/core/cart/cart.interfaces';
import { Action } from '@ngrx/store';
import { IAddToQuoteCartRequest } from '@app/core/quote-cart/quote-cart.interfaces';

export enum CartActionTypes {
  CREATE_CART = 'CREATE_CART',
  CREATE_CART_FAILED = 'CREATE_CART_FAILED',
  CREATE_CART_SUCCESS = 'CREATE_CART_SUCCESS',
  CLEAN_DELETE_STATUS = 'CLEAN_DELETE_STATUS',
  REQUEST_ITEMS_DELETION = 'REQUEST_ITEMS_DELETION',
  ITEMS_DELETION_REQUEST_FAILED = 'ITEMS_DELETION_REQUEST_FAILED',
  COMPLETE_ITEMS_DELETION = 'COMPLETE_ITEMS_DELETION',
  ITEMS_DELETION_SUCCESS = 'ITEMS_DELETION_SUCCESS',
  ITEMS_DELETION_FAILED = 'ITEMS_DELETION_FAILED',
  GET_CART_DATA = 'GET_CART_DATA',
  GET_CART_DATA_FAILED = 'GET_CART_DATA_FAILED',
  GET_CART_DATA_SUCCESS = 'GET_CART_DATA_SUCCESS',
  GET_CART_ITEM_COUNT = 'GET_CART_ITEM_COUNT',
  GET_CART_ITEM_COUNT_FAILED = 'GET_CART_ITEM_COUNT_FAILED',
  GET_CART_ITEM_COUNT_SUCCESS = 'GET_CART_ITEM_COUNT_SUCCESS',
  GET_QUOTED_ITEM_COUNT = 'GET_QUOTED_ITEM_COUNT',
  GET_QUOTED_ITEM_COUNT_FAILED = 'GET_QUOTED_ITEM_COUNT_FAILED',
  GET_QUOTED_ITEM_COUNT_SUCCESS = 'GET_QUOTED_ITEM_COUNT_SUCCESS',
  GET_CARTS = 'GET_CARTS',
  GET_CARTS_FAILED = 'GET_CARTS_FAILED',
  GET_CARTS_SUCCESS = 'GET_CARTS_SUCCESS',
  REQUEST_CART_PRICE_CHANGE = 'REQUEST_CART_PRICE_CHANGE',
  REQUEST_CART_PRICE_CHANGE_FAILED = 'REQUEST_CART_PRICE_CHANGE_FAILED',
  REQUEST_CART_PRICE_CHANGE_SUCCESS = 'REQUEST_CART_PRICE_CHANGE_SUCCESS',
  SELECT_LINE_ITEMS = 'SELECT_LINE_ITEMS',
  UPDATE_CART_CURRENCY = 'UPDATE_CART_CURRENCY',
  UPDATE_CART_ITEM_REEL = 'UPDATE_CART_ITEM_REEL',
  UPDATE_CART_ITEM_REEL_FAILED = 'UPDATE_CART_ITEM_REEL_FAILED',
  UPDATE_CART_ITEM_REEL_SUCCESS = 'UPDATE_CART_ITEM_REEL_SUCCESS',
  UPDATE_CART_ITEMS_FIELD = 'UPDATE_CART_ITEMS_FIELD',
  UPDATE_CART_STORE = 'UPDATE_CART_STORE',
  UPDATE_ITEMS = 'UPDATE_ITEMS',
  UPDATE_ITEMS_FAILED = 'UPDATE_ITEMS_FAILED',
  UPDATE_ITEMS_SUCCESS = 'UPDATE_ITEMS_SUCCESS',
  REFRESH_CART_ITEM = 'REFRESH_ITEM',
  REFRESH_CART_ITEM_SUCCESS = 'REFRESH_ITEM_SUCCESS',
  REFRESH_CART_ITEM_FAILED = 'REFRESH_ITEM_FAILED',
  REMOVE_QUOTED_CART_ITEM = 'REMOVE_QUOTED_CART_ITEM',
  REMOVE_QUOTED_CART_ITEM_SUCCESS = 'REMOVE_QUOTED_CART_ITEM_SUCCESS',
  REMOVE_QUOTED_CART_ITEM_FAILED = 'REMOVE_QUOTED_CART_ITEM_FAILED',
  PARTIAL_UPDATE_CART = 'PARTIAL_UPDATE_CART',
  PARTIAL_UPDATE_CART_SUCCESS = 'PARTIAL_UPDATE_CART_SUCCESS',
  PARTIAL_UPDATE_CART_FAILED = 'PARTIAL_UPDATE_CART_FAILED',
  ADD_TO_CART = 'ADD_TO_CART',
  ADD_TO_CART_SUCCESS = 'ADD_TO_CART_SUCCESS',
  ADD_TO_CART_FAILED = 'ADD_TO_CART_FAILED',
  ADD_TO_QUOTE = 'ADD_TO_QUOTE',
  ADD_TO_QUOTE_SUCCESS = 'ADD_TO_QUOTE_SUCCESS',
  ADD_TO_QUOTE_FAILED = 'ADD_TO_QUOTE_FAILED',
  GET_LINE_ITEM_PRICE = 'GET_LINE_ITEM_PRICE',
  GET_LINE_ITEM_PRICE_SUCCESS = 'GET_LINE_ITEM_PRICE_SUCCESS',
  GET_LINE_ITEM_PRICE_FAILED = 'GET_LINE_ITEM_PRICE_FAILED',
  VALIDATE_ITEMS = 'VALIDATE_ITEMS',
  VALIDATE_ITEMS_SUCCESS = 'VALIDATE_ITEMS_SUCCESS',
  VALIDATE_ITEMS_FAILED = 'VALIDATE_ITEMS_FAILED',
  SHOPPING_CART_LIMIT = 'SHOPPING_CART_LIMIT',
}

/**
 * Get Shopping Carts
 */
export class GetCarts implements Action {
  readonly type = CartActionTypes.GET_CARTS;
}

/**
 * Create Shopping Cart
 */
export class CreateCart implements Action {
  readonly type = CartActionTypes.CREATE_CART;
}

/**
 * Create Shopping Cart Success
 */
export class CreateCartSuccess implements Action {
  readonly type = CartActionTypes.CREATE_CART_SUCCESS;
}

/**
 * Create Shopping Cart Failed
 */
export class CreateCartFailed implements Action {
  readonly type = CartActionTypes.CREATE_CART_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Get Shopping Carts Success
 */
export class GetCartsSuccess implements Action {
  readonly type = CartActionTypes.GET_CARTS_SUCCESS;
  constructor(public payload: ICartsResponse) {}
}

/**
 * Get Shopping Carts failure
 */
export class GetCartsFailed implements Action {
  readonly type = CartActionTypes.GET_CARTS_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Clean delete cart status
 */
export class CleanDeleteCartStatus implements Action {
  readonly type = CartActionTypes.CLEAN_DELETE_STATUS;
}

/**
 * Request cart items deletion
 */
export class RequestCartItemsDeletion implements Action {
  readonly type = CartActionTypes.REQUEST_ITEMS_DELETION;
  constructor(public payload: IDeleteLineItemsRequest) {}
}

/**
 * Cart items deletion request failed
 */
export class CartItemsDeletionRequestFailed implements Action {
  readonly type = CartActionTypes.ITEMS_DELETION_REQUEST_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Complete cart items deletion
 */
export class CompleteCartItemsDeletion implements Action {
  readonly type = CartActionTypes.COMPLETE_ITEMS_DELETION;
  constructor(public payload: ICompleteLineItemsDeletion) {}
}

/**
 * Cart items deletion successful
 */
export class CartItemsDeletionSuccess implements Action {
  readonly type = CartActionTypes.ITEMS_DELETION_SUCCESS;
  constructor(public payload: Array<ICartSelectedProduct>) {}
}

/**
 * Cart items deletion failed
 */
export class CartItemsDeletionFailed implements Action {
  readonly type = CartActionTypes.ITEMS_DELETION_FAILED;
  constructor(public error: Error) {}
}

/**
 * Update cart items
 */
export class UpdateCartCurrency implements Action {
  readonly type = CartActionTypes.UPDATE_CART_CURRENCY;
  constructor(public payload: string) {}
}

/**
 * Update cart items
 */
export class UpdateCartItems implements Action {
  readonly type = CartActionTypes.UPDATE_ITEMS;
  constructor(public payload: IUpdateLineItemsRequest) {}
}

/**
 * Update cart items success
 */
export class UpdateCartItemsSuccess implements Action {
  readonly type = CartActionTypes.UPDATE_ITEMS_SUCCESS;
  constructor(public payload: Array<IShoppingCartItem>) {}
}

/**
 * Update cart items failure
 */
export class UpdateCartItemsFailed implements Action {
  readonly type = CartActionTypes.UPDATE_ITEMS_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Get cart items
 */
export class GetCartData implements Action {
  readonly type = CartActionTypes.GET_CART_DATA;
  constructor(public payload: IGetCartRequest) {}
}

/**
 * Get cart items success
 */
export class GetCartDataSuccess implements Action {
  readonly type = CartActionTypes.GET_CART_DATA_SUCCESS;
  constructor(public payload: IShoppingCartResponse) {}
}

/**
 * Get cart items failed
 */
export class GetCartDataFailed implements Action {
  readonly type = CartActionTypes.GET_CART_DATA_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Get cart items count
 */
export class GetCartItemCount implements Action {
  readonly type = CartActionTypes.GET_CART_ITEM_COUNT;
  constructor(public payload: { shoppingCartId: string }) {}
}

/**
 * Get cart items count success
 */
export class GetCartItemCountSuccess implements Action {
  readonly type = CartActionTypes.GET_CART_ITEM_COUNT_SUCCESS;
  constructor(public payload: ICartLineItemCount) {}
}

/**
 * Get cart items count failed
 */
export class GetCartItemCountFailed implements Action {
  readonly type = CartActionTypes.GET_CART_ITEM_COUNT_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Get quoted items count
 */
export class GetQuotedItemCount implements Action {
  readonly type = CartActionTypes.GET_QUOTED_ITEM_COUNT;
  constructor(public payload: { shoppingCartId: string }) {}
}

/**
 * Get quoted items count success
 */
export class GetQuotedItemCountSuccess implements Action {
  readonly type = CartActionTypes.GET_QUOTED_ITEM_COUNT_SUCCESS;
  constructor(public payload: ICartLineItemCount) {}
}

/**
 * Get quoted items count failed
 */
export class GetQuotedItemCountFailed implements Action {
  readonly type = CartActionTypes.GET_QUOTED_ITEM_COUNT_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Select cart line items
 */
export class ToggleLineItems implements Action {
  readonly type = CartActionTypes.SELECT_LINE_ITEMS;
  constructor(public payload: { lineItemIds: Array<string>; products?: Array<ICartSelectedProduct> }, public select: boolean) {}
}

/**
 * Update cart items
 */
export class UpdateCartStore implements Action {
  readonly type = CartActionTypes.UPDATE_CART_STORE;
  constructor(public payload: ICartStoreField) {}
}

/**
 * Update the Reel object inside a Cart's Line Item
 */
export class UpdateCartItemReel implements Action {
  readonly type = CartActionTypes.UPDATE_CART_ITEM_REEL;
  constructor(public payload: IShoppingCartReelUpdate) {}
}

/**
 * Updated successfully the Reel object inside a Cart's Line Item
 * it receives the modified data
 */
export class UpdateCartItemReelSuccess implements Action {
  readonly type = CartActionTypes.UPDATE_CART_ITEM_REEL_SUCCESS;
  constructor(public lineItem: IShoppingCartItem) {}
}

/**
 * Failed to update the Reel object inside a Cart's Line Item
 */
export class UpdateCartItemReelFailed implements Action {
  readonly type = CartActionTypes.UPDATE_CART_ITEM_REEL_FAILED;
  constructor(public payload: Error) {}
}

export class UpdateCartItemsField implements Action {
  readonly type = CartActionTypes.UPDATE_CART_ITEMS_FIELD;
  constructor(public payload: ICartStoreFields) {}
}

export class RefreshCartItem implements Action {
  readonly type = CartActionTypes.REFRESH_CART_ITEM;
  constructor(public payload: IShoppingCartItem) {}
}

export class RefreshCartItemSuccess implements Action {
  readonly type = CartActionTypes.REFRESH_CART_ITEM_SUCCESS;
  constructor(public payload: IShoppingCartItem[]) {}
}

export class RefreshCartItemFailed implements Action {
  readonly type = CartActionTypes.REFRESH_CART_ITEM_FAILED;
  constructor(public payload: Error) {}
}

export class RemoveQuotedCartItems implements Action {
  readonly type = CartActionTypes.REMOVE_QUOTED_CART_ITEM;
  constructor() {}
}

export class RemoveQuotedCartItemsSuccess implements Action {
  readonly type = CartActionTypes.REMOVE_QUOTED_CART_ITEM_SUCCESS;
  constructor(public payload: IShoppingCartItem[]) {}
}

export class RemoveQuotedCartItemsFailed implements Action {
  readonly type = CartActionTypes.REMOVE_QUOTED_CART_ITEM_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Update shopping cart after bill to account change.
 */
export class PartialUpdateShoppingCart implements Action {
  readonly type = CartActionTypes.PARTIAL_UPDATE_CART;
  constructor(public payload: IShoppingCartPatchRequest) {}
}

/**
 * Update shopping cart after bill to account change.
 */
export class PartialUpdateShoppingCartSuccess implements Action {
  readonly type = CartActionTypes.PARTIAL_UPDATE_CART_SUCCESS;
  constructor(public payload: IShoppingCartPatchResponse) {}
}

/**
 * Update shopping cart after bill to account change failure.
 */
export class PartialUpdateShoppingCartFailed implements Action {
  readonly type = CartActionTypes.PARTIAL_UPDATE_CART_FAILED;
  constructor(public payload: Error) {}
}

export class AddToCart implements Action {
  readonly type = CartActionTypes.ADD_TO_CART;
  constructor(public payload: IAddToCartRequest) {}
}

export class AddToCartSuccess implements Action {
  readonly type = CartActionTypes.ADD_TO_CART_SUCCESS;
  constructor(public payload: { request: IAddToCartRequest; path: string }) {}
}

export class AddToCartFailed implements Action {
  readonly type = CartActionTypes.ADD_TO_CART_FAILED;
  constructor(public payload: Error) {}
}

export class AddToQuote implements Action {
  readonly type = CartActionTypes.ADD_TO_QUOTE;
  constructor(public payload: IAddToQuoteCartRequest) {}
}

export class AddToQuoteSuccess implements Action {
  readonly type = CartActionTypes.ADD_TO_QUOTE_SUCCESS;
  constructor(public payload: IAddToQuoteCartRequest) {}
}

export class AddToQuoteFailed implements Action {
  readonly type = CartActionTypes.ADD_TO_QUOTE_FAILED;
  constructor(public payload: Error) {}
}

export class GetLineItemPrice implements Action {
  readonly type = CartActionTypes.GET_LINE_ITEM_PRICE;
  constructor(public payload: IGetLineItemPriceRequest) {}
}

export class GetLineItemPriceSuccess implements Action {
  readonly type = CartActionTypes.GET_LINE_ITEM_PRICE_SUCCESS;
  constructor(public payload: Array<IShoppingCartItem>) {}
}

export class GetLineItemPriceFailed implements Action {
  readonly type = CartActionTypes.GET_LINE_ITEM_PRICE_FAILED;
  constructor(public payload: Error) {}
}

export class ValidateItems implements Action {
  readonly type = CartActionTypes.VALIDATE_ITEMS;
  constructor(public payload: IValidatePendingItemsRequest) {}
}

export class ValidateItemsSuccess implements Action {
  readonly type = CartActionTypes.VALIDATE_ITEMS_SUCCESS;
}

export class ValidateItemsFailed implements Action {
  readonly type = CartActionTypes.VALIDATE_ITEMS_FAILED;
}

export class ShoppingCartLimit implements Action {
  readonly type = CartActionTypes.SHOPPING_CART_LIMIT;
  constructor(public payload: number) {}
}

export type CartActions =
  | CreateCart
  | CreateCartFailed
  | CreateCartSuccess
  | CleanDeleteCartStatus
  | RequestCartItemsDeletion
  | CartItemsDeletionRequestFailed
  | CompleteCartItemsDeletion
  | CartItemsDeletionSuccess
  | CartItemsDeletionFailed
  | GetCartData
  | GetCartDataFailed
  | GetCartDataSuccess
  | GetCartItemCount
  | GetCartItemCountFailed
  | GetCartItemCountSuccess
  | GetQuotedItemCount
  | GetQuotedItemCountFailed
  | GetQuotedItemCountSuccess
  | GetCarts
  | GetCartsFailed
  | GetCartsSuccess
  | ToggleLineItems
  | UpdateCartCurrency
  | UpdateCartItemReel
  | UpdateCartItemReelFailed
  | UpdateCartItemReelSuccess
  | UpdateCartItems
  | UpdateCartItemsFailed
  | UpdateCartItemsField
  | UpdateCartItemsSuccess
  | UpdateCartStore
  | RefreshCartItem
  | RefreshCartItemSuccess
  | RefreshCartItemFailed
  | RemoveQuotedCartItems
  | RemoveQuotedCartItemsSuccess
  | RemoveQuotedCartItemsFailed
  | PartialUpdateShoppingCart
  | PartialUpdateShoppingCartSuccess
  | PartialUpdateShoppingCartFailed
  | AddToCart
  | AddToCartSuccess
  | AddToCartFailed
  | AddToQuote
  | AddToQuoteSuccess
  | AddToQuoteFailed
  | GetLineItemPrice
  | GetLineItemPriceSuccess
  | GetLineItemPriceFailed
  | ValidateItems
  | ValidateItemsSuccess
  | ValidateItemsFailed
  | ShoppingCartLimit;
