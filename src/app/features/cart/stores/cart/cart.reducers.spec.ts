import cloneDeep from 'lodash-es/cloneDeep';
import {
  IShoppingCartItem,
  ICartLineItemCount,
  ICartsResponse,
  ICartState,
  IShoppingCartResponse,
  IShoppingCartPatchResponse,
  ICartStatus,
  ICartValidationStatus,
  IDeleteLineItemsRequest,
} from '@app/core/cart/cart.interfaces';
import {
  CartItemsDeletionSuccess,
  GetCartData,
  GetCartDataFailed,
  GetCartDataSuccess,
  GetCartItemCountFailed,
  GetCartItemCountSuccess,
  GetCarts,
  GetCartsSuccess,
  ToggleLineItems,
  UpdateCartItemReelFailed,
  UpdateCartItemReelSuccess,
  UpdateCartStore,
  PartialUpdateShoppingCartSuccess,
  GetLineItemPriceSuccess,
  GetLineItemPriceFailed,
  RefreshCartItem,
  ValidateItems,
  ValidateItemsSuccess,
  ValidateItemsFailed,
  RefreshCartItemSuccess,
  RefreshCartItemFailed,
  GetQuotedItemCountSuccess,
  GetQuotedItemCountFailed,
} from './cart.actions';
import { cartReducers, INITIAL_CART_STATE } from './cart.reducers';
import mockedLineItems from '../../pages/cart-details/cart-model-data-mock';
import * as math from 'mathjs';

describe('Shopping cart Reducers', () => {
  let initialState: ICartState;

  const mockDeleteItemsRequest: IDeleteLineItemsRequest = {
    lineItemsIds: ['5b2bbbc8c383160029b8419c', '5b2bbbc4c383160029b8419b'],
    products: [{ id: 1234, name: 'BAV99' }, { id: 5678, name: 'BAV100' }],
    shoppingCartId: 'asdasd989asd',
  };

  beforeEach(() => {
    initialState = INITIAL_CART_STATE;
  });

  it('`GET_CARTS` should set `loading` to true and not have an error property', () => {
    const action = new GetCarts();
    const result = cartReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeUndefined();
  });

  it('`GET_CARTS_SUCCESS` should set `shoppingCarts`', () => {
    const cartsResponseMock: ICartsResponse = {
      shoppingCarts: [
        {
          company: 'main account',
          billToId: 1,
          createdDate: '',
          currency: '',
          id: '5abd70cf8ef5631815b1b76b',
          status: 'IN_PROGRESS',
          terms: '',
          updatedDate: '',
          userId: 'david.foo@isawesome.com',
          accountNumber: 1,
        },
        {
          company: 'secondary account',
          billToId: 2,
          createdDate: '',
          currency: '',
          id: '5abd70cf8ef5631815b1b777',
          status: 'COMPLETE',
          terms: '',
          updatedDate: '',
          userId: 'david.green@isawesome.com',
          accountNumber: 2,
        },
      ],
      userId: 'david.foo@isawesome.com',
    };

    const action = new GetCartsSuccess(cartsResponseMock);
    const result = cartReducers(initialState, action);

    expect(result.id).toBeDefined();
    expect(result.id).toBe(cartsResponseMock.shoppingCarts[0].id);
  });

  it(`Delete Cart Action must reduce the shopping cart line items`, () => {
    const mockedData = mockedLineItems;
    const mockedDataCount = mockedData.length;
    const nextLineItemId = mockedData[1].id;
    initialState.lineItems = mockedData;
    initialState.selectedIds = [mockedData[0].id];
    const action = new CartItemsDeletionSuccess(mockDeleteItemsRequest.products);
    const result = cartReducers(initialState, action);
    expect(result.lineItems.length).toEqual(mockedDataCount - 1);
    expect(result.lineItems[0].id).toEqual(nextLineItemId);
  });

  it(`GetCartItems should get the initial state with loading in true`, () => {
    const action = new GetCartData({ shoppingCartId: '5abd70cf8ef5631815b1b76b' });
    const result = cartReducers(initialState, action);
    expect(result.error).not.toBeDefined();
    expect(result.lineItems).toBe(initialState.lineItems);
    expect(result.loading).toBeTruthy();
  });

  it(`GetCartItemsSuccess should set the results of the state`, () => {
    const response: IShoppingCartResponse = {
      company: 'asd',
      accountNumber: 1,
      id: '1',
      lineItems: [mockedLineItems[0]],
      status: 'IN_PROGRESS',
      billToId: 123,
      currency: 'USD',
      createdDate: '12-12-2017',
      updatedDate: '12-12-2017',
      itemCount: 2,
      userId: 'test',
    };
    const action = new GetCartDataSuccess(response);
    const result = cartReducers(initialState, action);
    expect(result.error).not.toBeDefined();
    expect(result.lineItems).toEqual(response.lineItems);
    expect(result.loading).toBeFalsy();
  });

  it(`GetCartItemsFailed error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new GetCartDataFailed(error);
    const result = cartReducers(initialState, action);
    expect(result.error).toBeDefined();
  });

  it(`GET_CART_ITEM_COUNT_SUCCESS should set the itemCount property`, () => {
    const response: ICartLineItemCount = {
      lineItemCount: 10,
      lineItemMax: 100,
      remainingLineItems: 0,
    };
    const action = new GetCartItemCountSuccess(response);
    const result = cartReducers(initialState, action);
    expect(result.itemCount).toBe(10);
  });

  it(`GET_CART_ITEM_COUNT_FAILED should set the itemCount property to 0`, () => {
    const error = new Error('foo');
    const action = new GetCartItemCountFailed(error);
    const result = cartReducers(initialState, action);
    expect(result.itemCount).toBe(0);
  });

  it(`GET_QUOTED_ITEM_COUNT_SUCCESS should set the quotedItemCount property`, () => {
    const response: ICartLineItemCount = {
      lineItemCount: 10,
      lineItemMax: 100,
      remainingLineItems: 0,
    };
    const action = new GetQuotedItemCountSuccess(response);
    const result = cartReducers(initialState, action);
    expect(result.quotedItemCount).toBe(10);
  });

  it(`GET_QUOTED_ITEM_COUNT_FAILED should set the quotedItemCount property to 0`, () => {
    const error = new Error('foo');
    const action = new GetQuotedItemCountFailed(error);
    const result = cartReducers(initialState, action);
    expect(result.quotedItemCount).toBe(0);
  });

  it(`SELECT_LINE_ITEMS should add the provided lineItems to the selection`, () => {
    initialState.selectedIds = [];
    const lineItemId = '1.2.2';
    const lineItems = [lineItemId];
    const expectedIds = [lineItemId];
    const action = new ToggleLineItems({ lineItemIds: lineItems }, true);
    const result = cartReducers(initialState, action);
    expect(result.selectedIds).toEqual(expectedIds);
  });

  it(`SELECT_LINE_ITEMS should remove the provided lineItems from the selection`, () => {
    const lineItemId = '1.2.2';
    const lineItems = [lineItemId];
    initialState.selectedIds = [lineItemId];
    const action = new ToggleLineItems({ lineItemIds: lineItems }, false);
    const result = cartReducers(initialState, action);
    expect(result.selectedIds).toEqual([]);
  });

  it(`SELECT_LINE_ITEMS should only return unique values in the selectedIds array`, () => {
    const lineItemId = '1.2.2';
    const lineItems = [lineItemId];
    initialState.selectedIds = [lineItemId];
    const action = new ToggleLineItems({ lineItemIds: lineItems }, true);
    const result = cartReducers(initialState, action);
    expect(result.selectedIds.length).toEqual(lineItems.length);
    expect(result.selectedIds).toEqual(lineItems);
  });

  it(`by default should return the state without transforming it`, () => {
    const result = cartReducers(undefined, {} as any);
    expect(result).toEqual(INITIAL_CART_STATE);
  });

  it(`UpdateCartItems should update the state with the new customerPartNumber`, () => {
    const lineItems: IShoppingCartItem[] = [mockedLineItems[0]];
    const cloneInitialState = cloneDeep(initialState);
    cloneInitialState.lineItems = lineItems;
    const cloneState = cloneDeep(cloneInitialState);
    cloneState.lineItems[0].customerPartNumberInput = 'DIAMOND-E-12M';
    cloneState.lineItems[0].changed = true;

    const action = new UpdateCartStore({
      fieldName: 'customerPartNumberInput',
      value: 'DIAMOND-E-12M',
      index: 0,
    });

    const result = cartReducers(cloneInitialState, action);

    expect(result).toEqual(cloneState);
  });

  it(`UPDATE_CART_ITEM_REEL_FAILED should have an error property`, () => {
    const error = new Error('foo');
    const action = new UpdateCartItemReelFailed(error);
    const result = cartReducers(initialState, action);
    expect(result.error).toBeDefined();
  });

  it(`UPDATE_CART_ITEM_REEL_SUCCESS should change the Reel object of the Line Item`, () => {
    initialState.lineItems = [mockedLineItems[1]];
    const lineItem = cloneDeep(mockedLineItems[1]);
    lineItem.reel = {
      numberOfReels: 2,
      itemsPerReel: 2,
    };
    lineItem.price = 1.234;
    lineItem.quantity = math.multiply(lineItem.reel.itemsPerReel, lineItem.reel.numberOfReels);
    const action = new UpdateCartItemReelSuccess(lineItem);
    const result = cartReducers(initialState, action);
    expect(result.lineItems[0].reel).toBe(lineItem.reel);
    expect(result.lineItems[0].price).toBe(lineItem.price);
    expect(result.lineItems[0].quantity).toBe(lineItem.quantity);
  });

  it(`PARTIAL_UPDATE_CART_SUCCESS should update the state for cart items`, () => {
    const cloneInitialState = cloneDeep(initialState);
    cloneInitialState.lineItems = mockedLineItems;
    cloneInitialState.billToId = 19870621;
    cloneInitialState.currency = 'CR';
    let actionResponse: IShoppingCartPatchResponse = {
      currency: 'USD',
      billToId: 21061987,
      status: ICartStatus.IN_PROGRESS,
    };
    let action = new PartialUpdateShoppingCartSuccess(actionResponse);
    let result = cartReducers(cloneInitialState, action);
    expect(result.billToId).toEqual(21061987);
    expect(result.currency).toEqual('USD');
    expect(result.status).toEqual(ICartStatus.IN_PROGRESS);

    // scenario 1 above (oldBillToId = 19870621, newBillToId: 21061987)
    result.lineItems.forEach(row => {
      expect(row.selectedCustomerPartNumber).toBeNull();
      expect(row.selectedEndCustomerSiteId).toBeNull();
      expect(row.validation).toEqual(ICartValidationStatus.PENDING);
    });

    // scenario 2 oldBillToId = newBillToId
    cloneInitialState.billToId = 19870621;
    actionResponse = {
      currency: 'CR',
      billToId: 19870621,
      status: ICartStatus.IN_PROGRESS,
    };
    action = new PartialUpdateShoppingCartSuccess(actionResponse);
    result = cartReducers(cloneInitialState, action);
    expect(result.lineItems).toEqual(cloneInitialState.lineItems);

    // scenario 3 oldBillToId = null newBillToId = 19870621
    cloneInitialState.billToId = null;
    actionResponse = {
      currency: 'USD',
      billToId: 19870621,
      status: ICartStatus.IN_PROGRESS,
    };
    action = new PartialUpdateShoppingCartSuccess(actionResponse);
    result = cartReducers(cloneInitialState, action);
    result.lineItems.forEach(row => {
      expect(row.selectedCustomerPartNumber).toBeNull();
      expect(row.selectedEndCustomerSiteId).toBeNull();
      expect(row.validation).toEqual(ICartValidationStatus.PENDING);
    });

    // scenario 4 cloneInitialState.lineItems = []
    cloneInitialState.lineItems = [];
    cloneInitialState.billToId = 1234556;
    actionResponse = {
      currency: 'USD',
      billToId: 19870621,
      status: ICartStatus.IN_PROGRESS,
    };
    action = new PartialUpdateShoppingCartSuccess(actionResponse);
    result = cartReducers(cloneInitialState, action);
    expect(result.lineItems.length).toEqual(0);

    // scenario 5 cloneInitialState.lineItems = null
    cloneInitialState.lineItems = null;
    cloneInitialState.billToId = 1234556;
    actionResponse = {
      currency: 'USD',
      billToId: 19870621,
      status: ICartStatus.IN_PROGRESS,
    };
    action = new PartialUpdateShoppingCartSuccess(actionResponse);
    result = cartReducers(cloneInitialState, action);
    expect(result.lineItems).toBeNull();
  });

  it(`GetLineItemPriceSuccess should return items updated with new tariff value`, () => {
    const mockedData = mockedLineItems;
    initialState.lineItems = cloneDeep(mockedData);
    mockedData[0].tariffValue = 1234;
    const action = new GetLineItemPriceSuccess(mockedData);
    const result = cartReducers(initialState, action);
    expect(result.lineItems[0].tariffValue).toEqual(1234);
  });

  it(`GetLineItemPriceFailed should have an error property`, () => {
    const error = new Error('foo');
    const action = new GetLineItemPriceFailed(error);
    const result = cartReducers(initialState, action);
    expect(result.error).toBeDefined();
  });

  it(`RefreshCartItem should return the same state`, () => {
    const action = new RefreshCartItem(mockedLineItems[0]);
    const result = cartReducers(initialState, action);
    expect(result).toBe(initialState);
  });

  it(`RefreshCartItemSuccess should update the line item in the store`, () => {
    const mockedData = cloneDeep(mockedLineItems);
    mockedData[0].warehouseId = 999999;
    const action = new RefreshCartItemSuccess(mockedData);
    const result = cartReducers(initialState, action);
    expect(result.lineItems).toBe(mockedData);
  });

  it(`RefreshCartItemFailed should return the same state`, () => {
    const error = new Error('foo');
    const action = new RefreshCartItemFailed(error);
    const result = cartReducers(initialState, action);
    expect(result.error).toBe(error);
  });

  it(`ValidateItems should return the same state`, () => {
    const action = new ValidateItems({ lineItemsIds: [mockedLineItems[0].id], shoppingCartId: '123' });
    const result = cartReducers(initialState, action);
    expect(result).toBe(initialState);
  });

  it(`ValidateItemsSuccess should return the same state`, () => {
    const action = new ValidateItemsSuccess();
    const result = cartReducers(initialState, action);
    expect(result).toBe(initialState);
  });

  it(`ValidateItemsFailed should return the same state`, () => {
    const action = new ValidateItemsFailed();
    const result = cartReducers(initialState, action);
    expect(result).toBe(initialState);
  });
});
