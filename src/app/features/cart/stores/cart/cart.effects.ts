import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, withLatestFrom } from 'rxjs/operators';
import * as math from 'mathjs';
import {
  ICartLineItemCount,
  ICartsResponse,
  ICartStatus,
  IShoppingCartResponse,
  IShoppingCartReelPatchRequest,
  ICartValidationStatus,
  IDeleteLineItemsResponse,
} from '@app/core/cart/cart.interfaces';
import { CartService } from '@app/core/cart/cart.service';
import { getPrice } from '@app/features/cart/cart.utils';

import {
  CartActionTypes,
  CreateCart,
  RefreshCartItemSuccess,
  RequestCartItemsDeletion,
  CartItemsDeletionRequestFailed,
  CompleteCartItemsDeletion,
  CartItemsDeletionSuccess,
  CartItemsDeletionFailed,
  CreateCartFailed,
  GetCartData,
  GetCartDataFailed,
  GetCartDataSuccess,
  GetCartItemCount,
  GetCartItemCountFailed,
  GetCartItemCountSuccess,
  GetQuotedItemCount,
  GetQuotedItemCountSuccess,
  GetQuotedItemCountFailed,
  GetCarts,
  GetCartsFailed,
  GetCartsSuccess,
  PartialUpdateShoppingCart,
  PartialUpdateShoppingCartFailed,
  PartialUpdateShoppingCartSuccess,
  UpdateCartItemReel,
  UpdateCartItemReelFailed,
  UpdateCartItemReelSuccess,
  UpdateCartItems,
  UpdateCartItemsFailed,
  UpdateCartItemsSuccess,
  AddToCart,
  AddToCartSuccess,
  AddToCartFailed,
  AddToQuoteSuccess,
  AddToQuoteFailed,
  AddToQuote,
  GetLineItemPrice,
  GetLineItemPriceSuccess,
  GetLineItemPriceFailed,
  ValidateItems,
  ValidateItemsSuccess,
  ValidateItemsFailed,
  RefreshCartItem,
  RemoveQuotedCartItems,
  RemoveQuotedCartItemsSuccess,
  RemoveQuotedCartItemsFailed,
  UpdateCartCurrency,
} from './cart.actions';
import { QuoteCartService } from '@app/core/quote-cart/quote-cart.service';
import { GetQuoteCartLineItemCount } from '@app/features/quotes/stores/quote-cart.actions';
import { ToggleSelected } from '@app/features/products/stores/product-search/product-search.actions';
import { getCartItemsSelector } from '@app/features/cart/stores/cart/cart.selectors';
import { IAppState } from '@app/shared/shared.interfaces';
import { find } from 'lodash';
import { InventoryService } from '@app/core/inventory/inventory.service';

@Injectable()
export class CartEffects {
  private readonly IN_PROGRESS = 'IN_PROGRESS';
  constructor(
    private actions$: Actions,
    private cartService: CartService,
    private quoteCartService: QuoteCartService,
    private inventroyService: InventoryService,
    private location: Location,
    private store: Store<IAppState>
  ) {}

  /**
   * Side-effect that combines all side effects for GetCarts facet
   * @type {Observable<any>}
   */
  @Effect()
  getCarts: Observable<any> = this.actions$.pipe(
    ofType<GetCarts>(CartActionTypes.GET_CARTS),
    switchMap(() => {
      return this.cartService.getShoppingCarts().pipe(
        map((data: ICartsResponse) => new GetCartsSuccess(data)),
        catchError(err => of(new GetCartsFailed(err)))
      );
    })
  );

  /**
   * Side-effect that combines all side effects for GetCartsSuccess facet
   * @type {Observable<GetCartItemCount | CreateCart>}
   */
  @Effect()
  getCartsSuccess$: Observable<any> = this.actions$.pipe(
    ofType<GetCartsSuccess>(CartActionTypes.GET_CARTS_SUCCESS),
    map(data => {
      if (data.payload.shoppingCarts.length) {
        const inProgressCart = data.payload.shoppingCarts.find(cart => {
          return cart.status === this.IN_PROGRESS;
        });

        this.store.dispatch(new UpdateCartCurrency(inProgressCart.currency));

        return new GetCartItemCount({ shoppingCartId: inProgressCart.id });
      } else {
        return new CreateCart();
      }
    })
  );

  /**
   * Side-effect that combines all side effects for GetCartItemCount facet
   */
  @Effect()
  getCartItemCount$ = this.actions$.pipe(
    ofType(CartActionTypes.GET_CART_ITEM_COUNT),
    switchMap((action: GetCartItemCount) => {
      return this.cartService.getLineItemCount(action.payload.shoppingCartId).pipe(
        map((data: ICartLineItemCount) => new GetCartItemCountSuccess(data)),
        catchError(err => of(new GetCartItemCountFailed(err)))
      );
    })
  );

  /**
   * Side-effect that combines all side effects for GetQuotedItemCount facet
   */
  @Effect()
  getQuotedItemCount$ = this.actions$.pipe(
    ofType(CartActionTypes.GET_QUOTED_ITEM_COUNT),
    switchMap((action: GetQuotedItemCount) => {
      return this.cartService.getLineItemCount(action.payload.shoppingCartId, true).pipe(
        map((data: ICartLineItemCount) => new GetQuotedItemCountSuccess(data)),
        catchError(err => of(new GetQuotedItemCountFailed(err)))
      );
    })
  );

  /**
   * Side-effect that combines all side effects for CreateCart facet
   * @type {Observable<any>}
   */
  @Effect()
  createCart$: Observable<any> = this.actions$.pipe(
    ofType<CreateCart>(CartActionTypes.CREATE_CART),
    switchMap(() => {
      return this.cartService.createCart().pipe(
        map(() => new GetCarts()),
        catchError(err => of(new CreateCartFailed(err)))
      );
    })
  );

  /**
   * Side-effect that combines all side effects for DeleteCartItems facet
   */
  @Effect()
  deleteCartItems$ = this.actions$.pipe(
    ofType(CartActionTypes.REQUEST_ITEMS_DELETION),
    switchMap((action: RequestCartItemsDeletion) => {
      const { products, shoppingCartId } = action.payload;
      return this.cartService.requestLineItemsDeletion(action.payload).pipe(
        map(
          (deleteRequestResponse: IDeleteLineItemsResponse) =>
            new CompleteCartItemsDeletion({ deleteRequestId: deleteRequestResponse.id, products, shoppingCartId })
        ),
        catchError(err => of(new CartItemsDeletionRequestFailed(err)))
      );
    })
  );

  /**
   * Side-effect that combines all side effects of completing the cart items deletion
   */
  @Effect()
  completeCartItemsDeletion$ = this.actions$.pipe(
    ofType(CartActionTypes.COMPLETE_ITEMS_DELETION),
    switchMap((action: CompleteCartItemsDeletion) => {
      return this.cartService.completeLineItemsDeletion(action.payload).pipe(
        switchMap(() => [
          new CartItemsDeletionSuccess(action.payload.products),
          new GetCartItemCount({ shoppingCartId: action.payload.shoppingCartId }),
        ]),
        catchError(err => of(new CartItemsDeletionFailed(err)))
      );
    })
  );

  /**
   * Side-effect that combines all side effects for UpdateCartItems facet
   */
  @Effect()
  updateCartItems$ = this.actions$.pipe(
    ofType(CartActionTypes.UPDATE_ITEMS),
    switchMap((action: UpdateCartItems) => {
      const updatedLineItems = action.payload.lineItems;
      updatedLineItems.forEach(lineItem => (lineItem.validation = ICartValidationStatus.PENDING));
      return this.cartService.updateLineItems(action.payload, true).pipe(
        withLatestFrom(this.store.pipe(select(getCartItemsSelector))),
        map(([anAction, currentLineItems]) => {
          const lineItems = currentLineItems.map(item => {
            const updatedItem = find(updatedLineItems, ['id', item.id]);
            return updatedItem ? updatedItem : item;
          });
          return new UpdateCartItemsSuccess(lineItems);
        }),
        catchError(err => of(new UpdateCartItemsFailed(err)))
      );
    })
  );

  /**
   * Side-effect that combines all side effects for GetCartData facet
   */
  @Effect()
  requestCartData$ = this.actions$.pipe(
    ofType(CartActionTypes.GET_CART_DATA),
    switchMap((action: GetCartData) => {
      action.payload.requestValidation = action.payload.requestValidation ? action.payload.requestValidation : false;
      return this.cartService.getShoppingCart(action.payload).pipe(
        map((data: IShoppingCartResponse) => new GetCartDataSuccess(data)),
        catchError(err => of(new GetCartDataFailed(err)))
      );
    })
  );

  /**
   * Side-effect that combines all side effects for
   */
  @Effect()
  updateCartItemReel$ = this.actions$.pipe(
    ofType(CartActionTypes.UPDATE_CART_ITEM_REEL),
    switchMap((action: UpdateCartItemReel) => {
      const { lineItem, shoppingCartId } = action.payload;
      const reelPatchRequest: IShoppingCartReelPatchRequest = {
        lineItemId: lineItem.id,
        price: lineItem.price,
        quantity: lineItem.quantity,
        reel: lineItem.reel,
        selectedCustomerPartNumber: lineItem.selectedCustomerPartNumber,
        selectedEndCustomerSiteId: lineItem.selectedEndCustomerSiteId,
        shoppingCartId: shoppingCartId,
      };

      return this.cartService.updateLineItemReel(reelPatchRequest).pipe(
        map(() => {
          lineItem.validation = ICartValidationStatus.PENDING;
          return new UpdateCartItemReelSuccess(lineItem);
        }),
        catchError(err => of(new UpdateCartItemReelFailed(err)))
      );
    })
  );

  @Effect()
  partialUpdateCart$ = this.actions$.pipe(
    ofType(CartActionTypes.PARTIAL_UPDATE_CART),
    switchMap((action: PartialUpdateShoppingCart) => {
      const isMarkingAsSubmitted = action.payload.status === ICartStatus.SUBMITTED;
      return this.cartService.partialUpdateCart(action.payload).pipe(
        switchMap(() => [
          new PartialUpdateShoppingCartSuccess(action.payload),
          new RemoveQuotedCartItems(),
          ...(isMarkingAsSubmitted ? [new CreateCart()] : []),
        ]),
        catchError(err => of(new PartialUpdateShoppingCartFailed(err)))
      );
    })
  );

  @Effect()
  removeQuotedCartItems$ = this.actions$.pipe(
    ofType(CartActionTypes.REMOVE_QUOTED_CART_ITEM),
    withLatestFrom(this.store.pipe(select(getCartItemsSelector))),
    map(([action, currentLineItems]) => {
      return new RemoveQuotedCartItemsSuccess(currentLineItems.filter(lineItem => !lineItem.quoted));
    }),
    catchError(err => of(new RemoveQuotedCartItemsFailed(err)))
  );

  @Effect()
  addToCart$: Observable<any> = this.actions$.pipe(
    ofType<AddToCart>(CartActionTypes.ADD_TO_CART),
    switchMap(action => {
      return this.cartService.addLineItems(action.payload).pipe(
        map(() => new AddToCartSuccess({ request: action.payload, path: this.location.path() })),
        catchError(err => of(new AddToCartFailed(err)))
      );
    })
  );

  @Effect()
  addToCartSuccess$: Observable<any> = this.actions$.pipe(
    ofType<AddToCartSuccess>(CartActionTypes.ADD_TO_CART_SUCCESS),
    switchMap(action => {
      const selectedIds = [];

      action.payload.request.lineItems.map(item => {
        selectedIds.push(item.docId);
      });

      return [new ToggleSelected(selectedIds, false), new GetCartItemCount({ shoppingCartId: action.payload.request.shoppingCartId })];
    })
  );

  @Effect()
  addToQuoteCart$: Observable<any> = this.actions$.pipe(
    ofType<AddToQuote>(CartActionTypes.ADD_TO_QUOTE),
    switchMap(action => {
      return this.quoteCartService.addLineItems(action.payload).pipe(
        map(() => new AddToQuoteSuccess(action.payload)),
        catchError(err => of(new AddToQuoteFailed(err)))
      );
    })
  );

  @Effect()
  addToQuoteCartSuccess$: Observable<any> = this.actions$.pipe(
    ofType<AddToQuoteSuccess>(CartActionTypes.ADD_TO_QUOTE_SUCCESS),
    switchMap(action => {
      const selectedIds = [];

      action.payload.lineItems.map(item => {
        selectedIds.push(item.docId);
      });

      return [new ToggleSelected(selectedIds, false), new GetQuoteCartLineItemCount(action.payload.quoteCartId)];
    })
  );

  @Effect()
  getLineItemPrice$: Observable<any> = this.actions$.pipe(
    ofType<GetLineItemPrice>(CartActionTypes.GET_LINE_ITEM_PRICE),
    switchMap(action => {
      return this.inventroyService.getPriceByQuantity(action.payload.request).pipe(
        withLatestFrom(this.store.pipe(select(getCartItemsSelector))),
        map(([productPrice, currentLineItems]) => {
          const lineItems = currentLineItems.map(item => {
            if (item.id === action.payload.id) {
              item.tariffValue = productPrice.tariffValue;
            }
            return item;
          });
          return new GetLineItemPriceSuccess(lineItems);
        }),
        catchError(err => of(new GetLineItemPriceFailed(err)))
      );
    })
  );

  @Effect()
  refreshLineItem$: Observable<any> = this.actions$.pipe(
    ofType<RefreshCartItem>(CartActionTypes.REFRESH_CART_ITEM),
    withLatestFrom(this.store.pipe(select(getCartItemsSelector))),
    map(([action, currentLineItems]) => {
      const updatedLineItem = action.payload;
      const priceTiers = updatedLineItem.priceTiers;
      updatedLineItem.priceTiersMissing = !priceTiers;
      if (updatedLineItem.reel && updatedLineItem.reel.itemsPerReel > 0 && updatedLineItem.reel.numberOfReels > 0) {
        updatedLineItem.quantity = math.multiply(updatedLineItem.reel.itemsPerReel, updatedLineItem.reel.numberOfReels);
      } else {
        updatedLineItem.price = !updatedLineItem.priceTiersMissing
          ? getPrice(updatedLineItem.quantity, updatedLineItem.price, priceTiers)
          : updatedLineItem.total
          ? math.divide(updatedLineItem.total, updatedLineItem.quantity)
          : null;
      }
      const updatedLineItems = currentLineItems.map(item => {
        const isRefreshNeeded = item.validation !== ICartValidationStatus.VALID;
        return item.id === updatedLineItem.id && isRefreshNeeded ? updatedLineItem : item;
      });
      return new RefreshCartItemSuccess(updatedLineItems);
    })
  );

  @Effect()
  validateItems$: Observable<any> = this.actions$.pipe(
    ofType<ValidateItems>(CartActionTypes.VALIDATE_ITEMS),
    switchMap(action => {
      return this.cartService.validateItems(action.payload).pipe(
        map(() => new ValidateItemsSuccess()),
        catchError(() => of(new ValidateItemsFailed()))
      );
    })
  );
}
