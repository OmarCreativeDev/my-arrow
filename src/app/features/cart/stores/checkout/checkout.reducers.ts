import { CheckoutActionTypes, CheckoutActions } from './checkout.actions';
import { ICheckoutState } from '@app/core/checkout/checkout.interfaces';
import { INcnrStatus } from '@app/features/cart/components/ncnr-dialog/ncnr-dialog.interfaces';

export const INITIAL_CHECKOUT_STATE: ICheckoutState = {
  data: {
    accountInfo: undefined,
    cart: undefined,
    shippingAddress: undefined,
    shippingOptions: [],
  },
  options: {
    poNumber: '',
    shipComplete: false,
    shippingMethod: 'Standard',
    salesRepReview: false,
    checkoutWithCreditCard: false,
    externalComments: undefined,
  },
  confirmedOrderId: undefined,
  error: undefined,
  loading: false,
  orderPlacementInProgress: false,
  orderPlacementError: undefined,
  ncnrUpdateStatus: INcnrStatus.NONE,
};

export function checkoutReducers(state: ICheckoutState = INITIAL_CHECKOUT_STATE, action: CheckoutActions): ICheckoutState {
  switch (action.type) {
    case CheckoutActionTypes.GET_CHECKOUT_DATA: {
      return {
        ...state,
        loading: true,
      };
    }
    case CheckoutActionTypes.GET_CHECKOUT_DATA_SUCCESS: {
      return {
        ...state,
        data: {
          ...action.payload,
        },
        error: undefined,
        loading: false,
        orderPlacementError: undefined,
      };
    }
    case CheckoutActionTypes.GET_CHECKOUT_DATA_FAILED: {
      return {
        ...INITIAL_CHECKOUT_STATE,
        error: action.payload,
      };
    }

    case CheckoutActionTypes.CLEAR_CHECKOUT_DATA: {
      return {
        ...state,
        data: INITIAL_CHECKOUT_STATE.data,
      };
    }

    case CheckoutActionTypes.UPDATE_CHECKOUT_OPTIONS: {
      return {
        ...state,
        options: {
          shippingMethod: action.payload.shippingMethod,
          shipComplete: action.payload.shipComplete,
          poNumber: action.payload.poNumber,
          salesRepReview: action.payload.salesRepReview,
          checkoutWithCreditCard: action.payload.checkoutWithCreditCard,
          externalComments: action.payload.externalComments,
        },
      };
    }

    case CheckoutActionTypes.UPDATE_SHIPPING_ADDRESS_IN_STORE: {
      return {
        ...state,
        data: {
          ...state.data,
          shippingAddress: action.payload,
        },
      };
    }

    case CheckoutActionTypes.PLACE_ORDER: {
      return {
        ...state,
        orderPlacementInProgress: true,
      };
    }

    case CheckoutActionTypes.PLACE_ORDER_SUCCESS: {
      return {
        ...state,
        confirmedOrderId: action.payload.orderId,
      };
    }

    case CheckoutActionTypes.PLACE_ORDER_FAILED: {
      return {
        ...state,
        orderPlacementInProgress: false,
        orderPlacementError: action.payload,
      };
    }

    case CheckoutActionTypes.PLACE_ORDER_RESET: {
      return {
        ...INITIAL_CHECKOUT_STATE,
      };
    }

    case CheckoutActionTypes.CLEAN_NCNR_STATE: {
      return {
        ...state,
        ncnrUpdateStatus: INcnrStatus.NONE,
      };
    }

    case CheckoutActionTypes.UPDATE_NCNR_AGREEMENT: {
      return {
        ...state,
        ncnrUpdateStatus: INcnrStatus.UPDATING,
      };
    }

    case CheckoutActionTypes.UPDATE_NCNR_AGREEMENT_SUCCESS: {
      return {
        ...state,
        data: action.payload,
        ncnrUpdateStatus: INcnrStatus.SUCCESSFUL,
      };
    }

    case CheckoutActionTypes.UPDATE_NCNR_AGREEMENT_FAILED: {
      return {
        ...state,
        ncnrUpdateStatus: INcnrStatus.FAILED,
      };
    }

    default:
      return state;
  }
}
