import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import {
  ICheckout,
  IShipTo,
  ICheckoutOptions,
  ICheckoutPlaceOrderRequest,
  ICheckoutConfirmation,
} from '@app/core/checkout/checkout.interfaces';
import { IAcceptNcnrItemsRequest } from '@app/features/cart/components/ncnr-dialog/ncnr-dialog.interfaces';

export enum CheckoutActionTypes {
  GET_CHECKOUT_DATA = 'GET_CHECKOUT_DATA',
  GET_CHECKOUT_DATA_SUCCESS = 'GET_CHECKOUT_DATA_SUCCESS',
  GET_CHECKOUT_DATA_FAILED = 'GET_CHECKOUT_DATA_FAILED',
  CLEAR_CHECKOUT_DATA = 'CLEAR_CHECKOUT_DATA',

  UPDATE_CHECKOUT_OPTIONS = 'UPDATE_CHECKOUT_OPTIONS',
  UPDATE_SHIPPING_ADDRESS_IN_STORE = 'UPDATE_SHIPPING_ADDRESS_IN_STORE',

  PLACE_ORDER = 'PLACE_ORDER',
  PLACE_ORDER_SUCCESS = 'PLACE_ORDER_SUCCESS',
  PLACE_ORDER_FAILED = 'PLACE_ORDER_FAILED',
  PLACE_ORDER_RESET = 'PLACE_ORDER_RESET',

  CLEAN_NCNR_STATE = 'CLEAN_NCNR_STATE',
  UPDATE_NCNR_AGREEMENT = 'UPDATE_NCNR_AGREEMENT',
  UPDATE_NCNR_AGREEMENT_FAILED = 'UPDATE_NCNR_AGREEMENT_FAILED',
  UPDATE_NCNR_AGREEMENT_SUCCESS = 'UPDATE_NCNR_AGREEMENT_SUCCESS',

  ACTIVATE_NCNR_MODAL = 'ACTIVATE_NCNR_MODAL',
}

/**
 * Get checkout data
 */
export class GetCheckoutData implements Action {
  readonly type = CheckoutActionTypes.GET_CHECKOUT_DATA;
}

/**
 * Get checkout data sucess
 */
export class GetCheckoutDataSuccess implements Action {
  readonly type = CheckoutActionTypes.GET_CHECKOUT_DATA_SUCCESS;
  constructor(public payload: ICheckout) {}
}

/**
 * Get checkout data failed
 */
export class GetCheckoutDataFailed implements Action {
  readonly type = CheckoutActionTypes.GET_CHECKOUT_DATA_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Clean checkout store data
 */
export class ClearCheckoutData implements Action {
  readonly type = CheckoutActionTypes.CLEAR_CHECKOUT_DATA;
}

/**
 * Update checkout options
 */
export class UpdateCheckoutOptions implements Action {
  readonly type = CheckoutActionTypes.UPDATE_CHECKOUT_OPTIONS;
  constructor(public payload: ICheckoutOptions) {}
}

/**
 * Update shipping address
 */
export class UpdateShippingAddressInStore implements Action {
  readonly type = CheckoutActionTypes.UPDATE_SHIPPING_ADDRESS_IN_STORE;
  constructor(public payload: IShipTo) {}
}

export class PlaceOrder implements Action {
  readonly type = CheckoutActionTypes.PLACE_ORDER;
  constructor(public payload: ICheckoutPlaceOrderRequest) {}
}

export class PlaceOrderSuccess implements Action {
  readonly type = CheckoutActionTypes.PLACE_ORDER_SUCCESS;
  constructor(public payload: ICheckoutConfirmation) {}
}

export class PlaceOrderFailed implements Action {
  readonly type = CheckoutActionTypes.PLACE_ORDER_FAILED;
  constructor(public payload: HttpErrorResponse) {}
}

export class PlaceOrderReset implements Action {
  readonly type = CheckoutActionTypes.PLACE_ORDER_RESET;
  constructor() {}
}

/**
 * Cleans the NCNR state for reuse
 */
export class CleanNCNRState implements Action {
  readonly type = CheckoutActionTypes.CLEAN_NCNR_STATE;
}

/**
 * Update cart items NCNR Agreement in local store
 */
export class UpdateNCNRAgreement implements Action {
  readonly type = CheckoutActionTypes.UPDATE_NCNR_AGREEMENT;
  constructor(public payload: IAcceptNcnrItemsRequest) {}
}

/**
 * Update cart items NCNR Agreement success
 */
export class UpdateNCNRAgreementSuccess implements Action {
  readonly type = CheckoutActionTypes.UPDATE_NCNR_AGREEMENT_SUCCESS;
  constructor(public payload: ICheckout) {}
}

/**
 * Update cart items NCNR Agreement failure
 */
export class UpdateNCNRAgreementFailed implements Action {
  readonly type = CheckoutActionTypes.UPDATE_NCNR_AGREEMENT_FAILED;
  constructor(public payload: Error) {}
}

export class ActivateNcnrModal implements Action {
  readonly type = CheckoutActionTypes.ACTIVATE_NCNR_MODAL;
  constructor(public payload: string) {}
}

export type CheckoutActions =
  | GetCheckoutData
  | GetCheckoutDataSuccess
  | GetCheckoutDataFailed
  | ClearCheckoutData
  | UpdateCheckoutOptions
  | UpdateShippingAddressInStore
  | PlaceOrder
  | PlaceOrderSuccess
  | PlaceOrderFailed
  | PlaceOrderReset
  | CleanNCNRState
  | UpdateNCNRAgreement
  | UpdateNCNRAgreementFailed
  | UpdateNCNRAgreementSuccess
  | ActivateNcnrModal;
