import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ICheckoutState } from '@app/core/checkout/checkout.interfaces';

/**
 * Selects cart state from the root state object
 */
export const getCheckoutState = createFeatureSelector<ICheckoutState>('checkout');

/**
 * Retrieves the checkout
 */
export const getCheckoutSelector = createSelector(getCheckoutState, checkoutState => checkoutState.data);

/**
 * Retrieves the checkout options
 */
export const getCheckoutOptions = createSelector(getCheckoutState, checkoutState => checkoutState.options);

/**
 * Retrieve the loading state of the page
 */
export const getLoading = createSelector(getCheckoutState, checkoutState => checkoutState.loading);

/**
 * Retrieve the error state of the page
 */
export const getCheckoutError = createSelector(getCheckoutState, checkoutState => checkoutState.error);

/**
 * Flag that states if an order placement is in progress
 */
export const getOrderPlacementInProgress = createSelector(getCheckoutState, checkoutState => checkoutState.orderPlacementInProgress);

/**
 * Order palcement error selector
 */
export const getOrderPlacementError = createSelector(getCheckoutState, checkoutState => checkoutState.orderPlacementError);

/**
 * The id of the confirmed order
 */
export const getConfirmedOrderId = createSelector(getCheckoutState, checkoutState => checkoutState.confirmedOrderId);

/**
 * Retrieves the checkout address
 */
export const getShippingAddressSelector = createSelector(getCheckoutState, checkoutState => checkoutState.data.shippingAddress);

/**
 * Retrieves the checkout account info
 */
export const getShippingAccountInfoSelector = createSelector(getCheckoutState, checkoutState => checkoutState.data.accountInfo);

/**
 * Retrieves the NCNR update progress status
 */
export const getNcnrUpdateStatus = createSelector(getCheckoutState, checkoutState => checkoutState.ncnrUpdateStatus);
