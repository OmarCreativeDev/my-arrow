import { ICheckoutState, ICheckoutPlaceOrderRequest } from '@app/core/checkout/checkout.interfaces';
import { INcnrStatus } from '@app/features/cart/components/ncnr-dialog/ncnr-dialog.interfaces';
import {
  GetCheckoutData,
  GetCheckoutDataSuccess,
  GetCheckoutDataFailed,
  UpdateShippingAddressInStore,
  PlaceOrder,
  PlaceOrderSuccess,
  PlaceOrderFailed,
} from './checkout.actions';
import { INITIAL_CHECKOUT_STATE, checkoutReducers } from './checkout.reducers';
import { HttpErrorResponse } from '@angular/common/http';
import { mockUser } from '@app/core/user/user.service.spec';

export const mockedCheckout: ICheckoutState = {
  orderPlacementInProgress: false,
  orderPlacementError: undefined,
  ncnrUpdateStatus: INcnrStatus.NONE,
  data: {
    accountInfo: {
      accountName: 'B & C ELECTRONIC ENGINEERING',
      accountNumber: 1067644,
      terms: '30 DAYS NET',
      registrationInstance: 'AC',
      region: 'arrowna',
      paymentsTermId: 4,
      paymentsTermName: '30 NET',
      currencyCode: 'USD',
      orgId: 241,
      selectedBillTo: 2171487,
      selectedShipTo: 2172033,
      accountId: 1305704,
    },
    shippingAddress: {
      primaryFlag: true,
      name: 'B & C ELECTRONIC ENGINEERING',
      address1: '  ',
      address2: '185 W Louisiana Avenue',
      address3: '',
      address4: null,
      city: 'Denver',
      county: 'Denver',
      state: 'CO',
      country: 'US',
      postCode: '80223',
    },
    shippingOptions: [{ name: 'Standard', code: 'Standard' }, { name: 'Express (Fastest Available)', code: 'Express' }],
    cart: {
      company: 'B & C ELECTRONIC ENGINEERING',
      billToId: 2171487,
      currency: 'USD',
      accountNumber: null,
      id: '5b0e8f35d93ddf000128c9db',
      userId: 'seamless_test@gmail.com',
      status: 'IN_PROGRESS',
      lineItems: [
        {
          id: '5b16a3cb768ae20001304291',
          total: 34.68,
          ncnrAccepted: null,
          ncnrAcceptedBy: null,
          manufacturerPartNumber: 'BAV99WT1G',
          valid: null,
          itemType: null,
          selectedEndCustomerSiteName: null,
          manufacturer: 'ON Semiconductor|ONSEMI',
          description: 'Diode Switching 100V 0.715A 3-Pin SC-70 T/R',
          image: 'http://download.siliconexpert.com/pdfs/2017/5/11/9/7/8/409/ons_/manual/3sc70.jpg',
          inStock: true,
          quotable: null,
          arrowReel: null,
          ncnr: true,
          businessCost: null,
          minimumOrderQuantity: 3000,
          multipleOrderQuantity: 3000,
          availableQuantity: 731999,
          bufferQuantity: 0,
          leadTime: '15',
          price: 0.01156,
          validation: 'PENDING',
          itemId: 7323176,
          warehouseId: 1790,
          docId: '1790_07323176',
          selectedCustomerPartNumber: null,
          enteredCustomerPartNumber: null,
          selectedEndCustomerSiteId: null,
          requestDate: '2018-06-08',
          quantity: 3000,
          reel: null,
        },
        {
          id: '5b16a418768ae20001304295',
          total: 2267.79,
          ncnrAccepted: null,
          ncnrAcceptedBy: null,
          manufacturerPartNumber: 'LM95071CIMFX/NOPB',
          valid: null,
          itemType: null,
          selectedEndCustomerSiteName: null,
          manufacturer: 'Texas Instruments|TI',
          description: 'Temp Sensor Digital Serial (3-Wire) 5-Pin SOT-23 T/R',
          image: 'http://download.siliconexpert.com/pdfs/2017/3/5/5/40/26/834/txn_/manual/dbv0005a.jpg',
          inStock: false,
          quotable: null,
          arrowReel: null,
          ncnr: true,
          businessCost: null,
          minimumOrderQuantity: 3000,
          multipleOrderQuantity: 3000,
          availableQuantity: 3000,
          bufferQuantity: 0,
          leadTime: '21',
          price: 0.75593,
          validation: 'VALID',
          itemId: 7283092,
          warehouseId: 1801,
          docId: '1801_07283092',
          selectedCustomerPartNumber: null,
          enteredCustomerPartNumber: null,
          selectedEndCustomerSiteId: null,
          requestDate: '2018-06-08',
          quantity: 3000,
          reel: null,
        },
      ],
      subTotal: 4062.9431,
      itemCount: 5,
    },
  },
  options: {
    shippingMethod: 'Standard',
    shipComplete: false,
    poNumber: 'poc1234',
    salesRepReview: true,
    checkoutWithCreditCard: false,
  },
  confirmedOrderId: 123,
  error: undefined,
  loading: false,
};

export const mockPlaceOrderRequest: ICheckoutPlaceOrderRequest = {
  accountInfo: { accountId: 1305704, accountName: 'B & C ELECTRONIC ENGINEERING', accountNumber: 1067644, region: 'arrowna', orgId: 241 },
  cart: mockedCheckout.data.cart,
  shippingAddress: {
    primaryFlag: true,
    name: 'B & C ELECTRONIC ENGINEERING',
    address1: '  ',
    address2: '185 W Louisiana Avenue',
    address3: '',
    address4: null,
    city: 'Denver',
    county: 'Denver',
    state: 'CO',
    country: 'US',
    postCode: '80223',
    id: 2172033,
  },
  orderDetails: {
    shippingMethod: 'Standard',
    shipComplete: false,
    poNumber: 'poc1234',
    salesRepReview: true,
    bookOrder: false,
    checkoutWithCreditCard: false,
  },
  userInfo: { firstName: 'David', lastName: 'Lane' },
  paymentType: 'ONACCOUNT',
  contact: mockUser.contact,
  billingAddress: {
    name: 'B & C ELECTRONIC ENGINEERING',
    accountId: 1305704,
    accountNumber: 1067644,
    ebsPersonId: 7054454,
    billToId: 2171627,
    orgId: 241,
    address1: ' ',
    address2: '185 W Louisiana Avenue',
    address3: '',
    address4: '',
    city: 'Denver',
    state: 'CO',
    province: '',
    country: 'US',
    countryDescription: 'United States',
    postalCode: '80223',
    selected: null,
    id: 2171627,
  },
};

describe('Checkout Reducers', () => {
  let initialState: ICheckoutState;

  beforeEach(() => {
    initialState = INITIAL_CHECKOUT_STATE;
  });

  it(`GetCheckoutData should get the initial state with loading in true`, () => {
    const action = new GetCheckoutData();
    const result = checkoutReducers(initialState, action);
    expect(result.error).not.toBeDefined();
    expect(result.data.cart).toBe(initialState.data.cart);
    expect(result.loading).toBeTruthy();
  });

  it(`GetCheckoutDataSuccess should set the results of the state`, () => {
    const response: ICheckoutState = mockedCheckout;
    const action = new GetCheckoutDataSuccess(response.data);
    const result = checkoutReducers(initialState, action);
    expect(result.error).not.toBeDefined();
    expect(result.data.cart).toEqual(response.data.cart);
    expect(result.loading).toBeFalsy();
  });

  it(`GetCheckoutDataFailed error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new GetCheckoutDataFailed(error);
    const result = checkoutReducers(initialState, action);
    expect(result.error).toBeDefined();
  });

  it(`should update state on UPDATE_SHIPPING_ADDRESS_IN_STORE`, () => {
    const action = new UpdateShippingAddressInStore(mockedCheckout.data.shippingAddress);
    const result = checkoutReducers(initialState, action);
    expect(result.data.shippingAddress).toEqual(mockedCheckout.data.shippingAddress);
  });

  it(`should update state on PLACE_ORDER`, () => {
    const action = new PlaceOrder(mockPlaceOrderRequest);
    const result = checkoutReducers(initialState, action);
    expect(result.orderPlacementInProgress).toBeTruthy();
  });

  it(`should update state on PLACE_ORDER_SUCCESS`, () => {
    const action = new PlaceOrderSuccess({ ...mockPlaceOrderRequest, orderId: 1234 });
    const result = checkoutReducers(initialState, action);
    expect(result).toEqual({ ...initialState, confirmedOrderId: 1234 });
  });

  it(`should update state on PLACE_ORDER_FAILED`, () => {
    const action = new PlaceOrderFailed(new HttpErrorResponse({ error: { status: '1000' } }));
    const result = checkoutReducers({ ...initialState, orderPlacementInProgress: true }, action);
    expect(result.orderPlacementInProgress).toBeFalsy();
    expect(result.orderPlacementError).toEqual(new HttpErrorResponse({ error: { status: '1000' } }));
  });
});
