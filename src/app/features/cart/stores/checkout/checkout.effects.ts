import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { Store, select } from '@ngrx/store';
import { of } from 'rxjs';
import { map, catchError, switchMap, withLatestFrom } from 'rxjs/operators';
import { cloneDeep, get, find } from 'lodash-es';
import { ICartStatus } from '@app/core/cart/cart.interfaces';
import { IAppState } from '@app/shared/shared.interfaces';
import {
  GetCheckoutDataSuccess,
  GetCheckoutDataFailed,
  CheckoutActionTypes,
  PlaceOrderSuccess,
  PlaceOrderFailed,
  PlaceOrder,
  UpdateNCNRAgreement,
  UpdateNCNRAgreementSuccess,
  UpdateNCNRAgreementFailed,
} from './checkout.actions';
import { CheckoutService } from '@app/core/checkout/checkout.service';
import { OrdersService } from '@app/core/orders/orders.service';
import { ICheckout } from '@app/core/checkout/checkout.interfaces';
import { Router } from '@angular/router';
import { PartialUpdateShoppingCart } from '@app/features/cart/stores/cart/cart.actions';
import { getCheckoutSelector } from '@app/features/cart/stores/checkout/checkout.selectors';

@Injectable()
export class CheckoutEffects {
  constructor(
    private actions$: Actions,
    private checkoutService: CheckoutService,
    private ordersService: OrdersService,
    private router: Router,
    private store: Store<IAppState>
  ) {}

  /**
   * Side-effect that combines all side effects for GetCheckoutData facet
   */
  @Effect()
  requestCheckoutData$ = this.actions$.pipe(
    ofType(CheckoutActionTypes.GET_CHECKOUT_DATA),
    switchMap(() => {
      return this.checkoutService.getCheckout().pipe(
        map((data: ICheckout) => new GetCheckoutDataSuccess(data)),
        catchError(err => of(new GetCheckoutDataFailed(err)))
      );
    })
  );

  @Effect()
  placeOrder$ = this.actions$.pipe(
    ofType(CheckoutActionTypes.PLACE_ORDER),
    switchMap((action: PlaceOrder) => {
      const { cart } = action.payload;
      const cartSubmittedPatchRequest = {
        shoppingCartId: cart.id,
        currency: cart.currency,
        billToId: cart.billToId,
        validation: 'VALID',
        status: ICartStatus.SUBMITTED,
      };
      return this.ordersService.createOrder(action.payload).pipe(
        switchMap(data => [
          new PlaceOrderSuccess({ ...action.payload, orderId: data.orderId }),
          new PartialUpdateShoppingCart(cartSubmittedPatchRequest),
        ]),
        catchError(err => of(new PlaceOrderFailed(err)))
      );
    })
  );

  @Effect({ dispatch: false })
  placeOrderSuccess = this.actions$.pipe(
    ofType(CheckoutActionTypes.PLACE_ORDER_SUCCESS),
    map(action => {
      this.router.navigateByUrl('cart/checkout/confirmation');
    })
  );

  /**
   * Side-effect that combines all side effects for UpdateNCNRAgreement facet
   */
  @Effect()
  updateNCNRAgreement$ = this.actions$.pipe(
    ofType(CheckoutActionTypes.UPDATE_NCNR_AGREEMENT),
    switchMap((action: UpdateNCNRAgreement) => {
      const updatedLineItems = action.payload.items;
      return this.checkoutService.acceptNcnrItems(action.payload).pipe(
        withLatestFrom(this.store.pipe(select(getCheckoutSelector))),
        map(([anAction, checkoutData]) => {
          const clonedCheckoutData: ICheckout = cloneDeep(checkoutData);
          const currentLineItems = get(clonedCheckoutData, 'cart.lineItems', []);
          const lineItems = currentLineItems.map(item => {
            const updatedItem = find(updatedLineItems, ['id', item.id]);
            return updatedItem ? updatedItem : item;
          });
          clonedCheckoutData.cart.lineItems = lineItems;
          return new UpdateNCNRAgreementSuccess(clonedCheckoutData);
        }),
        catchError(err => of(new UpdateNCNRAgreementFailed(err)))
      );
    })
  );
}
