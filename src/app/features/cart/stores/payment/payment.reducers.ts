import { PaymentActionTypes, PaymentActions } from './payment.actions';
import { IPaymentState } from '@app/core/payment/payment.interfaces';

export const INITIAL_PAYMENT_STATE: IPaymentState = {
  error: undefined,
  loading: false,
  decision: undefined,
  requestId: undefined,
  profileId: undefined,
};

export function paymentReducers(state: IPaymentState = INITIAL_PAYMENT_STATE, action: PaymentActions): IPaymentState {
  switch (action.type) {
    case PaymentActionTypes.VALIDATE_CREDIT_CARD: {
      return {
        ...state,
        loading: true,
      };
    }
    case PaymentActionTypes.VALIDATE_CREDIT_CARD_SUCCESS: {
      const { decision, requestId, profileId } = action.payload;
      return {
        ...state,
        error: undefined,
        loading: false,
        decision,
        requestId,
        profileId,
      };
    }
    case PaymentActionTypes.VALIDATE_CREDIT_CARD_FAILED: {
      return {
        ...INITIAL_PAYMENT_STATE,
        error: action.payload,
      };
    }

    default:
      return state;
  }
}
