import { Action } from '@ngrx/store';

import { IPaymentResponse, IPaymentRequest } from '@app/core/payment/payment.interfaces';

export enum PaymentActionTypes {
  VALIDATE_CREDIT_CARD = 'VALIDATE_CREDIT_CARD',
  VALIDATE_CREDIT_CARD_SUCCESS = 'VALIDATE_CREDIT_CARD_SUCCESS',
  VALIDATE_CREDIT_CARD_FAILED = 'VALIDATE_CREDIT_CARD_FAILED',
}

/**
 * Get checkout data
 */
export class ValidateCreditCard implements Action {
  readonly type = PaymentActionTypes.VALIDATE_CREDIT_CARD;
  constructor(public payload: IPaymentRequest) {}
}

/**
 * Get checkout data sucess
 */
export class ValidateCreditCardSuccess implements Action {
  readonly type = PaymentActionTypes.VALIDATE_CREDIT_CARD_SUCCESS;
  constructor(public payload: IPaymentResponse) {}
}

/**
 * Get checkout data failed
 */
export class ValidateCreditCardFailed implements Action {
  readonly type = PaymentActionTypes.VALIDATE_CREDIT_CARD_FAILED;
  constructor(public payload: Error) {}
}

export type PaymentActions = ValidateCreditCard | ValidateCreditCardSuccess | ValidateCreditCardFailed;
