import { IPaymentResponse, IPaymentState } from '@app/core/payment/payment.interfaces';
import { mockedPaymentRequest } from '@app/core/payment/payment.service.spec';
import { ValidateCreditCard, ValidateCreditCardSuccess, ValidateCreditCardFailed } from './payment.actions';
import { INITIAL_PAYMENT_STATE, paymentReducers } from './payment.reducers';

describe('Payment Reducers', () => {
  let initialState: IPaymentState;

  beforeEach(() => {
    initialState = INITIAL_PAYMENT_STATE;
  });

  it(`ValidateCreditCard should get the initial state with loading in true`, () => {
    const action = new ValidateCreditCard(mockedPaymentRequest);
    const result = paymentReducers(initialState, action);
    expect(result.error).not.toBeDefined();
    expect(result.loading).toBeTruthy();
  });

  it(`ValidateCreditCardSuccess should set the results of the state`, () => {
    const mockedCardResponse: IPaymentResponse = {
      decision: 'ACCEPT',
      merchantReferenceCode: '11234 test',
      missingFields: [],
      profileId: '991234',
      reasonCode: '2143',
      requestId: '152342',
    };

    const action = new ValidateCreditCardSuccess(mockedCardResponse);
    const result = paymentReducers(initialState, action);
    expect(result.error).not.toBeDefined();
    expect(result.decision).toEqual(mockedCardResponse.decision);
    expect(result.loading).toBeFalsy();
  });

  it(`ValidateCreditCardFailed error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new ValidateCreditCardFailed(error);
    const result = paymentReducers(initialState, action);
    expect(result.error).toBeDefined();
  });
});
