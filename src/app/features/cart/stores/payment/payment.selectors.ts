import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IPaymentState } from '@app/core/payment/payment.interfaces';

/**
 * Selects payment state from the root state object
 */
export const getPaymentState = createFeatureSelector<IPaymentState>('payment');

/**
 * Retrieves the payment
 */
export const getPaymentSelector = createSelector(getPaymentState, paymentState => paymentState);
