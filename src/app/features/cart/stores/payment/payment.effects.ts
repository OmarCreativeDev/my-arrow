import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, catchError, switchMap } from 'rxjs/operators';

import { PaymentActionTypes, ValidateCreditCard, ValidateCreditCardSuccess, ValidateCreditCardFailed } from './payment.actions';
import { PaymentService } from '@app/core/payment/payment.service';
import { IPaymentResponse } from '@app/core/payment/payment.interfaces';

@Injectable()
export class PaymentEffects {
  constructor(private actions$: Actions, private paymentService: PaymentService) {}

  /**
   * Side-effect that combines all side effects for ValidateCreditCard
   */
  @Effect()
  validateCreditCard$ = this.actions$.pipe(
    ofType(PaymentActionTypes.VALIDATE_CREDIT_CARD),
    switchMap((action: ValidateCreditCard) => {
      return this.paymentService.validateCreditCard(action.payload).pipe(
        map((data: IPaymentResponse) => new ValidateCreditCardSuccess(data)),
        catchError(err => of(new ValidateCreditCardFailed(err)))
      );
    })
  );
}
