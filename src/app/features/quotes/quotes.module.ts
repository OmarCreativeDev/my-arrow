import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { ReactiveFormsModule } from '@angular/forms';

import { QuotesComponent } from '@app/features/quotes/pages/quotes/quotes.component';
import { QuoteCartService } from '@app/core/quote-cart/quote-cart.service';
import { QuotesRoutingModule } from '@app/features/quotes/quotes-routing.module';
import { QuoteCartEffects } from '@app/features/quotes/stores/quote-cart.effects';
import { LineItemCountComponent } from '@app/features/quotes/components/line-item-count/line-item-count.component';
import { QuoteCartTableComponent } from '@app/features/quotes/components/quote-cart-table/quote-cart-table.component';
import { QuoteCartDeleteDialogComponent } from './components/quote-cart-delete-dialog/quote-cart-delete-dialog.component';
import { QuoteCartSubmitErrorComponent } from './components/quote-cart-submit-error/quote-cart-submit-error.component';

import { SharedModule } from '@app/shared/shared.module';
import { QuoteCartSummaryComponent } from './components/quote-cart-summary/quote-cart-summary.component';
import { QuoteCartTableControlsComponent } from './components/quote-cart-table-controls/quote-cart-table-controls.component';
import { QuoteCartItemCountComponent } from './components/quote-cart-item-count/quote-cart-item-count.component';
import { QuoteCartCompleteComponent } from './pages/quote-cart-complete/quote-cart-complete.component';
import { QuoteCartGuard } from '@app/core/quote-cart/quote-cart.guard';
import { DialogService } from '@app/core/dialog/dialog.service';
import { ViewSubmittedQuotesComponent } from '@app/features/quotes/components/view-submitted-quotes/view-submitted-quotes.component';
import { QuotesService } from '@app/core/quotes/quotes.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    QuotesRoutingModule,
    RouterModule,
    EffectsModule.forFeature([QuoteCartEffects]),
  ],
  providers: [QuoteCartService, QuoteCartGuard, DialogService, QuotesService],
  declarations: [
    QuotesComponent,
    LineItemCountComponent,
    QuoteCartTableComponent,
    QuoteCartSummaryComponent,
    QuoteCartTableControlsComponent,
    QuoteCartItemCountComponent,
    QuoteCartCompleteComponent,
    QuoteCartSubmitErrorComponent,
    QuoteCartDeleteDialogComponent,
    ViewSubmittedQuotesComponent,
  ],
  entryComponents: [QuoteCartDeleteDialogComponent, QuoteCartSubmitErrorComponent],
})
export class QuotesModule {}
