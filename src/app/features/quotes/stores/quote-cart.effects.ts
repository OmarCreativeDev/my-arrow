import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of, Observable } from 'rxjs';
import { map, catchError, switchMap } from 'rxjs/operators';

import {
  QuoteCartActionTypes,
  GetQuoteCartLineItemCount,
  GetQuoteCartLineItemCountSuccess,
  GetQuoteCartLineItemCountFailed,
  GetQuoteCart,
  GetQuoteCartSuccess,
  GetQuoteCartFailed,
  GetQuoteCarts,
  GetQuoteCartsSuccess,
  GetQuoteCartsFailed,
  DeleteQuoteCartLineItems,
  DeleteQuoteCartLineItemsSuccess,
  DeleteQuoteCartLineItemsFailed,
  UpdateQuoteCartLineItems,
  UpdateQuoteCartLineItemsSuccess,
  UpdateQuoteCartLineItemsFailed,
  SubmitQuoteCart,
  SubmitQuoteCartSuccess,
  SubmitQuoteCartFailed,
  UpdateQuoteCart,
  UpdateQuoteCartSuccess,
  UpdateQuoteCartFailed,
  GetSubmittedQuotesCount,
  GetSubmittedQuotesCountSuccess,
  GetSubmittedQuotesCountFailed,
  RefreshQuoteCart,
  ValidateQuoteCartLineItems,
  ValidateQuoteCartLineItemsFailed,
  DeleteQuoteCartLineItemsRequest,
  DeleteQuoteCartLineItemsRequestFailed,
  ValidateQuoteCartLineItemsSuccess,
} from '@app/features/quotes/stores/quote-cart.actions';
import { QuoteCartService } from '@app/core/quote-cart/quote-cart.service';
import { QuotesService } from '@app/core/quotes/quotes.service';
import {
  IQuoteCarts,
  IQuoteCart,
  IQuoteCartLineItemCount,
  IQuoteCartInfo,
  IQuoteCartDeleteLineItemsResponse,
} from '@app/core/quote-cart/quote-cart.interfaces';

@Injectable()
export class QuoteCartEffects {
  constructor(
    private actions$: Actions,
    private quoteCartService: QuoteCartService,
    private router: Router,
    private quotesService: QuotesService
  ) {}

  @Effect()
  GetQuoteCarts: Observable<any> = this.actions$.pipe(
    ofType<GetQuoteCarts>(QuoteCartActionTypes.GET_QUOTE_CARTS),
    switchMap(() => {
      return this.quoteCartService.getQuoteCarts().pipe(
        map((data: IQuoteCarts) => new GetQuoteCartsSuccess(data)),
        catchError(err => of(new GetQuoteCartsFailed(err)))
      );
    })
  );

  @Effect()
  GetQuoteCartsSuccess$: Observable<any> = this.actions$.pipe(
    ofType<GetQuoteCartsSuccess>(QuoteCartActionTypes.GET_QUOTE_CARTS_SUCCESS),
    switchMap(data => {
      const firstQuoteCartId = (<IQuoteCartInfo>data.payload.quoteCarts[0]).id;
      return [new GetQuoteCartLineItemCount(firstQuoteCartId)];
    })
  );

  @Effect()
  GetQuoteCartLineItemCount$ = this.actions$.pipe(
    ofType(QuoteCartActionTypes.GET_QUOTE_CART_LINE_ITEM_COUNT),
    switchMap((action: GetQuoteCartLineItemCount) => {
      return this.quoteCartService.getQuoteCartLineItemCount(action.payload).pipe(
        map((data: IQuoteCartLineItemCount) => new GetQuoteCartLineItemCountSuccess(data)),
        catchError(err => of(new GetQuoteCartLineItemCountFailed(err)))
      );
    })
  );

  @Effect()
  GetQuoteCartLineItems$ = this.actions$.pipe(
    ofType(QuoteCartActionTypes.GET_QUOTE_CART),
    switchMap((action: GetQuoteCart) => {
      return this.quoteCartService.getQuoteCart(action.payload).pipe(
        map((data: IQuoteCart) => {
          data.lineItems = data.lineItems.map(lineItem => {
            if (lineItem.targetPrice) {
              lineItem.targetPrice = lineItem.targetPrice >= 0 ? lineItem.targetPrice : 0;
            }

            return lineItem;
          });
          return new GetQuoteCartSuccess(data);
        }),
        catchError(err => of(new GetQuoteCartFailed(err)))
      );
    })
  );

  @Effect()
  DeleteQuoteCartLineItemsRequest$ = this.actions$.pipe(
    ofType(QuoteCartActionTypes.REQUEST_QUOTE_CART_ITEMS_DELETION),
    switchMap((action: DeleteQuoteCartLineItemsRequest) => {
      const { quoteCartId, lineItemIds } = action.payload;
      return this.quoteCartService.requestQuoteCartLineItemsDeletion(action.payload).pipe(
        map(
          (deleteRequestResponse: IQuoteCartDeleteLineItemsResponse) =>
            new DeleteQuoteCartLineItems({ quoteCartId: quoteCartId, deleteRequestId: deleteRequestResponse.id, lineItemIds: lineItemIds })
        ),
        catchError(err => of(new DeleteQuoteCartLineItemsRequestFailed(err)))
      );
    })
  );

  /**
   * This effect will do the actual Deletion.
   */
  @Effect()
  DeleteQuoteCartLineItems$ = this.actions$.pipe(
    ofType(QuoteCartActionTypes.DELETE_QUOTE_CART_LINE_ITEMS),
    switchMap((action: DeleteQuoteCartLineItems) => {
      return this.quoteCartService.deleteQuoteCartLineItems(action.payload).pipe(
        switchMap(() => [
          new DeleteQuoteCartLineItemsSuccess(action.payload.lineItemIds),
          new GetQuoteCartLineItemCount(action.payload.quoteCartId),
        ]),
        catchError(err => of(new DeleteQuoteCartLineItemsFailed(err)))
      );
    })
  );

  @Effect()
  UpdateQuoteCartLineItems$ = this.actions$.pipe(
    ofType(QuoteCartActionTypes.UPDATE_QUOTE_CART_LINE_ITEMS),
    switchMap((action: UpdateQuoteCartLineItems) => {
      return this.quoteCartService.updateQuoteCartLineItems(action.payload).pipe(
        map(() => new UpdateQuoteCartLineItemsSuccess()),
        map(() => new GetQuoteCart(action.payload.id)),
        catchError(err => of(new UpdateQuoteCartLineItemsFailed(err)))
      );
    })
  );

  @Effect()
  SubmitQuoteCart$: Observable<any> = this.actions$.pipe(
    ofType<SubmitQuoteCart>(QuoteCartActionTypes.SUBMIT_QUOTE_CART),
    switchMap((action: SubmitQuoteCart) => {
      return this.quoteCartService.submitQuoteCart(action.payload).pipe(
        map(response => {
          this.router.navigateByUrl('quotes/quote-cart-complete');

          return new SubmitQuoteCartSuccess(response);
        }),
        catchError(err => {
          this.router.navigateByUrl('quotes');
          return of(new SubmitQuoteCartFailed(err));
        })
      );
    })
  );

  @Effect()
  UpdateQuoteCart$ = this.actions$.pipe(
    ofType(QuoteCartActionTypes.UPDATE_QUOTE_CART),
    switchMap((action: UpdateQuoteCart) => {
      return this.quoteCartService.updateQuoteCart(action.payload).pipe(
        map(() => new UpdateQuoteCartSuccess()),
        catchError(err => of(new UpdateQuoteCartFailed(err)))
      );
    })
  );

  @Effect()
  getSubmittedQuotesCount$ = this.actions$.pipe(
    ofType(QuoteCartActionTypes.GET_SUBMITTED_QUOTES_COUNT),
    switchMap((action: GetSubmittedQuotesCount) => {
      return this.quotesService.getSubmittedQuotesCount().pipe(
        map((data: any) => new GetSubmittedQuotesCountSuccess(data)),
        catchError(err => of(new GetSubmittedQuotesCountFailed(err)))
      );
    })
  );

  @Effect()
  RefreshQuoteCart$: Observable<any> = this.actions$.pipe(
    ofType<RefreshQuoteCart>(QuoteCartActionTypes.REFRESH_QUOTE_CART),
    switchMap(() => {
      return this.quoteCartService.getQuoteCarts().pipe(
        map((data: IQuoteCarts) => new GetQuoteCartsSuccess(data)),
        catchError(err => of(new GetQuoteCartsFailed(err)))
      );
    })
  );

  @Effect()
  ValidateQuoteCartLineItems$: Observable<any> = this.actions$.pipe(
    ofType<ValidateQuoteCartLineItems>(QuoteCartActionTypes.VALIDATE_QUOTE_CART_LINE_ITEMS),
    switchMap((action: ValidateQuoteCartLineItems) => {
      return this.quoteCartService.validateQuoteCartLineItems(action.payload).pipe(
        map(() => new ValidateQuoteCartLineItemsSuccess()),
        catchError(err => of(new ValidateQuoteCartLineItemsFailed(err)))
      );
    })
  );
}
