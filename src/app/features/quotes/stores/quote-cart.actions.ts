import { Action } from '@ngrx/store';
import {
  IQuoteCarts,
  IQuoteCart,
  IQuoteCartLineItemCount,
  IFieldMap,
  ICheckQuoteCartLineItem,
  IQuoteCartLineItem,
  IQuoteCartLineItemsUpdateRequest,
  IQuoteCartLineItemsDeleteRequest,
  IQuoteCartSubmitRequest,
  IQuoteCartSubmitResponse,
  IQuoteCartUpdateRequest,
  ISubmittedQuotesCountResponse,
  IQuoteCartValidateLineItemsRequest,
  IQuoteCartCompleteLineItemsDeletion,
} from '@app/core/quote-cart/quote-cart.interfaces';
import { HttpErrorResponse } from '@angular/common/http';

export enum QuoteCartActionTypes {
  ADD_WS_QUOTE_CART_ITEM = '[QC] ADD_WS_QUOTE_CART_ITEM',
  CLEAR_ADDITIONAL_INFO_STORE = '[QC] CLEAR_ADDITIONAL_INFO_STORE',
  DELETE_QUOTE_CART_LINE_ITEMS = '[QC] DELETE_QUOTE_CART_LINE_ITEMS',
  DELETE_QUOTE_CART_LINE_ITEMS_FAILED = '[QC] DELETE_QUOTE_CART_LINE_ITEMS_FAILED',
  DELETE_QUOTE_CART_LINE_ITEMS_SUCCESS = '[QC] DELETE_QUOTE_CART_LINE_ITEMS_SUCCESS',
  GET_QUOTE_CART = '[QC] GET_QUOTE_CART',
  GET_QUOTE_CART_FAILED = '[QC] GET_QUOTE_CART_FAILED',
  GET_QUOTE_CART_LINE_ITEM_COUNT = '[QC] GET_QUOTE_CART_LINE_ITEM_COUNT',
  GET_QUOTE_CART_LINE_ITEM_COUNT_FAILED = '[QC] GET_QUOTE_CART_LINE_ITEM_COUNT_FAILED',
  GET_QUOTE_CART_LINE_ITEM_COUNT_SUCCESS = '[QC] GET_QUOTE_CART_LINE_ITEM_COUNT_SUCCESS',
  GET_QUOTE_CART_SUCCESS = '[QC] GET_QUOTE_CART_SUCCESS',
  GET_QUOTE_CARTS = '[QC] GET_QUOTE_CARTS',
  GET_QUOTE_CARTS_FAILED = '[QC] GET_QUOTE_CARTS_FAILED',
  GET_QUOTE_CARTS_SUCCESS = '[QC] GET_QUOTE_CARTS_SUCCESS',
  GET_SUBMITTED_QUOTES_COUNT = '[QC] GET_SUBMITTED_QUOTES_COUNT',
  GET_SUBMITTED_QUOTES_COUNT_FAILED = '[QC] GET_SUBMITTED_QUOTES_COUNT_FAILED',
  GET_SUBMITTED_QUOTES_COUNT_SUCCESS = '[QC] GET_SUBMITTED_QUOTES_COUNT_SUCCESS',
  ITEMS_DELETION_REQUEST_FAILED = '[QC] ITEMS_DELETION_REQUEST_FAILED',
  QUOTE_CART_LIMIT = '[QC] QUOTE_CART_LIMIT',
  RECEIVE_WS_QUOTE_CART_FAILED = '[QC] RECEIVE_WS_QUOTE_CART_FAILED',
  REFRESH_QUOTE_CART = '[QC] REFRESH_QUOTE_CART',
  REQUEST_QUOTE_CART_ITEMS_DELETION = '[QC] REQUEST_QUOTE_CART_ITEMS_DELETION',
  REQUEST_QUOTE_CART_ITEMS_DELETION_FAILED = '[QC] REQUEST_QUOTE_CART_ITEMS_DELETION_FAILED',
  REQUEST_QUOTE = '[QC] REQUEST_QUOTE',
  SUBMIT_QUOTE_CART = '[QC] SUBMIT_QUOTE_CART',
  SUBMIT_QUOTE_CART_FAILED = '[QC] SUBMIT_QUOTE_CART_FAILED',
  SUBMIT_QUOTE_CART_SUCCESS = '[QC] SUBMIT_QUOTE_CART_SUCCESS',
  TOGGLE_ALL_QUOTE_CART_LINE_ITEMS = '[QC] TOGGLE_ALL_QUOTE_CART_LINE_ITEMS',
  TOGGLE_CHECK_QUOTE_CART_LINE_ITEMS = '[QC] TOGGLE_CHECK_QUOTE_CART_LINE_ITEMS',
  UPDATE_ADDITIONAL_INFO_STORE = '[QC] UPDATE_ADDITIONAL_INFO_STORE',
  UPDATE_ITEM_FIELD_STORE = '[QC] UPDATE_ITEM_FIELD_STORE',
  UPDATE_QUOTE_CART = '[QC] UPDATE_QUOTE_CART',
  UPDATE_QUOTE_CART_FAILED = '[QC] UPDATE_QUOTE_CART_FAILED',
  UPDATE_QUOTE_CART_LINE_ITEMS = '[QC] UPDATE_QUOTE_CART_LINE_ITEMS',
  UPDATE_QUOTE_CART_LINE_ITEMS_FAILED = '[QC] UPDATE_QUOTE_CART_LINE_ITEMS_FAILED',
  UPDATE_QUOTE_CART_LINE_ITEMS_SUCCESS = '[QC] UPDATE_QUOTE_CART_LINE_ITEMS_SUCCESS',
  UPDATE_QUOTE_CART_SUCCESS = '[QC] UPDATE_QUOTE_CART_SUCCESS',
  UPDATE_QUOTE_CART_WITH_VALID_FLAG = '[QC] UPDATE_QUOTE_CART_WITH_VALID_FLAG',
  UPDATE_WS_QUOTE_CART = '[QC] UPDATE_WS_QUOTE_CART',
  VALIDATE_QUOTE_CART_LINE_ITEMS = '[QC] VALIDATE_QUOTE_CART_LINE_ITEMS',
  VALIDATE_QUOTE_CART_LINE_ITEMS_FAILED = '[QC] VALIDATE_QUOTE_CART_LINE_ITEMS_FAILED',
  VALIDATE_QUOTE_CART_LINE_ITEMS_SUCCESS = '[QC] VALIDATE_QUOTE_CART_LINE_ITEMS_SUCCESS',
}

/**
 * Get Quote Carts Info
 */
export class GetQuoteCarts implements Action {
  readonly type = QuoteCartActionTypes.GET_QUOTE_CARTS;
  constructor() {}
}

/**
 * * Get Quote Carts Success
 */
export class GetQuoteCartsSuccess implements Action {
  readonly type = QuoteCartActionTypes.GET_QUOTE_CARTS_SUCCESS;
  constructor(public payload: IQuoteCarts) {}
}

/**
 * * Get Quote Carts Failed
 */
export class GetQuoteCartsFailed implements Action {
  readonly type = QuoteCartActionTypes.GET_QUOTE_CARTS_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Get Quote CartLine Item Count
 */
export class GetQuoteCartLineItemCount implements Action {
  readonly type = QuoteCartActionTypes.GET_QUOTE_CART_LINE_ITEM_COUNT;
  constructor(public payload: string) {}
}

/**
 * * Get Quote Cart Line Item Count success
 */
export class GetQuoteCartLineItemCountSuccess implements Action {
  readonly type = QuoteCartActionTypes.GET_QUOTE_CART_LINE_ITEM_COUNT_SUCCESS;
  constructor(public payload: IQuoteCartLineItemCount) {}
}

/**
 * * Get Quote Cart Line Item Count failed
 */
export class GetQuoteCartLineItemCountFailed implements Action {
  readonly type = QuoteCartActionTypes.GET_QUOTE_CART_LINE_ITEM_COUNT_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Get Quote CartLine Items
 */
export class GetQuoteCart implements Action {
  readonly type = QuoteCartActionTypes.GET_QUOTE_CART;
  constructor(public payload: string) {}
}

/**
 * * Get Quote Cart Line Items success
 */
export class GetQuoteCartSuccess implements Action {
  readonly type = QuoteCartActionTypes.GET_QUOTE_CART_SUCCESS;
  constructor(public payload: IQuoteCart) {}
}

/**
 * * Get Quote Cart Line Items failed
 */
export class GetQuoteCartFailed implements Action {
  readonly type = QuoteCartActionTypes.GET_QUOTE_CART_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Update Line Item Field in the Store
 */
export class UpdateItemFieldInStore implements Action {
  readonly type = QuoteCartActionTypes.UPDATE_ITEM_FIELD_STORE;
  constructor(public payload: IFieldMap) {}
}

/**
 * Triggers the set check of the line items
 */
export class ToggleCheckQuoteCartLineItems implements Action {
  readonly type = QuoteCartActionTypes.TOGGLE_CHECK_QUOTE_CART_LINE_ITEMS;
  constructor(public payload: ICheckQuoteCartLineItem) {}
}

/**
 * Triggers the set check of All the line items
 */
export class ToggleAllQuoteCartLineItems implements Action {
  readonly type = QuoteCartActionTypes.TOGGLE_ALL_QUOTE_CART_LINE_ITEMS;
  constructor(public payload: Array<IQuoteCartLineItem>) {}
}

/**
 * Triggers the update action of all the line items changed
 */
export class UpdateQuoteCartLineItems implements Action {
  readonly type = QuoteCartActionTypes.UPDATE_QUOTE_CART_LINE_ITEMS;
  constructor(public payload: IQuoteCartLineItemsUpdateRequest) {}
}

/**
 * Triggers the update action of all the line items changed success
 */
export class UpdateQuoteCartLineItemsSuccess implements Action {
  readonly type = QuoteCartActionTypes.UPDATE_QUOTE_CART_LINE_ITEMS_SUCCESS;
}

/**
 * Triggers the update action of all the line items changed failed
 */
export class UpdateQuoteCartLineItemsFailed implements Action {
  readonly type = QuoteCartActionTypes.UPDATE_QUOTE_CART_LINE_ITEMS_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Update Additional Info in the Store
 */
export class UpdateAdditionalInfoInStore implements Action {
  readonly type = QuoteCartActionTypes.UPDATE_ADDITIONAL_INFO_STORE;
  constructor(public payload: string) {}
}

/**
 * Request QuoteCart LineItems Deletion
 */
export class DeleteQuoteCartLineItemsRequest implements Action {
  readonly type = QuoteCartActionTypes.REQUEST_QUOTE_CART_ITEMS_DELETION;
  constructor(public payload: IQuoteCartLineItemsDeleteRequest) {}
}

/**
 * QuoteCart items deletion request failed
 */
export class DeleteQuoteCartLineItemsRequestFailed implements Action {
  readonly type = QuoteCartActionTypes.REQUEST_QUOTE_CART_ITEMS_DELETION_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Delete QuoteCart LineItems
 */
export class DeleteQuoteCartLineItems implements Action {
  readonly type = QuoteCartActionTypes.DELETE_QUOTE_CART_LINE_ITEMS;
  constructor(public payload: IQuoteCartCompleteLineItemsDeletion) {}
}

/**
 * * Delete QuoteCart LineItems success
 */
export class DeleteQuoteCartLineItemsSuccess implements Action {
  readonly type = QuoteCartActionTypes.DELETE_QUOTE_CART_LINE_ITEMS_SUCCESS;
  constructor(public payload: Array<string>) {}
}

/**
 * * Delete QuoteCart LineItems failed
 */
export class DeleteQuoteCartLineItemsFailed implements Action {
  readonly type = QuoteCartActionTypes.DELETE_QUOTE_CART_LINE_ITEMS_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Update QuoteCart valid flag when a change is detected
 */
export class UpdateQuoteCartWithValidFlag implements Action {
  readonly type = QuoteCartActionTypes.UPDATE_QUOTE_CART_WITH_VALID_FLAG;
  constructor(public payload: boolean) {}
}

/**
 * Submit Quote Cart
 */
export class SubmitQuoteCart implements Action {
  readonly type = QuoteCartActionTypes.SUBMIT_QUOTE_CART;
  constructor(public payload: IQuoteCartSubmitRequest) {}
}

/**
 * * Submit Quote Cart success
 */
export class SubmitQuoteCartSuccess implements Action {
  readonly type = QuoteCartActionTypes.SUBMIT_QUOTE_CART_SUCCESS;
  constructor(public payload: IQuoteCartSubmitResponse) {}
}

/**
 * * Submit Quote Cart failed
 */
export class SubmitQuoteCartFailed implements Action {
  readonly type = QuoteCartActionTypes.SUBMIT_QUOTE_CART_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Add Quote Cart Line Item
 */
export class AddWSQuoteCartItem implements Action {
  readonly type = QuoteCartActionTypes.ADD_WS_QUOTE_CART_ITEM;
  constructor(public payload: IQuoteCartLineItem) {}
}

/**
 * * Refresh Quote Cart Line Item
 */
export class UpdateWSQuoteCart implements Action {
  readonly type = QuoteCartActionTypes.UPDATE_WS_QUOTE_CART;
  constructor(public payload: { index: number; lineItem: IQuoteCartLineItem }) {}
}

/**
 * * Get Web Socket Response for lineItem failed
 */
export class ReceiveWSQuoteCartFailed implements Action {
  readonly type = QuoteCartActionTypes.RECEIVE_WS_QUOTE_CART_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Clear additional info from Store
 */
export class ClearAdditionalInfoInStore implements Action {
  readonly type = QuoteCartActionTypes.CLEAR_ADDITIONAL_INFO_STORE;
  constructor() {}
}

/**
 * Updates Quote Cart
 */
export class UpdateQuoteCart implements Action {
  readonly type = QuoteCartActionTypes.UPDATE_QUOTE_CART;
  constructor(public payload: IQuoteCartUpdateRequest) {}
}

/**
 * Submit Quote Cart Sucess
 */
export class UpdateQuoteCartSuccess implements Action {
  readonly type = QuoteCartActionTypes.UPDATE_QUOTE_CART_SUCCESS;
}

/**
 * Submit Quote Failed
 */
export class UpdateQuoteCartFailed implements Action {
  readonly type = QuoteCartActionTypes.UPDATE_QUOTE_CART_FAILED;
  constructor(public payload: Error) {}
}

export class GetSubmittedQuotesCount implements Action {
  readonly type = QuoteCartActionTypes.GET_SUBMITTED_QUOTES_COUNT;
}

export class GetSubmittedQuotesCountSuccess implements Action {
  readonly type = QuoteCartActionTypes.GET_SUBMITTED_QUOTES_COUNT_SUCCESS;
  constructor(public payload: ISubmittedQuotesCountResponse) {}
}

export class GetSubmittedQuotesCountFailed implements Action {
  readonly type = QuoteCartActionTypes.GET_SUBMITTED_QUOTES_COUNT_FAILED;
  constructor(public payload: HttpErrorResponse) {}
}

export class QuoteCartLimit implements Action {
  readonly type = QuoteCartActionTypes.QUOTE_CART_LIMIT;
  constructor(public payload: number) {}
}

export class RefreshQuoteCart implements Action {
  readonly type = QuoteCartActionTypes.REFRESH_QUOTE_CART;
  constructor() {}
}

export class RequestQuote implements Action {
  readonly type = QuoteCartActionTypes.REQUEST_QUOTE;
  constructor(public payload: string) {}
}

export class ValidateQuoteCartLineItems implements Action {
  readonly type = QuoteCartActionTypes.VALIDATE_QUOTE_CART_LINE_ITEMS;
  constructor(public payload: IQuoteCartValidateLineItemsRequest) {}
}

export class ValidateQuoteCartLineItemsSuccess implements Action {
  readonly type = QuoteCartActionTypes.VALIDATE_QUOTE_CART_LINE_ITEMS_SUCCESS;
  constructor() {}
}

export class ValidateQuoteCartLineItemsFailed implements Action {
  readonly type = QuoteCartActionTypes.VALIDATE_QUOTE_CART_LINE_ITEMS_FAILED;
  constructor(public payload: HttpErrorResponse) {}
}

export type QuoteCartActions =
  | GetQuoteCarts
  | GetQuoteCartsSuccess
  | GetQuoteCartsFailed
  | GetQuoteCart
  | GetQuoteCartSuccess
  | GetQuoteCartFailed
  | GetQuoteCartLineItemCount
  | GetQuoteCartLineItemCountSuccess
  | GetQuoteCartLineItemCountFailed
  | UpdateItemFieldInStore
  | ToggleCheckQuoteCartLineItems
  | ToggleAllQuoteCartLineItems
  | DeleteQuoteCartLineItemsRequest
  | DeleteQuoteCartLineItemsRequestFailed
  | DeleteQuoteCartLineItems
  | DeleteQuoteCartLineItemsSuccess
  | DeleteQuoteCartLineItemsFailed
  | UpdateAdditionalInfoInStore
  | UpdateQuoteCartLineItems
  | UpdateQuoteCartLineItemsSuccess
  | UpdateQuoteCartLineItemsFailed
  | UpdateQuoteCartWithValidFlag
  | SubmitQuoteCart
  | SubmitQuoteCartSuccess
  | SubmitQuoteCartFailed
  | AddWSQuoteCartItem
  | UpdateWSQuoteCart
  | ReceiveWSQuoteCartFailed
  | ClearAdditionalInfoInStore
  | UpdateQuoteCart
  | UpdateQuoteCartSuccess
  | UpdateQuoteCartFailed
  | GetSubmittedQuotesCount
  | GetSubmittedQuotesCountSuccess
  | GetSubmittedQuotesCountFailed
  | QuoteCartLimit
  | RefreshQuoteCart
  | RequestQuote
  | ValidateQuoteCartLineItems
  | ValidateQuoteCartLineItemsSuccess
  | ValidateQuoteCartLineItemsFailed;
