import { get, cloneDeep } from 'lodash-es';
import { QuoteCartActions, QuoteCartActionTypes } from './quote-cart.actions';
import { IQuoteCartState, IQuoteCartLineItem } from '@app/core/quote-cart/quote-cart.interfaces';
import { QuoteCartErrorTypes } from '@app/core/quote-cart/quote-cart.enum';

export const INITIAL_QUOTE_CART_STATE: IQuoteCartState = {
  lineItemCount: 0,
  maxLineItems: 0,
  remainingLineItems: 0,
  submittedQuotesCount: 0,
  quoteCart: {
    id: '',
    total: undefined,
    status: undefined,
    lineItems: [],
    metadata: {},
    additionalInfo: undefined,
    valid: false,
    quoteNumber: undefined,
    quoteHeader: undefined,
    submittedDate: undefined,
  },
  carts: {
    userId: '',
    quoteCarts: [],
  },
  loading: false,
  error: undefined,
  errorType: undefined,
};

/**
 * Mutates the state for given set of LineItemsCountActions
 * @param state
 * @param action
 */
export function quoteCartReducers(state: IQuoteCartState = INITIAL_QUOTE_CART_STATE, action: QuoteCartActions) {
  switch (action.type) {
    case QuoteCartActionTypes.GET_QUOTE_CARTS: {
      return {
        ...state,
        loading: true,
        error: undefined,
        errorType: undefined,
        carts: {},
      };
    }
    case QuoteCartActionTypes.GET_QUOTE_CARTS_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: undefined,
        errorType: undefined,
        carts: action.payload,
      };
    }
    case QuoteCartActionTypes.GET_QUOTE_CARTS_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload,
        carts: {},
      };
    }
    case QuoteCartActionTypes.GET_QUOTE_CART_LINE_ITEM_COUNT: {
      return {
        ...state,
        loading: true,
        error: undefined,
        errorType: undefined,
      };
    }
    case QuoteCartActionTypes.GET_QUOTE_CART_LINE_ITEM_COUNT_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: undefined,
        errorType: undefined,
        lineItemCount: action.payload.lineItemCount,
        maxLineItems: action.payload.maxLineItems,
        remainingLineItems: action.payload.remainingLineItems,
      };
    }
    case QuoteCartActionTypes.GET_QUOTE_CART_LINE_ITEM_COUNT_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    }
    case QuoteCartActionTypes.GET_QUOTE_CART: {
      return {
        ...state,
        loading: true,
        error: undefined,
        errorType: undefined,
      };
    }

    case QuoteCartActionTypes.GET_QUOTE_CART_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: undefined,
        errorType: undefined,
        quoteCart: {
          ...action.payload,
          additionalInfo: state.quoteCart.additionalInfo,
        },
      };
    }

    case QuoteCartActionTypes.GET_QUOTE_CART_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload,
        quoteCart: {},
      };
    }

    case QuoteCartActionTypes.UPDATE_QUOTE_CART_LINE_ITEMS: {
      return {
        ...state,
        loading: true,
        error: undefined,
        errorType: undefined,
      };
    }
    case QuoteCartActionTypes.UPDATE_QUOTE_CART_LINE_ITEMS_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: undefined,
        errorType: undefined,
      };
    }
    case QuoteCartActionTypes.UPDATE_QUOTE_CART_LINE_ITEMS_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload,
        errorType: undefined,
      };
    }
    case QuoteCartActionTypes.UPDATE_ITEM_FIELD_STORE: {
      const { fieldName, value, index } = action.payload;
      const cloneLineItems = cloneDeep(state.quoteCart.lineItems);
      const lineItem = get(cloneLineItems, `[${index}]`, {});
      lineItem[fieldName] = value;
      lineItem.changed = true;

      return {
        ...state,
        quoteCart: {
          ...state.quoteCart,
          lineItems: [...cloneLineItems.slice(0, index), lineItem, ...cloneLineItems.slice(index + 1)],
        },
      };
    }
    case QuoteCartActionTypes.TOGGLE_CHECK_QUOTE_CART_LINE_ITEMS: {
      const { index, checked } = action.payload;
      const lineItem = cloneDeep(state.quoteCart.lineItems[index]);

      return {
        ...state,
        quoteCart: {
          ...state.quoteCart,
          lineItems: [
            ...state.quoteCart.lineItems.slice(0, index),
            {
              ...lineItem,
              checked,
            },
            ...state.quoteCart.lineItems.slice(index + 1),
          ],
        },
      };
    }
    case QuoteCartActionTypes.TOGGLE_ALL_QUOTE_CART_LINE_ITEMS: {
      return {
        ...state,
        quoteCart: {
          ...state.quoteCart,
          lineItems: action.payload,
        },
      };
    }
    case QuoteCartActionTypes.UPDATE_ADDITIONAL_INFO_STORE: {
      return {
        ...state,
        quoteCart: {
          ...state.quoteCart,
          additionalInfo: action.payload,
        },
      };
    }

    case QuoteCartActionTypes.DELETE_QUOTE_CART_LINE_ITEMS: {
      return {
        ...state,
        error: undefined,
        errorType: undefined,
        loading: true,
      };
    }

    case QuoteCartActionTypes.REQUEST_QUOTE_CART_ITEMS_DELETION: {
      return {
        ...state,
        error: undefined,
        loading: true,
      };
    }

    case QuoteCartActionTypes.REQUEST_QUOTE_CART_ITEMS_DELETION_FAILED: {
      return {
        ...state,
        error: action.payload,
        loading: false,
        errorType: undefined,
      };
    }

    case QuoteCartActionTypes.DELETE_QUOTE_CART_LINE_ITEMS: {
      return {
        ...state,
        error: undefined,
        errorType: undefined,
        loading: true,
      };
    }

    case QuoteCartActionTypes.DELETE_QUOTE_CART_LINE_ITEMS_SUCCESS: {
      const lineItems = <IQuoteCartLineItem[]>cloneDeep(state.quoteCart.lineItems);
      const indexesToRemove = action.payload;
      const newLineItems = lineItems.filter(e => !indexesToRemove.includes(e.id));

      return {
        ...state,
        quoteCart: {
          ...state.quoteCart,
          lineItems: newLineItems,
        },
        error: undefined,
        errorType: undefined,
        loading: false,
      };
    }

    case QuoteCartActionTypes.DELETE_QUOTE_CART_LINE_ITEMS_FAILED: {
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    }

    case QuoteCartActionTypes.UPDATE_QUOTE_CART_WITH_VALID_FLAG: {
      return {
        ...state,
        quoteCart: {
          ...state.quoteCart,
          valid: action.payload,
        },
        error: undefined,
        errorType: undefined,
        loading: false,
      };
    }
    case QuoteCartActionTypes.SUBMIT_QUOTE_CART: {
      return {
        ...state,
        loading: true,
        error: undefined,
        errorType: undefined,
      };
    }
    case QuoteCartActionTypes.SUBMIT_QUOTE_CART_SUCCESS: {
      const { quoteHeader, quoteNumber, status, submittedDate } = action.payload;

      return {
        ...state,
        quoteCart: {
          ...state.quoteCart,
          quoteHeader,
          quoteNumber,
          status,
          submittedDate,
        },
        error: undefined,
        errorType: undefined,
        loading: false,
        lineItemCount: 0,
      };
    }
    case QuoteCartActionTypes.SUBMIT_QUOTE_CART_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload,
        errorType: QuoteCartErrorTypes.SUBMIT_ERROR,
      };
    }
    case QuoteCartActionTypes.ADD_WS_QUOTE_CART_ITEM: {
      return {
        ...state,
        quoteCart: {
          ...state.quoteCart,
          lineItems: [...state.quoteCart.lineItems, action.payload],
        },
      };
    }
    case QuoteCartActionTypes.UPDATE_WS_QUOTE_CART: {
      const { index, lineItem } = action.payload;

      return {
        ...state,

        quoteCart: {
          ...state.quoteCart,
          lineItems: [...state.quoteCart.lineItems.slice(0, index), lineItem, ...state.quoteCart.lineItems.slice(index + 1)],
        },
      };
    }
    case QuoteCartActionTypes.RECEIVE_WS_QUOTE_CART_FAILED: {
      return {
        ...state,
        error: action.payload,
      };
    }
    case QuoteCartActionTypes.CLEAR_ADDITIONAL_INFO_STORE: {
      return {
        ...state,
        quoteCart: {
          ...state.quoteCart,
          additionalInfo: undefined,
        },
      };
    }
    case QuoteCartActionTypes.UPDATE_QUOTE_CART: {
      return {
        ...state,
        loading: true,
        error: undefined,
        errorType: undefined,
      };
    }
    case QuoteCartActionTypes.UPDATE_QUOTE_CART_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: undefined,
        errorType: undefined,
      };
    }
    case QuoteCartActionTypes.UPDATE_QUOTE_CART_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    }
    case QuoteCartActionTypes.GET_SUBMITTED_QUOTES_COUNT: {
      return {
        ...state,
      };
    }
    case QuoteCartActionTypes.GET_SUBMITTED_QUOTES_COUNT_SUCCESS: {
      return {
        ...state,
        submittedQuotesCount: action.payload.count,
      };
    }
    case QuoteCartActionTypes.GET_SUBMITTED_QUOTES_COUNT_FAILED: {
      return {
        ...state,
        error: action.payload,
      };
    }
    case QuoteCartActionTypes.VALIDATE_QUOTE_CART_LINE_ITEMS_SUCCESS: {
      return {
        ...state,
        error: undefined,
      };
    }
    case QuoteCartActionTypes.VALIDATE_QUOTE_CART_LINE_ITEMS_FAILED: {
      return {
        ...state,
        error: action.payload,
      };
    }
    case QuoteCartActionTypes.QUOTE_CART_LIMIT:
    case QuoteCartActionTypes.REFRESH_QUOTE_CART:
    case QuoteCartActionTypes.REQUEST_QUOTE:
    case QuoteCartActionTypes.VALIDATE_QUOTE_CART_LINE_ITEMS:
    default: {
      return state;
    }
  }
}
