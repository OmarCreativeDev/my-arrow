import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IQuoteCartState } from '@app/core/quote-cart/quote-cart.interfaces';

/**
 * Selects quote cart state from the root state object
 */
export const getQuoteCartState = createFeatureSelector<IQuoteCartState>('quoteCart');

export const getQuoteCartLoadingSelector = createSelector(getQuoteCartState, quoteCartState => quoteCartState.loading);

export const getCartSelector = createSelector(getQuoteCartState, quoteCartState => quoteCartState.quoteCart);

export const getLineItemCountSelector = createSelector(getQuoteCartState, quoteCartState => quoteCartState.lineItemCount);

/**
 * Get in progress quote cart id
 */
export const getQuoteCartId = createSelector(getQuoteCartState, quoteCartState => {
  const IN_PROGRESS = 'IN_PROGRESS';
  if (quoteCartState.carts.quoteCarts && quoteCartState.carts.quoteCarts.length) {
    const cartInProgress = quoteCartState.carts.quoteCarts.filter(cart => cart.status === IN_PROGRESS);

    return cartInProgress.length ? cartInProgress[0].id : '';
  }
  return '';
});

/**
 * Selects quote cart error type from the root state object
 */
export const getQuoteCartErrorType = createSelector(getQuoteCartState, quoteCartState => quoteCartState.errorType);

/**
 * Retrieve the number of submitted quotes
 */
export const getSubmittedQuotesCount = createSelector(getQuoteCartState, quoteCartState => quoteCartState.submittedQuotesCount);

/**
 * Retrieve the number of max line items
 */
export const getQuoteCartMaxLineItems = createSelector(getQuoteCartState, quoteCartState => quoteCartState.maxLineItems);

/**
 * Retrieve the number of remaining line items
 */
export const getQuoteCartRemainingLineItems = createSelector(getQuoteCartState, quoteCartState => quoteCartState.remainingLineItems);
