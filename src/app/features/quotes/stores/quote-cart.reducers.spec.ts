import cloneDeep from 'lodash-es/cloneDeep';
import {
  GetQuoteCarts,
  GetQuoteCartsSuccess,
  GetQuoteCartsFailed,
  GetQuoteCart,
  GetQuoteCartSuccess,
  GetQuoteCartFailed,
  GetQuoteCartLineItemCount,
  GetQuoteCartLineItemCountSuccess,
  GetQuoteCartLineItemCountFailed,
  UpdateItemFieldInStore,
  UpdateAdditionalInfoInStore,
  DeleteQuoteCartLineItems,
  DeleteQuoteCartLineItemsSuccess,
  DeleteQuoteCartLineItemsFailed,
  UpdateQuoteCartLineItems,
  UpdateQuoteCartLineItemsFailed,
  UpdateQuoteCartLineItemsSuccess,
  SubmitQuoteCartSuccess,
  SubmitQuoteCartFailed,
  AddWSQuoteCartItem,
  UpdateWSQuoteCart,
  ReceiveWSQuoteCartFailed,
  ClearAdditionalInfoInStore,
  UpdateQuoteCart,
  UpdateQuoteCartSuccess,
  UpdateQuoteCartFailed,
  QuoteCartLimit,
  RefreshQuoteCart,
  ValidateQuoteCartLineItems,
  ValidateQuoteCartLineItemsSuccess,
  ValidateQuoteCartLineItemsFailed,
  DeleteQuoteCartLineItemsRequest,
  DeleteQuoteCartLineItemsRequestFailed,
} from './quote-cart.actions';

import { quoteCartReducers, INITIAL_QUOTE_CART_STATE } from './quote-cart.reducers';
import { ToggleAllQuoteCartLineItems, SubmitQuoteCart } from '@app/features/quotes/stores/quote-cart.actions';
import {
  IQuoteCartState,
  IQuoteCartLineItem,
  IQuoteCart,
  IQuoteCartLineItemCount,
  IQuoteCarts,
  IQuoteCartLineItemsUpdateRequest,
  IQuoteCartLineItemsDeleteRequest,
  IQuoteCartSubmitResponse,
  IQuoteCartSubmitRequest,
  IQuoteCartUpdateRequest,
  IQuoteCartValidateLineItemsRequest,
  IQuoteCartCompleteLineItemsDeletion,
} from '@app/core/quote-cart/quote-cart.interfaces';

import mockedQuoteCartLineItems from '@app/features/quotes/pages/quotes/quotes-model-data-mock';
import { HttpErrorResponse } from '@angular/common/http';
import { QuoteCartLineItemStatus } from '@app/core/quote-cart/quote-cart.enum';

describe('Quote Cart Reducer', () => {
  let initialState: IQuoteCartState;
  const quoteCart = {
    id: '',
    total: undefined,
    status: undefined,
    additionalInfo: undefined,
    lineItems: [
      {
        itemId: 3417532,
        manufacturerPartNumber: 'XYZ-123',
        selectedCustomerPartNumber: 'jodwh334h45403fu32',
        selectedEndCustomerSiteId: 560366,
        requestDate: '2018-05-04',
        quantity: 36,
        id: '5aea15afd34431ed742490c2',
        total: 500,
        manufacturer: 'Google',
        priceTiers: [],
        description: 'Labore incididunt laborum consequat culpa sunt non laboris ut commodo.',
        price: 8557,
        checked: false,
        changed: false,
        tariffApplicable: true,
      },
    ],
    metadata: {},
  };
  const quoteCardId = '12h31k23h2kj3h213';
  const lineItem = {
    itemId: 1900287,
    warehouseId: 464840,
    manufacturerPartNumber: 'ASD-123',
    selectedCustomerPartNumber: 'SVR-115-0000-009',
    selectedEndCustomerSiteId: 560366,
    requestDate: '2018-04-24',
    quantity: 3,
    id: '5adf32b0359fdf8b31786608',
    total: 4.47,
    priceTiers: [
      {
        quantityFrom: 1,
        quantityTo: 999,
        pricePerItem: 1.49,
      },
      {
        quantityFrom: 1000,
        quantityTo: null,
        pricePerItem: 1.39,
      },
    ],
    manufacturer: 'Microsoft',
    description: 'Aliqua Lorem in sint cillum sint laboris.',
    image: '/product-image.png',
    inStock: true,
    quotable: true,
    minimumOrderQuantity: 217129,
    multipleOrderQuantity: 603604,
    availableQuantity: 495668,
    bufferQuantity: 482329,
    leadTime: '21 Weeks',
    datasheet: 'http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf',
    endCustomerRecords: [
      {
        cpn: 'SVR-115-0000-002',
        endCustomers: [
          {
            name: 'Siemens Industry Inc',
            endCustomerSiteId: 911729,
          },
          {
            name: 'Google',
            endCustomerSiteId: 138223,
          },
        ],
      },
      {
        cpn: 'SVR-115-0000-009',
        endCustomers: [
          {
            name: 'Apple',
            endCustomerSiteId: 559863,
          },
          {
            name: 'Samsung',
            endCustomerSiteId: 560366,
          },
        ],
      },
    ],
    price: 6833.0,
    multiple: 75,
    ncnr: true,
  };

  beforeEach(() => {
    initialState = INITIAL_QUOTE_CART_STATE;
  });

  it('`GET_QUOTE_CARTS` should set `loading` to true and not have an error property', () => {
    const action = new GetQuoteCarts();
    const result = quoteCartReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeUndefined();
  });

  it('`GET_QUOTE_CARTS_SUCCESS` should set `quoteCarts`', () => {
    const quoteCartsResponseMock: IQuoteCarts = {
      userId: 'usr-001',
      quoteCarts: [
        {
          id: '123',
          userId: 'usr-001',
          status: '',
        },
      ],
    };

    const action = new GetQuoteCartsSuccess(quoteCartsResponseMock);
    const result = <IQuoteCartState>quoteCartReducers(initialState, action);

    expect(result.carts).toBeDefined();
    expect(result.carts.quoteCarts).toBeDefined();
  });

  it(`GET_QUOTE_CARTS_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new GetQuoteCartsFailed(error);
    const result = quoteCartReducers(initialState, action);
    expect(result.error).toBeDefined();
  });

  it('`GET_QUOTE_CART_LINE_ITEM_COUNT` should set `loading` to true and not have an error property', () => {
    const action = new GetQuoteCartLineItemCount(quoteCardId);
    const result = quoteCartReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeUndefined();
  });

  it('`GET_QUOTE_CART_LINE_ITEM_COUNT_SUCCESS` should set `quoteCarts`', () => {
    const quoteCartsResponseMock: IQuoteCartLineItemCount = { lineItemCount: 100, maxLineItems: 100, remainingLineItems: 0 };

    const action = new GetQuoteCartLineItemCountSuccess(quoteCartsResponseMock);
    const result = <IQuoteCartState>quoteCartReducers(initialState, action);

    expect(result.lineItemCount).toBeDefined();
    expect(result.lineItemCount).toBe(100);
  });

  it(`GET_QUOTE_CART_LINE_ITEM_COUNT_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new GetQuoteCartLineItemCountFailed(error);
    const result = quoteCartReducers(initialState, action);
    expect(result.error).toBeDefined();
  });

  it('`GET_QUOTE_CART` should set `loading` to true and not have an error property', () => {
    const action = new GetQuoteCart(quoteCardId);
    const result = quoteCartReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeUndefined();
  });

  it('`GET_QUOTE_CART_SUCCESS` should set `quoteCarts`', () => {
    const quoteCartsResponseMock: IQuoteCart = {
      id: '23g14h32g4jh32g432',
      total: undefined,
      status: undefined,
      lineItems: [],
      metadata: {},
      additionalInfo: undefined,
    };

    const action = new GetQuoteCartSuccess(quoteCartsResponseMock);
    const result = <IQuoteCartState>quoteCartReducers(initialState, action);

    expect(result.quoteCart).toBeDefined();
    expect(result.quoteCart.id).toBeDefined();
  });

  it(`GET_QUOTE_CART_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new GetQuoteCartFailed(error);
    const result = quoteCartReducers(initialState, action);
    expect(result.error).toBeDefined();
  });

  it(`UPDATE_ITEM_FIELD_STORE should update the Line Item field`, () => {
    const action = new UpdateItemFieldInStore({ fieldName: 'selectedCustomerPartNumber', value: '2012R-10', index: 0 });

    const cloneInitialState = cloneDeep(initialState);
    cloneInitialState.quoteCart.lineItems = [lineItem];

    const cloneLineItem = cloneDeep(lineItem);
    cloneLineItem.selectedCustomerPartNumber = '2012R-10';
    cloneLineItem.changed = true;

    const result = <IQuoteCartState>quoteCartReducers(cloneInitialState, action);
    expect(result.quoteCart.lineItems[0]).toEqual(cloneLineItem);
  });

  it(`TOGGLE_ALL_QUOTE_CART_LINE_ITEMS should have the correct payload`, () => {
    const action = new ToggleAllQuoteCartLineItems(quoteCart.lineItems);
    const result = <IQuoteCartState>quoteCartReducers(initialState, action);

    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.quoteCart.lineItems).toEqual(quoteCart.lineItems);
  });

  it(`UPDATE_ADDITIONAL_INFO_STORE should update the Quote Cart Additional Info`, () => {
    const additionalInfoValue = 'Some additional Info';
    const action = new UpdateAdditionalInfoInStore(additionalInfoValue);
    const cloneInitialState = cloneDeep(initialState);

    const result = <IQuoteCartState>quoteCartReducers(cloneInitialState, action);
    expect(result.quoteCart.additionalInfo).toEqual(additionalInfoValue);
  });

  describe('Delete QuoteCartLineItems Action must do the quote cart line items deletion', () => {
    it(`REQUEST_QUOTE_CART_ITEMS_DELETION should return loading in true, and set no errors`, () => {
      const payload: IQuoteCartLineItemsDeleteRequest = {
        quoteCartId: '1234565768790',
        lineItemIds: ['5a8701382c1724bf4003132c', '5a8701382c1724br4003132c', '5a8701382c1724be4003132c'],
      };

      const action = new DeleteQuoteCartLineItemsRequest(payload);
      const result = quoteCartReducers(initialState, action);

      expect(result.loading).toBeTruthy();
      expect(result.error).toEqual(undefined);
    });

    it(`REQUEST_QUOTE_CART_ITEMS_DELETION_FAIL should return the state unaltered and set an error`, () => {
      const error = new Error('foo');
      const mockedData = mockedQuoteCartLineItems;
      const clonedInitialState = cloneDeep(initialState);
      clonedInitialState.quoteCart.lineItems = mockedData;
      const action = new DeleteQuoteCartLineItemsRequestFailed(error);
      const result = quoteCartReducers(clonedInitialState, action);
      expect(result.error).toBeDefined();
    });

    it(`DELETE_QUOTE_CART_LINE_ITEMS should return loading in true, and set no errors`, () => {
      const payload: IQuoteCartCompleteLineItemsDeletion = {
        deleteRequestId: '5a8701382c1724bf4003132c',
        lineItemIds: ['5a8701382c1724bf4003132c', '5a8701382c1724br4003132c', '5a8701382c1724be4003132c'],
        quoteCartId: '1234565768790',
      };
      const action = new DeleteQuoteCartLineItems(payload);
      const result = quoteCartReducers(initialState, action);
      expect(result.loading).toBeTruthy();
      expect(result.error).toEqual(undefined);
    });

    it(`DELETE_QUOTE_CART_LINE_ITEMS_SUCCESS should delete an item and not have an error`, () => {
      const clonedInitialState = cloneDeep(initialState);
      clonedInitialState.quoteCart.lineItems = mockedQuoteCartLineItems;

      const action = new DeleteQuoteCartLineItemsSuccess([
        '5a8701382c1724bf4003132c',
        '5a8701382c1724br4003132c',
        '5a8701382c1724be4003132c',
      ]);
      const result = quoteCartReducers(clonedInitialState, action);
      const finalState = cloneDeep(clonedInitialState);

      finalState.quoteCart.lineItems = clonedInitialState.quoteCart.lineItems.slice(3);
      expect(result.quoteCart).toEqual(finalState.quoteCart);
      expect(result.error).toEqual(undefined);
    });

    it(`DELETE_QUOTE_CART_LINE_ITEMS_FAILED should return the state unaltered and set an error`, () => {
      const error = new Error('foo');
      const mockedData = mockedQuoteCartLineItems;
      const clonedInitialState = cloneDeep(initialState);
      clonedInitialState.quoteCart.lineItems = mockedData;
      const action = new DeleteQuoteCartLineItemsFailed(error);
      const result = quoteCartReducers(clonedInitialState, action);
      expect(result.error).toBeDefined();
    });
  });

  it('`UPDATE_QUOTE_CART_LINE_ITEMS` should set `loading` to true and not have an error property', () => {
    const lineItemsUpdateRequest: IQuoteCartLineItemsUpdateRequest = {
      id: quoteCardId,
      lineItems: quoteCart.lineItems,
    };

    const action = new UpdateQuoteCartLineItems(lineItemsUpdateRequest);
    const result = <IQuoteCartState>quoteCartReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeUndefined();
  });

  it('`UPDATE_QUOTE_CART_LINE_ITEMS_SUCCESS` should set `quoteCarts`', () => {
    const action = new UpdateQuoteCartLineItemsSuccess();
    const result = <IQuoteCartState>quoteCartReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
  });

  it(`UPDATE_QUOTE_CART_LINE_ITEMS_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new UpdateQuoteCartLineItemsFailed(error);
    const result = <IQuoteCartState>quoteCartReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeDefined();
  });

  it('`SUBMIT_QUOTE_CART` should set `loading` to true and not have an error property', () => {
    const quoteCartRequestMock: IQuoteCartSubmitRequest = {
      accountId: 1305827,
      accountNumber: 1067767,
      additionalInfo: undefined,
      cartId: '5a8701382c1724bf4003132c',
      contact: {
        customerServiceEmail: 'ggustus@arrow.com',
        customerServiceNumber: '+1 877 237 8621',
        salesRepName: "O'Brien-Meek, Patrice",
        salesRepPhoneNumber: '+1 303-824-6459',
        salesRepEmail: 'patty.obrienmeek@arrow.com',
      },
      currencyCode: 'USD',
      email: 'david.lane@oncorems.com',
      firstName: 'David',
      lastName: 'Lane',
      orgId: 241,
      phoneNumber: undefined,
      billToId: 2174374,
      shipToId: 2175315,
      region: 'arrowna',
    };
    const action = new SubmitQuoteCart(quoteCartRequestMock);
    const result = quoteCartReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeUndefined();
  });

  it('`SUBMIT_QUOTE_CART_SUCCESS` should set `quoteHeader`, `quoteNumber` and `submittedDate`; should update `status` and set `loading` to false and not have an error property', () => {
    const quoteCartsSubmitResponseMock: IQuoteCartSubmitResponse = {
      id: '5a8701382c1724bf4003132c',
      quoteNumber: '8b64aa73-c9db-4e59-ad09-e8396943677e',
      quoteHeader: '0b61b61e-aa01-48eb-a7fd-be2588fbfe3d',
      status: 'SUBMITTED',
      submittedDate: '2018-05-30',
      userId: '17xvi0kpizcnds4s7mtn3g01',
    };

    const action = new SubmitQuoteCartSuccess(quoteCartsSubmitResponseMock);
    const result = <IQuoteCartState>quoteCartReducers(initialState, action);

    expect(result.quoteCart.quoteHeader).toBeDefined();
    expect(result.quoteCart.quoteHeader).toEqual(quoteCartsSubmitResponseMock.quoteHeader);
    expect(result.quoteCart.quoteNumber).toBeDefined();
    expect(result.quoteCart.quoteNumber).toEqual(quoteCartsSubmitResponseMock.quoteNumber);
    expect(result.quoteCart.submittedDate).toBeDefined();
    expect(result.quoteCart.submittedDate).toEqual(quoteCartsSubmitResponseMock.submittedDate);
    expect(result.quoteCart.status).toEqual(quoteCartsSubmitResponseMock.status);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
  });

  it('`SUBMIT_QUOTE_CART_FAILED` error property should have an error property and should set `loading` to false', () => {
    const error = new Error('foo');
    const action = new SubmitQuoteCartFailed(error);
    const result = quoteCartReducers(initialState, action);
    expect(result.error).toBeDefined();
    expect(result.loading).toBeFalsy();
  });

  describe('Web Socket reducers and actions associated to them', () => {
    const quoteCartLineItemResponseMock: IQuoteCartLineItem = {
      itemId: 1234,
      manufacturerPartNumber: '',
      selectedCustomerPartNumber: '',
      selectedEndCustomerSiteId: 1234,
      requestDate: '',
      quantity: 1,
      id: '123',
      total: 1,
      priceTiers: [
        {
          quantityFrom: 1,
          quantityTo: 1,
          pricePerItem: 1,
        },
      ],
      manufacturer: '',
      description: '',
      price: 1,
      checked: true,
      changed: true,
      targetPrice: 1,
      validation: QuoteCartLineItemStatus.VALID,
      ncnrAccepted: true,
      ncnrAcceptedBy: true,
      image: '',
      inStock: true,
      quotable: true,
      minimumOrderQuantity: 1,
      multipleOrderQuantity: 1,
      availableQuantity: 1,
      bufferQuantity: 1,
      leadTime: '',
      datasheet: '',
      multiple: 1,
      ncnr: true,
      tariffApplicable: true,
    };

    it('`ADD_WS_QUOTE_CART_ITEM` should add a new lineItem at the end of the lineItems array', () => {
      const action = new AddWSQuoteCartItem(quoteCartLineItemResponseMock);
      const result = <IQuoteCartState>quoteCartReducers(initialState, action);
      expect(result.quoteCart.lineItems).toBeDefined();
      expect(result.quoteCart.lineItems).toContain(quoteCartLineItemResponseMock);
    });

    it('`UPDATE_WS_QUOTE_CART` should update a lineItem in the lineItems array', () => {
      const action = new UpdateWSQuoteCart({ index: 0, lineItem: quoteCartLineItemResponseMock });
      const result = <IQuoteCartState>quoteCartReducers(initialState, action);
      expect(result.quoteCart.lineItems).toBeDefined();
      expect(result.quoteCart.lineItems).toContain(quoteCartLineItemResponseMock);
    });

    it('`RECEIVE_WS_QUOTE_CART_FAILED` should update a lineItem in the lineItems array', () => {
      const error = new Error('foo');
      const action = new ReceiveWSQuoteCartFailed(error);
      const result = <IQuoteCartState>quoteCartReducers(initialState, action);
      expect(result.error).toBeDefined();
    });

    it(`CLEAR_ADDITIONAL_INFO_STORE should clear the Quote CArt additional info field`, () => {
      const action = new ClearAdditionalInfoInStore();
      const cloneInitialState = cloneDeep(initialState);

      cloneInitialState.quoteCart.additionalInfo = 'Some text goes here';

      const result = <IQuoteCartState>quoteCartReducers(cloneInitialState, action);

      expect(result.quoteCart.additionalInfo).toBeUndefined();
    });
    it('`UPDATE_QUOTE_CART` should set `loading` to true and not have an error property', () => {
      const lineItemsUpdateRequest: IQuoteCartUpdateRequest = {
        cartId: '5adf32b0359fdf8b31786608',
        billToId: 12345,
        currency: 'USD',
      };

      const action = new UpdateQuoteCart(lineItemsUpdateRequest);
      const result = <IQuoteCartState>quoteCartReducers(initialState, action);
      expect(result.loading).toBeTruthy();
      expect(result.error).toBeUndefined();
    });

    it('`UPDATE_QUOTE_CART_SUCCESS` should set `loading` to false and not have an error property', () => {
      const action = new UpdateQuoteCartSuccess();
      const result = <IQuoteCartState>quoteCartReducers(initialState, action);
      expect(result.loading).toBeFalsy();
      expect(result.error).toBeUndefined();
    });

    it(`UPDATE_QUOTE_CART_FAILED should have an error property`, () => {
      const error = new Error('update error');
      const action = new UpdateQuoteCartFailed(error);
      const result = <IQuoteCartState>quoteCartReducers(initialState, action);
      expect(result.loading).toBeFalsy();
      expect(result.error).toBeDefined();
    });
  });

  it('`QUOTE_CART_LIMIT` should not modify the store state', () => {
    const action = new QuoteCartLimit(101);
    const result = quoteCartReducers(initialState, action);
    expect(result).toEqual(initialState);
  });

  it('`REFRESH_QUOTE_CART` should not modify the store state', () => {
    const action = new RefreshQuoteCart();
    const result = quoteCartReducers(initialState, action);
    expect(result).toEqual(initialState);
  });

  it('`VALIDATE_QUOTE_CART_LINE_ITEMS` should not modify the store state', () => {
    const request: IQuoteCartValidateLineItemsRequest = {
      lineItemsIds: quoteCart.lineItems.slice().map(_lineItem => _lineItem.id),
      quoteCartId: quoteCardId,
    };
    const action = new ValidateQuoteCartLineItems(request);
    const result = quoteCartReducers(initialState, action);
    expect(result).toEqual(initialState);
  });

  it('`VALIDATE_QUOTE_CART_LINE_ITEMS_SUCCESS` should set `loading` to false and not have an error property', () => {
    const action = new ValidateQuoteCartLineItemsSuccess();
    const result = <IQuoteCartState>quoteCartReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
  });

  it(`VALIDATE_QUOTE_CART_LINE_ITEMS_FAILED should have an error property`, () => {
    const error = new HttpErrorResponse({ error: 'line items validation error' });
    const action = new ValidateQuoteCartLineItemsFailed(error);
    const result = <IQuoteCartState>quoteCartReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeDefined();
  });
});
