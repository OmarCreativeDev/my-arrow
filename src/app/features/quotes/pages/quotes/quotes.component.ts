import { DialogService } from '@app/core/dialog/dialog.service';
import { Client, Message, StompSubscription } from '@stomp/stompjs';
import { Component, OnDestroy, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { isEmpty, findIndex, some, size } from 'lodash-es';

import { IQuoteCart, IQuoteCartLineItem, IQuoteCartValidateLineItemsRequest } from '@app/core/quote-cart/quote-cart.interfaces';
import { IAppState } from '@app/shared/shared.interfaces';
import {
  getCartSelector,
  getLineItemCountSelector,
  getQuoteCartErrorType,
  getQuoteCartId,
  getQuoteCartLoadingSelector,
} from '@app/features/quotes/stores/quote-cart.selectors';

import {
  AddWSQuoteCartItem,
  DeleteQuoteCartLineItemsRequest,
  GetQuoteCart,
  UpdateItemFieldInStore,
  UpdateQuoteCartLineItems,
  UpdateQuoteCartWithValidFlag,
  UpdateWSQuoteCart,
  ValidateQuoteCartLineItems,
} from '@app/features/quotes/stores/quote-cart.actions';

import { QuoteCartSubmitErrorComponent } from '@app/features/quotes/components/quote-cart-submit-error/quote-cart-submit-error.component';
import { getCurrencyCode } from '@app/core/user/store/user.selectors';
import { chain } from 'lodash';
import { QuoteCartErrorTypes, QuoteCartLineItemStatus } from '@app/core/quote-cart/quote-cart.enum';
import { WSSService } from '@app/core/ws/wss.service';
import { WSSState } from '@app/core/ws/wss.interface';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: 'app-quotes',
  templateUrl: './quotes.component.html',
  styleUrls: ['./quotes.component.scss'],
})
export class QuotesComponent implements OnInit, OnDestroy {
  public lineItemCount$: Observable<number>;
  public lineItemCount: number;

  public cart$: Observable<IQuoteCart>;
  public cart: IQuoteCart;

  public loading$: Observable<boolean>;
  public DATE_FORMAT: string = 'YYYY-MM-DD';

  public wsLineItems: Client;
  public quoteCartWSTopic: StompSubscription;
  public wasCartValidated: boolean = false;

  public errorType$: Observable<QuoteCartErrorTypes>;
  public currencyCode$: Observable<string>;
  public currencyCode: string;

  public showTariffHeadline: boolean = false;
  public privateFeatures$: Observable<object>;
  public privateFeatureFlags: object;
  public quoteCartId$: Observable<string>;
  private subscription: Subscription = new Subscription();

  constructor(private quoteCartStore: Store<IAppState>, private wssService: WSSService, private dialogService: DialogService) {
    this.lineItemCount$ = quoteCartStore.pipe(select(getLineItemCountSelector));
    this.cart$ = quoteCartStore.pipe(select(getCartSelector));
    this.loading$ = quoteCartStore.pipe(select(getQuoteCartLoadingSelector));
    this.errorType$ = quoteCartStore.pipe(select(getQuoteCartErrorType));
    this.currencyCode$ = quoteCartStore.pipe(select(getCurrencyCode));
    this.privateFeatures$ = quoteCartStore.pipe(select(getPrivateFeatureFlagsSelector));
    this.quoteCartId$ = this.quoteCartStore.pipe(select(getQuoteCartId));
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  ngOnDestroy() {
    this.unsubscribeToWSTopic();
    this.subscription.unsubscribe();
  }

  public setCurrencyCode(currencyCode: string): void {
    this.currencyCode = currencyCode;
  }

  public setFeatureFlags(featureFlags: object): void {
    this.privateFeatureFlags = featureFlags;
  }

  public setLineItemCount(lineItemCount: number): void {
    this.lineItemCount = lineItemCount;
  }

  private startSubscriptions() {
    this.subscription.add(this.lineItemCountSubscription());
    this.subscription.add(this.quoteCartSubscription());
    this.subscription.add(this.errorTypeSubscription());
    this.subscription.add(this.currencyCodeSubscription());
    this.subscription.add(this.featuresSubscription());
    this.subscription.add(this.quoteCartIdSubscription());
    this.subscription.add(this.wsClientStateSubscription());
  }

  private currencyCodeSubscription(): Subscription {
    return this.currencyCode$.subscribe(currencyCode => this.setCurrencyCode(currencyCode));
  }

  private errorTypeSubscription(): Subscription {
    return this.errorType$.subscribe(errorType => this.handleError(errorType));
  }

  private featuresSubscription(): Subscription {
    return this.privateFeatures$.subscribe(featureFlags => this.setFeatureFlags(featureFlags));
  }

  private lineItemCountSubscription(): Subscription {
    return this.lineItemCount$.subscribe(lineItemCount => this.setLineItemCount(lineItemCount));
  }

  private quoteCartSubscription(): Subscription {
    return this.cart$.subscribe(quoteCart => this.handleQuoteCartSubscription(quoteCart));
  }

  private handleQuoteCartSubscription(quoteCart: IQuoteCart): void {
    this.cartAssignmentAndCheckControl(quoteCart);
    this.subscribeToWSTopic(quoteCart.id);
    this.validateLineItems(quoteCart.id);
    this.checkTariff();
  }

  private handleQuoteCartIdSubscription(quoteCartId: string): void {
    if (quoteCartId) this.quoteCartStore.dispatch(new GetQuoteCart(quoteCartId));
  }

  private quoteCartIdSubscription(): Subscription {
    return this.quoteCartId$.subscribe(quoteCartId => this.handleQuoteCartIdSubscription(quoteCartId));
  }

  public checkTariff(): void {
    this.showTariffHeadline = some(this.cart.lineItems, lineItem => {
      return lineItem.tariffApplicable && lineItem.tariffValue;
    });
  }

  public cartAssignmentAndCheckControl(quoteCart: IQuoteCart): void {
    const cartLineItemsCheckControl = isEmpty(quoteCart)
      ? []
      : quoteCart.lineItems.map(item => ({
          changed: false,
          checked: false,
          description: undefined,
          endCustomerRecords: undefined,
          manufacturer: undefined,
          manufacturerPartNumber: undefined,
          price: undefined,
          validation: undefined,
          ...item,
        }));

    this.cart = quoteCart;
    this.cart.lineItems = cartLineItemsCheckControl;
  }

  public deleteQuoteCartLineItems() {
    const checkedlineItemIds = this.getCheckedIds();
    const { id } = this.cart;

    if (size(checkedlineItemIds) > 0) {
      this.quoteCartStore.dispatch(new DeleteQuoteCartLineItemsRequest({ quoteCartId: id, lineItemIds: checkedlineItemIds }));
    }
  }

  public areSomeQuoteCartLineItemsChecked(): boolean {
    return some(this.cart.lineItems, ['checked', true]);
  }

  public areSomeQuoteCartLineItemsChangedAndValid(): boolean {
    return this.isQuoteCartValid() ? some(this.cart.lineItems, ['changed', true]) : false;
  }

  public changeLineItemsDate(date: string): void {
    const dateString = moment(date).format(this.DATE_FORMAT);
    this.cart.lineItems
      .filter(lineItem => lineItem.checked === true)
      .map(lineItem => {
        const index: number = findIndex(this.cart.lineItems, { id: lineItem.id });
        this.updateRequestDateInStore('requestDate', dateString, index);
      });
  }

  public updateRequestDateInStore(fieldName: string, value: any, index: number): void {
    if (this.cart.lineItems[index][fieldName] !== value) {
      this.isQuoteCartValid()
        ? this.quoteCartStore.dispatch(new UpdateQuoteCartWithValidFlag(true))
        : this.quoteCartStore.dispatch(new UpdateQuoteCartWithValidFlag(false));

      this.quoteCartStore.dispatch(new UpdateItemFieldInStore({ fieldName, value, index }));
    }
  }

  public getCheckedIds(): Array<string> {
    return this.cart.lineItems.filter(lineItem => lineItem.checked).map(lineItemChecked => lineItemChecked.id);
  }

  public updateQuoteCartLineItems(): void {
    const { id } = this.cart;
    const lineItems: Array<IQuoteCartLineItem> = this.cart.lineItems.filter(lineItem => lineItem.changed);
    this.quoteCartStore.dispatch(new UpdateQuoteCartLineItems({ id, lineItems }));
  }

  public isQuoteCartValid(): boolean {
    return this.cart.valid === undefined || this.cart.valid;
  }

  public getPendingLineItemsIds(): Array<string> {
    return chain(this.cart.lineItems)
      .filter(lineItem => lineItem.validation === QuoteCartLineItemStatus.PENDING)
      .map(lineItem => lineItem.id)
      .value();
  }

  public handleConnectObservableSubscription(quoteCartId: string): void {
    const lineItemsIds = this.getPendingLineItemsIds();
    const request: IQuoteCartValidateLineItemsRequest = {
      lineItemsIds,
      quoteCartId,
    };

    if (lineItemsIds.length > 0 && !this.wasCartValidated) {
      this.quoteCartStore.dispatch(new ValidateQuoteCartLineItems(request));
      this.wasCartValidated = true;
    }
  }

  public validateLineItems(quoteCartId: string): void {
    if (this.wssService.isClientConnected()) {
      this.handleConnectObservableSubscription(quoteCartId);
    }
  }

  public wsClientStateSubscription() {
    return this.wssService.state$.subscribe((state: WSSState) => this.handleWSClientState(state));
  }

  public handleWSClientState(state: WSSState) {
    switch (state) {
      case WSSState.CONNECTED:
        this.onWSClientConnects(this.cart.id);
        break;
      case WSSState.DISCONNECTED:
        this.onWSClientDisconnects();
        break;
    }
  }

  public onWSClientConnects(quoteCartId: string): void {
    this.subscribeToWSTopic(quoteCartId);
    this.handleQuoteCartIdSubscription(quoteCartId);
    this.validateLineItems(quoteCartId);
  }

  public onWSClientDisconnects(): void {
    this.quoteCartWSTopic = undefined;
  }

  public subscribeToWSTopic(quoteCartId: string) {
    if (this.wssService.isClientConnected() && quoteCartId && !this.quoteCartWSTopic) {
      this.wsLineItems = this.wssService.client;
      this.quoteCartWSTopic = this.wssService.client.subscribe(`/quoteCart/queue/${quoteCartId}/lineItems`, (message: Message) => {
        const quoteCartLineItem: IQuoteCartLineItem = JSON.parse(message.body);
        this.handleQuoteCartItemInStore(quoteCartLineItem);
      });
    }
  }

  public unsubscribeToWSTopic() {
    if (this.wssService.isClientConnected() && this.quoteCartWSTopic) {
      this.quoteCartWSTopic.unsubscribe();
    }
  }

  public handleQuoteCartItemInStore(lineItem: IQuoteCartLineItem): void {
    const lineItemIndex = this.getIndexQuoteCartLineItem(lineItem.id);
    if (lineItemIndex === -1) {
      this.quoteCartStore.dispatch(new AddWSQuoteCartItem(lineItem));
    } else {
      this.quoteCartStore.dispatch(
        new UpdateWSQuoteCart({
          lineItem: this.prepareWSLineItemUpdate(lineItem, lineItemIndex),
          index: lineItemIndex,
        })
      );
    }
  }

  public getIndexQuoteCartLineItem(lineItemPayloadId: string): number {
    return this.cart.lineItems.findIndex(lineItem => lineItemPayloadId === lineItem.id);
  }

  public checkQuoteCartLineItemsValidationStatus(): QuoteCartLineItemStatus {
    if (some(this.cart.lineItems, ['validation', QuoteCartLineItemStatus.PENDING])) {
      return QuoteCartLineItemStatus.PENDING;
    } else if (some(this.cart.lineItems, ['validation', QuoteCartLineItemStatus.INVALID])) {
      return QuoteCartLineItemStatus.INVALID;
    }

    return QuoteCartLineItemStatus.VALID;
  }

  public handleError(errorType: QuoteCartErrorTypes): void {
    if (errorType === QuoteCartErrorTypes.SUBMIT_ERROR) {
      this.displaySubmitErrorModal();
    }
  }

  public displaySubmitErrorModal(): void {
    this.dialogService.open(QuoteCartSubmitErrorComponent, { size: 'large' });
  }

  public prepareWSLineItemUpdate(lineItem: IQuoteCartLineItem, index: number): IQuoteCartLineItem {
    const { changed, checked, enteredCustomerPartNumber, targetPrice } = this.cart.lineItems[index];
    const updatedLineItem = { ...lineItem, changed, checked, targetPrice };

    if (enteredCustomerPartNumber) {
      updatedLineItem.enteredCustomerPartNumber = enteredCustomerPartNumber;
    }

    return updatedLineItem;
  }
}
