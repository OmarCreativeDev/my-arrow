import { of } from 'rxjs';
import { cloneDeep } from 'lodash-es';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Store, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { FormBuilder } from '@angular/forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { SharedModule } from '@app/shared/shared.module';
import { QuotesComponent } from './quotes.component';
import { LineItemCountComponent } from '@app/features/quotes/components/line-item-count/line-item-count.component';
import { QuoteCartTableComponent } from '@app/features/quotes/components/quote-cart-table/quote-cart-table.component';
import { QuoteCartSummaryComponent } from '@app/features/quotes/components/quote-cart-summary/quote-cart-summary.component';
import { quoteCartReducers } from '@app/features/quotes/stores/quote-cart.reducers';
import { ValidateQuoteCartLineItems, DeleteQuoteCartLineItemsRequest } from '@app/features/quotes/stores/quote-cart.actions';
import { QuoteCartTableControlsComponent } from '@app/features/quotes/components/quote-cart-table-controls/quote-cart-table-controls.component';
import { DateService } from '@app/shared/services/date.service';
import quotesModelData from './quotes-model-data-mock';
import { QuoteCartItemCountComponent } from '@app/features/quotes/components/quote-cart-item-count/quote-cart-item-count.component';
import { UpdateQuoteCartLineItems } from '@app/features/quotes/stores/quote-cart.actions';
import { IQuoteCartLineItemsUpdateRequest, IQuoteCartValidateLineItemsRequest } from '@app/core/quote-cart/quote-cart.interfaces';
import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { OverlayContainer } from '@app/core/dialog/overlay-container';
import { DialogService } from '@app/core/dialog/dialog.service';
import { QuoteCartSubmitErrorComponent } from '@app/features/quotes/components/quote-cart-submit-error/quote-cart-submit-error.component';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { userReducers } from '@app/core/user/store/user.reducers';
import { ViewSubmittedQuotesComponent } from '@app/features/quotes/components/view-submitted-quotes/view-submitted-quotes.component';
import { propertiesReducers } from '@app/features/properties/store/properties.reducers';
import { QuoteCartLineItemStatus, QuoteCartErrorTypes } from '@app/core/quote-cart/quote-cart.enum';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';
import { PropertiesModule } from '@app/features/properties/properties.module';
import { mockPrivateProperties } from '@app/core/features/feature-flags.mock';
import { PropertiesEffects } from '@app/features/properties/store/properties.effects';
import { PropertiesService } from '@app/core/properties/properties.service';
import { OnlyPositiveNumbersDirective } from '@app/shared/directives/only-positive-numbers.directive';

describe('QuotesComponent', () => {
  let component: QuotesComponent;
  let _component: QuotesComponent;
  let fixture: ComponentFixture<QuotesComponent>;
  let store: Store<any>;
  let dialogService: DialogService;
  let wssService: WSSService;

  const lineItemsUpdateRequest: IQuoteCartLineItemsUpdateRequest = {
    id: '12h31k23h2kj3h213',
    lineItems: [
      {
        itemId: 3417532,
        manufacturerPartNumber: 'XYZ-123',
        selectedCustomerPartNumber: 'jodwh334h45403fu32',
        selectedEndCustomerSiteId: 560366,
        requestDate: '2018-05-04',
        quantity: 36,
        id: '5aea15afd34431ed742490c2',
        total: 500,
        manufacturer: 'Google',
        priceTiers: [
          {
            quantityFrom: 1,
            quantityTo: 7317,
            pricePerItem: 0.03727,
          },
        ],
        description: 'Labore incididunt laborum consequat culpa sunt non laboris ut commodo.',
        price: 8557,
        checked: false,
        changed: true,
        tariffApplicable: true,
      },
    ],
  };

  const mockPropertiesReducers = () => ({
    ...propertiesReducers,
    private: mockPrivateProperties,
  });

  class MockPropertiesService {
    public getProperties() {
      return of(mockPropertiesReducers);
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        HttpClientTestingModule,
        SharedModule,
        ReactiveFormsModule,
        PropertiesModule,
        StoreModule.forRoot({
          quoteCart: quoteCartReducers,
          user: userReducers,
          properties: mockPropertiesReducers,
        }),
        EffectsModule.forRoot([PropertiesEffects]),
      ],
      declarations: [
        QuotesComponent,
        LineItemCountComponent,
        QuoteCartTableComponent,
        QuoteCartSummaryComponent,
        QuoteCartTableControlsComponent,
        QuoteCartItemCountComponent,
        QuoteCartSubmitErrorComponent,
        ViewSubmittedQuotesComponent,
      ],
      providers: [
        FormBuilder,
        DateService,
        AuthTokenService,
        { provide: PropertiesService, useClass: MockPropertiesService },
        { provide: DialogService, useClass: DialogServiceMock },
        { provide: WSSService, useClass: MockStompWSSService },
        OverlayContainer,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuotesComponent);
    store = TestBed.get(Store);
    component = fixture.componentInstance;
    dialogService = TestBed.get(DialogService);
    wssService = TestBed.get(WSSService);
    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  afterEach(() => {
    component.ngOnDestroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('check if on quote cart are selected items', () => {
    it('onAnyCheckOnQuoteCart should return false if there is not a selected item', () => {
      const result = component.areSomeQuoteCartLineItemsChecked();
      expect(result).toBe(false);
    });

    it('onAnyCheckOnQuoteCart should return true if there is a selected item', () => {
      component.cart.lineItems = quotesModelData.map(item => ({
        ...item,
        checked: true,
      }));

      const result = component.areSomeQuoteCartLineItemsChecked();
      expect(result).toBe(true);
    });
  });

  describe('Delete button functionality', () => {
    it('delete action to have been called after clicking the delete button', () => {
      component.cart.lineItems = [
        {
          itemId: 3417532,
          manufacturerPartNumber: 'XYZ-123',
          selectedCustomerPartNumber: 'jodwh334h45403fu32',
          selectedEndCustomerSiteId: 560366,
          requestDate: '2018-05-04',
          quantity: 36,
          id: '5aea15afd34431ed742490c2',
          total: 500,
          manufacturer: 'Google',
          priceTiers: [],
          description: 'Labore incididunt laborum consequat culpa sunt non laboris ut commodo.',
          price: 8557,
          checked: true,
          changed: false,
          tariffApplicable: true,
        },
      ];
      const checkedlineItemIds = ['5aea15afd34431ed742490c2'];
      component.cart.id = '5a8701382c1724bf4003132c';

      component.deleteQuoteCartLineItems();

      const action = new DeleteQuoteCartLineItemsRequest({
        quoteCartId: component.cart.id,
        lineItemIds: checkedlineItemIds,
      });
      component.cart.id = '';

      expect(store.dispatch).toHaveBeenCalledWith(action);
    });
  });

  describe('check if quote cart is valid', () => {
    beforeAll(() => {
      _component = cloneDeep(component);
    });

    it('isQuoteCartValid should only return false if quote cart is invalid', () => {
      _component.cart.valid = false;

      const isValid = _component.isQuoteCartValid();

      expect(isValid).toBe(false);
    });

    it('isQuoteCartValid should return true if quote cart is valid', () => {
      _component.cart.valid = true;

      const isValid = _component.isQuoteCartValid();

      expect(isValid).toBe(true);
    });

    it('isQuoteCartValid should return true if cart.valid is undefined', () => {
      _component.cart.valid = undefined;

      const isValid = _component.isQuoteCartValid();

      expect(isValid).toBe(true);
    });
  });

  describe('check if connection to Web Socket was created on ngOnInit', () => {
    it('ngOnInit should call subscribeToWSTopic', () => {
      spyOn<any>(component, 'subscribeToWSTopic').and.returnValue(of([]));
      spyOn<any>(component, 'cartAssignmentAndCheckControl').and.callFake(() => {});
      component.ngOnInit();
      expect(component.subscribeToWSTopic).toHaveBeenCalled();
    });
  });

  describe('check if connection to Web Socket was destroyed on ngOnDestroy', () => {
    it('ngOnDestroy should call disconnectWebSocket', () => {
      spyOn<any>(component, 'unsubscribeToWSTopic');
      component.ngOnDestroy();
      expect(component.unsubscribeToWSTopic).toHaveBeenCalled();
    });
  });

  describe('Get Index or checkQuoteCart validations', () => {
    beforeEach(() => {
      component.cart.lineItems = [
        {
          itemId: 1234,
          manufacturerPartNumber: '',
          selectedCustomerPartNumber: '',
          selectedEndCustomerSiteId: 1234,
          requestDate: '',
          quantity: 1,
          id: '123',
          total: 1,
          priceTiers: [
            {
              quantityFrom: 1,
              quantityTo: 1,
              pricePerItem: 1,
            },
          ],
          manufacturer: '',
          description: '',
          price: 1,
          checked: true,
          changed: true,
          targetPrice: 1,
          validation: QuoteCartLineItemStatus.VALID,
          ncnrAccepted: true,
          ncnrAcceptedBy: true,
          image: '',
          inStock: true,
          quotable: true,
          minimumOrderQuantity: 1,
          multipleOrderQuantity: 1,
          availableQuantity: 1,
          bufferQuantity: 1,
          leadTime: '',
          datasheet: '',
          multiple: 1,
          ncnr: true,
          tariffApplicable: true,
        },
      ];
    });

    it('getIndexQuoteCartLineItem should return index of lineItem sent', () => {
      const result = component.getIndexQuoteCartLineItem('123');
      expect(result).toEqual(0);
    });

    it('getIndexQuoteCartLineItem should return -1 if index not found', () => {
      const result = component.getIndexQuoteCartLineItem('124');
      expect(result).toEqual(-1);
    });

    it('checkQuoteCartLineItemsValidationStatus should return QuoteCartLineItemStatus of VALID if lineItems validation is valid', () => {
      const result = component.checkQuoteCartLineItemsValidationStatus();
      expect(result).toEqual(QuoteCartLineItemStatus.VALID);
    });

    it('checkQuoteCartLineItemsValidationStatus should return QuoteCartLineItemStatus of PENDING if lineItems validation is pending', () => {
      component.cart.lineItems[0].validation = QuoteCartLineItemStatus.PENDING;
      const result = component.checkQuoteCartLineItemsValidationStatus();
      expect(result).toEqual(QuoteCartLineItemStatus.PENDING);
    });

    it('getPendingLineItemsIds() should retrieve line items with pending validation status', () => {
      let result = component.getPendingLineItemsIds();
      expect(result).toEqual([]);

      component.cart.lineItems[0].validation = QuoteCartLineItemStatus.PENDING;
      const pendingLineItemId = component.cart.lineItems[0].id;
      result = component.getPendingLineItemsIds();
      expect(result).toEqual([pendingLineItemId]);
    });

    it('checkQuoteCartLineItemsValidationStatus should return QuoteCartLineItemStatus of INVALID if lineItems validation is invalid', () => {
      component.cart.lineItems[0].validation = QuoteCartLineItemStatus.INVALID;
      const result = component.checkQuoteCartLineItemsValidationStatus();
      expect(result).toEqual(QuoteCartLineItemStatus.INVALID);
    });
  });

  describe('Handle Quote Cart error', () => {
    it('handleError should call displaySubmitErrorModal if there is a Quote Cart submission error', () => {
      spyOn(component, 'displaySubmitErrorModal').and.callThrough();

      component.handleError(QuoteCartErrorTypes.SUBMIT_ERROR);
      expect(component.displaySubmitErrorModal).toHaveBeenCalled();
    });

    it('displaySubmitErrorModal should call dialogService.open', () => {
      spyOn(dialogService, 'open').and.callThrough();

      component.displaySubmitErrorModal();
      expect(dialogService.open).toHaveBeenCalled();
    });
  });

  describe('Prepare Line Item update from Weck Socket Message', () => {
    it('should keep changed, checked, enteredCustomerPartNumber and targetPrice from store if they were edited by the user', () => {
      const [index, changed, checked, enteredCustomerPartNumber, targetPrice] = [0, true, true, 'abc123', 100];

      component.cart.lineItems[index] = {
        ...component.cart.lineItems[index],
        changed,
        checked,
        enteredCustomerPartNumber,
        targetPrice,
      };

      const lineItem = component.cart.lineItems[index];
      const newLineItem = {
        ...lineItem,
        changed: false,
        checked: false,
        enteredCustomerPartNumber: undefined,
        targetPrice: undefined,
      };
      const updatedLineItem = component.prepareWSLineItemUpdate(newLineItem, index);

      expect(updatedLineItem.changed).toEqual(lineItem.changed);
      expect(updatedLineItem.checked).toEqual(lineItem.checked);
      expect(updatedLineItem.enteredCustomerPartNumber).toEqual(lineItem.enteredCustomerPartNumber);
      expect(updatedLineItem.targetPrice).toEqual(lineItem.targetPrice);
    });
  });

  it('validateLineItems() should dispatch ValidateQuoteCartLineItems action', () => {
    spyOn(wssService, 'isClientConnected').and.returnValue(true);
    component.cart.lineItems = cloneDeep(quotesModelData).map(lineItem => ({
      ...lineItem,
      validation: QuoteCartLineItemStatus.PENDING,
    }));

    const lineItemsIds = component.getPendingLineItemsIds();
    const quoteCartId = 'abc';
    const request: IQuoteCartValidateLineItemsRequest = {
      lineItemsIds,
      quoteCartId,
    };

    component.validateLineItems(quoteCartId);

    const action = new ValidateQuoteCartLineItems(request);

    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  describe('check if on quote cart are changed items', () => {
    it('onAnyChangeOnQuoteCartLineItem should return false if there is not a changed item', () => {
      const result = component.areSomeQuoteCartLineItemsChangedAndValid();
      expect(result).toBe(false);
    });

    it('onAnyChangeOnQuoteCartLineItem should return true if there is a changed item', () => {
      component.cart.lineItems = quotesModelData.map(item => ({
        ...item,
        changed: true,
      }));
      component.cart.valid = true;

      const result = component.areSomeQuoteCartLineItemsChangedAndValid();
      expect(result).toBe(true);
    });

    it('Should call the store for updating the selected line items', () => {
      lineItemsUpdateRequest.lineItems = lineItemsUpdateRequest.lineItems.map(lineitem => ({
        ...lineitem,
        quantity: OnlyPositiveNumbersDirective.ensureNumber(lineitem.quantity),
        targetPrice: OnlyPositiveNumbersDirective.ensureNumber(lineitem.targetPrice),
      }));

      component.cart.id = lineItemsUpdateRequest.id;
      component.cart.lineItems = lineItemsUpdateRequest.lineItems;
      component.updateQuoteCartLineItems();

      const action = new UpdateQuoteCartLineItems(lineItemsUpdateRequest);
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });
  });
});
