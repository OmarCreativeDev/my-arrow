import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Router, NavigationStart } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { IQuoteCart } from '@app/core/quote-cart/quote-cart.interfaces';
import { IAppState } from '@app/shared/shared.interfaces';
import { getCartSelector, getQuoteCartId, getQuoteCartLoadingSelector } from '@app/features/quotes/stores/quote-cart.selectors';
import { GetQuoteCart, GetQuoteCarts, ClearAdditionalInfoInStore } from '@app/features/quotes/stores/quote-cart.actions';
import { getCurrencyCode } from '@app/core/user/store/user.selectors';
import { getUser } from '@app/core/user/store/user.selectors';
import { IUser } from '@app/core/user/user.interface';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: 'app-quote-cart-complete',
  templateUrl: './quote-cart-complete.component.html',
  styleUrls: ['./quote-cart-complete.component.scss'],
})
export class QuoteCartCompleteComponent implements OnInit, OnDestroy {
  public user$: Observable<IUser>;
  public user: IUser;
  public cart$: Observable<IQuoteCart>;
  public cart: IQuoteCart;
  public currencyCode$: Observable<string>;
  public currencyCode: string;
  public loading$: Observable<boolean>;
  public privateFeatures$: Observable<object>;
  public privateFeatureFlags: object;
  public subscription: Subscription = new Subscription();

  constructor(private quoteCartStore: Store<IAppState>, private router: Router) {
    this.user$ = quoteCartStore.pipe(select(getUser));
    this.cart$ = quoteCartStore.pipe(select(getCartSelector));
    this.currencyCode$ = quoteCartStore.pipe(select(getCurrencyCode));
    this.loading$ = quoteCartStore.pipe(select(getQuoteCartLoadingSelector));
    this.privateFeatures$ = quoteCartStore.pipe(select(getPrivateFeatureFlagsSelector));
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public getQuoteCart(): void {
    this.quoteCartStore.dispatch(new GetQuoteCarts());
    const quoteCartId$ = this.quoteCartStore.pipe(select(getQuoteCartId));
    quoteCartId$.pipe(take(1)).subscribe(quoteCartId => {
      if (quoteCartId) {
        this.quoteCartStore.dispatch(new GetQuoteCart(quoteCartId));
      }
    });
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribePrivateFeatureFlags());
    this.subscription.add(this.subscribeUser());
    this.subscription.add(this.subscribeRouter());
    this.subscription.add(this.subscribeQuoteCart());
    this.subscription.add(this.subscribeCurrency());
  }

  private subscribePrivateFeatureFlags(): Subscription {
    return this.privateFeatures$.subscribe(featureFlags => {
      this.privateFeatureFlags = featureFlags;
    });
  }

  private subscribeUser(): Subscription {
    return this.user$.subscribe(user => {
      this.user = user;
    });
  }

  private subscribeRouter(): Subscription {
    return this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.getQuoteCart();
        this.quoteCartStore.dispatch(new ClearAdditionalInfoInStore());
      }
    });
  }

  private subscribeQuoteCart(): Subscription {
    return this.cart$.subscribe(quoteCart => (this.cart = quoteCart));
  }

  private subscribeCurrency(): Subscription {
    return this.currencyCode$.subscribe(currencyCode => {
      this.currencyCode = currencyCode;
    });
  }

  public printPage() {
    window.print();
  }
}
