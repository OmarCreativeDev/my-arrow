import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';

import { QuoteCartCompleteComponent } from './quote-cart-complete.component';
import { QuoteCartTableComponent } from '../../components/quote-cart-table/quote-cart-table.component';
import { SharedModule } from '@app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';

class MockStore {
  select() {
    return of();
  }
  dispatch() {}
  pipe() {
    return of();
  }
}

class DummyComponent {}

describe('QuoteCartCompleteComponent', () => {
  let component: QuoteCartCompleteComponent;
  let fixture: ComponentFixture<QuoteCartCompleteComponent>;
  const routes = [{ path: '', component: DummyComponent }];

  const mockCart = {
    id: '5a8701382c1724bf4003132c',
    total: 8065.079999999999,
    status: 'IN_PROGRESS',
    lineItems: [],
    additionalInfo: 'Some Comments',
    metadata: {},
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, ReactiveFormsModule, RouterTestingModule.withRoutes(routes)],
      declarations: [QuoteCartCompleteComponent, QuoteCartTableComponent],
      providers: [{ provide: Store, useClass: MockStore }, FormBuilder],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteCartCompleteComponent);
    component = fixture.componentInstance;
    component.cart = mockCart;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call printPage', () => {
    spyOn(component, 'printPage').and.callFake(() => true);
    component.printPage();
    fixture.detectChanges();
    expect(component.printPage).toHaveBeenCalled();
  });
});
