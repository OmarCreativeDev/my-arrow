import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuotesComponent } from '@app/features/quotes/pages/quotes/quotes.component';
import { QuoteCartCompleteComponent } from '@app/features/quotes/pages/quote-cart-complete/quote-cart-complete.component';
import { QuoteCartGuard } from '@app/core/quote-cart/quote-cart.guard';

const routes: Routes = [
  {
    path: '',
    component: QuotesComponent,
    data: { title: 'Quote Cart' },
  },
  {
    path: 'quote-cart-complete',
    component: QuoteCartCompleteComponent,
    canActivate: [QuoteCartGuard],
    data: { title: 'Quote Cart - Complete' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class QuotesRoutingModule {}
