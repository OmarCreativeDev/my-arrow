import { Component, OnInit } from '@angular/core';

import { Dialog } from '@app/core/dialog/dialog.service';

@Component({
  selector: 'app-quote-cart-delete-dialog',
  templateUrl: './quote-cart-delete-dialog.component.html',
  styleUrls: ['./quote-cart-delete-dialog.component.scss'],
})
export class QuoteCartDeleteDialogComponent implements OnInit {
  public shouldDelete: boolean = true;

  constructor(private dialogService: Dialog<QuoteCartDeleteDialogComponent>) {}

  ngOnInit() {}

  public onDismiss(willDelete: boolean = false): void {
    this.dialogService.close(willDelete);
  }
}
