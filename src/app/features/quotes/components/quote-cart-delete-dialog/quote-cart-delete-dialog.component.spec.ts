import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { StoreModule, combineReducers } from '@ngrx/store';

import { Dialog } from '@app/core/dialog/dialog.service';
import { SharedModule } from '@app/shared/shared.module';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { QuoteCartDeleteDialogComponent } from './quote-cart-delete-dialog.component';
import { userReducers } from '@app/core/user/store/user.reducers';

describe('QuoteCartDeleteModalComponent', () => {
  let component: QuoteCartDeleteDialogComponent;
  let fixture: ComponentFixture<QuoteCartDeleteDialogComponent>;
  let dialog: Dialog<QuoteCartDeleteDialogComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [QuoteCartDeleteDialogComponent],
        imports: [
          SharedModule,
          HttpClientTestingModule,
          StoreModule.forRoot({
            user: combineReducers(userReducers),
          }),
        ],
        providers: [{ provide: Dialog, useClass: DialogMock }],
      }).compileComponents();

      dialog = TestBed.get(Dialog);
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteCartDeleteDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the dialog and send false to the dialog when no params are sent', () => {
    spyOn(dialog, 'close');
    component.onDismiss();
    expect(dialog.close).toHaveBeenCalledWith(false);
  });

  it('should close the dialog and send true to the dialog when it is called with a param', () => {
    spyOn(dialog, 'close');
    component.onDismiss(true);
    expect(dialog.close).toHaveBeenCalledWith(true);
  });
});
