import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { filter, first, take } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { getSubmittedQuotesCount } from '@app/features/quotes/stores/quote-cart.selectors';
import { GetSubmittedQuotesCount } from '@app/features/quotes/stores/quote-cart.actions';
import { getUserBillToAccount } from '@app/core/user/store/user.selectors';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: 'app-view-submitted-quotes',
  templateUrl: './view-submitted-quotes.component.html',
  styleUrls: ['./view-submitted-quotes.component.scss'],
})
export class ViewSubmittedQuotesComponent implements OnInit, OnDestroy {
  public quotesCount$: Observable<number>;
  public quotesCount: number;
  public billToAccountSub: Subscription;
  public billToAccount$: Observable<number>;
  public currentBillToAccount: number;
  public privateFeatureFlags: any;
  public privateFeatures$: Observable<any>;
  public subscription: Subscription = new Subscription();

  constructor(public store: Store<IAppState>) {
    this.quotesCount$ = this.store.pipe(select(getSubmittedQuotesCount));
    this.billToAccount$ = this.store.pipe(select(getUserBillToAccount));
    this.privateFeatures$ = this.store.pipe(select(getPrivateFeatureFlagsSelector));
  }

  ngOnInit() {
    this.getSubmittedQuotesCount();
    this.reloadQuoteCountOnBillToChange();
    this.startSubscriptions();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeQuotesCount());
    this.subscription.add(this.subscribePrivateFeatureFlags());
    this.subscription.add(this.subscribeBillToAccount());
  }

  private subscribeQuotesCount(): Subscription {
    return this.quotesCount$.subscribe(quotesCount => {
      this.quotesCount = quotesCount;
    });
  }

  private subscribePrivateFeatureFlags(): Subscription {
    return this.privateFeatures$.pipe(take(1)).subscribe(featureFlags => {
      this.privateFeatureFlags = featureFlags;
    });
  }

  private subscribeBillToAccount(): Subscription {
    return this.billToAccount$.pipe(first()).subscribe((billToAccount: number) => {
      this.setCurrentBillToAccount(billToAccount);
    });
  }

  public getSubmittedQuotesCount(): void {
    this.store.dispatch(new GetSubmittedQuotesCount());
  }

  public setCurrentBillToAccount(billToAccount: number): void {
    this.currentBillToAccount = billToAccount;
  }

  public reloadQuoteCountOnBillToChange(): void {
    this.billToAccountSub = this.billToAccount$.pipe(filter(billToAccount => billToAccount !== 0)).subscribe((billToAccount: number) => {
      if (this.currentBillToAccount !== billToAccount) {
        this.setCurrentBillToAccount(billToAccount);
        this.getSubmittedQuotesCount();
      }
    });
  }

  ngOnDestroy() {
    this.billToAccountSub.unsubscribe();
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }
}
