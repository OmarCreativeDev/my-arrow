import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { ViewSubmittedQuotesComponent } from './view-submitted-quotes.component';
import { Observable, of } from 'rxjs';
import { QuotesService } from '@app/core/quotes/quotes.service';
import { GetSubmittedQuotesCount } from '@app/features/quotes/stores/quote-cart.actions';

export class StoreMock {
  public dispatch(): void {}
  public select(): Observable<number> {
    return of(10);
  }
  public pipe() {
    return of({});
  }
}

export class QuotesServiceMock {
  public search(): Observable<any> {
    return of({});
  }
}

let store: Store<any>;

describe('ViewSubmittedQuotesComponent', () => {
  let component: ViewSubmittedQuotesComponent;
  let fixture: ComponentFixture<ViewSubmittedQuotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: Store, useClass: StoreMock }, { provide: QuotesService, useClass: QuotesServiceMock }],
      declarations: [ViewSubmittedQuotesComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSubmittedQuotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`GetSubmittedQuotesCount` action is dispatched on the store on init', () => {
    spyOn(store, 'dispatch');
    component.ngOnInit();
    expect(store.dispatch).toHaveBeenCalledWith(new GetSubmittedQuotesCount());
  });

  it('`setCurrentBillToAccount()` sets `currentBillToAccount`', () => {
    component.setCurrentBillToAccount(33291);
    expect(component.currentBillToAccount).toBeDefined();
    expect(component.currentBillToAccount).toEqual(33291);
  });

  it('`reloadQuoteCountOnBillToChange()` invokes `getSubmittedQuotesCount()`, `setCurrentBillToAccount()` if `billToAccount` has changed', () => {
    spyOn(component, 'setCurrentBillToAccount');
    spyOn(component, 'getSubmittedQuotesCount');

    component.currentBillToAccount = 32313;
    component.reloadQuoteCountOnBillToChange();

    expect(component.setCurrentBillToAccount).toHaveBeenCalled();
    expect(component.getSubmittedQuotesCount).toHaveBeenCalled();
  });
});
