import { cloneDeep } from 'lodash-es';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, ReactiveFormsModule, FormGroup } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import { Store, StoreModule } from '@ngrx/store';

import { SharedModule } from '@app/shared/shared.module';
import { QuoteCartTableComponent } from '@app/features/quotes/components/quote-cart-table/quote-cart-table.component';
import { quoteCartReducers } from '@app/features/quotes/stores/quote-cart.reducers';
import {
  UpdateItemFieldInStore,
  ToggleAllQuoteCartLineItems,
  ToggleCheckQuoteCartLineItems,
} from '@app/features/quotes/stores/quote-cart.actions';
import { IQuoteCartLineItem } from '@app/core/quote-cart/quote-cart.interfaces';

import cartModelData from './cart-model-data-mock';
import { QuoteCartLineItemStatus } from '@app/core/quote-cart/quote-cart.enum';

describe('QuotesCartTableComponent', () => {
  let component: QuoteCartTableComponent;
  let fixture: ComponentFixture<QuoteCartTableComponent>;
  let store: Store<any>;
  let formBuilder: FormBuilder;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CommonModule, SharedModule, ReactiveFormsModule, StoreModule.forRoot({ quoteCart: quoteCartReducers })],
      declarations: [QuoteCartTableComponent],
      providers: [FormBuilder],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteCartTableComponent);
    store = TestBed.get(Store);
    formBuilder = TestBed.get(FormBuilder);
    component = fixture.componentInstance;
    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#changeLineItemsDate', () => {
    it('Should call the store for updating the selected line items request date', () => {
      component.lineItems = cartModelData;
      const requestDate = '2099-12-12';
      const fieldName = 'requestDate';
      const index = 0;

      component.quoteCartForm = formBuilder.group({
        quoteCartLineItems: cartModelData,
      });
      component.updateItemFieldInStore(fieldName, requestDate, index);

      const action = new UpdateItemFieldInStore({ fieldName: fieldName, value: requestDate, index: index });
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });
  });

  describe('#getQuantityErrorMessage', () => {
    it('should return Please enter quantity when required error appears', () => {
      const quantity = new FormControl();

      quantity.setErrors({ required: true });
      const result = component.getQuantityErrorMessage(quantity);
      expect(result).toBe('Please enter quantity.');
    });
    it('should return Quantity cannot be 0. when required error appears', () => {
      const quantity = new FormControl();

      quantity.setValue(0);
      quantity.setErrors({ min: true });
      const result = component.getQuantityErrorMessage(quantity);
      expect(result).toBe('Quantity cannot be 0.');
    });
  });

  describe('Toggle checkboxes tests', () => {
    it('toggleAllFoundCheckboxes should dispatch the action ToggleAllQuoteCartLineItems', () => {
      fixture.detectChanges();

      const _lineItems: Array<IQuoteCartLineItem> = component.lineItems.map((lineItem: IQuoteCartLineItem) => ({
        ...lineItem,
        checked: false,
      }));

      component.toggleAllFoundCheckboxes();

      const action = new ToggleAllQuoteCartLineItems(_lineItems);
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });

    it('ToggleCheckQuoteCartLineItems should dispatch the action ToggleAllQuoteCartLineItems', () => {
      fixture.detectChanges();

      component.lineItems = [
        {
          itemId: 1234,
          manufacturerPartNumber: '',
          selectedCustomerPartNumber: '',
          selectedEndCustomerSiteId: 1234,
          requestDate: '',
          quantity: 1,
          id: '124',
          total: 1,
          priceTiers: [
            {
              quantityFrom: 1,
              quantityTo: 1,
              pricePerItem: 1,
            },
          ],
          manufacturer: '',
          description: '',
          price: 1,
          checked: true,
          changed: true,
          targetPrice: 1,
          validation: QuoteCartLineItemStatus.INVALID,
          ncnrAccepted: true,
          ncnrAcceptedBy: true,
          image: '',
          inStock: true,
          quotable: true,
          minimumOrderQuantity: 1,
          multipleOrderQuantity: 1,
          availableQuantity: 1,
          bufferQuantity: 1,
          leadTime: '',
          datasheet: '',
          multiple: 1,
          ncnr: true,
          tariffApplicable: true,
        },
      ];

      const partNumber = { id: '124' };
      const index = 0;

      component.ToggleCheckQuoteCartLineItems(partNumber);

      const action = new ToggleCheckQuoteCartLineItems({ index, checked: true });
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });

    it('areAllQuoteCartLineItemsChecked should return true when all Part Numbers are checked', () => {
      component.lineItems.map((lineItem: IQuoteCartLineItem) => ({
        ...lineItem,
        checked: true,
      }));

      const actual = component.areAllQuoteCartLineItemsChecked();
      expect(actual).toBeTruthy();
    });
  });

  it('getTargetPriceErrorMessage should return "Please enter a valid target price."', () => {
    const result = component.getTargetPriceErrorMessage();
    expect(result).toEqual('Please enter a valid target price.');
  });

  it('calculateTotal should return the total of the calculation depending on the parameters sent', () => {
    const price = 10;
    const quantity = 23;
    const result = component.calculateTotal(price, quantity);
    expect(result.toNumber()).toEqual(230);
  });

  it('calculateTotal should return 0 if the parameters sent make the calculation invalid', () => {
    const price = 0;
    const quantity = 23;
    const result = component.calculateTotal(price, quantity);
    expect(result.toNumber()).toEqual(0);
  });

  it('shouldShowPendingMessage should return true if parameter equals PENDING', () => {
    const pending = QuoteCartLineItemStatus.PENDING;
    const result = component.shouldShowPendingMessage(pending);
    expect(result).toBeTruthy();
  });

  it('shouldShowInvalidMessage should return true if parameter equals INVALID', () => {
    const invalid = QuoteCartLineItemStatus.INVALID;
    const result = component.shouldShowInvalidMessage(invalid);
    expect(result).toBeTruthy();
  });

  describe('#getRequestDateErrorMessage', () => {
    it('should return Request date cannot be a past date when invalid date error appears', () => {
      const requestDate = new FormControl();

      requestDate.setErrors({ invalidDate: true });
      const result = component.getRequestDateErrorMessage(requestDate);
      expect(result).toBe('Request date cannot be a past date.');
    });
  });

  it('getLineItemFormControlFromIndex should return FormGroup at requested index', () => {
    const index = 0;

    component.lineItems = cartModelData;

    component.ngOnChanges();

    const result = component.getLineItemFormControlFromIndex(index);

    expect(result).toEqual(component.quoteCartLineItems.controls[index]);
  });

  it('filterEndCustomerNameFromEndCustomerRecordsBySiteId should filter IEndCustomerRecord collection and find first element with matching endCustomerSiteId and return the endCustomerName', () => {
    const endCustomerName = 'test';
    const endCustomerSiteId = 123;
    const endCustomerRecords = [
      {
        cpn: 'abc',
        endCustomers: [
          {
            endCustomerSiteId: endCustomerSiteId,
            name: endCustomerName,
          },
          {
            endCustomerSiteId: 234,
            name: 'test 2',
          },
        ],
      },
    ];
    const filteredEndCustomerName = component.filterEndCustomerNameFromEndCustomerRecordsBySiteId(endCustomerRecords, endCustomerSiteId);

    expect(filteredEndCustomerName).toEqual(endCustomerName);
  });

  it('getEndCustomerName should retrieve the endCustomerName from the lineItem', () => {
    const index = 0;
    const _cartModelData = cloneDeep(cartModelData);
    const endCustomerName = 'test';
    const endCustomerSiteId = 123;

    _cartModelData.forEach(lineItem => {
      lineItem.endCustomerRecords = [
        {
          cpn: 'abc',
          endCustomers: [
            {
              endCustomerSiteId: endCustomerSiteId,
              name: endCustomerName,
            },
            {
              endCustomerSiteId: 234,
              name: 'test 2',
            },
          ],
        },
      ];

      lineItem.selectedEndCustomerSiteId = endCustomerSiteId;
    });

    const result = component.getEndCustomerName(_cartModelData[index]);

    expect(result).toEqual(endCustomerName);
  });

  describe('#shouldShowEndCustomer', () => {
    it('should return true if selectedEndCustomerSiteId is defined and validation is not PENDING', () => {
      const index = 0;
      const _cartModelData = cloneDeep(cartModelData);
      const endCustomerSiteId = 123;

      component.lineItems = _cartModelData.map(_lineItem => {
        _lineItem.selectedEndCustomerSiteId = endCustomerSiteId;
        _lineItem.validation = QuoteCartLineItemStatus.VALID;
        _lineItem.endCustomerRecords = [
          {
            cpn: '100123',
            endCustomers: [
              {
                name: 'EXAMPLE CUSTOMER NAME',
                endCustomerSiteId: endCustomerSiteId,
              },
            ],
          },
        ];
        return _lineItem;
      });

      component.ngOnChanges();

      const lineItem = <FormGroup>component.quoteCartLineItems.controls[index];

      const result = component.shouldShowEndCustomer(lineItem);

      expect(result).toBe(true);
    });

    it('should return false if selectedEndCustomerSiteId is defined but not selectedEndCustomerName is defined and validation is not PENDING', () => {
      const index = 0;
      const _cartModelData = cloneDeep(cartModelData);
      const endCustomerSiteId = 123;

      component.lineItems = _cartModelData.map(_lineItem => {
        _lineItem.selectedEndCustomerSiteId = endCustomerSiteId;
        _lineItem.validation = QuoteCartLineItemStatus.VALID;
        return _lineItem;
      });

      component.ngOnChanges();

      const lineItem = <FormGroup>component.quoteCartLineItems.controls[index];
      const result = component.shouldShowEndCustomer(lineItem);

      expect(result).toBe(false);
    });

    it('should return false if selectedEndCustomerSiteId is not defined and validation is not PENDING', () => {
      const index = 0;
      const _cartModelData = cloneDeep(cartModelData);

      component.lineItems = _cartModelData.map(_lineItem => {
        _lineItem.selectedEndCustomerSiteId = undefined;
        _lineItem.validation = QuoteCartLineItemStatus.VALID;

        return _lineItem;
      });

      component.ngOnChanges();

      const lineItem = <FormGroup>component.quoteCartLineItems.controls[index];
      const result = component.shouldShowEndCustomer(lineItem);

      expect(result).toBe(false);
    });

    it('should return false if selectedEndCustomerSiteId is defined and validation is PENDING', () => {
      const index = 0;
      const _cartModelData = cloneDeep(cartModelData);
      const endCustomerSiteId = 123;

      component.lineItems = _cartModelData.map(_lineItem => {
        _lineItem.selectedEndCustomerSiteId = endCustomerSiteId;
        _lineItem.validation = QuoteCartLineItemStatus.PENDING;

        return _lineItem;
      });

      component.ngOnChanges();

      const lineItem = <FormGroup>component.quoteCartLineItems.controls[index];
      const result = component.shouldShowEndCustomer(lineItem);

      expect(result).toBe(false);
    });
  });

  it('updateItemField should call updateItemFieldInStore with the appropiate arguments', () => {
    spyOn(component, 'updateItemFieldInStore').and.callThrough();

    const index = 0;
    const fieldName = 'quantity';

    component.lineItems = cartModelData;

    component.ngOnChanges();

    const lineItem = <FormGroup>component.quoteCartLineItems.controls[index];

    component.updateItemField(fieldName, lineItem, index);

    expect(component.updateItemFieldInStore).toHaveBeenCalledWith(fieldName, component.lineItems[index].quantity, index);
  });

  it('findSelectedTier should return the required tier according to the quantity sent', () => {
    const quantity = 1;
    const priceTiers = [
      {
        quantityFrom: 1,
        quantityTo: 7317,
        pricePerItem: 0.03727,
      },
    ];
    const expected = {
      quantityFrom: 1,
      quantityTo: 7317,
      pricePerItem: 0.03727,
    };

    const result = component.findSelectedTier(quantity, priceTiers);
    expect(result).toEqual(expected);
  });

  it('validateQuantity should return 1 if the value sent is empty or 0', () => {
    const value = '0';
    const result = component.validateQuantity(value);
    expect(result).toEqual(1);
  });

  it('updateQuantityField should call updateField with the appropiate arguments', () => {
    spyOn(component, 'updateItemFieldInStore').and.callThrough();
    const index = 0;
    const fieldName = 'quantity';

    component.lineItems = cartModelData;
    component.ngOnChanges();
    const lineItem = <FormGroup>component.quoteCartLineItems.controls[index];
    component.updateQuantityField(lineItem, index);
    expect(component.updateItemFieldInStore).toHaveBeenCalledWith(fieldName, component.lineItems[index].quantity, index);
  });

  it('addCurrencySymbol should return appropriate currency symbol', () => {
    component.currencyCode = 'USD';

    const dollarCurrencySymbol = component.addCurrencySymbol();

    component.currencyCode = 'EUR';

    const euroCurrencySymbol = component.addCurrencySymbol();

    expect(dollarCurrencySymbol).toBe('$');
    expect(euroCurrencySymbol).toBe('€');
  });

  it('shouldDisplayCurrencySymbol should return false if field value is empty or equal to 0', () => {
    const index = 0;
    const fieldName = 'targetPrice';

    component.lineItems = cartModelData;

    component.ngOnChanges();

    const lineItem = <FormControl>component.quoteCartLineItems.controls[index];

    lineItem.value.targetPrice = '';

    const emptyResult = component.shouldDisplayCurrencySymbol(lineItem, fieldName);

    lineItem.value.targetPrice = '0.0';

    const zeroResult = component.shouldDisplayCurrencySymbol(lineItem, fieldName);

    expect(emptyResult).toBe(false);
    expect(zeroResult).toBe(false);
  });

  it("shouldDisplayCurrencySymbol should return true if field value is not empty and it's greater than 0", () => {
    const index = 0;
    const fieldName = 'targetPrice';

    component.lineItems = cartModelData;

    component.ngOnChanges();

    const lineItem = <FormControl>component.quoteCartLineItems.controls[index];

    lineItem.value.targetPrice = '0.1';

    const result = component.shouldDisplayCurrencySymbol(lineItem, fieldName);

    expect(result).toBe(true);
  });

  it("ngOnChanges should set allCheckboxesTicked to false, if lineItems is set but it's empty", () => {
    component.lineItems = [];
    component.ngOnChanges();
    expect(component.allCheckboxesTicked).toEqual(false);
  });

  it('buildQuoteCartLineItems should not call patchOrBuildCartItem when method is call with no arguments', () => {
    spyOn(component, 'patchOrBuildCartItem');
    component.buildQuoteCartLineItems();
    expect(component.patchOrBuildCartItem).not.toHaveBeenCalled();
  });

  it('patchOrBuildCartItem should call buildQuoteCartLineItem when a new lineItem is passed as argument, and set lineItem quantity value to 1, if lineItem quantity is undefined', () => {
    const lineItems = cartModelData.slice();
    const lineItem = lineItems[0];

    lineItem.quantity = undefined;

    const result = component.patchOrBuildCartItem(lineItem);

    expect(result.controls.quantity.value).toEqual(1);
  });
});
