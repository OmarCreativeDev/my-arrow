import { findIndex, some, find } from 'lodash-es';
import * as moment from 'moment';
import { Component, Input, OnInit, OnChanges } from '@angular/core';
import * as math from 'mathjs';
import { Store } from '@ngrx/store';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl, AbstractControl } from '@angular/forms';
import { getCurrencySymbol } from '@angular/common';

import { IAppState } from '@app/shared/shared.interfaces';
import { IQuoteCartLineItem, IEndCustomerRecord } from '@app/core/quote-cart/quote-cart.interfaces';
import {
  UpdateItemFieldInStore,
  ToggleCheckQuoteCartLineItems,
  ToggleAllQuoteCartLineItems,
  UpdateQuoteCartWithValidFlag,
} from '@app/features/quotes/stores/quote-cart.actions';

import { IDateInputConfig } from '@app/shared/components/date-picker/date-picker.interfaces';
import { CustomValidators } from '@app/shared/classes/custom-validators';
import { isArray, chain } from 'lodash';
import { DatePickerOverlayActions } from '@app/shared/components/date-picker/date-picker-overlay/date-picker-overlay.interface';
import { QuoteCartLineItemStatus } from '@app/core/quote-cart/quote-cart.enum';
import { CurrencyFormatDirective } from '@app/shared/directives/currency-format.directive';
import { OnlyPositiveNumbersDirective } from '@app/shared/directives/only-positive-numbers.directive';

@Component({
  selector: 'app-quote-cart-table',
  templateUrl: './quote-cart-table.component.html',
  styleUrls: ['./quote-cart-table.component.scss'],
})
export class QuoteCartTableComponent implements OnInit, OnChanges {
  @Input()
  public view: string = '';
  @Input()
  public lineItems: Array<IQuoteCartLineItem> = [];
  @Input()
  public currencyCode: string = '';
  @Input()
  public displayTariffInfo: boolean = false;

  public quoteCartForm: FormGroup;
  public quoteCartLineItems: FormArray;
  public quoteCartLineItemFormGroupMap: Map<string, FormGroup> = new Map<string, FormGroup>();
  public DATE_FORMAT: string = 'YYYY-MM-DD';
  public minDate = moment(new Date())
    .startOf('day')
    .toDate();
  public allCheckboxesTicked: boolean;

  public allowedQuantityKeys = [8, 9, 13, 27, 35, 36, 37, 38, 39, 40, 46, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 110];
  public allowedTargePriceKeys = [...this.allowedQuantityKeys, 190];
  public defaultTargetPrice: string = '0.0';
  public showTiers = [];

  public requestDateConfiguration: IDateInputConfig[] = [
    {
      placeholder: 'Select Date',
    },
  ];
  public datePickerOverlayActions: Array<DatePickerOverlayActions> = [
    DatePickerOverlayActions.GO_TO_TODAY,
    DatePickerOverlayActions.SUBMIT,
  ];

  constructor(private formBuilder: FormBuilder, private quoteCartStore: Store<IAppState>) {}

  ngOnInit(): void {
    this.allCheckboxesTicked = false;
  }

  ngOnChanges(): void {
    /* istanbul ignore else */
    if (this.lineItems) {
      this.buildQuoteCartForm();
      this.allCheckboxesTicked = this.lineItems.length ? this.areAllQuoteCartLineItemsChecked() : false;
    }
  }

  private buildQuoteCartForm() {
    this.quoteCartLineItems = this.buildQuoteCartLineItems(this.lineItems);
    this.quoteCartForm = this.formBuilder.group({
      quoteCartLineItems: this.quoteCartLineItems,
    });
  }

  public buildQuoteCartLineItems(quoteCartLineItems: IQuoteCartLineItem[] = []): FormArray {
    const quoteCartLineItemsGroup: FormGroup[] = quoteCartLineItems.map(quoteCartLineItem => this.patchOrBuildCartItem(quoteCartLineItem));
    return this.formBuilder.array(quoteCartLineItemsGroup);
  }

  private buildQuoteCartLineItem(quoteCartLineItem: IQuoteCartLineItem): FormGroup {
    const {
      enteredCustomerPartNumber,
      selectedCustomerPartNumber,
      selectedEndCustomerSiteId,
      selectedEndCustomerName,
      quantity,
      requestDate,
      targetPrice,
    } = quoteCartLineItem;
    const requestDateArray = [moment(requestDate, this.DATE_FORMAT).toDate()];

    const quoteCartItemFormGroup: FormGroup = this.formBuilder.group({
      ...quoteCartLineItem,
      enteredCustomerPartNumber: enteredCustomerPartNumber || '',
      selectedCustomerPartNumber: selectedCustomerPartNumber || '',
      selectedEndCustomerSiteId: selectedEndCustomerSiteId || '',
      selectedEndCustomerName: selectedEndCustomerName || '',
      quantity: [
        Math.abs(quantity) || 1,
        Validators.compose([Validators.required, CustomValidators.integer, Validators.pattern('[0-9]+$'), Validators.min(1)]),
      ],
      requestDate: [
        {
          value: requestDateArray,
          disabled: false,
        },
        [CustomValidators.date, Validators.required],
      ],
      targetPriceFormatted: [
        CurrencyFormatDirective.moneyFormatNumber(String(targetPrice)) || 0,
        Validators.compose([Validators.pattern('([0-9](,[0-9])?)+(.[0-9]+)?$')]),
      ],
    });
    this.toggleEnableFormInputs(quoteCartItemFormGroup, quoteCartLineItem);

    quoteCartItemFormGroup
      .get('requestDate')
      .valueChanges.subscribe(dateArray => this.updateRequestDate(dateArray[0], quoteCartLineItem.id));

    this.quoteCartLineItemFormGroupMap.set(quoteCartLineItem.id, quoteCartItemFormGroup);

    return quoteCartItemFormGroup;
  }

  private updateRequestDate(date: Date, quoteCartItemId: string) {
    const dateString = moment(date).format(this.DATE_FORMAT);
    const index: number = findIndex(this.lineItems, { id: quoteCartItemId });

    this.updateItemFieldInStore('requestDate', dateString, index);
  }

  public patchOrBuildCartItem(quoteCartItem: IQuoteCartLineItem): FormGroup {
    quoteCartItem.selectedEndCustomerName = this.getEndCustomerName(quoteCartItem);

    let quoteCartLineItemFormGroup = this.quoteCartLineItemFormGroupMap.get(quoteCartItem.id);
    if (quoteCartLineItemFormGroup) {
      quoteCartLineItemFormGroup = this.patchCartItemFormGroup(quoteCartLineItemFormGroup, quoteCartItem);
    } else {
      quoteCartLineItemFormGroup = this.buildQuoteCartLineItem(quoteCartItem);
    }
    return quoteCartLineItemFormGroup;
  }

  public updateItemField(fieldName: string, quoteCartItemFormGroup: FormGroup, index: number) {
    const value = quoteCartItemFormGroup.value[fieldName];
    this.updateField(fieldName, quoteCartItemFormGroup, index, value);
  }

  public updateQuantityField(quoteCartItemFormGroup: FormGroup, index: number): void {
    const fieldName = 'quantity';
    const value = this.validateQuantity(quoteCartItemFormGroup.value[fieldName]);
    this.updateField(fieldName, quoteCartItemFormGroup, index, value);
  }

  public updateField(fieldName: string, quoteCartItemFormGroup: FormGroup, index: number, value) {
    const newLineItem = {
      ...this.lineItems[index],
      [fieldName]: value,
    };
    this.patchCartItemFormGroup(quoteCartItemFormGroup, newLineItem);
    this.updateItemFieldInStore(fieldName, value, index);
  }

  public validateQuantity(value: any): any {
    return value === '' || value === '0' ? 1 : value;
  }

  private patchCartItemFormGroup(quoteCartItemFormGroup: FormGroup, quoteCartItem): FormGroup {
    const requestDateArray = [moment(quoteCartItem.requestDate, this.DATE_FORMAT).toDate()];
    quoteCartItemFormGroup.patchValue({
      ...quoteCartItem,
      enteredCustomerPartNumber: quoteCartItem.enteredCustomerPartNumber || '',
      selectedCustomerPartNumber: quoteCartItem.selectedCustomerPartNumber || '',
      selectedEndCustomerSiteId: quoteCartItem.selectedEndCustomerSiteId || '',
      selectedEndCustomerName: quoteCartItem.selectedEndCustomerName || '',
      requestDate: requestDateArray,
    });

    this.toggleEnableFormInputs(quoteCartItemFormGroup, quoteCartItem);
    return quoteCartItemFormGroup;
  }

  public updateItemFieldInStore(fieldName: string, value: any, index: number) {
    if (this.lineItems[index][fieldName] !== value) {
      if (!value && this.lineItems[index][fieldName] === undefined) {
        return;
      }
      this.quoteCartStore.dispatch(new UpdateItemFieldInStore({ fieldName, value, index }));
      this.quoteCartForm.valid
        ? this.quoteCartStore.dispatch(new UpdateQuoteCartWithValidFlag(true))
        : this.quoteCartStore.dispatch(new UpdateQuoteCartWithValidFlag(false));
    }
  }

  public getQuantityErrorMessage(quantity: FormControl) {
    let errorMessage = '';
    if (quantity.errors.required) {
      errorMessage = 'Please enter quantity.';
    } else if (quantity.errors.min) {
      errorMessage = 'Quantity cannot be 0.';
    }
    return errorMessage;
  }

  public getTargetPriceErrorMessage() {
    return 'Please enter a valid target price.';
  }

  public areAllQuoteCartLineItemsChecked(): boolean {
    return !some(this.lineItems, ['checked', false]);
  }

  public toggleAllFoundCheckboxes(): void {
    const _checked = !this.allCheckboxesTicked;
    this.allCheckboxesTicked = !this.allCheckboxesTicked;

    const _lineItems: Array<any> = this.lineItems.map(lineItem => ({
      ...lineItem,
      checked: _checked,
    }));

    this.quoteCartStore.dispatch(new ToggleAllQuoteCartLineItems(_lineItems));
  }

  public ToggleCheckQuoteCartLineItems(partNumber, checked = false): void {
    const id = partNumber.id;
    const index: number = this.lineItems.findIndex(x => x.id === id);
    this.quoteCartStore.dispatch(new ToggleCheckQuoteCartLineItems({ index, checked: !checked }));
  }

  public calculateTotal(price: number, quantity: number) {
    try {
      return math
        .chain(price ? math.bignumber(price) : 0)
        .multiply(quantity ? math.bignumber(quantity) : 0)
        .done();
    } catch {
      return price * 1;
    }
  }

  public onQuantityInput($event, fieldName, quoteCartLineItem, index) {
    this.updateItemField(fieldName, quoteCartLineItem, index);
    this.updatePrice(index, quoteCartLineItem);
  }

  public updatePrice(index: number, lineItem: FormControl): void {
    if (
      lineItem.value.quantity !== '' &&
      lineItem.value.quantity !== '0' &&
      lineItem.value.priceTiers &&
      lineItem.value.priceTiers.length
    ) {
      const selectedTier = this.findSelectedTier(lineItem.value.quantity, lineItem.value.priceTiers);
      this.updateItemFieldInStore('price', selectedTier.pricePerItem, index);
    }
  }

  public findSelectedTier(quantity: number, priceTiers: Array<object>) {
    const currentTier = find(priceTiers, tier => {
      return quantity >= tier.quantityFrom && (quantity <= tier.quantityTo || tier.quantityTo === null);
    });

    return currentTier ? currentTier : priceTiers.slice(-1)[0];
  }

  public onTargetPriceChange($event, fieldName, quoteCartLineItem, index) {
    quoteCartLineItem.value[fieldName] = OnlyPositiveNumbersDirective.ensureNumber($event.target.value);
    this.updateItemField(fieldName, quoteCartLineItem, index);
  }

  public shouldShowInvalidMessage(lineItemValidation: string) {
    return lineItemValidation === QuoteCartLineItemStatus.INVALID;
  }

  public shouldShowPendingMessage(lineItemValidation: string) {
    return lineItemValidation === QuoteCartLineItemStatus.PENDING;
  }

  public getRequestDateErrorMessage(requestDate: FormControl): string {
    return requestDate.errors.invalidDate ? 'Request date cannot be a past date.' : '';
  }

  public getLineItemFormControlFromIndex(index: number): AbstractControl {
    return this.quoteCartLineItems.controls[index];
  }

  public filterEndCustomerNameFromEndCustomerRecordsBySiteId(
    endCustomerRecords: Array<IEndCustomerRecord>,
    selectedEndCustomerSiteId: number
  ): string {
    return chain(endCustomerRecords)
      .map(item => item.endCustomers)
      .flatMapDeep()
      .uniq()
      .find(({ endCustomerSiteId }) => endCustomerSiteId === selectedEndCustomerSiteId)
      .get('name', '')
      .value();
  }

  public getEndCustomerName(lineItem: IQuoteCartLineItem) {
    let endCustomerName = '';
    const endCustomerRecords: Array<IEndCustomerRecord> = isArray(lineItem.endCustomerRecords)
      ? lineItem.endCustomerRecords
      : [lineItem.endCustomerRecords];

    /* istanbul ignore else */
    if (endCustomerRecords[0]) {
      endCustomerName = this.filterEndCustomerNameFromEndCustomerRecordsBySiteId(endCustomerRecords, lineItem.selectedEndCustomerSiteId);
    }

    return endCustomerName;
  }

  public shouldShowEndCustomer(lineItem: FormGroup): boolean {
    return (
      !!lineItem.value.selectedEndCustomerSiteId &&
      lineItem.value.selectedEndCustomerName !== undefined &&
      lineItem.value.selectedEndCustomerName.length > 0 &&
      lineItem.value.validation !== QuoteCartLineItemStatus.PENDING
    );
  }

  public toggleQuoteCartPriceMatrix(i) {
    this.showTiers[i] = !this.showTiers[i];
  }

  public addCurrencySymbol(): string {
    return getCurrencySymbol(this.currencyCode, 'narrow');
  }

  public shouldDisplayCurrencySymbol(lineItem: FormControl, fieldName: string): boolean {
    return !!lineItem.value[fieldName] && parseFloat(lineItem.value[fieldName]) > 0.0;
  }

  public toggleEnableFormInputs(quoteCartItemFormGroup: FormGroup, quoteCartLineItem: IQuoteCartLineItem): void {
    if (this.shouldShowPendingMessage(quoteCartLineItem.validation)) {
      quoteCartItemFormGroup.controls['enteredCustomerPartNumber'].disable();
      quoteCartItemFormGroup.controls['requestDate'].disable();
      quoteCartItemFormGroup.controls['targetPriceFormatted'].disable();
      quoteCartItemFormGroup.controls['quantity'].disable();
    } else {
      quoteCartItemFormGroup.controls['enteredCustomerPartNumber'].enable();
      quoteCartItemFormGroup.controls['requestDate'].enable();
      quoteCartItemFormGroup.controls['targetPriceFormatted'].enable();
      quoteCartItemFormGroup.controls['quantity'].enable();
    }
  }
}
