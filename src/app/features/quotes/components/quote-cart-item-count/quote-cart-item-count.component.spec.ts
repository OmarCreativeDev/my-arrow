import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuoteCartItemCountComponent } from '@app/features/quotes/components/quote-cart-item-count/quote-cart-item-count.component';

describe('QuoteCartItemsCountComponent', () => {
  let component: QuoteCartItemCountComponent;
  let fixture: ComponentFixture<QuoteCartItemCountComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [QuoteCartItemCountComponent],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteCartItemCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
