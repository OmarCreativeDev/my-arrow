import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-quote-cart-item-count',
  templateUrl: './quote-cart-item-count.component.html',
  styleUrls: ['./quote-cart-item-count.component.scss'],
})
export class QuoteCartItemCountComponent {
  @Input() public lineItemCount: number = 0;
  @Input() public loading: boolean = false;
  constructor() {}
}
