import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { QuoteCartSummaryComponent } from './quote-cart-summary.component';
import { UpdateAdditionalInfoInStore, SubmitQuoteCart } from '@app/features/quotes/stores/quote-cart.actions';
import { QuoteCartLineItemStatus } from '@app/core/quote-cart/quote-cart.enum';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { userReducers } from '@app/core/user/store/user.reducers';
import { quoteCartReducers } from '@app/features/quotes/stores/quote-cart.reducers';
import { UserCustomerType } from '@app/core/user/user.interface';
import { SharedModule } from '@app/shared/shared.module';

class MockRouter {
  navigateByUrl(url: string) {
    return url;
  }
}

class DummyComponent {}

describe('QuoteCartSummaryComponent', () => {
  let component: QuoteCartSummaryComponent;
  let fixture: ComponentFixture<QuoteCartSummaryComponent>;
  let store: Store<any>;
  let router: Router;
  let dialogService: DialogService;
  const routes = [{ path: '/quotes/quote-cart-complete', component: DummyComponent }];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes(routes),
        SharedModule,
        StoreModule.forRoot({
          quoteCart: quoteCartReducers,
          user: userReducers,
        }),
      ],
      declarations: [QuoteCartSummaryComponent],
      providers: [{ provide: Router, useClass: MockRouter }, { provide: DialogService, useClass: DialogServiceMock }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteCartSummaryComponent);
    store = TestBed.get(Store);
    router = TestBed.get(Router);
    dialogService = TestBed.get(DialogService);
    component = fixture.componentInstance;
    spyOn(store, 'dispatch').and.callThrough();
    spyOn(router, 'navigateByUrl').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Should call the store for updating the additional info', () => {
    const mockedValue = 'Some additional Info!';
    const mockEvent = { target: { value: mockedValue } };
    component.updateAdditionalInfo(mockEvent);
    const action = new UpdateAdditionalInfoInStore(mockedValue);
    const mockedEmptyValue = '';
    const mockEmptyEvent = { target: { value: mockedEmptyValue } };
    component.updateAdditionalInfo(mockEmptyEvent);
    const emptyAction = new UpdateAdditionalInfoInStore(mockedEmptyValue);
    expect(store.dispatch).toHaveBeenCalledWith(emptyAction);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('Should call the store when submitting request for quote', () => {
    const mockedValue = {
      accountId: 1305827,
      accountNumber: 1067767,
      additionalInfo: undefined,
      cartId: '5a8701382c1724bf4003132c',
      contact: {
        customerServiceEmail: 'ggustus@arrow.com',
        customerServiceNumber: '+1 877 237 8621',
        salesRepName: "O'Brien-Meek, Patrice",
        salesRepPhoneNumber: '+1 303-824-6459',
        salesRepEmail: 'patty.obrienmeek@arrow.com',
      },
      currencyCode: 'USD',
      email: 'david.lane@oncorems.com',
      firstName: 'David',
      lastName: 'Lane',
      orgId: 241,
      phoneNumber: undefined,
      billToId: 2174374,
      shipToId: 2175315,
      region: 'arrowna',
    };

    component.additionalInfo = undefined;
    component.cartId = '5a8701382c1724bf4003132c';
    component.user = {
      organizationPartyID: 6160345,
      accountSiteID: 10125599,
      accountId: 1305827,
      accountNumber: 1067767,
      company: '',
      customerType: UserCustomerType.CEM,
      contact: {
        customerServiceEmail: 'ggustus@arrow.com',
        customerServiceNumber: '+1 877 237 8621',
        salesRepName: "O'Brien-Meek, Patrice",
        salesRepPhoneNumber: '+1 303-824-6459',
        salesRepEmail: 'patty.obrienmeek@arrow.com',
      },
      country: '',
      currencyCode: 'USD',
      currencyList: [],
      defaultLanguage: '',
      email: 'david.lane@oncorems.com',
      firstName: 'David',
      internalUserId: 0,
      isTsAndCsAccepted: true,
      lastName: 'Lane',
      orgId: 241,
      paymentsTermDescription: '',
      paymentsTermId: 0,
      paymentsTermName: '',
      permissionList: [],
      region: 'arrowna',
      registrationInstance: '',
      selectedBillTo: 2174374,
      selectedShipTo: 2175315,
      userType: 0,
      warehouses: [],
      phoneNumber: undefined,
      userPartyID: 7173135,
    };
    component.lineItems = [
      {
        itemId: 1,
        manufacturerPartNumber: '',
        selectedCustomerPartNumber: '',
        selectedEndCustomerSiteId: 1,
        requestDate: '',
        quantity: 1,
        id: '',
        total: 1,
        priceTiers: [
          {
            quantityFrom: 1,
            quantityTo: 1,
            pricePerItem: 1,
          },
        ],
        manufacturer: '',
        description: '',
        tariffApplicable: false,
      },
    ];

    component.submitQuoteCart();

    const action = new SubmitQuoteCart(mockedValue);

    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('shouldDisableSubmitButton should return true if cart is not valid or lineItemsValidationStatus is INVALID or isCartChanged', () => {
    component.lineItemsValidationStatus = QuoteCartLineItemStatus.INVALID;
    component.isCartValid = false;
    component.isCartChanged = true;
    const result = component.shouldDisableSubmitButton();
    expect(result).toBeTruthy();
  });

  it('shouldDisableSubmitButton should return true if cart is valid, isCartChanged is false and  lineItemsValidationStatus is INVALID', () => {
    component.lineItemsValidationStatus = QuoteCartLineItemStatus.INVALID;
    component.isCartValid = true;
    component.isCartChanged = false;
    const result = component.shouldDisableSubmitButton();
    expect(result).toBeTruthy();
  });

  it('shouldShowPendingMessage Should return true if lineItemsValidationStatus equals PENDING', () => {
    component.lineItemsValidationStatus = QuoteCartLineItemStatus.PENDING;
    const result = component.shouldShowPendingMessage();
    expect(result).toBeTruthy();
  });

  it('shouldShowInvalidMessage should return true if lineItemsValidationStatus equals INVALID', () => {
    component.lineItemsValidationStatus = QuoteCartLineItemStatus.INVALID;
    const result = component.shouldShowInvalidMessage();
    expect(result).toBeTruthy();
  });

  it('should open the bill to account modal', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.openBillToDialog();
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('toggleFocus should update isAdditionalInfoFocus with the provided value', () => {
    const focus = true;
    component.toggleFocus(focus);
    expect(component.isAdditionalInfoFocus).toEqual(focus);
  });
});
