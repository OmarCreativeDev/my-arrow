import { Observable, Subscription } from 'rxjs';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { IAppState } from '@app/shared/shared.interfaces';
import { UpdateAdditionalInfoInStore, SubmitQuoteCart, RequestQuote } from '@app/features/quotes/stores/quote-cart.actions';
import { getUser } from '@app/core/user/store/user.selectors';
import { getQuoteCartId } from '@app/features/quotes/stores/quote-cart.selectors';
import { IUser } from '@app/core/user/user.interface';
import { IQuoteCartLineItem } from '@app/core/quote-cart/quote-cart.interfaces';
import { QuoteCartLineItemStatus } from '@app/core/quote-cart/quote-cart.enum';
import { DialogService } from '@app/core/dialog/dialog.service';
import { BillToAccountsComponent } from '@app/shared/components/bill-to-accounts/bill-to-accounts.component';

@Component({
  selector: 'app-quote-cart-summary',
  templateUrl: './quote-cart-summary.component.html',
  styleUrls: ['./quote-cart-summary.component.scss'],
})
export class QuoteCartSummaryComponent implements OnInit, OnDestroy {
  @Input()
  public additionalInfo: string;
  @Input()
  public isCartChanged: boolean;
  @Input()
  public isCartValid: boolean;
  @Input()
  public lineItemsValidationStatus: QuoteCartLineItemStatus;
  @Input()
  public lineItems: Array<IQuoteCartLineItem>;
  public isAdditionalInfoFocus: boolean;
  public user$: Observable<IUser>;
  public user: IUser;
  public cartId$: Observable<string>;
  public cartId: string;
  public subscription: Subscription = new Subscription();

  constructor(private store: Store<IAppState>, private dialogService: DialogService) {
    this.user$ = store.pipe(select(getUser));
    this.cartId$ = store.pipe(select(getQuoteCartId));
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeUser());
    this.subscription.add(this.subscribeCartID());
  }

  private subscribeUser(): Subscription {
    return this.user$.subscribe(user => {
      this.user = user;
    });
  }

  private subscribeCartID(): Subscription {
    return this.cartId$.subscribe(cartId => {
      this.cartId = cartId;
    });
  }

  public toggleFocus(value) {
    this.isAdditionalInfoFocus = value;
  }

  public updateAdditionalInfo(event) {
    const value = event.target.value;
    this.store.dispatch(new UpdateAdditionalInfoInStore(value.length === 0 ? '' : value));
  }

  public submitQuoteCart() {
    const {
      accountId,
      accountNumber,
      contact,
      currencyCode,
      email,
      firstName,
      lastName,
      orgId,
      phoneNumber,
      selectedBillTo: billToId,
      selectedShipTo: shipToId,
      region,
    } = this.user;
    const { additionalInfo, cartId } = this;

    this.lineItems.forEach(lineItem => {
      this.store.dispatch(
        new RequestQuote(`${lineItem.manufacturerPartNumber} - Price: $${lineItem.price || 0} - Target: $${lineItem.targetPrice || 0}`)
      );
    });

    this.store.dispatch(
      new SubmitQuoteCart({
        accountId,
        accountNumber,
        additionalInfo,
        cartId,
        contact,
        currencyCode,
        email,
        firstName,
        lastName,
        orgId,
        phoneNumber,
        billToId,
        shipToId,
        region,
      })
    );
  }

  public shouldDisableSubmitButton() {
    return this.isCartChanged || !this.isCartValid || this.lineItemsValidationStatus !== QuoteCartLineItemStatus.VALID;
  }

  public shouldShowPendingMessage() {
    return this.lineItemsValidationStatus === QuoteCartLineItemStatus.PENDING;
  }

  public shouldShowInvalidMessage() {
    return this.lineItemsValidationStatus === QuoteCartLineItemStatus.INVALID;
  }

  public openBillToDialog(): void {
    this.dialogService.open(BillToAccountsComponent);
  }
}
