import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as moment from 'moment';

import { DialogService } from '@app/core/dialog/dialog.service';
import { IDateInputConfig } from '@app/shared/components/date-picker/date-picker.interfaces';
import { QuoteCartLineItemStatus } from '@app/core/quote-cart/quote-cart.enum';
import { QuoteCartDeleteDialogComponent } from '@app/features/quotes/components/quote-cart-delete-dialog/quote-cart-delete-dialog.component';
import { DatePickerOverlayActions } from '@app/shared/components/date-picker/date-picker-overlay/date-picker-overlay.interface';

@Component({
  selector: 'app-quote-cart-table-controls',
  templateUrl: './quote-cart-table-controls.component.html',
  styleUrls: ['./quote-cart-table-controls.component.scss'],
})
export class QuoteCartTableControlsComponent {
  @Input()
  isCartChanged: boolean;
  @Input()
  isCheckboxSelected: boolean;
  @Input()
  lineItemsValidationStatus: QuoteCartLineItemStatus;

  @Output()
  public updateCartItems = new EventEmitter<any>();
  @Output()
  public requestDateChange = new EventEmitter<any>();
  @Output()
  public deleteCartItems = new EventEmitter<any>();

  public requestDatePickerDates = [undefined];

  public minDate = moment(new Date())
    .startOf('day')
    .toDate();

  public multipleRequestDateConfiguration: IDateInputConfig[] = [
    {
      placeholder: 'Change Selected Dates',
    },
  ];
  public datePickerOverlayActions: Array<DatePickerOverlayActions> = [
    DatePickerOverlayActions.GO_TO_TODAY,
    DatePickerOverlayActions.SUBMIT,
  ];

  constructor(private dialogService: DialogService) {}

  public onUpdateClick() {
    this.updateCartItems.emit();
  }

  public changeLineItemsDate(dates: Array<string>) {
    this.requestDateChange.emit(dates[0]);
  }

  public onDeleteClick(): void {
    const quoteCartDeleteDialog = this.dialogService.open(QuoteCartDeleteDialogComponent, {
      size: 'large',
    });
    quoteCartDeleteDialog.afterClosed.subscribe(willDelete => {
      if (willDelete) {
        this.deleteCartItems.emit();
      }
    });
  }

  public shouldDisableUpdateControl() {
    return !this.isCartChanged || this.lineItemsValidationStatus === QuoteCartLineItemStatus.PENDING;
  }

  public shouldDisableSelectedCheckboxControl() {
    return !this.isCheckboxSelected || this.lineItemsValidationStatus === QuoteCartLineItemStatus.PENDING;
  }
}
