import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NgModule } from '@angular/core';
import { Store, StoreModule, combineReducers } from '@ngrx/store';

import { SharedModule } from '@app/shared/shared.module';
import { QuoteCartTableControlsComponent } from './quote-cart-table-controls.component';
import { DateService } from '@app/shared/services/date.service';
import { OverlayContainer } from '@app/core/dialog/overlay-container';
import { DialogService, Dialog } from '@app/core/dialog/dialog.service';
import { QuoteCartDeleteDialogComponent } from '@app/features/quotes/components/quote-cart-delete-dialog/quote-cart-delete-dialog.component';
import { userReducers } from '@app/core/user/store/user.reducers';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { QuoteCartLineItemStatus } from '@app/core/quote-cart/quote-cart.enum';

@NgModule({
  imports: [
    SharedModule,
    StoreModule.forRoot({
      user: combineReducers(userReducers),
    }),
  ],
  providers: [Store, { provide: Dialog, useClass: DialogMock }],
  declarations: [QuoteCartDeleteDialogComponent],
  entryComponents: [QuoteCartDeleteDialogComponent],
})
class TestModule {}

describe('QuoteCartTableControlsComponent', () => {
  let component: QuoteCartTableControlsComponent;
  let fixture: ComponentFixture<QuoteCartTableControlsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, HttpClientTestingModule, TestModule],
      declarations: [QuoteCartTableControlsComponent],
      providers: [DateService, DialogService, OverlayContainer],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteCartTableControlsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#changeLineItemsDate', () => {
    it('after changeLineItemsDate should execute emit event', () => {
      spyOn(component.requestDateChange, 'emit');

      const requestDate = '2099-12-12';
      component.changeLineItemsDate([requestDate]);

      fixture.detectChanges();

      expect(component.requestDateChange.emit).toHaveBeenCalled();
    });
  });

  describe('#updateLineItems', () => {
    it('after updateCartItems should execute emit event', () => {
      spyOn(component.updateCartItems, 'emit');
      component.onUpdateClick();
      fixture.detectChanges();
      expect(component.updateCartItems.emit).toHaveBeenCalled();
    });
  });

  describe('#deleteLineItems', () => {
    it('after deleteCartItems should not execute emit event if willDelete is false', () => {
      spyOn(component.deleteCartItems, 'emit');
      component.onDeleteClick();
      fixture.detectChanges();
      expect(component.deleteCartItems.emit).toHaveBeenCalledTimes(0);
    });
  });

  it('shouldDisableUpdateControl should return true if cart hasnt changed or validation is pending', () => {
    component.isCartChanged = false;
    component.lineItemsValidationStatus = QuoteCartLineItemStatus.PENDING;

    const result = component.shouldDisableUpdateControl();
    expect(result).toEqual(true);
  });

  it('shouldDisableUpdateControl should return false if cart has changed or validation is not pending', () => {
    component.isCartChanged = true;
    component.lineItemsValidationStatus = QuoteCartLineItemStatus.INVALID;

    const result = component.shouldDisableUpdateControl();
    expect(result).toEqual(false);
  });

  it('shouldDisableSelectedCheckboxControl should return true if checkbox hasnt been selected or validation is pending', () => {
    component.isCheckboxSelected = false;
    component.lineItemsValidationStatus = QuoteCartLineItemStatus.PENDING;

    const result = component.shouldDisableSelectedCheckboxControl();
    expect(result).toEqual(true);
  });

  it('shouldDisableSelectedCheckboxControl should return true if if checkbox has been selected or validation is not pending', () => {
    component.isCheckboxSelected = true;
    component.lineItemsValidationStatus = QuoteCartLineItemStatus.INVALID;

    const result = component.shouldDisableSelectedCheckboxControl();
    expect(result).toEqual(false);
  });
});
