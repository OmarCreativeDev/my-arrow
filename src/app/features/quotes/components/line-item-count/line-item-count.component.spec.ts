import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineItemCountComponent } from '@app/features/quotes/components/line-item-count/line-item-count.component';

describe('QuotesLineItemCountComponent', () => {
  let component: LineItemCountComponent;
  let fixture: ComponentFixture<LineItemCountComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [],
        declarations: [LineItemCountComponent],
        providers: [],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(LineItemCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
