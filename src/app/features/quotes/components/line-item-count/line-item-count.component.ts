import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-line-item-count',
  templateUrl: './line-item-count.component.html',
})
export class LineItemCountComponent {
  @Input() public lineItemCount: number = 0;
  constructor() {}
}
