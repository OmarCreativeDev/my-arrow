import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuoteCartSubmitErrorComponent } from './quote-cart-submit-error.component';
import { Dialog } from '@app/core/dialog/dialog.service';
import { SharedModule } from '@app/shared/shared.module';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { Store, StoreModule, combineReducers } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('QuoteCartSubmitErrorComponent', () => {
  let component: QuoteCartSubmitErrorComponent;
  let fixture: ComponentFixture<QuoteCartSubmitErrorComponent>;
  let store: Store<any>;
  let dialog: Dialog<QuoteCartSubmitErrorComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [QuoteCartSubmitErrorComponent],
        imports: [
          SharedModule,
          HttpClientTestingModule,
          StoreModule.forRoot({
            user: combineReducers(userReducers),
          }),
        ],
        providers: [{ provide: Dialog, useClass: DialogMock }],
      }).compileComponents();

      dialog = TestBed.get(Dialog);
      store = TestBed.get(Store);
      spyOn(store, 'dispatch').and.callThrough();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(QuoteCartSubmitErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the dialog when Cancel button is pressed', () => {
    spyOn(dialog, 'close');
    component.onDismiss();
    expect(dialog.close).toHaveBeenCalled();
  });
});
