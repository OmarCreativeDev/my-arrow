import { Dialog } from '@app/core/dialog/dialog.service';
import { Component } from '@angular/core';

@Component({
  selector: 'app-quote-cart-submit-error',
  templateUrl: './quote-cart-submit-error.component.html',
  styleUrls: ['./quote-cart-submit-error.component.scss'],
})
export class QuoteCartSubmitErrorComponent {
  constructor(private dialog: Dialog<QuoteCartSubmitErrorComponent>) {}

  onDismiss(): void {
    this.dialog.close();
  }
}
