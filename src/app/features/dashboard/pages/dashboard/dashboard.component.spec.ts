import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { StoreModule, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { of, throwError } from 'rxjs';

import { ApiService } from '@app/core/api/api.service';
import { BomsService } from '@app/core/boms/boms.service';
import { CoreModule } from '@app/core/core.module';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { IOrderLineQueryResponse } from '@app/core/orders/orders.interfaces';
import { OrdersService } from '@app/core/orders/orders.service';
import { UserService } from '@app/core/user/user.service';
import { IHttpException, IOrderLineStatus, IPurchaseOrderStatus } from '@app/shared/shared.interfaces';

import { DashboardComponent } from './dashboard.component';
import { HttpErrorResponse } from '@angular/common/http';
import { GetBomListing } from '@app/core/boms/store/boms.actions';
import { mockBomResponse } from '@app/core/boms/boms.service.spec';
import { CustomerBroadcastService } from '@app/core/customer-broadcast/customer-broadcast.service';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';
import { propertiesReducers } from '@app/features/properties/store/properties.reducers';
import { mockPrivateProperties } from '@app/core/features/feature-flags.mock';
import { PropertiesModule } from '@app/features/properties/properties.module';
import { PropertiesEffects } from '@app/features/properties/store/properties.effects';
import { bomReducers } from '@app/core/boms/store/boms.reducers';
import { googleAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.custom-events';

describe('DashboardPageComponent', () => {
  let ordersService: OrdersService;
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let dialogService: DialogService;
  let store: Store<any>;

  const orderLinesSearchResultsMock: IOrderLineQueryResponse = {
    orderLines: [
      {
        id: 1,
        purchaseOrderNumber: 'PO234098',
        lineItem: '1.2.1',
        customerPartNumber: 'BAV99',
        manufacturerPartNumber: 'SMC8903124',
        qtyReadyToShip: 1,
        qtyShipped: 2,
        qtyOrdered: 3,
        qtyRemaining: 850,
        requested: new Date('2018-07-21'),
        entered: new Date('2018-07-24'),
        committed: new Date('2018-07-30'),
        status: IOrderLineStatus.Open,
        buyerName: 'Tracie D.',
        manufacturerName: 'Vishay',
        unitResale: 0,
        extResale: 1745,
        salesOrderId: '5952303',
        salesHeaderId: 7453868,
        billToSiteUseId: 1234,
        purchaseOrderStatus: IPurchaseOrderStatus.Open,
        currencyCode: 'USD',
        docId: '12124124',
        itemId: 1234,
        warehouseId: 555,
        invoices: [],
        shipments: [],
      },
    ],
    paging: {
      currentPage: 1,
      totalPages: 1,
    },
  };

  const mockPropertiesReducers = () => ({
    ...propertiesReducers,
    private: mockPrivateProperties,
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        CoreModule,
        RouterTestingModule,
        PropertiesModule,
        StoreModule.forRoot({ googleAnalytics: googleAnalyticsMetaReducers, properties: mockPropertiesReducers, boms: bomReducers }),
        EffectsModule.forRoot([PropertiesEffects]),
      ],
      declarations: [DashboardComponent],
      providers: [
        ApiService,
        OrdersService,
        BomsService,
        UserService,
        CustomerBroadcastService,
        { provide: WSSService, useClass: MockStompWSSService },
        { provide: DialogService, useClass: DialogServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    ordersService = TestBed.get(OrdersService);
    dialogService = TestBed.get(DialogService);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('Recent Orders', () => {
    it('should return an array of recent orders when calling getRecentOrders()', fakeAsync(() => {
      spyOn(ordersService, 'orderLinesSearch').and.returnValue(of(orderLinesSearchResultsMock));
      component.getRecentOrders();
      expect(component.recentOrders.error).toBeUndefined();
      expect(component.recentOrders.loading).toBeFalsy();
      expect(component.recentOrders.items).toEqual(orderLinesSearchResultsMock.orderLines);
    }));

    it('should capture order error from service when getting recent orders', fakeAsync(() => {
      const apiFailureMessage: string = '500 error';
      spyOn(ordersService, 'orderLinesSearch').and.returnValue(
        throwError({
          error: apiFailureMessage,
        } as IHttpException)
      );
      component.getRecentOrders();
      expect(component.recentOrders.items).toBeUndefined();
      expect(component.recentOrders.loading).toBeFalsy();
      expect(component.recentOrders.error.error).toEqual(apiFailureMessage);
    }));
  });

  describe('Open Orders', () => {
    it('should return an array of open orders when calling getOpenOrders()', fakeAsync(() => {
      spyOn(ordersService, 'orderLinesSearch').and.returnValue(of(orderLinesSearchResultsMock));
      component.getOpenOrders();
      expect(component.openOrders.error).toBeUndefined();
      expect(component.openOrders.loading).toBeFalsy();
      expect(component.openOrders.items).toEqual(orderLinesSearchResultsMock.orderLines);
    }));

    it('should capture order error from service when getting open orders', fakeAsync(() => {
      const apiFailureMessage: string = '500 error';
      spyOn(ordersService, 'orderLinesSearch').and.returnValue(
        throwError({
          error: apiFailureMessage,
        } as IHttpException)
      );
      component.getOpenOrders();
      expect(component.openOrders.items).toBeUndefined();
      expect(component.openOrders.loading).toBeFalsy();
      expect(component.openOrders.error.error).toEqual(apiFailureMessage);
    }));
  });

  describe('Shipped Orders', () => {
    it('should return an array of shipped orders when calling getShippedOrders()', fakeAsync(() => {
      spyOn(ordersService, 'orderLinesSearch').and.returnValue(of(orderLinesSearchResultsMock));
      component.getShippedOrders();
      expect(component.shippedOrders.error).toBeUndefined();
      expect(component.shippedOrders.loading).toBeFalsy();
      expect(component.shippedOrders.items).toEqual(orderLinesSearchResultsMock.orderLines);
    }));

    it('should capture order error from service when getting shipped orders', fakeAsync(() => {
      const apiFailureMessage: string = '500 error';
      spyOn(ordersService, 'orderLinesSearch').and.returnValue(
        throwError({
          error: apiFailureMessage,
        } as IHttpException)
      );
      component.getShippedOrders();
      expect(component.shippedOrders.items).toBeUndefined();
      expect(component.shippedOrders.loading).toBeFalsy();
      expect(component.shippedOrders.error.error).toEqual(apiFailureMessage);
    }));
  });

  it('should return an array of boms when calling getBoms()', fakeAsync(() => {
    spyOn(store, 'pipe').and.returnValue(
      of({
        items: mockBomResponse.payload,
        error: undefined,
        loading: false,
      })
    );
    component.getBoms();
    expect(store.dispatch).toHaveBeenCalledWith(new GetBomListing());
    component.boms$.subscribe(boms => {
      expect(boms.error).toBeUndefined();
      expect(boms.loading).toBeFalsy();
      expect(boms.items.length).toEqual(1);
    });
  }));

  it('should capture BOMs error from service when getting boms', fakeAsync(() => {
    const error = new HttpErrorResponse({ status: 500 });
    spyOn(store, 'pipe').and.returnValue(
      of({
        items: undefined,
        error: error,
        loading: false,
      })
    );
    component.getBoms();
    component.boms$.subscribe(boms => {
      expect(boms.items).toBeUndefined();
      expect(boms.loading).toBeFalsy();
      expect(boms.error).toEqual(error);
    });
  }));

  it('should use the dialog service to open a POUpload dialog', () => {
    spyOn(dialogService, 'open');
    component.onPoUpload();
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should set localStorage when calling setFilter', () => {
    component.setFilter(0);
    expect(localStorage.getItem('selectedOrderFilter')).toEqual('0');
  });
});
