import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { IBom } from '@app/core/boms/boms.interface';
import { DialogService } from '@app/core/dialog/dialog.service';
import { IOrderLineQueryRequest, IOrderLineQueryResponse } from '@app/core/orders/orders.interfaces';
import { OrdersService } from '@app/core/orders/orders.service';
import { IUser } from '@app/core/user/user.interface';
import { UserService } from '@app/core/user/user.service';
import { UploadPurchaseOrderComponent } from '@app/features/dashboard/components/upload-purchase-order/upload-purchase-order.component';
import {
  IFilterCriteronOperatorEnum,
  IListingState,
  IOrderLine,
  IOrderLineStatus,
  ISelectableOption,
  ISortCriteronOrderEnum,
  IAppState,
} from '@app/shared/shared.interfaces';
import * as moment from 'moment';
import { GetBomListing } from '@app/core/boms/store/boms.actions';
import { getBomListing } from '@app/core/boms/store/boms.selectors';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

/* tslint:disable:no-unused-variable */
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit, OnDestroy {
  public dashboardOrderTabs: ISelectableOption<any>[] = [
    {
      label: 'Recent',
      value: 0,
    },
    {
      label: 'Open',
      value: 1,
    },
    {
      label: 'Shipped',
      value: 2,
    },
    {
      label: 'Delayed',
      value: 3,
    },
  ];

  public dashboardBomTabs: ISelectableOption<any>[] = [
    {
      // TODO: Comment back in when BOMS module is available. MYAR20-4133
      //label: 'Recent',
      // TODO: Remove when BOMS module is available. MYAR20-4133
      label: 'Saved BOMs',
      value: 0,
    },
  ];

  public recentOrders: IListingState<IOrderLine> = {
    loading: true,
    error: null,
    items: [],
  };

  public openOrders: IListingState<IOrderLine> = {
    loading: true,
    error: null,
    items: [],
  };

  public shippedOrders: IListingState<IOrderLine> = {
    loading: true,
    error: null,
    items: [],
  };

  public delayedOrders: IListingState<IOrderLine> = {
    loading: true,
    error: null,
    items: [],
  };

  public canUpload: boolean = false;
  public boms$: Observable<IListingState<IBom>>;
  public privateFeatureFlags$: Observable<any>;
  public linkTitle: string = 'View All Orders';
  public showDelayed: boolean = false;
  public subscription: Subscription = new Subscription();

  constructor(
    private ordersService: OrdersService,
    private dialogService: DialogService,
    private userService: UserService,
    private store: Store<IAppState>
  ) {
    this.privateFeatureFlags$ = this.store.pipe(select(getPrivateFeatureFlagsSelector));
  }

  ngOnInit() {
    this.getRecentOrders();
    this.getOpenOrders();
    this.getShippedOrders();
    this.getDelayedOrders();
    this.getBoms();
    this.getUserPerms();
    this.subscription.add(this.getPrivateFeatureFlags());
  }

  getPrivateFeatureFlags(): Subscription {
    return this.privateFeatureFlags$.subscribe(featureFlags => {
      if (featureFlags.ordersExceptions === false) {
        this.dashboardOrderTabs = this.dashboardOrderTabs.slice(0, 3);
      }
    });
  }

  public getRecentOrders(): void {
    const recentSearch: IOrderLineQueryRequest = {
      search: {
        type: '',
        value: '',
      },
      filter: [
        {
          criteria: [
            {
              key: 'status',
              value: IOrderLineStatus.Open,
              operator: IFilterCriteronOperatorEnum.Equals,
            },
          ],
        },
        {
          criteria: [
            {
              key: 'shipmentTrackingDate',
              value: moment()
                .startOf('day')
                .format(),
              operator: IFilterCriteronOperatorEnum.LessThanOrEqualTo,
            },
            {
              key: 'shipmentTrackingDate',
              value: moment()
                .startOf('day')
                .subtract(7, 'days')
                .format(),
              operator: IFilterCriteronOperatorEnum.GreaterThanOrEqualTo,
            },
          ],
        },
      ],
      sort: [
        {
          key: 'entered',
          order: ISortCriteronOrderEnum.DESC,
        },
        {
          key: 'salesOrderId',
          order: ISortCriteronOrderEnum.DESC,
        },
        {
          key: 'lineItem',
          order: ISortCriteronOrderEnum.DESC,
        },
      ],
      paging: {
        limit: 5,
        page: 1,
      },
    };

    this.ordersService.orderLinesSearch(recentSearch).subscribe(
      (data: IOrderLineQueryResponse) => {
        this.recentOrders = {
          loading: false,
          error: undefined,
          items: data.orderLines,
        };
      },
      (errorResponse: HttpErrorResponse) => {
        this.recentOrders = {
          loading: false,
          error: errorResponse,
          items: undefined,
        };
      }
    );
  }

  public getOpenOrders(): void {
    const openSearch: IOrderLineQueryRequest = {
      search: {
        type: '',
        value: '',
      },
      filter: [
        {
          criteria: [
            {
              key: 'status',
              value: IOrderLineStatus.Open,
              operator: IFilterCriteronOperatorEnum.Equals,
            },
          ],
        },
      ],
      sort: [
        {
          key: 'entered',
          order: ISortCriteronOrderEnum.DESC,
        },
        {
          key: 'salesOrderId',
          order: ISortCriteronOrderEnum.DESC,
        },
        {
          key: 'lineItem',
          order: ISortCriteronOrderEnum.DESC,
        },
      ],
      paging: {
        limit: 5,
        page: 1,
      },
    };

    this.ordersService.orderLinesSearch(openSearch).subscribe(
      (data: IOrderLineQueryResponse) => {
        this.openOrders = {
          loading: false,
          error: undefined,
          items: data.orderLines,
        };
      },
      (errorResponse: HttpErrorResponse) => {
        this.openOrders = {
          loading: false,
          error: errorResponse,
          items: undefined,
        };
      }
    );
  }

  public getShippedOrders(): void {
    const shippedSearch: IOrderLineQueryRequest = {
      search: {
        type: '',
        value: '',
      },
      filter: [
        {
          criteria: [
            {
              key: 'status',
              value: IOrderLineStatus.Shipped,
              operator: IFilterCriteronOperatorEnum.Equals,
            },
            {
              key: 'shipmentTrackingDate',
              value: moment()
                .startOf('day')
                .format(),
              operator: IFilterCriteronOperatorEnum.LessThanOrEqualTo,
            },
          ],
        },
      ],
      sort: [
        {
          key: 'shipmentTrackingDate',
          order: ISortCriteronOrderEnum.DESC,
        },
        {
          key: 'salesOrderId',
          order: ISortCriteronOrderEnum.DESC,
        },
        {
          key: 'lineItem',
          order: ISortCriteronOrderEnum.DESC,
        },
      ],
      paging: {
        limit: 5,
        page: 1,
      },
    };

    this.ordersService.orderLinesSearch(shippedSearch).subscribe(
      (data: IOrderLineQueryResponse) => {
        this.shippedOrders = {
          loading: false,
          error: undefined,
          items: data.orderLines,
        };
      },
      (errorResponse: HttpErrorResponse) => {
        this.shippedOrders = {
          loading: false,
          error: errorResponse,
          items: undefined,
        };
      }
    );
  }

  public getDelayedOrders(): void {
    const delayedSearch: IOrderLineQueryRequest = {
      search: {
        type: '',
        value: 'purchaseOrderNumber',
      },
      filter: [
        {
          criteria: [
            {
              key: 'businessCriteriaFilter',
              value: 'delayed',
              operator: IFilterCriteronOperatorEnum.Equals,
            },
          ],
        },
      ],
      sort: [
        {
          key: 'purchaseOrderNumber',
          order: ISortCriteronOrderEnum.DESC,
        },
        {
          key: 'lineItem',
          order: ISortCriteronOrderEnum.ASC,
        },
      ],
      paging: {
        limit: 5,
        page: 1,
      },
    };

    this.ordersService.orderLinesSearch(delayedSearch).subscribe(
      (data: IOrderLineQueryResponse) => {
        this.delayedOrders = {
          loading: false,
          error: undefined,
          items: data.orderLines,
        };
      },
      (errorResponse: HttpErrorResponse) => {
        this.delayedOrders = {
          loading: false,
          error: errorResponse,
          items: undefined,
        };
      }
    );
  }

  public getBoms(): void {
    this.boms$ = this.store.pipe(select(getBomListing));
    this.store.dispatch(new GetBomListing());
  }

  public onPoUpload(): void {
    this.dialogService.open(UploadPurchaseOrderComponent, { size: 'medium' });
  }

  public getUserPerms() {
    this.userService.getUser().subscribe((user: IUser) => {
      this.canUpload = user.permissionList.includes('POUpload') || user.permissionList.includes('PO Upload Conexiom') ? true : false;
    });
  }

  public setFilter(tab): void {
    let value: string;
    switch (tab.value) {
      case 0:
        this.linkTitle = 'View All Orders';
        value = '0';
        break;
      case 1:
        this.linkTitle = 'View All Open';
        value = '4';
        break;
      case 2:
        this.linkTitle = 'View All Shipped';
        value = '1';
        break;
      case 3:
        this.linkTitle = 'View All Delays';
        value = '8';
        break;
      default:
        this.linkTitle = 'View All Orders';
        value = '0';
    }

    localStorage.setItem('selectedOrderFilter', value);
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
