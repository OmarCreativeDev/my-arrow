import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, catchError, switchMap } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';

import {
  DashboardActionTypes,
  POUpload,
  POUploadSuccess,
  POUploadFailed,
  SendPOEmail,
  SendPOEmailSuccess,
  SendPOEmailFailed,
} from '@app/features/dashboard/stores/dashboard.actions';

import { PurchaseOrderService } from '@app/core/purchase-order/purchase-order.service';
import { EmailService } from '@app/core/email/email.service';
import { IAppState } from '@app/shared/shared.interfaces';
import { getDashboardPOUploadNumber } from '@app/features/dashboard/stores/dashboard.selectors';

@Injectable()
export class DashboardEffects {
  poNumber: string;

  constructor(
    private actions$: Actions,
    private poService: PurchaseOrderService,
    private emailService: EmailService,
    private store: Store<IAppState>
  ) {
    const poNumber$ = this.store.pipe(select(getDashboardPOUploadNumber));
    poNumber$.subscribe(poNumber => {
      this.poNumber = poNumber;
    });
  }

  @Effect()
  SendPOUpload$ = this.actions$.pipe(
    ofType(DashboardActionTypes.PO_UPLOAD),
    switchMap((action: POUpload) => {
      return this.poService.uploadPO(action.payload).pipe(
        map((data: any) => new POUploadSuccess(this.poNumber)),
        catchError(err => of(new POUploadFailed(err)))
      );
    })
  );

  @Effect()
  SendPOEmail$ = this.actions$.pipe(
    ofType(DashboardActionTypes.SEND_PO_EMAIL),
    switchMap((action: SendPOEmail) => {
      return this.emailService.sendEmail(action.payload).pipe(
        map((data: any) => new SendPOEmailSuccess()),
        catchError(err => of(new SendPOEmailFailed(err)))
      );
    })
  );
}
