import {
  POUpload,
  POUploadSuccess,
  POUploadFailed,
  SendPOEmail,
  SendPOEmailSuccess,
  SendPOEmailFailed,
} from '@app/features/dashboard/stores/dashboard.actions';
import { IDashboardState } from '@app/features/dashboard/stores/dashboard.interfaces';
import { dashboardReducers, INITIAL_DASHBOARD_STATE } from '@app/features/dashboard/stores/dashboard.reducers';
import { IEmail } from '@app/core/email/email.interfaces';
import { HttpErrorResponse } from '@angular/common/http';

describe('Dashboard Reducer', () => {
  let initialState: IDashboardState;

  beforeEach(() => {
    initialState = INITIAL_DASHBOARD_STATE;
  });

  it('`PO_UPLOAD` should set `loading` to true and not have an error property', () => {
    const formData: FormData = new FormData();
    const action = new POUpload(formData);
    const result = dashboardReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.POUploadError).toBeUndefined();
  });

  it('`PO_UPLOAD_SUCCESS` should set `POUploadSuccess`', () => {
    const action = new POUploadSuccess('poUpload1234');
    const result = <IDashboardState>dashboardReducers(initialState, action);
    expect(result.POUploadSuccess).toBeDefined();
  });

  it(`PO_UPLOAD_FAILED error property should have an error property`, () => {
    const error = new HttpErrorResponse({ status: 500 });
    const action = new POUploadFailed(error);
    const result = dashboardReducers(initialState, action);
    expect(result.POUploadError).toBeDefined();
  });

  it('`SEND_PO_EMAIL` should set `loading` to true and not have an error property', () => {
    const email: IEmail = {
      from: 'purchaseorder@arrow.com',
      to: ['test@arrow.com'],
      subject: `Your upload Purchase Order: 1234`,
      content: 'Test Email',
    };

    const action = new SendPOEmail(email);
    const result = dashboardReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.POUploadEmailError).toBeUndefined();
  });

  it('`SEND_PO_EMAIL_SUCCESS` should set `emailSent`', () => {
    const action = new SendPOEmailSuccess();
    const result = <IDashboardState>dashboardReducers(initialState, action);
    expect(result.POUploadEmailSent).toBeDefined();
  });

  it(`SEND_PO_EMAIL_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new SendPOEmailFailed(error);
    const result = dashboardReducers(initialState, action);
    expect(result.POUploadEmailError).toBeDefined();
  });
});
