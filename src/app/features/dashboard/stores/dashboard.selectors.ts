import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IDashboardState } from '@app/features/dashboard/stores/dashboard.interfaces';

/**
 * Selects dashboard state from the root state object
 */
export const getDashboardState = createFeatureSelector<IDashboardState>('dashboard');

export const getDashboardPOUploadSuccessSelector = createSelector(getDashboardState, dashboardState => dashboardState.POUploadSuccess);

export const getDashboardPOUploadErrorSelector = createSelector(getDashboardState, dashboardState => dashboardState.POUploadError);

export const getDashboardPOUploadNumber = createSelector(getDashboardState, dashboardState => dashboardState.PONumber);
