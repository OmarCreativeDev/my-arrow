import { DashboardActionTypes, DashboardActions } from '@app/features/dashboard/stores/dashboard.actions';
import { IDashboardState } from '@app/features/dashboard/stores/dashboard.interfaces';

export const INITIAL_DASHBOARD_STATE: any = {
  loading: false,
  PONumber: undefined,
  error: undefined,
  POUploadError: undefined,
  POUploadSuccess: false,
  POUploadEmailError: undefined,
  POUploadEmailSent: false,
};

/**
 * Mutates the state for given set of notifications
 * @param state
 * @param action
 */
export function dashboardReducers(state: IDashboardState = INITIAL_DASHBOARD_STATE, action: DashboardActions) {
  switch (action.type) {
    case DashboardActionTypes.PO_UPLOAD: {
      return {
        ...state,
        loading: true,
        POUploadError: undefined,
        POUploadSuccess: false,
      };
    }
    case DashboardActionTypes.PO_UPLOAD_SUCCESS: {
      return {
        ...state,
        loading: false,
        POUploadError: undefined,
        POUploadSuccess: true,
      };
    }
    case DashboardActionTypes.PO_UPLOAD_FAILED: {
      return {
        ...state,
        loading: false,
        POUploadError: action.payload,
        POUploadSuccess: false,
      };
    }
    case DashboardActionTypes.PO_UPLOAD_RESET: {
      return {
        ...state,
        loading: false,
        POUploadError: undefined,
        POUploadSuccess: undefined,
      };
    }
    case DashboardActionTypes.SEND_PO_EMAIL: {
      return {
        ...state,
        loading: true,
        POUploadEmailError: undefined,
      };
    }
    case DashboardActionTypes.SEND_PO_EMAIL_SUCCESS: {
      return {
        ...state,
        loading: false,
        POUploadEmailError: undefined,
        POUploadEmailSent: true,
      };
    }
    case DashboardActionTypes.SEND_PO_EMAIL_FAILED: {
      return {
        ...state,
        loading: false,
        POUploadEmailError: action.payload,
      };
    }
    case DashboardActionTypes.PO_UPLOAD_SET_FILENAME: {
      return {
        ...state,
        PONumber: action.payload,
      };
    }
    default: {
      return state;
    }
  }
}
