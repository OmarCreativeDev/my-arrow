import { Action } from '@ngrx/store';
import { IEmail } from '@app/core/email/email.interfaces';
import { HttpErrorResponse } from '@angular/common/http';

export enum DashboardActionTypes {
  PO_UPLOAD = 'PO_UPLOAD',
  PO_UPLOAD_SUCCESS = 'PO_UPLOAD_SUCCESS',
  PO_UPLOAD_FAILED = 'PO_UPLOAD_FAILED',
  PO_UPLOAD_RESET = 'PO_UPLOAD_RESET',
  SEND_PO_EMAIL = 'SEND_PO_EMAIL',
  SEND_PO_EMAIL_SUCCESS = 'SEND_PO_EMAIL_SUCCESS',
  SEND_PO_EMAIL_FAILED = 'SEND_PO_EMAIL_FAILED',
  PO_UPLOAD_SET_FILENAME = 'PO_UPLOAD_SET_FILENAME',
}

/**
 * * Upload PO
 */
export class POUpload implements Action {
  readonly type = DashboardActionTypes.PO_UPLOAD;
  constructor(public payload: FormData) {}
}

/**
 * * Upload PO Success
 */
export class POUploadSuccess implements Action {
  readonly type = DashboardActionTypes.PO_UPLOAD_SUCCESS;
  constructor(public payload: string) {}
}

/**
 * * Upload PO Failed
 */
export class POUploadFailed implements Action {
  readonly type = DashboardActionTypes.PO_UPLOAD_FAILED;
  constructor(public payload: HttpErrorResponse) {}
}

/**
 * * Upload PO
 */
export class POUploadSetPoNumber implements Action {
  readonly type = DashboardActionTypes.PO_UPLOAD_SET_FILENAME;
  constructor(public payload: string) {}
}

/**
 * * Upload PO Reset
 */
export class POUploadReset implements Action {
  readonly type = DashboardActionTypes.PO_UPLOAD_RESET;
  constructor() {}
}

/**
 * * Send confimation email
 */
export class SendPOEmail implements Action {
  readonly type = DashboardActionTypes.SEND_PO_EMAIL;
  constructor(public payload: IEmail) {}
}

/**
 * * Send confimation email Success
 */
export class SendPOEmailSuccess implements Action {
  readonly type = DashboardActionTypes.SEND_PO_EMAIL_SUCCESS;
  constructor() {}
}

/**
 * * Send confimation email Failed
 */
export class SendPOEmailFailed implements Action {
  readonly type = DashboardActionTypes.SEND_PO_EMAIL_FAILED;
  constructor(public payload: Error) {}
}

export type DashboardActions =
  | POUpload
  | POUploadSuccess
  | POUploadFailed
  | POUploadReset
  | SendPOEmail
  | SendPOEmailSuccess
  | SendPOEmailFailed
  | POUploadSetPoNumber;
