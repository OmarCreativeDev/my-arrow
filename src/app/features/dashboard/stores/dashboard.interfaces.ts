export interface IDashboardState {
  loading: boolean;
  error?: object;
  POUploadError?: object;
  POUploadSuccess?: boolean;
  POUploadEmailError?: object;
  POUploadEmailSent: boolean;
  PONumber: string;
}
