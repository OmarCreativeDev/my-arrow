import { DatePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { IsoDatePipe } from '@app/shared/pipes/iso-date/iso-date.pipe';
import { DateService } from '@app/shared/services/date.service';
import { IOrderLine, IOrderLineStatus, IPurchaseOrderStatus } from '@app/shared/shared.interfaces';

import { OrderLinesOverviewComponent } from './order-lines-overview.component';

class DummyComponent {}

const mockOrderLine = {
  buyerName: 'Tracie D.',
  carrier: 'FEDEX',
  committed: new Date('2018-07-30'),
  customerPartNumber: 'BAV99',
  entered: new Date('2018-07-24'),
  extResale: 1745,
  id: 1,
  invoiced: new Date('2018-01-21'),
  lineItem: '1.2.1',
  manufacturerName: 'Vishay',
  manufacturerPartNumber: 'SMC8903124',
  purchaseOrderNumber: 'PO234098',
  qtyOrdered: 3,
  qtyReadyToShip: 1,
  qtyRemaining: 850,
  qtyShipped: 2,
  requested: new Date('2018-07-21'),
  salesOrderId: '5952303',
  salesHeaderId: 7453868,
  billToSiteUseId: 1234,
  shipmentTrackingDate: new Date('2018-01-01'),
  shipmentTrackingReference: 'UPS190321-GB',
  status: <IOrderLineStatus>'OPEN',
  unitResale: 0,
  shipmentTrackingUrl: 'http://www.fedex.com/123',
  invoices: [],
  shipments: [],
  purchaseOrderStatus: IPurchaseOrderStatus.Open,
  docId: 'abc',
  itemId: 12345,
  warehouseId: 555,
};

describe('OrderLinesOverviewComponent', () => {
  let component: OrderLinesOverviewComponent;
  let fixture: ComponentFixture<OrderLinesOverviewComponent>;
  let router: Router;

  const routes = [{ path: 'orders/:orderId', component: DummyComponent }];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      declarations: [OrderLinesOverviewComponent, IsoDatePipe],
      providers: [DatePipe, DateService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderLinesOverviewComponent);
    router = TestBed.get(Router);
    component = fixture.componentInstance;
    component.orderLines = [mockOrderLine] as Array<IOrderLine>;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`reducedOrderLines` should return an array with a maximum size of the amount of rows to display', () => {
    component.rows = 5;
    component.orderLines = Array(10).fill(mockOrderLine);
    expect(component.reducedOrderLines.length).toEqual(5);

    component.rows = 5;
    component.orderLines = Array(3).fill(mockOrderLine);
    expect(component.reducedOrderLines.length).toEqual(3);

    component.rows = 5;
    component.orderLines = Array(0).fill(mockOrderLine);
    expect(component.reducedOrderLines.length).toEqual(0);
  });

  it('`emptyRows` should provide an array of size equal to the number of rows minus the size of the orders array', () => {
    component.orderLines = Array(5).fill(mockOrderLine);

    component.rows = 10;
    expect(component.emptyRows.length).toEqual(5);

    component.rows = 5;
    expect(component.emptyRows.length).toEqual(0);
  });

  it('should should naviagate to the order when clicking on the order row', () => {
    const spy = spyOn(router, 'navigateByUrl');
    fixture.nativeElement.querySelector('.orderline-overview').click();
    const url = spy.calls.first().args[0];
    expect(url).toBe('/orders/5952303');
  });

  it('should display shipped shipmentTrackingDate if displayShippedDate is set to true', () => {
    component.displayShippedDate = true;
    expect(component.getOrderLineDate(mockOrderLine)).toEqual(mockOrderLine.shipmentTrackingDate);
  });
});
