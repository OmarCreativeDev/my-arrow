import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { IOrderLine } from '@app/shared/shared.interfaces';
import { Status } from '@app/shared/locale/i18n.maps';

@Component({
  selector: 'app-order-lines-overview',
  templateUrl: './order-lines-overview.component.html',
  styleUrls: ['./order-lines-overview.component.scss'],
})
export class OrderLinesOverviewComponent {
  @Input()
  orderLines: Array<IOrderLine> = [];
  @Input()
  rows: number = 5;
  @Input()
  displayShippedDate = false;

  public orderLineStatusMap: any = Status.orderLineStatusMap;

  constructor(public router: Router) {}

  public get reducedOrderLines(): Array<IOrderLine> {
    return this.orderLines.slice(0, this.rows);
  }

  public get emptyRows(): Array<null> {
    return new Array(this.rows - this.reducedOrderLines.length);
  }

  public getOrderLineDate(orderLine: IOrderLine) {
    return this.displayShippedDate ? orderLine.shipmentTrackingDate : orderLine.requested;
  }

  public goToOrder($event, orderId): void {
    /* istanbul ignore else */
    if (!$event.target.classList.contains('orderlines-overview__tracking') && orderId) {
      this.router.navigateByUrl(`/orders/${orderId}`);
    }
  }
}
