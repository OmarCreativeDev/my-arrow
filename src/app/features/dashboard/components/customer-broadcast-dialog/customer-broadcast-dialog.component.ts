import { Component, Inject, OnDestroy } from '@angular/core';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { CustomerBroadcastService } from '@app/core/customer-broadcast/customer-broadcast.service';
import { Subscription } from 'rxjs';

export interface DialogData {
  id: string;
  title: string;
  content: string;
}

@Component({
  selector: 'app-latest-updates-dialog-component',
  templateUrl: './customer-broadcast-dialog.component.html',
  styleUrls: ['./customer-broadcast-dialog.component.scss'],
})
export class CustomerBroadcastDialogComponent implements OnDestroy {
  public title: string;
  public content: string;
  public messageId: string;
  public notDisplayMessageAgain = false;
  private subscription: Subscription = new Subscription();

  /* tslint:disable:no-unused-variable */
  constructor(
    private dialog: Dialog<CustomerBroadcastDialogComponent>,
    @Inject(APP_DIALOG_DATA) public data: DialogData,
    private customerBroadcastService: CustomerBroadcastService
  ) {
    this.title = data.title;
    this.content = data.content;
    this.messageId = data.id;
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public onClose(): void {
    this.dialog.close();
  }

  public onClickOk(): void {
    if (this.notDisplayMessageAgain) {
      this.muteMessage();
    } else {
      this.dialog.close();
    }
  }

  private subscribeToMuteMessage(): Subscription {
    return this.customerBroadcastService.muteMessage(this.messageId).subscribe(() => {
      this.dialog.close();
    });
  }

  private muteMessage(): void {
    this.subscription.add(this.subscribeToMuteMessage());
  }
}
