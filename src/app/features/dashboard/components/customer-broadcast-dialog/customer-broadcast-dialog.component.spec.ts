import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CustomerBroadcastDialogComponent, DialogData } from './customer-broadcast-dialog.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Dialog, APP_DIALOG_DATA } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { CustomerBroadcastService } from '@app/core/customer-broadcast/customer-broadcast.service';
import { CoreModule } from '@app/core/core.module';
import { of } from 'rxjs';

describe('CustomerBroadcastDialogComponent', () => {
  let dialogService: Dialog<any>;
  let component: CustomerBroadcastDialogComponent;
  let fixture: ComponentFixture<CustomerBroadcastDialogComponent>;
  let customerBroadcastService: CustomerBroadcastService;

  const data: DialogData = {
    id: 'id',
    title: 'title',
    content: 'content',
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CustomerBroadcastDialogComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [CoreModule],
      providers: [{ provide: Dialog, useClass: DialogMock }, { provide: APP_DIALOG_DATA, useValue: data }, CustomerBroadcastService],
    }).compileComponents();
    dialogService = TestBed.get(Dialog);
    customerBroadcastService = TestBed.get(CustomerBroadcastService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerBroadcastDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the dialog after clicking the goIt button', () => {
    spyOn(dialogService, 'close');
    component.onClickOk();
    expect(dialogService.close).toHaveBeenCalled();
  });

  it('should call the muteMessage when notDisplayMessageAgain is active', () => {
    component.notDisplayMessageAgain = true;
    spyOn(customerBroadcastService, 'muteMessage').and.callFake(() => of());
    component.onClickOk();
    expect(customerBroadcastService.muteMessage).toHaveBeenCalled();
  });
});
