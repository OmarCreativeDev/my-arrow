import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';

import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { IEmail } from '@app/core/email/email.interfaces';
import { IUser } from '@app/core/user/user.interface';
import { getUser } from '@app/core/user/store/user.selectors';
import { AlertComponentEnum } from '@app/shared/shared.interfaces';
import { SendPOEmail, POUpload, POUploadReset, POUploadSetPoNumber } from '@app/features/dashboard/stores/dashboard.actions';
import { IPOUpload } from '@app/core/purchase-order/purchase-order.interface';
import { getDashboardPOUploadErrorSelector, getDashboardPOUploadSuccessSelector } from '@app/features/dashboard/stores/dashboard.selectors';
import { IAppState } from '@app/shared/shared.interfaces';

let nextUniqueId = 0;

@Component({
  selector: 'app-upload-purchase-order',
  templateUrl: './upload-purchase-order.component.html',
  styleUrls: ['./upload-purchase-order.component.scss'],
})
export class UploadPurchaseOrderComponent implements OnInit, OnDestroy {
  public static MULTIPLE_FILE_ERROR = 'Cannot use multiple files';

  readonly inputId = `upload-purchase-order__control-${++nextUniqueId}`;
  public sendConfirmation: boolean = false;
  public poNumber: string;
  private _file: File = null;
  public userEmail: string;
  public uploadTitle: string = 'Upload a Purchase Order';
  public successfulUpload: boolean = false;
  public userProfile$: Observable<IUser>;
  public permissionList: Array<string>;
  public errorMessage: string;
  public alertComponentEnum = AlertComponentEnum;
  public POUploadError$: Observable<any>;
  public POUploadSuccess$: Observable<any>;
  public errors: object;
  public loadNotificationDownloadsError$: object;
  public region: string;
  public loading: boolean = false;
  public subscription: Subscription = new Subscription();

  public get canUpload(): boolean {
    return this._file !== null;
  }

  public get fileTitle(): string {
    return this._file ? this._file.name : '';
  }

  /* tslint:disable:no-unused-variable */
  constructor(
    private store: Store<IAppState>,
    private dialog: Dialog<UploadPurchaseOrderComponent>,
    @Inject(APP_DIALOG_DATA) public data: any
  ) {
    this.POUploadError$ = store.pipe(select(getDashboardPOUploadErrorSelector));
    this.POUploadSuccess$ = store.pipe(select(getDashboardPOUploadSuccessSelector));
    this.userProfile$ = store.pipe(select(getUser));
  }

  ngOnInit() {
    this.startSubscriptions();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeUser());
    this.subscription.add(this.subscribeError());
    this.subscription.add(this.subscribeSuccess());
  }

  private subscribeSuccess(): Subscription {
    return this.POUploadSuccess$.subscribe(response => {
      if (response === true) {
        this.errorMessage = null;
        this.successfulUpload = true;
      }
    });
  }

  private subscribeError(): Subscription {
    return this.POUploadError$.subscribe(error => {
      if (error && error.error) {
        this.successfulUpload = false;
        this.errorMessage =
          error.error.status === 413
            ? 'Upload failed. File size must be less than 20mb'
            : 'Upload failed. Please try uploading your file again';
      }
    });
  }

  private subscribeUser(): Subscription {
    return this.userProfile$.subscribe(user => {
      if (user) {
        this.userEmail = user.email;
        this.permissionList = user.permissionList;
        this.region = user.region;
      }
    });
  }

  public onClose(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
    this.dialog.close();
  }

  public onUpload(): void {
    this.loading = true;
    const uploadType = this.permissionList && this.permissionList.includes('PO Upload Conexiom') ? 'CONEXIOM' : 'MANUAL';
    const data: IPOUpload = {
      userEmail: this.userEmail,
      region: this.region,
      uploadType: uploadType,
      poNumber: this.poNumber,
    };
    const formData: FormData = new FormData();
    formData.append('file', this._file, this._file.name);
    formData.append('data', JSON.stringify(data));

    this.store.dispatch(new POUpload(formData));
    this.store.dispatch(new POUploadSetPoNumber(this.poNumber));
  }

  public onFileChange(event: any): void {
    this.errorMessage = null;
    this.loading = false;
    const target = event.target || event.srcElement;
    const files = target.files;

    if (files.length > 1) {
      throw new Error(UploadPurchaseOrderComponent.MULTIPLE_FILE_ERROR);
    }

    const file = files[0];

    if (file) {
      this._file = file;
    }
  }

  public sendConfirmationEmail(): void {
    const content = `Thank you. Your purchase order ${this.poNumber} has been successfully uploaded and is being processed.
    You will receive an additional email with detailed information once the order details have been entered into our system.
    <br /><br /><br /><br />


    Sincerely,
    <br /><br /><br /><br />


    Arrow Electronics, Inc.
    `;

    const email: IEmail = {
      from: 'purchaseorder@arrow.com',
      to: [this.userEmail],
      subject: `Your upload Purchase Order: ${this.poNumber}`,
      content: content,
    };
    this.store.dispatch(new SendPOEmail(email));
    this.onClose();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
    this.store.dispatch(new POUploadReset());
    this.errorMessage = null;
    this.loading = false;
    this.successfulUpload = null;
  }
}
