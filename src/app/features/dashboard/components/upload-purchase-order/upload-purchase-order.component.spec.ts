import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadPurchaseOrderComponent } from './upload-purchase-order.component';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { SharedModule } from '@app/shared/shared.module';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { dashboardReducers } from '@app/features/dashboard/stores/dashboard.reducers';

describe('UploadPurchaseOrderComponent', () => {
  let component: UploadPurchaseOrderComponent;
  let fixture: ComponentFixture<UploadPurchaseOrderComponent>;
  const data: any = {};
  let store: Store<any>;
  let dialog: Dialog<UploadPurchaseOrderComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [UploadPurchaseOrderComponent],
        imports: [
          SharedModule,
          HttpClientTestingModule,
          FormsModule,
          StoreModule.forRoot({
            user: combineReducers(userReducers),
            dashboard: dashboardReducers,
          }),
        ],
        providers: [{ provide: Dialog, useClass: DialogMock }, { provide: APP_DIALOG_DATA, useValue: data }],
      }).compileComponents();
      dialog = TestBed.get(Dialog);
      store = TestBed.get(Store);
      spyOn(store, 'dispatch').and.callThrough();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadPurchaseOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the dialog when Cancel button is pressed', () => {
    spyOn(dialog, 'close');
    component.onClose();
    expect(dialog.close).toHaveBeenCalled();
  });

  it('should return empty title if no file has been selected', () => {
    expect(component.fileTitle).toEqual('');
  });

  it('should throw an error when tries to upload more than one files', () => {
    const file = { name: 'foo.swf' };
    expect(function() {
      component.onFileChange({
        target: {
          files: [file, file, file],
        },
      });
    }).toThrow(new Error(UploadPurchaseOrderComponent.MULTIPLE_FILE_ERROR));
  });

  it('should ignore an empty file', () => {
    component.onFileChange({ target: { files: [] } });
    expect(component.fileTitle).toEqual('');
  });

  it('should change file title when a file is selected', () => {
    component.onFileChange({ target: { files: [{ name: 'foo.swf' }] } });
    expect(component.fileTitle).toBe('foo.swf');
    component.onFileChange({ srcElement: { files: [{ name: 'bar.swf' }] } });
    expect(component.fileTitle).toBe('bar.swf');
  });
});
