import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { IBom } from '@app/core/boms/boms.interface';

@Component({
  selector: 'app-bom-lines-overview',
  templateUrl: './bom-lines-overview.component.html',
  styleUrls: ['./bom-lines-overview.component.scss'],
})
export class BomLinesOverviewComponent {
  @Input()
  bomLines: Array<IBom> = [];
  @Input()
  rows: number = 5;

  constructor(public router: Router) {}

  public get reducedBomLines(): Array<IBom> {
    return this.bomLines.slice(0, this.rows);
  }

  public get emptyRows(): Array<null> {
    return new Array(this.rows - this.reducedBomLines.length);
  }

  public goToBom(bomId): void {
    /* istanbul ignore else */
    if (bomId) {
      this.router.navigateByUrl(`/bom/view/${bomId}/1`);
    }
  }
}
