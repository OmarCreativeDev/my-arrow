import { DatePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { IBom } from '@app/core/boms/boms.interface';
import { IsoDatePipe } from '@app/shared/pipes/iso-date/iso-date.pipe';
import { DateService } from '@app/shared/services/date.service';

import { BomLinesOverviewComponent } from './bom-lines-overview.component';

class DummyComponent {}

const mockBom: IBom = {
  id: '1236236',
  name: 'Fake name',
  lastEdited: '2018-09-10',
  partCount: 4503,
};

describe('BomLinesOverviewComponent', () => {
  let component: BomLinesOverviewComponent;
  let fixture: ComponentFixture<BomLinesOverviewComponent>;
  let router: Router;

  const routes = [{ path: 'bom/view/:bomId/1', component: DummyComponent }];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes)],
      declarations: [BomLinesOverviewComponent, IsoDatePipe],
      providers: [DatePipe, DateService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BomLinesOverviewComponent);
    component = fixture.componentInstance;
    component.bomLines = [mockBom] as Array<IBom>;
    router = TestBed.get(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`reducedBomLines` should return an array with a maximum size of the amount of rows to display', () => {
    component.rows = 5;
    component.bomLines = Array(10).fill(mockBom);
    expect(component.reducedBomLines.length).toEqual(5);

    component.rows = 5;
    component.bomLines = Array(3).fill(mockBom);
    expect(component.reducedBomLines.length).toEqual(3);

    component.rows = 5;
    component.bomLines = Array(0).fill(mockBom);
    expect(component.reducedBomLines.length).toEqual(0);
  });

  it('`emptyRows` should provide an array of size equal to the number of rows minus the size of the boms array', () => {
    component.bomLines = Array(5).fill(mockBom);

    component.rows = 10;
    expect(component.emptyRows.length).toEqual(5);

    component.rows = 5;
    expect(component.emptyRows.length).toEqual(0);
  });

  it('should should navigate to the bom when clicking on the bom row', () => {
    const spy = spyOn(router, 'navigateByUrl');
    fixture.nativeElement.querySelector('.orderline-overview').click();
    const url = spy.calls.first().args[0];
    expect(url).toBe('/bom/view/1236236/1');
  });
});
