import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { SharedModule } from '@app/shared/shared.module';
import { OrderLinesOverviewComponent } from './components/order-lines-overview/order-lines-overview.component';
import { OrdersModule } from '@app/features/orders/orders.module';
import { BomLinesOverviewComponent } from './components/bom-lines-overview/bom-lines-overview.component';
import { RouterModule } from '@angular/router';
import { UploadPurchaseOrderComponent } from './components/upload-purchase-order/upload-purchase-order.component';

@NgModule({
  imports: [CommonModule, DashboardRoutingModule, SharedModule, OrdersModule, RouterModule, FormsModule],
  declarations: [
    DashboardComponent,
    OrderLinesOverviewComponent,
    BomLinesOverviewComponent,
    UploadPurchaseOrderComponent,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  entryComponents: [UploadPurchaseOrderComponent,],
})
export class DashboardModule {}
