import { ErrorHandler, Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { SendAnalyticsException } from '@app/core/analytics/stores/analytics.actions';
import { IGaState } from '@app/core/analytics/stores/analytics.reducer';

@Injectable()
export class ErrorsHandler implements ErrorHandler {
  constructor(private injector: Injector) {}

  handleError(error: Error) {
    const store: Store<IGaState> = this.injector.get<Store<IGaState>>(Store);
    const router: Router = this.injector.get(Router);

    store.dispatch(new SendAnalyticsException(error));
    // Do whatever you like with the error (send it to the server?)
    // And log it to the console
    console.error('Exception: ', error);

    // redirect the user to the error page
    router.navigateByUrl(`/error`, { skipLocationChange: true });
  }
}
