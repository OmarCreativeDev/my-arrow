//TODO: Translation in code
export const errorsMessageHeader: string = 'Internal Error';

export const errorsMessageBody: string =
  'Sorry, an error prevented the display of this view.<br/>Please refresh the page or choose from the navigation above.';
