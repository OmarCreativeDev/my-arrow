import { TestBed, inject } from '@angular/core/testing';
import { ErrorsHandler } from '@app/features/errors/errors-handler';
import { RouterTestingModule } from '@angular/router/testing';
import { StoreModule, Store } from '@ngrx/store';
import { googleAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.custom-events';
import { IAppState } from '@app/shared/shared.interfaces';
import { SendAnalyticsException } from '@app/core/analytics/stores/analytics.actions';

class DummyComponent {}

const mockError = {
  name: 'TESTING EXAMPLE ERROR - PLEASE DISMISS',
  message: 'TESTING EXAMPLE ERROR - PLEASE DISMISS',
};

describe('ErrorsHandler', () => {
  const routes = [
    { path: 'login', component: DummyComponent },
    { path: 'error', component: DummyComponent },
    { path: 'dashboard', component: DummyComponent },
  ];
  let errorsHandler: ErrorsHandler;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes(routes), StoreModule.forRoot({ googleAnalytics: googleAnalyticsMetaReducers })],
      providers: [ErrorsHandler],
    });

    errorsHandler = TestBed.get(ErrorsHandler);
  });

  it('should be created', () => {
    expect(errorsHandler).toBeTruthy();
  });

  it('should handle an error', () => {
    spyOn(console, 'error');
    errorsHandler.handleError(mockError);
    expect(console.error).toHaveBeenCalled();
  });

  it('should dispatch SendAnalyticsException', inject([Store], (store: Store<IAppState>) => {
    spyOn(store, 'dispatch');
    errorsHandler.handleError(mockError);
    expect(store.dispatch).toHaveBeenCalledWith(new SendAnalyticsException(mockError));
  }));
});
