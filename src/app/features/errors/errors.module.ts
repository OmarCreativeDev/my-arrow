import { ErrorHandler, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { ErrorsComponent } from './pages/errors/errors.component';
import { SharedModule } from '@app/shared/shared.module';
import { ErrorsHandler } from '@app/features/errors/errors-handler';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
  ],
  declarations: [NotFoundComponent, ErrorsComponent],
  providers: [
    {
      provide: ErrorHandler,
      useClass: ErrorsHandler,
    },
  ],
})
export class ErrorsModule {
}
