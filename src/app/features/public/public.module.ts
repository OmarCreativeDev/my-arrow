import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { DialogService } from '@app/core/dialog/dialog.service';

import { AcceptTermsComponent } from './components/accept-terms/accept-terms.component';
import { HelpPanelComponent } from './components/help-panel/help-panel.component';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { LoginMessageComponent } from './components/login-message/login-message.component';
import { PasswordResetFormComponent } from './components/password-reset-form/password-reset-form.component';
import { PasswordSuccessComponent } from './components/password-success/password-success.component';
import { RecoverPasswordFormComponent } from './components/recover-password-form/recover-password-form.component';
import { RegistrationFormComponent } from './components/registration-form/registration-form.component';
import { AccountPageComponent } from './containers/account-page/account-page.component';
import { LinkExpiredComponent } from './pages/link-expired/link-expired.component';
import { LoginComponent } from './pages/login/login.component';
import { PasswordResetComponent } from './pages/password-reset/password-reset.component';
import { RecoverPasswordComponent } from './pages/recover-password/recover-password.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { RequestErrorComponent } from './pages/request-error/request-error.component';
import { SuccessComponent } from './pages/success/success.component';
import { TermsOfUseComponent } from './pages/terms-of-use/terms-of-use.component';
import { UserErrorComponent } from './pages/user-error/user-error.component';
import { UserRoutingModule } from './public-routing.module';
import { FormErrorMessageComponent } from './components/form-error-message/form-error-message.component';
import { AcceptTermsDialogComponent } from './components/accept-terms-dialog/accept-terms-dialog.component';
import { RegistrationErrorComponent } from './pages/registration-error/registration-error.component';

@NgModule({
  providers: [DialogService],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, RouterModule, UserRoutingModule, SharedModule],
  declarations: [
    LoginComponent,
    LoginFormComponent,
    LoginMessageComponent,
    TermsOfUseComponent,
    AcceptTermsComponent,
    UserErrorComponent,
    RecoverPasswordComponent,
    RecoverPasswordFormComponent,
    AccountPageComponent,
    HelpPanelComponent,
    PasswordResetComponent,
    PasswordResetFormComponent,
    PasswordSuccessComponent,
    RegistrationFormComponent,
    RegistrationComponent,
    SuccessComponent,
    LinkExpiredComponent,
    RequestErrorComponent,
    FormErrorMessageComponent,
    AcceptTermsDialogComponent,
    RegistrationErrorComponent,
  ],
  entryComponents: [AcceptTermsDialogComponent],
})
export class PublicModule {}
