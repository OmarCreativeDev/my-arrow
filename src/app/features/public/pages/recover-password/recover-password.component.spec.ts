import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@app/core/core.module';
import { AccountPageComponent } from '@app/features/public/containers/account-page/account-page.component';
import { SharedModule } from '@app/shared/shared.module';
import { Store, StoreModule } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';

import { RecoverPasswordFormComponent } from '../../components/recover-password-form/recover-password-form.component';
import { RecoverPasswordComponent } from './recover-password.component';
import { PasswordSuccessComponent } from '../../components/password-success/password-success.component';
import { Observable, of } from 'rxjs';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';

class StoreMock {
  public select(): Observable<any> {
    return of({});
  }
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

describe('RecoverPasswordComponent', () => {
  let component: RecoverPasswordComponent;
  let fixture: ComponentFixture<RecoverPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        RouterTestingModule,
        StoreModule.forRoot({ user: userReducers }),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      declarations: [AccountPageComponent, RecoverPasswordComponent, RecoverPasswordFormComponent, PasswordSuccessComponent],
      providers: [{ provide: Store, useClass: StoreMock }, { provide: WSSService, useClass: MockStompWSSService }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoverPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
