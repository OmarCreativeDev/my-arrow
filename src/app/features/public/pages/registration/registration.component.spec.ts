import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from '@app/shared/shared.module';
import { RegistrationComponent } from './registration.component';
import { RegistrationFormComponent } from '@app/features/public/components/registration-form/registration-form.component';
import { FormErrorMessageComponent } from '@app/features/public/components/form-error-message/form-error-message.component';
import { DialogService } from '@app/core/dialog/dialog.service';
import { OverlayContainer } from '@app/core/dialog/overlay-container';

class MockStore {
  select() {
    return of();
  }
  dispatch() {}
  pipe() {
    return of();
  }
}

describe('RegistrationComponent', () => {
  let component: RegistrationComponent;
  let fixture: ComponentFixture<RegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RegistrationComponent, RegistrationFormComponent, FormErrorMessageComponent],
      imports: [FormsModule, ReactiveFormsModule, HttpClientTestingModule, SharedModule, RouterTestingModule],
      providers: [HttpClient, HttpHandler, { provide: Store, useClass: MockStore }, DialogService, OverlayContainer],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
