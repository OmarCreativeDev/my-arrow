import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RequestErrorComponent } from '@app/features/public/pages/request-error/request-error.component';
import { CoreModule } from '@app/core/core.module';
import { ListingMessageComponent } from '@app/shared/components/listing-message/listing-message.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('RequestErrorComponent', () => {
  let component: RequestErrorComponent;
  let fixture: ComponentFixture<RequestErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule],
      declarations: [RequestErrorComponent, ListingMessageComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
