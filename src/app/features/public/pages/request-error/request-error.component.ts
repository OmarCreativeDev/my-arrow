import { Component } from '@angular/core';

@Component({
  selector: 'app-request-error',
  templateUrl: './request-error.component.html',
  styleUrls: ['./request-error.component.scss'],
})
export class RequestErrorComponent {}
