import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { StoreModule } from '@ngrx/store';
import { CoreModule } from '@app/core/core.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { userReducers } from '@app/core/user/store/user.reducers';

import { TermsOfUseComponent } from './terms-of-use.component';
import { AccountPageComponent } from '@app/features/public/containers/account-page/account-page.component';


describe('TermsOfUseComponent', () => {
  let component: TermsOfUseComponent;
  let fixture: ComponentFixture<TermsOfUseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({ user: userReducers }), RouterTestingModule, CoreModule],
      declarations: [AccountPageComponent, TermsOfUseComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TermsOfUseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
