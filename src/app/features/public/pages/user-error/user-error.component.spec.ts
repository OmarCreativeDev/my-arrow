import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';
import { UserErrorComponent } from './user-error.component';
import { CoreModule } from '@app/core/core.module';
import { userReducers } from '@app/core/user/store/user.reducers';
import { ListingMessageComponent } from '@app/shared/components/listing-message/listing-message.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';

class DummyComponent {}

describe('UserErrorComponent', () => {
  const routes = [{ path: 'dashboard', component: DummyComponent }];
  let component: UserErrorComponent;
  let fixture: ComponentFixture<UserErrorComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        StoreModule.forRoot({
          user: userReducers,
        }),
        RouterTestingModule.withRoutes(routes),
      ],
      declarations: [UserErrorComponent, ListingMessageComponent],
      providers: [{ provide: WSSService, useClass: MockStompWSSService }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
