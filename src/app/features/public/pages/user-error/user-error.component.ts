import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { AuthService } from '@app/core/auth/auth.service';
import { IAppState, IException } from '@app/shared/shared.interfaces';
import * as UserActions from '@app/core/user/store/user.actions';
import * as UserSelectors from '@app/core/user/store/user.selectors';

@Component({
  selector: 'app-user-error',
  templateUrl: './user-error.component.html',
  styleUrls: ['./user-error.component.scss'],
})
export class UserErrorComponent implements OnInit, OnDestroy {
  public error$: Observable<IException>;
  public subscription: Subscription = new Subscription();

  constructor(private store: Store<IAppState>, private authService: AuthService) {
    this.error$ = this.store.pipe(select(UserSelectors.getUserError));
  }

  ngOnInit() {
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeErrors());
  }

  private subscribeErrors(): Subscription {
    return this.error$.subscribe(error => {
      /* istanbul ignore else */
      if (!error) {
        this.store.dispatch(new UserActions.Redirect());
      }
    });
  }

  signIn() {
    this.authService.logout();
  }
}
