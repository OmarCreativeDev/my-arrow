import { Component, OnDestroy, OnInit } from '@angular/core';
import { getUserError } from '@app/core/user/store/user.selectors';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import { isUserNotReady } from '@app/core/user/user.utils';
import { getPublicFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  public showInvalidUsertypeMessage: boolean = false;

  public publicFeatureFlags: any;

  public subscription: Subscription = new Subscription();
  private publicFeatureFlags$: Observable<any>;
  private userError$: Observable<any>;

  constructor(private store: Store<IAppState>) {
    this.publicFeatureFlags$ = this.store.pipe(select(getPublicFeatureFlagsSelector));
    this.userError$ = this.store.pipe(select(getUserError));
  }

  ngOnInit() {
    this.startSubscriptions();
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribePublicFeatureFlags());
    this.subscription.add(this.subscribeUserError());
  }

  private subscribePublicFeatureFlags(): Subscription {
    return this.publicFeatureFlags$.subscribe(featureFlags => {
      this.publicFeatureFlags = featureFlags;
    });
  }

  private subscribeUserError(): Subscription {
    return this.userError$.subscribe(error => (this.showInvalidUsertypeMessage = isUserNotReady(error)));
  }

  public onLoginAsDifferentUser() {
    this.showInvalidUsertypeMessage = false;
  }
}
