import { HttpErrorResponse } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@app/core/core.module';
import { UserError } from '@app/core/user/store/user.actions';
import { userReducers } from '@app/core/user/store/user.reducers';
import { getUserError } from '@app/core/user/store/user.selectors';
import { SharedModule } from '@app/shared/shared.module';
import { Store, StoreModule, select } from '@ngrx/store';
import { Observable, of } from 'rxjs';

import { LoginFormComponent } from '../../components/login-form/login-form.component';
import { LoginMessageComponent } from '../../components/login-message/login-message.component';
import { LoginComponent } from './login.component';

class StoreMock {
  public select(): Observable<any> {
    return of({});
  }
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

xdescribe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        CoreModule,
        StoreModule.forRoot({
          user: userReducers,
        }),
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
      ],
      declarations: [LoginComponent, LoginFormComponent, LoginMessageComponent],
      providers: [{ provide: Store, useClass: StoreMock }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set showInvalidUsertypeMessage to false when onLoginAsDifferentUser is called', () => {
    component.onLoginAsDifferentUser();
    expect(component.showInvalidUsertypeMessage).toBeFalsy();
  });

  it(`should set showInvalidUsertypeMessage to true when a 403 user error with errorCode '3002' is returned`, () => {
    store.dispatch(new UserError(new HttpErrorResponse({ status: 403, error: { status: '3002' } })));
    store.pipe(select(getUserError)).subscribe(error => {
      expect(component.showInvalidUsertypeMessage).toBeTruthy();
    });
  });

  it(`showInvalidUsertypeMessage should be false when a 500 user error is returned`, () => {
    store.dispatch(new UserError(new HttpErrorResponse({ status: 500, error: { status: '3001' } })));
    store.pipe(select(getUserError)).subscribe(error => {
      expect(component.showInvalidUsertypeMessage).toBeFalsy();
    });
  });
});
