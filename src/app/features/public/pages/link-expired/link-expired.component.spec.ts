import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@app/core/core.module';
import { AccountPageComponent } from '@app/features/public/containers/account-page/account-page.component';
import { StoreModule } from '@ngrx/store';

import { LinkExpiredComponent } from './link-expired.component';

describe('LinkExpiredComponent', () => {
  let component: LinkExpiredComponent;
  let fixture: ComponentFixture<LinkExpiredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule, RouterTestingModule, StoreModule.forRoot({})],
      declarations: [AccountPageComponent, LinkExpiredComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LinkExpiredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
