import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';
import { RecoverPasswordFormComponent } from './recover-password-form.component';
import { PasswordSuccessComponent } from '../password-success/password-success.component';
import { SharedModule } from '@app/shared/shared.module';
import { Store, StoreModule } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';
import { AuthService } from '@app/core/auth/auth.service';
import { Observable, of, throwError } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@app/core/core.module';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';

class StoreMock {
  public select(): Observable<any> {
    return of({});
  }
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

class DummyComponent {}

describe('RecoverPasswordFormComponent', () => {
  const routes = [{ path: 'login', component: DummyComponent }];
  let component: RecoverPasswordFormComponent;
  let fixture: ComponentFixture<RecoverPasswordFormComponent>;
  let store: Store<any>;
  let emailField: FormControl;
  let authService: AuthService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({ user: userReducers }),
        CoreModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule.withRoutes(routes),
      ],
      declarations: [RecoverPasswordFormComponent, PasswordSuccessComponent],
      providers: [AuthService, { provide: Store, useClass: StoreMock }, { provide: WSSService, useClass: MockStompWSSService }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecoverPasswordFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    emailField = <FormControl>component.form.controls.email;
    authService = TestBed.get(AuthService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not submit form if email is not entered', () => {
    component.submit();
    expect(component.form.valid).toBeFalsy();
  });

  it('should not submit form if email field is not valid', () => {
    emailField.setValue('email@email');
    component.submit();
    expect(component.form.valid).toBeFalsy();
  });

  it('should accept input email and receive a success response from recoverPassword()', () => {
    spyOn(authService, 'recoverPassword').and.returnValue(of({ accepted: true }));
    emailField.setValue('email@email.com');
    component.submit();
    expect(component.submitted).toBeTruthy();
  });

  it('should redirect to /request-error if recoverPassword returns error', inject([Router], (router: Router) => {
    const spy = spyOn(router, 'navigateByUrl');
    spyOn(authService, 'recoverPassword').and.returnValue(throwError(new HttpErrorResponse({ status: 500 })));
    emailField.setValue('email@email.com');
    component.submit();
    expect(spy.calls.first().args[0]).toBe('request-error');
  }));
});
