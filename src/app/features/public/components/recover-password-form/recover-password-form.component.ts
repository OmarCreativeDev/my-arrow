import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from '@app/core/auth/auth.service';
import { Router } from '@angular/router';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import { CustomValidators } from '@app/shared/classes/custom-validators';
import { Subscription, Observable } from 'rxjs';
import { getPublicFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: 'app-recover-password-form',
  templateUrl: './recover-password-form.component.html',
})
export class RecoverPasswordFormComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public publicFeatureFlags: object;
  public submitted = false;
  public subscription: Subscription = new Subscription();
  private publicFeatureFlags$: Observable<any>;

  constructor(private authService: AuthService, private router: Router, private store: Store<IAppState>) {
    this.publicFeatureFlags$ = this.store.pipe(select(getPublicFeatureFlagsSelector));
  }

  ngOnInit() {
    this.startSubscriptions();
    this.form = new FormGroup(
      {
        email: new FormControl('', CustomValidators.email),
      },
      { updateOn: 'submit' }
    );
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribePublicFeatureFlags());
  }

  private subscribePublicFeatureFlags(): Subscription {
    return this.publicFeatureFlags$.subscribe(featureFlags => {
      this.publicFeatureFlags = featureFlags;
    });
  }

  public submit() {
    if (this.form.valid) {
      this.subscription.add(
        this.authService.recoverPassword(this.form.value.email).subscribe(
          () => {
            this.submitted = true;
          },
          () => this.router.navigateByUrl('request-error')
        )
      );
    }
  }
}
