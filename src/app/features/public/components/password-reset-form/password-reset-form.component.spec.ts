import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { PasswordResetFormComponent } from './password-reset-form.component';
import { SharedModule } from '@app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@app/core/core.module';
import { Store, StoreModule } from '@ngrx/store';
import { Observable, of, throwError } from 'rxjs';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AuthService } from '@app/core/auth/auth.service';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { userReducers } from '@app/core/user/store/user.reducers';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';

describe('PasswordResetFormComponent', () => {
  let component: PasswordResetFormComponent;
  let fixture: ComponentFixture<PasswordResetFormComponent>;
  let authService: AuthService;
  let passwordField: AbstractControl;
  let confirmPasswordField: AbstractControl;

  class StoreMock {
    public select(): Observable<any> {
      return of({});
    }
    public dispatch(): void {}
    public pipe() {
      return of({});
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({ user: userReducers }),
        CoreModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        RouterTestingModule,
      ],
      declarations: [PasswordResetFormComponent],
      providers: [AuthService, { provide: Store, useClass: StoreMock }, { provide: WSSService, useClass: MockStompWSSService }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordResetFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.get(AuthService);
    passwordField = component.form.controls.newPassword;
    confirmPasswordField = component.form.controls.confirmNewPassword;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not submit form if password is not set', () => {
    confirmPasswordField.setValue('password');
    component.submit();
    expect(component.form.valid).toBeFalsy();
  });

  it('should not submit form if confirm password is not set', () => {
    passwordField.setValue('password');
    component.submit();
    expect(component.form.valid).toBeFalsy();
  });

  it('should display matching error correctly', () => {
    passwordField.setValue('Password1');
    confirmPasswordField.setValue('password1');
    component.submit();
    expect(component.showMatchingError).toBeTruthy();
  });

  it('should accept input password and confirmed password and receive a success response from resetPassword()', () => {
    spyOn(authService, 'resetPassword').and.returnValue(of({}));
    passwordField.setValue('Password1');
    confirmPasswordField.setValue('Password1');
    component.submit();
    expect(component.submitted).toBeTruthy();
  });

  it('should redirect to /request-error if resetPassword() returns an error', inject([Router], (router: Router) => {
    const spy = spyOn(router, 'navigateByUrl');
    spyOn(authService, 'resetPassword').and.returnValue(throwError(new HttpErrorResponse({ status: 500 })));
    passwordField.setValue('Password1');
    confirmPasswordField.setValue('Password1');
    component.submit();
    expect(spy.calls.first().args[0]).toBe('request-error');
  }));
});
