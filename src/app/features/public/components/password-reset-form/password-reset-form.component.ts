import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from '@app/shared/classes/custom-validators';
import { AuthService } from '@app/core/auth/auth.service';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import { Subscription, Observable } from 'rxjs';
import { PasswordResetSuccess } from '@app/core/user/store/user.actions';
import { getPublicFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: 'app-password-reset-form',
  templateUrl: './password-reset-form.component.html',
  styleUrls: ['./password-reset-form.component.scss'],
})
export class PasswordResetFormComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public newPasswordControl = new FormControl('', CustomValidators.password);
  public confirmNewPasswordControl = new FormControl('', Validators.required);
  public publicFeatureFlags: object;
  public resetId: string;
  public key: string;
  public submitted = false;
  public subscription: Subscription = new Subscription();
  private publicFeatureFlags$: Observable<any>;

  constructor(private authService: AuthService, private route: ActivatedRoute, private router: Router, private store: Store<IAppState>) {
    this.publicFeatureFlags$ = this.store.pipe(select(getPublicFeatureFlagsSelector));
  }

  ngOnInit() {
    this.startSubscriptions();
    this.form = new FormGroup(
      {
        newPassword: this.newPasswordControl,
        confirmNewPassword: this.confirmNewPasswordControl,
      },
      {
        validators: Validators.compose([
          Validators.required,
          CustomValidators.equal(this.newPasswordControl, this.confirmNewPasswordControl),
        ]),
      }
    );
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribePublicFeatureFlags());
    this.subscription.add(this.subscribeQueryParams());
  }

  private subscribePublicFeatureFlags(): Subscription {
    return this.publicFeatureFlags$.subscribe(featureFlags => {
      this.publicFeatureFlags = featureFlags;
    });
  }

  private subscribeQueryParams(): Subscription {
    return this.route.queryParams.subscribe(params => {
      this.resetId = params.rid;
      this.key = params.key;
    });
  }

  public avoidPaste(event) {
    event.preventDefault();
  }

  public submit() {
    if (this.form.valid) {
      this.subscription.add(
        this.authService.resetPassword(this.resetId, this.key, this.form.value.newPassword).subscribe(
          () => {
            this.store.dispatch(new PasswordResetSuccess('Reset Completed'));
            this.submitted = true;
          },
          () => this.router.navigateByUrl('request-error')
        )
      );
    }
  }

  get showMatchingError() {
    return this.newPasswordControl.valid && (this.form.errors && this.form.errors['matching']);
  }
}
