import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { StoreModule, Store } from '@ngrx/store';
import { CoreModule } from '@app/core/core.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { userReducers } from '@app/core/user/store/user.reducers';
import { AcceptedTerms } from '@app/core/user/store/user.actions';
import { AcceptTermsComponent } from './accept-terms.component';
import { AuthService } from '@app/core/auth/auth.service';
import { of } from 'rxjs';
import { Redirect } from '@app/core/user/store/user.actions';
import { SharedModule } from '@app/shared/shared.module';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';

class DummyComponent {}

describe('AcceptTermsComponent', () => {
  const routes = [{ path: 'dashboard', component: DummyComponent }];
  let component: AcceptTermsComponent;
  let fixture: ComponentFixture<AcceptTermsComponent>;
  let store: Store<any>;
  let authService: AuthService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot({ user: userReducers }), RouterTestingModule.withRoutes(routes), CoreModule],
      declarations: [AcceptTermsComponent],
      providers: [AuthService, { provide: WSSService, useClass: MockStompWSSService }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptTermsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    authService = TestBed.get(AuthService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update store when terms are accepted', () => {
    component.acceptAccountTerms();
    expect(store.dispatch).toHaveBeenCalledWith(new AcceptedTerms(true));
  });

  it('should redirect user if terms are already accepted', () => {
    spyOn(store, 'pipe').and.returnValue(of(true));
    component.ngAfterViewInit();
    expect(store.dispatch).toHaveBeenCalledWith(new Redirect());
  });

  it('should logout when clicking the Cancel button', () => {
    spyOn(authService, 'logout');
    component.cancel();
    expect(authService.logout).toHaveBeenCalled();
  });
});
