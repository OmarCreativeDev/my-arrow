import { Component, Input, EventEmitter, Output, AfterViewInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AuthService } from '@app/core/auth/auth.service';
import { IAppState } from '@app/shared/shared.interfaces';
import { IUser } from '@app/core/user/user.interface';
import { AcceptedTerms, Redirect } from '@app/core/user/store/user.actions';

import * as userSelectors from '@app/core/user/store/user.selectors';

import { Observable } from 'rxjs';

@Component({
  selector: 'app-accept-terms',
  templateUrl: './accept-terms.component.html',
  styleUrls: ['./accept-terms.component.scss'],
})
export class AcceptTermsComponent implements AfterViewInit {
  @Input()
  public isUserLoggedIn: boolean = true;
  @Output()
  public acceptTerms: EventEmitter<boolean> = new EventEmitter();

  public user$: Observable<IUser>;
  public termsAccepted$: Observable<boolean>;
  public termsCheckboxValue = false;

  constructor(private store: Store<IAppState>, private authService: AuthService) {}

  ngAfterViewInit() {
    /* istanbul ignore else */
    if (this.isUserLoggedIn) {
      this.termsAccepted$ = this.store.pipe(select(userSelectors.getTermsAccepted));
      this.termsAccepted$.subscribe(termsAccepted => {
        /* istanbul ignore else */
        if (termsAccepted) {
          this.store.dispatch(new Redirect());
        }
      });
    }
  }

  public acceptAccountTerms() {
    if (this.isUserLoggedIn) {
      this.store.dispatch(new AcceptedTerms(true));
    } else {
      this.acceptTerms.emit(true);
    }
  }

  public cancel() {
    if (this.isUserLoggedIn) {
      this.authService.logout();
    } else {
      this.acceptTerms.emit(false);
    }
  }
}
