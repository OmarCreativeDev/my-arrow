import { Component } from '@angular/core';
import { Dialog } from '@app/core/dialog/dialog.service';

@Component({
  selector: 'app-accept-terms-dialog',
  templateUrl: './accept-terms-dialog.component.html',
})
export class AcceptTermsDialogComponent {
  constructor(public dialog: Dialog<AcceptTermsDialogComponent>) {}

  public onAcceptTerms(didAccept: boolean): void {
    this.dialog.close(didAccept);
  }
}
