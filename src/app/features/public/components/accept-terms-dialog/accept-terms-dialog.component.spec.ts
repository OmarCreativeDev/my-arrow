import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AcceptTermsDialogComponent } from './accept-terms-dialog.component';
import { AcceptTermsComponent } from '@app/features/public/components/accept-terms/accept-terms.component';
import { DialogService, Dialog } from '@app/core/dialog/dialog.service';
import { of } from 'rxjs';
import { StoreModule } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';

import { CoreModule } from '@app/core/core.module';
import { SharedModule } from '@app/shared/shared.module';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';

export class DialogMock {
  close() {}
  get afterClosed() {
    return of();
  }
}

describe('AcceptTermsDialogComponent', () => {
  let component: AcceptTermsDialogComponent;
  let fixture: ComponentFixture<AcceptTermsDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule, SharedModule, StoreModule.forRoot({ user: userReducers })],
      providers: [
        { provide: DialogService, useClass: DialogServiceMock },
        { provide: Dialog, useClass: DialogMock },
        { provide: WSSService, useClass: MockStompWSSService },
      ],
      declarations: [AcceptTermsDialogComponent, AcceptTermsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AcceptTermsDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
