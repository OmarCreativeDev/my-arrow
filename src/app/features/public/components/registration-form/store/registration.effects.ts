import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { RegistrationService } from '@app/core/registration/registration.service';
import { Observable, of } from 'rxjs';
import {
  RegistrationSubmit,
  RegistrationSubmitSuccess,
  RegistrationActionTypes,
  RegistrationSubmitFailed,
  RegistrationVerificationUser,
  RegistrationVerificationUserSuccess,
  RegistrationVerificationUserFailed,
} from './registration.actions';
import { switchMap, map, catchError } from 'rxjs/operators';

@Injectable()
export class RegistrationEffects {
  constructor(private actions$: Actions, private registrationService: RegistrationService) {}

  /**
   * Side-effect that combines all side effects for RegistrationSubmit facet
   * @type {Observable<any>}
   */

  @Effect()
  postRegistrationForm: Observable<any> = this.actions$.pipe(
    ofType<RegistrationSubmit>(RegistrationActionTypes.REGISTRATION_SUBMIT),
    switchMap((action: RegistrationSubmit) => {
      return this.registrationService.postRegistration(action.payload).pipe(
        map(() => {
          return new RegistrationSubmitSuccess(action.payload.email);
        }),
        catchError(err => of(new RegistrationSubmitFailed(err)))
      );
    })
  );

  /**
   * Side-effect that combines all side effects for RegistrationUserVerification facet
   * @type {Observable<any>}
   */

  @Effect()
  postUserVerification: Observable<any> = this.actions$.pipe(
    ofType<RegistrationVerificationUser>(RegistrationActionTypes.REGISTRATION_USER_VERIFICATION),
    switchMap((action: RegistrationVerificationUser) => {
      return this.registrationService.postUserExists(action.payload).pipe(
        map(() => {
          return new RegistrationVerificationUserSuccess(true);
        }),
        catchError(err => of(new RegistrationVerificationUserFailed(false)))
      );
    })
  );
}
