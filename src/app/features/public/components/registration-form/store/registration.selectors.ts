import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IRegistrationState } from '@app/core/registration/registration.interfaces';

/**
 * Selects registration state from the root state object
 */
export const getRegistrationState = createFeatureSelector<IRegistrationState>('registration');
export const getLoadingFlagSelector = createSelector(getRegistrationState, registrationState => registrationState.loading);
export const getLoadingEmailFlagSelector = createSelector(getRegistrationState, registrationState => registrationState.loadingEmail);
export const getUserExistsFlagSelector = createSelector(getRegistrationState, registrationState => registrationState.userExists);
export const getUserIdSelector = createSelector(getRegistrationState, registrationState => registrationState.userId);
export const getRegistrationErrorSelector = createSelector(getRegistrationState, registrationState => registrationState.error);
