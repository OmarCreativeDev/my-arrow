import { IRegistrationState } from '@app/core/registration/registration.interfaces';
import { RegistrationActions, RegistrationActionTypes } from './registration.actions';

export const INITIAL_REGISTRATION_STATE: IRegistrationState = {
  error: null,
  loading: false,
  loadingEmail: false,
  userId: null,
  userExists: false,
};

export function registrationReducers(
  state: IRegistrationState = INITIAL_REGISTRATION_STATE,
  action: RegistrationActions
): IRegistrationState {
  switch (action.type) {
    case RegistrationActionTypes.REGISTRATION_SUBMIT: {
      return {
        ...state,
        error: null,
        loading: true,
        userId: null,
      };
    }

    case RegistrationActionTypes.REGISTRATION_SUBMIT_SUCCESS: {
      return {
        ...state,
        error: null,
        loading: false,
        userId: action.userId,
      };
    }

    case RegistrationActionTypes.REGISTRATION_SUBMIT_FAILED: {
      return {
        ...state,
        userId: null,
        loading: false,
        error: action.payload,
      };
    }

    case RegistrationActionTypes.RESET_REGISTRATION_STATE: {
      return {
        ...state,
        ...INITIAL_REGISTRATION_STATE,
      };
    }

    case RegistrationActionTypes.REGISTRATION_USER_VERIFICATION: {
      return {
        ...state,
        loadingEmail: true,
        userExists: false,
      };
    }

    case RegistrationActionTypes.REGISTRATION_USER_VERIFICATION_SUCCESS: {
      return {
        ...state,
        loadingEmail: false,
        userExists: action.payload,
      };
    }

    case RegistrationActionTypes.REGISTRATION_USER_VERIFICATION_FAILED: {
      return {
        ...state,
        loadingEmail: false,
        userExists: action.payload,
      };
    }

    default:
      return state;
  }
}
