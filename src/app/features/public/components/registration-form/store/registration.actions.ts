import { Action } from '@ngrx/store';
import { IRegistration } from '@app/core/registration/registration.interfaces';
import { HttpErrorResponse } from '@angular/common/http';

export enum RegistrationActionTypes {
  REGISTRATION_SUBMIT = 'REGISTRATION_SUBMIT',
  REGISTRATION_SUBMIT_FAILED = 'REGISTRATION_SUBMIT_FAILED',
  REGISTRATION_SUBMIT_SUCCESS = 'REGISTRATION_SUBMIT_SUCCESS',
  REGISTRATION_USER_VERIFICATION = 'REGISTRATION_USER_VERIFICATION',
  REGISTRATION_USER_VERIFICATION_FAILED = 'REGISTRATION_USER_VERIFICATION_FAILED',
  REGISTRATION_USER_VERIFICATION_SUCCESS = 'REGISTRATION_USER_VERIFICATION_SUCCESS',
  RESET_REGISTRATION_STATE = 'RESET_REGISTRATION_STATE',
}

export class RegistrationSubmit implements Action {
  readonly type = RegistrationActionTypes.REGISTRATION_SUBMIT;
  constructor(public payload: IRegistration) {}
}

export class RegistrationSubmitSuccess implements Action {
  readonly type = RegistrationActionTypes.REGISTRATION_SUBMIT_SUCCESS;
  constructor(public userId: string) {}
}

export class RegistrationSubmitFailed implements Action {
  readonly type = RegistrationActionTypes.REGISTRATION_SUBMIT_FAILED;
  constructor(public payload: HttpErrorResponse) {}
}

export class ResetRegistrationState implements Action {
  readonly type = RegistrationActionTypes.RESET_REGISTRATION_STATE;
}

export class RegistrationVerificationUser implements Action {
  readonly type = RegistrationActionTypes.REGISTRATION_USER_VERIFICATION;
  constructor(public payload: string) {}
}

export class RegistrationVerificationUserSuccess implements Action {
  readonly type = RegistrationActionTypes.REGISTRATION_USER_VERIFICATION_SUCCESS;
  constructor(public payload: boolean) {}
}

export class RegistrationVerificationUserFailed implements Action {
  readonly type = RegistrationActionTypes.REGISTRATION_USER_VERIFICATION_FAILED;
  constructor(public payload: boolean) {}
}

export type RegistrationActions =
  | RegistrationSubmit
  | RegistrationSubmitFailed
  | RegistrationSubmitSuccess
  | RegistrationVerificationUser
  | RegistrationVerificationUserSuccess
  | RegistrationVerificationUserFailed
  | ResetRegistrationState;
