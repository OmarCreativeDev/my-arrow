import { IRegistrationState, IRegistration } from '@app/core/registration/registration.interfaces';
import {
  registrationReducers,
  INITIAL_REGISTRATION_STATE,
} from '@app/features/public/components/registration-form/store/registration.reducers';
import {
  RegistrationSubmit,
  RegistrationSubmitSuccess,
  RegistrationSubmitFailed,
  RegistrationVerificationUserFailed,
  RegistrationVerificationUserSuccess,
  RegistrationVerificationUser,
} from '@app/features/public/components/registration-form/store/registration.actions';
import { HttpErrorResponse } from '@angular/common/http';

const registrationForm: IRegistration = {
  firstName: '',
  lastName: '',
  companyName: '',
  email: '',
  phone: '',
  salesRepEmail: '',
  jobDescription: '',
  billingStreet: '',
  billingCity: '',
  billingState: '',
  billingPostalCode: '',
  billingCountry: '',
  shippingStreet: '',
  shippingCity: '',
  shippingState: '',
  shippingPostalCode: '',
  shippingCountry: '',
  assistanceRequiredBySalesRep: false,
  arrowAccountNumber: '',
  source: '',
  acceptedTermsAndConditions: false,
};

describe('Registration Reducers', () => {
  let initialState: IRegistrationState;
  beforeEach(() => {
    initialState = INITIAL_REGISTRATION_STATE;
  });

  it('`REGISTRATION_SUBMIT` should set `loading` to true and not have an error property', () => {
    const action = new RegistrationSubmit(registrationForm);
    const result = registrationReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeNull();
  });

  it('`REGISTRATION_SUBMIT_SUCCESS` should set `loading` to false and userId to the email passed', () => {
    const action = new RegistrationSubmitSuccess('john@doe.com');
    const result = registrationReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.userId).toBe('john@doe.com');
    expect(result.error).toBeNull();
  });

  it('`REGISTRATION_SUBMIT_FAILED` should set `error`  and  userId to null', () => {
    const httpError = new HttpErrorResponse({ error: { error_description: 'error' } });
    const action = new RegistrationSubmitFailed(httpError);
    const result = registrationReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.userId).toBeNull();
    expect(result.error).toBe(httpError);
  });

  it('`REGISTRATION_USER_VERIFICATION_FAILED` should set `loadingEmail` to false and userExists to false', () => {
    const action = new RegistrationVerificationUserFailed(false);
    const result = registrationReducers(initialState, action);
    expect(result.loadingEmail).toBeFalsy();
    expect(result.userExists).toBeFalsy();
  });

  it('`REGISTRATION_USER_VERIFICATION_SUCCESS` should set `loadingEmail` to false and userExists to true', () => {
    const action = new RegistrationVerificationUserSuccess(true);
    const result = registrationReducers(initialState, action);
    expect(result.loadingEmail).toBeFalsy();
    expect(result.userExists).toBeTruthy();
  });

  it('`REGISTRATION_USER_VERIFICATION` should set `loadingEmail` to false and userExists to true', () => {
    const action = new RegistrationVerificationUser('test@test.com');
    const result = registrationReducers(initialState, action);
    expect(result.loadingEmail).toBeTruthy();
    expect(result.userExists).toBeFalsy();
  });
});
