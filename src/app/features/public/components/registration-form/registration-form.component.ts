import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { ICountry } from '@app/core/properties/properties.interface';
import { GetCountryList } from '@app/features/properties/store/properties.actions';
import { getCountryListSelector } from '@app/features/properties/store/properties.selectors';
import { CustomValidators } from '@app/shared/classes/custom-validators';
import { Router } from '@angular/router';
import { IRegistrationProcess, IJobType, IRegistration } from '@app/core/registration/registration.interfaces';
import { engineerJobTypes, purchaserJobTypes } from './jobTypes';
import { RegistrationSubmit, ResetRegistrationState } from '@app/features/public/components/registration-form/store/registration.actions';
import { DialogService, Dialog } from '@app/core/dialog/dialog.service';
import { AcceptTermsDialogComponent } from '@app/features/public/components/accept-terms-dialog/accept-terms-dialog.component';
import { RegistrationVerificationUser } from '@app/features/public/components/registration-form/store/registration.actions';
import {
  getLoadingFlagSelector,
  getUserExistsFlagSelector,
  getUserIdSelector,
  getLoadingEmailFlagSelector,
  getRegistrationErrorSelector,
} from '@app/features/public/components/registration-form/store/registration.selectors';
import { debounceTime, take, filter } from 'rxjs/operators';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls: ['./registration-form.component.scss'],
})
export class RegistrationFormComponent implements OnInit, OnDestroy {
  public registrationForm: FormGroup;
  public registrationProcessAccountNumber = IRegistrationProcess.ACCOUNT_NUMBER;
  public registrationProcessSRMEmail = IRegistrationProcess.SMR_EMAIL;
  public registrationProcessUnknown = IRegistrationProcess.UNKNOWN;
  public jobTypeEngineer = IJobType.ENGINEER;
  public jobTypePurchaser = IJobType.PURCHASER;
  public jobTypeSupplier = IJobType.SUPPLIER;
  public engineerJobTypes = engineerJobTypes;
  public purchaserJobTypes = purchaserJobTypes;
  public loading$: Observable<boolean>;
  public countryList$: Observable<ICountry[]>;
  public userExists$: Observable<boolean>;
  public loadingEmailFlag$: Observable<boolean>;
  public userExistsFlag: boolean = false;
  public countryListFinal: Array<any> = [
    {
      label: 'Select your country',
      value: '',
    },
  ];
  public shipCountry: String;
  public billCountry: String;
  public billingCountry = new FormControl('', Validators.required);
  public shippingCountry = new FormControl('', Validators.required);
  public email = new FormControl('', Validators.compose([CustomValidators.email, Validators.required]));
  public confirmEmail = new FormControl('', Validators.compose([CustomValidators.email, Validators.required]));
  public accountValuesDisabled: Boolean = false;
  public useSameShippingAndBilling = false;
  public submitted: boolean;
  public acceptTermsDialog: Dialog<AcceptTermsDialogComponent, any, any>;
  public subscription: Subscription = new Subscription();
  public registrationError: Error | string;

  constructor(private store: Store<IAppState>, private router: Router, private dialogService: DialogService) {
    this.countryList$ = this.store.pipe(select(getCountryListSelector));
    this.loading$ = this.store.pipe(select(getLoadingFlagSelector));
    this.loadingEmailFlag$ = this.store.pipe(select(getLoadingEmailFlagSelector));
    this.userExists$ = this.store.pipe(select(getUserExistsFlagSelector));
  }

  ngOnInit() {
    // This initialization order is important, please don't change
    this.createRegistrationForm();
    this.store.dispatch(new GetCountryList());
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    this.store.dispatch(new ResetRegistrationState());
    if (this.acceptTermsDialog) this.acceptTermsDialog.close();
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    // This subscription order is important, please don't change
    // Preliminary Subscriptions
    this.subscription.add(this.subscribeEmailValueChanges());
    this.subscription.add(this.subscribeLoadingEmailFlag());
    this.subscription.add(this.subscribeUserExists());
    this.subscription.add(this.goToErrorPageIfFails());
    this.subscription.add(this.goToSuccessPageIfUserIsCreated());
    // Form data subscriptions
    this.subscription.add(this.subscribeCountryList());
    // Form behavior subscriptions
    this.subscription.add(this.subscribeFormAccountDetailsProcess());
    this.subscription.add(this.subscribeJobType());
  }

  private subscribeEmailValueChanges(): Subscription {
    return this.email.valueChanges.pipe(debounceTime(2000)).subscribe(newValue => {
      this.store.dispatch(new RegistrationVerificationUser(newValue));
    });
  }

  private subscribeLoadingEmailFlag(): Subscription {
    return this.loadingEmailFlag$.subscribe(loading => {
      if (loading) {
        this.email.disable({ emitEvent: false });
      } else {
        this.email.enable({ emitEvent: false });
      }
    });
  }

  private subscribeUserExists(): Subscription {
    return this.userExists$.subscribe(exists => {
      this.userExistsFlag = exists;
    });
  }

  private subscribeCountryList(): Subscription {
    return this.countryList$.subscribe(countryList => this.transformCountryListInformation(countryList));
  }

  private subscribeFormAccountDetailsProcess(): Subscription {
    return this.registrationForm.get('accountDetailsProcess').valueChanges.subscribe((value: string) => {
      switch (value) {
        case IRegistrationProcess.ACCOUNT_NUMBER:
          this.updateValidators(['arrowAccountNumber'], [Validators.required]);
          this.updateValidators(['salesRepEmail'], null);
          this.registrationForm.get('salesRepEmail').reset();
          break;

        case IRegistrationProcess.SMR_EMAIL:
          this.updateValidators(['arrowAccountNumber'], null);
          this.updateValidators(['salesRepEmail'], [CustomValidators.email, Validators.required]);
          this.registrationForm.get('arrowAccountNumber').reset();
          break;

        case IRegistrationProcess.UNKNOWN:
          this.updateValidators(['arrowAccountNumber', 'salesRepEmail'], null);
          this.registrationForm.get('arrowAccountNumber').reset();
          this.registrationForm.get('salesRepEmail').reset();
          break;
      }
    });
  }

  private subscribeJobType(): Subscription {
    return this.registrationForm.get('jobType').valueChanges.subscribe((value: string) => {
      switch (value) {
        case IJobType.ENGINEER:
          this.updateValidators(['engineerJobType'], [Validators.required]);
          this.updateValidators(['purchaserJobType'], null);
          this.registrationForm.get('purchaserJobType').reset();
          break;

        case IJobType.PURCHASER:
          this.updateValidators(['engineerJobType'], null);
          this.updateValidators(['purchaserJobType'], [Validators.required]);
          this.registrationForm.get('engineerJobType').reset();
          break;

        case IJobType.SUPPLIER:
          this.updateValidators(['engineerJobType', 'purchaserJobType'], null);
          this.registrationForm.get('purchaserJobType').reset();
          this.registrationForm.get('engineerJobType').reset();
          break;
      }
    });
  }

  public navigateTo(path: string) {
    this.router.navigateByUrl(path);
  }

  public goToSuccessPageIfUserIsCreated() {
    return this.store
      .pipe(
        select(getUserIdSelector),
        filter(userId => userId !== null)
      )
      .subscribe(() => {
        this.navigateTo('/success');
      });
  }

  public goToErrorPageIfFails() {
    return this.store
      .pipe(
        select(getRegistrationErrorSelector),
        filter(error => error !== null)
      )
      .subscribe(error => {
        this.registrationError = error;
        this.navigateTo('/register/error');
      });
  }

  public openAcceptTermsDialog(): void {
    this.acceptTermsDialog = this.dialogService.open(AcceptTermsDialogComponent, { size: 'large' });
    /* istanbul ignore else */
    if (this.acceptTermsDialog) {
      this.acceptTermsDialog.afterClosed.pipe(take(1)).subscribe(acceptedTerms => {
        if (acceptedTerms) {
          const registration: IRegistration = this.prepareRegistrationEntity(this.registrationForm.value);
          this.store.dispatch(new RegistrationSubmit(registration));
        } else {
          this.navigateTo('/register');
        }
      });
    }
  }

  public avoidPaste(event) {
    event.preventDefault();
  }

  public updateValidators(fields: Array<any>, validators: Array<any>) {
    fields.forEach(field => {
      this.registrationForm.get(field).setValidators(Validators.compose(validators));
      this.registrationForm.get(field).updateValueAndValidity();
    });
  }

  public createRegistrationForm(): void {
    this.registrationForm = new FormGroup(
      {
        firstName: new FormControl('', CustomValidators.firstOrLastNames),
        lastName: new FormControl('', CustomValidators.firstOrLastNames),
        companyName: new FormControl('', Validators.required),
        phone: new FormControl('', CustomValidators.phoneNumber),
        email: this.email,
        confirmEmail: this.confirmEmail,

        accountDetailsProcess: new FormControl(IRegistrationProcess.SMR_EMAIL, Validators.required),
        arrowAccountNumber: new FormControl(),
        salesRepEmail: new FormControl('', Validators.compose([CustomValidators.email, Validators.required])),
        assistanceRequiredBySalesRepresentative: new FormControl(''),
        jobType: new FormControl(IJobType.ENGINEER, Validators.required),
        engineerJobType: new FormControl(null, Validators.required),
        purchaserJobType: new FormControl(),

        billingAddress1: new FormControl('', Validators.required),
        billingAddress2: new FormControl(''),
        billingCity: new FormControl('', Validators.compose([CustomValidators.onlyLettersAndSpaces, Validators.required])),
        billingCountry: this.billingCountry,
        billingRegion: new FormControl('', Validators.compose([CustomValidators.onlyLettersAndSpaces, Validators.required])),
        billingPostal: new FormControl('', Validators.required),

        shippingAddress1: new FormControl('', Validators.required),
        shippingAddress2: new FormControl(''),
        shippingCity: new FormControl('', Validators.compose([CustomValidators.onlyLettersAndSpaces, Validators.required])),
        shippingCountry: this.shippingCountry,
        shippingRegion: new FormControl('', Validators.compose([CustomValidators.onlyLettersAndSpaces, Validators.required])),
        shippingPostal: new FormControl('', Validators.required),
      },
      {
        validators: [
          Validators.compose([Validators.required, CustomValidators.equal(this.email, this.confirmEmail, 'matchingEmails')]),
          CustomValidators.dropwdownRequired(this.billingCountry),
          CustomValidators.dropwdownRequired(this.shippingCountry),
        ],
        updateOn: 'change',
      }
    );
  }

  get showShippingCountryValidationError() {
    return this.submitted && this.shippingCountry.invalid && this.registrationForm.errors;
  }

  get showBillingCountryValidationError() {
    return this.submitted && this.billingCountry.invalid && this.registrationForm.errors;
  }

  get showEmailMatchingError() {
    return this.email.valid && (this.registrationForm.errors && this.registrationForm.errors['matchingEmails']) ? true : false;
  }

  get showEngineerJobTypeError() {
    return (
      this.submitted &&
      this.registrationForm.controls['engineerJobType'].invalid &&
      this.registrationForm.controls['engineerJobType'].errors
    );
  }

  get showPurchaserJobTypeError() {
    return (
      this.submitted &&
      this.registrationForm.controls['purchaserJobType'].invalid &&
      this.registrationForm.controls['purchaserJobType'].errors
    );
  }

  private useShippingAsBilling() {
    const {
      shippingAddress1,
      shippingAddress2,
      shippingCity,
      shippingCountry,
      shippingPostal,
      shippingRegion,
    } = this.registrationForm.value;
    this.registrationForm.patchValue({
      billingAddress1: shippingAddress1,
      billingAddress2: shippingAddress2,
      billingCity: shippingCity,
      billingCountry: shippingCountry,
      billingRegion: shippingRegion,
      billingPostal: shippingPostal,
    });
  }

  public signUp() {
    this.submitted = true;
    /* istanbul ignore else */
    if (this.useSameShippingAndBilling) {
      this.useShippingAsBilling();
    }
    /* istanbul ignore else */
    if (this.registrationForm.valid) {
      this.openAcceptTermsDialog();
    }
  }

  public transformCountryListInformation(countryList) {
    if (countryList.length > 1) {
      this.countryListFinal = countryList.map(country => ({ label: country.name, value: country.code }));
      this.countryListFinal.unshift({
        label: 'Select your country',
        value: '',
      });
    }
  }

  public updateShippingCountry($event) {
    this.shipCountry = $event;
  }

  public updateBillingCountry($event) {
    this.billCountry = $event;
  }

  public namesFieldValidator(event) {
    const pattern = /^[a-zA-Z\s']+$/;
    this.testPattern(pattern, event);
  }

  public phoneFieldValidator(event) {
    const pattern = /^[\d\s]+$/;
    this.testPattern(pattern, event);
  }

  private testPattern(pattern, event) {
    if (pattern !== null && event !== null && event.keyCode !== 8) {
      const inputChar = String.fromCharCode(event.charCode || event.keyCode);
      if (!pattern.test(inputChar)) {
        event.preventDefault();
      }
    }
  }

  public preventNumbers(event) {
    const charCode = event.which || event.keyCode;
    const charStr = String.fromCharCode(charCode);
    /* istanbul ignore else */
    if (/\d/.test(charStr)) {
      event.preventDefault();
    }
  }

  private prepareRegistrationEntity(form) {
    const jobDescription =
      form.jobType === IJobType.PURCHASER ? form.purchaserJobType : form.jobType === IJobType.ENGINEER ? form.engineerJobType : 'Supplier';
    const assistanceRequiredBySalesRep = form.accountDetailsProcess === IRegistrationProcess.UNKNOWN;
    const shippingStreet = form.shippingAddress2 ? `${form.shippingAddress1} ${form.shippingAddress2}` : form.shippingAddress1;
    const billingStreet = form.billingAddress2 ? `${form.billingAddress1} ${form.billingAddress2}` : form.billingAddress1;
    const registration: IRegistration = {
      firstName: form.firstName,
      lastName: form.lastName,
      companyName: form.companyName,
      email: form.email,
      phone: form.phone,
      salesRepEmail: form.salesRepEmail,
      jobDescription: jobDescription,
      billingStreet: billingStreet,
      billingCity: form.billingCity,
      billingState: form.billingRegion,
      billingPostalCode: form.billingPostal,
      billingCountry: form.billingCountry,
      shippingStreet: shippingStreet,
      shippingCity: form.shippingCity,
      shippingState: form.shippingRegion,
      shippingPostalCode: form.shippingPostal,
      shippingCountry: form.shippingCountry,
      assistanceRequiredBySalesRep: assistanceRequiredBySalesRep,
      arrowAccountNumber: form.arrowAccountNumber,
      source: 'OneWeb',
      acceptedTermsAndConditions: true,
    };

    return registration;
  }
}
