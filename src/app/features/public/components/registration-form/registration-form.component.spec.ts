import { CoreModule } from '@app/core/core.module';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '@app/shared/shared.module';
import { propertiesReducers, INITIAL_RESULTS_STATE } from '@app/features/properties/store/properties.reducers';
import {
  registrationReducers,
  INITIAL_REGISTRATION_STATE,
} from '@app/features/public/components/registration-form/store/registration.reducers';
import { RegistrationFormComponent } from './registration-form.component';
import { FormErrorMessageComponent } from '@app/features/public/components/form-error-message/form-error-message.component';
import { AcceptTermsDialogComponent } from '@app/features/public/components/accept-terms-dialog/accept-terms-dialog.component';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';

class MockRouter {
  navigateByUrl(url: string) {
    return url;
  }
}
const inititalState = {
  properties: INITIAL_RESULTS_STATE,
  registration: INITIAL_REGISTRATION_STATE,
};
describe('RegistrationFormComponent', () => {
  let component: RegistrationFormComponent;
  let fixture: ComponentFixture<RegistrationFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
        StoreModule.forRoot(
          {
            properties: propertiesReducers,
            registration: registrationReducers,
          },
          { initialState: inititalState }
        ),
      ],
      declarations: [RegistrationFormComponent, FormErrorMessageComponent, AcceptTermsDialogComponent],
      providers: [{ provide: Router, useClass: MockRouter }, { provide: DialogService, useClass: DialogServiceMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create the registration form', () => {
    spyOn(component, 'createRegistrationForm').and.callThrough();
    component.ngOnInit();
    expect(component.createRegistrationForm).toHaveBeenCalled();
  });

  it('should validate if the email has a correct format', () => {
    component.email.setValue('test@gmail.com');
    expect(component.email.errors).toEqual(null);
  });

  it('should validate if the email has no a correct format', () => {
    component.email.setValue('test.gmail');
    expect(component.email.errors).not.toEqual(null);
  });

  it('should validate if the countryList was populated', () => {
    spyOn(component, 'transformCountryListInformation').and.callThrough();
    component.ngOnInit();
    expect(component.transformCountryListInformation).toHaveBeenCalled();
  });

  it('should be valid when the billingCountry was selected', () => {
    component.registrationForm.controls['billingCountry'].setValue('US');
    expect(component.registrationForm.controls['billingCountry'].valid).toEqual(true);
  });

  it('should be valid when the shippingCountry was selected', () => {
    component.registrationForm.controls['shippingCountry'].setValue('US');
    expect(component.registrationForm.controls['shippingCountry'].valid).toEqual(true);
  });

  it('should be valid when the jobType was selected', () => {
    component.registrationForm.controls['jobType'].setValue('Engineer');
    expect(component.registrationForm.controls['jobType'].valid).toEqual(true);
  });

  it('should copy the values of shipping address to billing address when "Same as Shipping Address" is checked', () => {
    component.registrationForm.controls['shippingAddress1'].setValue('123 Ave');
    component.registrationForm.controls['shippingAddress2'].setValue('Suite 3');
    component.registrationForm.controls['shippingCity'].setValue('Denver');
    component.registrationForm.controls['shippingCountry'].setValue('US');
    component.registrationForm.controls['shippingRegion'].setValue('CO');
    component.registrationForm.controls['shippingPostal'].setValue('80014');
    component.useSameShippingAndBilling = true;
    component.signUp();
    expect(component.registrationForm.controls['billingAddress1'].value).toBe(
      component.registrationForm.controls['shippingAddress1'].value
    );
    expect(component.registrationForm.controls['billingAddress2'].value).toBe(
      component.registrationForm.controls['shippingAddress2'].value
    );
    expect(component.registrationForm.controls['billingCity'].value).toBe(component.registrationForm.controls['shippingCity'].value);
    expect(component.registrationForm.controls['billingCountry'].value).toBe(component.registrationForm.controls['shippingCountry'].value);
    expect(component.registrationForm.controls['billingRegion'].value).toBe(component.registrationForm.controls['shippingRegion'].value);
    expect(component.registrationForm.controls['billingPostal'].value).toBe(component.registrationForm.controls['shippingPostal'].value);
  });

  it('should register a new user when the signUp method is called', () => {
    component.registrationForm.controls['firstName'].setValue('Test');
    component.registrationForm.controls['lastName'].setValue('Test');
    component.registrationForm.controls['companyName'].setValue('ACM');
    component.registrationForm.controls['phone'].setValue('1212121212');
    component.registrationForm.controls['email'].setValue('test@test.com');
    component.registrationForm.controls['confirmEmail'].setValue('test@test.com');

    component.registrationForm.controls['salesRepEmail'].setValue('test@test.com');
    component.registrationForm.controls['jobType'].setValue('Purchaser');
    component.registrationForm.controls['purchaserJobType'].setValue('Sales/Marketing Mgmt.');

    component.registrationForm.controls['accountDetailsProcess'].setValue('SMR_EMAIL');
    component.registrationForm.controls['shippingAddress1'].setValue('123 Ave');
    component.registrationForm.controls['shippingCity'].setValue('Denver');
    component.registrationForm.controls['shippingCountry'].setValue('US');
    component.registrationForm.controls['shippingRegion'].setValue('CO');
    component.registrationForm.controls['shippingPostal'].setValue('80014');
    component.useSameShippingAndBilling = true;
    spyOn(component, 'openAcceptTermsDialog').and.callThrough();
    component.signUp();
    expect(component.registrationForm.valid).toBeTruthy();
    expect(component.openAcceptTermsDialog).toHaveBeenCalled();
  });
});
