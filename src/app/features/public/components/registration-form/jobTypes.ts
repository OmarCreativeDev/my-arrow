import { ISelectableOption } from '@app/shared/shared.interfaces';

export const engineerJobTypes: Array<ISelectableOption<any>> = [
  {
    label: 'Design Engineer',
    value: 'Design Engineer',
  },
  {
    label: 'Component Engineer',
    value: 'Component Engineer',
  },
  {
    label: 'Engineering Manager',
    value: 'Engineering Manager',
  },
  {
    label: 'Manufacturing Engineer',
    value: 'Manufacturing Engineer',
  },
  {
    label: 'Reliability Engineer',
    value: 'Reliability Engineer',
  },
  {
    label: 'Systems Engineer',
    value: 'Systems Engineer',
  },
];

export const purchaserJobTypes: Array<ISelectableOption<any>> = [
  {
    label: 'Corporate/General Mgmt.',
    value: 'Corporate/General Mgmt.',
  },
  {
    label: 'Operations/Manuf./Production Mgmt.',
    value: 'Operations/Manuf./Production Mgmt.',
  },
  {
    label: 'Purchasing (Buyer, Planner)',
    value: 'Purchasing (Buyer, Planner)',
  },
  {
    label: 'Sales/Marketing Mgmt.',
    value: 'Sales/Marketing Mgmt.',
  },
  {
    label: 'Purchasing Mgmt./Materials Mgmt.',
    value: 'Purchasing Mgmt./Materials Mgmt.',
  },
];
