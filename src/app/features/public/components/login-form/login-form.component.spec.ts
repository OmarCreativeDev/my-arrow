import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from '@app/core/auth/auth.service';
import { CoreModule } from '@app/core/core.module';
import { UserLoggedIn } from '@app/core/user/store/user.actions';
import { userReducers } from '@app/core/user/store/user.reducers';
import { SharedModule } from '@app/shared/shared.module';
import { HttpErrorResponse } from '@angular/common/http';
import { Store, StoreModule } from '@ngrx/store';
import { Observable, of, throwError } from 'rxjs';

import { LoginFormComponent } from './login-form.component';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';

class StoreMock {
  public select(): Observable<any> {
    return of({});
  }
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

class DummyComponent {}

describe('LoginFormComponent', () => {
  const routes = [{ path: 'dashboard', component: DummyComponent }];
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;
  let authService: AuthService;

  let usernameField;
  let passwordField;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({ user: userReducers }),
        RouterTestingModule.withRoutes(routes),
        CoreModule,
        FormsModule,
        ReactiveFormsModule,
        SharedModule,
      ],
      declarations: [LoginFormComponent],
      providers: [AuthService, { provide: Store, useClass: StoreMock }, { provide: WSSService, useClass: MockStompWSSService }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    authService = TestBed.get(AuthService);
    usernameField = component.loginForm.controls.username;
    passwordField = component.loginForm.controls.password;
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be valid with correct values values set', () => {
    usernameField.setValue('username@email.com');
    passwordField.setValue('password');
    expect(component.loginForm.valid).toBeTruthy();
  });

  it('should be invalid with client-side errors', fakeAsync(() => {
    spyOn(authService, 'login').and.returnValue(of({}));
    usernameField.setValue('email');
    passwordField.setValue('password');
    component.login();
    expect(component.loginForm.valid).toBeFalsy();
  }));

  it('should call User LoggedIn action when logged in', fakeAsync(() => {
    spyOn(authService, 'login').and.returnValue(of({}));
    usernameField.setValue('username@email.com');
    passwordField.setValue('password');
    component.login();
    expect(store.dispatch).toHaveBeenCalledWith(new UserLoggedIn());
  }));

  it('should capture authentication error from service', fakeAsync(() => {
    const httpError = new HttpErrorResponse({
      status: 400,
      error: { error_description: 'Authentication for user username@email.com failed' },
    });
    spyOn(authService, 'login').and.returnValue(throwError(httpError));
    usernameField.setValue('username@email.com');
    passwordField.setValue('password');
    component.login();
    expect(component.isUserBlocked).toBeFalsy();
    expect(component.hasAuthenticationError).toBeTruthy();
  }));

  it('should capture when user is blocked from service', fakeAsync(() => {
    const httpError = new HttpErrorResponse({
      status: 400,
      error: { error_description: '[2001]: Account for user username@email.com is currently locked' },
    });
    spyOn(authService, 'login').and.returnValue(throwError(httpError));
    usernameField.setValue('username@email.com');
    passwordField.setValue('password');
    component.login();
    expect(component.hasAuthenticationError).toBeFalsy();
    expect(component.isUserBlocked).toBeTruthy();
  }));
});
