import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserLoggedIn, UserLoggedInFailed } from '@app/core/user/store/user.actions';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import { AuthService } from '../../../../core/auth/auth.service';
import { HttpErrorResponse } from '@angular/common/http';
import { CustomValidators } from '@app/shared/classes/custom-validators';
import { getPublicFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit, OnDestroy {
  public loginForm: FormGroup;
  public hasAuthenticationError = false;
  public loggingIn = false;
  public isUserBlocked = false;

  public publicFeatureFlags: any;

  private subscription: Subscription = new Subscription();
  private publicFeatures$: Observable<any>;

  constructor(private store: Store<IAppState>, private authService: AuthService) {
    this.publicFeatures$ = store.pipe(select(getPublicFeatureFlagsSelector));
  }

  ngOnInit() {
    this.startSubscriptions();
    this.createLoginForm();
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribePublicFeatures());
  }

  private subscribePublicFeatures(): Subscription {
    return this.publicFeatures$.subscribe(featureFlags => {
      this.publicFeatureFlags = featureFlags;
    });
  }

  public createLoginForm(): void {
    this.loginForm = new FormGroup(
      {
        username: new FormControl('', CustomValidators.email),
        password: new FormControl('', Validators.required),
      },
      { updateOn: 'change' }
    );
  }

  /**
   * Authenticates user and redirects to the dashboard
   */
  public login() {
    if (this.loginForm.valid) {
      const val = this.loginForm.value;
      this.loggingIn = true;
      this.authService.login(val.username, val.password).subscribe(
        () => {
          this.store.dispatch(new UserLoggedIn());
        },
        err => this.handleLoginError(err)
      );
    } else {
      this.hasAuthenticationError = false;
    }
  }

  public handleLoginError(err: HttpErrorResponse) {
    const errorDetail = err.error;

    if (errorDetail.error_description && errorDetail.error_description.startsWith('[2001]')) {
      this.hasAuthenticationError = false;
      this.isUserBlocked = true;
    } else {
      this.isUserBlocked = false;
      this.hasAuthenticationError = true;
    }
    this.loggingIn = false;
    this.store.dispatch(new UserLoggedInFailed('Authentication Failure'));
  }
}
