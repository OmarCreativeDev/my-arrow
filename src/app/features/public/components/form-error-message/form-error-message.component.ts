import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-form-error-message',
  templateUrl: './form-error-message.component.html',
})
export class FormErrorMessageComponent {
  @Input()
  cssClass: string;
  constructor() {}
}
