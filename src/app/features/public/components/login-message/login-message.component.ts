import { Component, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { ClearUserError } from '@app/core/user/store/user.actions';

@Component({
  selector: 'app-login-message',
  templateUrl: './login-message.component.html',
})
export class LoginMessageComponent implements OnDestroy {
  @Output()
  loginAsDifferentUser: EventEmitter<boolean> = new EventEmitter();

  constructor(private store: Store<IAppState>) {}

  ngOnDestroy(): void {
    this.store.dispatch(new ClearUserError());
  }
}
