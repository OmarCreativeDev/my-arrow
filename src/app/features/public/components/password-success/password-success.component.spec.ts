import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CoreModule } from '@app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { StoreModule } from '@ngrx/store';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PasswordSuccessComponent } from '@app/features/public/components/password-success/password-success.component';
import { SharedModule } from '@app/shared/shared.module';

describe('PasswordSuccessComponent', () => {
  let component: PasswordSuccessComponent;
  let fixture: ComponentFixture<PasswordSuccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}), RouterTestingModule, CoreModule, FormsModule, ReactiveFormsModule, SharedModule],
      declarations: [PasswordSuccessComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordSuccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
