import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-pwd-success',
  templateUrl: './password-success.component.html',
})
export class PasswordSuccessComponent {
  @Input()
  message: string = '';

  constructor() {}
}
