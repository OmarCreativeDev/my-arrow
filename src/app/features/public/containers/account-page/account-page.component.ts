import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-account-page',
  templateUrl: './account-page.component.html',
  styleUrls: ['./account-page.component.scss']
})
export class AccountPageComponent {

  @Input() title: string;

  constructor() { }

  get displayTitle() {
    return this.title.replace(/(MyArrow)/ig,
      `<span class="myarrow-logo logo-inline${this.title.startsWith('MyArrow') ? ' startswith' : ''}">$1</span>`);
  }

}
