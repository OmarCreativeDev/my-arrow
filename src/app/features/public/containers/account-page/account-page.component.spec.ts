import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountPageComponent } from './account-page.component';

describe('AccountPageComponent', () => {
  let component: AccountPageComponent;
  let fixture: ComponentFixture<AccountPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AccountPageComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountPageComponent);
    component = fixture.componentInstance;
    component.title = 'Create a MyArrow account';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should replace MyArrow text with logo', () => {
    expect(component.displayTitle).toEqual(`Create a <span class="myarrow-logo logo-inline">MyArrow</span> account`);
  });
});
