import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@app/core/auth/auth.guard';
import { PublicFeaturesGuard } from '@app/core/features/public-features.guard';
import { LoginGuard } from '@app/core/login/login.guard';
import { PasswordResetGuard } from '@app/core/password-reset/password-reset.guard';
import { UserGuard } from '@app/core/user/user.guard';

import { LinkExpiredComponent } from './pages/link-expired/link-expired.component';
import { LoginComponent } from './pages/login/login.component';
import { PasswordResetComponent } from './pages/password-reset/password-reset.component';
import { RecoverPasswordComponent } from './pages/recover-password/recover-password.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { RequestErrorComponent } from './pages/request-error/request-error.component';
import { SuccessComponent } from './pages/success/success.component';
import { TermsOfUseComponent } from './pages/terms-of-use/terms-of-use.component';
import { UserErrorComponent } from './pages/user-error/user-error.component';
import { RegistrationErrorComponent } from './pages/registration-error/registration-error.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent, canActivate: [LoginGuard], data: { title: 'Login' } },
  { path: 'register', component: RegistrationComponent, canActivate: [LoginGuard], data: { title: 'Register' } },
  { path: 'register/error', component: RegistrationErrorComponent, canActivate: [LoginGuard], data: { title: 'Register Error' } },
  { path: 'success', component: SuccessComponent, canActivate: [LoginGuard], data: { title: 'Register' } },
  { path: 'terms-of-use', component: TermsOfUseComponent, canActivate: [AuthGuard, UserGuard], data: { title: 'Terms of Use' } },
  {
    path: 'recover-password',
    component: RecoverPasswordComponent,
    canActivate: [PublicFeaturesGuard],
    data: { title: 'Recover Password' },
  },
  {
    path: 'password-reset',
    component: PasswordResetComponent,
    canActivate: [PasswordResetGuard],
    data: { title: 'Reset Password' },
  },
  { path: 'user-error', component: UserErrorComponent, data: { title: 'Error' } },
  { path: 'link-expired', component: LinkExpiredComponent, data: { title: 'Reset Password' } },
  { path: 'request-error', component: RequestErrorComponent, data: { title: 'Error' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class UserRoutingModule {}
