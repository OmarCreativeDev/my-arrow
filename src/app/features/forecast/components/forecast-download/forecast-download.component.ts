import { Component, EventEmitter, Output, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription, zip, interval, from } from 'rxjs';
import { map } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';

import { IUser } from '@app/core/user/user.interface';
import { getUser } from '@app/core/user/store/user.selectors';

import { IForecastDownloadFileTypes } from '@app/core/forecast/forecast.interfaces';
import { Dialog } from '@app/core/dialog/dialog.service';
import { IAppState } from '@app/shared/shared.interfaces';
import { IForecastDownload } from '@app/core/forecast/forecast.interfaces';
import { fileTypeOptions, columns, downloadDateRange } from './forecast-download.models';
import { ISortableItem } from '@app/shared/shared.interfaces';
import { errorsMessageHeader, errorsMessageBody } from '@app/features/errors/pages/errors.message';
import {
  getForecastDownloadErrorSelector,
  getForecastStatus,
} from '@app/features/forecast/stores/forecast-download/forecast-download.selectors';
import {
  GetForecastDownloadFile,
  GetForecastDownloadFileReset,
  OpenedForecastDownloadModal,
  SelectedForecastDownloadFileColumns,
  SelectedForecastDownloadFileDateRange,
  SelectedForecastDownloadFileType,
} from '@app/features/forecast/stores/forecast-download/forecast-download.actions';

@Component({
  selector: 'app-forecast-download',
  templateUrl: './forecast-download.component.html',
  styleUrls: ['./forecast-download.component.scss'],
})
export class ForecastDownloadComponent implements OnInit, OnDestroy {
  @Output()
  public close: EventEmitter<boolean> = new EventEmitter();
  @Output()
  initialColumns = new EventEmitter();

  fileType: IForecastDownloadFileTypes = 'XLS';
  selectedColumns = [];
  noData: boolean = false;
  userProfile$: Observable<IUser>;
  weeksInHorizon: number = 52;
  billToNumber: number;
  accountNumber: number;
  salesRepName: string;
  fileTypeOptions = fileTypeOptions;
  columns: Array<ISortableItem> = columns;
  downloadDateRange = downloadDateRange;
  errors: object;
  errorHeader: string = errorsMessageHeader;
  errorBody: string = errorsMessageBody;
  private subscription: Subscription = new Subscription();
  downloadError$: Observable<any>;
  status$: Observable<any>;
  status: number;

  constructor(private store: Store<IAppState>, private dialog: Dialog<ForecastDownloadComponent>) {
    this.userProfile$ = this.store.pipe(select(getUser));
    this.status$ = this.store.pipe(select(getForecastStatus));
    this.downloadError$ = store.pipe(select(getForecastDownloadErrorSelector));
  }

  subAddUser(): Subscription {
    return this.userProfile$.subscribe(user => {
      if (user) {
        this.accountNumber = user.accountNumber;
        this.billToNumber = user.selectedBillTo;
        this.salesRepName = user.contact.salesRepName;
      }
    });
  }

  subGetError(): Subscription {
    return this.downloadError$.subscribe(error => {
      this.errors = error;
    });
  }

  subGetStatus(): Subscription {
    return this.status$.subscribe(status => {
      this.status = status;
      if (this.status === 204) {
        this.handleNoData();
      } else if (this.status === 200) {
        this.store.dispatch(new GetForecastDownloadFileReset());
        this.onClose();
      }
    });
  }

  ngOnInit(): void {
    this.startSubscriptions();
    this.store.dispatch(
      new OpenedForecastDownloadModal({
        url: window.location.href,
      })
    );
  }

  private startSubscriptions() {
    this.subscription.add(this.subAddUser());
    this.subscription.add(this.subGetError());
    this.subscription.add(this.subGetStatus());
  }

  getDownloadParams(): IForecastDownload {
    const params: any = {
      accountNumber: this.accountNumber,
      billToNumber: this.billToNumber,
      fileType: this.fileType,
      smrName: this.salesRepName,
      columns: this.selectedColumns.map(function(item) {
        return item.propName;
      }),
      weeksInHorizon: this.weeksInHorizon,
    };

    return params;
  }

  public handleNoData(): void {
    this.noData = true;
  }

  download(): void {
    const params = this.getDownloadParams();
    this.store.dispatch(new GetForecastDownloadFile(params));
    this.subscription.add(this.analyticsSubscription(params));
  }

  analyticsSubscription(params): Subscription {
    const actions = [SelectedForecastDownloadFileColumns, SelectedForecastDownloadFileType, SelectedForecastDownloadFileDateRange];
    return zip(from(actions), interval(500))
      .pipe(map(([action]) => action))
      .subscribe(action => {
        this.store.dispatch(new action(params));
      });
  }

  onClose(): void {
    this.dialog.close();
    this.noData = false;
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }
}
