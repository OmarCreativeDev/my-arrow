import { ISortableItem } from '@app/shared/shared.interfaces';

export const fileTypeOptions = [
  {
    value: 'XLS',
    label: '.xls',
  },
  {
    value: 'XLSX',
    label: '.xlsx',
  },
  {
    value: 'CSV',
    label: '.csv',
  },
];

export const columns: Array<ISortableItem> = [
  {
    id: 0,
    label: 'Customer Part Number',
    propName: 'customerPartNumber',
  },
  {
    id: 1,
    label: 'Manufacturer Part Number',
    propName: 'manufacturerPartNumber',
  },
  {
    id: 2,
    label: 'Manufacturer Name',
    propName: 'manufacturerName',
  },
  {
    id: 3,
    label: 'Public Inventory',
    propName: 'publicInventory',
  },
  {
    id: 4,
    label: 'Buffer',
    propName: 'buffer',
  },
  {
    id: 5,
    label: 'Mult',
    propName: 'mult',
  },
  {
    id: 6,
    label: 'Item Status',
    propName: 'itemStatus',
  },

  {
    id: 7,
    label: 'Lead Time',
    propName: 'leadTime',
  },

  {
    id: 8,
    label: 'Pipeline',
    propName: 'pipeline',
  },
  {
    id: 9,
    label: 'Bill-To Location ID',
    propName: 'billToLocationId',
  },
  {
    id: 10,
    label: 'Ship-To Location ID',
    propName: 'shipToLocationId',
  },
  {
    id: 11,
    label: 'Inventory Item ID',
    propName: 'inventoryItemId',
  },
  {
    id: 12,
    label: '1st Short',
    propName: 'firstShort',
  },
  {
    id: 13,
    label: "PO's in transit",
    propName: 'posInTransit',
  },
];

export const downloadDateRange = [
  {
    label: '2 weeks',
    value: 2,
  },
  {
    label: '8 weeks',
    value: 8,
  },
  {
    label: '13 weeks',
    value: 13,
  },
  {
    label: '24 weeks',
    value: 24,
  },
  {
    label: '52 weeks',
    value: 52,
  },
];
