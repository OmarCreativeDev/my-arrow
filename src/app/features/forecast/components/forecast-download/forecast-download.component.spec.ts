import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CoreModule } from '@app/core/core.module';
import { of } from 'rxjs';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import * as FileSaver from 'file-saver';
import { Store, StoreModule } from '@ngrx/store';

import { ForecastDownloadComponent } from './forecast-download.component';
import { Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { FileService } from '@app/shared/services/file.service';
import { ForecastService } from '@app/core/forecast/forecast.service';
import { BrowserService } from '@app/shared/services/browser.service';
import { userReducers } from '@app/core/user/store/user.reducers';
import { forecastReducers } from '../../stores/forecast/forecast.reducers';
import { GetForecastDownloadFile } from '../../stores/forecast-download/forecast-download.actions';

class MockStore {
  select() {
    return of();
  }
  dispatch() {}
  pipe() {
    return of();
  }
}

describe('ForecastDownloadComponent', () => {
  let component: ForecastDownloadComponent;
  let fixture: ComponentFixture<ForecastDownloadComponent>;
  let forecastService: ForecastService;
  let fileService: FileService;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ForecastDownloadComponent],
      imports: [
        StoreModule.forRoot({
          user: userReducers,
          forecast: forecastReducers,
        }),
        CoreModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [FileService, BrowserService, { provide: Dialog, useClass: DialogMock }, { provide: Store, useClass: MockStore }],
    }).compileComponents();
    forecastService = TestBed.get(ForecastService);
    fileService = TestBed.get(FileService);
    store = TestBed.get(Store);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastDownloadComponent);
    component = fixture.componentInstance;
    component.accountNumber = 1067283;
    component.billToNumber = 2182504;
    component.salesRepName = 'Test Rep';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onClose', () => {
    spyOn(component, 'onClose').and.callThrough();
    component.onClose();
    fixture.detectChanges();
    expect(component.onClose).toHaveBeenCalled();
    expect(component.noData).toEqual(false);
  });

  it('should call correctly and GetForecastDownloadFile when download() is invoked', () => {
    spyOn(forecastService, 'getForecastFile').and.callFake(() => of({ status: 200 }));
    spyOn(FileSaver, 'saveAs').and.callFake(() => {});
    spyOn(fileService, 'saveResponse').and.callThrough();
    spyOn(store, 'dispatch').and.callThrough();

    component.download();

    expect(store.dispatch).toHaveBeenCalledWith(new GetForecastDownloadFile(component.getDownloadParams()));
  });
});
