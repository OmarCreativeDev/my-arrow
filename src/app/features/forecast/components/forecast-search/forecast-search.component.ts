import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { ISelectableOption, ISearchCriteria } from '@app/shared/shared.interfaces';
import { find } from 'lodash-es';

@Component({
  selector: 'app-forecast-search',
  templateUrl: './forecast-search.component.html',
  styleUrls: ['./forecast-search.component.scss'],
})
export class ForecastSearchComponent implements OnInit {
  @Input()
  searchCriteria: ISearchCriteria;
  @Output()
  search = new EventEmitter<ISearchCriteria>();

  public searchOptions: Array<ISelectableOption<any>> = [
    { label: 'ANY', value: 'any', payload: { placeholder: 'Search by MFR, CPN and MPN' } },
    { label: 'MFR', value: 'manufacturerName', payload: { placeholder: 'Search by MFR' } },
    { label: 'CPN', value: 'customerPartNumber', payload: { placeholder: 'Search by CPN' } },
    { label: 'MPN', value: 'arrowPartNumber', payload: { placeholder: 'Search by MPN' } },
  ];

  public selectedSearchOption: ISelectableOption<any>;

  /**
   * Local state which is managed locally and broadcast to action
   * that replaces the value in the RxJS state
   */
  public searchState: ISearchCriteria;

  constructor() {
    this.searchState = this.getEmptySearchState();
    this.updateSearchType(this.searchOptions[0].value);
  }

  /**
   * Submits the default search options when initted
   * TODO: This would be better placed in the constructor
   */
  ngOnInit() {
    this.submitSearch();
  }

  /**
   * Submits the locally-stored searchCriteria
   */
  public submitSearch() {
    this.search.emit({ ...this.searchState });
  }

  /**
   * Resetting the form effectively creates a new searchCriteria and emits to the parent container
   */
  public resetForm() {
    this.searchState = this.getEmptySearchState();
    this.searchCriteria = this.getEmptySearchState();
    this.submitSearch();
  }

  public updateSearchType($event) {
    this.searchState.type = $event;
    this.selectedSearchOption = find(this.searchOptions, ['value', this.searchState.type]);
  }

  /**
   * Create a blank, 'default' searchCriteria
   */
  public getEmptySearchState(): ISearchCriteria {
    return {
      value: '',
      type: this.searchOptions[0].value,
    };
  }
}
