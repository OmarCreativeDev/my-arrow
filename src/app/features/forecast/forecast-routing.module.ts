import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForecastComponent } from '@app/features/forecast/pages/forecast/forecast.component';
import { ForecastDetailComponent } from '@app/features/forecast/pages/forecast-detail/forecast-detail.component';

const routes: Routes = [
  {
    path: '',
    component: ForecastComponent,
    data: { title: 'Forecast' },
  },
  {
    path: 'details',
    component: ForecastDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class ForecastRoutingModule {}
