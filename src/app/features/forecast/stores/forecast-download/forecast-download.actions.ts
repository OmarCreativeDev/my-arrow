import { Action } from '@ngrx/store';

import { IForecastDownload, IForecastDownloadModal } from '@app/core/forecast/forecast.interfaces';

export enum ForecastDownloadActionTypes {
  GET_FORECAST_DOWNLOAD_FILE = 'GET_FORECAST_DOWNLOAD_FILE',
  GET_FORECAST_DOWNLOAD_FILE_SUCCESS = 'GET_FORECAST_DOWNLOAD_FILE_SUCCESS',
  GET_FORECAST_DOWNLOAD_FILE_FAILED = 'GET_FORECAST_DOWNLOAD_FILE_FAILED',
  GET_FORECAST_DOWNLOAD_RESET = 'GET_FORECAST_DOWNLOAD_RESET',
  OPENED_FORECAST_DOWNLOAD_MODAL ='OPENED_FORECAST_DOWNLOAD_MODAL',
  SELECTED_FORECAST_DOWNLOAD_FILE_TYPE = 'SELECTED_FORECAST_DOWNLOAD_FILE_TYPE',
  SELECTED_FORECAST_DOWNLOAD_FILE_COLUMNS = 'SELECTED_FORECAST_DOWNLOAD_FILE_COLUMNS',
  SELECTED_FORECAST_DOWNLOAD_FILE_DATE_RANGE = 'SELECTED_FORECAST_DOWNLOAD_FILE_DATE_RANGE',
}

/**
 * * Get Forecast Download File
 */
export class GetForecastDownloadFile implements Action {
  readonly type = ForecastDownloadActionTypes.GET_FORECAST_DOWNLOAD_FILE;
  constructor(public payload: IForecastDownload) {}
}

/**
 * * Get Forecast Download File Success
 */
export class GetForecastDownloadFileSuccess implements Action {
  readonly type = ForecastDownloadActionTypes.GET_FORECAST_DOWNLOAD_FILE_SUCCESS;
  constructor(public payload: number) {}
}

/**
 * * Get Forecast Download File Failed
 */
export class GetForecastDownloadFileFailed implements Action {
  readonly type = ForecastDownloadActionTypes.GET_FORECAST_DOWNLOAD_FILE_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Reset Forecast Download File
 */
export class GetForecastDownloadFileReset implements Action {
  readonly type = ForecastDownloadActionTypes.GET_FORECAST_DOWNLOAD_RESET;
  constructor() {}
}

/**
 * * Opened the Forecast Download Modal
 */
export class OpenedForecastDownloadModal implements Action {
  readonly type = ForecastDownloadActionTypes.OPENED_FORECAST_DOWNLOAD_MODAL;
  constructor(public payload: IForecastDownloadModal) {}
}

/**
 * * Get Forecast Download File
 */
export class SelectedForecastDownloadFileType implements Action {
  readonly type = ForecastDownloadActionTypes.SELECTED_FORECAST_DOWNLOAD_FILE_TYPE;
  constructor(public payload: IForecastDownload) {}
}

export class SelectedForecastDownloadFileColumns implements Action {
  readonly type = ForecastDownloadActionTypes.SELECTED_FORECAST_DOWNLOAD_FILE_COLUMNS;
  constructor(public payload: IForecastDownload) {}
}

export class SelectedForecastDownloadFileDateRange implements Action {
  readonly type = ForecastDownloadActionTypes.SELECTED_FORECAST_DOWNLOAD_FILE_DATE_RANGE;
  constructor(public payload: IForecastDownload) {}
}

export type ForecastDownloadActions =
  | GetForecastDownloadFile
  | GetForecastDownloadFileSuccess
  | GetForecastDownloadFileFailed
  | GetForecastDownloadFileReset
  | OpenedForecastDownloadModal
  | SelectedForecastDownloadFileType
  | SelectedForecastDownloadFileColumns
  | SelectedForecastDownloadFileDateRange;
