import { ForecastDownloadActions, ForecastDownloadActionTypes } from './forecast-download.actions';

import { IForecastDownloadState } from '@app/core/forecast/forecast.interfaces';

export const INITIAL_FORECAST_DOWNLOAD_STATE: IForecastDownloadState = {
  loading: true,
  error: undefined,
  status: undefined,
};

/**
 * Mutates the state for given set of ForecastDownloadActions
 * @param state
 * @param action
 */
export function forecastDownloadReducers(
  state: IForecastDownloadState = INITIAL_FORECAST_DOWNLOAD_STATE,
  action: ForecastDownloadActions
): IForecastDownloadState {
  switch (action.type) {
    case ForecastDownloadActionTypes.GET_FORECAST_DOWNLOAD_FILE: {
      return {
        ...state,
        loading: true,
        error: undefined,
        status: undefined,
      };
    }
    case ForecastDownloadActionTypes.GET_FORECAST_DOWNLOAD_FILE_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: undefined,
        status: action.payload,
      };
    }
    case ForecastDownloadActionTypes.GET_FORECAST_DOWNLOAD_FILE_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload,
        status: undefined,
      };
    }

    case ForecastDownloadActionTypes.GET_FORECAST_DOWNLOAD_RESET: {
      return {
        ...state,
        loading: false,
        error: undefined,
        status: undefined,
      };
    }

    default: {
      return state;
    }
  }
}
