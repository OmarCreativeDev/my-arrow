import { createFeatureSelector, createSelector } from '@ngrx/store';

import { IForecastDownloadState } from '@app/core/forecast/forecast.interfaces';
/**
 * Selects the Forecast state from the root state object
 */
export const getForecastState = createFeatureSelector<IForecastDownloadState>('forecastDownload');

/**
 * Retrieve the loading state of the page
 */
export const getForecastLoading = createSelector(getForecastState, forecastState => forecastState.loading);

export const getForecastDownloadErrorSelector = createSelector(getForecastState, forecastState => forecastState.error);

export const getForecastStatus = createSelector(getForecastState, forecastState => forecastState.status);
