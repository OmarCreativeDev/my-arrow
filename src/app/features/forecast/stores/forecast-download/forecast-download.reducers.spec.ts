import { GetForecastDownloadFile, GetForecastDownloadFileFailed, GetForecastDownloadFileSuccess } from './forecast-download.actions';
import { IForecastDownload } from '@app/core/forecast/forecast.interfaces';
import { forecastDownloadReducers } from './forecast-download.reducers';
import { IForecastDownloadState } from '@app/core/forecast/forecast.interfaces';
import { INITIAL_FORECAST_DOWNLOAD_STATE } from '@app/features/forecast/stores/forecast-download/forecast-download.reducers';

describe('Forecast Reducers', () => {
  let initialState: IForecastDownloadState;

  beforeEach(() => {
    initialState = {
      loading: true,
      error: undefined,
      status: undefined,
    };
  });

  it(`by default should return the state without transforming it`, () => {
    const result = forecastDownloadReducers(undefined, {} as any);
    expect(result).toEqual(INITIAL_FORECAST_DOWNLOAD_STATE);
  });

  it('`GET_FORECAST_DOWNLOAD_FILE` should set `loading` to true and not have an error property', () => {
    const params: IForecastDownload = {
      accountNumber: 1067767,
      billToNumber: 2174372,
      fileType: 'XLS',
      smrName: 'Test REP',
      columns: ['customerPartNumber'],
      weeksInHorizon: 52,
    };
    const action = new GetForecastDownloadFile(params);
    const result = forecastDownloadReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeUndefined();
  });

  it('`GET_FORECAST_DOWNLOAD_FILE_SUCCESS` should set `status`', () => {
    const action = new GetForecastDownloadFileSuccess(204);
    const result = forecastDownloadReducers(initialState, action);
    expect(result.status).toBeTruthy();
    expect(result.error).toBeUndefined();
  });

  it(`GET_FORECAST_DOWNLOAD_FILE_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new GetForecastDownloadFileFailed(error);
    const result = forecastDownloadReducers(initialState, action);
    expect(result.error).toBeDefined();
  });
});
