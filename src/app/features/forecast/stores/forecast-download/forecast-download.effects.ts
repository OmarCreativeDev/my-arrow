import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { switchMap, map, catchError } from 'rxjs/operators';
import { ForecastService } from '@app/core/forecast/forecast.service';
import { FileService } from '@app/shared/services/file.service';
import { HttpResponse } from '@angular/common/http';

import {
  ForecastDownloadActionTypes,
  GetForecastDownloadFile,
  GetForecastDownloadFileSuccess,
  GetForecastDownloadFileFailed,
} from '@app/features/forecast/stores/forecast-download/forecast-download.actions';

@Injectable()
export class ForecastDownloadEffects {
  constructor(private actions$: Actions, private forecastService: ForecastService, private fileService: FileService) {}
  @Effect()
  GetForecastDownloadFile$ = this.actions$.pipe(
    ofType(ForecastDownloadActionTypes.GET_FORECAST_DOWNLOAD_FILE),
    switchMap((action: GetForecastDownloadFile) => {
      return this.forecastService.getForecastFile(action.payload).pipe(
        map((response: any) => new GetForecastDownloadFileSuccess(this.handleDownload(response))),
        catchError(err => of(new GetForecastDownloadFileFailed(err)))
      );
    })
  );

  handleDownload(response: HttpResponse<Blob>) {
    if (response.status === 200) this.fileService.saveResponse(response);
    return response.status;
  }
}
