import { Injectable } from '@angular/core';

import { combineLatest, Observable, of, merge } from 'rxjs';
import { switchMap, map, catchError, debounceTime } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { Store, select } from '@ngrx/store';

import { ForecastService } from '@app/core/forecast/forecast.service';
import { IForecast } from '@app/core/forecast/forecast.interfaces';
import {
  ForecastActionTypes,
  GetForecasts,
  GetForecastsSuccess,
  GetForecastsFailed,
  GetForecastSummary,
  GetForecastSummarySuccess,
  GetForecastSummaryFailed,
  SubmitSearch,
  Query,
  QueryComplete,
  QueryError,
  ChangePage,
  ClearFilter,
  RetainFilter,
  ResetForecasts,
} from '@app/features/forecast/stores/forecast/forecast.actions';
import { INITIAL_FORECAST_STATE } from '@app/features/forecast/stores/forecast/forecast.reducers';
import { IAppState } from '@app/shared/shared.interfaces';
import { getAccountNumber, getUserBillToAccount } from '@app/core/user/store/user.selectors';
import { Actions, Effect, ofType } from '@ngrx/effects';

@Injectable()
export class ForecastEffects {
  accountNumber: number;
  billToId: number;

  constructor(private actions$: Actions, private forecastService: ForecastService, private store: Store<IAppState>) {
    const accountNumber$ = this.store.pipe(select(getAccountNumber));
    const billToId$ = this.store.pipe(select(getUserBillToAccount));
    combineLatest(accountNumber$, billToId$).subscribe(latestValues => {
      this.accountNumber = latestValues[0];
      this.billToId = latestValues[1];
    });
  }

  @Effect()
  GetForecasts = this.actions$.pipe(
    ofType(ForecastActionTypes.GET_FORECASTS),
    switchMap((action: GetForecasts) => {
      return this.forecastService
        .getForecastSearchDetails({
          accountNumber: action.payload.accountNumber,
          billToNumber: action.payload.billToNumber,
          page: action.payload.page,
          size: action.payload.size,
        })
        .pipe(
          map((data: IForecast) => new GetForecastsSuccess(data)),
          catchError(err => of(new GetForecastsFailed(err)))
        );
    })
  );

  @Effect()
  GetForecastSummary = this.actions$.pipe(
    ofType(ForecastActionTypes.GET_FORECAST_SUMMARY),
    switchMap((action: GetForecastSummary) => {
      return this.forecastService.getForecastSummary(this.accountNumber, this.billToId).pipe(
        map((data: IForecast) => new GetForecastSummarySuccess(data)),
        catchError(err => of(new GetForecastSummaryFailed(err)))
      );
    })
  );

  /**
   * Side-effect that combines all side effects for these facets:
   *  - Search
   *  - Filters
   *  - Limit
   *  - Page
   */
  @Effect()
  facets$: Observable<any> = combineLatest(
    this.actions$.pipe(ofType(ForecastActionTypes.SUBMIT_FORECAST_SEARCH)),
    this.actions$.pipe(ofType(ForecastActionTypes.CHANGE_FORECAST_PAGE_LIMIT)),
    this.actions$.pipe(ofType(ForecastActionTypes.CHANGE_FORECAST_PAGE)),
    this.actions$.pipe(ofType(ForecastActionTypes.CHANGE_FORECAST_SORT)),
    this.actions$.pipe(ofType(ForecastActionTypes.CHANGE_FORECAST_SHORTAGES_ONLY)),
    this.actions$.pipe(ofType(ForecastActionTypes.CHANGE_FORECAST_POTENTIAL_SHORTAGES))
  ).pipe(
    debounceTime(50),
    map((actions: any) => {
      const payloads = actions.map(action => action.payload);
      return new Query({
        accountNumber: this.accountNumber,
        billToNumber: this.billToId,
        paging: {
          limit: payloads[1],
          page: payloads[2],
        },
        sort: payloads[3],
        searchText: payloads[0],
        shortagesOnly: payloads[4],
        potentialShortages: payloads[5],
      });
    })
  );

  /**
   * Side-effect for the Query being changed directly. Trigger by the facets$ Side-effect
   */
  @Effect()
  query$: Observable<any> = this.actions$.pipe(
    ofType(ForecastActionTypes.FORECAST_QUERY),
    switchMap((action: Query) => {
      return this.forecastService
        .getForecastSearchDetails({
          accountNumber: action.payload.accountNumber,
          billToNumber: action.payload.billToNumber,
          page: action.payload.paging.page,
          size: action.payload.paging.limit,
          sortColumn: action.payload.sort[0].key,
          sortDirection: action.payload.sort[0].order,
          searchText: action.payload.searchText.value,
          searchBy: action.payload.searchText.type,
          shortagesOnly: action.payload.shortagesOnly,
          shortageDateRange: action.payload.potentialShortages,
        })
        .pipe(
          map((queryResponse: any) => this.handleQueryResponse(queryResponse)),
          catchError((errorResponse: HttpErrorResponse) => of(new QueryError(errorResponse)))
        );
    })
  );

  /**
   * Reset the filter if the search value was changed
   */
  @Effect()
  resetFilterOnSearch$ = this.actions$.pipe(
    ofType(ForecastActionTypes.SUBMIT_FORECAST_SEARCH),
    map((action: SubmitSearch) => {
      if (action.payload.value !== INITIAL_FORECAST_STATE.search.value) {
        return new ClearFilter();
      } else {
        return new RetainFilter();
      }
    })
  );

  /**
   * Change the page to page 1 if a new search, a new filter or a new page limit is set
   */
  @Effect()
  searchFilterUpdate$ = merge(
    this.actions$.pipe(ofType(ForecastActionTypes.CHANGE_FORECAST_PAGE_LIMIT)),
    this.actions$.pipe(ofType(ForecastActionTypes.CHANGE_FORECAST_SORT))
  ).pipe(
    map((action: any) => {
      return new ChangePage(INITIAL_FORECAST_STATE.pagination.current);
    })
  );

  handleQueryResponse(queryResponse) {
    if (queryResponse) {
      return new QueryComplete(queryResponse);
    } else {
      // handle 204 response
      return new ResetForecasts();
    }
  }
}
