import { createFeatureSelector, createSelector } from '@ngrx/store';

import { IForecastSearchState } from '@app/shared/shared.interfaces';
import { IForecastDetails } from '@app/core/forecast/forecast.interfaces';

/**
 * Selects the Forecast state from the root state object
 */
export const getForecastState = createFeatureSelector<IForecastSearchState<IForecastDetails>>('forecast');

/**
 * Retrieves the forecasts
 */
export const getForecasts = createSelector(
  getForecastState,
  forecastState => forecastState.items
);

/**
 * Retrieves the forecast details
 */
export const getForecastSummaryDetails = createSelector(
  getForecastState,
  forecastState => forecastState.summaryDetails
);

/**
 * Retrieves the forecast errors
 */
export const getForecastErrors = createSelector(
  getForecastState,
  forecastState => forecastState.error
);

/**
 * Retrieve the number of items that should show per page
 */
export const getLimit = createSelector(
  getForecastState,
  forecastState => forecastState.pagination.limit
);

/**
 * Retrieve the number of items that should show per page
 */
export const getPage = createSelector(
  getForecastState,
  forecastState => forecastState.pagination.current
);

/**
 * Retrieve the number of items that should show per page
 */
export const getTotalPages = createSelector(
  getForecastState,
  forecastState => forecastState.pagination.total
);

/**
 * Retrieve the sorting of items
 */
export const getSort = createSelector(
  getForecastState,
  forecastState => forecastState.sort
);

/**
 * Retrieve the sorting of items
 */
export const getShortageOnly = createSelector(
  getForecastState,
  forecastState => forecastState.shortagesOnly
);

/**
 * Retrieve the search query
 */
export const getSearchQuery = createSelector(
  getForecastState,
  forecastState => forecastState.search
);

/**
 * Retrieve the potential shortages
 */
export const getPotentialShortages = createSelector(
  getForecastState,
  forecastState => forecastState.potentialShortages
);
