import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';

import { IForecast, IForecastSearchGetParams } from '@app/core/forecast/forecast.interfaces';
import { ISearchCriteria, ISortCriteron } from '@app/shared/shared.interfaces';

export enum ForecastActionTypes {
  GET_FORECASTS = 'GET_FORECASTS',
  GET_FORECASTS_SUCCESS = 'GET_FORECASTS_SUCCESS',
  GET_FORECASTS_FAILED = 'GET_FORECASTS_FAILED',
  GET_FORECAST_SUMMARY = 'GET_FORECAST_SUMMARY',
  GET_FORECAST_SUMMARY_SUCCESS = 'GET_FORECAST_SUMMARY_SUCCESS',
  GET_FORECAST_SUMMARY_FAILED = 'GET_FORECAST_SUMMARY_FAILED',
  SUBMIT_FORECAST_SEARCH = 'SUBMIT_FORECAST_SEARCH',
  FORECAST_QUERY = 'FORECAST_QUERY',
  FORECAST_QUERY_COMPLETE = 'FORECAST_QUERY_COMPLETE',
  FORECAST_QUERY_ERROR = 'FORECAST_QUERY_ERROR',
  CHANGE_FORECAST_PAGE_LIMIT = 'CHANGE_FORECAST_PAGE_LIMIT',
  CHANGE_FORECAST_PAGE = 'CHANGE_FORECAST_PAGE',
  CHANGE_FORECAST_SORT = 'CHANGE_FORECAST_SORT',
  CHANGE_FORECAST_SHORTAGES_ONLY = 'CHANGE_FORECAST_SHORTAGES_ONLY',
  CHANGE_FORECAST_POTENTIAL_SHORTAGES = 'CHANGE_FORECAST_POTENTIAL_SHORTAGES',
  SELECT_FORECAST_LINES = 'SELECT_FORECAST_LINES',
  CLEAR_FORECAST_FILTER = 'CLEAR_FORECAST_FILTER',
  RETAIN_FORECAST_FILTER = 'RETAIN_FORECAST_FILTER',
  FORECAST_RESET = 'FORECAST_RESET',
}

/**
 * * Get Forecasts
 */
export class GetForecasts implements Action {
  readonly type = ForecastActionTypes.GET_FORECASTS;
  constructor(public payload: IForecastSearchGetParams) {}
}

/**
 * * Get Forecasts Success
 */
export class GetForecastsSuccess implements Action {
  readonly type = ForecastActionTypes.GET_FORECASTS_SUCCESS;
  constructor(public payload: IForecast) {}
}

/**
 * * Get Forecasts Failed
 */
export class GetForecastsFailed implements Action {
  readonly type = ForecastActionTypes.GET_FORECASTS_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Get Forecast Summary
 */
export class GetForecastSummary implements Action {
  readonly type = ForecastActionTypes.GET_FORECAST_SUMMARY;
  constructor() {}
}

/**
 * * Get Forecast Summary Success
 */
export class GetForecastSummarySuccess implements Action {
  readonly type = ForecastActionTypes.GET_FORECAST_SUMMARY_SUCCESS;
  constructor(public payload: IForecast) {}
}

/**
 * * Get Forecast Summary Failed
 */
export class GetForecastSummaryFailed implements Action {
  readonly type = ForecastActionTypes.GET_FORECAST_SUMMARY_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Submit the Search facets for the Query
 */
export class SubmitSearch implements Action {
  readonly type = ForecastActionTypes.SUBMIT_FORECAST_SEARCH;
  constructor(public payload: ISearchCriteria) {}
}

/**
 * Updates the Query with the provided facets
 */
export class Query implements Action {
  readonly type = ForecastActionTypes.FORECAST_QUERY;
  constructor(public payload: any) {}
}

/**
 * Results of the Query
 */
export class QueryComplete implements Action {
  readonly type = ForecastActionTypes.FORECAST_QUERY_COMPLETE;
  constructor(public payload: any) {}
}

/**
 * Thrown if the Query received an Error
 */
export class QueryError implements Action {
  readonly type = ForecastActionTypes.FORECAST_QUERY_ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

/**
 * Changes the limit on each page
 */
export class ChangePageLimit implements Action {
  readonly type = ForecastActionTypes.CHANGE_FORECAST_PAGE_LIMIT;
  constructor(public payload: number) {}
}

/**
 * Change the page number
 */
export class ChangePage implements Action {
  readonly type = ForecastActionTypes.CHANGE_FORECAST_PAGE;
  constructor(public payload: number) {}
}

/**
 * Changes the limit on each page
 */
export class ChangeSort implements Action {
  readonly type = ForecastActionTypes.CHANGE_FORECAST_SORT;
  constructor(public payload: Array<ISortCriteron>) {}
}

/**
 * Changes the type to shortages only
 */
export class ChangeShortagesOnly implements Action {
  readonly type = ForecastActionTypes.CHANGE_FORECAST_SHORTAGES_ONLY;
  constructor(public payload: boolean) {}
}

/**
 * Adds an OrderLine to the selection
 */
export class ToggleOrderLines implements Action {
  readonly type = ForecastActionTypes.SELECT_FORECAST_LINES;
  constructor(public payload: Array<string>, public select: boolean) {}
}

export class ClearFilter implements Action {
  readonly type = ForecastActionTypes.CLEAR_FORECAST_FILTER;
  constructor() {}
}

export class RetainFilter implements Action {
  readonly type = ForecastActionTypes.RETAIN_FORECAST_FILTER;
  constructor() {}
}

export class ResetForecasts implements Action {
  readonly type = ForecastActionTypes.FORECAST_RESET;
  constructor() {}
}

export class ChangePotentialShortages implements Action {
  readonly type = ForecastActionTypes.CHANGE_FORECAST_POTENTIAL_SHORTAGES;
  constructor(public payload: number) {}
}

export type ForecastActions =
  | GetForecasts
  | GetForecastsSuccess
  | GetForecastsFailed
  | GetForecastSummary
  | GetForecastSummarySuccess
  | GetForecastSummaryFailed
  | SubmitSearch
  | Query
  | QueryComplete
  | QueryError
  | ChangePageLimit
  | ChangePage
  | ChangeSort
  | ChangeShortagesOnly
  | ToggleOrderLines
  | ClearFilter
  | RetainFilter
  | ResetForecasts
  | ChangePotentialShortages;
