import { IForecastSearchState } from '@app/shared/shared.interfaces';
import { ForecastActions, ForecastActionTypes } from './forecast.actions';
import { IForecastDetails } from '@app/core/forecast/forecast.interfaces';
import { ISortCriteronOrderEnum } from '@app/shared/shared.interfaces';

export const INITIAL_FORECAST_STATE: IForecastSearchState<IForecastDetails> = {
  loading: true,
  error: undefined,
  search: {
    type: '',
    value: '',
  },
  filter: {
    value: undefined,
    rules: [],
  },
  pagination: {
    limit: 10,
    current: 1,
    total: 0,
  },
  items: undefined,
  sort: [
    {
      key: 'customerPartNumber',
      order: ISortCriteronOrderEnum.ASC,
    },
  ],
  selectedIds: [],
  summaryDetails: undefined,
  shortagesOnly: false,
  potentialShortages: 0,
};

/**
 * Mutates the state for given set of ForecastActions
 * @param state
 * @param action
 */
export function forecastReducers(state: any = INITIAL_FORECAST_STATE, action: ForecastActions) {
  switch (action.type) {
    case ForecastActionTypes.GET_FORECASTS: {
      return {
        ...state,
        loading: true,
        error: undefined,
      };
    }
    case ForecastActionTypes.GET_FORECASTS_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: undefined,
        items: action.payload,
        pagination: {
          ...state.pagination,
          current: action.payload.number ? action.payload.number : INITIAL_FORECAST_STATE.pagination.current,
          total: action.payload.totalPages ? action.payload.totalPages : INITIAL_FORECAST_STATE.pagination.total,
        },
      };
    }
    case ForecastActionTypes.GET_FORECASTS_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    }
    case ForecastActionTypes.GET_FORECAST_SUMMARY: {
      return {
        ...state,
        loading: true,
        error: undefined,
      };
    }
    case ForecastActionTypes.GET_FORECAST_SUMMARY_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: undefined,
        summaryDetails: action.payload,
      };
    }
    case ForecastActionTypes.GET_FORECAST_SUMMARY_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    }

    case ForecastActionTypes.SUBMIT_FORECAST_SEARCH: {
      return {
        ...state,
        search: action.payload,
      };
    }
    case ForecastActionTypes.FORECAST_QUERY: {
      return {
        ...state,
        error: undefined,
        loading: true,
        items: undefined,
        selectedIds: [],
      };
    }
    case ForecastActionTypes.FORECAST_QUERY_COMPLETE: {
      return {
        ...state,
        items: action.payload,
        pagination: {
          ...state.pagination,
          current: action.payload.number ? action.payload.number : INITIAL_FORECAST_STATE.pagination.current,
          total: action.payload.totalPages ? action.payload.totalPages : INITIAL_FORECAST_STATE.pagination.total,
        },
        loading: false,
      };
    }
    case ForecastActionTypes.FORECAST_QUERY_ERROR: {
      return {
        ...state,
        error: action.payload,
        items: undefined,
        loading: false,
        pagination: {
          limit: state.pagination.limit,
          current: 1,
          total: 0,
        },
      };
    }
    case ForecastActionTypes.CHANGE_FORECAST_PAGE_LIMIT: {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          limit: action.payload,
        },
      };
    }
    case ForecastActionTypes.CHANGE_FORECAST_PAGE: {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          current: action.payload,
        },
      };
    }

    case ForecastActionTypes.CHANGE_FORECAST_SORT: {
      return {
        ...state,
        sort: action.payload ? action.payload : [],
      };
    }

    case ForecastActionTypes.CHANGE_FORECAST_SHORTAGES_ONLY: {
      return {
        ...state,
        shortagesOnly: action.payload,
      };
    }

    case ForecastActionTypes.CHANGE_FORECAST_POTENTIAL_SHORTAGES: {
      return {
        ...state,
        potentialShortages: action.payload,
      };
    }

    case ForecastActionTypes.FORECAST_RESET: {
      // handle 204
      return {
        ...state,
        error: undefined,
        items: undefined,
        selectedIds: [],
      };
    }

    default: {
      return state;
    }
  }
}
