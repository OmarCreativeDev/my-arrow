import { HttpErrorResponse } from '@angular/common/http';

import {
  GetForecasts,
  GetForecastsSuccess,
  GetForecastsFailed,
  GetForecastSummary,
  GetForecastSummarySuccess,
  GetForecastSummaryFailed,
  Query,
  ChangePageLimit,
  ChangePage,
  ChangeSort,
  ChangeShortagesOnly,
  ResetForecasts,
  QueryError,
  QueryComplete,
  SubmitSearch,
  ChangePotentialShortages,
} from '@app/features/forecast/stores/forecast/forecast.actions';
import { forecastReducers, INITIAL_FORECAST_STATE } from './forecast.reducers';
import { IForecastGetParams } from '@app/core/forecast/forecast.interfaces';
import mockedForecastDetails from '@app/core/forecast/forecast-mock-response';
import { IForecastSearchState } from '@app/shared/shared.interfaces';
import { ISortCriteronOrderEnum } from '@app/shared/shared.interfaces';
import { IForecastDetails } from '@app/core/forecast/forecast.interfaces';
import { ISearchCriteria } from '@app/shared/shared.interfaces';

describe('Forecast Reducers', () => {
  let initialState: IForecastSearchState<IForecastDetails>;

  beforeEach(() => {
    initialState = INITIAL_FORECAST_STATE;
  });

  it(`by default should return the state without transforming it`, () => {
    const result = forecastReducers(undefined, {} as any);
    expect(result).toEqual(INITIAL_FORECAST_STATE);
  });

  it('`GET_FORECASTS` should set `loading` to true and not have an error property', () => {
    const params: IForecastGetParams = {
      accountNumber: 1067767,
      billToNumber: 1067767,
    };
    const action = new GetForecasts(params);
    const result = forecastReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeUndefined();
  });

  it('`GET_FORECASTS_SUCCESS` should set `forecasts`', () => {
    const action = new GetForecastsSuccess(mockedForecastDetails);
    const result = forecastReducers(initialState, action);
    expect(result.items).toBeDefined();
  });

  it(`GET_FORECASTS_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new GetForecastsFailed(error);
    const result = forecastReducers(initialState, action);
    expect(result.error).toBeDefined();
  });

  it('`GET_FORECAST_SUMMARY` should set `loading` to true and not have an error property', () => {
    const action = new GetForecastSummary();
    const result = forecastReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeUndefined();
  });

  it('`GET_FORECAST_SUMMARY_SUCCESS` should set `forecasts`', () => {
    const action = new GetForecastSummarySuccess(mockedForecastDetails);
    const result = forecastReducers(initialState, action);
    expect(result.summaryDetails).toBeDefined();
  });

  it(`GET_FORECAST_SUMMARY_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new GetForecastSummaryFailed(error);
    const result = forecastReducers(initialState, action);
    expect(result.error).toBeDefined();
  });

  it(`FORECAST_QUERY should clear any errors and any items, set loading to true and reset the selection`, () => {
    const query = {
      search: {
        type: 'foo',
        value: 'bar',
      },
      paging: {
        limit: 10,
        page: 2,
      },
    };
    const action = new Query(query);
    const result = forecastReducers(initialState, action);
    expect(result.loading).toEqual(true);
    expect(result.error).toEqual(undefined);
    expect(result.items).toEqual(undefined);
    expect(result.selectedIds.length).toEqual(0);
  });

  it(`CHANGE_FORECAST_PAGE should be update the query limit`, () => {
    const dummyPage = 5;
    const action = new ChangePage(dummyPage);
    const result = forecastReducers(initialState, action);
    expect(result.pagination.current).toEqual(dummyPage);
  });

  it(`CHANGE_FORECAST_PAGE_LIMIT should be update the query limit`, () => {
    const dummyPageLimit = 50;
    const action = new ChangePageLimit(dummyPageLimit);
    const result = forecastReducers(initialState, action);
    expect(result.pagination.limit).toEqual(dummyPageLimit);
  });

  it(`CHANGE_FORECAST_SORT should be update the sort field`, () => {
    const sort = [
      {
        key: 'customerPartNumber',
        order: ISortCriteronOrderEnum.ASC,
      },
    ];
    const action = new ChangeSort(sort);
    const result = forecastReducers(initialState, action);
    expect(result.sort).toEqual(sort);
  });

  it(`CHANGE_FORECAST_SHORTAGES_ONLY should be update the sort field`, () => {
    const shortagesOnly = true;
    const action = new ChangeShortagesOnly(shortagesOnly);
    const result = forecastReducers(initialState, action);
    expect(result.shortagesOnly).toEqual(shortagesOnly);
  });

  it(`FORECAST_RESET should be update the sort field`, () => {
    const action = new ResetForecasts();
    const result = forecastReducers(initialState, action);
    expect(result.shortagesOnly).toEqual(false);
  });

  it(`FORECAST_QUERY_ERROR should be update the sort field`, () => {
    const error = {
      error: 'foo',
    } as HttpErrorResponse;
    const action = new QueryError(error);
    const result = forecastReducers(initialState, action);
    expect(result.error).toBeDefined();
  });

  it(`FORECAST_QUERY_COMPLETE should be update the sort field`, () => {
    const data = {
      payload: mockedForecastDetails,
    };
    const action = new QueryComplete(data);
    const result = forecastReducers(initialState, action);
    expect(result.items).toBeDefined();
  });

  it(`SUBMIT_FORECAST_SEARCH should be update the sort field`, () => {
    const search: ISearchCriteria = {
      value: 'test',
      type: 'test',
    };
    const action = new SubmitSearch(search);
    const result = forecastReducers(initialState, action);
    expect(result.search).toBeDefined();
  });

  it(`CHANGE_FORECAST_POTENTIAL_SHORTAGES should update potentialShortages field`, () => {
    const potentialShortage = 12;
    const action = new ChangePotentialShortages(potentialShortage);
    const result = forecastReducers(initialState, action);
    expect(result.potentialShortages).toEqual(potentialShortage);
  });
});
