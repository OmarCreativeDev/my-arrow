import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ForecastComponent } from './pages/forecast/forecast.component';
import { ForecastDetailComponent } from './pages/forecast-detail/forecast-detail.component';
import { ForecastRoutingModule } from '@app/features/forecast/forecast-routing.module';
import { ForecastSearchComponent } from '@app/features/forecast/components/forecast-search/forecast-search.component';
import { RouterModule } from '@angular/router';
import { SharedModule } from '@app/shared/shared.module';
import { WINDOW_PROVIDERS } from '@app/shared/services/window.service';
import { StoreModule } from '@ngrx/store';
import { forecastDownloadAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.forecast-download';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ForecastRoutingModule,
    RouterModule,
    SharedModule,
    StoreModule.forFeature('forecastDownload', forecastDownloadAnalyticsMetaReducers),
  ],
  declarations: [ForecastComponent, ForecastDetailComponent, ForecastSearchComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [WINDOW_PROVIDERS],
})
export class ForecastModule {}
