import { OnDestroy } from '@angular/core';
import { Component, OnInit } from '@angular/core';

import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';

import { IAppState, ISortCriteron, ISortCriteronOrderEnum, ISearchCriteria, ISelectableOption } from '@app/shared/shared.interfaces';
import {
  getForecasts,
  getLimit,
  getPage,
  getSort,
  getTotalPages,
  getForecastSummaryDetails,
  getSearchQuery,
  getForecastErrors,
  getShortageOnly,
  getPotentialShortages,
} from '@app/features/forecast/stores/forecast/forecast.selectors';
import { getUser } from '@app/core/user/store/user.selectors';
import { ForecastDownloadComponent } from '@app/features/forecast/components/forecast-download/forecast-download.component';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DEFAULT_MODAL_TITLE, EmailModalComponent, DEFAULT_MODAL_INTRO } from '@app/shared/components/email-modal/email-modal.component';
import { IUser } from '@app/core/user/user.interface';
import { IForecastPartList } from '@app/core/forecast/forecast.interfaces';
import {
  ChangePageLimit,
  ChangePage,
  ChangeSort,
  GetForecastSummary,
  ChangeShortagesOnly,
  ChangePotentialShortages,
} from '@app/features/forecast/stores/forecast/forecast.actions';
import { SubmitSearch } from '../../stores/forecast/forecast.actions';
import { errorsMessageHeader, errorsMessageBody } from '@app/features/errors/pages/errors.message';
import { BillToAccountsComponent } from '@app/shared/components/bill-to-accounts/bill-to-accounts.component';
import { GetPublicProperties } from '@app/features/properties/store/properties.actions';
import { getPublicDomainsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: 'app-forecast-detail',
  templateUrl: './forecast-detail.component.html',
  styleUrls: ['./forecast-detail.component.scss'],
})
export class ForecastDetailComponent implements OnInit, OnDestroy {
  public billToId: number;
  public accountNumber: number;
  public forecasts$: Observable<any>;
  public forecastsDetails$: Observable<any>;
  public summary$: Observable<any>;
  public publicDomains$: Observable<any>;
  public userProfile$: Observable<IUser>;
  public accountNumber$: Observable<number>;
  public billToId$: Observable<number>;
  public subscription: Subscription = new Subscription();
  public moreWeeks: boolean = true;
  public moreWeeksText: string = 'More';
  public salesRepName: string;
  public salesRepPhoneNumber: string;
  public salesRepEmail: string;
  public region: string;
  public parts: any;
  public weeks = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
  public details: any;
  public selectAll: boolean = false;
  public sort$: Observable<Array<ISortCriteron>>;
  public sortOrder: Map<string, ISortCriteronOrderEnum>;
  public totalPages$: Observable<number>;
  public page$: Observable<number>;
  public limit$: Observable<number>;
  public partsInFilteredView: number = 0;
  public search$: Observable<ISearchCriteria>;
  public search: ISearchCriteria;
  public errorHeader: string = errorsMessageHeader;
  public errorBody: string = errorsMessageBody;
  public forecastErrors$: Observable<any>;
  public errors: object;
  public showShortages: boolean = false;
  public shortagesOnly$: Observable<boolean>;
  public potentialShortages: number;
  public potentialShortages$: Observable<number>;

  private CUST_NAME_INHOSUE = 'IN HOUSE,';
  private CUST_NAME_REPRESENTATIVE = 'Representative';

  public potentialShortagesOptions: Array<ISelectableOption<any>> = [
    { label: 'All Potential Shortages', value: 0 },
    { label: 'First 2 Weeks', value: 2 },
    { label: 'First 6 Weeks', value: 6 },
    { label: 'First 8 Weeks', value: 8 },
    { label: 'First 12 Weeks', value: 12 },
  ];

  constructor(private store: Store<IAppState>, private dialogService: DialogService) {
    this.userProfile$ = this.store.pipe(select(getUser));
    this.forecasts$ = this.store.pipe(select(getForecasts));
    this.forecastsDetails$ = this.store.pipe(select(getForecastSummaryDetails));
    this.publicDomains$ = this.store.pipe(select(getPublicDomainsSelector));
    this.search$ = this.store.pipe(select(getSearchQuery));
    this.sort$ = this.store.pipe(select(getSort));
    this.shortagesOnly$ = this.store.pipe(select(getShortageOnly));
    this.forecasts$ = this.store.pipe(select(getForecasts));
    this.forecastErrors$ = store.pipe(select(getForecastErrors));
    this.potentialShortages$ = store.pipe(select(getPotentialShortages));
    this.getStoreSlices();
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  subAssignUserAndSalesLocalVariables(): Subscription {
    return this.userProfile$.subscribe(user => {
      if (user) {
        this.region = user.region;
        if (this.billToId > 0 && user.selectedBillTo !== this.billToId) {
          this.submitSearch({ type: 'any', value: '' });
        }
        this.billToId = user.selectedBillTo;

        if (user.contact) {
          this.salesRepName = user.contact.salesRepName;
          this.salesRepPhoneNumber = user.contact.salesRepPhoneNumber;
          this.salesRepEmail = user.contact.salesRepEmail;
        }
      }
    });
  }

  subAssignForcasts(): Subscription {
    return this.forecasts$.subscribe(forecasts => {
      if (forecasts && forecasts.content) {
        this.parts = forecasts.content.map(obj => {
          obj.selected = false;
          return obj;
        });
        this.partsInFilteredView = forecasts.totalElements;
      } else {
        this.parts = undefined;
        this.partsInFilteredView = 0;
      }
    });
  }

  subAssignForcastDetails(): Subscription {
    return this.forecastsDetails$.subscribe(details => {
      if (details) {
        this.details = {
          companyName: details.name,
          received: details.receivedDate,
          lastUpdate: details.lastArrowUpdateDate,
          forcastHorizon: details.weeksInHorizon,
          partsCovered: details.partsCovered,
          potentialShortages: details.potentialShortages,
        };
      }
    });
  }

  subAssignSalesRep(): Subscription {
    return this.publicDomains$.subscribe(domains => this.handleDomainSalesDetails(domains, this.region));
  }

  handleDomainSalesDetails(domains: object, region: string) {
    if (region && domains && domains[region]) {
      if (!this.salesRepEmail) {
        this.setDomainSalesRepEmailDetails(domains, region);
      }
      if (!this.salesRepPhoneNumber) {
        this.setDomainSalesRepPhoneDetails(domains, region);
      }
    }
  }

  setDomainSalesRepEmailDetails(domains: object, region: string) {
    this.salesRepEmail = domains[region].custServiceEmail;
  }

  setDomainSalesRepPhoneDetails(domains: object, region: string) {
    this.salesRepPhoneNumber = domains[region].custServicePhoneNum;
  }

  subAssignSort(): Subscription {
    return this.sort$.subscribe(updatedSort => {
      this.sortOrder = new Map();
      /* istanbul ignore next */
      if (updatedSort.length) {
        updatedSort.forEach(updatedSortItem => {
          this.sortOrder.set(updatedSortItem.key, updatedSortItem.order);
        });
      }
    });
  }

  subAssignSearch(): Subscription {
    return this.search$.subscribe(search => {
      this.search = search;
    });
  }

  subAssignError(): Subscription {
    return this.forecastErrors$.subscribe(error => {
      this.errors = error;
    });
  }

  subAssignShortageOnly(): Subscription {
    return this.shortagesOnly$.subscribe(showShortages => {
      this.showShortages = showShortages;
    });
  }

  subAssignPotentialShortages(): Subscription {
    return this.potentialShortages$.subscribe(potentialShortages => {
      this.potentialShortages = potentialShortages;
    });
  }

  private startSubscriptions(): void {
    this.subscription.add(this.subAssignUserAndSalesLocalVariables());
    this.subscription.add(this.subAssignForcasts());
    this.subscription.add(this.subAssignForcastDetails());
    this.subscription.add(this.subAssignSalesRep());
    this.subscription.add(this.subAssignSort());
    this.subscription.add(this.subAssignSearch());
    this.subscription.add(this.subAssignShortageOnly());
    this.subscription.add(this.subAssignError());
    this.subscription.add(this.subAssignPotentialShortages());

    this.store.dispatch(new GetForecastSummary());
    this.store.dispatch(new GetPublicProperties());
    this.pageLimitChange(10);
    this.pageChange(0);
    this.store.dispatch(new ChangeSort([{ key: 'customerPartNumber', order: ISortCriteronOrderEnum.ASC }]));
    this.store.dispatch(new ChangeShortagesOnly(false));
    this.store.dispatch(new ChangePotentialShortages(0));
  }

  private getStoreSlices(): void {
    this.limit$ = this.store.pipe(select(getLimit));
    this.page$ = this.store.pipe(select(getPage));
    this.totalPages$ = this.store.pipe(select(getTotalPages));
    this.sort$ = this.store.pipe(select(getSort));
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  openEmailModal(): void {
    if (this.getSelectedParts().length) {
      this.dialogService.open(EmailModalComponent, {
        size: 'large',
        data: {
          header: DEFAULT_MODAL_TITLE,
          headerDescription: DEFAULT_MODAL_INTRO,
          subject: 'Question about MyArrow Forecast Details Dated ' + this.details.received,
          payload: {
            receivedDate: this.details.received,
            forecastPartList: this.getSelectedParts(),
            salesRep: this.salesRepName !== this.CUST_NAME_INHOSUE ? this.salesRepName : this.CUST_NAME_REPRESENTATIVE,
            salesPhone: this.salesRepPhoneNumber,
            salesMail: this.salesRepEmail,
          },
          templateName: 'forecast',
          ccSelf: 'true',
        },
      });
    }
  }

  openDownloadModalDialog(): void {
    this.dialogService.open(ForecastDownloadComponent, {
      size: 'large',
    });
  }

  toggleMoreWeeks(): void {
    this.moreWeeks = !this.moreWeeks;
    this.moreWeeksText = this.moreWeeks ? 'More' : 'Less';
  }

  toggleAll(): void {
    this.selectAll = !this.selectAll;
    this.parts = this.parts.map(obj => {
      obj.selected = this.selectAll;
      return obj;
    });
  }

  public toggleSelected(selectedPart: IForecastPartList, isSelected: boolean): void {
    const id = selectedPart.itemId;
    const index: number = this.parts.findIndex(x => x.itemId === id);
    this.parts[index].selected = !this.parts[index].selected;
  }

  getSelectedParts() {
    return this.parts ? this.parts.filter(part => part.selected) : [];
  }

  pageChange(page: number) {
    this.store.dispatch(new ChangePage(page));
  }

  /**
   * Saves the specified page limit, triggering a new search
   * @param pageLimit the number of items to show per page
   */
  pageLimitChange(pageLimit: number): void {
    this.store.dispatch(new ChangePageLimit(pageLimit));
  }

  sortChange(sortCriteria?: Array<ISortCriteron>): void {
    this.store.dispatch(new ChangeSort(sortCriteria));
  }

  /**
   * Submits a search criteria to the Query; will trigger a Query if all other facets are also set
   * @param searchCriteria the search criteria that is saved to the state
   */
  public submitSearch(searchCriteria: ISearchCriteria): void {
    this.store.dispatch(new SubmitSearch({ ...searchCriteria }));
  }

  toggleShortagesOnly(): void {
    this.showShortages = !this.showShortages;
    this.store.dispatch(new ChangeShortagesOnly(this.showShortages));
  }

  openBillToDialog(): void {
    this.dialogService.open(BillToAccountsComponent);
  }

  changePotentialShortagesValue(value): void {
    this.potentialShortages = value;
    this.store.dispatch(new ChangePotentialShortages(this.potentialShortages));
  }
}
