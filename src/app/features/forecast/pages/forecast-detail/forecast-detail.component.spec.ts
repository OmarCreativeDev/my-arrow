import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { Store } from '@ngrx/store';
import { RouterTestingModule } from '@angular/router/testing';

import { Dialog, DialogService } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { ForecastDetailComponent } from './forecast-detail.component';
import mockedForecasts from '@app/core/forecast/forecast-mock-response';
import { ApiService } from '@app/core/api/api.service';
import { IForecastPartList } from '@app/core/forecast/forecast.interfaces';
import { mockUserInHouse } from '@app/core/user/user.service.spec';
import { mockDomainProps } from '@app/core/properties/properties.service.spec';
import { ISortCriteronOrderEnum, ISearchCriteria } from '@app/shared/shared.interfaces';
import { of } from 'rxjs';

class MockStore {
  select() {
    return of();
  }
  dispatch() {}
  pipe() {
    return of();
  }
}

class MockApiService {
  public get() {
    return of();
  }
}

describe('ForecastDetailComponent', () => {
  let component: ForecastDetailComponent;
  let fixture: ComponentFixture<ForecastDetailComponent>;
  let dialogService: DialogService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ForecastDetailComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      imports: [RouterTestingModule],
      providers: [
        { provide: Dialog, useClass: DialogMock },
        { provide: DialogService, useClass: DialogServiceMock },
        { provide: Store, useClass: MockStore },
        { provide: ApiService, useClass: MockApiService },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastDetailComponent);
    component = fixture.componentInstance;
    dialogService = TestBed.get(DialogService);
    component.forecasts$ = of(mockedForecasts);
    component.forecastsDetails$ = of(mockedForecasts);
    component.userProfile$ = of(mockUserInHouse);
    component.publicDomains$ = of(mockDomainProps);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call toggleMoreWeeks', () => {
    spyOn(component, 'toggleMoreWeeks').and.callThrough();
    component.toggleMoreWeeks();
    fixture.detectChanges();
    expect(component.toggleMoreWeeks).toHaveBeenCalled();
  });

  it('should use the dialog service to open download', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.openDownloadModalDialog();
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should call `dialogService` on `openEmailRepresentativeDialog()', () => {
    const parts = [
      { custItemId: '26-1016-0006-7', mfrItemId: 'OP184ESZ-REEL7', mfrName: 'ADI', quantityPublic: 0, mult: 1000, selected: false },
      { custItemId: '26-1014-7590-8', mfrItemId: 'AD8402ARUZ10', mfrName: 'ADI', quantityPublic: 660, mult: 192, selected: false },
    ];
    component.parts = parts;
    component.toggleAll();
    spyOn(dialogService, 'open').and.callThrough();
    component.openEmailModal();
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should select/deselect all parts on toggleAll', () => {
    const parts = [
      { custItemId: '26-1016-0006-7', mfrItemId: 'OP184ESZ-REEL7', mfrName: 'ADI', quantityPublic: 0, mult: 1000, selected: false },
      { custItemId: '26-1014-7590-8', mfrItemId: 'AD8402ARUZ10', mfrName: 'ADI', quantityPublic: 660, mult: 192, selected: false },
    ];
    component.parts = parts;
    component.toggleAll();
    expect(component.parts[0].selected).toBe(true);
    expect(component.parts[1].selected).toBe(true);
    component.toggleAll();
    expect(component.parts[0].selected).toBe(false);
    expect(component.parts[1].selected).toBe(false);
  });

  it('should select seleceted parts on toggleSelected', () => {
    const parts = [
      {
        custItemId: '26-1016-0006-7',
        mfrItemId: 'OP184ESZ-REEL7',
        mfrName: 'ADI',
        itemId: 6228106,
        quantityPublic: 0,
        mult: 1000,
        selected: false,
      },
      {
        custItemId: '26-1014-7590-8',
        mfrItemId: 'AD8402ARUZ10',
        mfrName: 'ADI',
        itemId: 679112,
        quantityPublic: 660,
        mult: 192,
        selected: false,
      },
      {
        custItemId: '26-1014-7590-9',
        mfrItemId: 'AD8402ARDRTY',
        mfrName: 'ADI',
        itemId: 679113,
        quantityPublic: 660,
        mult: 224,
        selected: false,
      },
    ];
    const part: IForecastPartList = {
      custItemId: '26-1016-0006-7',
      mfrItemId: 'OP184ESZ-REEL7',
      mfrName: 'ADI',
      quantityPublic: 0,
      mult: 1000,
      itemId: 6228106,
      whsId: 1790,
      itemStatus: 'Active',
      factoryLeadTime: 27,
      restrictedWhCode: null,
      selected: false,
      firstShortDate: '2018-09-10T00:00:00',
      onOrderPipeline: { leadtime: 27, onOrderList: [{ orderDate: '2109', quantity: 9 }] },
      inventoryAvailableToSell: { publicQty: 0, bufferQty: 0, onSiteQty: 0 },
      horizonList: [{ id: 1, date: '2018-09-03T00:00:00', quantity: 928, cumulativeShort: 0 }],
    };
    component.parts = parts;
    component.toggleSelected(part, true);
    expect(component.parts[0].selected).toBe(true);
    expect(component.parts[1].selected).toBe(false);
    expect(component.parts[2].selected).toBe(false);
    expect(component.getSelectedParts().length).toBe(1);
  });

  it('should call toggleAll', () => {
    spyOn(component, 'toggleAll').and.callThrough();
    component.toggleAll();
    fixture.detectChanges();
    expect(component.toggleAll).toHaveBeenCalled();
  });

  it('should call pageChange', () => {
    spyOn(component, 'pageChange').and.callThrough();
    component.pageChange(2);
    fixture.detectChanges();
    expect(component.pageChange).toHaveBeenCalled();
  });

  it('should call pageLimitChange', () => {
    spyOn(component, 'pageLimitChange').and.callThrough();
    component.pageLimitChange(2);
    fixture.detectChanges();
    expect(component.pageLimitChange).toHaveBeenCalled();
  });

  it('should call sortChange', () => {
    const sort = [
      {
        key: 'customerPartNumber',
        order: ISortCriteronOrderEnum.ASC,
      },
    ];
    spyOn(component, 'sortChange').and.callThrough();
    component.sortChange(sort);
    fixture.detectChanges();
    expect(component.sortChange).toHaveBeenCalled();
  });

  it('should call submitSearch', () => {
    const search: ISearchCriteria = {
      value: 'test',
      type: 'test',
    };
    spyOn(component, 'submitSearch').and.callThrough();
    component.submitSearch(search);
    fixture.detectChanges();
    expect(component.submitSearch).toHaveBeenCalled();
  });

  it('should call toggleShortagesOnly', () => {
    spyOn(component, 'toggleShortagesOnly').and.callThrough();
    component.toggleShortagesOnly();
    fixture.detectChanges();
    expect(component.toggleShortagesOnly).toHaveBeenCalled();
  });

  it('should call `dialogService` on `openBillToDialog()', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.openBillToDialog();
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should call changePotentialShortagesValue', () => {
    const potentialShortage: number = 12;
    spyOn(component, 'changePotentialShortagesValue').and.callThrough();
    component.changePotentialShortagesValue(potentialShortage);
    fixture.detectChanges();
    expect(component.changePotentialShortagesValue).toHaveBeenCalled();
  });
});
