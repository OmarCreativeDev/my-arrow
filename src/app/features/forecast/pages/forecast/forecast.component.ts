import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { at, reject, isNil, find } from 'lodash-es';

import { getForecastSummaryDetails } from '@app/features/forecast/stores/forecast/forecast.selectors';
import { GetForecastSummary } from '@app/features/forecast/stores/forecast/forecast.actions';
import { IForecast } from '@app/core/forecast/forecast.interfaces';
import { IListingState, IAppState } from '@app/shared/shared.interfaces';
import { IBillTo } from '@app/core/user/user.interface';
import { RequestBillToAccounts } from '@app/core/user/store/user.actions';
import { getAccountNumber, getUserBillToAccount, getUserBillToAccounts } from '@app/core/user/store/user.selectors';
import { ForecastDownloadComponent } from '@app/features/forecast/components/forecast-download/forecast-download.component';
import { DialogService } from '@app/core/dialog/dialog.service';
import { tap, filter, take } from 'rxjs/operators';
import { BillToAccountsComponent } from '@app/shared/components/bill-to-accounts/bill-to-accounts.component';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.scss'],
})
export class ForecastComponent implements OnInit, OnDestroy {
  forecast: IForecast;
  forecastSummary$: Observable<any>;
  accountNumber$: Observable<number>;
  billToAccounts: IListingState<IBillTo> = {
    loading: true,
    error: null,
    items: [],
  };
  billToAccounts$: Observable<IListingState<IBillTo>>;
  originalBillToAccountId: number;
  selectedBillToAccountId: number;
  billToAddress: string;
  billToId: number;
  private getSelectedBillToSub: Subscription;
  private subscription: Subscription = new Subscription();

  constructor(private store: Store<IAppState>, private router: Router, private dialogService: DialogService) {
    this.accountNumber$ = this.store.pipe(select(getAccountNumber));
    this.billToAccounts$ = store.pipe(select(getUserBillToAccounts));
    this.forecastSummary$ = store.pipe(select(getForecastSummaryDetails));
    this.store.dispatch(new GetForecastSummary());
  }

  getAddress(addressObject): string {
    const addressProps = ['address1', 'address2', 'address3', 'address4', 'city', 'state', 'postCode', 'postalCode'];
    let address = at(addressObject, addressProps);
    address = reject(address, line => {
      return isNil(line) || !/\S/.test(line);
    });
    address = address.join(', ');

    return address;
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeBillTo());
    this.subscription.add(this.subscribeForecast());
    this.subscription.add(this.subscribeBillToId());
  }

  private subscribeBillToId(): Subscription {
    return (this.getSelectedBillToSub = this.store.pipe(select(getUserBillToAccount)).subscribe(billToAccountId => {
      if (this.billToId > 0 && billToAccountId !== this.billToId) {
        this.store.dispatch(new GetForecastSummary());
      }
      this.billToId = this.selectedBillToAccountId = billToAccountId;
    }));
  }

  private subscribeForecast(): Subscription {
    return this.forecastSummary$.subscribe(forecasts => {
      this.forecast = forecasts || undefined;
    });
  }

  private subscribeBillTo(): Subscription {
    return this.billToAccounts$
      .pipe(
        tap(billToAccounts => {
          /* istanbul ignore else */
          if (!billToAccounts) {
            this.store.dispatch(new RequestBillToAccounts());
          }
        }),
        filter(billToAccounts => billToAccounts !== undefined),
        take(1)
      )
      .subscribe(billToAccounts => {
        /* istanbul ignore next */
        if (billToAccounts && billToAccounts.items) {
          this.billToAccounts = billToAccounts;
          this.billToAddress = this.getAddress(find(billToAccounts.items, { billToId: this.billToId }));
        }
      });
  }

  openDownloadModalDialog(): void {
    this.dialogService.open(ForecastDownloadComponent, {
      size: 'large',
    });
  }

  viewDetails(): void {
    this.router.navigateByUrl('forecast/details');
  }

  openBillToDialog(): void {
    this.dialogService.open(BillToAccountsComponent);
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }
}
