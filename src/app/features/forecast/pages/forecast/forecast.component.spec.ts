import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';

import { ForecastComponent } from './forecast.component';
import { Dialog, DialogService } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import mockedForecasts from '@app/core/forecast/forecast-mock-response';

class MockStore {
  select() {
    return of();
  }
  dispatch() {}
  pipe() {
    return of();
  }
}

class MockRouter {
  navigateByUrl(url: string) {
    return url;
  }
}

const mockAddress = {
  accountId: 1305827,
  accountNumber: 1067767,
  address1: ' ',
  address2: '9340 Owensmouth Ave',
  address3: '',
  address4: '',
  billToId: 2171627,
  city: 'Chatsworth',
  country: 'US',
  countryDescription: 'United States',
  ebsPersonId: 6161197,
  name: 'ONCORE MANUFACTURING SERVICES',
  orgId: 241,
  postalCode: '91311',
  province: '',
  selected: null,
  state: 'CA',
};

describe('ForecastComponent', () => {
  let component: ForecastComponent;
  let fixture: ComponentFixture<ForecastComponent>;
  let dialogService: DialogService;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ForecastComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: Dialog, useClass: DialogMock },
        { provide: DialogService, useClass: DialogServiceMock },
        { provide: Store, useClass: MockStore },
        { provide: Router, useClass: MockRouter },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForecastComponent);
    component = fixture.componentInstance;
    dialogService = TestBed.get(DialogService);
    router = TestBed.get(Router);

    component.forecastSummary$ = of(mockedForecasts);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should use the dialog service to open a download dialog', () => {
    spyOn(dialogService, 'open');
    component.openDownloadModalDialog();
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should redirect user to results page', () => {
    const spy = spyOn(router, 'navigateByUrl');
    component.viewDetails();
    const url = spy.calls.first().args[0];
    expect(url).toBe('forecast/details');
  });

  it('should call getAddress', () => {
    spyOn(component, 'getAddress').and.callThrough();
    component.getAddress(mockAddress);
    fixture.detectChanges();
    expect(component.getAddress).toHaveBeenCalled();
  });

  it('should call `dialogService` on `openBillToDialog()', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.openBillToDialog();
    expect(dialogService.open).toHaveBeenCalled();
  });
});
