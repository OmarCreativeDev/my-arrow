import { Action } from '@ngrx/store';
import {
  IProductSearchQueryRequest,
  IAppliedFilters,
  ISortProductSearch,
  IProductAdditionalFilter,
  IProductSearchQueryState,
} from '@app/core/inventory/product-search.interface';
import { IProduct } from '@app/shared/shared.interfaces';

export enum ProductSearchActionTypes {
  ADD_PRODUCT_SEARCH_FILTER = '[Product Search] Add Filter',
  ADD_PRODUCT_SEARCH_MANUFACTURER = '[Product Search] Add Manufacturer',
  CHANGE_PRODUCT_SEARCH_PAGE_LIMIT = '[Product Search] Change Page Limit',
  CHANGE_PRODUCT_SEARCH_PAGE = '[Product Search] Change page',
  SUBMIT_PRODUCT_SEARCH_QUERY = '[Product Search] Query',
  SUBMIT_PRODUCT_SEARCH_QUERY_COMPLETE = '[Product Search] Query Complete',
  SUBMIT_PRODUCT_SEARCH_QUERY_ERROR = '[Product Search] Query Error',
  REMOVE_PRODUCT_SEARCH_FILTER = '[Product Search] Remove Filter',
  REMOVE_PRODUCT_SEARCH_MANUFACTURER = '[Product Search] Remove Manufacturer',
  SET_PRODUCT_SEARCH_CATEGORY = '[Product Search] Set Category',
  SET_PRODUCT_SEARCH_SORT_OPTIONS = '[Product Search] Set Sort Options',
  SUBMIT_PRODUCT_SEARCH_FILTERS = '[Product Search] Submit Filters',
  SUBMIT_PRODUCT_SEARCH = '[Product Search] Submit Search',
  PRODUCT_CLICKED = '[Product Search] Product Clicked',
  TOGGLE_PRODUCT_SEARCH_SELECTED_IDS = '[Product Search] Toggle Selected',
  TOGGLE_PRODUCT_SEARCH_QUOTE_TOOLTIP = '[Product Search] Toggle Quote Tooltip',
}

export class AddFilter implements Action {
  readonly type = ProductSearchActionTypes.ADD_PRODUCT_SEARCH_FILTER;
  constructor(public payload: string) {}
}

export class AddManufacturer implements Action {
  readonly type = ProductSearchActionTypes.ADD_PRODUCT_SEARCH_MANUFACTURER;
  constructor(public payload: IProductAdditionalFilter) {}
}

/**
 * Changes the limit on each page
 */
export class ChangePageLimit implements Action {
  readonly type = ProductSearchActionTypes.CHANGE_PRODUCT_SEARCH_PAGE_LIMIT;
  constructor(public payload: number) {}
}

/**
 * Change the page number
 */
export class ChangePage implements Action {
  readonly type = ProductSearchActionTypes.CHANGE_PRODUCT_SEARCH_PAGE;
  constructor(public payload: number) {}
}

/**
 * Updates the Query with the provided facets
 */
export class SubmitProductSearchQuery implements Action {
  readonly type = ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH_QUERY;
  constructor(public payload: IProductSearchQueryRequest) {}
}

/**
 * Results of the Query
 */
export class SubmitProductSearchQueryComplete implements Action {
  readonly type = ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH_QUERY_COMPLETE;
  constructor(public payload: IProductSearchQueryState) {}
}

/**
 * Thrown if the Query received an Error
 */
export class SubmitProductSearchQueryError implements Action {
  readonly type = ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH_QUERY_ERROR;
  constructor(public payload: Error) {}
}

export class RemoveFilter implements Action {
  readonly type = ProductSearchActionTypes.REMOVE_PRODUCT_SEARCH_FILTER;
  constructor(public payload: string) {}
}

export class RemoveManufacturer implements Action {
  readonly type = ProductSearchActionTypes.REMOVE_PRODUCT_SEARCH_MANUFACTURER;
  constructor(public payload: IProductAdditionalFilter) {}
}

export class SetCategory implements Action {
  readonly type = ProductSearchActionTypes.SET_PRODUCT_SEARCH_CATEGORY;
  constructor(public payload: string) {}
}

/**
 * Set sort options
 */
export class SetSortOptions implements Action {
  readonly type = ProductSearchActionTypes.SET_PRODUCT_SEARCH_SORT_OPTIONS;
  constructor(public payload: ISortProductSearch) {}
}

/**
 * Submit the filters facets for the Query
 */
export class SubmitFilters implements Action {
  readonly type = ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH_FILTERS;
  constructor(public payload: IAppliedFilters) {}
}

export class SubmitSearch implements Action {
  readonly type = ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH;
  constructor(public payload: string) {}
}

/**
 * Action when a product is clicked
 */
export class ProductClicked implements Action {
  readonly type = ProductSearchActionTypes.PRODUCT_CLICKED;
  constructor(public payload: { product: IProduct; index: number }) {}
}

export class ToggleSelected implements Action {
  readonly type = ProductSearchActionTypes.TOGGLE_PRODUCT_SEARCH_SELECTED_IDS;
  constructor(public payload: Array<string>, public select: boolean) {}
}

export class ToggleQuoteTooltip implements Action {
  readonly type = ProductSearchActionTypes.TOGGLE_PRODUCT_SEARCH_QUOTE_TOOLTIP;
  constructor(public payload: number) {}
}
