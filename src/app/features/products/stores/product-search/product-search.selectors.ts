import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IProductSearchListingState } from 'app/core/inventory/product-search.interface';

/**
 * Selects the Product search state from the root state object
 */
export const getProductSearchState = createFeatureSelector<IProductSearchListingState>('productSearch');

/**
 * Retrieves the loading state
 */
export const getLoading = createSelector(getProductSearchState, productSearchState => productSearchState.loading);

/**
 * Retrieves the search
 */
export const getSearchQuery = createSelector(getProductSearchState, productSearchState => productSearchState.searchQuery);

/**
 * Retrieve the categories
 */
export const getCategories = createSelector(getProductSearchState, productSearchState => productSearchState.categories);

/**
 * Retrieve the filters
 */
export const getFilters = createSelector(getProductSearchState, productSearchState => productSearchState.filters);

/**
 * Retrieve the manufacturers
 */
export const getManufacturers = createSelector(getProductSearchState, productSearchState => productSearchState.manufacturers);

/**
 * Retrieves the selected filters
 */
export const getSelectedFilters = createSelector(getProductSearchState, productSearchState => productSearchState.selectedFilters);

/**
 * Retrieves the selected manufacturers
 */
export const getSelectedManufacturers = createSelector(
  getProductSearchState,
  productSearchState => productSearchState.selectedManufacturers
);

/**
 * Retrieves the applied product filters
 */
export const getAppliedFilters = createSelector(getProductSearchState, productSearchState => productSearchState.appliedFilters);

/**
 * Retrieves the applied manufacturers
 */
export const getAppliedManufacturers = createSelector(getProductSearchState, productSearchState => productSearchState.appliedManufacturers);

/**
 * Retrieve the applied category
 */
export const getAppliedCategory = createSelector(getProductSearchState, productSearchState => productSearchState.appliedCategory);

/**
 * Retrieve the number of items that should show per page
 */
export const getLimit = createSelector(getProductSearchState, productSearchState => productSearchState.pagination.limit);

/**
 * Retrieve the number of items that should show per page
 */
export const getPage = createSelector(getProductSearchState, productSearchState => productSearchState.pagination.current);

/**
 * Retrieve the number of items that should show per page
 */
export const getTotalPages = createSelector(getProductSearchState, productSearchState => productSearchState.pagination.total);

/**
 * Retrieve the sort options
 */
export const getSortOptions = createSelector(getProductSearchState, productSearchState => productSearchState.sort);

/**
 * Retrieve the error state
 */
export const getError = createSelector(getProductSearchState, productSearchState => productSearchState.error);

/**
 * Retrieve the error code message
 */
export const getErrorCodeMessage = createSelector(getProductSearchState, productSearchState => productSearchState.errorCodeMessage);

/**
 * Retrieve the product search results
 */
export const getSearchResults = createSelector(getProductSearchState, productSearchState => productSearchState.products);

/**
 * Retrieve the total number of products
 */
export const getProductsTotal = createSelector(getProductSearchState, productSearchState => productSearchState.productsTotal);

/**
 * Retrieve global inventory info
 */
export const getGlobalInventory = createSelector(getProductSearchState, productSearchState => productSearchState.globalInventory);

export const getSelectedIds = createSelector(getProductSearchState, productSearchState => productSearchState.selectedIds);

export const getSelectedProducts = createSelector(getSelectedIds, getSearchResults, (ids, products) => {
  if (!products) {
    return [];
  }

  return products.filter(product => ids.includes(product.id));
});

export const getAllSelected = createSelector(getSelectedIds, getSearchResults, (ids, products) => {
  if (ids.length > 0) {
    return ids.length === products.length;
  }

  return false;
});

export const getQuoteOnlyItemsLength = createSelector(getProductSearchState, productSearchState => productSearchState.quoteOnlyItemsLength);
