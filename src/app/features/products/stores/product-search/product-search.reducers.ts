import { ProductSearchActionTypes } from '@app/features/products/stores/product-search/product-search.actions';
import { IProductSearchListingState } from 'app/core/inventory/product-search.interface';
import { uniq, without, includes } from 'lodash-es';

export const INITIAL_SEARCH_STATE: IProductSearchListingState = {
  appliedCategory: '',
  appliedFilters: [],
  appliedManufacturers: [],
  error: null,
  errorCodeMessage: '',
  pagination: {
    limit: 10,
    current: 1,
    total: 0,
  },
  loading: true,
  products: [],
  productsTotal: null,
  searchQuery: '',
  selectedFilters: [],
  selectedIds: [],
  selectedManufacturers: [],
  quoteOnlyItemsLength: 0,
};

export function productSearchReducer(state: IProductSearchListingState = INITIAL_SEARCH_STATE, action: any) {
  switch (action.type) {
    case ProductSearchActionTypes.ADD_PRODUCT_SEARCH_FILTER: {
      const selectedFilters = [...state.selectedFilters];

      if (!selectedFilters.includes(action.payload)) {
        selectedFilters.push(action.payload);
      }

      return {
        ...state,
        selectedFilters: selectedFilters,
      };
    }

    case ProductSearchActionTypes.ADD_PRODUCT_SEARCH_MANUFACTURER: {
      const selectedManufacturers = [...state.selectedManufacturers];

      if (!includes(selectedManufacturers, action.payload)) {
        selectedManufacturers.push(action.payload);
      }

      return {
        ...state,
        selectedManufacturers: selectedManufacturers,
      };
    }

    case ProductSearchActionTypes.CHANGE_PRODUCT_SEARCH_PAGE_LIMIT: {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          limit: action.payload,
        },
      };
    }

    case ProductSearchActionTypes.CHANGE_PRODUCT_SEARCH_PAGE: {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          current: action.payload,
        },
      };
    }

    case ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH_QUERY: {
      return {
        ...state,
        error: false,
        loading: true,
        products: null,
        selectedIds: [],
      };
    }

    case ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH_QUERY_COMPLETE: {
      return {
        ...state,
        error: false,
        loading: false,
        additionalFilters: action.payload.additionalFilters,
        categories: action.payload.categories,
        filters: action.payload.filters,
        globalInventory: action.payload.globalInventory,
        manufacturers: action.payload.manufacturers,
        pagination: {
          ...state.pagination,
          current: action.payload.pagination ? action.payload.pagination.current : INITIAL_SEARCH_STATE.pagination.current,
          total: action.payload.pagination ? action.payload.pagination.total : INITIAL_SEARCH_STATE.pagination.total,
        },
        products: action.payload.products,
        productsTotal: action.payload.productsTotal,
      };
    }

    case ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH_QUERY_ERROR: {
      return {
        ...state,
        errorCodeMessage: action.payload.error.messages[0],
        error: action.payload,
        loading: false,
        products: null,
      };
    }

    case ProductSearchActionTypes.REMOVE_PRODUCT_SEARCH_FILTER: {
      let selectedFilters = [...state.selectedFilters];

      if (selectedFilters.includes(action.payload)) {
        selectedFilters = without(selectedFilters, action.payload);
      }

      return {
        ...state,
        selectedFilters: selectedFilters,
      };
    }

    case ProductSearchActionTypes.REMOVE_PRODUCT_SEARCH_MANUFACTURER: {
      const selectedManufacturers = state.selectedManufacturers.filter(item => item.name !== action.payload.name);

      return {
        ...state,
        selectedManufacturers: selectedManufacturers,
      };
    }

    case ProductSearchActionTypes.SET_PRODUCT_SEARCH_CATEGORY: {
      return {
        ...state,
        appliedCategory: action.payload,
      };
    }

    case ProductSearchActionTypes.SET_PRODUCT_SEARCH_SORT_OPTIONS: {
      return {
        ...state,
        sort: action.payload,
      };
    }

    case ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH_FILTERS: {
      const appliedCategory = action.payload.appliedCategory || '';
      const appliedFilters = action.payload.appliedFilters || [];
      const appliedManufacturers = action.payload.appliedManufacturers || [];
      const selectedFilters = action.payload.clearSelectedFilters ? [] : state.selectedFilters;
      const selectedManufacturers = action.payload.clearSelectedFilters ? [] : state.selectedManufacturers;

      return {
        ...state,
        appliedCategory: appliedCategory,
        appliedFilters: appliedFilters,
        appliedManufacturers: appliedManufacturers,
        selectedFilters: selectedFilters,
        selectedManufacturers: selectedManufacturers,
      };
    }

    case ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH: {
      return {
        ...state,
        searchQuery: action.payload,
      };
    }

    case ProductSearchActionTypes.TOGGLE_PRODUCT_SEARCH_SELECTED_IDS: {
      let selectedIds = [...state.selectedIds];

      if (action.select) {
        selectedIds.push(...action.payload);
      } else {
        selectedIds = without(selectedIds, ...action.payload);
      }

      return {
        ...state,
        selectedIds: uniq(selectedIds),
      };
    }

    case ProductSearchActionTypes.TOGGLE_PRODUCT_SEARCH_QUOTE_TOOLTIP: {
      return {
        ...state,
        quoteOnlyItemsLength: action.payload,
      };
    }

    default:
      return state;
  }
}
