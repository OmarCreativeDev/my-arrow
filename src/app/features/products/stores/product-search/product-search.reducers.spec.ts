import {
  IProductSearchListingState,
  IProductAdditionalFilter,
  IProductSearchQueryRequest,
  IProductSearchQueryState,
  IAppliedFilters,
  ISortProductSearch,
} from '@app/core/inventory/product-search.interface';
import {
  AddFilter,
  AddManufacturer,
  ChangePageLimit,
  ChangePage,
  SubmitProductSearchQuery,
  SubmitProductSearchQueryComplete,
  RemoveFilter,
  SubmitFilters,
  SetCategory,
  SetSortOptions,
  ToggleSelected,
} from './product-search.actions';
import { productSearchReducer } from './product-search.reducers';

const searchResultsMock: IProductSearchQueryState = {
  productsTotal: 2,
  products: [
    {
      docId: 'xxxx_xxxx',
      itemId: 1,
      mpn: 'JAYZ',
      manufacturer: 'orange ltd',
      description: 'microphone',
      image: '/product-image.png',
      itemType: 'COMPONENTS',
      compliance: {},
      warehouseId: 145,
      quotable: false,
      arrowReel: true,
      minimumOrderQuantity: 100,
      availableQuantity: 10,
      bufferQuantity: 200,
      leadTime: 1,
      customerPartNumber: 'LOB1',
      price: 22.99,
      pipeline: {
        deliveryDate: '03/06/2018',
        quantity: 7000,
      },
      spq: 578,
      multipleOrderQuantity: 75,
      id: '1234_12345678',
      specs: [
        {
          name: 'EU RoHS Compliant',
          value: 'ON OFFF',
        },
      ],
    },
  ],
  categories: [
    {
      name: 'test category',
      secondaryCategories: [
        {
          name: 'secondary category',
          tertiaryCategories: [
            {
              name: 'tertiary category',
              productsTotal: 333,
              id: '^2015/13290/2023',
            },
          ],
        },
      ],
    },
  ],
  query: 'test',
};

describe('Product Search reducers', () => {
  let initialState: IProductSearchListingState;

  beforeEach(() => {
    initialState = {
      appliedCategory: '',
      appliedFilters: [],
      appliedManufacturers: [],
      error: null,
      errorCodeMessage: '',
      pagination: {
        limit: 10,
        current: 1,
        total: 0,
      },
      loading: false,
      products: [],
      productsTotal: null,
      searchQuery: '',
      selectedFilters: ['Test Filter 1'],
      selectedIds: [],
      selectedManufacturers: [],
      quoteOnlyItemsLength: 0,
    };
  });

  it(`ADD_PRODUCT_SEARCH_FILTER should update filters array if filter doesn't exist yet`, () => {
    const action = new AddFilter('Test Filter 2');
    const result = productSearchReducer(initialState, action);
    expect(result.selectedFilters).toContain('Test Filter 2');
    expect(result.error).toBe(null);
  });

  it(`ADD_PRODUCT_SEARCH_MANUFACTURER should update manufacturers array if filter doesn't exist yet`, () => {
    const additionalFilter: IProductAdditionalFilter = {
      id: 'Comchip Technology|funky|String',
      name: 'Comchip Technology',
      productsTotal: 1,
    };

    const action = new AddManufacturer(additionalFilter);
    const result = productSearchReducer(initialState, action);
    expect(result.selectedManufacturers).toContain(additionalFilter);
    expect(result.error).toBe(null);
  });

  it(`CHANGE_PRODUCT_SEARCH_PAGE_LIMIT should update the page limit in pagination`, () => {
    const action = new ChangePageLimit(20);
    const result = productSearchReducer(initialState, action);
    expect(result.pagination.limit).toBe(20);
    expect(result.error).toBe(null);
  });

  it(`CHANGE_PRODUCT_SEARCH_PAGE should update the current page`, () => {
    const action = new ChangePage(2);
    const result = productSearchReducer(initialState, action);
    expect(result.pagination.current).toBe(2);
    expect(result.error).toBe(null);
  });

  it(`SUBMIT_PRODUCT_SEARCH_QUERY should set loading flag to true and products to null`, () => {
    const mockSearchRequest: IProductSearchQueryRequest = {
      searchQuery: '',
      paging: {
        limit: 10,
        page: 1,
      },
    };

    const action = new SubmitProductSearchQuery(mockSearchRequest);
    const result = productSearchReducer(initialState, action);
    expect(result.products).toBeNull();
    expect(result.loading).toBeTruthy();
  });

  it(`SUBMIT_PRODUCT_SEARCH_QUERY_COMPLETE should update state with search results`, () => {
    const action = new SubmitProductSearchQueryComplete(searchResultsMock);
    const result = productSearchReducer(initialState, action);
    expect(result.products).toContain(searchResultsMock.products[0]);
    expect(result.categories).toContain(searchResultsMock.categories[0]);
    expect(result.loading).toBeFalsy();
  });

  it(`REMOVE_PRODUCT_SEARCH_FILTER should remove the given filter from the state`, () => {
    const action = new RemoveFilter('Test Filter 1');
    const result = productSearchReducer(initialState, action);
    expect(result.selectedFilters).not.toContain('Test Filter 1');
    expect(result.loading).toBeFalsy();
  });

  it(`SUBMIT_PRODUCT_SEARCH_FILTERS should remove the given filter from the state`, () => {
    const mockFilters: IAppliedFilters = {
      appliedFilters: [],
      appliedManufacturers: [],
    };
    const action = new SubmitFilters(mockFilters);
    const result = productSearchReducer(initialState, action);
    expect(result.appliedFilters.length).toBe(0);
  });

  it(`SET_PRODUCT_SEARCH_CATEGORY should add search category if it doesn't exist yet in the state`, () => {
    const mockCategory = 'test category';
    const action = new SetCategory('test category');
    const result = productSearchReducer(initialState, action);
    expect(result.appliedCategory).toBe(mockCategory);
  });

  it(`SET_PRODUCT_SEARCH_SORT_OPTIONS should update current sorting options in the state`, () => {
    const mockSortOptions: ISortProductSearch = { sortField: 'testField', sortDirection: 'ascending' };
    const action = new SetSortOptions(mockSortOptions);
    const result = productSearchReducer(initialState, action);
    expect(result.sort).toBe(mockSortOptions);
  });

  it(`TOGGLE_PRODUCT_SEARCH_SELECTED_IDS should update selected ids in the state to be toggled`, () => {
    const action = new ToggleSelected(['1790_18978016'], true);
    const result = productSearchReducer(initialState, action);
    expect(result.selectedIds).toContain('1790_18978016');
  });
});
