import { Injectable } from '@angular/core';
import { Observable, of, combineLatest } from 'rxjs';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { debounceTime, switchMap, map, catchError } from 'rxjs/operators';

import { InventoryService } from 'app/core/inventory/inventory.service';
import {
  ProductSearchActionTypes,
  SubmitProductSearchQuery,
  SubmitProductSearchQueryComplete,
  SubmitProductSearchQueryError,
  ChangePage,
} from '@app/features/products/stores/product-search/product-search.actions';
import { IProductSearch } from 'app/core/inventory/product-search.interface';

@Injectable()
export class ProductSearchEffects {
  @Effect()
  facets$: Observable<any> = combineLatest(
    this.actions$.pipe(ofType(ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH)),
    this.actions$.pipe(ofType(ProductSearchActionTypes.SET_PRODUCT_SEARCH_CATEGORY)),
    this.actions$.pipe(ofType(ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH_FILTERS)),
    this.actions$.pipe(ofType(ProductSearchActionTypes.CHANGE_PRODUCT_SEARCH_PAGE_LIMIT)),
    this.actions$.pipe(ofType(ProductSearchActionTypes.CHANGE_PRODUCT_SEARCH_PAGE)),
    this.actions$.pipe(ofType(ProductSearchActionTypes.SET_PRODUCT_SEARCH_SORT_OPTIONS))
  ).pipe(
    debounceTime(50),
    map((actions: any) => {
      const payloads = actions.map(action => action.payload);

      return new SubmitProductSearchQuery({
        searchQuery: payloads[0],
        category: payloads[1],
        filters: payloads[2].appliedFilters,
        manufacturers: payloads[2].appliedManufacturers,
        sort: payloads[5],
        paging: {
          limit: payloads[3],
          page: payloads[4],
        },
      });
    })
  );

  /**
   * Side-effect for the Query being changed directly. Trigger by the facets$ Side-effect
   */
  @Effect()
  query$: Observable<any> = this.actions$.pipe(
    ofType(ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH_QUERY),
    switchMap((action: SubmitProductSearchQuery) => {
      return this.inventoryService.search(action.payload).pipe(
        map(
          (queryResponse: IProductSearch) => new SubmitProductSearchQueryComplete({ ...queryResponse, query: action.payload.searchQuery })
        ),
        catchError(err => of(new SubmitProductSearchQueryError(err)))
      );
    })
  );

  @Effect()
  resetPagination$: Observable<any> = this.actions$.pipe(
    ofType(ProductSearchActionTypes.SUBMIT_PRODUCT_SEARCH_FILTERS),
    map(() => new ChangePage(1))
  );

  constructor(private actions$: Actions, private inventoryService: InventoryService) {}
}
