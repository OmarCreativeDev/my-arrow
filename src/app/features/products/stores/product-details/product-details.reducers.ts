import { IProductDetailsState } from '@app/core/inventory/product-details.interface';
import { ProductDetailsActionTypes } from '@app/features/products/stores/product-details/product-details.actions';

export const INITIAL_PRODUCT_DETAILS_STATE: IProductDetailsState = {
  loading: true,
  product: undefined,
  error: undefined,
  crossReference: undefined,
};

export function productDetailsReducers(state: IProductDetailsState = INITIAL_PRODUCT_DETAILS_STATE, action: any) {
  switch (action.type) {
    case ProductDetailsActionTypes.GET_PRODUCT_DETAILS: {
      return {
        ...state,
        loading: true,
      };
    }
    case ProductDetailsActionTypes.GET_PRODUCT_DETAILS_SUCCESS: {
      return {
        ...state,
        loading: false,
        product: action.payload,
        error: undefined,
      };
    }
    case ProductDetailsActionTypes.GET_PRODUCT_CROSS_REFERENCE_SUCCESS: {
      return {
        ...state,
        crossReference: action.payload,
      };
    }
    case ProductDetailsActionTypes.GET_PRODUCT_DETAILS_FAILED: {
      return {
        loading: false,
        product: undefined,
        error: action.payload,
        crossReference: undefined,
      };
    }

    default:
      return state;
  }
}
