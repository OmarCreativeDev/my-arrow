import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IProductDetailsState } from '@app/core/inventory/product-details.interface';

/**
 * Selects cart state from the root state object
 */
export const getProductDetailsState = createFeatureSelector<IProductDetailsState>('productDetails');

/**
 * Retrieves the product details
 */
export const getProductDetails = createSelector(getProductDetailsState, productDetailsState => productDetailsState.product);
