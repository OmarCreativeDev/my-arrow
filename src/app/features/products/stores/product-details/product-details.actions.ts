import { HttpErrorResponse } from '@angular/common/http';
import { IProductDetails } from '@app/core/inventory/product-details.interface';
import { Action } from '@ngrx/store';
import { ISearchListing } from '@app/core/inventory/product-search.interface';

export enum ProductDetailsActionTypes {
  GET_PRODUCT_DETAILS = 'GET_PRODUCT_DETAILS',
  GET_PRODUCT_DETAILS_FAILED = 'GET_PRODUCT_DETAILS_FAILED',
  GET_PRODUCT_DETAILS_SUCCESS = 'GET_PRODUCT_DETAILS_SUCCESS',
  GET_PRODUCT_CROSS_REFERENCE_SUCCESS = 'GET_PRODUCT_CROSS_REFERENCE_SUCCESS',
}

export class GetProductDetails implements Action {
  readonly type = ProductDetailsActionTypes.GET_PRODUCT_DETAILS;
  constructor(public payload: string) {}
}

export class GetProductDetailsFailed implements Action {
  readonly type = ProductDetailsActionTypes.GET_PRODUCT_DETAILS_FAILED;
  constructor(public payload: HttpErrorResponse) {}
}

export class GetProductDetailsSuccess implements Action {
  readonly type = ProductDetailsActionTypes.GET_PRODUCT_DETAILS_SUCCESS;
  constructor(public payload: IProductDetails) {}
}
export class GetProductCrossReferenceSuccess implements Action {
  readonly type = ProductDetailsActionTypes.GET_PRODUCT_CROSS_REFERENCE_SUCCESS;
  constructor(public payload: ISearchListing) {}
}

export type ProductDetailsActions = GetProductDetails | GetProductDetailsFailed | GetProductDetailsSuccess;
