import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, catchError, switchMap } from 'rxjs/operators';

import {
  ProductDetailsActionTypes,
  GetProductDetails,
  GetProductDetailsSuccess,
  GetProductDetailsFailed,
  GetProductCrossReferenceSuccess,
} from '@app/features/products/stores/product-details/product-details.actions';
import { InventoryService } from '@app/core/inventory/inventory.service';
import { IProductDetails } from '@app/core/inventory/product-details.interface';
import { ISearchListing } from '@app/core/inventory/product-search.interface';

@Injectable()
export class ProductDetailsEffects {
  constructor(private actions$: Actions, private inventoryService: InventoryService) {}

  @Effect()
  requestProductDetails$ = this.actions$.pipe(
    ofType(ProductDetailsActionTypes.GET_PRODUCT_DETAILS),
    switchMap((action: GetProductDetails) => {
      return this.inventoryService.getProductDetails(action.payload).pipe(
        map((productDetails: IProductDetails) => new GetProductDetailsSuccess(productDetails)),
        catchError(err => of(new GetProductDetailsFailed(err)))
      );
    })
  );

  @Effect()
  requestProductCrossReference$ = this.actions$.pipe(
    ofType(ProductDetailsActionTypes.GET_PRODUCT_DETAILS),
    switchMap((action: GetProductDetails) => {
      return this.inventoryService.getProductCrossReference(action.payload).pipe(
        map((crossReference: ISearchListing) => new GetProductCrossReferenceSuccess(crossReference)),
        catchError(err => of(new GetProductCrossReferenceSuccess(undefined)))
      );
    })
  );
}
