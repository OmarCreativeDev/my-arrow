import { CatalogueActionTypes, CatalogueActions } from '@app/features/products/stores/catalogue/catalogue.actions';
import { ICatalogueState } from '@app/core/inventory/product-category.interface';

export const INITIAL_CATALOGUE_STATE: ICatalogueState = {
  loading: false,
  error: undefined,
  taxonomy: undefined,
  isTaxonomyLoaded: false,
  category: undefined,
  level: undefined,
};

export function catalogueReducers(state: ICatalogueState = INITIAL_CATALOGUE_STATE, action: CatalogueActions) {
  switch (action.type) {
    case CatalogueActionTypes.LOAD_CATALOGUE: {
      return {
        ...state,
        loading: true,
        error: undefined,
      };
    }
    case CatalogueActionTypes.LOAD_CATALOGUE_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: undefined,
        taxonomy: action.payload,
        isTaxonomyLoaded: true,
      };
    }
    case CatalogueActionTypes.LOAD_CATALOGUE_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload,
        taxonomy: undefined,
        isTaxonomyLoaded: false,
      };
    }
    case CatalogueActionTypes.SELECT_CATEGORY: {
      return {
        ...state,
        category: action.payload.name,
        level: action.payload.level,
      };
    }
    case CatalogueActionTypes.RESET_SELECTIONS: {
      return {
        ...state,
        category: INITIAL_CATALOGUE_STATE.category,
        level: INITIAL_CATALOGUE_STATE.level,
      };
    }
    default:
      return state;
  }
}
