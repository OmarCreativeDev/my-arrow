import { ICatalogueState, IProductCategory, ISelectedCategory } from '@app/core/inventory/product-category.interface';
import { LoadCatalogue, LoadCatalogueSuccess, LoadCatalogueFailed, SelectCategory, ResetSelections } from './catalogue.actions';
import { catalogueReducers } from './catalogue.reducers';

describe('Catalogue reducers', () => {
  let initialState: ICatalogueState;

  beforeEach(() => {
    initialState = {
      loading: true,
      error: undefined,
      taxonomy: undefined,
      isTaxonomyLoaded: false,
      category: undefined,
      level: undefined,
    };
  });

  it('LOAD_CATALOGUE should set the loading to true and remove any value from the error property', () => {
    const action = new LoadCatalogue({ warehouseList: ['V90', 'V91', 'V92'], billTo: 12345, region: 'AC' });
    const result = catalogueReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBe(undefined);
    expect(result.isTaxonomyLoaded).toBeFalsy();
    expect(result.taxonomy).toBe(undefined);
  });

  it('LOAD_CATALOGUE_SUCCESS should set the loading to false, set the hasTaxonomyLoaded to true and set the values for taxonomy', () => {
    const response: Array<IProductCategory> = [
      {
        id: 82,
        name: 'Electromechanical',
        totalProducts: 224241,
      },
    ];
    const action = new LoadCatalogueSuccess(response);
    const result = catalogueReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBe(undefined);
    expect(result.isTaxonomyLoaded).toBeTruthy();
    expect(result.taxonomy).toEqual(response);
  });

  it('LOAD_CATALOGUE_FAILED should set the loading to false and set the error property with the returned error', () => {
    const error: Error = new Error('Service returned some type of error');
    const action = new LoadCatalogueFailed(error);
    const result = catalogueReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toEqual(error);
    expect(result.isTaxonomyLoaded).toBeFalsy();
    expect(result.taxonomy).toBe(undefined);
  });

  it('SELECT_CATEGORY should set the selected category and the level of taxonomy', () => {
    const selection: ISelectedCategory = {
      name: 'Electromechanical',
      level: 2,
    };
    const action = new SelectCategory(selection);
    const result = catalogueReducers(initialState, action);
    expect(result.category).toEqual(selection.name);
    expect(result.level).toEqual(selection.level);
  });

  it('RESET_SELECTIONS should set the selected category and level of taxonomy to the initial state', () => {
    const action = new ResetSelections();
    const result = catalogueReducers(initialState, action);
    expect(result.category).toBe(undefined);
    expect(result.level).toBe(undefined);
  });
});
