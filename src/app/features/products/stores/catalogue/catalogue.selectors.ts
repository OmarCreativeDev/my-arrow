import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ICatalogueState } from '@app/core/inventory/product-category.interface';

export const getCatalogueState = createFeatureSelector<ICatalogueState>('catalogue');

export const getIsTaxonomyLoaded = createSelector(
  getCatalogueState,
  catalogueState => catalogueState.isTaxonomyLoaded
);

export const getTaxonomy = createSelector(
  getCatalogueState,
  catalogueState => catalogueState.taxonomy
);

export const getLoading = createSelector(
  getCatalogueState,
  catalogueState => catalogueState.loading
);

export const getError = createSelector(
  getCatalogueState,
  catalogueState => catalogueState.error
);
