import { Action } from '@ngrx/store';
import { IProductCategory, ISelectedCategory, IUserTaxonomy } from '@app/core/inventory/product-category.interface';

export enum CatalogueActionTypes {
  LOAD_CATALOGUE = 'LOAD_CATALOGUE',
  LOAD_CATALOGUE_SUCCESS = 'LOAD_CATALOGUE_SUCCESS',
  LOAD_CATALOGUE_FAILED = 'LOAD_CATALOGUE_FAILED',
  SELECT_CATEGORY = '[Product Catalogue] Select Category',
  RESET_SELECTIONS = '[Product Catalogue] Reset Selection',
}

export class LoadCatalogue implements Action {
  readonly type = CatalogueActionTypes.LOAD_CATALOGUE;
  constructor(public payload: IUserTaxonomy) {}
}

export class LoadCatalogueSuccess implements Action {
  readonly type = CatalogueActionTypes.LOAD_CATALOGUE_SUCCESS;
  constructor(public payload: Array<IProductCategory>) {}
}

export class LoadCatalogueFailed implements Action {
  readonly type = CatalogueActionTypes.LOAD_CATALOGUE_FAILED;
  constructor(public payload: Error) {}
}

export class SelectCategory implements Action {
  readonly type = CatalogueActionTypes.SELECT_CATEGORY;
  constructor(public payload: ISelectedCategory) {}
}

export class ResetSelections implements Action {
  readonly type = CatalogueActionTypes.RESET_SELECTIONS;
  constructor() {}
}

export type CatalogueActions = LoadCatalogue | LoadCatalogueSuccess | LoadCatalogueFailed | SelectCategory | ResetSelections;
