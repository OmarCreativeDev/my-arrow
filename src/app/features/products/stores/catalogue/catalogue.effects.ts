import { InventoryService } from '@app/core/inventory/inventory.service';
import { Actions, ofType, Effect } from '@ngrx/effects';
import { CatalogueActionTypes, LoadCatalogue, LoadCatalogueSuccess, LoadCatalogueFailed } from './catalogue.actions';
import { map, switchMap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { IProductCategory } from '@app/core/inventory/product-category.interface';
import { Injectable } from '@angular/core';

@Injectable()
export class CatalogueEffects {
  constructor(private actions$: Actions, private inventoryService: InventoryService) {}

  @Effect()
  LoadCategories$ = this.actions$.pipe(
    ofType(CatalogueActionTypes.LOAD_CATALOGUE),
    switchMap((action: LoadCatalogue) => {
      return this.inventoryService.categories(action.payload).pipe(
        map((data: Array<IProductCategory>) => new LoadCatalogueSuccess(data)),
        catchError(error => of(new LoadCatalogueFailed(error)))
      );
    })
  );
}
