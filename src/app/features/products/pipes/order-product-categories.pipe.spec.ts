import { OrderProductCategoriesPipe } from '@app/features/products/pipes/order-product-categories.pipe';
import { IProductCategory } from '@app/core/inventory/product-category.interface';

const testData: Array<IProductCategory> = [
  {
    'id': 1,
    'name': 'Passives',
    'totalProducts': 790548
  }, {
    'id': 2,
    'name': 'Interconnect',
    'totalProducts': 260356
  }, {
    'id': 3,
    'name': 'Electromechanical',
    'totalProducts': 50076
  }, {
    'id': 4,
    'name': 'Tools',
    'totalProducts': 8809
  }
];

const orderByName: Array<IProductCategory> = [
  {
    'id': 3,
    'name': 'Electromechanical',
    'totalProducts': 50076
  }, {
    'id': 2,
    'name': 'Interconnect',
    'totalProducts': 260356
  }, {
    'id': 1,
    'name': 'Passives',
    'totalProducts': 790548
  }, {
    'id': 4,
    'name': 'Tools',
    'totalProducts': 8809
  }
];

const orderByTotalProducts: Array<IProductCategory> = [
  {
    'id': 4,
    'name': 'Tools',
    'totalProducts': 8809
  }, {
    'id': 3,
    'name': 'Electromechanical',
    'totalProducts': 50076
  }, {
    'id': 2,
    'name': 'Interconnect',
    'totalProducts': 260356
  }, {
    'id': 1,
    'name': 'Passives',
    'totalProducts': 790548
  }
];

const orderBySame: Array<IProductCategory> = [
  {
    'id': 3,
    'name': 'Electromechanical',
    'totalProducts': 50076
  },
  {
    'id': 3,
    'name': 'Electromechanical',
    'totalProducts': 50076
  },
  {
    'id': 3,
    'name': 'Electromechanical',
    'totalProducts': 50076
  }
];

describe('OrderProductCategoriesPipe', () => {
  let pipe: OrderProductCategoriesPipe;

  beforeEach(() => {
    pipe = new OrderProductCategoriesPipe;
  });

  it('order array by `name` property', () => {
    expect(JSON.stringify(pipe.transform(testData, 'name')) === JSON.stringify(orderByName)).toBeTruthy();
  });

  it('order array by `totalProducts` property', () => {
    expect(JSON.stringify(pipe.transform(testData, 'totalProducts')) === JSON.stringify(orderByTotalProducts)).toBeTruthy();
  });

  it('order array by same `name` property', () => {
    expect(JSON.stringify(pipe.transform(orderBySame, 'name')) === JSON.stringify(orderBySame)).toBeTruthy();
  });

});
