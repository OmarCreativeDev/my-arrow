import { Pipe, PipeTransform } from '@angular/core';
import { IProductCategory } from '@app/core/inventory/product-category.interface';

@Pipe({ name: 'orderCategoriesBy' })
export class OrderProductCategoriesPipe implements PipeTransform {
  transform(array: Array<IProductCategory>, prop: string): Array<IProductCategory> {
    array.sort((a: any, b: any) => {
      if (a[prop] < b[prop]) {
        return -1;
      } else if (a[prop] > b[prop]) {
        return 1;
      } else {
        return 0;
      }
    });

    return array;
  }
}
