import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable, of } from 'rxjs';
import { Store } from '@ngrx/store';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { mockProductDetails } from '@app/features/products/components/product-detail/product-detail.component.spec';

import { BuyPartsComponent } from './buy-parts.component';

export class StoreMock {
  public dispatch(): void {}
  public select(): Observable<any> {
    return of([]);
  }
  public pipe() {
    return of({});
  }
}

describe('BuyPartsComponent', () => {
  let component: BuyPartsComponent;
  let fixture: ComponentFixture<BuyPartsComponent>;
  let dialogService: DialogService;

  class MockActivatedRoute {
    queryParams = of({
      cpn: 'cpnFromQueryParam',
      endCustomerSiteId: 456789,
    });
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([])],
      declarations: [BuyPartsComponent],
      providers: [
        { provide: ActivatedRoute, useClass: MockActivatedRoute },
        { provide: DialogService, useClass: DialogServiceMock },
        { provide: Store, useClass: StoreMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyPartsComponent);
    component = fixture.componentInstance;
    component.product = mockProductDetails;
    dialogService = TestBed.get(DialogService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set correct price tiers', () => {
    component.ngOnInit();
    expect(component.priceTiers).toEqual(mockProductDetails.price.priceTier);
  });

  it('should set a single price if price tiers contain one item', () => {
    component.priceTiers = null;
    component.price = null;
    component.product.price.priceTier = [
      {
        minQuantity: 1,
        maxQuantity: 100,
        price: 1.221,
      },
    ];
    component.ngOnInit();
    expect(component.priceTiers).toBeNull();
    expect(component.price).toEqual(1.221);
  });

  it('should not set any pricing info if returned price is null', () => {
    component.priceTiers = null;
    component.price = null;
    component.product.price = null;
    component.ngOnInit();
    expect(component.priceTiers).toBeNull();
    expect(component.price).toBeNull();
  });

  it('should not display End Customer dropdowns if data does not exist', () => {
    component.displayEndCustomerDropdowns = false;
    component.product.endCustomerRecords = [];
    component.ngOnInit();
    expect(component.displayEndCustomerDropdowns).toBeFalsy();
  });

  it('should update cpn and endCustomerId from queryParams', () => {
    expect(component.cpn).toEqual('cpnFromQueryParam');
    expect(component.endCustomerSiteId).toEqual(456789);
  });

  it('should update cpn and endCustomerId onCpnChange()', () => {
    component.onDropdownsSelectionChange({
      cpn: 'SVR-115-0000-000',
      endCustomerSiteId: 172451,
    });
    expect(component.cpn).toBe('SVR-115-0000-000');
    expect(component.endCustomerSiteId).toBe(172451);
  });

  it('should update quantity on onSetQuantity()', () => {
    component.onSetQuantity({ quantity: 10 });
    expect(component.quantity).toEqual(10);
  });

  it('should update price on onPriceChange()', () => {
    component.onPriceChange(10);
    expect(component.price).toEqual(10);
  });

  it('should update priceTiers on onPriceTiersChange()', () => {
    component.onPriceTiersChange([
      {
        maxQuantity: 10,
        minQuantity: 0,
        price: 1.07,
      },
    ]);
    expect(component.priceTiers).toEqual([
      {
        maxQuantity: 10,
        minQuantity: 0,
        price: 1.07,
      },
    ]);
  });

  it('should open the reel dialog', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.openReelModal();
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should add Reel to Cart', () => {
    spyOn(dialogService, 'open').and.callThrough();
    const reelData = {
      itemId: 6691198,
      pricePerItem: 8.79,
      reel: {
        itemsPerReel: 1,
        numberOfReels: 1,
      },
    };
    component.addReelToCart(reelData);
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should call `dialogService.open` when calling `openAddToCartCapDialog`', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.openAddToCartCapDialog({} as any);
    expect(dialogService.open).toHaveBeenCalled();
  });
});
