import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import * as math from 'mathjs';
import * as moment from 'moment';
import { IAddToCartRequestItem } from '@app/core/cart/cart.interfaces';
import { DialogService, Dialog } from '@app/core/dialog/dialog.service';
import { IProductDetails } from '@app/core/inventory/product-details.interface';
import { IProductPriceTiers } from '@app/core/inventory/product-price.interface';
import { getCurrencyCode, getUserBillToAccount } from '@app/core/user/store/user.selectors';
import { IQuantity } from '@app/shared/components/add-quantity-to-cart/add-quantity-to-cart.interface';
import { AddToCartDialogComponent, CartType } from '@app/shared/components/add-to-cart-dialog/add-to-cart-dialog.component';
import { IPricePostData } from '@app/shared/components/end-customer-dropdowns/end-customer-dropdowns.interface';
import { ReelDialogComponent } from '@app/shared/components/reel-dialog/reel-dialog.component';
import { IReelCallbackResponse, IReelDialog } from '@app/shared/components/reel-dialog/reel-dialog.interfaces';
import { IAppState } from '@app/shared/shared.interfaces';
import { getQuoteCartMaxLineItems, getLineItemCountSelector } from '@app/features/quotes/stores/quote-cart.selectors';
import { getCartMaxLineItems, getCartItemsCount } from '@app/features/cart/stores/cart/cart.selectors';
import { AddToCartCapDialogComponent } from '@app/shared/components/add-to-cart-cap-dialog/add-to-cart-cap-dialog.component';
import { IAddToCartCapData } from '@app/shared/components/add-to-cart-cap-dialog/add-to-cart-cap-dialog.interface';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: 'app-buy-parts',
  templateUrl: './buy-parts.component.html',
  styleUrls: ['./buy-parts.component.scss'],
})
export class BuyPartsComponent implements OnInit, OnDestroy {
  @Input()
  product: IProductDetails;

  public cpn: string = null;
  public displayEndCustomerDropdowns: boolean = false;
  public endCustomerSiteId: number = null;
  public price: number = null;
  public priceTiers: Array<IProductPriceTiers> = null;
  public quantity: number;
  public selectForPricing: boolean = false;
  public billToId$: Observable<number>;
  public billToId: number;
  public currencyCode$: Observable<string>;
  public currencyCode: string;
  public quoteCartMaxLineItems$: Observable<number>;
  public quoteCartLineItemsCount$: Observable<number>;
  public cartMaxLineItems$: Observable<number>;
  public cartLineItemsCount$: Observable<number>;
  public cartMaxLineItems: number;
  public cartLineItemsCount: number;
  public purchasable: boolean;
  private capDialog: Dialog<AddToCartCapDialogComponent>;
  public privateFeatureFlags$: Observable<object>;
  public privateFeatureFlags: object;
  private subscription: Subscription = new Subscription();

  public get showTariffLink(): boolean {
    return this.privateFeatureFlags && this.privateFeatureFlags['tariff'];
  }
  /* tslint:disable:no-unused-variable */
  constructor(private activatedRoute: ActivatedRoute, private dialogService: DialogService, private store: Store<IAppState>) {
    this.billToId$ = store.pipe(select(getUserBillToAccount));
    this.currencyCode$ = store.pipe(select(getCurrencyCode));
    this.quoteCartMaxLineItems$ = this.store.pipe(select(getQuoteCartMaxLineItems));
    this.quoteCartLineItemsCount$ = this.store.pipe(select(getLineItemCountSelector));
    this.cartMaxLineItems$ = this.store.pipe(select(getCartMaxLineItems));
    this.cartLineItemsCount$ = this.store.pipe(select(getCartItemsCount));
    this.privateFeatureFlags$ = this.store.pipe(select(getPrivateFeatureFlagsSelector));
  }

  private subscribeBillToId(): Subscription {
    return this.billToId$.subscribe(billToId => {
      this.billToId = billToId;
    });
  }

  private subscribeCurrency(): Subscription {
    return this.currencyCode$.subscribe(currencyCode => {
      this.currencyCode = currencyCode;
    });
  }

  private subscribeMaxLineItems(): Subscription {
    return this.cartMaxLineItems$.subscribe(maxLineItems => (this.cartMaxLineItems = maxLineItems));
  }

  private subscribeLineItemsCount(): Subscription {
    return this.cartLineItemsCount$.subscribe(lineItemsCount => (this.cartLineItemsCount = lineItemsCount));
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeBillToId());
    this.subscription.add(this.subscribeCurrency());
    this.subscription.add(this.subscribeMaxLineItems());
    this.subscription.add(this.subscribeLineItemsCount());
  }

  ngOnInit() {
    this.startSubscriptions();
    // set the pricing tiers (if only one tier set as a single price)
    if (this.product.price !== null) {
      const priceTiers = this.product.price.priceTier;
      if (priceTiers.length > 1) {
        this.priceTiers = priceTiers;
      } else {
        this.price = priceTiers[0].price;
      }
    }

    // determin whether or not to display end customer dropdowns
    if (this.product.endCustomerRecords && this.product.endCustomerRecords.length) {
      this.displayEndCustomerDropdowns = true;
    }

    // read query params and set default cpn
    this.activatedRoute.queryParams.subscribe(params => {
      this.cpn = params.cpn;
      this.endCustomerSiteId = parseInt(params.endCustomerSiteId, 10);
    });

    this.privateFeatureFlags$.subscribe(featureFlags => {
      this.privateFeatureFlags = featureFlags;
    });
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
    if (this.capDialog) {
      this.capDialog.close();
    }
  }

  public onDropdownsSelectionChange(selectedDropdowns: IPricePostData) {
    this.cpn = selectedDropdowns.cpn;
    this.endCustomerSiteId = selectedDropdowns.endCustomerSiteId;
    this.selectForPricing = selectedDropdowns.selectForPricing;

    this.product.selectedCustomerPartNumber = selectedDropdowns.cpn;
    this.product.selectedEndCustomerSiteId = selectedDropdowns.endCustomerSiteId;
  }

  public onSetQuantity($event: IQuantity) {
    this.quantity = $event.quantity;
    this.product.quantity = this.quantity;
  }

  /**
   * When single price is set or changes store on component
   * So it can be passed to sibling component
   * @param {number} price
   */
  public onPriceChange(price: number) {
    this.price = price;
  }

  public onPurchasableChange(purchasable: boolean) {
    this.purchasable = purchasable;
  }

  /**
   * When priceTiers are set or changed store on component
   * So they can be passed to sibling component
   * @param {Array<IProductPriceTiers>} priceTiers
   */
  public onPriceTiersChange(priceTiers: Array<IProductPriceTiers>) {
    this.priceTiers = priceTiers;
  }

  public onTariffChange(showLink: boolean) {
    // Never used reported by lint
    // this._showTariffLink = showLink;
  }

  public openAddToCartCapDialog(data: IAddToCartCapData): void {
    this.capDialog = this.dialogService.open(AddToCartCapDialogComponent, {
      data,
      size: 'small',
    });
  }

  public openReelModal(): void {
    const reelItem: IReelDialog = {
      reelItem: {
        availableQuantity: this.product.availableQuantity,
        billToId: this.billToId,
        cpn: this.product.selectedCustomerPartNumber,
        currency: this.currencyCode,
        endCustomerId: this.product.selectedEndCustomerSiteId,
        endCustomerRecords: this.product.endCustomerRecords,
        itemId: this.product.itemId,
        partNumber: this.product.mpn,
        warehouseId: this.product.warehouseId,
      },
    };

    const dialog = this.dialogService.open(ReelDialogComponent, {
      data: reelItem,
      size: 'x-large',
    });

    /* istanbul ignore next */
    dialog.afterClosed.pipe(take(1)).subscribe((reelData: IReelCallbackResponse) => {
      if (reelData) {
        if (this.cartLineItemsCount + 1 > this.cartMaxLineItems) {
          this.openAddToCartCapDialog({
            cap: this.cartMaxLineItems,
            cartType: CartType.SHOPPING_CART,
          });
        } else {
          this.addReelToCart(reelData);
        }
      }
    });
  }

  public addReelToCart(reelData: IReelCallbackResponse) {
    const { docId, itemId, warehouseId, mpn, description, manufacturer } = this.product;
    const { selectedCustomerPartNumber, selectedEndCustomerSiteId, reel } = reelData;
    const item: IAddToCartRequestItem = {
      manufacturerPartNumber: mpn,
      docId: docId,
      itemId: itemId,
      warehouseId: warehouseId,
      quantity: math.multiply(reel.itemsPerReel, reel.numberOfReels),
      requestDate: moment(new Date()).format('YYYY-MM-DD'),
      selectedCustomerPartNumber,
      selectedEndCustomerSiteId,
      description,
      manufacturer,
      reel: {
        itemsPerReel: reel.itemsPerReel,
        numberOfReels: reel.numberOfReels,
      },
    };

    this.dialogService.open(AddToCartDialogComponent, {
      data: {
        cartType: CartType.SHOPPING_CART,
        items: [item],
      },
      size: 'large',
    });
  }
}
