import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { productSearchAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.product-search';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { mockProduct } from '@app/core/inventory/inventory.service.spec';
import { IProductPriceTiers } from '@app/core/inventory/product-price.interface';
import { IQuantity } from '@app/shared/components/add-quantity-to-cart/add-quantity-to-cart.interface';
import { IPricePostData } from '@app/shared/components/end-customer-dropdowns/end-customer-dropdowns.interface';
import { IProduct } from '@app/shared/shared.interfaces';
import { Store, StoreModule } from '@ngrx/store';
import { Observable, of } from 'rxjs';

import { ProductRowComponent } from './product-row.component';
import { IAddToCartRequestItem } from '@app/core/cart/cart.interfaces';
import moment = require('moment');

export class StoreMock {
  public dispatch(): void {}
  public select(): Observable<any> {
    return of([]);
  }
  public pipe() {
    return of({});
  }
}

describe('ProductRowComponent', () => {
  let component: ProductRowComponent;
  let fixture: ComponentFixture<ProductRowComponent>;
  let dialogService: DialogService;
  let store: Store<any>;

  const productMock: IProduct = {
    docId: 'xxxx_xxxx',
    itemId: 1,
    mpn: 'JAYZ',
    manufacturer: 'orange ltd',
    description: 'microphone',
    image: 'data:image/gif;base64,R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs=',
    itemType: 'COMPONENTS',
    compliance: { EU_ROHS: 'EU_ROHS_RHC' },
    warehouseId: 145,
    quotable: false,
    arrowReel: true,
    minimumOrderQuantity: 100,
    availableQuantity: 10,
    bufferQuantity: 200,
    leadTime: 1,
    customerPartNumber: 'LOB1',
    price: 22.99,
    pipeline: {
      deliveryDate: '03/06/2018',
      quantity: 7000,
    },
    spq: 578,
    multipleOrderQuantity: 75,
    id: '1234_12345678',
    specs: [
      {
        name: 'EU RoHS Compliant',
        value: 'ON OFFF',
      },
    ],
  };

  const mockReel = {
    itemsPerReel: 50,
    numberOfReels: 5,
  };

  const mockReelData = {
    cartLineId: 'abc',
    itemId: 123,
    pricePerItem: 123,
    reel: mockReel,
    selectedCustomerPartNumber: 'abc',
    selectedEndCustomerSiteId: 123,
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({ productSearch: productSearchAnalyticsMetaReducers }), RouterTestingModule],
      declarations: [ProductRowComponent],
      providers: [{ provide: DialogService, useClass: DialogServiceMock }, { provide: Store, useClass: StoreMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductRowComponent);
    component = fixture.componentInstance;
    component.product = productMock;
    dialogService = TestBed.get(DialogService);
    this.store = TestBed.get(Store);
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('`product` is initially defined', () => {
    expect(component.product).toBeDefined();
  });

  it('should update the quantity', () => {
    const event: IQuantity = {
      quantity: 10,
    };
    component.onSetQuantity(event);
    expect(component.quantity).toBe(10);
  });

  it('should update the product price', () => {
    component.onPriceChange(42, mockProduct);
    expect(mockProduct.price).toBe(42);
  });

  it('should update the product priceTiers', () => {
    const priceTiers: Array<IProductPriceTiers> = [
      { maxQuantity: 1, minQuantity: 2, price: 3 },
      { maxQuantity: 4, minQuantity: 5, price: 6 },
      { maxQuantity: 13, minQuantity: 1, price: 6 },
      { maxQuantity: 13, minQuantity: 9, price: 86 },
    ];

    component.onPriceTiersChange(priceTiers, mockProduct);
    expect(mockProduct.priceTiers).toBe(priceTiers);
  });

  it('`cpn` is initially set as null', () => {
    expect(component.productDetailsQueryParams.cpn).toBeNull();
  });

  it('`endCustomerSiteId` is initially set as null', () => {
    expect(component.productDetailsQueryParams.endCustomerSiteId).toBeNull();
  });

  it('`onBufferQuantityChange()` updates `bufferQuantity` on the product record', () => {
    component.onBufferQuantityChange(5555, productMock);
    expect(productMock.bufferQuantity).toBe(5555);
  });

  it('should update query params productDetailsQueryParams()', () => {
    const mockProductDetailsQueryParams: IPricePostData = { cpn: '1111-111', endCustomerSiteId: 1111 };
    component.onDropdownsSelectionChange(mockProductDetailsQueryParams);

    expect(component.productDetailsQueryParams.cpn).toEqual(mockProductDetailsQueryParams.cpn);
    expect(component.productDetailsQueryParams.endCustomerSiteId).toEqual(mockProductDetailsQueryParams.endCustomerSiteId);
  });

  it('should open the reel modal', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.openReelModal();
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should call `dialogService.open` when calling `openAddToCartCapDialog`', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.openAddToCartCapDialog({} as any);
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('`createAddToCartRequestItem` should create a new `IAddToCartRequestItem` when calling to', () => {
    const { docId, itemId, warehouseId, mpn, manufacturer, description } = mockProduct;
    const { selectedCustomerPartNumber, selectedEndCustomerSiteId, reel } = mockReelData;

    const mockAddToCartRequest: IAddToCartRequestItem = {
      manufacturerPartNumber: mpn,
      docId: docId,
      itemId: itemId,
      warehouseId: warehouseId,
      quantity: 250,
      requestDate: moment(new Date()).format('YYYY-MM-DD'),
      selectedCustomerPartNumber,
      selectedEndCustomerSiteId,
      manufacturer,
      description,
      reel: {
        itemsPerReel: reel.itemsPerReel,
        numberOfReels: reel.numberOfReels,
      },
    };

    const resultItem = component.createAddToCartRequestItem(mockProduct, mockReelData);
    expect(mockAddToCartRequest).toEqual(resultItem);
  });
});
