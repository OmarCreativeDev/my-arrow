import { ChangeDetectorRef, Component, HostBinding, Input, OnInit, OnDestroy } from '@angular/core';
import { IAddToCartRequestItem } from '@app/core/cart/cart.interfaces';
import { DialogService, Dialog } from '@app/core/dialog/dialog.service';
import { IProductPriceTiers } from '@app/core/inventory/product-price.interface';
import { ISearchListing } from '@app/core/inventory/product-search.interface';
import { ProductClicked, ToggleSelected } from '@app/features/products/stores/product-search/product-search.actions';
import { getSelectedIds } from '@app/features/products/stores/product-search/product-search.selectors';
import { IQuantity } from '@app/shared/components/add-quantity-to-cart/add-quantity-to-cart.interface';
import { AddToCartDialogComponent, CartType } from '@app/shared/components/add-to-cart-dialog/add-to-cart-dialog.component';
import { IPricePostData } from '@app/shared/components/end-customer-dropdowns/end-customer-dropdowns.interface';
import { ReelDialogComponent } from '@app/shared/components/reel-dialog/reel-dialog.component';
import { IReelCallbackResponse, IReelDialog } from '@app/shared/components/reel-dialog/reel-dialog.interfaces';
import { IAppState, IProduct } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import * as math from 'mathjs';
import * as moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { getQuoteCartMaxLineItems, getLineItemCountSelector } from '@app/features/quotes/stores/quote-cart.selectors';
import { getCartMaxLineItems, getCartItemsCount } from '@app/features/cart/stores/cart/cart.selectors';
import { AddToCartCapDialogComponent } from '@app/shared/components/add-to-cart-cap-dialog/add-to-cart-cap-dialog.component';
import { IAddToCartCapData } from '@app/shared/components/add-to-cart-cap-dialog/add-to-cart-cap-dialog.interface';
import { take } from 'rxjs/operators';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: '[app-product-row]',
  templateUrl: './product-row.component.html',
  styleUrls: ['./product-row.component.scss'],
})
export class ProductRowComponent implements OnInit, OnDestroy {
  @Input()
  public product: ISearchListing;
  @Input()
  public searchQuery: string = null;
  @Input()
  public billToId: number;
  @Input()
  public currencyCode: string;
  @Input()
  public index: number;

  @HostBinding('class.products-listing__selected-row')
  select: boolean = false;

  public productDetailsQueryParams: IPricePostData = {
    cpn: null,
    endCustomerSiteId: null,
  };
  public quantity: number = 0;
  public selectedIds$: Observable<Array<string>>;
  public quoteCartMaxLineItems$: Observable<number>;
  public quoteCartLineItemsCount$: Observable<number>;
  public cartLineItemsCount$: Observable<number>;
  public cartMaxLineItems$: Observable<number>;
  public cartLineItemsCount: number;
  public cartMaxLineItems: number;
  public privateFeatureFlags: any;
  public privateFeatureFlags$: Observable<any>;
  private capDialog: Dialog<AddToCartCapDialogComponent>;
  private subscription: Subscription = new Subscription();

  constructor(private dialogService: DialogService, private cdr: ChangeDetectorRef, private store: Store<IAppState>) {
    this.getStoreSlices();
  }

  public ngOnInit() {
    // solves ExpressionChangedAfterItHasBeenCheckedError
    this.cdr.detectChanges();
    this.startSubscriptions();
  }

  private startSubscriptions() {
    this.subscription.add(this.getFeatureFlags());
    this.subscription.add(this.getCartLineItemsCount());
    this.subscription.add(this.getCartMaxLineItems());
  }

  public ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
    if (this.capDialog) {
      this.capDialog.close();
    }
  }

  private getFeatureFlags(): Subscription {
    return this.privateFeatureFlags$.pipe(take(1)).subscribe(featureFlags => {
      this.privateFeatureFlags = featureFlags;
    });
  }

  private getCartLineItemsCount(): Subscription {
    return this.cartLineItemsCount$.subscribe(lineItemsCount => (this.cartLineItemsCount = lineItemsCount));
  }

  private getCartMaxLineItems(): Subscription {
    return this.cartMaxLineItems$.subscribe(maxLineItems => (this.cartMaxLineItems = maxLineItems));
  }

  public getStoreSlices(): void {
    this.selectedIds$ = this.store.pipe(select(getSelectedIds));
    this.quoteCartMaxLineItems$ = this.store.pipe(select(getQuoteCartMaxLineItems));
    this.quoteCartLineItemsCount$ = this.store.pipe(select(getLineItemCountSelector));
    this.cartMaxLineItems$ = this.store.pipe(select(getCartMaxLineItems));
    this.cartLineItemsCount$ = this.store.pipe(select(getCartItemsCount));
    this.privateFeatureFlags$ = this.store.pipe(select(getPrivateFeatureFlagsSelector));
  }

  public isSelected(product: IProduct, selectedIds: Array<string>): boolean {
    return selectedIds.includes(product.id);
  }

  public toggleSelected(isSelected: boolean): void {
    this.select = isSelected;
    this.store.dispatch(new ToggleSelected([this.product.id], isSelected));
  }

  public onSetQuantity($event: IQuantity): void {
    this.quantity = $event.quantity;
    this.product.quantity = this.quantity;
  }

  public onBufferQuantityChange(bufferQuantity: number, record: IProduct): void {
    record.bufferQuantity = bufferQuantity;
  }

  /**
   * Upates productDetailsQueryParams when any value changes on the end customer dropdowns
   * @param {IPricePostData} selectedDropdowns
   */
  public onDropdownsSelectionChange(selectedDropdowns: IPricePostData) {
    this.productDetailsQueryParams.cpn = selectedDropdowns.cpn;
    this.productDetailsQueryParams.endCustomerSiteId = selectedDropdowns.endCustomerSiteId;
    this.productDetailsQueryParams.selectForPricing = selectedDropdowns.selectForPricing;

    this.product.selectedCustomerPartNumber = selectedDropdowns.cpn;
    this.product.selectedEndCustomerSiteId = selectedDropdowns.endCustomerSiteId;
  }

  /**
   * When single price is set or changes set to the record
   * So it can be passed to sibling component
   * @param {number} price
   * @param {IProduct} record
   */
  public onPriceChange(price: number, record: IProduct) {
    record.price = price;
  }

  /**
   * When priceTiers are set or changed set to the record
   * So it can be passed to sibling component
   * @param {Array<IProductPriceTiers>} priceTiers
   * @param {IProduct} record
   */
  public onPriceTiersChange(priceTiers: Array<IProductPriceTiers>, record: IProduct) {
    record.priceTiers = priceTiers;
  }

  public updatePurchasableFlag(purchasable: boolean, record: IProduct) {
    record.quotable = !purchasable;
  }

  public openAddToCartCapDialog(data: IAddToCartCapData): void {
    this.capDialog = this.dialogService.open(AddToCartCapDialogComponent, {
      data,
      size: 'small',
    });
  }

  public openReelModal(): void {
    const reelItem: IReelDialog = {
      reelItem: {
        availableQuantity: this.product.availableQuantity,
        billToId: this.billToId,
        businessCost: this.product.businessCost,
        cpn: this.product.selectedCustomerPartNumber,
        currency: this.currencyCode,
        endCustomerId: this.product.selectedEndCustomerSiteId,
        endCustomerRecords: this.product.endCustomerRecords,
        itemId: this.product.itemId,
        partNumber: this.product.mpn,
        warehouseId: this.product.warehouseId,
      },
    };

    const dialog = this.dialogService.open(ReelDialogComponent, {
      data: reelItem,
      size: 'x-large',
    });

    dialog.afterClosed.pipe(take(1)).subscribe((reelData: IReelCallbackResponse) => {
      if (reelData) {
        if (this.cartLineItemsCount + 1 > this.cartMaxLineItems) {
          this.openAddToCartCapDialog({
            cap: this.cartMaxLineItems,
            cartType: CartType.SHOPPING_CART,
          });
        } else {
          const item = this.createAddToCartRequestItem(this.product, reelData);

          this.dialogService.open(AddToCartDialogComponent, {
            data: {
              cartType: CartType.SHOPPING_CART,
              items: [item],
            },
            size: 'large',
          });
        }
      }
    });
  }

  public createAddToCartRequestItem(product: ISearchListing, reelData: IReelCallbackResponse): IAddToCartRequestItem {
    const { docId, itemId, warehouseId, mpn, manufacturer, description } = product;
    const { selectedCustomerPartNumber, selectedEndCustomerSiteId, reel } = reelData;

    const item: IAddToCartRequestItem = {
      manufacturerPartNumber: mpn,
      docId: docId,
      itemId: itemId,
      warehouseId: warehouseId,
      quantity: math.multiply(reel.itemsPerReel, reel.numberOfReels),
      requestDate: moment(new Date()).format('YYYY-MM-DD'),
      selectedCustomerPartNumber,
      selectedEndCustomerSiteId,
      manufacturer,
      description,
      reel: {
        itemsPerReel: reel.itemsPerReel,
        numberOfReels: reel.numberOfReels,
      },
    };

    return item;
  }

  public productClicked() {
    this.store.dispatch(new ProductClicked({ product: this.product, index: this.index }));
  }
}
