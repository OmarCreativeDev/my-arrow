import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IProductSearch } from '@app/core/inventory/product-search.interface';
import { Observable, of } from 'rxjs';
import { ProductRowComponent } from '@app/features/products/components/product-search-listing/product-row/product-row.component';
import { ProductSearchListingComponent } from './product-search-listing.component';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { ToggleSelected, ToggleQuoteTooltip } from '@app/features/products/stores/product-search/product-search.actions';

export const searchResultsMock: IProductSearch = {
  productsTotal: 2,
  products: [
    {
      docId: 'xxxx_xxxx',
      itemId: 1,
      mpn: 'JAYZ',
      manufacturer: 'orange ltd',
      description: 'microphone',
      image: '/product-image.png',
      itemType: 'COMPONENTS',
      compliance: {},
      warehouseId: 145,
      quotable: false,
      arrowReel: true,
      minimumOrderQuantity: 100,
      availableQuantity: 10,
      bufferQuantity: 200,
      leadTime: 1,
      customerPartNumber: 'LOB1',
      price: 22.99,
      pipeline: {
        deliveryDate: '03/06/2018',
        quantity: 7000,
      },
      spq: 578,
      multipleOrderQuantity: 75,
      id: '1234_12345678',
      specs: [
        {
          name: 'EU RoHS Compliant',
          value: 'ON OFFF',
        },
      ],
    },
    {
      docId: 'xxxx_xxxx',
      itemId: 2,
      mpn: 'OMG',
      manufacturer: 'some fancy place',
      description: 'something important',
      image: '/product-image.png',
      itemType: 'COMPONENTS',
      compliance: {},
      warehouseId: 17,
      quotable: false,
      arrowReel: true,
      minimumOrderQuantity: 20,
      availableQuantity: 1000,
      bufferQuantity: 600,
      leadTime: 99,
      customerPartNumber: 'OPPG',
      datasheet: 'http://unec.edu.az/application/uploads/2014/12/pdf-sample.pdf',
      price: 3.99,
      spq: 514,
      multipleOrderQuantity: 92,
      id: '1234_12345679',
      specs: [
        {
          name: 'EU RoHS Compliant',
          value: 'ON OFFF',
        },
      ],
    },
  ],
  categories: [
    {
      name: 'test category',
      secondaryCategories: [
        {
          name: 'secondary category',
          tertiaryCategories: [
            {
              name: 'tertiary category',
              productsTotal: 333,
              id: '^2015/13290/2023',
            },
          ],
        },
      ],
    },
  ],
};

export class StoreMock {
  public dispatch(): void {}
  public select(): Observable<number> {
    return of(10);
  }
  public pipe() {
    return of({});
  }
}

describe('ProductSearchListingComponent', () => {
  let component: ProductSearchListingComponent;
  let fixture: ComponentFixture<ProductSearchListingComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ProductSearchListingComponent, ProductRowComponent],
      providers: [{ provide: Store, useClass: StoreMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSearchListingComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('`sortDirections` are defined', () => {
    expect(component.sortDirections.length).toBe(2);
    expect(component.sortDirections[0]).toBe('asc');
    expect(component.sortDirections[1]).toBe('desc');
  });

  it('`sortFields` are defined', () => {
    expect(component.sortFields.length).toBe(2);
    expect(component.sortFields[0]).toBe('mpn');
    expect(component.sortFields[1]).toBe('manufacturer');
  });

  it('`toggleSelected()` adds the selected ids to the store when `$event` is true', () => {
    component.searchResults = searchResultsMock['products'];
    component.toggleSelected(component.searchResults, true);

    expect(store.dispatch).toHaveBeenCalledWith(new ToggleSelected(['1234_12345678', '1234_12345679'], true));
  });

  it('`toggleSelected()` removes the selected ids from the store and dispatches `ToggleQuoteTooltip()` when `$event` is false', () => {
    component.searchResults = searchResultsMock['products'];
    component.toggleSelected(component.searchResults, false);

    expect(store.dispatch).toHaveBeenCalledWith(new ToggleSelected(['1234_12345678', '1234_12345679'], false));
    expect(store.dispatch).toHaveBeenCalledWith(new ToggleQuoteTooltip(0));
  });

  it('`sortField` is set, `setSortDirection(true)` is called when `setSortField()` is invoked with parameter', () => {
    spyOn(component, 'setSortDirection');
    component.setSortField('mpn');

    expect(component.sortField).toBe('mpn');
    expect(component.setSortDirection).toHaveBeenCalled();
  });

  it('`sortField` is not set, `setSortDirection()` is called when `setSortField()` is invoked with a parameter', () => {
    spyOn(component, 'setSortDirection');
    component.sortField = 'manufacturer';
    component.setSortField('manufacturer');

    expect(component.setSortDirection).toHaveBeenCalled();
  });

  it('`resetSortDirection()` is called when `setSortDirection()` is invoked with a parameter', () => {
    spyOn(component, 'resetSortDirection');
    component.setSortDirection(true);

    expect(component.resetSortDirection).toHaveBeenCalled();
  });

  it('`toggleSortDirection()` is called when `setSortDirection()` is invoked without a parameter', () => {
    spyOn(component, 'toggleSortDirection');
    component.setSortDirection();

    expect(component.toggleSortDirection).toHaveBeenCalled();
  });

  it('`resetSortDirection()` sets sortDirection to `asc` and invokes `emitSortingChange()`', () => {
    spyOn(component, 'emitSortingChange');
    component.resetSortDirection();

    expect(component.sortDirection).toBe('asc');
    expect(component.emitSortingChange).toHaveBeenCalled();
  });

  it('`toggleSortDirection()` sets `sortDirection` to `asc` if it was previously `desc`', () => {
    component.sortDirection = 'desc';
    component.toggleSortDirection();

    expect(component.sortDirection).toBe('asc');
  });

  it('`toggleSortDirection()` sets `sortDirection` to `desc` if it was previously `asc`', () => {
    component.sortDirection = 'asc';
    component.toggleSortDirection();

    expect(component.sortDirection).toBe('desc');
  });

  it('`emitSortingChange()` is called when `toggleSortDirection()` is invoked', () => {
    spyOn(component, 'emitSortingChange');
    component.toggleSortDirection();

    expect(component.emitSortingChange).toHaveBeenCalled();
  });

  it('`sortingChange.emit()` is called when `emitSortingChange()` is invoked', () => {
    spyOn(component.sortingChange, 'emit');
    component.emitSortingChange();

    expect(component.sortingChange.emit).toHaveBeenCalled();
  });
});
