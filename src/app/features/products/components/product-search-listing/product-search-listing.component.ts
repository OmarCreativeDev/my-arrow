import { Component, EventEmitter, Input, Output, OnInit, OnDestroy } from '@angular/core';
import { ISortProductSearch } from '@app/core/inventory/product-search.interface';
import {
  getSortOptions,
  getSelectedIds,
  getSelectedProducts,
  getAllSelected,
} from '@app/features/products/stores/product-search/product-search.selectors';
import { getCurrencyCode, getUserBillToAccount } from '@app/core/user/store/user.selectors';
import { IProduct, IAppState } from '@app/shared/shared.interfaces';
import { Observable, Subscription } from 'rxjs';
import { SetSortOptions, ToggleSelected, ToggleQuoteTooltip } from '@app/features/products/stores/product-search/product-search.actions';
import { Store, select } from '@ngrx/store';

@Component({
  selector: 'app-product-search-listing',
  templateUrl: './product-search-listing.component.html',
  styleUrls: ['./product-search-listing.component.scss'],
})
export class ProductSearchListingComponent implements OnInit, OnDestroy {
  @Input() public loading: boolean = null;
  @Input() public searchQuery: string = null;
  @Input() public searchResults: Array<IProduct> = null;
  @Output() public sortingChange = new EventEmitter<boolean>();

  public cpn = null;
  public endCustomerSiteId = null;
  public selectedIds$: Observable<Array<string>>;
  public allSelected$: Observable<boolean>;
  public selectedProducts$: Observable<Array<IProduct>>;
  public selectedProducts: Array<IProduct>;
  public sortDirection: string;
  public sortDirections: Array<string> = ['asc', 'desc'];
  public sortField: string;
  public sortFields: Array<string> = ['mpn', 'manufacturer'];
  public sortOptions$: Observable<ISortProductSearch>;
  public currencyCode$: Observable<string>;
  public billToId$: Observable<number>;
  private subscription: Subscription = new Subscription();

  constructor(private store: Store<IAppState>) {
    this.sortOptions$ = store.pipe(select(getSortOptions));
    this.currencyCode$ = store.pipe(select(getCurrencyCode));
    this.billToId$ = store.pipe(select(getUserBillToAccount));
    this.selectedIds$ = store.pipe(select(getSelectedIds));
    this.selectedProducts$ = store.pipe(select(getSelectedProducts));
    this.allSelected$ = store.pipe(select(getAllSelected));

    this.sortOptions$.subscribe(sortOptions => {
      /* istanbul ignore else */
      if (sortOptions) {
        this.sortDirection = sortOptions.sortDirection;
        this.sortField = sortOptions.sortField;
      }
    });
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  startSubscriptions(): any {
    this.subscription.add(this.subscribeSelectedProducts());
  }

  private subscribeSelectedProducts(): Subscription {
    return this.selectedProducts$.subscribe(selectedProducts => {
      this.selectedProducts = selectedProducts;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public toggleSelected(searchResults: Array<IProduct>, isSelected: boolean): void {
    const searchResultIds = searchResults.map(product => product.id);

    this.store.dispatch(new ToggleSelected(searchResultIds, isSelected));
    /* istanbul ignore else */
    if (!isSelected) {
      this.store.dispatch(new ToggleQuoteTooltip(0));
    }
  }

  public setSortField(sortField: string): void {
    if (!this.sortField || this.sortField !== sortField) {
      this.sortField = sortField;
      this.setSortDirection(true);
    } else {
      this.setSortDirection();
    }
  }

  public setSortDirection(sortFieldChanged?: boolean): void {
    if (sortFieldChanged) {
      this.resetSortDirection();
    } else {
      this.toggleSortDirection();
    }
  }

  public resetSortDirection(): void {
    this.sortDirection = this.sortDirections[0];
    this.emitSortingChange();
  }

  public toggleSortDirection(): void {
    if (this.sortDirection === this.sortDirections[0]) {
      this.sortDirection = this.sortDirections[1];
    } else {
      this.sortDirection = this.sortDirections[0];
    }

    this.emitSortingChange();
  }

  public emitSortingChange(): void {
    this.store.dispatch(
      new SetSortOptions({
        sortField: this.sortField,
        sortDirection: this.sortDirection,
      })
    );

    this.sortingChange.emit(true);
  }
}
