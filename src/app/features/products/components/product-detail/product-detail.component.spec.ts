import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { IProductDetails } from '@app/core/inventory/product-details.interface';

import { ProductDetailComponent } from './product-detail.component';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';

export const mockProductDetails: IProductDetails = {
  arrowReel: true,
  availableQuantity: 0,
  bufferQuantity: 0,
  category: 'Product Category',
  compliance: { EU_ROHS: 'EU_ROHS_RHC' },
  customerPartNumber: 'string',
  datasheet: 'http://www.sdgsgd.com/file.pdf',
  description: 'Diode Switching 70V 0.215A 3-Pin SOT-23',
  docId: '1313_123',
  id: '1234_12345678',
  ncnr: false,
  endCustomerRecords: [
    {
      cpn: 'SVR-115-0000-000',
      endCustomers: [{ endCustomerSiteId: 480510, name: 'Siemens Industry Inc' }, { endCustomerSiteId: 172451, name: 'Google' }],
    },
    {
      cpn: 'SVR-115-0000-006',
      endCustomers: [{ endCustomerSiteId: 480510, name: 'Apple' }, { endCustomerSiteId: 480510, name: 'Samsung' }],
    },
  ],
  image: 'data:image/gif;base64,R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs=',
  itemId: 1,
  itemType: 'COMPONENTS',
  leadTime: 'string',
  manufacturer: 'On Semiconductors',
  minimumOrderQuantity: 0,
  mpn: 'BAV99',
  multipleOrderQuantity: 0,
  pipeline: {
    deliveryDate: 'string',
    quantity: 0,
  },
  price: {
    priceTier: [
      {
        minQuantity: 1,
        maxQuantity: 100,
        price: 1.221,
      },
      {
        minQuantity: 101,
        maxQuantity: 500,
        price: 2.221,
      },
    ],
  },
  purchasable: true,
  specs: [
    {
      name: 'EU RoHS Compliant',
      value: 'ON OFF',
    },
  ],
  spq: 1.5,
  warehouseId: 0,
};

class StoreMock {
  public dispatch(): void {}
  public select(): Observable<any> {
    return of(10);
  }
  public pipe() {
    return of({});
  }
}

describe('ProductDetailComponent', () => {
  let component: ProductDetailComponent;
  let fixture: ComponentFixture<ProductDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ProductDetailComponent],
      providers: [{ provide: DialogService, useClass: DialogServiceMock }, { provide: Store, useClass: StoreMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDetailComponent);
    component = fixture.componentInstance;
    component.product = mockProductDetails;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call `openAddToCartCapDialog` when addToQuoteCart and max cap has been reached', () => {
    spyOn(component, 'openAddToCartCapDialog').and.callThrough();
    component.quoteCartRemainingLineItems = 0;
    component.addToQuoteCart();
    expect(component.openAddToCartCapDialog).toHaveBeenCalled();
  });
});
