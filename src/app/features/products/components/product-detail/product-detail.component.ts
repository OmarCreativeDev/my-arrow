import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { IProductDetails } from '@app/core/inventory/product-details.interface';
import { IAddToQuoteCartRequestItem } from '@app/core/quote-cart/quote-cart.interfaces';
import { AddToCartDialogComponent, CartType } from '@app/shared/components/add-to-cart-dialog/add-to-cart-dialog.component';
import { DialogService, Dialog } from '@app/core/dialog/dialog.service';
import { IAppState } from '@app/shared/shared.interfaces';
import { QuoteCartLimit } from '@app/features/quotes/stores/quote-cart.actions';
import { IAddToBomPart } from '@app/core/boms/boms.interface';
import { OpenAddToBomModal } from '@app/core/boms/store/boms.actions';
import { Router } from '@angular/router';
import { AddToCartCapDialogComponent } from '@app/shared/components/add-to-cart-cap-dialog/add-to-cart-cap-dialog.component';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit, OnDestroy {
  @Input()
  product: IProductDetails;
  @Input()
  quoteCartMaxLineItems: number;
  @Input()
  quoteCartRemainingLineItems: number;

  private capDialog: Dialog<AddToCartCapDialogComponent>;
  public privateFeatureFlags$: Observable<object>;

  constructor(private router: Router, private dialogService: DialogService, private store: Store<IAppState>) {}

  public ngOnInit() {
    this.privateFeatureFlags$ = this.store.pipe(select(getPrivateFeatureFlagsSelector));
  }

  public ngOnDestroy(): void {
    if (this.capDialog) {
      this.capDialog.close();
    }
  }

  public openAddToCartCapDialog(cap: number, cartType: CartType): void {
    this.capDialog = this.dialogService.open(AddToCartCapDialogComponent, {
      data: {
        cap,
        cartType,
      },
      size: 'small',
    });
  }

  public addToQuoteCart() {
    if (this.quoteCartRemainingLineItems === 0) {
      const requestedItemsCount = this.quoteCartMaxLineItems + 1;
      this.openAddToCartCapDialog(this.quoteCartMaxLineItems, CartType.QUOTE_CART);
      this.store.dispatch(new QuoteCartLimit(requestedItemsCount));
    } else {
      const {
        docId,
        itemId,
        warehouseId,
        quantity,
        selectedCustomerPartNumber,
        selectedEndCustomerSiteId,
        mpn: manufacturerPartNumber,
      } = this.product;

      const items: Array<IAddToQuoteCartRequestItem> = [
        {
          docId,
          itemId,
          warehouseId,
          quantity: quantity ? quantity : 1,
          manufacturerPartNumber,
          ...(selectedCustomerPartNumber && { selectedCustomerPartNumber: selectedCustomerPartNumber }),
          ...(selectedEndCustomerSiteId && { selectedEndCustomerSiteId: selectedEndCustomerSiteId }),
        },
      ];
      this.dialogService.open(AddToCartDialogComponent, {
        data: {
          cartType: CartType.QUOTE_CART,
          items,
        },
        size: 'large',
      });
    }
  }

  public addToBom() {
    const partToAdd: IAddToBomPart = {
      manufacturerPartNumber: this.product.mpn,
      manufacturer: this.product.manufacturer,
      docId: this.product.docId,
      itemId: this.product.itemId,
      warehouseId: this.product.warehouseId,
      quantity: this.product.quantity || 1,
    };

    this.store.dispatch(new OpenAddToBomModal({ url: this.router.url, parts: [partToAdd] }));
  }
}
