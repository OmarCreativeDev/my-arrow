import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { IAppState } from '@app/shared/shared.interfaces';
import { IProductAdditionalFilter, IProductFilter, IProductPrimaryCategory } from '@app/core/inventory/product-search.interface';
import { Store, select } from '@ngrx/store';
import {
  getAppliedCategory,
  getAppliedFilters,
  getAppliedManufacturers,
  getSelectedFilters,
  getSelectedManufacturers,
} from '@app/features/products/stores/product-search/product-search.selectors';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-product-search-filters',
  templateUrl: './product-search-filters.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProductSearchFiltersComponent {
  @Input() public categories: Array<IProductPrimaryCategory> = [];
  @Input() public filters: Array<IProductFilter> = [];
  @Input() public manufacturers: Array<IProductAdditionalFilter> = [];

  public appliedCategory$: Observable<string>;
  public appliedFilters$: Observable<Array<IProductFilter>>;
  public appliedManufacturers$: Observable<Array<IProductAdditionalFilter>>;
  public selectedFilters$: Observable<Array<string>>;
  public selectedManufacturers$: Observable<Array<IProductAdditionalFilter>>;

  constructor(private store: Store<IAppState>) {
    this.getStoreSlices();
  }

  public getStoreSlices(): void {
    this.appliedCategory$ = this.store.pipe(select(getAppliedCategory));
    this.appliedFilters$ = this.store.pipe(select(getAppliedFilters));
    this.appliedManufacturers$ = this.store.pipe(select(getAppliedManufacturers));
    this.selectedFilters$ = this.store.pipe(select(getSelectedFilters));
    this.selectedManufacturers$ = this.store.pipe(select(getSelectedManufacturers));
  }
}
