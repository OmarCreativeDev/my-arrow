import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ProductFiltersActionsComponent } from './product-filters-actions.component';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';

export class StoreMock {
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

describe('ProductFiltersActionsComponent', () => {
  let component: ProductFiltersActionsComponent;
  let fixture: ComponentFixture<ProductFiltersActionsComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductFiltersActionsComponent],
      providers: [{ provide: Store, useClass: StoreMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductFiltersActionsComponent);
    store = TestBed.get(Store);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('`clearAllFilters()` dispatches two events to the store', () => {
    spyOn(store, 'dispatch');
    component.clearAllFilters();
    expect(store.dispatch).toHaveBeenCalledTimes(2);
  });

  it('`hasFiltersSelected()` returns true if no product filters & manufacturers are selected and applied', () => {
    component.appliedManufacturers = [];
    component.appliedFilters = [];
    component.selectedManufacturers = [];
    component.selectedFilters = [];
    expect(component.hasFiltersSelected()).toBeTruthy();
  });

  it('`hasFiltersSelected()` returns false if product filters or manufacturers are selected or applied', () => {
    component.appliedManufacturers = [];
    component.appliedFilters = [];
    component.selectedManufacturers = [{ name: 'SAMSUNG DIODES', id: 'some|funky|String', productsTotal: 321 }];
    component.selectedFilters = [];
    expect(component.hasFiltersSelected()).toBeFalsy();
  });

  it('`applyFilters()` dispatches an event to the store', () => {
    spyOn(store, 'dispatch');
    component.applyFilters();
    expect(store.dispatch).toHaveBeenCalled();
  });
});
