import { Component, Input } from '@angular/core';
import { IAppState } from '@app/shared/shared.interfaces';
import { IAppliedFilters, IProductAdditionalFilter, IProductFilter } from '@app/core/inventory/product-search.interface';
import { Store } from '@ngrx/store';
import { SetCategory, SubmitFilters } from '@app/features/products/stores/product-search/product-search.actions';

@Component({
  selector: 'app-product-filters-actions',
  templateUrl: './product-filters-actions.component.html',
  styleUrls: ['./product-filters-actions.component.scss'],
})
export class ProductFiltersActionsComponent {
  @Input() public appliedCategory: string;
  @Input() public appliedFilters: Array<IProductFilter> = [];
  @Input() public appliedManufacturers: Array<IProductAdditionalFilter> = [];
  @Input() public selectedFilters: Array<string> = [];
  @Input() public selectedManufacturers: Array<IProductAdditionalFilter> = [];

  constructor(public store: Store<IAppState>) {}

  public clearAllFilters(): void {
    this.store.dispatch(new SetCategory(''));
    this.store.dispatch(new SubmitFilters({ clearSelectedFilters: true }));
  }

  public hasFiltersSelected(): boolean {
    if (
      !this.selectedFilters.length &&
      !this.appliedFilters.length &&
      !this.selectedManufacturers.length &&
      !this.appliedManufacturers.length
    ) {
      return true;
    } else {
      return false;
    }
  }

  public applyFilters(): void {
    const filters: IAppliedFilters = {};

    if (this.appliedCategory) {
      filters.appliedCategory = this.appliedCategory;
    }

    if (this.selectedFilters.length) {
      filters.appliedFilters = [];

      // todo - remove once BE can expect filters and manufacturers as array of strings instead of objects
      this.selectedFilters.forEach(item => {
        filters.appliedFilters.push({
          name: item,
        });
      });
    }

    if (this.selectedManufacturers.length) {
      filters.appliedManufacturers = this.selectedManufacturers;
    }

    this.store.dispatch(new SubmitFilters(filters));
  }
}
