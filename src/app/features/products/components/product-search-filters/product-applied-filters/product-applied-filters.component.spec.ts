import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { InventoryService } from '@app/core/inventory/inventory.service';
import { IProductFilter, IProductAdditionalFilter } from '@app/core/inventory/product-search.interface';
import { Observable, of } from 'rxjs';
import { ProductAppliedFiltersComponent } from './product-applied-filters.component';
import { Store } from '@ngrx/store';

export class StoreMock {
  public dispatch(): void {}
  public select(): Observable<any> {
    return of([]);
  }
  public pipe() {
    return of({});
  }
}

export class InventoryServiceMock {
  public localiseEnum(): void {}
}

describe('ProductAppliedFiltersComponent', () => {
  let component: ProductAppliedFiltersComponent;
  let fixture: ComponentFixture<ProductAppliedFiltersComponent>;
  let store: Store<any>;
  let inventoryService: InventoryService;
  const appliedFilters: Array<IProductFilter> = [{ name: 'EU_RHC' }, { name: 'IN_STOCK' }];
  const appliedManufacturers: Array<IProductAdditionalFilter> = [
    {
      id: 'Comchip Technology|funky|String',
      name: 'Comchip Technology',
      productsTotal: 1,
    },
    {
      id: 'Good-Ark|funky|String',
      name: 'Good-Ark',
      productsTotal: 181,
    },
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductAppliedFiltersComponent],
      providers: [{ provide: Store, useClass: StoreMock }, { provide: InventoryService, useClass: InventoryServiceMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductAppliedFiltersComponent);
    component = fixture.componentInstance;
    inventoryService = TestBed.get(InventoryService);
    store = TestBed.get(Store);
    fixture.detectChanges();
  });

  it('`removeFilter()` should dispatch an event', () => {
    component.appliedFilters = appliedFilters;
    component.appliedManufacturers = appliedManufacturers;

    spyOn(store, 'dispatch');
    component.removeFilter('IN_STOCK');
    expect(store.dispatch).toHaveBeenCalled();
  });

  it('`removeManufacturer()` should dispatch an event', () => {
    component.appliedFilters = appliedFilters;
    component.appliedManufacturers = appliedManufacturers;

    spyOn(store, 'dispatch');
    component.removeManufacturer({ id: 'Comchip Technology|funky|String', name: 'Comchip Technology', productsTotal: 1 });
    expect(store.dispatch).toHaveBeenCalled();
  });

  it('`localiseEnum()` should invoke `inventoryService.localiseEnum()`', () => {
    spyOn(inventoryService, 'localiseEnum');
    component.localiseEnum('IN_STOCK');
    expect(inventoryService.localiseEnum).toHaveBeenCalled();
  });

  it('`submitFilters()` should invoke `removeAppliedFilter` and dispatch and event', () => {
    component.appliedFilters = appliedFilters;
    component.appliedManufacturers = appliedManufacturers;

    spyOn(component, 'removeAppliedFilter');
    spyOn(store, 'dispatch');

    component.submitFilters('IN_STOCK');

    expect(component.removeAppliedFilter).toHaveBeenCalledWith(component.appliedFilters, 'IN_STOCK');
    expect(component.removeAppliedFilter).toHaveBeenCalledWith(component.appliedManufacturers, 'IN_STOCK');
    expect(store.dispatch).toHaveBeenCalled();
  });
});
