import { Component, Input } from '@angular/core';
import { InventoryService } from '@app/core/inventory/inventory.service';
import { RemoveFilter, RemoveManufacturer, SubmitFilters } from '@app/features/products/stores/product-search/product-search.actions';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { IAppliedFilters, IProductAdditionalFilter, IProductFilter } from '@app/core/inventory/product-search.interface';
import {
  getAppliedCategory,
  getSelectedFilters,
  getSelectedManufacturers,
} from '@app/features/products/stores/product-search/product-search.selectors';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-product-applied-filters',
  templateUrl: './product-applied-filters.component.html',
  styleUrls: ['./product-applied-filters.component.scss'],
})
export class ProductAppliedFiltersComponent {
  @Input() public appliedFilters: Array<IProductFilter>;
  @Input() public appliedManufacturers: Array<IProductAdditionalFilter>;
  public appliedCategory$: Observable<string>;
  public selectedFilters$: Observable<Array<string>>;
  public selectedManufacturers$: Observable<Array<IProductAdditionalFilter>>;

  constructor(public store: Store<IAppState>, public inventoryService: InventoryService) {
    this.getStoreSlices();
  }

  public getStoreSlices(): void {
    this.appliedCategory$ = this.store.pipe(select(getAppliedCategory));
    this.selectedFilters$ = this.store.pipe(select(getSelectedFilters));
    this.selectedManufacturers$ = this.store.pipe(select(getSelectedManufacturers));
  }

  public removeFilter(filterName: string): void {
    this.store.dispatch(new RemoveFilter(filterName));
    this.submitFilters(filterName);
  }

  /**
   * remove manufacturer from store and submit filters to trigger api call
   * @param {IProductAdditionalFilter} manufacturer
   */
  public removeManufacturer(manufacturer: IProductAdditionalFilter): void {
    this.store.dispatch(new RemoveManufacturer(manufacturer));
    this.submitFilters(manufacturer.name);
  }

  public submitFilters(removedFilter): void {
    const filters: IAppliedFilters = {};

    this.appliedCategory$.subscribe(appliedCategory => {
      filters.appliedCategory = appliedCategory;
    });

    this.selectedFilters$.subscribe(selectedFilters => {
      filters.appliedFilters = this.removeAppliedFilter(this.appliedFilters, removedFilter);
    });

    this.selectedManufacturers$.subscribe(selectedManufacturers => {
      filters.appliedManufacturers = this.removeAppliedFilter(this.appliedManufacturers, removedFilter);
    });

    this.store.dispatch(new SubmitFilters(filters));
  }

  public removeAppliedFilter(appliedFilters, removedFilter) {
    return appliedFilters.filter((item, index) => item.name !== removedFilter);
  }

  /**
   * convert enum to friendly label
   * @param {string} filterName
   * @returns {string}
   */
  public localiseEnum(filterName: string): string {
    return this.inventoryService.localiseEnum(filterName);
  }
}
