import { AddFilter, RemoveFilter } from '@app/features/products/stores/product-search/product-search.actions';
import { Component, Input } from '@angular/core';
import { InventoryService } from '@app/core/inventory/inventory.service';
import { IAppState } from '@app/shared/shared.interfaces';
import { IProductFilter } from '@app/core/inventory/product-search.interface';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-product-filters',
  templateUrl: './product-filters.component.html',
  styleUrls: ['./product-filters.component.scss'],
})
export class ProductFiltersComponent {
  @Input() public filters: Array<IProductFilter> = [];
  @Input() public selectedFilters: Array<string>;

  constructor(private store: Store<IAppState>, private inventoryService: InventoryService) {}

  public localiseEnum(filterName: string): string {
    return this.inventoryService.localiseEnum(filterName);
  }

  public isChecked(filterName: string): boolean {
    return this.selectedFilters ? this.selectedFilters.includes(filterName) : false;
  }

  /**
   * Collect selected filters on store
   * @param {boolean} isChecked
   * @param {string} filterName
   */
  public toggleFilter(isChecked: boolean, filterName: string): void {
    if (!isChecked) {
      this.store.dispatch(new RemoveFilter(filterName));
    } else {
      this.store.dispatch(new AddFilter(filterName));
    }
  }
}
