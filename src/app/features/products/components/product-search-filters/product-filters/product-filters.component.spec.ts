import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { InventoryService } from '@app/core/inventory/inventory.service';
import { ProductFiltersComponent } from './product-filters.component';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';

export class StoreMock {
  public dispatch(): void {}
  public select(): void {}
  public pipe() {
    return of({});
  }
}

export class InventoryServiceMock {
  public localiseEnum(): void {}
}

describe('ProductFiltersComponent', () => {
  let component: ProductFiltersComponent;
  let fixture: ComponentFixture<ProductFiltersComponent>;
  let store: Store<any>;
  let inventoryService: InventoryService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductFiltersComponent],
      providers: [{ provide: Store, useClass: StoreMock }, { provide: InventoryService, useClass: InventoryServiceMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductFiltersComponent);
    inventoryService = TestBed.get(InventoryService);
    store = TestBed.get(Store);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('`filters` should be initially an empty array', () => {
    expect(component.filters.length).toBe(0);
  });

  it('`localiseEnum()` should invoke `inventoryService.localiseEnum()`', () => {
    spyOn(inventoryService, 'localiseEnum');
    component.localiseEnum('China RoHS');
    expect(inventoryService.localiseEnum).toHaveBeenCalled();
  });

  it('`isChecked()` should return true if `filterName` exists in `selectedFilters`', () => {
    component.selectedFilters = ['IN STOCK'];
    expect(component.isChecked('IN STOCK')).toBeTruthy();
  });

  it('`isChecked()` should return false if `filterName` does not exist in `selectedFilters`', () => {
    component.selectedFilters = [];
    expect(component.isChecked('IN STOCK')).toBeFalsy();
  });

  it('`toggleFilter()` should dispatch an event to the store', () => {
    spyOn(store, 'dispatch');
    component.toggleFilter(true, 'CHINA ROHS');
    expect(store.dispatch).toHaveBeenCalled();
  });
});
