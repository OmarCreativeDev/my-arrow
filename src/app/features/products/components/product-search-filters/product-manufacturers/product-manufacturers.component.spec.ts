import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ProductManufacturersComponent } from './product-manufacturers.component';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';

export class StoreMock {
  public select(): void {}
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

describe('ProductManufacturersComponent', () => {
  let component: ProductManufacturersComponent;
  let fixture: ComponentFixture<ProductManufacturersComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductManufacturersComponent],
      providers: [{ provide: Store, useClass: StoreMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductManufacturersComponent);
    store = TestBed.get(Store);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('`isChecked()` should return true if `manufacturerName` exists in `selectedFilters`', () => {
    component.selectedManufacturers = [{ name: 'SAMSUNG DIODES', id: 'some|funky|String', productsTotal: 31 }];
    expect(component.isChecked(component.selectedManufacturers[0])).toBeTruthy();
  });

  it('`isChecked()` should return false if `manufacturerName` does not exist in `selectedFilters`', () => {
    component.selectedManufacturers = [];
    expect(component.isChecked(component.selectedManufacturers[0])).toBeFalsy();
  });

  it('`selectManufacturer()` should dispatch an event on the store`', () => {
    spyOn(store, 'dispatch');
    component.selectManufacturer(false, { name: 'SAMSUNG DIODES', id: 'some|funky|String', productsTotal: 121 });
    expect(store.dispatch).toHaveBeenCalled();
  });
});
