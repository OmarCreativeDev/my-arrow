import { AddManufacturer, RemoveManufacturer } from '@app/features/products/stores/product-search/product-search.actions';
import { Component, Input } from '@angular/core';
import { IAppState } from '@app/shared/shared.interfaces';
import { IProductAdditionalFilter } from '@app/core/inventory/product-search.interface';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-product-manufacturers',
  templateUrl: './product-manufacturers.component.html',
  styleUrls: ['./product-manufacturers.component.scss'],
})
export class ProductManufacturersComponent {
  @Input() public manufacturers: Array<IProductAdditionalFilter> = [];
  @Input() public selectedManufacturers: Array<IProductAdditionalFilter>;

  constructor(public store: Store<IAppState>) {}

  /**
   * Check if manufacturer exists in selectedManufacturers collection
   * @param {IProductAdditionalFilter} manufacturer
   * @returns {boolean}
   */
  public isChecked(manufacturer: IProductAdditionalFilter): boolean {
    let isChecked: boolean;

    this.selectedManufacturers.forEach(item => {
      if (item.name === manufacturer.name) {
        isChecked = true;
      }
    });

    return isChecked;
  }

  /**
   *
   * @param {boolean} isChecked
   * @param {IProductAdditionalFilter} manufacturer
   */
  public selectManufacturer(isChecked: boolean, manufacturer: IProductAdditionalFilter): void {
    if (!isChecked) {
      this.store.dispatch(new RemoveManufacturer(manufacturer));
    } else {
      this.store.dispatch(new AddManufacturer(manufacturer));
    }
  }
}
