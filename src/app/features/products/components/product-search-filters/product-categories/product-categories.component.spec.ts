import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IProductPrimaryCategory } from '@app/core/inventory/product-search.interface';
import { ProductCategoriesComponent } from './product-categories.component';
import { ScrollToElementDirective } from '@app/shared/directives/scroll-to-element/scroll-to-element.directive';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';

export class StoreMock {
  public select(): void {}
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

describe('ProductCategoriesComponent', () => {
  let component: ProductCategoriesComponent;
  let fixture: ComponentFixture<ProductCategoriesComponent>;
  let store: Store<any>;

  const categoryMock: IProductPrimaryCategory = {
    name: 'some amazing category',
    secondaryCategories: [
      {
        name: 'a sub category',
        tertiaryCategories: [
          {
            name: 'test',
            productsTotal: 22,
            id: '^2015/13290/2023',
          },
        ],
      },
    ],
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductCategoriesComponent, ScrollToElementDirective],
      providers: [{ provide: Store, useClass: StoreMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCategoriesComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);
    fixture.detectChanges();
  });

  it('`expandCategory()` sets `isOpen` to true on category record', () => {
    categoryMock.isOpen = false;
    component.expandCategory(categoryMock);

    expect(categoryMock.isOpen).toBeTruthy();
  });

  it('`collapseCategory()` sets `isOpen` to false on category record', () => {
    categoryMock.isOpen = true;
    component.collapseCategory(categoryMock);

    expect(categoryMock.isOpen).toBeFalsy();
  });

  it('`setCategory()` dispatches an event on the store', () => {
    spyOn(store, 'dispatch');
    component.setCategory('DIODES');
    expect(store.dispatch).toHaveBeenCalled();
  });
});
