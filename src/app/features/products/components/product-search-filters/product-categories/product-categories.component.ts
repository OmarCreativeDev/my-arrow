import { Component, Input } from '@angular/core';
import { IAppState } from '@app/shared/shared.interfaces';
import { IProductPrimaryCategory, IProductSecondaryCategory } from '@app/core/inventory/product-search.interface';
import { Store } from '@ngrx/store';
import { SetCategory } from '@app/features/products/stores/product-search/product-search.actions';

@Component({
  selector: 'app-product-categories',
  templateUrl: './product-categories.component.html',
  styleUrls: ['./product-categories.component.scss'],
})
export class ProductCategoriesComponent {
  @Input() public categories: Array<IProductPrimaryCategory>;
  @Input() public appliedCategory: string;

  constructor(private store: Store<IAppState>) {}

  public expandCategory(category: IProductPrimaryCategory | IProductSecondaryCategory): void {
    category.isOpen = true;
  }

  public collapseCategory(category: IProductPrimaryCategory | IProductSecondaryCategory): void {
    category.isOpen = false;
  }

  public setCategory(categoryName: string): void {
    this.store.dispatch(new SetCategory(categoryName));
  }
}
