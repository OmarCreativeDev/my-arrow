import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ProductSearchFiltersComponent } from './product-search-filters.component';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';

export class StoreMock {
  public select(): void {}
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

describe('ProductSearchFiltersComponent', () => {
  let component: ProductSearchFiltersComponent;
  let fixture: ComponentFixture<ProductSearchFiltersComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductSearchFiltersComponent],
      providers: [{ provide: Store, useClass: StoreMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSearchFiltersComponent);
    store = TestBed.get(Store);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('`getStoreSlices()` grabs 5 slices from the store', () => {
    spyOn(store, 'pipe').calls.count();
    component.getStoreSlices();
    expect(store.pipe).toHaveBeenCalledTimes(5);
  });
});
