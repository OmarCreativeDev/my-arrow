import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { IProductCategory } from '@app/core/inventory/product-category.interface';
import { catalogueReducers } from '@app/features/products/stores/catalogue/catalogue.reducers';
import { StoreModule, Store } from '@ngrx/store';

import { CatalogueListComponent } from './catalogue-list.component';
import { OrderProductCategoriesPipe } from '@app/features/products/pipes/order-product-categories.pipe';
import { Router } from '@angular/router';
import { userReducers } from '@app/core/user/store/user.reducers';
import { SharedModule } from '@app/shared/shared.module';
import { SelectCategory } from '../../stores/catalogue/catalogue.actions';
import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { propertiesReducers } from '@app/features/properties/store/properties.reducers';
import { mockPrivateProperties } from '@app/core/features/feature-flags.mock';

class DummyComponent {}

const mockPropertiesReducers = () => ({
  ...propertiesReducers,
  private: mockPrivateProperties,
});

const taxonomyMock: IProductCategory = {
  id: 7015,
  name: 'BOM',
  totalProducts: 12,
  childCategories: [
    {
      id: 7033,
      name: 'Other VAS',
      totalProducts: 12,
      childCategories: [
        {
          id: 7119,
          name: 'Misc- Other VAS',
          totalProducts: 4,
        },
      ],
    },
  ],
};

describe('CatalogueListComponent', () => {
  const routes = [{ path: 'products/search/', component: DummyComponent }];
  let component: CatalogueListComponent;
  let fixture: ComponentFixture<CatalogueListComponent>;
  let store: Store<any>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        StoreModule.forRoot({ catalogue: catalogueReducers, user: userReducers, properties: mockPropertiesReducers }),
        RouterTestingModule.withRoutes(routes),
        NoopAnimationsModule,
      ],
      declarations: [CatalogueListComponent, OrderProductCategoriesPipe],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.get(Router);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('loadTaxonomy is invoked when ngOnInit() event has fired', () => {
    spyOn(component, 'loadTaxonomy');
    component.ngOnInit();
    expect(component.loadTaxonomy).toHaveBeenCalled();
  });

  it('should select and deselect a category when onSelectedCategory is called with level 1', () => {
    component.taxonomy = [taxonomyMock];
    component.onSelectedCategory(taxonomyMock, 1);
    fixture.detectChanges();
    const element = <HTMLInputElement>fixture.debugElement.query(By.css('.category--level-1 span.small')).nativeElement;
    expect(component.selectedCategory).toEqual(taxonomyMock);
    expect(component.selectedSubCategory).toBeNull();
    expect(element.innerText).toBe('12 products');

    component.onSelectedCategory(taxonomyMock, 1);
    expect(component.selectedCategory).toBeNull();
    expect(component.selectedSubCategory).toBeNull();
  });

  it('should select and deselect a subcategory when onSelectedCategory is called with level 2', () => {
    component.selectedCategory = taxonomyMock;
    component.onSelectedCategory(taxonomyMock.childCategories[0], 2);
    fixture.detectChanges();
    const element = <HTMLInputElement>fixture.debugElement.query(By.css('.category--level-2 span.small')).nativeElement;
    expect(component.selectedSubCategory).toEqual(taxonomyMock.childCategories[0]);
    expect(element.innerText).toBe('12 products');

    component.onSelectedCategory(taxonomyMock.childCategories[0], 2);
    expect(component.selectedSubCategory).toBeNull();
  });

  it('should navigate to /products/search when onSelectedCategory is called with level 3', () => {
    const spy = spyOn(router, 'navigate');
    component.selectedCategory = taxonomyMock;
    component.selectedSubCategory = taxonomyMock.childCategories[0];
    component.onSelectedCategory(taxonomyMock.childCategories[0].childCategories[0], 3);
    fixture.detectChanges();
    const element = <HTMLInputElement>fixture.debugElement.query(By.css('.category--level-3 span.small')).nativeElement;
    const spyResult = spy.calls.first().args;
    expect(spyResult[0][0]).toBe('/products/search');
    expect(spyResult[1].queryParams.cat).toBe('7119');
    expect(spyResult[1].queryParams.categoryName).toBe('Misc- Other VAS');
    expect(element.innerText).toBe('4 products');
  });

  it('should register the category selection onSelectedCategory', () => {
    const action = new SelectCategory({ name: 'foo', level: 1 });
    component.registerCategorySelection('foo', 1);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
});

describe('CatalogueListComponent', () => {
  const routes = [{ path: 'products/search/', component: DummyComponent }];
  let component: CatalogueListComponent;
  let fixture: ComponentFixture<CatalogueListComponent>;
  let store: Store<any>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        StoreModule.forRoot({ catalogue: catalogueReducers, user: userReducers }),
        RouterTestingModule.withRoutes(routes),
        NoopAnimationsModule,
      ],
      declarations: [CatalogueListComponent, OrderProductCategoriesPipe],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogueListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.get(Router);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should not display count when onSelectedCategory is called with level 1 and flag is false', () => {
    component.taxonomy = [taxonomyMock];
    component.onSelectedCategory(taxonomyMock, 1);
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('.category--level-1 span.small'))).toBeNull();
    expect(component.selectedCategory).toEqual(taxonomyMock);
    expect(component.selectedSubCategory).toBeNull();
  });

  it('should not display count when onSelectedCategory is called with level 2 and flag is false', () => {
    component.selectedCategory = taxonomyMock;
    component.onSelectedCategory(taxonomyMock.childCategories[0], 2);
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('.category--level-2 span.small'))).toBeNull();
    expect(component.selectedSubCategory).toEqual(taxonomyMock.childCategories[0]);
  });

  it('should not display count when onSelectedCategory is called with level 3 and flag is false', () => {
    const spy = spyOn(router, 'navigate');
    component.selectedCategory = taxonomyMock;
    component.selectedSubCategory = taxonomyMock.childCategories[0];
    component.onSelectedCategory(taxonomyMock.childCategories[0].childCategories[0], 3);
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('.category--level-3 span.small'))).toBeNull();
    const spyResult = spy.calls.first().args;
    expect(spyResult[0][0]).toBe('/products/search');
  });
});
