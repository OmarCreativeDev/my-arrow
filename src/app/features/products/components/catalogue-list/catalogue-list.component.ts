import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { IProductCategory, IUserTaxonomy } from '@app/core/inventory/product-category.interface';
import { TaxonomyLevel } from '@app/core/inventory/catalog-list.enum.ts';
import { SelectCategory, LoadCatalogue } from '@app/features/products/stores/catalogue/catalogue.actions';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { getIsTaxonomyLoaded, getTaxonomy, getLoading, getError } from '../../stores/catalogue/catalogue.selectors';
import { take } from 'rxjs/operators';
import { IUser } from '@app/core/user/user.interface';
import { getUser } from '@app/core/user/store/user.selectors';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: 'app-catalogue-list',
  templateUrl: './catalogue-list.component.html',
  styleUrls: ['./catalogue-list.component.scss'],
})
export class CatalogueListComponent implements OnInit, OnDestroy {
  public error$: Observable<Error>;
  public loading$: Observable<boolean>;
  public isTaxonomyLoaded$: Observable<boolean>;
  public taxonomy$: Observable<Array<IProductCategory>>;
  public user$: Observable<IUser>;
  public privateFeatureFlags$: Observable<any>;

  public selectedCategory: IProductCategory;
  public selectedSubCategory: IProductCategory;
  public taxonomy: Array<IProductCategory>;
  public user: IUser;
  public error: object;
  public startHeight: number;
  public privateFeatureFlags: object;

  private subscription: Subscription = new Subscription();

  constructor(private router: Router, private store: Store<IAppState>) {
    this.getStoreSlices();
  }

  ngOnInit() {
    this.startSubscriptions();
    this.loadTaxonomy();
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public onSelectedCategory(category: IProductCategory, level: TaxonomyLevel): void {
    let categoryName: string = category.name;
    if (level === TaxonomyLevel.LEVEL_1) {
      this.selectedSubCategory = null;
      this.selectedCategory = category !== this.selectedCategory ? category : null;
      categoryName = this.selectedCategory ? this.selectedCategory.name : null;
    } else if (level === TaxonomyLevel.LEVEL_2) {
      this.selectedSubCategory = category !== this.selectedSubCategory ? category : null;
      categoryName = this.selectedSubCategory ? this.selectedSubCategory.name : null;
    }
    this.registerCategorySelection(categoryName, level);

    if (level === TaxonomyLevel.LEVEL_3) {
      this.router.navigate(['/products/search'], {
        queryParams: { cat: `${category.id}`, categoryName: category.name },
      });
    }
  }

  public registerCategorySelection(categoryName: string, level: TaxonomyLevel): void {
    if (categoryName !== null) {
      this.store.dispatch(new SelectCategory({ name: categoryName, level: level }));
    }
  }

  private getStoreSlices(): void {
    this.loading$ = this.store.pipe(select(getLoading));
    this.error$ = this.store.pipe(select(getError));
    this.isTaxonomyLoaded$ = this.store.pipe(select(getIsTaxonomyLoaded));
    this.taxonomy$ = this.store.pipe(select(getTaxonomy));
    this.user$ = this.store.pipe(select(getUser));
    this.privateFeatureFlags$ = this.store.pipe(select(getPrivateFeatureFlagsSelector));
  }

  private startSubscriptions(): void {
    this.subscription.add(this.subscribeToTaxonomy());
    this.subscription.add(this.subscribeToUser());
    this.subscription.add(this.subscribeToErrors());
    this.subscription.add(this.subscribePrivateFeatureFlags());
  }

  public loadTaxonomy(): void {
    this.isTaxonomyLoaded$.pipe(take(1)).subscribe(isTaxonomyLoaded => {
      if (!isTaxonomyLoaded) {
        const warehouseCodes: Array<string> = this.user ? this.user.warehouses.map(warehouse => warehouse.code) : [];
        const userTaxonomy: IUserTaxonomy = {
          warehouseList: warehouseCodes,
          billTo: this.user ? this.user.selectedBillTo : 0,
          region: this.user ? this.user.registrationInstance : '',
        };
        this.store.dispatch(new LoadCatalogue(userTaxonomy));
      }
    });
  }

  private subscribeToTaxonomy(): Subscription {
    return this.taxonomy$.subscribe(taxonomy => (this.taxonomy = taxonomy));
  }

  private subscribeToErrors(): Subscription {
    return this.error$.subscribe(error => (this.error = error));
  }

  private subscribeToUser(): Subscription {
    return this.user$.subscribe(user => (this.user = user));
  }

  private subscribePrivateFeatureFlags(): Subscription {
    return this.privateFeatureFlags$.subscribe(featureFlags => (this.privateFeatureFlags = featureFlags));
  }
}
