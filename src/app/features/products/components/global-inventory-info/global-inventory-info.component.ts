import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-global-inventory-info',
  templateUrl: './global-inventory-info.component.html',
  styleUrls: ['./global-inventory-info.component.scss']
})
export class GlobalInventoryInfoComponent implements OnInit {

  @Input() additionalPartsCount: number = null;
  @Input() searchQuery: string = '';
  @Input() inventoryInfoVisible: boolean;
  public modalVisible: boolean = false;
  public searchUrl: string = 'https://www.arrow.com/en/products/search?q=';
  public showInventoryModalCheckbox: boolean = null;

  public ngOnInit(): void {
    this.setSearchUrl();
    this.getModalVisibilitySetting();
  }

  public setSearchUrl(): void {
    this.searchUrl += this.searchQuery;
  }

  public getModalVisibilitySetting(): boolean {
    return localStorage.getItem('hideGlobalInventoryModal') ? true : false;
  }

  public setModalVisibilitySetting(): void {
    if (this.showInventoryModalCheckbox) {
      localStorage.setItem('hideGlobalInventoryModal', JSON.stringify(this.showInventoryModalCheckbox));
    } else {
      this.removeModalVisibilitySetting();
    }
  }

  public removeModalVisibilitySetting(): void {
    localStorage.removeItem('hideGlobalInventoryModal');
  }

  public showModal(): void {
    this.modalVisible = true;
  }

  public hideModal(): void {
    this.modalVisible = false;
  }

  public hideInventoryInfo(): void {
    this.inventoryInfoVisible = false;
  }

}
