import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { BackdropServiceMock, ModalServiceMock } from '@app/app.component.spec';
import { GlobalInventoryInfoComponent } from './global-inventory-info.component';
import { ModalService } from '@app/shared/services/modal.service';
import { SharedModule } from '@app/shared/shared.module';

describe('GlobalInventoryInfoComponent', () => {
  let component: GlobalInventoryInfoComponent;
  let fixture: ComponentFixture<GlobalInventoryInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule
      ],
      declarations: [GlobalInventoryInfoComponent],
      providers: [
        { provide: BackdropService, useClass: BackdropServiceMock },
        { provide: ModalService, useClass: ModalServiceMock }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalInventoryInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('`additionalPartsCount` should be initially undefined', () => {
    expect(component.additionalPartsCount).toBeNull();
  });

  it('`searchQuery` should be initially an empty string', () => {
    expect(typeof component.searchQuery).toBe('string');
    expect(component.searchQuery.length).toBe(0);
  });

  it('`showInventoryModalCheckbox` should be initially undefined', () => {
    expect(component.showInventoryModalCheckbox).toBeNull();
  });

  it('`modalVisible` should be initially false', () => {
    expect(component.modalVisible).toBeFalsy();
  });

  it('`searchUrl` should be initially set to `https://www.arrow.com/en/products/search?q=`', () => {
    const searchUrlMock: string = 'https://www.arrow.com/en/products/search?q=';

    expect(typeof component.searchUrl).toBe('string');
    expect(component.searchUrl.length).toBe(searchUrlMock.length);
  });

  it('`ngOnInit` should invoke `setSearchUrl()` and `getModalVisibilitySetting()`', () => {
    spyOn(component, 'setSearchUrl');
    spyOn(component, 'getModalVisibilitySetting');

    component.ngOnInit();
    expect(component.setSearchUrl).toHaveBeenCalled();
    expect(component.getModalVisibilitySetting).toHaveBeenCalled();
  });

  it('`setSearchUrl()` sets `searchUrl` to https://www.arrow.com/en/products/search?q=BAV99', () => {
    component.searchQuery = 'BAV99';
    component.setSearchUrl();

    expect(component.searchUrl.length).toBe(48);
  });

  it('`getModalVisibilitySetting()` should return true if its present in local storage', () => {
    component.showInventoryModalCheckbox = true;
    component.setModalVisibilitySetting();
    expect(component.getModalVisibilitySetting()).toBeTruthy();
  });

  it('`getModalVisibilitySetting()` should return false if its not present in local storage', () => {
    component.removeModalVisibilitySetting();
    expect(component.getModalVisibilitySetting()).toBeFalsy();
  });

  it('`setModalVisibilitySetting()` should setItem on localStorage if `showInventoryModalCheckbox` is true',
    () => {
      spyOn(localStorage, 'setItem');
      component.showInventoryModalCheckbox = true;
      component.setModalVisibilitySetting();

      expect(localStorage.setItem).toHaveBeenCalled();
    });

  it('`removeModalVisibilitySetting()` should be invoked if `showInventoryModalCheckbox` is false',
    () => {
      spyOn(component, 'removeModalVisibilitySetting');
      component.showInventoryModalCheckbox = false;
      component.setModalVisibilitySetting();

      expect(component.removeModalVisibilitySetting).toHaveBeenCalled();
    });

  it('`removeModalVisibilitySetting()` should removeItem from localStorage', () => {
    spyOn(localStorage, 'removeItem');
    component.removeModalVisibilitySetting();

    expect(localStorage.removeItem).toHaveBeenCalled();
  });

  it('`showModal()` should set `modalVisible` to true', () => {
    component.showModal();
    expect(component.modalVisible).toBeTruthy();
  });

  it('`hideModal()` should set `modalVisible` to false', () => {
    component.hideModal();
    expect(component.modalVisible).toBeFalsy();
  });

  it('`hideInventoryInfo()` should set `inventoryInfoVisible` to false', () => {
    component.hideInventoryInfo();
    expect(component.inventoryInfoVisible).toBeFalsy();
  });

});
