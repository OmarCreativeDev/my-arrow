import { Component, Input } from '@angular/core';
import { IProductSpecification } from '@app/shared/shared.interfaces';

@Component({
  selector: 'app-product-specification',
  templateUrl: './product-specification.component.html',
  styleUrls: ['./product-specification.component.scss'],
})
export class ProductSpecificationComponent {
  @Input() specifications: IProductSpecification;
}
