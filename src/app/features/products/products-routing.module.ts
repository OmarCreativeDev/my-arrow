import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CatalogueComponent } from './pages/catalogue/catalogue.component';
import { ProductSearchComponent } from './pages/product-search/product-search.component';
import { ProductDetailsComponent } from '@app/features/products/pages/product-details/product-details.component';
import { NotFoundComponent } from '@app/features/errors/pages/not-found/not-found.component';
import { CatalogueGuard } from '@app/core/inventory/catalogue.guard';

const productsRoutes: Routes = [
  { path: '', component: CatalogueComponent, data: { title: 'Products' }, canActivate: [CatalogueGuard] },
  { path: 'search/:searchQuery', component: ProductSearchComponent, data: { title: 'Products Search' } },
  { path: 'search', component: ProductSearchComponent, data: { title: 'Products Search' } },
  { path: ':productId', component: ProductDetailsComponent, data: { title: 'View Product' } },
  { path: '**', component: NotFoundComponent, data: { title: 'Products Search' } },
];

@NgModule({
  imports: [RouterModule.forChild(productsRoutes)],
  exports: [RouterModule],
})
export class ProductsRoutingModule {}
