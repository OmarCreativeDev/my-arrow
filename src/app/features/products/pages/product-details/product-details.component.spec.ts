import { ActivatedRoute } from '@angular/router';
import { ApiService } from '@app/core/api/api.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CoreModule } from '@app/core/core.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { InventoryService } from '@app/core/inventory/inventory.service';
import { of } from 'rxjs';
import { ProductDetailComponent } from '@app/features/products/components/product-detail/product-detail.component';
import { ProductDetailsComponent } from './product-details.component';
import { ProductSpecificationComponent } from '@app/features/products/components/product-specification/product-specification.component';
import { Store, StoreModule } from '@ngrx/store';
import { productDetailsReducers } from '@app/features/products/stores/product-details/product-details.reducers';
import { GetProductDetails } from '@app/features/products/stores/product-details/product-details.actions';

describe('ProductDetailsComponent', () => {
  const mockProductId: string = '1234_12345678';
  let component: ProductDetailsComponent;
  let fixture: ComponentFixture<ProductDetailsComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({ productDetails: productDetailsReducers }), CoreModule, HttpClientTestingModule],
      declarations: [ProductDetailsComponent, ProductDetailComponent, ProductSpecificationComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              productId: mockProductId,
            }),
          },
        },
        ApiService,
        InventoryService,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch GetProductDetails action with correct productId param', () => {
    component.ngOnInit();
    component.productDetailsState$.subscribe(() => {
      expect(store.dispatch).toHaveBeenCalledWith(new GetProductDetails(mockProductId));
    });
  });
});
