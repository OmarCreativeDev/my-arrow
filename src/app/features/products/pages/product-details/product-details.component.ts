import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { Store, select } from '@ngrx/store';
import { IProductDetails, IProductDetailsState } from '@app/core/inventory/product-details.interface';
import { GetProductDetails } from '@app/features/products/stores/product-details/product-details.actions';
import { getProductDetailsState } from '@app/features/products/stores/product-details/product-details.selectors';
import { IAppState } from '@app/shared/shared.interfaces';
import { getQuoteCartMaxLineItems, getQuoteCartRemainingLineItems } from '@app/features/quotes/stores/quote-cart.selectors';
import { ISearchListing } from '@app/core/inventory/product-search.interface';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
  public loading = true;
  public product: IProductDetails;
  public crossReference: ISearchListing;
  public error: HttpErrorResponse;
  public productId: string;
  public productDetailsState$: Observable<IProductDetailsState>;
  public permissions$: Observable<Array<string>>;
  public permissions: Array<string>;
  public quoteCartMaxLineItems$: Observable<number>;
  public quoteCartRemainingLineItems$: Observable<number>;
  private subscription: Subscription = new Subscription();

  constructor(private activatedRoute: ActivatedRoute, private store: Store<IAppState>) {
    this.getStoreSlices();
  }

  ngOnInit() {
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions(): any {
    this.subscription.add(this.subscribeRouteParams());
  }

  private subscribeRouteParams(): Subscription {
    return this.activatedRoute.params.subscribe(value => {
      this.productId = value.productId;
      this.productDetailsState$.pipe(take(4)).subscribe(productDetailsState => {
        this.loading = productDetailsState.loading;
        this.product = productDetailsState.product;
        this.error = productDetailsState.error;
        this.crossReference = productDetailsState.crossReference;
      });
      this.store.dispatch(new GetProductDetails(this.productId));
    });
  }

  public getStoreSlices(): void {
    this.productDetailsState$ = this.store.pipe(select(getProductDetailsState));
    this.quoteCartMaxLineItems$ = this.store.pipe(select(getQuoteCartMaxLineItems));
    this.quoteCartRemainingLineItems$ = this.store.pipe(select(getQuoteCartRemainingLineItems));
  }
}
