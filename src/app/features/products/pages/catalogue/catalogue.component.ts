import { Component, OnDestroy } from '@angular/core';
import { ResetSelections } from '@app/features/products/stores/catalogue/catalogue.actions';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.component.html',
  styleUrls: ['./catalogue.component.scss'],
})
export class CatalogueComponent implements OnDestroy {
  constructor(private store: Store<IAppState>) {}

  ngOnDestroy() {
    this.store.dispatch(new ResetSelections());
  }
}
