import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IAddToCartRequestItem } from '@app/core/cart/cart.interfaces';
import { DialogService, Dialog } from '@app/core/dialog/dialog.service';
import {
  IGlobalInventory,
  IProductAdditionalFilter,
  IProductFilter,
  IProductPrimaryCategory,
  ISearchListing,
  ISortProductSearch,
} from '@app/core/inventory/product-search.interface';
import { IAddToQuoteCartRequestItem } from '@app/core/quote-cart/quote-cart.interfaces';
import { getUserBillToAccount } from '@app/core/user/store/user.selectors';
import {
  ChangePage,
  ChangePageLimit,
  SetCategory,
  SetSortOptions,
  SubmitFilters,
  SubmitSearch,
  ToggleQuoteTooltip,
} from '@app/features/products/stores/product-search/product-search.actions';
import {
  getCategories,
  getError,
  getErrorCodeMessage,
  getFilters,
  getGlobalInventory,
  getLimit,
  getLoading,
  getManufacturers,
  getPage,
  getProductsTotal,
  getQuoteOnlyItemsLength,
  getSearchQuery,
  getSearchResults,
  getSelectedIds,
  getSortOptions,
  getTotalPages,
} from '@app/features/products/stores/product-search/product-search.selectors';
import { getQuoteCartMaxLineItems, getQuoteCartRemainingLineItems } from '@app/features/quotes/stores/quote-cart.selectors';
import { getCartMaxLineItems, getCartRemainingLineItems } from '@app/features/cart/stores/cart/cart.selectors';
import { AddToCartDialogComponent, CartType } from '@app/shared/components/add-to-cart-dialog/add-to-cart-dialog.component';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import * as moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { filter, first, take } from 'rxjs/operators';
import { QuoteCartLimit } from '@app/features/quotes/stores/quote-cart.actions';
import { IAddToBomPart } from '@app/core/boms/boms.interface';
import { OpenAddToBomModal } from '@app/core/boms/store/boms.actions';
import { AddToCartCapDialogComponent } from '@app/shared/components/add-to-cart-cap-dialog/add-to-cart-cap-dialog.component';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';
import { ShoppingCartLimit } from '@app/features/cart/stores/cart/cart.actions';

@Component({
  selector: 'app-product-search',
  templateUrl: './product-search.component.html',
  styleUrls: ['./product-search.component.scss'],
})
export class ProductSearchComponent implements OnInit, OnDestroy {
  // bindings to store slices
  public billToAccount$: Observable<number>;
  public categories$: Observable<Array<IProductPrimaryCategory>>;
  public error$: Observable<Error>;
  public errorCodeMessage$: Observable<string>;
  public filters$: Observable<Array<IProductFilter>>;
  public globalInventory$: Observable<IGlobalInventory>;
  public limit$: Observable<number>;
  public loading$: Observable<boolean>;
  public manufacturers$: Observable<Array<IProductAdditionalFilter>>;
  public page$: Observable<number>;
  public productsTotal$: Observable<number>;
  public selectedIds$: Observable<Array<string>>;
  public selectedIds: Array<string>;
  public searchQuery$: Observable<string>;
  public searchResults$: Observable<Array<ISearchListing>>;
  public searchResults: Array<ISearchListing>;
  public sortOptions$: Observable<ISortProductSearch>;
  public totalPages$: Observable<number>;
  public quoteOnlyItemsLength$: Observable<number>;
  public quoteOnlyItemsLength: number;

  // cache search query & selected bill to
  public currentBillToAccount: number;
  public searchQuery: string;
  public quoteCartMaxLineItems$: Observable<number>;
  public quoteCartRemainingLineItems$: Observable<number>;
  public quoteCartMaxLineItems: number;
  public quoteCartRemainingLineItems: number;
  public cartMaxLineItems$: Observable<number>;
  public cartRemainingLineItems$: Observable<number>;
  public cartMaxLineItems: number;
  public cartRemainingLineItems: number;
  public privateFeatures$: Observable<object>;
  private capDialog: Dialog<AddToCartCapDialogComponent>;
  private subscription: Subscription = new Subscription();

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public store: Store<IAppState>,
    public dialogService: DialogService
  ) {
    this.getStoreSlices();
    this.getCurrentBillToAccount();
    this.setInitialValuesInStore();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeRemainingLineItems());
    this.subscription.add(this.subscribeMaxLineItems());
    this.subscription.add(this.subscribeCartRemainingLineItems());
    this.subscription.add(this.subscribeSelectedIds());
    this.subscription.add(this.subscribeSearchResults());
    this.subscription.add(this.subscribeQuoteOnlyItemsLength());
    this.subscription.add(this.subscribeAccountId());
  }

  private subscribeAccountId(): Subscription {
    return this.quoteCartMaxLineItems$.subscribe(MaxLineItems => {
      this.quoteCartMaxLineItems = MaxLineItems;
    });
  }

  private subscribeRemainingLineItems(): Subscription {
    return this.quoteCartRemainingLineItems$.subscribe(RemainingLineItems => {
      this.quoteCartRemainingLineItems = RemainingLineItems;
    });
  }

  private subscribeMaxLineItems(): Subscription {
    return this.cartMaxLineItems$.subscribe(maxLineItems => (this.cartMaxLineItems = maxLineItems));
  }

  private subscribeCartRemainingLineItems(): Subscription {
    return this.cartRemainingLineItems$.subscribe(remainingLineItems => (this.cartRemainingLineItems = remainingLineItems));
  }

  private subscribeSelectedIds(): Subscription {
    return this.selectedIds$.subscribe(selectedIds => {
      this.selectedIds = selectedIds;

      if (this.selectedIds.length < 1) {
        this.store.dispatch(new ToggleQuoteTooltip(0));
      }
    });
  }

  private subscribeSearchResults(): Subscription {
    return this.searchResults$.subscribe(searchResults => {
      this.searchResults = searchResults;
    });
  }

  private subscribeQuoteOnlyItemsLength(): Subscription {
    return this.quoteOnlyItemsLength$.subscribe(quoteOnlyItemsLength => {
      this.quoteOnlyItemsLength = quoteOnlyItemsLength;
    });
  }

  public getStoreSlices(): void {
    this.privateFeatures$ = this.store.pipe(select(getPrivateFeatureFlagsSelector));
    this.billToAccount$ = this.store.pipe(select(getUserBillToAccount));
    this.categories$ = this.store.pipe(select(getCategories));
    this.error$ = this.store.pipe(select(getError));
    this.errorCodeMessage$ = this.store.pipe(select(getErrorCodeMessage));
    this.filters$ = this.store.pipe(select(getFilters));
    this.globalInventory$ = this.store.pipe(select(getGlobalInventory));
    this.limit$ = this.store.pipe(select(getLimit));
    this.loading$ = this.store.pipe(select(getLoading));
    this.manufacturers$ = this.store.pipe(select(getManufacturers));
    this.page$ = this.store.pipe(select(getPage));
    this.productsTotal$ = this.store.pipe(select(getProductsTotal));
    this.searchQuery$ = this.store.pipe(select(getSearchQuery));
    this.searchResults$ = this.store.pipe(select(getSearchResults));
    this.sortOptions$ = this.store.pipe(select(getSortOptions));
    this.totalPages$ = this.store.pipe(select(getTotalPages));
    this.selectedIds$ = this.store.pipe(select(getSelectedIds));
    this.quoteOnlyItemsLength$ = this.store.pipe(select(getQuoteOnlyItemsLength));
    this.quoteCartMaxLineItems$ = this.store.pipe(select(getQuoteCartMaxLineItems));
    this.quoteCartRemainingLineItems$ = this.store.pipe(select(getQuoteCartRemainingLineItems));
    this.cartMaxLineItems$ = this.store.pipe(select(getCartMaxLineItems));
    this.cartRemainingLineItems$ = this.store.pipe(select(getCartRemainingLineItems));
  }

  /**
   * Retrieve and cache bill to account
   * Use first() to close subscription
   */
  public getCurrentBillToAccount(): void {
    this.billToAccount$.pipe(first()).subscribe((billToAccount: number) => {
      this.setCurrentBillToAccount(billToAccount);
    });
  }

  /**
   * Set initial billTo Account
   * @param {number} billToAccount
   */
  public setCurrentBillToAccount(billToAccount: number): void {
    this.currentBillToAccount = billToAccount;
  }

  public setInitialValuesInStore(): void {
    this.setPageLimit(10);
    this.pageChange(1);
    this.store.dispatch(new SetCategory(''));
    this.store.dispatch(
      new SetSortOptions({
        sortField: '',
        sortDirection: '',
      })
    );
    this.store.dispatch(new SubmitFilters({}));
  }

  public ngOnInit(): void {
    this.startSubscriptions();
    this.checkForHashFragment();
    this.triggerSearchOnBillToChange();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
    if (this.capDialog) {
      this.capDialog.close();
    }
  }

  public checkForHashFragment(): void {
    this.activatedRoute.fragment.subscribe((hashFragment: string) => {
      this.getSearchQuery(hashFragment);
    });
  }

  /**
   * Subscribe to billToAccount$ in user store
   * Then if changed trigger a default product search
   */
  public triggerSearchOnBillToChange(): void {
    this.billToAccount$.pipe(filter(billToAccount => billToAccount !== 0)).subscribe((billToAccount: number) => {
      if (this.currentBillToAccount !== billToAccount) {
        this.setCurrentBillToAccount(billToAccount);
        this.defaultProductSearch();
      }
    });
  }

  /**
   * Get Search Query
   * and dispatch events to the store
   * @param {string} hashFragment
   */
  public getSearchQuery(hashFragment?: string): void {
    this.activatedRoute.params.subscribe(params => {
      this.searchQuery = hashFragment ? `${params.searchQuery}#${hashFragment}` : params.searchQuery;
      this.defaultProductSearch();
    });
    this.activatedRoute.queryParams.pipe(take(1)).subscribe(params => {
      if (params.cat) {
        const categoryName = params.categoryName || '';
        this.store.dispatch(new SetCategory(params.cat));
        this.store.dispatch(new SubmitFilters({ clearSelectedFilters: true }));
        this.store.dispatch(new SubmitSearch(categoryName));
      }
    });
  }

  public defaultProductSearch(): void {
    if (!this.searchQuery) {
      return;
    }
    this.store.dispatch(new SubmitSearch(this.searchQuery));
    this.store.dispatch(new SetCategory(''));
    this.store.dispatch(new SubmitFilters({ clearSelectedFilters: true }));
    this.pageChange(1);
  }

  public setPageLimit(limit: number): void {
    this.store.dispatch(new ChangePageLimit(limit));
  }

  public pageChange(page: number) {
    this.store.dispatch(new ChangePage(page));
  }

  public openAddToCartCapDialog(cap: number, cartType: CartType): void {
    this.capDialog = this.dialogService.open(AddToCartCapDialogComponent, {
      data: {
        cap,
        cartType,
      },
      size: 'small',
    });
  }

  public addToQuoteCart(selectedIds: Array<string>): void {
    if (this.quoteCartRemainingLineItems === 0 || selectedIds.length > this.quoteCartRemainingLineItems) {
      const requestedItemsCount = this.quoteCartMaxLineItems - this.quoteCartRemainingLineItems + selectedIds.length;
      this.openAddToCartCapDialog(this.quoteCartMaxLineItems, CartType.QUOTE_CART);
      this.store.dispatch(new QuoteCartLimit(requestedItemsCount));
    } else {
      const products = this.searchResults.filter(product => selectedIds.includes(product.docId));
      const items: Array<IAddToQuoteCartRequestItem> = products.map(product => {
        return {
          docId: product.docId,
          itemId: product.itemId,
          warehouseId: product.warehouseId,
          quantity: product.quantity ? product.quantity : 1,
          ...(product.selectedCustomerPartNumber && { selectedCustomerPartNumber: product.selectedCustomerPartNumber }),
          ...(product.selectedEndCustomerSiteId && { selectedEndCustomerSiteId: product.selectedEndCustomerSiteId }),
          ...(product.price && product.price >= 0 && { targetPrice: product.price }),
          ...(product.mpn && { manufacturerPartNumber: product.mpn }),
        };
      });

      this.dialogService.open(AddToCartDialogComponent, {
        data: {
          cartType: CartType.QUOTE_CART,
          items,
        },
        size: 'large',
      });
    }
  }

  public addToCart(selectedIds: Array<string>): void {
    const products = this.searchResults.filter(product => selectedIds.includes(product.docId) && product.quotable !== true);
    if (this.cartRemainingLineItems === 0 || products.length > this.cartRemainingLineItems) {
      this.openAddToCartCapDialog(this.cartMaxLineItems, CartType.SHOPPING_CART);
      this.store.dispatch(new ShoppingCartLimit(this.cartMaxLineItems));
    } else {
      const items: Array<IAddToCartRequestItem> = products.map(product => {
        return {
          manufacturerPartNumber: product.mpn,
          docId: product.docId,
          itemId: product.itemId,
          warehouseId: product.warehouseId,
          quantity: product.quantity ? product.quantity : 1,
          manufacturer: product.manufacturer,
          description: product.description,
          requestDate: moment(new Date()).format('YYYY-MM-DD'),
          ...(product.selectedCustomerPartNumber && { selectedCustomerPartNumber: product.selectedCustomerPartNumber }),
          ...(product.selectedEndCustomerSiteId && { selectedEndCustomerSiteId: product.selectedEndCustomerSiteId }),
        };
      });

      const dialog = this.dialogService.open(AddToCartDialogComponent, {
        data: {
          cartType: CartType.SHOPPING_CART,
          items,
        },
        size: 'large',
      });

      if (typeof dialog !== 'undefined') {
        dialog.afterClosed.subscribe(dialogCancelled => {
          if (!dialogCancelled) {
            this.toggleTooltip(selectedIds);
          }
        });
      }
    }
  }

  public toggleTooltip(selectedIds: Array<string>): void {
    const quoteOnlyItemsLength = this.searchResults.filter(product => selectedIds.includes(product.docId) && product.quotable).length;

    this.store.dispatch(new ToggleQuoteTooltip(quoteOnlyItemsLength));
  }

  public get purchasableSelected(): Boolean {
    const purchasableSelected = this.searchResults.filter(product => {
      return this.selectedIds.includes(product.docId) && !product.quotable;
    });

    return purchasableSelected.length > 0;
  }

  public addToBom(selectedIds: Array<string>) {
    const partsToAdd: Array<IAddToBomPart> = this.searchResults
      .filter(part => {
        return selectedIds.includes(part.docId);
      })
      .map(part => {
        return {
          manufacturerPartNumber: part.mpn,
          manufacturer: part.manufacturer,
          quantity: part.quantity || 1,
          docId: part.docId,
          itemId: part.itemId,
          warehouseId: part.warehouseId,
        };
      });

    this.store.dispatch(new OpenAddToBomModal({ url: this.router.url, parts: partsToAdd }));
  }
}
