import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { InventoryService } from '@app/core/inventory/inventory.service';
import {
  ChangePage,
  ChangePageLimit,
  SetCategory,
  SetSortOptions,
  SubmitFilters,
  SubmitSearch,
} from '@app/features/products/stores/product-search/product-search.actions';
import { SharedModule } from '@app/shared/shared.module';
import { Store } from '@ngrx/store';
import { Observable, of } from 'rxjs';

import { ProductSearchComponent } from './product-search.component';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { BackdropServiceMock, ModalServiceMock } from '@app/app.component.spec';
import { ModalService } from '@app/shared/services/modal.service';

export class InventoryServiceMock {
  public search(): Observable<any> {
    return of({});
  }
}

export class StoreMock {
  public dispatch(): void {}
  public select(): Observable<number> {
    return of(10);
  }
  public pipe() {
    return of({});
  }
}

class MockRouter {
  navigateByUrl(url: string) {
    return url;
  }
}

describe('ProductSearchComponent', () => {
  const searchQueryString: string = 'someAwesomeSearchQuery';

  const mockSearchResult = [
    {
      arrowReel: false,
      availableQuantity: 0,
      bufferQuantity: 0,
      compliance: {},
      description: 'BAV99^EICSEMI',
      docId: '1790_18978016',
      id: '1790_18978016',
      itemId: 18978016,
      image: 'image.png',
      price: 0.1234,
      itemType: 'COMPONENTS',
      leadTime: 9,
      manufacturer: 'Electronics Industry Public Company Limited',
      minimumOrderQuantity: 3000,
      mpn: 'BAV99',
      multipleOrderQuantity: 3000,
      quantity: 10,
      quotable: false,
      spq: 3000,
      specs: [],
      warehouseId: 1790,
      selectedCustomerPartNumber: '1234',
      selectedEndCustomerSiteId: 34839,
    },
  ];

  const mockSearchResultWithQuotableTrue = [
    {
      arrowReel: false,
      availableQuantity: 0,
      bufferQuantity: 0,
      compliance: {},
      description: 'BAV99^EICSEMI',
      docId: '1790_18978016',
      id: '1790_18978016',
      itemId: 18978016,
      image: 'image.png',
      price: 0.1234,
      itemType: 'COMPONENTS',
      leadTime: 9,
      manufacturer: 'Electronics Industry Public Company Limited',
      minimumOrderQuantity: 3000,
      mpn: 'BAV99',
      multipleOrderQuantity: 3000,
      quantity: 10,
      quotable: true,
      spq: 3000,
      specs: [],
      warehouseId: 1790,
      selectedCustomerPartNumber: '1234',
      selectedEndCustomerSiteId: 34839,
    },
  ];

  let component: ProductSearchComponent;
  let fixture: ComponentFixture<ProductSearchComponent>;
  let dialogService: DialogService;
  let store: Store<any>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, HttpClientTestingModule],
      declarations: [ProductSearchComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              searchQuery: searchQueryString,
            }),
            queryParams: of({
              cat: 'foo/bar/baz',
            }),
            fragment: of('someFunkyFragmentString'),
          },
        },
        { provide: DialogService, useClass: DialogServiceMock },
        { provide: InventoryService, useClass: InventoryServiceMock },
        { provide: Store, useClass: StoreMock },
        { provide: BackdropService, useClass: BackdropServiceMock },
        { provide: ModalService, useClass: ModalServiceMock },
        { provide: Router, useClass: MockRouter },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSearchComponent);
    component = fixture.componentInstance;
    dialogService = TestBed.get(DialogService);
    store = TestBed.get(Store);
    router = TestBed.get(Router);
    spyOn(router, 'navigateByUrl').and.callThrough();
    fixture.detectChanges();
  });

  it('`getCurrentBillToAccount()` invokes `setCurrentBillToAccount()`', () => {
    spyOn(component, 'setCurrentBillToAccount');
    component.getCurrentBillToAccount();
    expect(component.setCurrentBillToAccount).toHaveBeenCalled();
  });

  it('`setCurrentBillToAccount()` sets `currentBillToAccount`', () => {
    component.setCurrentBillToAccount(77712);
    expect(component.currentBillToAccount).toBeDefined();
    expect(component.currentBillToAccount).toEqual(77712);
  });

  it('`setInitialValuesInStore()` sets initial values by dispatching 5 events', () => {
    spyOn(component, 'setPageLimit');
    spyOn(component, 'pageChange');
    spyOn(store, 'dispatch');

    component.setInitialValuesInStore();

    expect(component.setPageLimit).toHaveBeenCalledWith(10);
    expect(component.pageChange).toHaveBeenCalledWith(1);
    expect(store.dispatch).toHaveBeenCalledWith(new SetCategory(''));
    expect(store.dispatch).toHaveBeenCalledWith(new SetSortOptions({ sortField: '', sortDirection: '' }));
    expect(store.dispatch).toHaveBeenCalledWith(new SubmitFilters({}));
  });

  it('`checkForHashFragment()` is invoked when `ngOnInit()` event has fired', () => {
    spyOn(component, 'checkForHashFragment');
    component.ngOnInit();
    expect(component.checkForHashFragment).toHaveBeenCalled();
  });

  it('`triggerSearchOnBillToChange()` is invoked when `ngOnInit()` event has fired', () => {
    spyOn(component, 'triggerSearchOnBillToChange');
    component.ngOnInit();
    expect(component.triggerSearchOnBillToChange).toHaveBeenCalled();
  });

  it('`checkForHashFragment()` invokes `getSearchQuery()`', () => {
    spyOn(component, 'getSearchQuery');
    component.checkForHashFragment();
    expect(component.getSearchQuery).toHaveBeenCalled();
  });

  it('`triggerSearchOnBillToChange()` invokes `defaultProductSearch()` and `setCurrentBillToAccount()` if `billToAccount` has changed', () => {
    spyOn(component, 'setCurrentBillToAccount');
    spyOn(component, 'defaultProductSearch');
    component.currentBillToAccount = 32313;
    component.triggerSearchOnBillToChange();
    expect(component.setCurrentBillToAccount).toHaveBeenCalled();
    expect(component.defaultProductSearch).toHaveBeenCalled();
  });

  it('`getSearchQuery()` sets `searchQuery` and invokes `defaultProductSearch()`', () => {
    spyOn(component, 'defaultProductSearch');
    component.getSearchQuery();
    expect(component.searchQuery).toBeDefined();
    expect(component.defaultProductSearch).toHaveBeenCalled();
  });

  it('`defaultProductSearch` dispatches relevant actions to the store', () => {
    spyOn(store, 'dispatch');
    spyOn(component, 'pageChange');

    component.searchQuery = searchQueryString;
    component.defaultProductSearch();

    expect(store.dispatch).toHaveBeenCalledWith(new SubmitSearch(searchQueryString));
    expect(store.dispatch).toHaveBeenCalledWith(new SetCategory(''));
    expect(store.dispatch).toHaveBeenCalledWith(new SubmitFilters({ clearSelectedFilters: true }));
    expect(component.pageChange).toHaveBeenCalledWith(1);
  });

  it('`ChangePageLimit` action is dispatched on the store when `setPageLimit()` method has fired', () => {
    spyOn(store, 'dispatch');
    component.setPageLimit(10);
    expect(store.dispatch).toHaveBeenCalledWith(new ChangePageLimit(10));
  });

  it('`ChangePage` action is dispatched on the store when `pageChange()` method has fired', () => {
    spyOn(store, 'dispatch');
    component.pageChange(3);
    expect(store.dispatch).toHaveBeenCalledWith(new ChangePage(3));
  });

  it('`addToCart()` should add product(s) to the shopping cart', () => {
    component.searchResults = mockSearchResult;
    const mockSelectedIds = ['1790_18978016'];

    spyOn(dialogService, 'open');
    component.addToCart(mockSelectedIds);
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('`addToQuoteCart()` should add product(s) to the quote cart', () => {
    component.searchResults = mockSearchResult;
    const mockSelectedIds = ['1790_18978016'];

    spyOn(dialogService, 'open');
    component.addToQuoteCart(mockSelectedIds);
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('`purchasableSelected()` should return true if the selected items are purchasable', () => {
    component.searchResults = mockSearchResult;
    component.selectedIds = ['1790_18978016'];
    const result = component.purchasableSelected;

    expect(result).toBeTruthy();
  });

  it('`purchasableSelected()` should return false if the selected items are not purchasable`', () => {
    component.searchResults = mockSearchResultWithQuotableTrue;
    component.selectedIds = ['1790_18978016'];
    const result = component.purchasableSelected;

    expect(result).toBeFalsy();
  });

  it('should call `openAddToCartCapDialog` when addToQuoteCart and max cap has been reached', () => {
    spyOn(component, 'openAddToCartCapDialog').and.callThrough();
    component.quoteCartRemainingLineItems = 0;
    component.addToQuoteCart([]);
    expect(component.openAddToCartCapDialog).toHaveBeenCalled();
  });

  it('should call `openAddToCartCapDialog` when addToCart and max cap has been reached', () => {
    spyOn(component, 'openAddToCartCapDialog').and.callThrough();
    component.searchResults = mockSearchResult;
    component.cartRemainingLineItems = 0;
    component.addToCart([mockSearchResult[0].docId]);
    expect(component.openAddToCartCapDialog).toHaveBeenCalled();
  });
});
