import { BuyPartsComponent } from './components/buy-parts/buy-parts.component';
import { CatalogueComponent } from './pages/catalogue/catalogue.component';
import { CatalogueListComponent } from './components/catalogue-list/catalogue-list.component';
import { CommonModule } from '@angular/common';
import { EffectsModule } from '@ngrx/effects';
import { ErrorsModule } from '@app/features/errors/errors.module';
import { GlobalInventoryInfoComponent } from './components/global-inventory-info/global-inventory-info.component';
import { InventoryService } from '@app/core/inventory/inventory.service';
import { NgModule } from '@angular/core';
import { OrderProductCategoriesPipe } from './pipes/order-product-categories.pipe';
import { ProductAppliedFiltersComponent } from './components/product-search-filters/product-applied-filters/product-applied-filters.component';
import { ProductCategoriesComponent } from './components/product-search-filters/product-categories/product-categories.component';
import { ProductDetailComponent } from './components/product-detail/product-detail.component';
import { ProductDetailsComponent } from './pages/product-details/product-details.component';
import { ProductFiltersActionsComponent } from './components/product-search-filters/product-filters-actions/product-filters-actions.component';
import { ProductFiltersComponent } from './components/product-search-filters/product-filters/product-filters.component';
import { ProductManufacturersComponent } from './components/product-search-filters/product-manufacturers/product-manufacturers.component';
import { ProductRowComponent } from './components/product-search-listing/product-row/product-row.component';
import { ProductSearchComponent } from './pages/product-search/product-search.component';
import { ProductSearchEffects } from '@app/features/products/stores/product-search/product-search.effects';
import { ProductSearchFiltersComponent } from './components/product-search-filters/product-search-filters.component';
import { ProductSearchListingComponent } from './components/product-search-listing/product-search-listing.component';
import { ProductSpecificationComponent } from './components/product-specification/product-specification.component';
import { ProductsRoutingModule } from './products-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { StoreModule } from '@ngrx/store';
import { productSearchAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.product-search';
import { CatalogueGuard } from '@app/core/inventory/catalogue.guard';

@NgModule({
  imports: [
    CommonModule,
    EffectsModule.forFeature([ProductSearchEffects]),
    ErrorsModule,
    ProductsRoutingModule,
    SharedModule,
    StoreDevtoolsModule.instrument({ maxAge: 25 }),
    StoreModule.forFeature('productSearch', productSearchAnalyticsMetaReducers),
  ],
  declarations: [
    BuyPartsComponent,
    CatalogueComponent,
    CatalogueListComponent,
    GlobalInventoryInfoComponent,
    OrderProductCategoriesPipe,
    ProductAppliedFiltersComponent,
    ProductCategoriesComponent,
    ProductDetailComponent,
    ProductDetailsComponent,
    ProductFiltersActionsComponent,
    ProductFiltersComponent,
    ProductManufacturersComponent,
    ProductRowComponent,
    ProductSearchComponent,
    ProductSearchFiltersComponent,
    ProductSearchListingComponent,
    ProductSpecificationComponent,
  ],
  providers: [InventoryService, CatalogueGuard],
})
export class ProductsModule {}
