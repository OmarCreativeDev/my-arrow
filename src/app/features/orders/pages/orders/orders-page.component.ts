import { ChangeDetectionStrategy, Component } from '@angular/core';
import { IFilter, IOrderLine, ISearchCriteria, ISortCriteron, IAppState } from '@app/shared/shared.interfaces';
import { forkJoin, Observable } from 'rxjs';
import { first } from 'rxjs/operators';

import { Store, select } from '@ngrx/store';
import { getError, getFilter, getLoading, getResult, getSearch, getSort } from '../../stores/orders/orders.selectors';
import { SubmitSearch } from '../../stores/orders/orders.actions';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogData, DownloadOrdersComponent } from '@app/features/orders/components/download-orders/download-orders.component';
import { HttpErrorResponse } from '@angular/common/http';
import { getQuoteCartMaxLineItems, getQuoteCartRemainingLineItems } from '@app/features/quotes/stores/quote-cart.selectors';

@Component({
  selector: 'app-orders-page',
  templateUrl: './orders-page.component.html',
  styleUrls: ['./orders-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrdersPageComponent {
  public search$: Observable<ISearchCriteria>;
  public filter$: Observable<IFilter>;
  public result$: Observable<Array<IOrderLine>>;
  public loading$: Observable<boolean>;
  public error$: Observable<HttpErrorResponse>;
  public sort$: Observable<Array<ISortCriteron>>;

  public quoteCartMaxLineItems$: Observable<number>;
  public quoteCartRemainingLineItems$: Observable<number>;

  constructor(private store: Store<IAppState>, private dialogService: DialogService) {
    this.getStoreSlices();
  }

  public getStoreSlices() {
    this.search$ = this.store.pipe(select(getSearch));
    this.filter$ = this.store.pipe(select(getFilter));
    this.result$ = this.store.pipe(select(getResult));
    this.loading$ = this.store.pipe(select(getLoading));
    this.error$ = this.store.pipe(select(getError));
    this.sort$ = this.store.pipe(select(getSort));
    this.quoteCartMaxLineItems$ = this.store.pipe(select(getQuoteCartMaxLineItems));
    this.quoteCartRemainingLineItems$ = this.store.pipe(select(getQuoteCartRemainingLineItems));
  }

  /**
   * Submits a search criteria to the Query; will trigger a Query if all other facets are also set
   * @param searchCriteria the search criteria that is saved to the state
   */
  public submitSearch(searchCriteria: ISearchCriteria): void {
    this.store.dispatch(new SubmitSearch({ ...searchCriteria }));
  }

  /**
   * Opens a DownloadOrders Dialog
   */
  public openDownloadOrdersDialog(): void {
    forkJoin(
      // User first() so there is no need to unsubscribe
      this.search$.pipe(first()),
      this.filter$.pipe(first()),
      this.sort$.pipe(first())
    ).subscribe(results => {
      const data: DialogData = {
        search: results[0],
        filter: results[1],
        sort: results[2],
      };
      this.dialogService.open(DownloadOrdersComponent, { size: 'large', data });
    });
  }
}
