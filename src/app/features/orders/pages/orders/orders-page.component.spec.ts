import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OrdersPageComponent } from './orders-page.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import { ordersReducers } from '../../stores/orders/orders.reducers';
import { SubmitSearch } from '@app/features/orders/stores/orders/orders.actions';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { quoteCartReducers } from '@app/features/quotes/stores/quote-cart.reducers';

describe('OrdersPageComponent', () => {
  let component: OrdersPageComponent;
  let fixture: ComponentFixture<OrdersPageComponent>;
  let store: Store<any>;
  let dialogService: DialogService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrdersPageComponent],
      imports: [
        StoreModule.forRoot({
          orders: combineReducers(ordersReducers),
          quoteCart: combineReducers(quoteCartReducers),
        }),
      ],
      providers: [{ provide: DialogService, useClass: DialogServiceMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    fixture = TestBed.createComponent(OrdersPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    dialogService = TestBed.get(DialogService);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it(`should dispatch a Search event on search`, () => {
    const searchCriteria = {
      type: 'foo',
      value: 'bar',
    };
    const action = new SubmitSearch(searchCriteria);
    component.submitSearch(searchCriteria);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('should use the Dialog Service to open a Download Orders dialog', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.openDownloadOrdersDialog();
    expect(dialogService.open).toHaveBeenCalled();
  });
});
