import { ComponentFixture, TestBed } from '@angular/core/testing';
import { OrderReturnsComponent } from './order-returns.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule } from '@ngrx/store';
import { orderDetailsReducers } from '@app/features/orders/stores/order-details/order-details.reducers';
import { QueryComplete } from '@app/features/orders/stores/order-details/order-details.actions';
import { orderReturnReason } from '@app/shared/shared.interfaces';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@app/shared/shared.module';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { OrdersService } from '@app/core/orders/orders.service';
import { of } from 'rxjs/internal/observable/of';
import { throwError } from 'rxjs/internal/observable/throwError';
import { mockUser } from '@app/core/user/user.service.spec';
import { userReducers } from '@app/core/user/store/user.reducers';
import { mockOrder, ngFormMock } from '@app/core/orders/orders.mock';

class DummyComponent {}

class MockOrdersService {
  public requestReturnOrder() {}

  public removeAttachmentsByOrderId() {}
}

describe('OrderReturnsComponent', () => {
  let component: OrderReturnsComponent;
  let fixture: ComponentFixture<OrderReturnsComponent>;
  let router: Router;
  let store: Store<any>;
  let dialogService: DialogService;
  let ordersService: OrdersService;

  const routes = [{ path: 'orders/:orderId', component: DummyComponent }];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes),
        StoreModule.forRoot({
          orderDetails: orderDetailsReducers,
          user: userReducers,
        }),
        FormsModule,
      ],
      declarations: [OrderReturnsComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{ provide: DialogService, useClass: DialogServiceMock }, { provide: OrdersService, useClass: MockOrdersService }],
    });
    router = TestBed.get(Router);
    ordersService = TestBed.get(OrdersService);
    fixture = TestBed.createComponent(OrderReturnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    dialogService = TestBed.get(DialogService);
    ordersService = TestBed.get(OrdersService);
  });

  beforeEach(() => {
    store.dispatch(new QueryComplete(mockOrder));
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should remove the attachments and navigate to order details when canceling the form', () => {
    const spy = spyOn(router, 'navigateByUrl');
    spyOn(ordersService, 'removeAttachmentsByOrderId').and.callFake(() => of({}));
    component.onCancelOrder();
    expect(ordersService.removeAttachmentsByOrderId).toHaveBeenCalledWith(mockOrder.details.salesOrderId);
    const url = spy.calls.first().args[0];
    expect(url).toBe(`orders/${mockOrder.details.salesOrderId}?salesHeaderId=${mockOrder.orderLines[0].salesHeaderId}`);
  });

  it('should update selectedReturnReason when onSelectedReason()', () => {
    component.onSelectReason(orderReturnReason.shortShip);
    expect(component.selectedReturnReason).toBe('Short Ship');
  });

  it('should return true when form is invalid', () => {
    const result = component.isInvalidForm(ngFormMock);
    expect(result).toBeTruthy();
  });

  it('should open dialog when onSubmitSuccess()', () => {
    const returnReason: string = 'Damaged';
    spyOn(dialogService, 'open');
    component.onSubmitSuccess(returnReason);
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should open dialog when onSubmitError()', () => {
    spyOn(dialogService, 'open');
    component.onSubmitError();
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should successfully submit the request return form', () => {
    spyOn(ordersService, 'requestReturnOrder').and.callFake(() => of([]));
    spyOn(component, 'onSubmitSuccess').and.callThrough();
    component.user = mockUser;
    component.onSubmit(ngFormMock);
    expect(ordersService.requestReturnOrder).toHaveBeenCalled();
    expect(component.onSubmitSuccess).toHaveBeenCalled();
  });

  it('should handle the error when submit the request return form fails', () => {
    spyOn(ordersService, 'requestReturnOrder').and.callFake(() => throwError('Error'));
    spyOn(component, 'onSubmitError').and.callThrough();
    component.user = mockUser;
    component.onSubmit(ngFormMock);
    expect(ordersService.requestReturnOrder).toHaveBeenCalled();
    expect(component.onSubmitError).toHaveBeenCalled();
  });
});
