import { Component, OnDestroy, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import {
  IAppState,
  IOrder,
  IRmaSubmitRequest,
  IOrderReturnFormHeader,
  IRmaLineItemRequest,
  IOrderReturnFormLineItem,
  IOrderLine,
  IRmaShipmentRequest,
  orderReturnReason,
  orderReturnCategory,
  IRmaAttachmentRequest,
} from '@app/shared/shared.interfaces';
import { getOrder } from '@app/features/orders/stores/order-details/order-details.selectors';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { DialogService } from '@app/core/dialog/dialog.service';
import {
  OrderReturnsSubmitDialogComponent,
  DialogData,
} from '../../components/order-returns-submit-dialog/order-returns-submit-dialog.component';
import { OrdersService } from '@app/core/orders/orders.service';
import { IUser, IWarehouse } from '@app/core/user/user.interface';
import { getUser } from '@app/core/user/store/user.selectors';
import { SubmitRequestReturnOrder } from '../../stores/orders/orders.actions';

@Component({
  selector: 'app-order-returns',
  templateUrl: './order-returns.component.html',
  styleUrls: ['./order-returns.component.scss'],
})
export class OrderReturnsComponent implements OnInit, OnDestroy {
  public order$: Observable<IOrder>;
  public user$: Observable<IUser>;
  public order: IOrder;
  public user: IUser;
  public selectedReturnReason: orderReturnReason;
  public submitProcessing: boolean = false;

  private subscription: Subscription = new Subscription();

  constructor(
    private store: Store<IAppState>,
    private router: Router,
    private dialogService: DialogService,
    private ordersService: OrdersService
  ) {
    this.getStoreSlices();
  }

  ngOnInit() {
    this.startSubscriptions();
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private getStoreSlices(): void {
    this.order$ = this.store.pipe(select(getOrder));
    this.user$ = this.store.pipe(select(getUser));
  }

  private subscribeToOrder(): Subscription {
    return this.order$.subscribe(order => {
      this.order = order;
    });
  }

  private subscribeToUser(): Subscription {
    return this.user$.subscribe(user => {
      this.user = user;
    });
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeToOrder());
    this.subscription.add(this.subscribeToUser());
  }

  private getSalesHeaderId(): number {
    const orderLine = this.order.orderLines.find(line => line.salesHeaderId !== undefined);
    return orderLine ? orderLine.salesHeaderId : 0;
  }

  private hasInvalidOrderLinesOrReturningParts(returnForm: NgForm): boolean {
    const orderLines: Array<string> = Object.keys(returnForm.value).filter(key => key !== 'header');
    const invalidOrder: Array<string> = orderLines.filter(key => !returnForm.value[key]);
    return invalidOrder.length > 0 || (!returnForm.value.header.partsYes && !returnForm.value.header.partsNo);
  }

  public gotoOrderDetails(): void {
    const salesHeaderId = this.getSalesHeaderId();
    const withSalesHeaderId = salesHeaderId !== 0 ? `?salesHeaderId=${salesHeaderId}` : '';
    this.router.navigateByUrl(`orders/${this.order.details.salesOrderId}${withSalesHeaderId}`);
  }

  public onSelectReason(reason: orderReturnReason): void {
    this.selectedReturnReason = reason;
  }

  public isInvalidForm(returnForm: NgForm): boolean {
    return this.selectedReturnReason === undefined || returnForm.form.invalid || this.hasInvalidOrderLinesOrReturningParts(returnForm);
  }

  private buildRmaSubmitRequest(returnForm: NgForm): IRmaSubmitRequest {
    const returnFormValues: object = returnForm.value;
    const formHeader: IOrderReturnFormHeader = returnFormValues['header'];
    const issueType = [orderReturnReason.defectiveParts, orderReturnReason.earlyShipment, orderReturnReason.courtesyReturn];

    return {
      arrowSalesContactEmail: '',
      billToAccount: `${this.order.details.billToID || ''}`,
      shipToAccount: `${this.order.details.shipToID || ''}`,
      contactEmail: this.user.email,
      contactPhone: this.user.phoneNumber,
      customerCapaReference: formHeader.capaNumber,
      customerName: this.user.company,
      customerPONumber: `${this.order.details.purchaseOrderNumber || ''}`,
      customerQualityContactName: `${this.user.firstName} ${this.user.lastName}`,
      enteringBranch: this.order.details.enteringBranch,
      branchTransferOrder: false, // TODO: Field to be field on future user story
      capaRequired: formHeader.capa,
      creditRequired: formHeader.credit,
      failureAnalysisRequired: formHeader.failure,
      repairReplacement: formHeader.repair,
      returningParts: formHeader.partsYes,
      issueType: issueType.includes(this.selectedReturnReason) ? orderReturnCategory.RMA : orderReturnCategory.FQR,
      lineItems: this.buildSubmitRequestLineItem(returnFormValues),
      operatingUnit: this.order.details.operatingUnit,
      oracleAccountNumber: `${this.user.accountNumber}`,
      problemCategory: this.selectedReturnReason,
      problemDescription: formHeader.description,
      region: this.user.region,
      salesOrderNumber: `${this.order.details.salesOrderId}`,
    } as IRmaSubmitRequest;
  }

  private buildSubmitRequestLineItem(returnFormValues: object): Array<IRmaLineItemRequest> {
    const lineItems: Array<IRmaLineItemRequest> = [];
    const orderLines: Array<string> = Object.keys(returnFormValues).filter(key => key !== 'header');

    orderLines.forEach(key => {
      const formItem: IOrderReturnFormLineItem = returnFormValues[key];
      const itemId: number = parseInt(key.replace('orderLine-', ''), 10);
      const orderLine: IOrderLine = this.order.orderLines.find(line => line.itemId === itemId);
      const shipmentRequest: Array<IRmaShipmentRequest> = orderLine.shipments.map(shipment => {
        return {
          carrier: shipment.carrier,
          dateCodeSystem: shipment.dateCode,
          countryOfOrigin: shipment.countryOfOrigin,
          deliveryId: `${shipment.deliveryId}`,
          invoiceNumber: orderLine.invoices.length ? orderLine.invoices[0].number : '',
          shipDate: shipment.shipmentTrackingDate ? shipment.shipmentTrackingDate.toString() : '',
          trackingNumber: shipment.shipmentTrackingReference,
        } as IRmaShipmentRequest;
      });
      const attachmentRequest: Array<IRmaAttachmentRequest> = formItem.attachments.map(formAttachment => {
        return {
          id: formAttachment.id,
          name: formAttachment.name,
        } as IRmaAttachmentRequest;
      });
      const itemWarehouse: IWarehouse = this.user.warehouses.find(warehouse => warehouse.id === orderLine.warehouseId);
      lineItems.push({
        attachments: attachmentRequest,
        customerPartNumber: orderLine.customerPartNumber,
        dateCodeUser: formItem.dateCodeUser,
        deliveries: shipmentRequest,
        lineItem: orderLine.lineItem,
        manufacturerName: orderLine.manufacturerName,
        manufacturerPartNumber: '',
        orderLineType: orderLine.orderType,
        partNumberOrdered: orderLine.manufacturerPartNumber,
        partNumberReceived: formItem.partNumberReceived,
        quantityAffected: formItem.quantityAffected,
        quantityOrdered: orderLine.qtyOrdered,
        quantityReceived: formItem.quantityReceived,
        stockNumber: `${orderLine.itemId}`,
        facilityCode: orderLine.facilityCode,
        warehouseCode: `${itemWarehouse ? itemWarehouse.code : 0}`,
        shipToAccount: orderLine.shipToAccountNumber,
        specialHandlingCode: orderLine.specialHandlingCode,
      } as IRmaLineItemRequest);
    });

    return lineItems;
  }

  public onSubmit(returnForm: NgForm): void {
    this.submitProcessing = true;
    const submitRequest: IRmaSubmitRequest = this.buildRmaSubmitRequest(returnForm);
    this.ordersService
      .requestReturnOrder(submitRequest)
      .subscribe(() => this.onSubmitSuccess(submitRequest.problemCategory), () => this.onSubmitError());
  }

  public onSubmitSuccess(returnReason: string): void {
    this.submitProcessing = false;
    this.store.dispatch(new SubmitRequestReturnOrder(returnReason));
    this.dialogService.open(OrderReturnsSubmitDialogComponent, {
      size: 'small',
      data: { submitFailed: false, orderId: this.order.details.salesOrderId, salesHeaderId: this.getSalesHeaderId() } as DialogData,
    });
  }

  public onSubmitError(): void {
    this.submitProcessing = false;
    this.submitProcessing = false;
    this.dialogService.open(OrderReturnsSubmitDialogComponent, {
      size: 'small',
      data: { submitFailed: true } as DialogData,
    });
  }

  public onCancelOrder(): void {
    this.ordersService.removeAttachmentsByOrderId(this.order.details.salesOrderId).subscribe(() => {
      this.gotoOrderDetails();
    });
  }
}
