import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { IAddToCartRequestItem } from '@app/core/cart/cart.interfaces';
import { DialogService, Dialog } from '@app/core/dialog/dialog.service';
import { OrdersService } from '@app/core/orders/orders.service';
import { IAddToQuoteCartRequestItem } from '@app/core/quote-cart/quote-cart.interfaces';
import { EditRequestedDateDialogComponent } from '@app/features/orders/components/edit-requested-date-dialog/edit-requested-date-dialog.component';
import { InvoiceListingDialogComponent } from '@app/features/orders/components/invoice-listing-dialog/invoice-listing-dialog.component';
import { ShipmentTrackingDialogComponent } from '@app/features/orders/components/shipment-tracking-dialog/shipment-tracking-dialog.component';
import { SingleOrderLinesListingComponent } from '@app/features/orders/components/single-order-lines-listing/single-order-lines-listing.component';
import { Query, ToggleOrderLines } from '@app/features/orders/stores/order-details/order-details.actions';
import { RequestReturnOrder } from '@app/features/orders/stores/orders/orders.actions';
import {
  getDetails,
  getError,
  getLoading,
  getOrder,
  getOrderLines,
  getSelectedIds,
  getSelectedOrderLines,
} from '@app/features/orders/stores/order-details/order-details.selectors';
import { AddToCartDialogComponent, CartType } from '@app/shared/components/add-to-cart-dialog/add-to-cart-dialog.component';
import { DEFAULT_MODAL_INTRO, DEFAULT_MODAL_TITLE, EmailModalComponent } from '@app/shared/components/email-modal/email-modal.component';
import { IAppState, IOrder, IOrderDetails, IOrderLine, IOrderLineStatus } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import * as moment from 'moment';
import { Observable, Subscription } from 'rxjs';
import { getQuoteCartMaxLineItems, getQuoteCartRemainingLineItems } from '@app/features/quotes/stores/quote-cart.selectors';
import { getCartMaxLineItems, getCartRemainingLineItems } from '@app/features/cart/stores/cart/cart.selectors';
import { QuoteCartLimit } from '@app/features/quotes/stores/quote-cart.actions';
import { AddToCartCapDialogComponent } from '@app/shared/components/add-to-cart-cap-dialog/add-to-cart-cap-dialog.component';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.scss'],
  providers: [OrdersService],
})
export class OrderDetailsComponent implements OnInit, OnDestroy {
  @ViewChild(SingleOrderLinesListingComponent)
  orderLinesListing: SingleOrderLinesListingComponent;

  public trackingVisible: boolean = false;
  public invoicesVisible: boolean = false;

  public orderLines$: Observable<Array<IOrderLine>>;
  public details$: Observable<IOrderDetails>;
  public order$: Observable<IOrder>;
  public loading$: Observable<boolean>;
  public error$: Observable<HttpErrorResponse>;
  public selectedIds$: Observable<Array<string>>;
  public selectedOrderLines$: Observable<Array<IOrderLine>>;
  public selectedOrderLines: Array<IOrderLine>;

  public orderId: string = '';

  public headerId: number;
  public billToSiteUseId: number;
  public order: IOrder;

  public orderLines: Array<IOrderLine> = [];

  private capDialog: Dialog<AddToCartCapDialogComponent>;
  public quoteCartMaxLineItems$: Observable<number>;
  public quoteCartRemainingLineItems$: Observable<number>;
  public quoteCartMaxLineItems: number;
  public quoteCartRemainingLineItems: number;
  public cartMaxLineItems$: Observable<number>;
  public cartRemainingLineItems$: Observable<number>;
  public cartMaxLineItems: number;
  public cartRemainingLineItems: number;
  public privateFeatureFlags: object;
  public subscription: Subscription = new Subscription();
  public requestReturnLabel: string;
  public disableRequestReturn: boolean;
  public requestReturnTooltipLabel: string;
  public showReturnTooltip: boolean;
  public isTooltipFixed: boolean;

  public privateFeatureFlags$: Observable<object>;

  public get onlyOpenOrderLines(): boolean {
    return this.selectedOrderLines.length && this.selectedOrderLines.filter(o => o.status !== IOrderLineStatus.Open).length === 0;
  }

  public get showEditDate(): boolean {
    return this.privateFeatureFlags['ordersPushPull'];
  }

  public get showRequestReturn(): boolean {
    return this.privateFeatureFlags['ordersRequestReturn'];
  }

  constructor(
    private route: ActivatedRoute,
    private store: Store<IAppState>,
    private dialogService: DialogService,
    private router: Router
  ) {
    this.getStoreSlices();
  }

  ngOnInit() {
    this.startSubscriptions();
    this.handleGetOrderDetails();
    this.resetRequestReturnState();
  }

  private startSubscriptions(): void {
    this.subscription.add(this.quoteCartMaxLineItemsSubscription());
    this.subscription.add(this.quoteCartRemainingLineItemsSubscription());
    this.subscription.add(this.cartMaxLineItemsSubscription());
    this.subscription.add(this.cartRemainingLineItemsSubscription());
    this.subscription.add(this.orderSubscription());
    this.subscription.add(this.orderLinesSubscription());
    this.subscription.add(this.paramsSubscription());
    this.subscription.add(this.queryParamsSubscription());
    this.subscription.add(this.selectedOrderLineSubscription());
    this.subscription.add(this.featureFlagsSubscription());
  }

  private quoteCartMaxLineItemsSubscription(): Subscription {
    return this.quoteCartMaxLineItems$.subscribe(MaxLineItems => {
      this.quoteCartMaxLineItems = MaxLineItems;
    });
  }

  private quoteCartRemainingLineItemsSubscription(): Subscription {
    return this.quoteCartRemainingLineItems$.subscribe(RemainingLineItems => {
      this.quoteCartRemainingLineItems = RemainingLineItems;
    });
  }

  private cartMaxLineItemsSubscription(): Subscription {
    return this.cartMaxLineItems$.subscribe(maxLineItems => (this.cartMaxLineItems = maxLineItems));
  }

  private cartRemainingLineItemsSubscription(): Subscription {
    return this.cartRemainingLineItems$.subscribe(remainingLineItems => (this.cartRemainingLineItems = remainingLineItems));
  }

  private orderSubscription(): Subscription {
    return this.order$.subscribe(order => {
      this.order = order;
    });
  }

  private orderLinesSubscription(): Subscription {
    return this.orderLines$.subscribe(orderLines => {
      this.orderLines = orderLines;
    });
  }

  private paramsSubscription(): Subscription {
    return this.route.params.subscribe(params => {
      this.orderId = params['orderId'];
    });
  }

  private queryParamsSubscription(): Subscription {
    return this.route.queryParams.subscribe(query => {
      this.headerId = query['salesHeaderId'] ? query['salesHeaderId'] : null;
      this.billToSiteUseId = query['billToSiteUseId'] ? query['billToSiteUseId'] : null;
    });
  }

  private handleGetOrderDetails(): void {
    const params = {
      orderId: this.orderId,
      salesHeaderId: this.headerId,
      billToSiteUseId: this.billToSiteUseId,
    };

    this.store.dispatch(new Query(params));
  }

  private selectedOrderLineSubscription(): Subscription {
    return this.selectedOrderLines$.subscribe(selectedOrders => {
      this.selectedOrderLines = selectedOrders;
    });
  }

  private featureFlagsSubscription(): Subscription {
    return this.privateFeatureFlags$.subscribe(featureFlags => {
      this.privateFeatureFlags = featureFlags;
    });
  }

  private getStoreSlices(): void {
    this.orderLines$ = this.store.pipe(select(getOrderLines));
    this.details$ = this.store.pipe(select(getDetails));
    this.order$ = this.store.pipe(select(getOrder));
    this.loading$ = this.store.pipe(select(getLoading));
    this.error$ = this.store.pipe(select(getError));
    this.selectedIds$ = this.store.pipe(select(getSelectedIds));
    this.selectedOrderLines$ = this.store.pipe(select(getSelectedOrderLines));
    this.quoteCartMaxLineItems$ = this.store.pipe(select(getQuoteCartMaxLineItems));
    this.quoteCartRemainingLineItems$ = this.store.pipe(select(getQuoteCartRemainingLineItems));
    this.cartMaxLineItems$ = this.store.pipe(select(getCartMaxLineItems));
    this.cartRemainingLineItems$ = this.store.pipe(select(getCartRemainingLineItems));
    this.privateFeatureFlags$ = this.store.pipe(select(getPrivateFeatureFlagsSelector));
  }

  ngOnDestroy(): void {
    if (this.capDialog) {
      this.capDialog.close();
    }
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public openEmailRepresentativeDialog(): void {
    const dialog = this.dialogService.open(EmailModalComponent, {
      size: 'large',
      data: {
        header: DEFAULT_MODAL_TITLE,
        headerDescription: DEFAULT_MODAL_INTRO,
        subject: 'Question About MyArrow Orders',
        payload: {
          orderLines: this.selectedOrderLines,
        },
        templateName: 'order-history',
      },
    });

    dialog.afterClosed.pipe(take(1)).subscribe(() => {
      this.deselectAllOrderLines();
    });
  }

  public openEditRequestedDateDialog(): void {
    this.dialogService.open(EditRequestedDateDialogComponent, {
      size: 'x-large',
      data: {
        orderLines: this.selectedOrderLines,
        orderNumber: this.order.details.salesOrderId,
        poNumber: this.order.details.purchaseOrderNumber,
      },
    });

    this.deselectAllOrderLines();
  }

  public openAddToCartCapDialog(cap: number, cartType: CartType): void {
    this.capDialog = this.dialogService.open(AddToCartCapDialogComponent, {
      data: {
        cap,
        cartType,
      },
      size: 'small',
    });
  }

  public addToCart(selectedOrderLines: Array<IOrderLine>): void {
    if (this.cartRemainingLineItems === 0 || selectedOrderLines.length > this.cartRemainingLineItems) {
      this.openAddToCartCapDialog(this.cartMaxLineItems, CartType.SHOPPING_CART);
    } else {
      // Map the selected order lines to AddToCartRequestItems
      const items: Array<IAddToCartRequestItem> = selectedOrderLines.map(selectedOrderLine => {
        return {
          manufacturerPartNumber: selectedOrderLine.manufacturerPartNumber,
          docId: selectedOrderLine.docId,
          itemId: selectedOrderLine.itemId,
          warehouseId: selectedOrderLine.warehouseId,
          quantity: selectedOrderLine.qtyOrdered,
          requestDate: moment(new Date()).format('YYYY-MM-DD'),
          selectedCustomerPartNumber: selectedOrderLine.customerPartNumber,
        };
      });
      // Add the mapped items to the Cart
      const addToCartDialog = this.dialogService.open(AddToCartDialogComponent, {
        data: {
          cartType: CartType.SHOPPING_CART,
          items,
        },
      });

      addToCartDialog.afterClosed.pipe(take(1)).subscribe(() => {
        this.deselectAllOrderLines();
      });
    }
  }

  public addToQuoteCart(selectedOrderLines: Array<IOrderLine>): void {
    if (this.quoteCartRemainingLineItems === 0 || selectedOrderLines.length > this.quoteCartRemainingLineItems) {
      const requestedItemsCount = this.quoteCartMaxLineItems - this.quoteCartRemainingLineItems + selectedOrderLines.length;
      this.openAddToCartCapDialog(this.quoteCartMaxLineItems, CartType.QUOTE_CART);
      this.store.dispatch(new QuoteCartLimit(requestedItemsCount));
    } else {
      const items: Array<IAddToQuoteCartRequestItem> = selectedOrderLines.map(selectedOrderLine => {
        return {
          docId: selectedOrderLine.docId,
          itemId: selectedOrderLine.itemId,
          warehouseId: selectedOrderLine.warehouseId,
          quantity: selectedOrderLine.qtyOrdered,
          requestDate: moment(new Date()).format('YYYY-MM-DD'),
          selectedCustomerPartNumber: selectedOrderLine.customerPartNumber,
          targetPrice: selectedOrderLine.unitResale,
          manufacturerPartNumber: selectedOrderLine.manufacturerPartNumber,
        };
      });
      const dialog = this.dialogService.open(AddToCartDialogComponent, {
        data: {
          cartType: CartType.QUOTE_CART,
          items,
        },
      });

      dialog.afterClosed.pipe(take(1)).subscribe(() => {
        this.deselectAllOrderLines();
      });
    }
  }

  public showInvoicesForOrderLines(orderLines: Array<IOrderLine>): void {
    this.dialogService.open(InvoiceListingDialogComponent, {
      size: 'large',
      data: {
        orderLines,
      },
    });
  }

  public showTrackingDetailsForOrder(order: IOrder): void {
    this.dialogService.open(ShipmentTrackingDialogComponent, {
      size: 'large',
      data: {
        order,
      },
    });
  }

  public onIdSelected(event: ToggleOrderLines): void {
    this.store.dispatch(event);
    if (this.showRequestReturn) {
      this.validateReturnableItems();
    }
  }

  private deselectAllOrderLines(): void {
    this.orderLinesListing.deselectAll();
  }

  private resetRequestReturnState(): void {
    this.requestReturnLabel = 'Request a Return/Quality Issue';
    this.disableRequestReturn = true;
    this.showReturnTooltip = false;
    this.isTooltipFixed = false;
    if (this.orderLinesListing) {
      this.orderLinesListing.updateInvalidOrders(new Array<string>());
    }
  }

  public validateReturnableItems(): void {
    const selectableIds = this.selectedOrderLines
      .filter(orderItem => this.isItemValidForReturn(orderItem))
      .map(orderItem => `${orderItem.salesOrderId}_${orderItem.lineItem}`);
    const notSelectableIds = this.selectedOrderLines
      .filter(orderItem => !this.isItemValidForReturn(orderItem))
      .map(orderItem => `${orderItem.salesOrderId}_${orderItem.lineItem}`);
    const areAllNonShippedOrders: boolean =
      this.selectedOrderLines.length && this.selectedOrderLines.every(orderItem => this.isItemOpenedWithZeroQtyShipped(orderItem));

    this.resetRequestReturnState();
    if (areAllNonShippedOrders) {
      this.checkForAllNonShippedItems();
    } else {
      this.checkForNonReturnableItems(selectableIds, notSelectableIds);
      this.checkForReturnableItems(selectableIds, notSelectableIds);
      this.checkForEligibleReturnItems(selectableIds, notSelectableIds);
    }
  }

  private checkForAllNonShippedItems(): void {
    this.orderLinesListing.updateInvalidOrders(new Array<string>());
    this.showReturnTooltip = true;
    this.requestReturnTooltipLabel = 'Request a Return/QI is only available for Shipped Status';
    this.disableRequestReturn = true;
  }

  private checkForNonReturnableItems(selectableIds: Array<string>, notSelectableIds: Array<string>): void {
    if (selectableIds.length === 0 && notSelectableIds.length > 0) {
      this.requestReturnTooltipLabel = 'Items not eligible for return/quality issue';
      this.showReturnTooltip = true;
      this.disableRequestReturn = true;
      this.orderLinesListing.updateInvalidOrders(notSelectableIds);
    }
  }

  private checkForReturnableItems(selectableIds: Array<string>, notSelectableIds: Array<string>): void {
    if (selectableIds.length > 0 && notSelectableIds.length === 0) {
      this.showReturnTooltip = false;
      this.disableRequestReturn = false;
    }
  }

  private checkForEligibleReturnItems(selectableIds: Array<string>, notSelectableIds: Array<string>): void {
    if (selectableIds.length > 0 && notSelectableIds.length > 0) {
      this.orderLinesListing.updateInvalidOrders(notSelectableIds);
      this.isTooltipFixed = false;
      this.showReturnTooltip = true;
      this.requestReturnLabel = 'Request a Return/QI for Eligible Items';
      this.requestReturnTooltipLabel = 'Some items not eligible for return/quality issue';
      this.disableRequestReturn = false;
    }
  }

  public isItemOpenedWithZeroQtyShipped(orderItem: IOrderLine): boolean {
    return orderItem.status === IOrderLineStatus.Open && (orderItem.qtyShipped === 0 || orderItem.qtyShipped === undefined);
  }

  public isItemValidForReturn(orderItem: IOrderLine): boolean {
    return (
      (orderItem.status === IOrderLineStatus.Closed ||
        orderItem.status === IOrderLineStatus.Shipped ||
        (orderItem.status === IOrderLineStatus.Open && orderItem.qtyShipped > 0)) &&
      moment(orderItem.shipmentTrackingDate).isAfter(moment().subtract(6, 'M')) &&
      !orderItem.ncnrStatus.valueOf()
    );
  }

  public requestToReturn(): void {
    this.store.dispatch(new RequestReturnOrder());

    this.router.navigateByUrl('/orders/return');
  }
}
