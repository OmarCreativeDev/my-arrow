import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { OrderDetailsComponent } from './order-details.component';
import { OrderDetailsMetaComponent } from '../../components/order-details-meta/order-details-meta.component';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { SingleOrderLinesListingComponent } from '@app/features/orders/components/single-order-lines-listing/single-order-lines-listing.component';
import { SharedModule } from '@app/shared/shared.module';
import { ShipmentTrackingDialogComponent } from '@app/features/orders/components/shipment-tracking-dialog/shipment-tracking-dialog.component';
import { BackdropServiceMock, ModalServiceMock } from '@app/app.component.spec';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { Store, StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { orderDetailsReducers } from '@app/features/orders/stores/order-details/order-details.reducers';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DateService } from '@app/shared/services/date.service';
import { DatePipe } from '@angular/common';
import { InvoiceListingDialogComponent } from '@app/features/orders/components/invoice-listing-dialog/invoice-listing-dialog.component';
import { ApiService } from '@app/core/api/api.service';
import { RouterTestingModule } from '@angular/router/testing';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { userReducers } from '@app/core/user/store/user.reducers';
import { IOrderLineStatus, IPurchaseOrderStatus, IOrderLine, IOrder } from '@app/shared/shared.interfaces';
import { IAddToQuoteCartRequestItem } from '@app/core/quote-cart/quote-cart.interfaces';
import { AddToCartDialogComponent, CartType } from '@app/shared/components/add-to-cart-dialog/add-to-cart-dialog.component';
import * as moment from 'moment';
import { IAddToCartRequestItem } from '@app/core/cart/cart.interfaces';
import { ToggleOrderLines } from '@app/features/orders/stores/order-details/order-details.actions';
import { ModalService } from '@app/shared/services/modal.service';
import { quoteCartReducers } from '@app/features/quotes/stores/quote-cart.reducers';
import { cartReducers } from '@app/features/cart/stores/cart/cart.reducers';
import { RequestReturnOrder } from '../../stores/orders/orders.actions';
import { PropertiesEffects } from '@app/features/properties/store/properties.effects';
import { mockPrivateProperties } from '@app/core/features/feature-flags.mock';
import { propertiesReducers } from '@app/features/properties/store/properties.reducers';
import { PropertiesModule } from '@app/features/properties/properties.module';

class ActivatedRouteMock {
  public get params() {
    return of([]);
  }

  public get queryParams() {
    return of([]);
  }
}

class MockApiService {
  public get() {
    return of();
  }
}

class MockRouter {
  navigateByUrl(url: string) {
    return url;
  }
}

const selectedOrderLine: IOrderLine = {
  id: 1234,
  purchaseOrderNumber: 'PO1234',
  purchaseOrderStatus: IPurchaseOrderStatus.Open,
  lineItem: '1.2.1',
  customerPartNumber: 'BAV99',
  manufacturerPartNumber: 'BAV99',
  manufacturerName: 'Vishay',
  qtyOrdered: 500,
  qtyShipped: 200,
  qtyReadyToShip: 200,
  qtyRemaining: 100,
  requested: new Date(),
  entered: new Date(),
  committed: new Date(),
  status: IOrderLineStatus.Open,
  buyerName: 'David Lane',
  salesOrderId: 'ASDF',
  salesHeaderId: 7438563,
  billToSiteUseId: 1234,
  unitResale: 0,
  extResale: 0,
  currencyCode: 'USD',
  docId: '555_123456',
  itemId: 123456,
  warehouseId: 555,
  shipments: [],
  invoices: [],
  ncnrStatus: false,
};

const mockPropertiesReducers = () => ({
  ...propertiesReducers,
  private: mockPrivateProperties,
});

describe('OrderDetailsComponent', () => {
  const orderLinesListing = jasmine.createSpyObj('SingleOrderLinesListingComponent', ['updateInvalidOrders']);
  let component: OrderDetailsComponent;
  let fixture: ComponentFixture<OrderDetailsComponent>;
  let dialogService: DialogService;
  let store: Store<any>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        StoreModule.forRoot({
          orderDetails: orderDetailsReducers,
          user: userReducers,
          quoteCart: quoteCartReducers,
          cart: cartReducers,
          properties: mockPropertiesReducers,
        }),
        EffectsModule.forRoot([PropertiesEffects]),
        RouterTestingModule,
        HttpClientTestingModule,
        PropertiesModule,
      ],
      declarations: [
        OrderDetailsComponent,
        OrderDetailsMetaComponent,
        SingleOrderLinesListingComponent,
        ShipmentTrackingDialogComponent,
        InvoiceListingDialogComponent,
      ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteMock },
        { provide: BackdropService, useClass: BackdropServiceMock },
        { provide: ApiService, useClass: MockApiService },
        { provide: DialogService, useClass: DialogServiceMock },
        { provide: ModalService, useClass: ModalServiceMock },
        { provide: Router, useClass: MockRouter },
        DatePipe,
        DateService,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(OrderDetailsComponent);
    dialogService = TestBed.get(DialogService);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.get(Router);
    spyOn(router, 'navigateByUrl').and.callThrough();
    jasmine.clock().uninstall();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call `dialogService` on `addToCart()', () => {
    jasmine.clock().install();

    const addToCartRequestItem: IAddToCartRequestItem = {
      manufacturerPartNumber: 'BAV99',
      docId: '555_123456',
      itemId: 123456,
      warehouseId: 555,
      quantity: 500,
      requestDate: moment(new Date()).format('YYYY-MM-DD'),
      selectedCustomerPartNumber: 'BAV99',
    };

    spyOn(dialogService, 'open').and.callThrough();
    component.cartRemainingLineItems = 100;
    component.addToCart([selectedOrderLine]);
    expect(dialogService.open).toHaveBeenCalledWith(AddToCartDialogComponent, {
      data: {
        cartType: CartType.SHOPPING_CART,
        items: [addToCartRequestItem],
      },
    });

    jasmine.clock().uninstall();
  });

  it('should call `dialogService` on `addToQuoteCart()', () => {
    jasmine.clock().install();

    const addToQuoteCartRequestItem: IAddToQuoteCartRequestItem = {
      docId: '555_123456',
      itemId: 123456,
      warehouseId: 555,
      quantity: 500,
      requestDate: moment(new Date()).format('YYYY-MM-DD'),
      selectedCustomerPartNumber: 'BAV99',
      targetPrice: 0,
      manufacturerPartNumber: 'BAV99',
    };

    spyOn(dialogService, 'open').and.callThrough();
    component.quoteCartRemainingLineItems = 100;
    component.addToQuoteCart([selectedOrderLine]);
    expect(dialogService.open).toHaveBeenCalledWith(AddToCartDialogComponent, {
      data: {
        cartType: CartType.QUOTE_CART,
        items: [addToQuoteCartRequestItem],
      },
    });

    jasmine.clock().uninstall();
  });

  it('should call `dialogService` on `showInvoicesForOrderLines()', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.showInvoicesForOrderLines([selectedOrderLine]);
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should call `dialogService` on `showTrackingDetailsForOrder()', () => {
    const order: IOrder = component.order;
    spyOn(dialogService, 'open').and.callThrough();
    component.showTrackingDetailsForOrder(order);
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should call `dialogService` on `openEmailRepresentativeDialog()', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.openEmailRepresentativeDialog();
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('#onIdSelected should map the provided orderLines to an array of Ids and dispatch a idSelected action to the store', () => {
    const action = new ToggleOrderLines([component.orderId], false);
    component.orderLinesListing = orderLinesListing;
    component.onIdSelected(action);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('should call `openAddToCartCapDialog` when addToQuoteCart and max cap has been reached', () => {
    spyOn(component, 'openAddToCartCapDialog').and.callThrough();
    component.quoteCartRemainingLineItems = 0;
    component.addToQuoteCart([selectedOrderLine]);
    expect(component.openAddToCartCapDialog).toHaveBeenCalled();
  });

  it('#requestToReturn should dispatch a request return action & navigate to return page', () => {
    const action = new RequestReturnOrder();
    component.requestToReturn();
    expect(store.dispatch).toHaveBeenCalledWith(action);
    expect(router.navigateByUrl).toHaveBeenCalledWith('/orders/return');
  });

  it('should call `openAddToCartCapDialog` when addToCart and max cap has been reached', () => {
    spyOn(component, 'openAddToCartCapDialog').and.callThrough();
    component.cartRemainingLineItems = 0;
    component.addToCart([selectedOrderLine]);
    expect(component.openAddToCartCapDialog).toHaveBeenCalled();
  });

  it('should return true when isItemOpenedWithZeroQtyShipped is called with invalid order', () => {
    const orderLine = { ...selectedOrderLine };
    orderLine.status = IOrderLineStatus.Open;
    orderLine.qtyShipped = 0;
    const result = component.isItemOpenedWithZeroQtyShipped(orderLine);
    expect(result).toBeTruthy();
  });

  it('should return true when isItemValidForReturn is called with order in Closed status and ship date less than 6 months', () => {
    const orderLine = { ...selectedOrderLine };
    orderLine.status = IOrderLineStatus.Closed;
    orderLine.shipmentTrackingDate = moment()
      .subtract(2, 'M')
      .toDate();
    const result = component.isItemValidForReturn(orderLine);
    expect(result).toBeTruthy();
  });

  it('should return true when isItemValidForReturn is called with order in Shipped status and ship date less than 6 months', () => {
    const orderLine = { ...selectedOrderLine };
    orderLine.status = IOrderLineStatus.Shipped;
    orderLine.shipmentTrackingDate = moment()
      .subtract(5, 'M')
      .toDate();
    const result = component.isItemValidForReturn(orderLine);
    expect(result).toBeTruthy();
  });

  it('should return false when isItemValidForReturn is called with order in Opened status and ship date more than 6 months', () => {
    const orderLine = { ...selectedOrderLine };
    orderLine.status = IOrderLineStatus.Open;
    orderLine.shipmentTrackingDate = moment()
      .subtract(8, 'M')
      .toDate();
    const result = component.isItemValidForReturn(orderLine);
    expect(result).toBeFalsy();
  });

  describe('should update Request Return fields when validating item orders as:', () => {
    let selectedOrder1: IOrderLine;
    let selectedOrder2: IOrderLine;

    beforeEach(() => {
      selectedOrder1 = { ...selectedOrderLine };
      selectedOrder2 = { ...selectedOrderLine };
      component.requestReturnLabel = 'Request a Return';
      component.disableRequestReturn = true;
      component.requestReturnTooltipLabel = '';
      component.showReturnTooltip = false;
      component.isTooltipFixed = false;
      component.orderLinesListing = orderLinesListing;
    });

    it('Request Return/Quality Issue button is enabled when single selected line order is valid', () => {
      selectedOrder1.status = IOrderLineStatus.Shipped;
      selectedOrder1.shipmentTrackingDate = moment()
        .subtract(3, 'M')
        .toDate();
      component.selectedOrderLines = [selectedOrder1];
      component.validateReturnableItems();
      expect(component.disableRequestReturn).toBeFalsy();
    });

    it('Request Return/Quality Issue button is enabled when multiple selected line order is valid', () => {
      selectedOrder1.status = IOrderLineStatus.Shipped;
      selectedOrder1.shipmentTrackingDate = moment()
        .subtract(3, 'M')
        .toDate();
      selectedOrder2.status = IOrderLineStatus.Closed;
      selectedOrder2.shipmentTrackingDate = moment()
        .subtract(5, 'M')
        .toDate();
      component.selectedOrderLines = [selectedOrder1, selectedOrder2];
      component.validateReturnableItems();
      expect(component.disableRequestReturn).toBeFalsy();
    });

    it('Request Return/Quality Issue button is disabled and tooltip shown on hover with "Request a Return/QI is only available for Shipped Status"', () => {
      selectedOrder1.status = IOrderLineStatus.Open;
      selectedOrder1.qtyShipped = 0;
      selectedOrder2.status = IOrderLineStatus.Open;
      selectedOrder2.qtyShipped = 0;
      component.selectedOrderLines = [selectedOrder1, selectedOrder2];
      component.validateReturnableItems();
      expect(component.disableRequestReturn).toBeTruthy();
      expect(component.showReturnTooltip).toBeTruthy();
      expect(component.isTooltipFixed).toBeFalsy();
      expect(component.requestReturnTooltipLabel).toBe('Request a Return/QI is only available for Shipped Status');
    });

    it('Request Return/Quality Issue button is disabled and tooltip shown on hover with "Request a Return/QI is only available for Shipped Status" when quatity shipped is not present', () => {
      selectedOrder1.status = IOrderLineStatus.Open;
      selectedOrder1.qtyShipped = undefined;
      selectedOrder2.status = IOrderLineStatus.Open;
      selectedOrder2.qtyShipped = 0;
      component.selectedOrderLines = [selectedOrder1, selectedOrder2];
      component.validateReturnableItems();
      expect(component.disableRequestReturn).toBeTruthy();
      expect(component.showReturnTooltip).toBeTruthy();
      expect(component.isTooltipFixed).toBeFalsy();
      expect(component.requestReturnTooltipLabel).toBe('Request a Return/QI is only available for Shipped Status');
    });

    it('Request Return/Quality Issue button is enabled and tooltip shown on hover with "Some items not eligible for return/quality issue" when at least one item is opened and zero quantity shipped', () => {
      selectedOrder1.status = IOrderLineStatus.Shipped;
      selectedOrder1.shipmentTrackingDate = moment()
        .subtract(3, 'M')
        .toDate();
      selectedOrder2.status = IOrderLineStatus.Open;
      selectedOrder2.qtyShipped = 0;
      component.selectedOrderLines = [selectedOrder1, selectedOrder2];
      component.validateReturnableItems();
      expect(component.disableRequestReturn).toBeFalsy();
      expect(component.showReturnTooltip).toBeTruthy();
      expect(component.isTooltipFixed).toBeFalsy();
      expect(component.requestReturnLabel).toBe('Request a Return/QI for Eligible Items');
      expect(component.requestReturnTooltipLabel).toBe('Some items not eligible for return/quality issue');
    });

    it('Request Return/Quality Issue button is enabled and tooltip shown on hover with "Some items not eligible for return" when at least one item ship date > 6 months', () => {
      selectedOrder1.status = IOrderLineStatus.Shipped;
      selectedOrder1.shipmentTrackingDate = moment()
        .subtract(3, 'M')
        .toDate();
      selectedOrder2.status = IOrderLineStatus.Shipped;
      selectedOrder2.shipmentTrackingDate = moment()
        .subtract(8, 'M')
        .toDate();
      component.selectedOrderLines = [selectedOrder1, selectedOrder2];
      component.validateReturnableItems();
      expect(component.disableRequestReturn).toBeFalsy();
      expect(component.showReturnTooltip).toBeTruthy();
      expect(component.isTooltipFixed).toBeFalsy();
      expect(component.requestReturnLabel).toBe('Request a Return/QI for Eligible Items');
      expect(component.requestReturnTooltipLabel).toBe('Some items not eligible for return/quality issue');
    });

    it('Request Return/Quality Issue button is enabled and tooltip shown on hover with "Some items not eligible for return/quality issue" when at least one item ship date > 6 months', () => {
      selectedOrder1.status = IOrderLineStatus.Shipped;
      selectedOrder1.shipmentTrackingDate = moment()
        .subtract(3, 'M')
        .toDate();
      selectedOrder2.status = IOrderLineStatus.Shipped;
      selectedOrder2.ncnrStatus = true;
      component.selectedOrderLines = [selectedOrder1, selectedOrder2];
      component.validateReturnableItems();
      expect(component.disableRequestReturn).toBeFalsy();
      expect(component.showReturnTooltip).toBeTruthy();
      expect(component.isTooltipFixed).toBeFalsy();
      expect(component.requestReturnLabel).toBe('Request a Return/QI for Eligible Items');
      expect(component.requestReturnTooltipLabel).toBe('Some items not eligible for return/quality issue');
    });

    it('Request Return/Quality Issue button is disabled and the tooltip shown on hover with "Items not eligible for return/quality issue" since all items are invalid', () => {
      selectedOrder1.status = IOrderLineStatus.Open;
      selectedOrder1.qtyShipped = 0;
      selectedOrder2.status = IOrderLineStatus.Shipped;
      selectedOrder2.shipmentTrackingDate = moment()
        .subtract(8, 'M')
        .toDate();
      component.selectedOrderLines = [selectedOrder1, selectedOrder2];
      component.validateReturnableItems();
      expect(component.disableRequestReturn).toBeTruthy();
      expect(component.showReturnTooltip).toBeTruthy();
      expect(component.isTooltipFixed).toBeFalsy();
      expect(component.requestReturnTooltipLabel).toBe('Items not eligible for return/quality issue');
    });
  });
});
