import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrdersPageComponent } from './pages/orders/orders-page.component';
import { OrderDetailsComponent } from '@app/features/orders/pages/order-details/order-details.component';
import { OrderReturnsComponent } from '@app/features/orders/pages/order-returns/order-returns.component';

const routes: Routes = [
  { path: '', component: OrdersPageComponent, data: { title: 'Orders' } },
  { path: 'return', component: OrderReturnsComponent, data: { title: 'Returns / FQR' } },
  { path: ':orderId', component: OrderDetailsComponent, data: { title: 'View Order' } },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrdersRoutingModule {}
