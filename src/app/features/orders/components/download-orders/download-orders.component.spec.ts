import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogData, DownloadOrdersComponent } from './download-orders.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from '@app/shared/shared.module';
import { OrdersService } from '@app/core/orders/orders.service';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { of, throwError } from 'rxjs';
import * as FileSaver from 'file-saver';
import { IFileTypeEnum, ISortableItem } from '@app/shared/shared.interfaces';
import { HttpHeaders } from '@angular/common/http';
import { userReducers } from '@app/core/user/store/user.reducers';
import { combineReducers, Store, StoreModule } from '@ngrx/store';

class MockOrdersService {
  public downloadOrderLines() {}
}

describe('DownloadOrdersComponent', () => {
  let component: DownloadOrdersComponent;
  let fixture: ComponentFixture<DownloadOrdersComponent>;
  let ordersService: OrdersService;
  let dialog: Dialog<DownloadOrdersComponent>;
  let store: Store<any>;

  const search = { value: 'aaa', type: 'aaa' };
  const filter = { value: 0, rules: [] };
  const sort = [];
  const data: DialogData = { search, filter, sort };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DownloadOrdersComponent],
      imports: [
        SharedModule,
        HttpClientTestingModule,
        StoreModule.forRoot({
          orders: combineReducers(userReducers),
        }),
      ],
      providers: [
        { provide: OrdersService, useClass: MockOrdersService },
        { provide: Dialog, useClass: DialogMock },
        { provide: APP_DIALOG_DATA, useValue: data },
      ],
    }).compileComponents();
    ordersService = TestBed.get(OrdersService);
    dialog = TestBed.get(Dialog);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the dialog when Cancel button is pressed', () => {
    spyOn(dialog, 'close');
    component.onClose();
    expect(dialog.close).toHaveBeenCalled();
  });

  describe('#canDownload', () => {
    const testCases = [
      {
        description: 'should return true if there are selectedColumns',
        selectedColumns: ['1', '2'],
        result: true,
      },
      {
        description: 'should return false if there are no selectedColumns',
        selectedColumns: [],
        result: false,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        component.selectedColumns = testCase.selectedColumns;
        const result = component.canDownload();
        expect(result).toEqual(testCase.result);
      });
    }
  });

  describe('#onDownload', () => {
    it('should call ordersService with search, filters, fileType and selectedColumns and invoke #saveResponse when successful', () => {
      spyOn(ordersService, 'downloadOrderLines').and.callFake(() => of([]));
      spyOn(FileSaver, 'saveAs').and.callFake(() => {});
      spyOn(component, 'saveResponse').and.callThrough();
      const selectedFileType = IFileTypeEnum.XLSX;
      const selectedColumns = [{} as ISortableItem];
      component.selectedFileType = selectedFileType;
      component.selectedColumns = selectedColumns;
      component.onDownload();
      expect(ordersService.downloadOrderLines).toHaveBeenCalledWith({
        search: search,
        filter: filter.rules,
        sort: sort,
        fileType: selectedFileType,
        columns: selectedColumns,
        title: 'Order Summary',
      });
      expect(component.saveResponse).toHaveBeenCalled();
    });

    it('should invoke handleDownloadError if there is an error with the api', () => {
      spyOn(ordersService, 'downloadOrderLines').and.callFake(() => throwError('Error'));
      spyOn(component, 'handleError').and.callThrough();
      component.onDownload();
      expect(component.handleError).toHaveBeenCalled();
    });

    describe('#getFilenameFromHeaders', () => {
      it('should return the value of the filename in the Content-Disposition header', () => {
        const filename = 'dummy-filename.csv';
        const headers = new HttpHeaders({
          'Content-Disposition': `attachment; filename=${filename}`,
        });
        const result = component.getFilenameFromHeaders(headers);
        expect(result).toEqual(filename);
      });

      it('should return an empty string if no filename was found', () => {
        const headers = new HttpHeaders({
          'Content-Disposition': 'attachment',
        });
        const result = component.getFilenameFromHeaders(headers);
        expect(result).toEqual('');
      });

      it(`should return an empty string if no 'Content-Disposition' header was found`, () => {
        const headers = new HttpHeaders({});
        const result = component.getFilenameFromHeaders(headers);
        expect(result).toEqual('');
      });
    });
  });
});
