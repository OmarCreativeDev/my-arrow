import { Component, Inject } from '@angular/core';
import {
  IDownloadColumnTypeEnum,
  IFileTypeEnum,
  IFileTypeOption,
  IFilter,
  ISearchCriteria,
  ISortableItem,
  ISortCriteron,
} from '@app/shared/shared.interfaces';
import { OrdersService } from '@app/core/orders/orders.service';
import { saveAs } from 'file-saver';
import { HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';

export interface DialogData {
  search: ISearchCriteria;
  filter: IFilter;
  sort: Array<ISortCriteron>;
}

@Component({
  selector: 'app-download-orders',
  templateUrl: './download-orders.component.html',
  styleUrls: ['./download-orders.component.scss'],
})
export class DownloadOrdersComponent {
  public fileTypeOptions: Array<IFileTypeOption> = [
    {
      label: '.xls',
      value: IFileTypeEnum.XLS,
    },
    {
      label: '.xlsx',
      value: IFileTypeEnum.XLSX,
    },
    {
      label: '.csv',
      value: IFileTypeEnum.CSV,
    },
  ];

  public selectedFileType = this.fileTypeOptions[0].value;

  // TODO: `label` strings should be localizable
  public columns: Array<ISortableItem> = [
    {
      id: 0,
      label: 'Customer P.O Number',
      properties: ['purchaseOrderNumber'],
      type: IDownloadColumnTypeEnum.String,
    },
    {
      id: 1,
      label: 'Buyer',
      properties: ['buyerName'],
      type: IDownloadColumnTypeEnum.String,
    },
    {
      id: 2,
      label: 'P.O. Status',
      properties: ['purchaseOrderStatus'],
      type: IDownloadColumnTypeEnum.String,
    },
    {
      id: 3,
      label: 'Customer Part Number',
      properties: ['customerPartNumber'],
      type: IDownloadColumnTypeEnum.String,
    },
    {
      id: 4,
      label: 'Manufacturer Part Number',
      properties: ['manufacturerPartNumber'],
      type: IDownloadColumnTypeEnum.String,
    },
    {
      id: 5,
      label: 'Manufacturer Name',
      properties: ['manufacturerName'],
      type: IDownloadColumnTypeEnum.String,
    },
    {
      id: 6,
      label: 'Date Entered',
      type: IDownloadColumnTypeEnum.Date,
      properties: ['entered'],
      format: 'YYYY-MM-DD',
    },
    {
      id: 7,
      label: 'Request Date',
      type: IDownloadColumnTypeEnum.Date,
      properties: ['requested'],
      format: 'YYYY-MM-DD',
    },
    {
      id: 8,
      label: 'Commit Date',
      type: IDownloadColumnTypeEnum.Date,
      properties: ['committed'],
      format: 'YYYY-MM-DD',
    },
    {
      id: 9,
      label: 'Quantity Ordered',
      type: IDownloadColumnTypeEnum.String,
      properties: ['qtyOrdered'],
    },
    {
      id: 10,
      label: 'Quantity Shipped',
      type: IDownloadColumnTypeEnum.String,
      properties: ['qtyShipped'],
    },
    {
      id: 11,
      label: 'Quantity Ready to Ship',
      type: IDownloadColumnTypeEnum.String,
      properties: ['qtyReadyToShip'],
    },
    {
      id: 12,
      label: 'Quantity Remaining',
      type: IDownloadColumnTypeEnum.String,
      properties: ['qtyRemaining'],
    },
    {
      id: 13,
      label: 'Item Status',
      type: IDownloadColumnTypeEnum.String,
      properties: ['status'],
    },
    {
      id: 14,
      label: 'Ship Date',
      type: IDownloadColumnTypeEnum.Date,
      properties: ['shipmentTrackingDate'],
      format: 'YYYY-MM-DD',
    },
    {
      id: 15,
      label: 'Tracking Number',
      type: IDownloadColumnTypeEnum.Url,
      value: 'shipmentTrackingReference',
      url: 'shipmentTrackingUrl',
    },
    {
      id: 16,
      label: 'Ship Method',
      type: IDownloadColumnTypeEnum.String,
      properties: ['carrier'],
    },
    {
      id: 17,
      label: 'Sales Order / Line Item',
      type: IDownloadColumnTypeEnum.String,
      properties: ['salesOrderId', 'lineItem'],
      format: '{0} / {1}',
    },
    {
      id: 18,
      label: 'Unit Resale',
      type: IDownloadColumnTypeEnum.String,
      properties: ['unitResale'],
    },
    {
      id: 19,
      label: 'Extended Resale',
      type: IDownloadColumnTypeEnum.String,
      properties: ['extResale'],
    },
  ];
  public selectedColumns = [];
  public downloadProcessing = false;
  public downloadError = undefined;

  constructor(
    private ordersService: OrdersService,
    private dialog: Dialog<DownloadOrdersComponent>,
    @Inject(APP_DIALOG_DATA) private data: DialogData
  ) {}

  public onClose(): void {
    this.dialog.close();
  }

  public onDownload(): void {
    this.downloadProcessing = true;
    // TODO: `title` string should be localizable
    this.ordersService
      .downloadOrderLines({
        search: this.data.search,
        filter: this.data.filter.rules,
        sort: this.data.sort,
        fileType: this.selectedFileType as IFileTypeEnum,
        columns: this.selectedColumns,
        title: 'Order Summary',
      })
      .subscribe((response: HttpResponse<Blob>) => this.saveResponse(response), (err: HttpErrorResponse) => this.handleError(err));
  }

  public saveResponse(response: HttpResponse<Blob>): void {
    this.downloadProcessing = false;
    const filename = this.getFilenameFromHeaders(response.headers);
    saveAs(response.body, filename);
  }

  public getFilenameFromHeaders(headers: HttpHeaders) {
    // From https://shekhargulati.com/2017/07/16/implementing-file-save-functionality-with-angular-4/
    const contentDisposition = {};
    if (headers) {
      const contentDispositionHeader: string = headers.get('Content-Disposition');
      if (contentDispositionHeader) {
        const contentDispositionParts: Array<string> = contentDispositionHeader.split(';');
        for (const contentDispositionPart of contentDispositionParts) {
          const keyValuePair = contentDispositionPart.split('=');
          const key = keyValuePair[0].trim();
          const value = (keyValuePair[1] || '').trim();
          contentDisposition[key] = value;
        }
      }
    }
    return contentDisposition['filename'] || '';
  }

  public handleError(err: HttpErrorResponse): void {
    this.downloadError = HttpErrorResponse;
    this.downloadProcessing = false;
  }

  public canDownload(): boolean {
    return this.selectedColumns.length > 0;
  }
}
