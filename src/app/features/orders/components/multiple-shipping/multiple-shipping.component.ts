import { Component, Inject } from '@angular/core';

import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { IOrderLine } from '@app/shared/shared.interfaces';

@Component({
  selector: 'app-multiple-shipping',
  templateUrl: './multiple-shipping.component.html',
  styleUrls: ['./multiple-shipping.component.scss'],
})
export class MultipleShippingComponent {
  constructor(private dialog: Dialog<MultipleShippingComponent>, @Inject(APP_DIALOG_DATA) public orderLine: IOrderLine) {}

  onClose(): void {
    this.dialog.close();
  }
}
