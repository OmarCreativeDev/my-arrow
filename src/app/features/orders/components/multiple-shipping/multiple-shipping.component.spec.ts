import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { MultipleShippingComponent } from './multiple-shipping.component';

import { Dialog, APP_DIALOG_DATA } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';

import { DatePipe } from '@angular/common';
import { IsoDatePipe } from '@app/shared/pipes/iso-date/iso-date.pipe';
import { DateService } from '@app/shared/services/date.service';

import { IOrderLine, IPurchaseOrderStatus, IOrderLineStatus } from '@app/shared/shared.interfaces';

const selectedOrderLine: IOrderLine = {
  id: 1234,
  purchaseOrderNumber: 'PO1234',
  purchaseOrderStatus: IPurchaseOrderStatus.Open,
  lineItem: '1.2.1',
  customerPartNumber: 'BAV99',
  manufacturerPartNumber: 'BAV99',
  manufacturerName: 'Vishay',
  qtyOrdered: 500,
  qtyShipped: 200,
  qtyReadyToShip: 200,
  qtyRemaining: 100,
  requested: new Date(),
  entered: new Date(),
  committed: new Date(),
  status: IOrderLineStatus.Open,
  buyerName: 'David Lane',
  salesOrderId: 'ASDF',
  salesHeaderId: 7453868,
  billToSiteUseId: 1234,
  unitResale: 0,
  extResale: 0,
  currencyCode: 'USD',
  docId: '555_123456',
  itemId: 123456,
  warehouseId: 555,
  shipments: [],
  invoices: [],
};

class DateServiceMock {
  getMsForDays() {
    return 1000;
  }
  getDate() {
    return '';
  }
}

describe('MultipleShippingComponent', () => {
  let component: MultipleShippingComponent;
  let fixture: ComponentFixture<MultipleShippingComponent>;
  let dialog: Dialog<MultipleShippingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MultipleShippingComponent, IsoDatePipe],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: Dialog, useClass: DialogMock },
        { provide: APP_DIALOG_DATA, useValue: { data: selectedOrderLine } },
        DatePipe,
        { provide: DateService, useClass: DateServiceMock },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleShippingComponent);
    component = fixture.componentInstance;
    dialog = TestBed.get(Dialog);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onClose', () => {
    spyOn(dialog, 'close');
    component.onClose();
    expect(dialog.close).toHaveBeenCalled();
  });
});
