import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleOrderLinesListingComponent } from './single-order-lines-listing.component';
import { SharedModule } from '@app/shared/shared.module';
import { DatePipe } from '@angular/common';
import { DateService } from '@app/shared/services/date.service';
import { ToggleOrderLines } from '@app/features/orders/stores/order-details/order-details.actions';
import { RouterTestingModule } from '@angular/router/testing';
import { IOrderLine, IOrderLineStatus, IPurchaseOrderStatus } from '@app/shared/shared.interfaces';

const dummyOrderLine: IOrderLine = {
  id: 1234,
  purchaseOrderNumber: 'PO1234',
  purchaseOrderStatus: IPurchaseOrderStatus.Open,
  lineItem: '1.2.1',
  customerPartNumber: 'BAV99',
  manufacturerPartNumber: 'BAV99',
  manufacturerName: 'Vishay',
  qtyOrdered: 500,
  qtyShipped: 200,
  qtyReadyToShip: 200,
  qtyRemaining: 100,
  requested: new Date(),
  entered: new Date(),
  committed: new Date(),
  status: IOrderLineStatus.Open,
  buyerName: 'David Lane',
  salesOrderId: 'ASDF',
  salesHeaderId: 7453868,
  billToSiteUseId: 1234,
  unitResale: 0,
  extResale: 0,
  currencyCode: 'USD',
  docId: '555_123456',
  itemId: 123456,
  warehouseId: 555,
  shipments: [],
  invoices: [],
};

describe('SingleOrderLinesListingComponent', () => {
  let component: SingleOrderLinesListingComponent;
  let fixture: ComponentFixture<SingleOrderLinesListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SingleOrderLinesListingComponent],
      providers: [DatePipe, DateService],
      imports: [SharedModule, RouterTestingModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleOrderLinesListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#isSelected', () => {
    it('should return true if the provided orderLine is in the selection', () => {
      const salesOrderId = 'xxx';
      const lineItem = '1.2.3';
      const orderLine = { salesOrderId, lineItem };
      const orderLineId = `${salesOrderId}_${lineItem}`;
      const result = component.isSelected(orderLine as any, [orderLineId]);
      expect(result).toBeTruthy();
    });
    it('should return false if the provided orderLine is not in the selection', () => {
      const salesOrderId = 'xxx';
      const lineItem = '1.2.3';
      const orderLine = { salesOrderId, lineItem };
      const result = component.isSelected(orderLine as any, []);
      expect(result).toBeFalsy();
    });
  });

  it('#toggleOrderLines should map the provided orderLines to an array of Ids and dispatch a idSelected action to the parent', () => {
    spyOn(component.idSelected, 'emit');
    const salesOrderId = 'xxx';
    const lineItem = '1.2.3';
    const orderLineId = `${salesOrderId}_${lineItem}`;
    const orderLine = { salesOrderId, lineItem };
    const orderLines = [orderLine];
    const operation = false;
    const action = new ToggleOrderLines([orderLineId], operation);
    component.toggleOrderLines(operation, orderLines as Array<any>);
    expect(component.idSelected.emit).toHaveBeenCalledWith(action);
  });

  it('should deselect all the order lines', () => {
    spyOn(component.idSelected, 'emit');

    const orderLines: Array<IOrderLine> = [dummyOrderLine];
    component.orderLines = orderLines;
    component.deselectAll();
    expect(component.idSelected.emit).toHaveBeenCalled();
  });

  it('should update the list of invalid components', () => {
    const invalidOrders: Array<string> = ['123_1', '123_2', '123_3'];
    component.invalidSelectedIds = [];
    component.updateInvalidOrders(invalidOrders);
    expect(component.invalidSelectedIds).toEqual(invalidOrders);
  });

  it('should return true when isOrderInvalid is called and the line order is invalid', () => {
    const orderLine = { ...dummyOrderLine };
    orderLine.salesOrderId = '123';
    orderLine.lineItem = '1';
    component.invalidSelectedIds = ['123_1', '123_2', '123_3'];
    const result = component.isOrderInvalid(orderLine, component.invalidSelectedIds);
    expect(result).toBeTruthy();
  });

  it('should return false when isOrderInvalid is called and the line order IS valid', () => {
    const orderLine = { ...dummyOrderLine };
    orderLine.salesOrderId = '123';
    orderLine.lineItem = '4';
    component.invalidSelectedIds = ['123_1', '123_2', '123_3'];
    const result = component.isOrderInvalid(orderLine, component.invalidSelectedIds);
    expect(result).toBeFalsy();
  });
});
