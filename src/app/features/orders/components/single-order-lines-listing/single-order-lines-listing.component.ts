import { Component, EventEmitter, Input, Output } from '@angular/core';
import { IOrderLine } from '@app/shared/shared.interfaces';
import { ToggleOrderLines } from '@app/features/orders/stores/order-details/order-details.actions';
import { Status } from '@app/shared/locale/i18n.maps';

@Component({
  selector: 'app-single-order-lines-listing',
  templateUrl: './single-order-lines-listing.component.html',
  styleUrls: ['./single-order-lines-listing.component.scss'],
})
export class SingleOrderLinesListingComponent {
  @Input()
  public orderLines: IOrderLine[];
  @Input()
  public selectedIds: Array<string>;
  @Input()
  public currencyCode: string;

  @Output()
  public idSelected: EventEmitter<ToggleOrderLines> = new EventEmitter<ToggleOrderLines>();

  public invalidSelectedIds: Array<string> = [];

  public orderLineStatusMap: any = Status.orderLineStatusMap;

  public toggleOrderLines(select: boolean, orderLines: Array<IOrderLine>): void {
    const orderLineIds = orderLines.map(orderLine => {
      return `${orderLine.salesOrderId}_${orderLine.lineItem}`;
    });
    this.idSelected.emit(new ToggleOrderLines(orderLineIds, select));
  }

  public isSelected(orderLine: IOrderLine, selectedIds: Array<string>): boolean {
    return selectedIds.includes(`${orderLine.salesOrderId}_${orderLine.lineItem}`);
  }

  public deselectAll(): void {
    this.toggleOrderLines(false, this.orderLines);
  }

  public updateInvalidOrders(invalidIds: Array<string>): void {
    this.invalidSelectedIds = invalidIds;
  }

  public isOrderInvalid(orderLine: IOrderLine, invalidIds: Array<string>): boolean {
    return invalidIds.includes(`${orderLine.salesOrderId}_${orderLine.lineItem}`);
  }
}
