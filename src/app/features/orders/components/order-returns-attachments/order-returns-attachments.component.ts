import { Component, Input, forwardRef, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, NG_VALIDATORS, ControlValueAccessor, Validator, FormControl } from '@angular/forms';
import { OrdersService } from '@app/core/orders/orders.service';
import { IRmaRequestAttachment, IRmaAttachment, RmaFileTypeEnum, IRmaLineItemFile } from '@app/core/orders/orders.interfaces';
import { IOrderDetails, IOrderLine } from '@app/shared/shared.interfaces';
import { DialogService } from '@app/core/dialog/dialog.service';
import { OrderReturnsDeleteAttachmentDialogComponent } from '@app/features/orders/components/order-returns-delete-attachment-dialog/order-returns-delete-attachment-dialog.component';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-order-returns-attachments',
  templateUrl: './order-returns-attachments.component.html',
  styleUrls: ['./order-returns-attachments.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => OrderReturnsAttachmentsComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => OrderReturnsAttachmentsComponent),
      multi: true,
    },
  ],
})
export class OrderReturnsAttachmentsComponent implements ControlValueAccessor, Validator {
  @Input() orderLine: IOrderLine;
  @Input() orderDetails: IOrderDetails;
  @Input() attachmentTotalSize: number;
  @Input() lineItemHasFocus: boolean;

  @Output() attachedFile: EventEmitter<File>;
  @Output() removedFile: EventEmitter<File>;
  @Output() lineItemFocus: EventEmitter<any>;

  public acceptedFiles: string = '';
  public lineItemFiles: Array<IRmaLineItemFile> = [];
  public isFileSizeInvalid: boolean;
  public isRepeatedFile: boolean;
  public isFileReadingError: boolean;
  public isTotalSizeInvalid: boolean;
  public MAX_SIZE_FILE: number = 12 * 1048576;
  public MAX_TOTAL_SIZE: number = 50 * 1048576;

  private rmaAttachments: Array<IRmaAttachment> = [];

  constructor(private ordersService: OrdersService, private dialogService: DialogService) {
    this.attachedFile = new EventEmitter<File>();
    this.removedFile = new EventEmitter<File>();
    this.lineItemFocus = new EventEmitter<any>();
    this.acceptedFiles = Object.keys(RmaFileTypeEnum)
      .map(key => RmaFileTypeEnum[key])
      .join(', ');
  }

  onChange = (val: any) => {};
  onTouched = () => {};

  get value(): Array<IRmaAttachment> {
    return this.rmaAttachments;
  }

  set value(val: Array<IRmaAttachment>) {
    if (val !== this.rmaAttachments) {
      this.rmaAttachments = val;
      this.onChange(val);
    }
  }

  writeValue(val: Array<IRmaAttachment>): void {
    const value = val || [];
    if (value !== this.rmaAttachments) {
      this.rmaAttachments = value;
      this.onChange(value);
    }
  }

  registerOnChange(fn: (val: any) => void) {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void) {
    this.onTouched = fn;
  }

  public validate(c: FormControl) {
    return this.value.length ? null : { required: true };
  }

  public onFileChange(event: any): void {
    const { files } = event.target;

    this.lineItemFocus.emit(this.orderLine.lineItem);
    this.isFileSizeInvalid = false;
    this.isRepeatedFile = false;
    this.isTotalSizeInvalid = false;
    if (files.length) {
      const file: File = files[0] as File;

      if (this.isValidFile(file)) {
        this.readFile(file);
      }
    }
  }

  private isValidFile(file: File): boolean {
    this.isTotalSizeInvalid = file.size + this.attachmentTotalSize > this.MAX_TOTAL_SIZE;
    this.isFileSizeInvalid = file.size > this.MAX_SIZE_FILE;
    this.isRepeatedFile = Boolean(this.lineItemFiles.find(item => item.file.name === file.name));
    return !this.isFileSizeInvalid && !this.isRepeatedFile && !this.isTotalSizeInvalid;
  }

  public showConfirmRemoveAttachment(index: number): void {
    const deleteDialog = this.dialogService.open(OrderReturnsDeleteAttachmentDialogComponent, {
      data: this.lineItemFiles[index].file,
      size: 'large',
    });

    deleteDialog.afterClosed.pipe(take(1)).subscribe(response => {
      if (response.deleteAttachmentAccepted) {
        this.removeAttachment(index);
      }
    });
  }

  public removeAttachment(index: number): void {
    this.ordersService.removeRmaAttachmentOrderLines(this.value[index]).subscribe(() => {
      this.value = this.value.filter((attachment, i) => index !== i);
      this.removedFile.emit(this.lineItemFiles[index].file);
      this.lineItemFiles.splice(index, 1);
    });
  }

  public hasErrors(): boolean {
    return this.isFileSizeInvalid || this.isRepeatedFile || this.isTotalSizeInvalid || this.isFileReadingError;
  }

  private readFile(file: File): void {
    const fileReader: FileReader = new FileReader();

    this.isFileReadingError = false;
    fileReader.onerror = this.fileOnError.bind(this);
    fileReader.onloadend = this.fileOnLoadEnd.bind(this, file);
    fileReader.readAsDataURL(file);
  }

  public fileOnLoadEnd(file: File, event: ProgressEvent): void {
    const { result } = event.target as FileReader;
    const rmaRequestAttachment: IRmaRequestAttachment = {
      orderId: this.orderDetails.salesOrderId.toString(),
      lineItemId: this.orderLine.lineItem,
      type: 'file',
      size: file.size,
      value: `${file.name}/${result}`,
    };
    const lineItemFile: IRmaLineItemFile = {
      file,
      uploaded: false,
    };

    this.lineItemFiles.push(lineItemFile);
    this.ordersService.loadRmaAttachmentOrderLines(rmaRequestAttachment).subscribe(
      (response: IRmaAttachment) => {
        const attachment: IRmaAttachment = {
          id: response.id,
          name: file.name,
        };

        this.value = [...this.value, attachment];
        lineItemFile.uploaded = true;
        this.attachedFile.emit(file);
      },
      (error: any) => {
        lineItemFile.uploaded = false;
        this.lineItemFiles.pop();
      }
    );
  }

  private fileOnError(event: any): void {
    this.isFileReadingError = true;
  }
}
