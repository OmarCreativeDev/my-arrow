import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { CoreModule } from '@app/core/core.module';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { OrderReturnsAttachmentsComponent } from '@app/features/orders/components/order-returns-attachments/order-returns-attachments.component';
import { IOrderDetails, IOrderLine } from '@app/shared/shared.interfaces';
import { IRmaAttachment, IRmaLineItemFile, IRmaRequestAttachment } from '@app/core/orders/orders.interfaces';
import { OrdersService } from '@app/core/orders/orders.service';
import { of } from 'rxjs/internal/observable/of';
import { Store, StoreModule } from '@ngrx/store';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';
import { ApiService } from '@app/core/api/api.service';
import { ApiServiceMock } from '@app/core/api/api.service.spec';

describe('OrderReturnsAttachmentsComponent', () => {
  let dialogService: DialogService;
  let ordersService: OrdersService;
  let component: OrderReturnsAttachmentsComponent;
  let fixture: ComponentFixture<OrderReturnsAttachmentsComponent>;
  let store: Store<any>;

  const mockOrderLine = {
    lineItem: 'lineItemTest',
  } as IOrderLine;

  const mockOrderDetails = {
    salesOrderId: 1,
  } as IOrderDetails;

  const mockEevent = {
    target: {
      files: [
        {
          size: 1,
          name: 'testFile.pdf',
        },
      ],
    },
  };

  const mockAttachment = {
    id: 'attachmentTestId',
    name: mockEevent.target.files[0].name,
  } as IRmaAttachment;

  const mockLineItemFile = {
    file: {
      name: mockEevent.target.files[0].name,
    },
    uploaded: true,
  } as IRmaLineItemFile;

  const mockRequestAttachment = {
    orderId: mockOrderDetails.salesOrderId.toString(),
    lineItemId: mockOrderLine.lineItem,
    type: 'file',
  } as IRmaRequestAttachment;

  const mockProgressEvent = {
    lengthComputable: true,
    loaded: null,
    total: null,
    initProgressEvent: () => {},
    ...new Event('progressEvent'),
    target: {} as FileReader,
  } as ProgressEvent;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderReturnsAttachmentsComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [CoreModule, StoreModule.forRoot({})],
      providers: [
        OrdersService,
        { provide: DialogService, useClass: DialogServiceMock },
        { provide: WSSService, useClass: MockStompWSSService },
        { provide: ApiService, useClass: ApiServiceMock },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderReturnsAttachmentsComponent);
    component = fixture.componentInstance;
    dialogService = TestBed.get(DialogService);
    ordersService = TestBed.get(OrdersService);
    component.orderLine = mockOrderLine;
    component.orderDetails = mockOrderDetails;
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should validate the size of the file', () => {
    const event = mockEevent;
    event.target.files[0].size = 2 * component.MAX_SIZE_FILE;
    component.onFileChange(event);
    expect(component.isFileSizeInvalid).toBeTruthy();
  });

  it('should validate the file is not repeated', () => {
    component.lineItemFiles = [
      {
        file: {
          name: mockEevent.target.files[0].name,
        },
        uploaded: true,
      } as IRmaLineItemFile,
    ];
    component.onFileChange(mockEevent);
    expect(component.isRepeatedFile).toBeTruthy();
  });

  it('should validate the total size of the files', () => {
    const event = mockEevent;
    event.target.files[0].size = component.MAX_SIZE_FILE;
    component.attachmentTotalSize = 4 * component.MAX_SIZE_FILE;
    component.onFileChange(event);
    fixture.detectChanges();
    expect(component.isTotalSizeInvalid).toBeTruthy();
  });

  it('should show error messages only if the line item has focus', () => {
    component.isFileSizeInvalid = true;
    component.lineItemHasFocus = true;
    fixture.detectChanges();
    expect(fixture.debugElement.query(By.css('.order-returns-items__attachments-file-error'))).not.toBeNull();
  });

    it('should attach a file when has been loaded', fakeAsync(() => {
    const text = 'test content';
    const data = new Blob([text], { type: 'text/plain' });
    const file = new File([data], 'test.txt');
    const result = new Buffer(text).toString('base64');
    const progressEvent = {
      ...mockProgressEvent,
      loaded: file.size,
      total: file.size,
      target: {
        result,
      } as FileReader,
    };
    mockRequestAttachment.size = file.size;
    mockRequestAttachment.value = `${file.name}/${result}`;
    spyOn(ordersService, 'loadRmaAttachmentOrderLines').and.callFake(() => of(mockAttachment));
    component.fileOnLoadEnd(file, progressEvent);
    expect(ordersService.loadRmaAttachmentOrderLines).toHaveBeenCalledWith(mockRequestAttachment);
    expect(component.value.length).toBe(1);
    expect(component.lineItemFiles.length).toBe(1);
  }));

  it('should show a confirmation dialog when removing an attachment ', fakeAsync(() => {
    component.lineItemFiles = [mockLineItemFile];
    spyOn(dialogService, 'open').and.callThrough();
    component.showConfirmRemoveAttachment(0);
    expect(dialogService.open).toHaveBeenCalled();
  }));

  it('should remove the attachment when calling the removeAttachment method', fakeAsync(() => {
    component.lineItemFiles = [mockLineItemFile];
    component.value = [mockAttachment];
    spyOn(ordersService, 'removeRmaAttachmentOrderLines').and.callFake(() => of({}));
    component.removeAttachment(0);
    expect(ordersService.removeRmaAttachmentOrderLines).toHaveBeenCalledWith(mockAttachment);
    expect(component.value.length).toBe(0);
    expect(component.lineItemFiles.length).toBe(0);
  }));
});
