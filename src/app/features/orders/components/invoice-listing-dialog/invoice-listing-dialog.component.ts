import { Component, ChangeDetectorRef, Inject } from '@angular/core';
import { IOrderLine, IInvoice } from '@app/shared/shared.interfaces';
import { uniq, difference, pullAll } from 'lodash';
import { environment } from '@env/environment';
import { FileService } from '@app/shared/services/file.service';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';

export interface IInvoiceListingDialogDialogData {
  orderLines: Array<IOrderLine>;
}

@Component({
  selector: 'app-invoice-listing',
  templateUrl: './invoice-listing-dialog.component.html',
  styleUrls: ['./invoice-listing-dialog.component.scss'],
})
export class InvoiceListingDialogComponent {
  public selectedIds = [];
  public invoices: Array<IInvoice> = [];
  public openInvoiceError: Error = undefined;

  constructor(
    public dialog: Dialog<InvoiceListingDialogComponent>,
    @Inject(APP_DIALOG_DATA) public data: IInvoiceListingDialogDialogData,
    private changeDetector: ChangeDetectorRef,
    private fileService: FileService
  ) {
    if (data.orderLines && data.orderLines.length) {
      data.orderLines.forEach(orderLine => {
        if (orderLine.invoices && orderLine.invoices.length) {
          orderLine.invoices.forEach(invoice => {
            this.invoices.push(invoice);
          });
        }
      });
      if (this.invoices.length === 1) {
        this.toggleLine(this.invoices[0], true);
      }
    }
  }

  public get currencyCode(): string {
    return this.data.orderLines[0].currencyCode;
  }

  /**
   * Determines if all lines have been selected
   */
  public allSelected() {
    if (this.invoices && this.invoices.length) {
      const invoiceIds = this.getAllLineIds();
      return difference(invoiceIds, this.selectedIds).length <= 0;
    } else {
      return false;
    }
  }

  /**
   * Determines if an OrderLine is selected
   * @param orderLine the OrderLine to find out the selection status for
   */
  public isSelected(invoice: IInvoice): boolean {
    return this.selectedIds.includes(invoice.number);
  }

  /**
   * Toggles an OrderLine selection
   * @param orderLine the OrderLine to toggle
   * @param isChecked whether the OrderLine should be selected or unselected
   */
  public toggleLine(invoice: IInvoice, isChecked: boolean): void {
    this.toggleLineIds([invoice.number], isChecked);
  }

  /**
   * Toggles all OrderLines
   * @param isChecked whether to select or unselected all lines
   */
  public toggleAllLines(isChecked: boolean): void {
    this.toggleLineIds(this.getAllLineIds(), isChecked);
  }

  /**
   * Toggles all lines specified by Id
   * @param lineIds the linedIds to toggle
   * @param isChecked whether to select or unselect the specified lines
   */
  public toggleLineIds(lineIds: Array<string>, isChecked: boolean): void {
    if (isChecked) {
      this.selectedIds = [...this.selectedIds, ...lineIds];
    } else {
      this.selectedIds = pullAll(this.selectedIds, lineIds);
    }
    this.selectedIds = uniq(this.selectedIds);
  }

  /**
   * Gets the Ids of all lines
   */
  public getAllLineIds(): Array<string> {
    return this.invoices.map(invoice => invoice.number);
  }

  public openInvoice(invoice: IInvoice): void {
    this.openInvoiceError = undefined;
    // TODO: take the second parameter from the headers of the response
    this.fileService.getAndOpenFile(`${environment.baseUrls.serviceOrderHistory}${invoice.url}`, `Invoice: ${invoice.number}`).subscribe(
      () => {},
      err => {
        // Expose the error to the component
        this.openInvoiceError = err;
        this.changeDetector.markForCheck();
      }
    );
  }

  public onClose(): void {
    this.dialog.close();
  }
}
