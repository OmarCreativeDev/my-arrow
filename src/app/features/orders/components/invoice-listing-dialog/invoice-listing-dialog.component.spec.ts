import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoiceListingDialogComponent } from './invoice-listing-dialog.component';
import { SharedModule } from '@app/shared/shared.module';
import { DateService } from '@app/shared/services/date.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { IOrderLine, IInvoice } from '@app/shared/shared.interfaces';
import { OrdersService } from '@app/core/orders/orders.service';
import { of, throwError } from 'rxjs';
import { FileService } from '@app/shared/services/file.service';
import { environment } from '@env/environment';
import { Dialog, APP_DIALOG_DATA } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { StoreModule, combineReducers } from '@ngrx/store';
import { ordersReducers } from '@app/features/orders/stores/orders/orders.reducers';

class MockOrdersService {
  public getInvoicesForOrderLineRefs() {
    return of([]);
  }
}

class MockFileService {
  getAndOpenFile() {}
}

describe('InvoiceListingDialogComponent', () => {
  let component: InvoiceListingDialogComponent;
  let fixture: ComponentFixture<InvoiceListingDialogComponent>;
  let fileService: FileService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        HttpClientTestingModule,
        StoreModule.forRoot({
          orders: combineReducers(ordersReducers),
        }),
      ],
      providers: [
        { provide: OrdersService, useClass: MockOrdersService },
        { provide: FileService, useClass: MockFileService },
        { provide: Dialog, useClass: DialogMock },
        DateService,
        { provide: APP_DIALOG_DATA, useValue: {} },
      ],
      declarations: [InvoiceListingDialogComponent],
    }).compileComponents();
    fileService = TestBed.get(FileService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoiceListingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should preselect invoice if there is only 1', () => {
    const mockOrderLines = [
      {
        invoices: [{ number: '1' } as IInvoice],
      } as IOrderLine,
    ];
    const expectedSelection = mockOrderLines[0].invoices.map(invoice => invoice.number);
    const instance = new InvoiceListingDialogComponent(
      undefined,
      {
        orderLines: mockOrderLines,
      },
      undefined,
      undefined
    );
    expect(instance.selectedIds).toEqual(expectedSelection);
  });

  it('should not preselect any invoices if there are more than 1', () => {
    const mockOrderLines = [
      {
        invoices: [{ number: '1' } as IInvoice],
      } as IOrderLine,
      {
        invoices: [{ number: '2' } as IInvoice],
      } as IOrderLine,
    ];
    const instance = new InvoiceListingDialogComponent(
      undefined,
      {
        orderLines: mockOrderLines,
      },
      undefined,
      undefined
    );
    expect(instance.selectedIds).toEqual([]);
  });

  describe('#allSelected', () => {
    it('should return false if there are no invoices', () => {
      // Missing Order
      component.invoices = [];
      const result1 = component.allSelected();
      expect(result1).toBeFalsy();
    });

    const testCases = [
      {
        description: 'should return true if all ids are selected',
        lineIds: ['1', '2', '3'],
        selectedIds: ['1', '2', '3'],
        result: true,
      },
      {
        description: 'should return false if all ids are not selected',
        lineIds: ['1', '2', '3'],
        selectedIds: ['1'],
        result: false,
      },
      {
        description: 'should return false if selected id is not found',
        lineIds: ['1'],
        selectedIds: ['100'],
        result: false,
      },
    ];

    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        spyOn(component, 'getAllLineIds').and.returnValue(testCase.lineIds);
        component.invoices = [{} as IInvoice];
        component.selectedIds = testCase.selectedIds;
        const result = component.allSelected();
        expect(result).toEqual(testCase.result);
      });
    }
  });

  describe('#isSelected', () => {
    const testCases = [
      {
        description: 'should return true if the specified orderLines is selected',
        invoice: { number: '1' },
        selectedIds: ['1'],
        result: true,
      },
      {
        description: 'should return true if the specified orderLines is selected',
        invoice: { number: '0' },
        selectedIds: ['1'],
        result: false,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        component.selectedIds = testCase.selectedIds;
        const result = component.isSelected(testCase.invoice as IInvoice);
        expect(result).toEqual(testCase.result);
      });
    }
  });

  describe('#getAllLineIds', () => {
    it('should return a map of the invoice numbers of all orderLines', () => {
      const mockInvoiceNumber = 'mock-invoice-number';
      const expectedResult = [mockInvoiceNumber];
      component.invoices = [{ number: mockInvoiceNumber } as IInvoice];
      const result = component.getAllLineIds();
      expect(result).toEqual(expectedResult);
    });
    it('should return an empty array if there are no invoices', () => {
      component.invoices = [];
      const result = component.getAllLineIds();
      expect(result).toEqual([]);
    });
  });

  describe('toggleLines', () => {
    it('#toggleLine should add/remove the invoiceNumber to the selectedIds', () => {
      const mockInvoiceNumber = '1';
      const mockInvoice = { number: mockInvoiceNumber } as IInvoice;
      component.toggleLine(mockInvoice, true);
      expect(component.selectedIds).toContain(mockInvoiceNumber);
      component.toggleLine(mockInvoice, false);
      expect(component.selectedIds).not.toContain(mockInvoiceNumber);
    });

    it('#toggleLine should add/remove multiple invoiceNumber to the selectedIds', () => {
      const mockInvoiceNumbers = ['1', '2'];
      spyOn(component, 'getAllLineIds').and.returnValue(mockInvoiceNumbers);
      component.toggleAllLines(true);
      expect(component.selectedIds).toContain(mockInvoiceNumbers[0]);
      expect(component.selectedIds).toContain(mockInvoiceNumbers[1]);
      component.toggleAllLines(false);
      expect(component.selectedIds).not.toContain(mockInvoiceNumbers[0]);
      expect(component.selectedIds).not.toContain(mockInvoiceNumbers[1]);
    });

    it('#toggleLine should not add duplicates to the selectedIds', () => {
      const mockInvoiceNumber = '1';
      component.selectedIds = [mockInvoiceNumber];
      component.toggleLine({ number: mockInvoiceNumber } as IInvoice, true);
      expect(component.selectedIds).toEqual([mockInvoiceNumber]);
    });
  });

  describe('#openInvoice', () => {
    it('should call #getAndOpenFile with the url of the provided Invoice', () => {
      const getAndOpenFileStub = spyOn(fileService, 'getAndOpenFile').and.callFake(() => of([]));
      const dummyInvoice = { url: '/dummy-url', number: '1234567890' } as IInvoice;
      component.openInvoice(dummyInvoice);
      expect(getAndOpenFileStub).toHaveBeenCalledWith(
        `${environment.baseUrls.serviceOrderHistory}${dummyInvoice.url}`,
        `Invoice: ${dummyInvoice.number}`
      );
    });
    it('should set an invoice error on the component if retrieving the Invoice failed for any reason', () => {
      const expectedError = new Error('dummy error');
      const dummyInvoice = { url: '/dummy-url' } as IInvoice;
      spyOn(fileService, 'getAndOpenFile').and.callFake(() => throwError(expectedError));
      component.openInvoice(dummyInvoice);
      expect(component.openInvoiceError).toEqual(expectedError);
    });
  });
});
