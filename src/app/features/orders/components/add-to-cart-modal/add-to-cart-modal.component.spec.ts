import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToCartModalComponent } from './add-to-cart-modal.component';
import { Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Store } from '@ngrx/store';
import { CommonModule } from '@angular/common';

class MockStore {
  select() {
    return of();
  }
  dispatch() {}
  pipe() {
    return of();
  }
}

describe('AddToCartModalComponent', () => {
  let component: AddToCartModalComponent;
  let fixture: ComponentFixture<AddToCartModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddToCartModalComponent],
      imports: [CommonModule, FormsModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{ provide: Dialog, useClass: DialogMock }, { provide: Store, useClass: MockStore }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToCartModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call dismiss', () => {
    spyOn(component, 'dismiss').and.callThrough();
    component.dismiss();
    fixture.detectChanges();
    expect(component.dismiss).toHaveBeenCalled();
  });

  it('should call confirm', () => {
    spyOn(component, 'confirm').and.callThrough();
    component.confirm();
    fixture.detectChanges();
    expect(component.confirm).toHaveBeenCalled();
  });
});
