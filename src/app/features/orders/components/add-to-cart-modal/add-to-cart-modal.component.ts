import { Component, OnInit } from '@angular/core';

import { Dialog } from '@app/core/dialog/dialog.service';
import { Store } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { ConfirmAddToCart } from '@app/features/orders/stores/order-details/order-details.actions';

@Component({
  selector: 'app-add-to-cart-modal',
  templateUrl: './add-to-cart-modal.component.html',
  styleUrls: ['./add-to-cart-modal.component.scss'],
})
export class AddToCartModalComponent implements OnInit {
  constructor(private store: Store<IAppState>, private dialog: Dialog<AddToCartModalComponent>) {}

  ngOnInit() {}

  public dismiss(): void {
    this.dialog.close();
  }

  public confirm(): void {
    this.store.dispatch(new ConfirmAddToCart());
    this.dialog.close();
  }
}
