import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { ISelectableOption, ISearchCriteria } from '@app/shared/shared.interfaces';
import { find } from 'lodash-es';

@Component({
  selector: 'app-order-lines-search',
  templateUrl: './order-lines-search.component.html',
  styleUrls: ['./order-lines-search.component.scss'],
})
export class OrderLinesSearchComponent implements OnInit {
  @Input()
  searchCriteria: ISearchCriteria;
  @Output()
  search = new EventEmitter<ISearchCriteria>();

  public searchOptions: Array<ISelectableOption<any>> = [
    { label: 'PO #', value: 'purchaseOrderNumber', payload: { placeholder: 'Search by Purchase Order' } },
    { label: 'Manufacturer Part #', value: 'manufacturerPartNumber', payload: { placeholder: 'Search by Manufacturer Part #' } },
    { label: 'Buyer Name', value: 'buyerName', payload: { placeholder: 'Search by Buyer Name' } },
    { label: 'Customer Part #', value: 'customerPartNumber', payload: { placeholder: 'Search by Customer Part #' } },
  ];

  public selectedSearchOption: ISelectableOption<any>;

  /**
   * Local state which is managed locally and broadcast to action
   * that replaces the value in the RxJS state
   */
  public searchState: ISearchCriteria;

  constructor() {
    this.searchState = this.getEmptySearchState();
    this.updateSearchType(this.searchOptions[0].value);
  }
  /**
   * Submits the default search options when initted
   * TODO: This would be better placed in the constructor
   */
  ngOnInit() {
    this.submitSearch();
  }

  /**
   * Submits the locally-stored searchCriteria
   */
  public submitSearch() {
    this.search.emit({ ...this.searchState });
  }

  /**
   * Resetting the form effectively creates a new searchCriteria and emits to the parent container
   */
  public resetForm() {
    this.searchState = this.getEmptySearchState();
    this.submitSearch();
  }

  public updateSearchType($event) {
    this.searchState.type = $event;
    this.selectedSearchOption = find(this.searchOptions, ['value', this.searchState.type]);
  }

  /**
   * Create a blank, 'default' searchCriteria
   */
  public getEmptySearchState(): ISearchCriteria {
    return {
      value: '',
      type: this.searchOptions[0].value,
    };
  }

  public onPaste(event): void {
    let searchValue: string;

    if (window['clipboardData'] && window['clipboardData'].getData) {
      // IE11
      searchValue = window['clipboardData'].getData('Text').trim();
    } else if (event.clipboardData) {
      searchValue = event.clipboardData.getData('text').trim();
    }

    this.searchState.value = searchValue;
  }
}
