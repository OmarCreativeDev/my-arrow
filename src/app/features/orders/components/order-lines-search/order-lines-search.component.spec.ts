import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderLinesSearchComponent } from './order-lines-search.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('OrderLinesSearchComponent', () => {
  let component: OrderLinesSearchComponent;
  let fixture: ComponentFixture<OrderLinesSearchComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrderLinesSearchComponent],
      schemas: [NO_ERRORS_SCHEMA],
    });
    fixture = TestBed.createComponent(OrderLinesSearchComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the local searchCriteria; the type should be the first option value', () => {
    expect(component.searchState).toBeDefined();
    expect(component.searchState.type).toEqual(component.searchOptions[0].value);
  });

  it('should set the search when initted', () => {
    const searchSpy = spyOn(component.search, 'emit');
    component.ngOnInit();
    expect(searchSpy).toHaveBeenCalled();
  });

  it('#submitSearch should dispatch an event on search', () => {
    const expectedSearchState = {
      type: 'foo',
      value: 'bar',
    };
    component.searchState = expectedSearchState;
    const searchSpy = spyOn(component.search, 'emit');
    component.submitSearch();
    expect(searchSpy).toHaveBeenCalledWith(expectedSearchState);
  });

  it('#resetForm should use a default search criteria and invoke #submitSearch', () => {
    const expectedSearchState = component.getEmptySearchState();
    const searchSpy = spyOn(component.search, 'emit');
    component.resetForm();
    expect(searchSpy).toHaveBeenCalledWith(expectedSearchState);
  });

  it('#getEmptySearchCriteria should return an empty searchCriteria object', () => {
    const result = component.getEmptySearchState();
    expect(result.value).toEqual('');
    expect(result.type).toEqual(component.searchOptions[0].value);
  });
});
