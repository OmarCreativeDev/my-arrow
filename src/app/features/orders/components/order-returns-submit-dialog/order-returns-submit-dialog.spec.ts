import { async, TestBed, ComponentFixture } from '@angular/core/testing';
import { OrderReturnsSubmitDialogComponent } from './order-returns-submit-dialog.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Dialog, APP_DIALOG_DATA } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { orderReturnsSubmitDialogData } from '@app/core/orders/orders.mock';

class DummyComponent {}

describe('OrderReturnsSubmitDialogComponent', () => {
  const routes = [{ path: 'orders/:orderId', component: DummyComponent }];
  let component: OrderReturnsSubmitDialogComponent;
  let fixture: ComponentFixture<OrderReturnsSubmitDialogComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderReturnsSubmitDialogComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [RouterTestingModule.withRoutes(routes)],
      providers: [{ provide: Dialog, useClass: DialogMock }, { provide: APP_DIALOG_DATA, useValue: orderReturnsSubmitDialogData }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderReturnsSubmitDialogComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call dialog close and do nothing when invoking onDismiss() and submit failed', () => {
    const closeEvent = spyOn(component.dialog, 'close');
    component.submitFailed = true;
    component.onDismiss();
    expect(closeEvent).toHaveBeenCalled();
  });

  it('should call dialog close and redirect to order details when invoking onDismiss() and submit is successful', () => {
    const closeEvent = spyOn(component.dialog, 'close');
    const spyRoute = spyOn(router, 'navigateByUrl').and.callThrough();
    component.submitFailed = false;
    component.orderId = 123;
    component.salesHeaderId = 456;
    component.onDismiss();
    expect(closeEvent).toHaveBeenCalled();
    expect(spyRoute.calls.first().args[0]).toBe(`/orders/123?salesHeaderId=456`);
  });
});
