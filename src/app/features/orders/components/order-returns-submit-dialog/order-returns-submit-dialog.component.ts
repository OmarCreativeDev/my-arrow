import { Component, Inject } from '@angular/core';
import { Dialog, APP_DIALOG_DATA } from '@app/core/dialog/dialog.service';
import { Router } from '@angular/router';

export interface DialogData {
  submitFailed: boolean;
  orderId?: number;
  salesHeaderId?: number;
}

@Component({
  selector: 'app-order-returns-submit-dialog',
  templateUrl: './order-returns-submit-dialog.component.html',
  styleUrls: ['./order-returns-submit-dialog.component.scss'],
})
export class OrderReturnsSubmitDialogComponent {
  public submitFailed: boolean;
  public orderId: number;
  public salesHeaderId: number;

  constructor(
    public router: Router,
    public dialog: Dialog<OrderReturnsSubmitDialogComponent>,
    @Inject(APP_DIALOG_DATA) public data: DialogData
  ) {
    this.submitFailed = data.submitFailed;
    this.orderId = data.orderId;
    this.salesHeaderId = data.salesHeaderId;
  }

  onDismiss(): void {
    this.dialog.close();
    if (!this.submitFailed) {
      const withSalesHeaderId = this.salesHeaderId !== 0 ? `?salesHeaderId=${this.salesHeaderId}` : '';
      this.router.navigateByUrl(`/orders/${this.orderId}${withSalesHeaderId}`);
    }
  }
}
