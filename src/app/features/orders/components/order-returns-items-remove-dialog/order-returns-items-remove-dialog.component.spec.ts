import { OrderReturnsItemsRemoveDialogComponent } from '@app/features/orders/components/order-returns-items-remove-dialog/order-returns-items-remove-dialog.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CoreModule } from '@app/core/core.module';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';

describe('OrderReturnsItemsRemoveDialogComponent', () => {
  let component: OrderReturnsItemsRemoveDialogComponent;
  let fixture: ComponentFixture<OrderReturnsItemsRemoveDialogComponent>;

  const data = {
    lineItem: '1.1',
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderReturnsItemsRemoveDialogComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [CoreModule],
      providers: [{ provide: Dialog, useClass: DialogMock }, { provide: APP_DIALOG_DATA, useValue: data }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderReturnsItemsRemoveDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call dialog close method when invoking onClose', () => {
    const closeEvent = spyOn(component.dialog, 'close');
    component.onClose(false);
    expect(closeEvent).toHaveBeenCalled();
  });
});
