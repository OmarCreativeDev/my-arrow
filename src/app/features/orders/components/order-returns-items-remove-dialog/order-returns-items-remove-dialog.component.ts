import { Component, Inject } from '@angular/core';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { IOrderLine } from '@app/shared/shared.interfaces';

@Component({
  selector: 'app-order-returns-items-remove-dialog',
  templateUrl: './order-returns-items-remove-dialog.component.html',
  styleUrls: ['./order-returns-items-remove-dialog.component.scss'],
})
export class OrderReturnsItemsRemoveDialogComponent {
  constructor(@Inject(APP_DIALOG_DATA) public orderLine: IOrderLine, public dialog: Dialog<OrderReturnsItemsRemoveDialogComponent>) {}

  public onClose(removeItemAccepted: boolean): void {
    this.dialog.close({
      removeItemAccepted,
    });
  }
}
