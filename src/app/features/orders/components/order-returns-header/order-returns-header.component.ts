import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { IAppState, IOrderDetails, orderReturnReason, ISelectableOption } from '@app/shared/shared.interfaces';
import { IUser } from '@app/core/user/user.interface';
import { select, Store } from '@ngrx/store';
import { getDetails } from '@app/features/orders/stores/order-details/order-details.selectors';
import { getUser } from '@app/core/user/store/user.selectors';
import { Observable, Subscription } from 'rxjs';
import { ControlContainer, NgForm } from '@angular/forms';
import { CheckboxComponent } from '@app/shared/components/checkbox/checkbox.component';

@Component({
  selector: 'app-order-returns-header',
  templateUrl: './order-returns-header.component.html',
  styleUrls: ['./order-returns-header.component.scss'],
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm }],
})
export class OrderReturnsHeaderComponent implements OnInit, OnDestroy {
  @Output() selectReturnReason: EventEmitter<orderReturnReason>;

  public partsYes = undefined;
  public partsNo = undefined;
  public isCreditRequired = false;
  public hasCAPA = false;
  public repairOption = undefined;
  public failureOption = undefined;
  public creditOption = undefined;
  public details$: Observable<IOrderDetails>;
  public user$: Observable<IUser>;
  public user: IUser;
  public details: IOrderDetails;
  public selectedReason: orderReturnReason;
  public description: string;
  public returnReasons: Array<ISelectableOption<object>> = [
    {
      value: orderReturnReason.shortShip,
      label: 'Short Ship',
    },
    {
      value: orderReturnReason.overShip,
      label: 'Over Ship',
    },
    {
      value: orderReturnReason.defectiveParts,
      label: 'Defective Parts',
    },
    {
      value: orderReturnReason.damagedParts,
      label: 'Damaged Parts',
    },
    {
      value: orderReturnReason.packagingIssue,
      label: 'Packaging Issue',
    },
    {
      value: orderReturnReason.wrongParts,
      label: 'Wrong Parts',
    },
    {
      value: orderReturnReason.anotherCustomersParts,
      label: "Another Customer's Parts",
    },
    {
      value: orderReturnReason.earlyShipment,
      label: 'Early Shipment',
    },
    {
      value: orderReturnReason.courtesyReturn,
      label: 'Courtesy Return',
    },
    {
      value: orderReturnReason.customerRequirementNotFollowed,
      label: 'Customer Requirement Not Followed',
    },
    {
      value: orderReturnReason.issueNotListed,
      label: 'Issue Not Listed',
    },
  ];

  private subscription: Subscription = new Subscription();

  constructor(private store: Store<IAppState>) {
    this.selectReturnReason = new EventEmitter<orderReturnReason>();
    this.getStoreSlices();
  }

  ngOnInit() {
    this.startSubscriptions();
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public selectReason(reason: orderReturnReason): void {
    this.selectedReason = reason;
    this.partsNo = this.isNotRetuningParts();
    this.partsYes = this.isReturningParts();
    this.resetDefectivePartsOptions();
    this.selectReturnReason.emit(reason);
  }

  public isCreditRequiredVisible(): boolean {
    return (
      this.selectedReason === orderReturnReason.packagingIssue ||
      this.selectedReason === orderReturnReason.issueNotListed ||
      this.selectedReason === orderReturnReason.customerRequirementNotFollowed
    );
  }

  public isSelectOneVisible(): boolean {
    return this.selectedReason === orderReturnReason.defectiveParts || this.selectedReason === orderReturnReason.damagedParts;
  }

  public areDefectivePartsVisible(): boolean {
    return this.selectedReason === orderReturnReason.defectiveParts;
  }

  public isCAPARequestedVisible(): boolean {
    return this.selectedReason !== undefined;
  }

  public unCheckReturningPartsAnswer(event: boolean, checkbox: CheckboxComponent): void {
    if (event) {
      checkbox.value = false;
    }
  }

  public verifyCheckboxSelection(): void {
    if (this.repairOption) {
      this.failureOption = Boolean(this.failureOption);
      this.creditOption = Boolean(this.creditOption);
    }

    if (this.failureOption) {
      this.repairOption = Boolean(this.repairOption);
      this.creditOption = Boolean(this.creditOption);
    }

    if (this.creditOption) {
      this.repairOption = Boolean(this.repairOption);
      this.failureOption = Boolean(this.failureOption);
    }

    if (!this.repairOption && !this.failureOption && !this.creditOption) {
      this.resetDefectivePartsOptions();
    }
  }

  private getStoreSlices(): void {
    this.details$ = this.store.pipe(select(getDetails));
    this.user$ = this.store.pipe(select(getUser));
  }

  private subscribeToOrderDetails(): Subscription {
    return this.details$.subscribe(details => {
      this.details = details;
    });
  }

  private subscribeToUser(): Subscription {
    return this.user$.subscribe(user => {
      this.user = user;
    });
  }

  private startSubscriptions(): void {
    this.subscription.add(this.subscribeToOrderDetails());
    this.subscription.add(this.subscribeToUser());
  }

  private isReturningParts(): boolean {
    return (
      this.selectedReason === orderReturnReason.defectiveParts ||
      this.selectedReason === orderReturnReason.earlyShipment ||
      this.selectedReason === orderReturnReason.overShip ||
      this.selectedReason === orderReturnReason.courtesyReturn
    );
  }

  private isNotRetuningParts(): boolean {
    return this.selectedReason === orderReturnReason.shortShip;
  }

  private resetDefectivePartsOptions(): void {
    this.repairOption = undefined;
    this.failureOption = undefined;
    this.creditOption = undefined;
  }
}
