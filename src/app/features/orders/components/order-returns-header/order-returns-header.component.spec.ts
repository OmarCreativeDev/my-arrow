import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { OrderReturnsHeaderComponent } from './order-returns-header.component';
import { Store, StoreModule } from '@ngrx/store';
import { orderDetailsReducers } from '../../stores/order-details/order-details.reducers';
import { userReducers } from '../../../../core/user/store/user.reducers';
import { IInvoice, IOrder, IOrderLine, orderReturnReason } from '@app/shared/shared.interfaces';
import { QueryComplete } from '../../stores/order-details/order-details.actions';

describe('OrderReturnsComponent', () => {
  let component: OrderReturnsHeaderComponent;
  let fixture: ComponentFixture<OrderReturnsHeaderComponent>;
  let store: Store<any>;

  const mockInvoices = [
    {
      number: '123',
    } as IInvoice,
  ];
  const mockOrderLines = [
    {
      invoices: mockInvoices,
    } as IOrderLine,
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          orderDetails: orderDetailsReducers,
          user: userReducers,
        }),
      ],
      declarations: [OrderReturnsHeaderComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
    fixture = TestBed.createComponent(OrderReturnsHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  beforeEach(() => {
    store.dispatch(
      new QueryComplete({
        orderLines: mockOrderLines,
      } as IOrder)
    );
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should show the packaging section when selecting the packaging option ', () => {
    component.selectReason(orderReturnReason.packagingIssue);
    expect(component.isCreditRequiredVisible()).toBeTruthy();
  });

  it('should show the defective section when selecting the defective parts option', () => {
    component.selectReason(orderReturnReason.defectiveParts);
    expect(component.areDefectivePartsVisible).toBeTruthy();
  });

  it('should show the CAPA section when selecting any reason', () => {
    component.selectReason(orderReturnReason.overShip);
    expect(component.isCAPARequestedVisible()).toBeTruthy();
  });

  it('should set No returning parts to true when selecting the shortShip reason', () => {
    component.selectReason(orderReturnReason.shortShip);
    expect(component.partsNo).toBeTruthy();
    expect(component.partsYes).toBeFalsy();
  });

  it('should set Yes returning parts to true when selecting defectiveParts, earlyShipment, overShip or courtesyReturn reasons', () => {
    const reasons = [
      orderReturnReason.defectiveParts,
      orderReturnReason.earlyShipment,
      orderReturnReason.overShip,
      orderReturnReason.courtesyReturn,
    ];
    reasons.forEach(reason => {
      component.selectReason(reason);
      expect(component.partsNo).toBeFalsy();
      expect(component.partsYes).toBeTruthy();
    });
  });

  it('should set returning parts to blank when selecting damagedParts, packagingIssue, wrongParts, customerRequirementNotFollowed or issueNotListed reasons', () => {
    const reasons = [
      orderReturnReason.damagedParts,
      orderReturnReason.packagingIssue,
      orderReturnReason.wrongParts,
      orderReturnReason.customerRequirementNotFollowed,
      orderReturnReason.issueNotListed,
    ];
    reasons.forEach(reason => {
      component.selectReason(reason);
      expect(component.partsNo).toBeFalsy();
      expect(component.partsYes).toBeFalsy();
    });
  });

  it('should clear the defective parts options when selecting a reason', () => {
    component.creditOption = true;
    component.failureOption = true;
    component.repairOption = false;
    component.selectReason(orderReturnReason.damagedParts);
    expect(component.creditOption).toBeUndefined();
    expect(component.failureOption).toBeUndefined();
    expect(component.repairOption).toBeUndefined();
  });
});
