import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShipmentTrackingDialogComponent } from './shipment-tracking-dialog.component';
import { SharedModule } from '@app/shared/shared.module';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Dialog, APP_DIALOG_DATA } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { StoreModule, combineReducers } from '@ngrx/store';
import { ordersReducers } from '@app/features/orders/stores/orders/orders.reducers';

describe('ShipmentTrackingDialogComponent', () => {
  let component: ShipmentTrackingDialogComponent;
  let fixture: ComponentFixture<ShipmentTrackingDialogComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ShipmentTrackingDialogComponent],
        providers: [{ provide: Dialog, useClass: DialogMock }, { provide: APP_DIALOG_DATA, useValue: {} }],
        imports: [
          SharedModule,
          HttpClientTestingModule,
          StoreModule.forRoot({
            orders: combineReducers(ordersReducers),
          }),
        ],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipmentTrackingDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should trigger close when button is pressed', () => {
    spyOn(component.dialog, 'close');
    component.onClose();
    expect(component.dialog.close).toHaveBeenCalled();
  });
});
