import { Component, Inject } from '@angular/core';
import { IOrder } from '@app/shared/shared.interfaces';
import { Dialog, APP_DIALOG_DATA } from '@app/core/dialog/dialog.service';

export interface IShipmentTrackingDialogData {
  order: IOrder;
}

@Component({
  selector: 'app-shipment-tracking',
  templateUrl: './shipment-tracking-dialog.component.html',
  styleUrls: ['./shipment-tracking-dialog.component.scss'],
})
export class ShipmentTrackingDialogComponent {
  public order: IOrder;

  constructor(public dialog: Dialog<ShipmentTrackingDialogComponent>, @Inject(APP_DIALOG_DATA) public data: IShipmentTrackingDialogData) {
    this.order = data.order;
  }

  public get hasShipmentTracking(): boolean {
    if (!this.order || !this.order.orderLines) return false;

    const tracking = this.order.orderLines.filter(orderLine => {
      return orderLine.shipments ? orderLine.shipments.length : false;
    });

    return Boolean(tracking.length);
  }

  public onClose(): void {
    this.dialog.close();
  }
}
