import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { IsoDatePipe } from '@app/shared/pipes/iso-date/iso-date.pipe';
import { DialogData, EditRequestedDateDialogComponent } from './edit-requested-date-dialog.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DialogMock } from '../../../../core/dialog/dialog.service.spec';
import { APP_DIALOG_DATA, Dialog } from '../../../../core/dialog/dialog.service';
import { DatePipe } from '@angular/common';
import { DateService } from '../../../../shared/services/date.service';
import { PushPullOrderService } from '@app/core/push-pull-order/push-pull-order.service';
import { ToastService } from '@app/core/toast/toast.service';
import { Store, StoreModule } from '@ngrx/store';
import { IOrderLine, IOrderLineStatus, IPurchaseOrderStatus } from '@app/shared/shared.interfaces';
import * as moment from 'moment';
import { of, throwError } from 'rxjs';
import { IPushPullOrderRequest } from '@app/core/push-pull-order/push-pull-order.interfaces';
import { PushPullOrder } from '@app/features/orders/stores/orders/orders.actions';

class DateServiceMock {
  getMsForDays() {
    return 1000;
  }
  getDate() {
    return '';
  }
}

class PushPullMock {
  changeOrderDate() {
    return 200;
  }
}

class MockToastService {
  public showToast() {}
}

describe('EditRequestedDateDialogComponent', () => {
  let component: EditRequestedDateDialogComponent;
  let fixture: ComponentFixture<EditRequestedDateDialogComponent>;
  let pushPullService: PushPullOrderService;
  let dialogService: Dialog<any>;
  let toastService: ToastService;
  let store: Store<any>;

  const orderLine: IOrderLine = {
    id: 1234,
    purchaseOrderNumber: 'PO1234',
    purchaseOrderStatus: IPurchaseOrderStatus.Open,
    lineItem: '1.2.1',
    customerPartNumber: 'BAV99',
    manufacturerPartNumber: 'BAV99',
    manufacturerName: 'Vishay',
    qtyOrdered: 500,
    qtyShipped: 200,
    qtyReadyToShip: 200,
    qtyRemaining: 100,
    requested: new Date(),
    entered: new Date(),
    committed: new Date(),
    status: IOrderLineStatus.Open,
    buyerName: 'David Lane',
    salesOrderId: 'ASDF',
    salesHeaderId: 7453868,
    billToSiteUseId: 1234,
    unitResale: 0,
    extResale: 0,
    currencyCode: 'USD',
    docId: '555_123456',
    itemId: 123456,
    warehouseId: 555,
    shipments: [],
    invoices: [],
  };

  const data: DialogData = {
    orderLines: [orderLine],
    poNumber: '123',
    orderNumber: '678',
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([]), StoreModule.forRoot({})],
      declarations: [EditRequestedDateDialogComponent, IsoDatePipe],
      providers: [
        { provide: Dialog, useClass: DialogMock },
        { provide: DateService, useClass: DateServiceMock },
        { provide: PushPullOrderService, useClass: PushPullMock },
        { provide: ToastService, useClass: MockToastService },
        DateService,
        { provide: APP_DIALOG_DATA, useValue: data },
        DatePipe,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
    pushPullService = TestBed.get(PushPullOrderService);
    dialogService = TestBed.get(Dialog);
    toastService = TestBed.get(ToastService);
  }));

  beforeEach(() => {
    store = TestBed.get(Store);
    spyOn(store, 'dispatch');

    fixture = TestBed.createComponent(EditRequestedDateDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the selected date as the new date', () => {
    const selectedDate = moment(orderLine.requested)
      .add(2, 'days')
      .toDate();
    component.onHeaderDateSelected([selectedDate]);
    expect(component.newDates[orderLine.lineItem]).toEqual([selectedDate]);
  });

  describe('when submitting the new date', () => {
    const selectedDate = moment(orderLine.requested)
      .add(2, 'days')
      .toDate();

    it('should call the changeOrderDate method', () => {
      const mockRequest = {
        orderNumber: data.orderNumber,
        poNumber: data.poNumber,
        coItems: [
          {
            currentDate: String(orderLine.requested),
            customerPartNumber: orderLine.customerPartNumber,
            manufacturerName: orderLine.manufacturerName,
            manufacturerPartNumber: orderLine.manufacturerPartNumber,
            orderLineItemId: orderLine.lineItem,
            updatedDate: moment(selectedDate).format('YYYY-MM-DD'),
          },
        ],
      } as IPushPullOrderRequest;
      const changeOrderDateStub = spyOn(pushPullService, 'changeOrderDate').and.callFake(() => of({}));

      component.newDates[orderLine.lineItem] = [selectedDate];
      component.onSubmit();
      expect(changeOrderDateStub).toHaveBeenCalledWith(mockRequest);
    });

    it('should close the dialog and show a toast after calling the changeOrderDate method successfully', () => {
      spyOn(pushPullService, 'changeOrderDate').and.callFake(() => of({}));
      const closeStub = spyOn(dialogService, 'close').and.callFake(() => null);
      const toastStub = spyOn(toastService, 'showToast').and.callFake(() => null);

      component.newDates[orderLine.lineItem] = [selectedDate];
      component.onSubmit();
      expect(closeStub).toHaveBeenCalled();
      expect(toastStub).toHaveBeenCalled();
    });

    it('should dispatch a PushPullOrder action after calling the changeOrderDate method successfully', () => {
      spyOn(pushPullService, 'changeOrderDate').and.callFake(() => of({}));
      const pushpullAction = new PushPullOrder([orderLine.itemId.toString(10)]);

      component.newDates[orderLine.lineItem] = [selectedDate];
      component.onSubmit();
      expect(store.dispatch).toHaveBeenCalledWith(pushpullAction);
    });

    it('should set an error on the component if the changeOrderDate method fails', () => {
      const expectedError = new Error('test error');
      spyOn(pushPullService, 'changeOrderDate').and.callFake(() => throwError(expectedError));
      component.newDates[orderLine.lineItem] = [selectedDate];
      component.onSubmit();
      expect(component.error).toEqual(expectedError);
    });
  });
});
