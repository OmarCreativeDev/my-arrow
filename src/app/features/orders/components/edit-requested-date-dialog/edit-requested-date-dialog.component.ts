import { Component, Inject, OnInit } from '@angular/core';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { IAppState, IOrderLine } from '@app/shared/shared.interfaces';
import * as moment from 'moment';
import { PushPullOrderService } from '@app/core/push-pull-order/push-pull-order.service';
import { IPushPullOrderRequest, IPushPullOrderRequestItem } from '@app/core/push-pull-order/push-pull-order.interfaces';
import { ToastService } from '@app/core/toast/toast.service';
import { PushPullOrder } from '@app/features/orders/stores/orders/orders.actions';
import { Store } from '@ngrx/store';

export interface DialogData {
  poNumber: string;
  orderNumber: string;
  orderLines: Array<IOrderLine>;
}

@Component({
  selector: 'app-edit-requested-date-dialog',
  templateUrl: './edit-requested-date-dialog.component.html',
  styleUrls: ['./edit-requested-date-dialog.component.scss'],
})
export class EditRequestedDateDialogComponent implements OnInit {
  readonly poNumber: string;
  readonly orderNumber: string;
  private productNames: Array<string>;
  public error: Error;
  public submitProcessing: boolean = false;
  public orderLines: Array<IOrderLine> = [];
  public newDates: any = {};
  public datePickerDates: any = {};

  public minDate = moment()
    .startOf('day')
    .toDate();

  public datePickerDatesConfig = [
    {
      label: '',
      placeholder: 'yyyy-mm-dd',
    },
  ];

  public get canSubmit(): boolean {
    return Object.keys(this.newDates).length === this.orderLines.length;
  }

  /* tslint:disable:no-unused-variable */
  constructor(
    private dialog: Dialog<EditRequestedDateDialogComponent>,
    @Inject(APP_DIALOG_DATA) public data: DialogData,
    private pushPull: PushPullOrderService,
    private store: Store<IAppState>,
    private toastService: ToastService
  ) {
    this.orderLines = data.orderLines;
    this.poNumber = data.poNumber;
    this.orderNumber = data.orderNumber;

    for (const orderLine of this.orderLines) {
      this.datePickerDates[orderLine.lineItem] = [undefined];
    }

    this.datePickerDates['head'] = [undefined];
  }

  ngOnInit() {}

  public onClose(): void {
    this.dialog.close();
  }

  public onSubmit(): void {
    this.submitProcessing = true;
    this.productNames = [];
    const request: IPushPullOrderRequest = {
      orderNumber: this.orderNumber,
      poNumber: this.poNumber,
      coItems: this.orderLines.map(orderLine => {
        this.productNames.push(orderLine.itemId.toString(10));
        const coItem: IPushPullOrderRequestItem = {
          currentDate: String(orderLine.requested),
          customerPartNumber: orderLine.customerPartNumber,
          manufacturerName: orderLine.manufacturerName,
          manufacturerPartNumber: orderLine.manufacturerPartNumber,
          orderLineItemId: orderLine.lineItem,
          updatedDate: moment(this.newDates[orderLine.lineItem][0]).format('YYYY-MM-DD'),
        };
        return coItem;
      }),
    };

    this.pushPull.changeOrderDate(request).subscribe(done => this.onSuccess(), error => this.onError(error));
  }

  public onHeaderDateSelected(event): void {
    for (const orderLine of this.orderLines) {
      this.datePickerDates[orderLine.lineItem] = [event[0]];
      this.newDates[orderLine.lineItem] = [event[0]];
    }
  }

  public onLineTimeDateSelected(lineItem: string, event): void {
    this.newDates[lineItem] = [event[0]];
  }

  public onSuccess(): void {
    this.submitProcessing = false;
    this.dialog.close();
    this.toastService.showToast('Your request has been submitted. Please be aware that responses may take up to 24 hours.');
    this.store.dispatch(new PushPullOrder(this.productNames));
  }

  public onError(error): void {
    this.submitProcessing = false;
    this.error = error;
  }
}
