import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { IOrderDetails, IAppState } from '@app/shared/shared.interfaces';
import { Subscription } from 'rxjs';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';
import { Store, select } from '@ngrx/store';

@Component({
  selector: 'app-order-details-meta',
  templateUrl: './order-details-meta.component.html',
  styleUrls: ['./order-details-meta.component.scss'],
})
export class OrderDetailsMetaComponent implements OnInit, OnDestroy {
  @Input()
  orderDetails: IOrderDetails;

  public privateFeatureFlags: object;
  public subscription: Subscription = new Subscription();

  constructor(private store: Store<IAppState>) {}

  public get showRequestReturn(): boolean {
    return this.privateFeatureFlags['ordersRequestReturn'];
  }

  ngOnInit() {
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions(): void {
    this.subscription.add(this.subscribePrivateFeatureFlags());
  }

  private subscribePrivateFeatureFlags(): Subscription {
    return this.store.pipe(select(getPrivateFeatureFlagsSelector)).subscribe(featureFlags => {
      this.privateFeatureFlags = featureFlags;
    });
  }
}
