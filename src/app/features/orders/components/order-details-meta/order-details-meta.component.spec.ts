import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderDetailsMetaComponent } from './order-details-meta.component';
import { Store, StoreModule } from '@ngrx/store';
import { SharedModule } from '@app/shared/shared.module';
import { EffectsModule } from '@ngrx/effects';
import { PropertiesEffects } from '@app/features/properties/store/properties.effects';
import { propertiesReducers } from '@app/features/properties/store/properties.reducers';
import { mockPrivateProperties } from '@app/core/features/feature-flags.mock';
import { PropertiesModule } from '@app/features/properties/properties.module';
import { of } from 'rxjs';
import { ApiService } from '@app/core/api/api.service';

const mockPropertiesReducers = () => ({
  ...propertiesReducers,
  private: mockPrivateProperties,
});

class MockApiService {
  public get() {
    return of();
  }
}

describe('OrderDetailsHeaderComponent', () => {
  let component: OrderDetailsMetaComponent;
  let fixture: ComponentFixture<OrderDetailsMetaComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        StoreModule.forRoot({
          properties: mockPropertiesReducers,
        }),
        EffectsModule.forRoot([PropertiesEffects]),
        PropertiesModule,
      ],
      declarations: [OrderDetailsMetaComponent],
      providers: [{ provide: ApiService, useClass: MockApiService }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderDetailsMetaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
