import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CoreModule } from '@app/core/core.module';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { OrderReturnsDeleteAttachmentDialogComponent } from '@app/features/orders/components/order-returns-delete-attachment-dialog/order-returns-delete-attachment-dialog.component';

describe('OrderReturnsDeleteAttachmentDialogComponent', () => {
  let component: OrderReturnsDeleteAttachmentDialogComponent;
  let fixture: ComponentFixture<OrderReturnsDeleteAttachmentDialogComponent>;

  const dialogData = {
    file: {
      name: 'test',
    },
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrderReturnsDeleteAttachmentDialogComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [CoreModule],
      providers: [{ provide: Dialog, useClass: DialogMock }, { provide: APP_DIALOG_DATA, useValue: dialogData }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderReturnsDeleteAttachmentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call dialog close method when invoking onClose', () => {
    const closeEvent = spyOn(component.dialog, 'close');
    component.onClose(false);
    expect(closeEvent).toHaveBeenCalled();
  });
});
