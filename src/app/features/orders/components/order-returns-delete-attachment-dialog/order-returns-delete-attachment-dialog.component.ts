import { Component, Inject } from '@angular/core';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';

@Component({
  selector: 'app-order-returns-delete-attachment-dialog',
  templateUrl: './order-returns-delete-attachment-dialog.component.html',
  styleUrls: ['./order-returns-delete-attachment-dialog.component.scss'],
})
export class OrderReturnsDeleteAttachmentDialogComponent {
  constructor(@Inject(APP_DIALOG_DATA) public file: File, public dialog: Dialog<OrderReturnsDeleteAttachmentDialogComponent>) {}

  public onClose(deleteAttachmentAccepted: boolean): void {
    this.dialog.close({
      deleteAttachmentAccepted,
    });
  }
}
