import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import {
  IAppState,
  IOrderDetails,
  IOrderLine,
  IOrderLineStatus,
  IShipment,
  IInvoice,
  orderReturnReason,
} from '@app/shared/shared.interfaces';
import { Observable, Subscription } from 'rxjs';
import { select, Store } from '@ngrx/store';
import { getDetails, getSelectedOrderLines } from '@app/features/orders/stores/order-details/order-details.selectors';
import { ToggleOrderLines } from '@app/features/orders/stores/order-details/order-details.actions';
import { Dialog, DialogService } from '@app/core/dialog/dialog.service';
import { OrderReturnsItemsRemoveDialogComponent } from '@app/features/orders/components/order-returns-items-remove-dialog/order-returns-items-remove-dialog.component';
import { take } from 'rxjs/operators';
import { ControlContainer, NgForm } from '@angular/forms';
import * as moment from 'moment';
import { OrdersService } from '@app/core/orders/orders.service';

@Component({
  selector: 'app-order-returns-items',
  templateUrl: './order-returns-items.component.html',
  styleUrls: ['./order-returns-items.component.scss'],
  viewProviders: [{ provide: ControlContainer, useExisting: NgForm }],
})
export class OrderReturnsItemsComponent implements OnInit, OnDestroy {
  @Input() selectedReturnReason: orderReturnReason;
  public selectedOrderLines$: Observable<Array<IOrderLine>>;
  public selectedOrderLines: Array<IOrderLine>;
  private subscription: Subscription = new Subscription();

  public removeItemDialog: Dialog<OrderReturnsItemsRemoveDialogComponent>;
  public details: IOrderDetails;
  public attachmentTotalSize: number = 0;
  public currentLineItem: string;

  private subscriptions: Subscription;
  private details$: Observable<IOrderDetails>;

  constructor(private store: Store<IAppState>, private dialogService: DialogService, private ordersService: OrdersService) {
    this.subscriptions = new Subscription();
    this.getStoreSlices();
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions(): void {
    this.subscriptions.add(this.startSelectedOrderLinesSubscription());
    this.subscriptions.add(this.subscribeToOrderDetails());
  }

  private startSelectedOrderLinesSubscription(): Subscription {
    return this.selectedOrderLines$.subscribe(selectedOrders => {
      this.selectedOrderLines = selectedOrders.filter(orderItem => this.isItemValidForReturn(orderItem));
    });
  }

  private subscribeToOrderDetails(): Subscription {
    return this.details$.subscribe(details => {
      this.details = details;
    });
  }

  private isItemValidForReturn(orderItem: IOrderLine): boolean {
    return (
      (orderItem.status === IOrderLineStatus.Closed ||
        orderItem.status === IOrderLineStatus.Shipped ||
        (orderItem.status === IOrderLineStatus.Open && orderItem.qtyShipped > 0)) &&
      moment(orderItem.shipmentTrackingDate).isAfter(moment().subtract(6, 'M')) &&
      orderItem.ncnrStatus !== undefined &&
      !orderItem.ncnrStatus.valueOf()
    );
  }

  private getStoreSlices(): void {
    this.selectedOrderLines$ = this.store.pipe(select(getSelectedOrderLines));
    this.details$ = this.store.pipe(select(getDetails));
  }

  public checkRemoveOrderLine(orderLine: IOrderLine): void {
    this.removeItemDialog = this.dialogService.open(OrderReturnsItemsRemoveDialogComponent, {
      data: orderLine,
      size: 'medium',
    });
    this.removeItemDialog.afterClosed.pipe(take(1)).subscribe(selection => {
      if (selection.removeItemAccepted) {
        this.removeOrderLine(orderLine);
      }
    });
  }

  public removeOrderLine(orderLine: IOrderLine) {
    const orderLineId = `${orderLine.salesOrderId}_${orderLine.lineItem}`;
    this.ordersService.removeRmaAttachmentsByLineItemId(this.details.salesOrderId, orderLine.lineItem).subscribe(() => {
      this.store.dispatch(new ToggleOrderLines([orderLineId], false));
    });
  }

  public isQuantityAffectedVisible(): boolean {
    return (
      this.selectedReturnReason !== undefined &&
      this.selectedReturnReason !== orderReturnReason.earlyShipment &&
      this.selectedReturnReason !== orderReturnReason.courtesyReturn
    );
  }

  public isQuantityReceivedVisible(): boolean {
    return this.isQuantityAffectedVisible() && this.selectedReturnReason !== orderReturnReason.defectiveParts;
  }

  public isPartNumberReceivedVisible(): boolean {
    return (
      this.selectedReturnReason === orderReturnReason.overShip ||
      this.selectedReturnReason === orderReturnReason.wrongParts ||
      this.selectedReturnReason === orderReturnReason.anotherCustomersParts ||
      this.selectedReturnReason === orderReturnReason.customerRequirementNotFollowed ||
      this.selectedReturnReason === orderReturnReason.issueNotListed
    );
  }

  public onAttachedFile(file: File): void {
    this.attachmentTotalSize += file.size;
  }

  public onRemovedFile(file: File): void {
    this.attachmentTotalSize -= file.size;
  }

  public onFocusLineItem(lineItem: string): void {
    this.currentLineItem = lineItem;
  }

  public displayShipments(shipments: Array<IShipment>): string {
    return shipments.map(shipment => shipment.deliveryId).join(', ');
  }

  public displayInvoices(invoices: Array<IInvoice>): string {
    return invoices.map(invoice => invoice.number).join(', ');
  }
}
