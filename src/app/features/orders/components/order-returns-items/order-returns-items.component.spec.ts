import { OrderReturnsItemsComponent } from '@app/features/orders/components/order-returns-items/order-returns-items.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CoreModule } from '@app/core/core.module';
import { Store, StoreModule } from '@ngrx/store';
import { orderDetailsReducers } from '@app/features/orders/stores/order-details/order-details.reducers';
import { QueryComplete, ToggleOrderLines } from '@app/features/orders/stores/order-details/order-details.actions';
import {
  IOrder,
  IOrderDetails,
  IOrderLine,
  IOrderLineStatus,
  IPurchaseOrderStatus,
  orderReturnReason,
} from '@app/shared/shared.interfaces';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { FormsModule } from '@angular/forms';
import { OrdersService } from '@app/core/orders/orders.service';
import { ApiService } from '@app/core/api/api.service';
import { ApiServiceMock } from '@app/core/api/api.service.spec';
import { of } from 'rxjs/internal/observable/of';

const selectedOrderLine: IOrderLine = {
  id: 1234,
  purchaseOrderNumber: 'PO1234',
  purchaseOrderStatus: IPurchaseOrderStatus.Open,
  lineItem: '1.2.1',
  customerPartNumber: 'BAV99',
  manufacturerPartNumber: 'BAV99',
  manufacturerName: 'Vishay',
  qtyOrdered: 500,
  qtyShipped: 200,
  qtyReadyToShip: 200,
  qtyRemaining: 100,
  requested: new Date(),
  entered: new Date(),
  committed: new Date(),
  status: IOrderLineStatus.Open,
  buyerName: 'David Lane',
  salesHeaderId: 7453868,
  billToSiteUseId: 1234,
  salesOrderId: 'ASDF',
  unitResale: 0,
  extResale: 0,
  currencyCode: 'USD',
  docId: '555_123456',
  itemId: 123456,
  warehouseId: 555,
  shipments: [
    {
      shipmentTrackingDate: new Date(),
      shipmentTrackingReference: '463743369649',
      shipmentTrackingUrl: 'http://fedex.com/Tracking?tracknumbers=463743369649',
      carrier: 'FEDEX',
      qtyShipped: 25,
      deliveryId: 4633346,
      countryOfOrigin: 'CN',
      dateCode: '1804',
    },
    {
      shipmentTrackingDate: new Date(),
      shipmentTrackingReference: '463743201319',
      shipmentTrackingUrl: 'http://fedex.com/Tracking?tracknumbers=463743201319',
      carrier: 'FEDEX',
      qtyShipped: 30,
      deliveryId: 4598682,
      countryOfOrigin: 'PH',
      dateCode: '1807',
    },
  ],
  invoices: [
    {
      number: '123',
      date: '2018-01-01',
      quantity: 10,
      amount: 10,
      url: '/invoices/123/pdf',
    },
    {
      number: '456',
      date: '2018-10-10',
      quantity: 20,
      amount: 20,
      url: '/invoices/456/pdf',
    },
  ],
  ncnrStatus: false,
};

const invalidOrderLine: IOrderLine = {
  id: 1010,
  qtyShipped: 0,
  salesOrderId: 'ASDF',
  lineItem: '1.2.2',
} as IOrderLine;

const mockDetails: IOrderDetails = {
  salesOrderId: 1,
} as IOrderDetails;

describe('OrderReturnsItemsComponent', () => {
  let component: OrderReturnsItemsComponent;
  let fixture: ComponentFixture<OrderReturnsItemsComponent>;
  let store: Store<any>;
  let dialogService: DialogService;
  let ordersService: OrdersService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrderReturnsItemsComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [
        CoreModule,
        StoreModule.forRoot({
          orderDetails: orderDetailsReducers,
        }),
        FormsModule,
      ],
      providers: [
        OrdersService,
        { provide: ApiService, useClass: ApiServiceMock },
        { provide: DialogService, useClass: DialogServiceMock },
      ],
    });
    fixture = TestBed.createComponent(OrderReturnsItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    dialogService = TestBed.get(DialogService);
    ordersService = TestBed.get(OrdersService);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });

  it('#removeOrderLine should remove the related attachments and dispatch a toggle order lines action', () => {
    const isSelected = false;
    const orderLineId = `${selectedOrderLine.salesOrderId}_${selectedOrderLine.lineItem}`;
    const action = new ToggleOrderLines([orderLineId], isSelected);
    spyOn(ordersService, 'removeRmaAttachmentsByLineItemId').and.callFake(() => of({}));
    component.details = mockDetails;
    component.removeOrderLine(selectedOrderLine);
    expect(ordersService.removeRmaAttachmentsByLineItemId).toHaveBeenCalledWith(mockDetails.salesOrderId, selectedOrderLine.lineItem);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('should use the dialog service to open a order return item remove dialog', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.checkRemoveOrderLine(selectedOrderLine);
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('Quantity Affected field should be visible when selected return reason is valid', () => {
    component.selectedReturnReason = orderReturnReason.overShip;
    expect(component.isQuantityAffectedVisible).toBeTruthy();
  });

  it('Quantity Affected field should not be visible when selected return reason is invalid', () => {
    component.selectedReturnReason = orderReturnReason.earlyShipment;
    expect(component.isQuantityAffectedVisible()).toBeFalsy();
  });

  it('Quantity Received field should be visible when selected return reason is valid', () => {
    component.selectedReturnReason = orderReturnReason.overShip;
    expect(component.isQuantityReceivedVisible).toBeTruthy();
  });

  it('Quantity Received field should not be visible when selected return reason is invalid', () => {
    component.selectedReturnReason = orderReturnReason.earlyShipment;
    expect(component.isQuantityReceivedVisible()).toBeFalsy();
  });

  it('Part Number Received field should be visible when selected return reason is valid', () => {
    component.selectedReturnReason = orderReturnReason.overShip;
    expect(component.isPartNumberReceivedVisible()).toBeTruthy();
  });

  it('Part Number Received field should not be visible when selected return reason is invalid', () => {
    component.selectedReturnReason = orderReturnReason.earlyShipment;
    expect(component.isPartNumberReceivedVisible()).toBeFalsy();
  });

  it('should valid fields be visible when selected return reason is shortShip', () => {
    component.selectedReturnReason = orderReturnReason.shortShip;
    expect(component.isQuantityAffectedVisible()).toBeTruthy();
    expect(component.isQuantityReceivedVisible()).toBeTruthy();
    expect(component.isPartNumberReceivedVisible()).toBeFalsy();
  });

  it('should valid fields be visible when selected return reason is defectiveParts', () => {
    component.selectedReturnReason = orderReturnReason.defectiveParts;
    expect(component.isQuantityAffectedVisible()).toBeTruthy();
    expect(component.isQuantityReceivedVisible()).toBeFalsy();
    expect(component.isPartNumberReceivedVisible()).toBeFalsy();
  });

  it('should valid fields be visible when selected return reason is anotherCustomersParts', () => {
    component.selectedReturnReason = orderReturnReason.anotherCustomersParts;
    expect(component.isQuantityAffectedVisible()).toBeTruthy();
    expect(component.isQuantityReceivedVisible()).toBeTruthy();
    expect(component.isPartNumberReceivedVisible()).toBeTruthy();
  });

  it('should filter valid items for return', () => {
    const orderLineId = `${selectedOrderLine.salesOrderId}_${selectedOrderLine.lineItem}`;
    const invalidOrderLineId = `${invalidOrderLine.salesOrderId}_${invalidOrderLine.lineItem}`;
    const order = {
      orderLines: [selectedOrderLine, invalidOrderLine],
    } as IOrder;
    store.dispatch(new QueryComplete(order));
    store.dispatch(new ToggleOrderLines([orderLineId, invalidOrderLineId], true));
    expect(component.selectedOrderLines.length).toBe(1);
    expect(component.selectedOrderLines[0]).toEqual(selectedOrderLine);
  });

  it('should return a csv string with the invoices numbers', () => {
    const invoicesString = component.displayInvoices(selectedOrderLine.invoices);

    expect(invoicesString).toBe('123, 456');
  });

  it('should return a csv string with the deliveries IDs', () => {
    const shipmentsString = component.displayShipments(selectedOrderLine.shipments);

    expect(shipmentsString).toBe('4633346, 4598682');
  });

  it('should set the current line item if the line item has the focus', () => {
    component.onFocusLineItem('1.1');
    expect(component.currentLineItem).toBe('1.1');
  });
});
