import { ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import {
  IFilter,
  IFilterRule,
  IOrderLine,
  ISortCriteron,
  ISortCriteronOrderEnum,
  ISearchCriteria,
  IAppState,
} from '@app/shared/shared.interfaces';
import {
  ChangePage,
  ChangePageLimit,
  ChangeSort,
  SetFilter,
  SubmitFilter,
  ToggleOrderLines,
} from '@app/features/orders/stores/orders/orders.actions';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { debounceTime, take } from 'rxjs/operators';
import {
  getLimit,
  getPage,
  getSelectedIds,
  getSelectedOrderLines,
  getSort,
  getTotalPages,
  getFilter,
  getSearch,
  getHasNcnrOrderLines,
} from '@app/features/orders/stores/orders/orders.selectors';
import * as moment from 'moment';
import { AddToCartDialogComponent, CartType } from '@app/shared/components/add-to-cart-dialog/add-to-cart-dialog.component';
import { DatePickerComponent } from '@app/shared/components/date-picker/date-picker.component';
import { DialogService, Dialog } from '@app/core/dialog/dialog.service';
import { IAddToCartRequestItem } from '@app/core/cart/cart.interfaces';
import { Status } from '@app/shared/locale/i18n.maps';
import { without, difference, uniq } from 'lodash';
import { INITIAL_ORDERS_STATE } from '@app/features/orders/stores/orders/orders.reducers';
import { EmailModalComponent, DEFAULT_MODAL_TITLE, DEFAULT_MODAL_INTRO } from '@app/shared/components/email-modal/email-modal.component';
import { InvoiceListingDialogComponent } from '@app/features/orders/components/invoice-listing-dialog/invoice-listing-dialog.component';
import { IAddToQuoteCartRequestItem } from '@app/core/quote-cart/quote-cart.interfaces';
import { QuoteCartLimit } from '@app/features/quotes/stores/quote-cart.actions';
import { AddToCartCapDialogComponent } from '@app/shared/components/add-to-cart-cap-dialog/add-to-cart-cap-dialog.component';
import { MultipleShippingComponent } from '@app/features/orders/components/multiple-shipping/multiple-shipping.component';
import filters from '@app/features/orders/components/order-lines-listing/order-lines-listing.filters';
import { find } from 'lodash-es';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';
import { AddToCartModalComponent } from '@app/features/orders/components/add-to-cart-modal/add-to-cart-modal.component';
import { getConfirmed } from '@app/features/orders/stores/order-details/order-details.selectors';
import { ConfirmReset } from '@app/features/orders/stores/order-details/order-details.actions';

@Component({
  selector: 'app-order-lines-listing',
  templateUrl: './order-lines-listing.component.html',
  styleUrls: ['./order-lines-listing.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class OrderLinesListingComponent implements OnInit, OnDestroy {
  @Input()
  orderLines: Array<IOrderLine>;
  @Input()
  filter: IFilter;
  @Input()
  quoteCartMaxLineItems: number;
  @Input()
  quoteCartRemainingLineItems: number;

  @ViewChild('datePicker')
  datePicker: DatePickerComponent;

  private capDialog: Dialog<AddToCartCapDialogComponent>;

  public filterOptions = filters;

  public editableFilterValues: Array<Date> = [];

  public orderLineStatusMap: any = Status.orderLineStatusMap;

  public purchaseOrderStatusMap: any = Status.purchaseOrderStatusMap;

  public limit$: Observable<number>;
  public editableFilterIsSelected = false;
  public selectedIds$: Observable<Array<string>>;
  public page$: Observable<number>;
  public totalPages$: Observable<number>;

  public sort$: Observable<Array<ISortCriteron>>;
  public sortOrder: Map<string, ISortCriteronOrderEnum>;

  public selectedOrderLines$: Observable<Array<IOrderLine>>;
  public selectedOrderLines: Array<IOrderLine>;

  public filter$: Observable<IFilter>;
  public search$: Observable<ISearchCriteria>;
  public hasNcnr$: Observable<boolean>;
  public hasNcnr: boolean;
  public confirmCart$: Observable<boolean>;

  public privateFeatureFlags$: Observable<any>;
  public subscription: Subscription = new Subscription();

  public ordersAddToCart: boolean = false;
  public showConfirmation: boolean = false;

  constructor(private store: Store<IAppState>, private dialogService: DialogService, private changeDetector: ChangeDetectorRef) {
    this.getStoreSlices();
  }

  ngOnInit(): void {
    this.startSubscriptions();
    const selectedFilterId = localStorage.getItem('selectedOrderFilter');
    if (selectedFilterId) {
      const value = parseInt(selectedFilterId, 10);
      const filter = find(this.filterOptions, ['value', value]);
      const label = filter.label;
      const track = filter.track;
      const payload = filter.payload;
      this.store.dispatch(new SetFilter({ value, rules: payload }, label, track));
    }
  }

  getSelectedOrderLines(): Subscription {
    return this.selectedOrderLines$.subscribe(selectedOrders => {
      this.selectedOrderLines = selectedOrders;
    });
  }

  getSort(): Subscription {
    return this.sort$.subscribe(updatedSort => {
      this.sortOrder = new Map();
      if (updatedSort.length) {
        updatedSort.forEach(updatedSortItem => {
          this.sortOrder.set(updatedSortItem.key, updatedSortItem.order);
        });
      }
    });
  }

  getSearch(): Subscription {
    return this.search$.subscribe(search => {
      // If the search was reset/same as initial value
      if (search.value === INITIAL_ORDERS_STATE.search.value) {
        // Apply the default filter
        this.setFilter(this.filterOptions[0].value, this.filterOptions[0].payload, false);
      }
    });
  }

  getFilter(): Subscription {
    return this.filter$
      .pipe(
        debounceTime(50) // Added to avoid race conditions between ngrx and the search$ subscription in this component
      )
      .subscribe(filter => {
        this.handleFilterChange(filter);
      });
  }

  getHasNcnr(): Subscription {
    return this.hasNcnr$.subscribe(ncnr => {
      this.hasNcnr = ncnr;
    });
  }

  getPrivateFeatureFlags(): Subscription {
    return this.privateFeatureFlags$.subscribe(featureFlags => {
      this.ordersAddToCart = featureFlags.ordersAddToCart;
      if (featureFlags.ordersExceptions === false) {
        this.filterOptions = filters.slice(0, 7);
      }
    });
  }

  getConfirmation(): Subscription {
    return this.confirmCart$.subscribe(confirmed => {
      if (confirmed === true) {
        this.addToCart(this.selectedOrderLines);
        this.store.dispatch(new ConfirmReset());
      }
    });
  }

  private startSubscriptions(): void {
    this.subscription.add(this.getSelectedOrderLines());
    this.subscription.add(this.getSort());
    this.subscription.add(this.getSearch());
    this.subscription.add(this.getFilter());
    this.subscription.add(this.getHasNcnr());
    this.subscription.add(this.getPrivateFeatureFlags());
    this.subscription.add(this.getConfirmation());

    // Set the page limit
    this.pageLimitChange(INITIAL_ORDERS_STATE.pagination.limit);
    // Set the default sort order
    this.sortChange([
      {
        key: 'purchaseOrderNumber',
        order: ISortCriteronOrderEnum.DESC,
      },
      {
        key: 'lineItem',
        order: ISortCriteronOrderEnum.ASC,
      },
    ]);
  }

  ngOnDestroy(): void {
    if (this.capDialog) {
      this.capDialog.close();
    }
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private getStoreSlices(): void {
    this.limit$ = this.store.pipe(select(getLimit));
    this.page$ = this.store.pipe(select(getPage));
    this.totalPages$ = this.store.pipe(select(getTotalPages));
    this.sort$ = this.store.pipe(select(getSort));
    this.selectedIds$ = this.store.pipe(select(getSelectedIds));
    this.selectedOrderLines$ = this.store.pipe(select(getSelectedOrderLines));
    this.filter$ = this.store.pipe(select(getFilter));
    this.search$ = this.store.pipe(select(getSearch));
    this.hasNcnr$ = this.store.pipe(select(getHasNcnrOrderLines));
    this.privateFeatureFlags$ = this.store.pipe(select(getPrivateFeatureFlagsSelector));
    this.confirmCart$ = this.store.pipe(select(getConfirmed));
  }

  /**
   * Sets a filter in the store and, if none of the filters are editable, submits the filters to trigger a search
   * @param value the index of the filter in the dropdown
   * @param criteria the filters' criteria
   */
  public setFilter(value: number, rules: Array<IFilterRule>, track: boolean = true): void {
    if (rules) {
      // prevent firefox from firing before item is actually selected
      this.store.dispatch(new SetFilter({ value, rules }, this.filterOptions[value].label, track));
    }
  }

  /**
   * Invoked whenever the filter property of the store is updated
   * @param filter the value of the stores' filter property
   */
  private handleFilterChange(filter: IFilter): void {
    // Editable fields only supported on Filters that have a single Criteria Set
    let calculatedEditableFilterValues = [];
    if (filter.rules.length === 1) {
      calculatedEditableFilterValues = filter.rules[0].criteria.map(criteron => {
        if (criteron.editable) {
          return criteron.value ? new Date(criteron.value) : undefined;
        } else {
          return null;
        }
      });
      calculatedEditableFilterValues = without(calculatedEditableFilterValues, null);
    }
    this.editableFilterValues = calculatedEditableFilterValues;
    // Set a flag in the component to hide/show the date selection
    this.editableFilterIsSelected = this.editableFilterValues.length > 0;
    // Determine if any editable criteria are yet to be completed
    const remainingEditableValues = this.editableFilterValues.filter(editableValue => editableValue === undefined);
    // Show the date picker if there are editable values still to select
    if (
      this.editableFilterIsSelected &&
      remainingEditableValues.length > 0 &&
      this.datePicker !== undefined &&
      this.datePicker.open !== undefined
    ) {
      setTimeout(() => {
        this.datePicker.open(0);
        this.changeDetector.markForCheck();
      });
    }
    // If all filters are complete, submit the filter to trigger a search
    if (!remainingEditableValues.length) {
      this.store.dispatch(new SubmitFilter(filter));
    }
    // As this handler is debounced, we need to manually mark the state for a diff check to force the UI update
    this.changeDetector.markForCheck();
  }

  /**
   * Saves the specified page limit, triggering a new search
   * @param pageLimit the number of items to show per page
   */
  public pageLimitChange(pageLimit: number): void {
    this.store.dispatch(new ChangePageLimit(pageLimit));
  }

  /**
   * Converts dates and sets the values of each filter criteria then save the filters in the store
   * @param dates an array of strings, to be converted into Dates
   */
  public setDates(dates: Array<Date>): void {
    // Set the current editable dates
    this.editableFilterValues = dates;
    // Ensure the current filter only has one criteria set
    if (this.filter.rules.length === 1) {
      // Map the dates to the editable values
      this.filter.rules[0].criteria.forEach((editableField, index) => {
        if (editableField.editable) {
          editableField.value = moment(dates[index]).format();
        }
      });
    }
    // Set the filter in the Store
    this.setFilter(this.filter.value, this.filter.rules);
  }

  public isSelected(orderLine: IOrderLine, selectedIds: Array<string>): boolean {
    return selectedIds.includes(`${orderLine.salesOrderId}_${orderLine.lineItem}`);
  }

  public toggleOrderLines(isSelected: boolean, orderLines: Array<IOrderLine>): void {
    const orderLineIds = this.getOrderLineIds(orderLines);
    this.store.dispatch(new ToggleOrderLines(orderLineIds, isSelected));
  }

  private deselectAll(): void {
    this.toggleOrderLines(false, this.orderLines);
  }

  private getOrderLineIds(orderLines: Array<IOrderLine>) {
    return orderLines.map(orderLine => {
      return `${orderLine.salesOrderId}_${orderLine.lineItem}`;
    });
  }

  /**
   * Determines if all lines have been selected
   */
  public allSelected(orderLines: Array<IOrderLine>, selectedIds: Array<string>) {
    if (orderLines && orderLines.length) {
      const allLineIds = uniq(this.getOrderLineIds(orderLines));
      return difference(allLineIds, selectedIds).length <= 0;
    } else {
      return false;
    }
  }

  public pageChange(page: number) {
    this.store.dispatch(new ChangePage(page));
  }

  public sortChange(sortCriteria?: Array<ISortCriteron>): void {
    this.store.dispatch(new ChangeSort(sortCriteria));
  }

  public showInvoicesForOrderLine(orderLine: IOrderLine) {
    this.dialogService.open(InvoiceListingDialogComponent, {
      size: 'large',
      data: {
        orderLines: [orderLine],
      },
    });
  }

  public openEmailModal(orderLines: Array<IOrderLine>): void {
    const dialog = this.dialogService.open(EmailModalComponent, {
      size: 'large',
      data: {
        header: DEFAULT_MODAL_TITLE,
        headerDescription: DEFAULT_MODAL_INTRO,
        subject: 'Question About MyArrow Orders',
        ccSelf: true,
        payload: {
          orderLines: this.selectedOrderLines,
        },
        templateName: 'order-history',
      },
    });

    dialog.afterClosed.pipe(take(1)).subscribe(() => {
      this.deselectAll();
    });
  }

  public addToCart(selectedOrderLines: Array<IOrderLine>): void {
    // Map the selected order lines to AddToCartRequestItems
    const items: Array<IAddToCartRequestItem> = selectedOrderLines.map(selectedOrderLine => {
      return {
        manufacturerPartNumber: selectedOrderLine.manufacturerPartNumber,
        docId: selectedOrderLine.docId,
        itemId: selectedOrderLine.itemId,
        warehouseId: selectedOrderLine.warehouseId,
        quantity: selectedOrderLine.qtyOrdered,
        requestDate: moment(new Date()).format('YYYY-MM-DD'),
        selectedCustomerPartNumber: selectedOrderLine.customerPartNumber,
      };
    });

    const dialog = this.dialogService.open(AddToCartDialogComponent, {
      data: {
        cartType: CartType.SHOPPING_CART,
        items,
      },
      size: 'large',
    });

    dialog.afterClosed.pipe(take(1)).subscribe(() => {
      this.deselectAll();
    });
  }

  public openAddToCartCapDialog(cap: number, cartType: CartType): void {
    this.capDialog = this.dialogService.open(AddToCartCapDialogComponent, {
      data: {
        cap,
        cartType,
      },
      size: 'small',
    });
  }

  public addToQuoteCart(selectedOrderLines: Array<IOrderLine>): void {
    if (this.quoteCartRemainingLineItems === 0 || selectedOrderLines.length > this.quoteCartRemainingLineItems) {
      const requestedItemsCount = this.quoteCartMaxLineItems - this.quoteCartRemainingLineItems + selectedOrderLines.length;
      this.openAddToCartCapDialog(this.quoteCartMaxLineItems, CartType.QUOTE_CART);
      this.store.dispatch(new QuoteCartLimit(requestedItemsCount));
    } else {
      const items: Array<IAddToQuoteCartRequestItem> = selectedOrderLines.map(selectedOrderLine => {
        return {
          docId: selectedOrderLine.docId,
          itemId: selectedOrderLine.itemId,
          warehouseId: selectedOrderLine.warehouseId,
          quantity: selectedOrderLine.qtyOrdered,
          requestDate: moment(new Date()).format('YYYY-MM-DD'),
          selectedCustomerPartNumber: selectedOrderLine.customerPartNumber,
          targetPrice: selectedOrderLine.unitResale,
          manufacturerPartNumber: selectedOrderLine.manufacturerPartNumber,
        };
      });

      const dialog = this.dialogService.open(AddToCartDialogComponent, {
        data: {
          cartType: CartType.QUOTE_CART,
          items,
        },
        size: 'large',
      });

      dialog.afterClosed.pipe(take(1)).subscribe(() => {
        this.deselectAll();
      });
    }
  }

  public onDatePickerClosed(): void {
    this.changeDetector.markForCheck();
  }

  public openMultipleShippingModalDialog(orderLine): void {
    this.dialogService.open(MultipleShippingComponent, {
      size: 'large',
      data: orderLine,
    });
  }

  public openAddToCartConfirmationModal(): void {
    this.dialogService.open(AddToCartModalComponent, {
      size: 'large',
    });
  }
}
