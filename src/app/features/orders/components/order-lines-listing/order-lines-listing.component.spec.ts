import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderLinesListingComponent } from './order-lines-listing.component';
import { NO_ERRORS_SCHEMA, Component } from '@angular/core';
import { DateService } from '@app/shared/services/date.service';
import {
  IFilter,
  IFilterCriteronOperatorEnum,
  ISortCriteronOrderEnum,
  IFilterCriteron,
  IFilterRule,
  IOrderLine,
  IPurchaseOrderStatus,
  IOrderLineStatus,
} from '@app/shared/shared.interfaces';
import {
  ChangePage,
  ChangePageLimit,
  ChangeSort,
  SetFilter,
  SubmitFilter,
  ToggleOrderLines,
} from '@app/features/orders/stores/orders/orders.actions';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { IsoDatePipe } from '@app/shared/pipes/iso-date/iso-date.pipe';
import { DatePipe } from '@angular/common';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import * as moment from 'moment';
import { INITIAL_ORDERS_STATE } from '@app/features/orders/stores/orders/orders.reducers';
import { AddToCartDialogComponent, CartType } from '@app/shared/components/add-to-cart-dialog/add-to-cart-dialog.component';
import { IAddToQuoteCartRequestItem } from '@app/core/quote-cart/quote-cart.interfaces';
import { IAddToCartRequestItem } from '@app/core/cart/cart.interfaces';
import { InvoiceListingDialogComponent } from '@app/features/orders/components/invoice-listing-dialog/invoice-listing-dialog.component';
import { DEFAULT_MODAL_INTRO, DEFAULT_MODAL_TITLE, EmailModalComponent } from '@app/shared/components/email-modal/email-modal.component';

class StoreMock {
  select() {
    return of();
  }
  dispatch() {}
  pipe() {
    return of();
  }
}

class DateServiceMock {
  getMsForDays() {
    return 1000;
  }
  getDate() {
    return '';
  }
}

@Component({
  selector: 'app-date-picker',
  template: '',
})
class DatePickerComponent {
  show() {}
}

const selectedOrderLine: IOrderLine = {
  id: 1234,
  purchaseOrderNumber: 'PO1234',
  purchaseOrderStatus: IPurchaseOrderStatus.Open,
  lineItem: '1.2.1',
  customerPartNumber: 'BAV99',
  manufacturerPartNumber: 'BAV99',
  manufacturerName: 'Vishay',
  qtyOrdered: 500,
  qtyShipped: 200,
  qtyReadyToShip: 200,
  qtyRemaining: 100,
  requested: new Date(),
  entered: new Date(),
  committed: new Date(),
  status: IOrderLineStatus.Open,
  buyerName: 'David Lane',
  salesOrderId: 'ASDF',
  salesHeaderId: 7453868,
  billToSiteUseId: 1234,
  unitResale: 0,
  extResale: 0,
  currencyCode: 'USD',
  docId: '555_123456',
  itemId: 123456,
  warehouseId: 555,
  shipments: [],
  invoices: [],
};

describe('OrderLinesListingComponent', () => {
  let component: OrderLinesListingComponent;
  let fixture: ComponentFixture<OrderLinesListingComponent>;
  let dialogService: DialogService;
  let store: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [OrderLinesListingComponent, IsoDatePipe, DatePickerComponent],
      providers: [
        { provide: DateService, useClass: DateServiceMock },
        { provide: Store, useClass: StoreMock },
        { provide: DialogService, useClass: DialogServiceMock },
        DatePipe,
        DateService,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    });

    // store.dispatch used frequently, including during component construction, so spy on it before construction:
    store = TestBed.get(Store);
    spyOn(store, 'dispatch');

    fixture = TestBed.createComponent(OrderLinesListingComponent);
    dialogService = TestBed.get(DialogService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    jasmine.clock().uninstall();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set the page limit when initted', () => {
    // Pagination
    const limitAction = new ChangePageLimit(INITIAL_ORDERS_STATE.pagination.limit);
    const sortAction = new ChangeSort([
      {
        key: 'purchaseOrderNumber',
        order: ISortCriteronOrderEnum.DESC,
      },
      {
        key: 'lineItem',
        order: ISortCriteronOrderEnum.ASC,
      },
    ]);
    expect(store.dispatch).toHaveBeenCalledWith(limitAction);
    expect(store.dispatch).toHaveBeenCalledWith(sortAction);
  });

  xit('#setFilter should dispatch an event with the provided filter', () => {
    const dummyFilter: IFilter = {
      value: 0,
      rules: [
        {
          criteria: [
            {
              key: 'foo',
              value: 'bar',
              operator: IFilterCriteronOperatorEnum.Equals,
            },
          ],
        },
      ],
    };
    const setFilterAction = new SetFilter(dummyFilter);
    component.setFilter(dummyFilter.value, dummyFilter.rules);
    expect(store.dispatch).toHaveBeenCalledWith(setFilterAction);
  });

  it('#pageLimitChange should dispatch an event on the store with the provided pageLimit', () => {
    const pageLimit = 50;
    const action = new ChangePageLimit(pageLimit);
    component.pageLimitChange(pageLimit);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  describe('Handling filter changes', () => {
    const testCases = [
      {
        description: 'should dispatch a SubmitFilter action if there are no editable criteria',
        filter: {
          value: 0,
          rules: [
            {
              criteria: [
                {
                  editable: false,
                } as IFilterCriteron,
              ],
            },
          ],
        },
        result: {
          editableFilterIsSelected: false,
          editableFilterValues: [],
          submitFilterActionDispatched: true,
        },
      },
      {
        description: 'should dispatch a SubmitFilter action if the active filter has more than one filter rule',
        filter: {
          value: 0,
          rules: [{} as IFilterRule, {} as IFilterRule],
        },
        result: {
          editableFilterIsSelected: false,
          editableFilterValues: [],
          submitFilterActionDispatched: true,
        },
      },
      {
        description: 'should not dispatch a SubmitFilter action if there are editable filter criteria',
        filter: {
          value: 0,
          rules: [
            {
              criteria: [
                {
                  editable: true,
                  value: undefined,
                },
              ],
            } as IFilterRule,
          ],
        },
        result: {
          editableFilterIsSelected: true,
          editableFilterValues: [undefined],
          submitFilterActionDispatched: false,
        },
      },
      {
        description: 'should dispatch a SubmitFilter action if the editable filter had previously been selected',
        filter: {
          value: 0,
          rules: [
            {
              criteria: [
                {
                  editable: true,
                  value: '2018-01-01',
                } as IFilterCriteron,
              ],
            } as IFilterRule,
          ],
        },
        result: {
          editableFilterIsSelected: true,
          editableFilterValues: [new Date('2018-01-01')],
          submitFilterActionDispatched: true,
        },
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        // Stub the select method
        spyOn(store, 'pipe').and.callFake(() => {
          return of(testCase.filter);
        });
        jasmine.createSpy('date-picker-show');
        // Re-create the component
        fixture = TestBed.createComponent(OrderLinesListingComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
        // Expectations
        expect(component.editableFilterIsSelected).toEqual(testCase.result.editableFilterIsSelected);
        expect(component.editableFilterValues).toEqual(testCase.result.editableFilterValues);
        // if (testCase.result.editableFilterValues.filter(val => val === undefined).length > 0) {
        //   expect(datePickerShowSpy).toHaveBeenCalled();
        // } else {
        //   expect(datePickerShowSpy).not.toHaveBeenCalled();
        // }
        // If necessary, test that the SubmitFilter was called
        const submitFilterAction = new SubmitFilter(testCase.filter);
        if (!testCase.result.submitFilterActionDispatched) {
          expect(store.dispatch).not.toHaveBeenCalledWith(submitFilterAction);
        } else {
          expect(store.dispatch).toHaveBeenCalledWith(submitFilterAction);
        }
      });
    }
  });

  describe('#setDates', () => {
    const testCases = [
      {
        description: 'should set dates on the current criteria',
        criteria: [
          {
            key: 'foo',
            value: undefined,
            operator: IFilterCriteronOperatorEnum.Equals,
            editable: true,
          },
          {
            key: 'bar',
            value: undefined,
            operator: IFilterCriteronOperatorEnum.Equals,
            editable: true,
          },
        ],
        dates: [new Date('2018-01-01T00:00:00Z'), new Date('2019-01-01T00:00:00Z')],
        result: [moment('2018-01-01T00:00:00Z').format(), moment('2019-01-01T00:00:00Z').format()],
      },
      {
        description: 'should not set the dates if the filters are not configured as editable',
        criteria: [
          {
            key: 'foo',
            value: moment('1900-01-01T00:00:00Z').format(),
            operator: IFilterCriteronOperatorEnum.Equals,
          },
          {
            key: 'bar',
            value: moment('1901-01-01T00:00:00Z').format(),
            operator: IFilterCriteronOperatorEnum.Equals,
          },
        ],
        dates: [new Date('2018-01-01T00:00:00Z'), new Date('2019-01-01T00:00:00Z')],
        result: [moment('1900-01-01T00:00:00Z').format(), moment('1901-01-01T00:00:00Z').format()],
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        spyOn(component, 'setFilter');
        component.filter = {
          value: 0,
          rules: [
            {
              criteria: testCase.criteria,
            },
          ],
        };
        component.setDates(testCase.dates);
        component.filter.rules[0].criteria.forEach((criteron, index) => {
          expect(criteron.value).toEqual(testCase.result[index]);
        });
      });
    }
  });

  describe('#isSelected', () => {
    it('should return true if the provided orderLine is in the selection', () => {
      const salesOrderId = 'xxx';
      const lineItem = '1.2.3';
      const orderLine = { salesOrderId, lineItem };
      const orderLineId = `${salesOrderId}_${lineItem}`;
      const result = component.isSelected(orderLine as any, [orderLineId]);
      expect(result).toBeTruthy();
    });
    it('should return false if the provided orderLine is not in the selection', () => {
      const salesOrderId = 'xxx';
      const lineItem = '1.2.3';
      const orderLine = { salesOrderId, lineItem };
      const result = component.isSelected(orderLine as any, []);
      expect(result).toBeFalsy();
    });
  });

  it('#toggleOrderLines should map the provided orderLines to an array of Ids and dispatch a ToggleOrderLines action to the store', () => {
    const salesOrderId = 'xxx';
    const lineItem = '1.2.3';
    const orderLineId = `${salesOrderId}_${lineItem}`;
    const orderLine = { salesOrderId, lineItem };
    const orderLines = [orderLine];
    const operation = false;
    const action = new ToggleOrderLines([orderLineId], operation);
    component.toggleOrderLines(operation, orderLines as Array<any>);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('#pageChange should dispatch an event on the store with the provided page', () => {
    const targetPage = 14;
    const action = new ChangePage(targetPage);
    component.pageChange(targetPage);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  describe('#allSelected', () => {
    const testCases = [
      {
        description: `should return false if orderLines is undefined`,
        orderLines: undefined,
        selectedIds: [],
        result: false,
      },
      {
        description: `should return false if orderLines is defined but empty`,
        orderLines: [],
        selectedIds: [],
        result: false,
      },
      {
        description: `should return false if there are unselected orderLines`,
        orderLines: [
          {
            salesOrderId: '1',
            lineItem: '1',
          },
          {
            salesOrderId: '2',
            lineItem: '2',
          },
        ],
        selectedIds: ['1_1'],
        result: false,
      },
      {
        description: `should return true if all orderLines are selected`,
        orderLines: [
          {
            salesOrderId: '1',
            lineItem: '1',
          },
          {
            salesOrderId: '2',
            lineItem: '2',
          },
        ],
        selectedIds: ['1_1', '2_2'],
        result: true,
      },
      {
        description: `should return true there were duplicated orderLines`,
        orderLines: [
          {
            salesOrderId: '1',
            lineItem: '1',
          },
          {
            salesOrderId: '1',
            lineItem: '1',
          },
        ],
        selectedIds: ['1_1', '1_1'],
        result: true,
      },
    ];

    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        const result = component.allSelected(testCase.orderLines as Array<IOrderLine>, testCase.selectedIds);
        expect(result).toEqual(testCase.result);
      });
    }
  });

  it('should call `dialogService` on `addToCart()', () => {
    jasmine.clock().install();

    const addToCartRequestItem: IAddToCartRequestItem = {
      manufacturerPartNumber: 'BAV99',
      docId: '555_123456',
      itemId: 123456,
      warehouseId: 555,
      quantity: 500,
      requestDate: moment(new Date()).format('YYYY-MM-DD'),
      selectedCustomerPartNumber: 'BAV99',
    };

    spyOn(dialogService, 'open').and.callThrough();
    component.addToCart([selectedOrderLine]);
    expect(dialogService.open).toHaveBeenCalledWith(AddToCartDialogComponent, {
      data: {
        cartType: CartType.SHOPPING_CART,
        items: [addToCartRequestItem],
      },
      size: 'large',
    });

    jasmine.clock().uninstall();
  });

  it('should call `dialogService` on `addToQuoteCart()', () => {
    jasmine.clock().install();

    const addToQuoteCartRequestItem: IAddToQuoteCartRequestItem = {
      docId: '555_123456',
      itemId: 123456,
      warehouseId: 555,
      quantity: 500,
      requestDate: moment(new Date()).format('YYYY-MM-DD'),
      selectedCustomerPartNumber: 'BAV99',
      targetPrice: 0,
      manufacturerPartNumber: 'BAV99',
    };

    spyOn(dialogService, 'open').and.callThrough();
    component.addToQuoteCart([selectedOrderLine]);
    expect(dialogService.open).toHaveBeenCalledWith(AddToCartDialogComponent, {
      data: {
        cartType: CartType.QUOTE_CART,
        items: [addToQuoteCartRequestItem],
      },
      size: 'large',
    });

    jasmine.clock().uninstall();
  });

  it('should call `dialogService` on `showInvoicesForOrderLine()', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.showInvoicesForOrderLine(selectedOrderLine);
    expect(dialogService.open).toHaveBeenCalledWith(InvoiceListingDialogComponent, {
      data: {
        orderLines: [selectedOrderLine],
      },
      size: 'large',
    });
  });

  it('should call `dialogService` on `openEmailModal()', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.openEmailModal([selectedOrderLine]);
    expect(dialogService.open).toHaveBeenCalledWith(EmailModalComponent, {
      data: {
        header: DEFAULT_MODAL_TITLE,
        headerDescription: DEFAULT_MODAL_INTRO,
        subject: 'Question About MyArrow Orders',
        ccSelf: true,
        payload: {
          orderLines: this.selectedOrderLines,
        },
        templateName: 'order-history',
      },
      size: 'large',
    });
  });

  it('should call `changeDetector` on `onDatePickerClosed()`', () => {
    const spy = spyOn((component as any).changeDetector, 'markForCheck').and.callThrough();
    component.onDatePickerClosed();
    expect(spy).toHaveBeenCalled();
  });

  it('should call `openAddToCartCapDialog` when addToQuoteCart and max cap has been reached', () => {
    spyOn(component, 'openAddToCartCapDialog').and.callThrough();
    component.quoteCartRemainingLineItems = 0;
    component.addToQuoteCart([selectedOrderLine]);
    expect(component.openAddToCartCapDialog).toHaveBeenCalled();
  });

  it('should call `dialogService` on `openMultipleShippingModalDialog()', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.openMultipleShippingModalDialog(selectedOrderLine);
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('should call `dialogService` on `openMultipleShippingModalDialog()', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.openAddToCartConfirmationModal();
    expect(dialogService.open).toHaveBeenCalled();
  });
});
