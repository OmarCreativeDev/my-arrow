import { IFilterCriteronOperatorEnum, IOrderLineStatus } from '@app/shared/shared.interfaces';
import * as moment from 'moment';
// ISelectableOption<Array<IFilterRule>>
export default [
  {
    label: 'All open orders + last 7 days shipped',
    value: 0,
    track: true,
    payload: [
      {
        criteria: [
          {
            key: 'status',
            value: IOrderLineStatus.Open,
            operator: IFilterCriteronOperatorEnum.Equals,
          },
        ],
      },
      {
        criteria: [
          {
            key: 'status',
            value: IOrderLineStatus.Shipped,
            operator: IFilterCriteronOperatorEnum.Equals,
          },
          {
            key: 'shipmentTrackingDate',
            value: moment()
              .startOf('day')
              .format(),
            operator: IFilterCriteronOperatorEnum.LessThanOrEqualTo,
          },
          {
            key: 'shipmentTrackingDate',
            value: moment()
              .startOf('day')
              .subtract(7, 'days')
              .format(),
            operator: IFilterCriteronOperatorEnum.GreaterThanOrEqualTo,
          },
        ],
      },
    ],
  },
  {
    label: 'All Orders Shipped in Last 30 Days',
    value: 1,
    track: true,
    payload: [
      {
        criteria: [
          {
            key: 'shipmentTrackingDate',
            value: moment()
              .startOf('day')
              .format(),
            operator: IFilterCriteronOperatorEnum.LessThanOrEqualTo,
          },
          {
            key: 'shipmentTrackingDate',
            value: moment()
              .startOf('day')
              .subtract(30, 'days')
              .format(),
            operator: IFilterCriteronOperatorEnum.GreaterThanOrEqualTo,
          },
          {
            key: 'status',
            value: IOrderLineStatus.Shipped,
            operator: IFilterCriteronOperatorEnum.Equals,
          },
        ],
      },
    ],
  },
  {
    label: 'All Orders Due in Next 30 Days',
    value: 2,
    track: true,
    payload: [
      {
        criteria: [
          {
            key: 'committed',
            value: moment()
              .startOf('day')
              .add(30, 'days')
              .format(),
            operator: IFilterCriteronOperatorEnum.LessThanOrEqualTo,
          },
          {
            key: 'committed',
            value: moment()
              .startOf('day')
              .format(),
            operator: IFilterCriteronOperatorEnum.GreaterThanOrEqualTo,
          },
        ],
      },
    ],
  },
  {
    label: 'All Orders Placed in Last 30 Days',
    value: 3,
    track: true,
    payload: [
      {
        criteria: [
          {
            key: 'entered',
            value: moment()
              .startOf('day')
              .subtract(30, 'days')
              .format(),
            operator: IFilterCriteronOperatorEnum.GreaterThanOrEqualTo,
          },
          {
            key: 'entered',
            value: moment()
              .startOf('day')
              .format(),
            operator: IFilterCriteronOperatorEnum.LessThanOrEqualTo,
          },
        ],
      },
    ],
  },
  {
    label: 'All Open Orders (Backlog)',
    value: 4,
    track: true,
    payload: [
      {
        criteria: [
          {
            key: 'status',
            value: IOrderLineStatus.Open,
            operator: IFilterCriteronOperatorEnum.Equals,
          },
        ],
      },
    ],
  },
  {
    label: 'All Ship Dates in Selected Range',
    value: 5,
    track: false,
    payload: [
      {
        criteria: [
          {
            key: 'shipmentTrackingDate',
            value: undefined,
            operator: IFilterCriteronOperatorEnum.GreaterThanOrEqualTo,
            editable: true,
          },
          {
            key: 'shipmentTrackingDate',
            value: undefined,
            operator: IFilterCriteronOperatorEnum.LessThanOrEqualTo,
            editable: true,
          },
          {
            key: 'status',
            value: IOrderLineStatus.Shipped,
            operator: IFilterCriteronOperatorEnum.Equals,
          },
        ],
      },
    ],
  },
  {
    label: 'All Orders Placed in Selected Date Range',
    value: 6,
    track: false,
    payload: [
      {
        criteria: [
          {
            key: 'entered',
            value: undefined,
            operator: IFilterCriteronOperatorEnum.GreaterThanOrEqualTo,
            editable: true,
          },
          {
            key: 'entered',
            value: undefined,
            operator: IFilterCriteronOperatorEnum.LessThanOrEqualTo,
            editable: true,
          },
        ],
      },
    ],
  },
  {
    label: 'All Orders Invoiced in Selected Date Range',
    value: 7,
    track: false,
    payload: [
      {
        criteria: [
          {
            key: 'invoices.invoiced',
            value: undefined,
            operator: IFilterCriteronOperatorEnum.GreaterThanOrEqualTo,
            editable: true,
          },
          {
            key: 'invoices.invoiced',
            value: undefined,
            operator: IFilterCriteronOperatorEnum.LessThanOrEqualTo,
            editable: true,
          },
        ],
      },
    ],
  },
  {
    label: 'All Delays',
    value: 8,
    track: true,
    payload: [
      {
        criteria: [
          {
            key: 'businessCriteriaFilter',
            value: 'delayed',
            operator: IFilterCriteronOperatorEnum.Equals,
          },
          {
            key: 'status',
            value: 'OPEN',
            operator: IFilterCriteronOperatorEnum.Equals,
          },
          {
            key: 'requested',
            value: moment()
              .startOf('day')
              .add(30, 'days')
              .format(),
            operator: IFilterCriteronOperatorEnum.LessThanOrEqualTo,
          },
          {
            key: 'requested',
            value: moment()
              .startOf('day')
              .format(),
            operator: IFilterCriteronOperatorEnum.GreaterThanOrEqualTo,
          },
        ],
      },
    ],
  },
  {
    label: 'Past Due',
    value: 9,
    track: true,
    payload: [
      {
        criteria: [
          {
            key: 'businessCriteriaFilter',
            value: 'pastDue',
            operator: IFilterCriteronOperatorEnum.Equals,
          },
        ],
      },
    ],
  },
];
