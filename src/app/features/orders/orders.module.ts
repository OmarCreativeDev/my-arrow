import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { OrdersPageComponent } from './pages/orders/orders-page.component';
import { SharedModule } from '@app/shared/shared.module';
import { OrderLinesListingComponent } from './components/order-lines-listing/order-lines-listing.component';
import { OrderLinesSearchComponent } from './components/order-lines-search/order-lines-search.component';
import { FormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { ordersAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.orders';
import { OrdersEffects } from './stores/orders/orders.effects';
import { EffectsModule } from '@ngrx/effects';
import { OrderDetailsComponent } from '@app/features/orders/pages/order-details/order-details.component';
import { OrderDetailsMetaComponent } from '@app/features/orders/components/order-details-meta/order-details-meta.component';
import { SingleOrderLinesListingComponent } from './components/single-order-lines-listing/single-order-lines-listing.component';
import { ShipmentTrackingDialogComponent } from './components/shipment-tracking-dialog/shipment-tracking-dialog.component';
import { orderDetailsReducers } from './stores/order-details/order-details.reducers';
import { OrderDetailsEffects } from './stores/order-details/order-details.effects';
import { DownloadOrdersComponent } from './components/download-orders/download-orders.component';
import { InvoiceListingDialogComponent } from './components/invoice-listing-dialog/invoice-listing-dialog.component';
import { DialogModule } from '@app/core/dialog/dialog-module';
import { EditRequestedDateDialogComponent } from './components/edit-requested-date-dialog/edit-requested-date-dialog.component';
import { OrderReturnsComponent } from '@app/features/orders/pages/order-returns/order-returns.component';
import { OrderReturnsHeaderComponent } from '@app/features/orders/components/order-returns-header/order-returns-header.component';
import { OrderReturnsItemsComponent } from '@app/features/orders/components/order-returns-items/order-returns-items.component';
import { OrderReturnsItemsRemoveDialogComponent } from '@app/features/orders/components/order-returns-items-remove-dialog/order-returns-items-remove-dialog.component';
import { OrderReturnsSubmitDialogComponent } from '@app/features/orders/components/order-returns-submit-dialog/order-returns-submit-dialog.component';
import { OrderReturnsAttachmentsComponent } from '@app/features/orders/components/order-returns-attachments/order-returns-attachments.component';
import { OrderReturnsDeleteAttachmentDialogComponent } from '@app/features/orders/components/order-returns-delete-attachment-dialog/order-returns-delete-attachment-dialog.component';
import { AddToCartModalComponent } from './components/add-to-cart-modal/add-to-cart-modal.component';

@NgModule({
  imports: [
    CommonModule,
    OrdersRoutingModule,
    SharedModule,
    FormsModule,
    StoreModule.forFeature('orders', ordersAnalyticsMetaReducers),
    StoreModule.forFeature('orderDetails', orderDetailsReducers),
    EffectsModule.forFeature([OrdersEffects, OrderDetailsEffects]),
    DialogModule,
  ],
  declarations: [
    OrdersPageComponent,
    OrderLinesListingComponent,
    OrderLinesSearchComponent,
    OrderDetailsComponent,
    OrderDetailsMetaComponent,
    SingleOrderLinesListingComponent,
    ShipmentTrackingDialogComponent,
    DownloadOrdersComponent,
    InvoiceListingDialogComponent,
    EditRequestedDateDialogComponent,
    OrderReturnsComponent,
    OrderReturnsHeaderComponent,
    OrderReturnsItemsComponent,
    OrderReturnsItemsRemoveDialogComponent,
    OrderReturnsSubmitDialogComponent,
    OrderReturnsAttachmentsComponent,
    OrderReturnsDeleteAttachmentDialogComponent,
    AddToCartModalComponent,
  ],
  exports: [OrderLinesSearchComponent],
  entryComponents: [
    DownloadOrdersComponent,
    InvoiceListingDialogComponent,
    ShipmentTrackingDialogComponent,
    EditRequestedDateDialogComponent,
    OrderReturnsItemsRemoveDialogComponent,
    OrderReturnsSubmitDialogComponent,
    OrderReturnsDeleteAttachmentDialogComponent,
    AddToCartModalComponent,
  ],
})
export class OrdersModule {}
