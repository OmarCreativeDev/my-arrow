import { IListingSearchState, IOrderLine } from '@app/shared/shared.interfaces';
import uniq from 'lodash-es/uniq';
import without from 'lodash-es/without';

import { OrdersActions, OrdersActionTypes } from './orders.actions';

export const INITIAL_ORDERS_STATE: IListingSearchState<IOrderLine> = {
  loading: true,
  error: undefined,
  search: {
    type: '',
    value: '',
  },
  filter: {
    value: undefined,
    rules: [],
  },
  pagination: {
    limit: 10,
    current: 1,
    total: 0,
  },
  items: undefined,
  sort: [],
  selectedIds: [],
};

/**
 * Mutates the state for given set of OrdersActions
 * @param state
 * @param action
 */
export function ordersReducers(
  state: IListingSearchState<IOrderLine> = INITIAL_ORDERS_STATE,
  action: OrdersActions
): IListingSearchState<IOrderLine> {
  switch (action.type) {
    case OrdersActionTypes.SUBMIT_SEARCH: {
      return {
        ...state,
        search: action.payload,
      };
    }

    case OrdersActionTypes.SUBMIT_FILTER: {
      return {
        ...state,
        filter: action.payload,
      };
    }

    case OrdersActionTypes.SET_FILTER: {
      return {
        ...state,
        filter: action.payload,
      };
    }

    case OrdersActionTypes.QUERY: {
      return {
        ...state,
        error: undefined,
        loading: true,
        items: undefined,
        selectedIds: [],
      };
    }

    case OrdersActionTypes.QUERY_COMPLETE: {
      return {
        ...state,
        items: action.payload.orderLines,
        pagination: {
          ...state.pagination,
          current: action.payload.paging ? action.payload.paging.currentPage : INITIAL_ORDERS_STATE.pagination.current,
          total: action.payload.paging ? action.payload.paging.totalPages : INITIAL_ORDERS_STATE.pagination.total,
        },
        loading: false,
      };
    }

    case OrdersActionTypes.QUERY_ERROR: {
      return {
        ...state,
        error: action.payload,
        items: undefined,
        loading: false,
        pagination: {
          limit: state.pagination.limit,
          current: 1,
          total: 0,
        },
      };
    }

    case OrdersActionTypes.CHANGE_PAGE_LIMIT: {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          limit: action.payload,
        },
      };
    }

    case OrdersActionTypes.CHANGE_PAGE: {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          current: action.payload,
        },
      };
    }

    case OrdersActionTypes.CHANGE_SORT: {
      return {
        ...state,
        sort: action.payload ? action.payload : [],
      };
    }

    case OrdersActionTypes.SELECT_ORDER_LINES: {
      let selectedIds = [...state.selectedIds];

      if (action.select) {
        selectedIds.push(...action.payload);
      } else {
        selectedIds = without(selectedIds, ...action.payload);
      }

      return {
        ...state,
        selectedIds: uniq(selectedIds),
      };
    }

    case OrdersActionTypes.CLEAR_FILTER: {
      return {
        ...state,
        filter: INITIAL_ORDERS_STATE.filter,
      };
    }

    case OrdersActionTypes.REQUEST_RETURN_ORDER:
    case OrdersActionTypes.SUBMIT_REQUEST_RETURN_ORDER:
    default: {
      return state;
    }
  }
}
