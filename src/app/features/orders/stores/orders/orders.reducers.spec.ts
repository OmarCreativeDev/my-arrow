import {
  Query,
  QueryComplete,
  QueryError,
  SubmitSearch,
  SubmitFilter,
  ChangePageLimit,
  SetFilter,
  ToggleOrderLines,
  RequestReturnOrder,
  SubmitRequestReturnOrder,
} from './orders.actions';
import { ordersReducers, INITIAL_ORDERS_STATE } from './orders.reducers';
import { IOrderLine, IFilterCriteronOperatorEnum, IListingSearchState, IHttpException } from '@app/shared/shared.interfaces';
import { IOrderLineQueryResponse, OrderServiceHttpExceptionStatusEnum } from '@app/core/orders/orders.interfaces';
import { HttpErrorResponse } from '@angular/common/http';

describe('Orders Reducers', () => {
  let initialState: IListingSearchState<IOrderLine>;

  beforeEach(() => {
    initialState = {
      search: {
        value: '',
        type: '',
      },
      filter: {
        value: undefined,
        rules: [],
      },
      pagination: {
        limit: 10,
        current: 2,
        total: 48,
      },
      loading: true,
      items: undefined,
      error: undefined,
      selectedIds: [],
    };
  });

  it(`SUBMIT_SEARCH should set the value of the search`, () => {
    const searchCriteria = {
      value: 'foo',
      type: 'bar',
    };
    const action = new SubmitSearch(searchCriteria);
    const result = ordersReducers(initialState, action);
    expect(result.search).toEqual(searchCriteria);
  });

  it(`SUBMIT_FILTERS should set the value of the filters`, () => {
    const filter = {
      value: 0,
      rules: [
        {
          criteria: [
            {
              value: 'foo',
              key: 'bar',
              operator: IFilterCriteronOperatorEnum.Equals,
            },
          ],
        },
      ],
    };
    const action = new SubmitFilter(filter);
    const result = ordersReducers(initialState, action);
    expect(result.filter).toEqual(filter);
  });

  it(`SET_FILTERS should set the valie of the filter`, () => {
    const filter = {
      value: 0,
      rules: [
        {
          criteria: [
            {
              value: 'foo',
              key: 'bar',
              operator: IFilterCriteronOperatorEnum.Equals,
            },
          ],
        },
      ],
    };
    const action = new SetFilter(filter);
    const result = ordersReducers(initialState, action);
    expect(result.filter).toEqual(filter);
  });

  it(`QUERY should clear any errors and any items, set loading to true and reset the selection`, () => {
    const query = {
      search: {
        type: 'foo',
        value: 'bar',
      },
      filter: [
        {
          criteria: [
            {
              value: 'foo',
              key: 'bar',
              operator: IFilterCriteronOperatorEnum.Equals,
            },
          ],
        },
      ],
      paging: {
        limit: 10,
        page: 2,
      },
    };
    const action = new Query(query);
    const result = ordersReducers(initialState, action);
    expect(result.loading).toEqual(true);
    expect(result.error).toEqual(undefined);
    expect(result.items).toEqual(undefined);
    expect(result.selectedIds.length).toEqual(0);
  });

  it(`SEARCH_COMPLETE should set the orderLines of the state, change loading to false and not have an error property`, () => {
    const response: IOrderLineQueryResponse = {
      orderLines: [],
      paging: {
        currentPage: 1,
        totalPages: 100,
      },
    };
    const action = new QueryComplete(response);
    const result = ordersReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.items).toEqual(response.orderLines);
    expect(result.pagination.total).toEqual(response.paging.totalPages);
    expect(result.pagination.current).toEqual(response.paging.currentPage);
  });

  it(`SEARCH_ERROR orderLines should be undefined and error property should have an error property`, () => {
    const expectedError = {
      error: {
        error: 'foo',
        status: OrderServiceHttpExceptionStatusEnum.BAD_REQUEST,
      } as IHttpException,
    } as HttpErrorResponse;
    const action = new QueryError(expectedError);
    const result = ordersReducers(initialState, action);
    expect(result.error).toEqual(expectedError);
    expect(result.items).toBeUndefined();
  });

  it(`CHANGE_PAGE_LIMIT should be update the query limit`, () => {
    const dummyPageLimit = 50;
    const action = new ChangePageLimit(dummyPageLimit);
    const result = ordersReducers(initialState, action);
    expect(result.pagination.limit).toEqual(dummyPageLimit);
  });

  it(`SELECT_ORDER_LINES should add the provided orderLines to the selection`, () => {
    const salesOrderId = 'xxx';
    const lineItem = '1.2.2';
    const orderLineId = `${salesOrderId}_${lineItem}`;
    const orderLines = [orderLineId];
    const expectedIds = [`${salesOrderId}_${lineItem}`];
    const action = new ToggleOrderLines(orderLines, true);
    const result = ordersReducers(initialState, action);
    expect(result.selectedIds).toEqual(expectedIds);
  });

  it(`SELECT_ORDER_LINES should remove the provided orderLines from the selection`, () => {
    const salesOrderId = 'xxx';
    const lineItem = '1.2.2';
    const orderLineId = `${salesOrderId}_${lineItem}`;
    const orderLines = [orderLineId];
    initialState.selectedIds = [orderLineId];
    const action = new ToggleOrderLines(orderLines, false);
    const result = ordersReducers(initialState, action);
    expect(result.selectedIds).toEqual([]);
  });

  it(`SELECT_ORDER_LINES should only return unique values in the selectedIds array`, () => {
    const salesOrderId = 'xxx';
    const lineItem = '1.2.2';
    const orderLineId = `${salesOrderId}_${lineItem}`;
    const orderLines = [orderLineId];
    initialState.selectedIds = [orderLineId];
    const action = new ToggleOrderLines(orderLines, true);
    const result = ordersReducers(initialState, action);
    expect(result.selectedIds.length).toEqual(orderLines.length);
    expect(result.selectedIds).toEqual(orderLines);
  });

  it(`REQUEST_RETURN_ORDER should return the state without transforming it`, () => {
    const action = new RequestReturnOrder();
    const result = ordersReducers(initialState, action);
    expect(result.selectedIds).toEqual(INITIAL_ORDERS_STATE.selectedIds);
  });

  it(`SUBMIT_REQUEST_RETURN_ORDER should return the state without transforming it`, () => {
    const returnReason: string = 'Damaged';
    const action = new SubmitRequestReturnOrder(returnReason);
    const result = ordersReducers(initialState, action);
    expect(result.selectedIds).toEqual(INITIAL_ORDERS_STATE.selectedIds);
  });

  it(`by default should return the state without transforming it`, () => {
    const result = ordersReducers(undefined, {} as any);
    expect(result).toEqual(INITIAL_ORDERS_STATE);
  });
});
