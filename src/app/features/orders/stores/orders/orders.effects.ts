import { Injectable } from '@angular/core';
import { Observable, of, combineLatest, merge } from 'rxjs';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { OrdersService } from '@app/core/orders/orders.service';

import { debounceTime, switchMap, map, catchError } from 'rxjs/operators';

import {
  OrdersActionTypes,
  QueryError,
  QueryComplete,
  Query,
  ChangePage,
  SubmitSearch,
  ClearFilter,
  RetainFilter,
} from '@app/features/orders/stores/orders/orders.actions';
import { IOrderLineQueryResponse } from '@app/core/orders/orders.interfaces';

import { INITIAL_ORDERS_STATE } from '@app/features/orders/stores/orders/orders.reducers';

import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class OrdersEffects {
  /**
   * Side-effect that combines all side effects for these facets:
   *  - Search
   *  - Filters
   *  - Limit
   *  - Page
   */
  @Effect()
  facets$: Observable<any> = combineLatest(
    this.actions$.pipe(ofType(OrdersActionTypes.SUBMIT_SEARCH)),
    this.actions$.pipe(ofType(OrdersActionTypes.SUBMIT_FILTER)),
    this.actions$.pipe(ofType(OrdersActionTypes.CHANGE_PAGE_LIMIT)),
    this.actions$.pipe(ofType(OrdersActionTypes.CHANGE_PAGE)),
    this.actions$.pipe(ofType(OrdersActionTypes.CHANGE_SORT))
  ).pipe(
    debounceTime(50),
    map((actions: any) => {
      const payloads = actions.map(action => action.payload);
      return new Query({
        search: payloads[0],
        filter: payloads[1].rules,
        paging: {
          limit: payloads[2],
          page: payloads[3],
        },
        sort: payloads[4],
      });
    })
  );

  /**
   * Side-effect for the Query being changed directly. Trigger by the facets$ Side-effect
   */
  @Effect()
  query$: Observable<any> = this.actions$.pipe(
    ofType(OrdersActionTypes.QUERY),
    switchMap((action: Query) => {
      return this.ordersService.orderLinesSearch(action.payload).pipe(
        map((queryResponse: IOrderLineQueryResponse) => new QueryComplete(queryResponse)),
        catchError((errorResponse: HttpErrorResponse) => of(new QueryError(errorResponse)))
      );
    })
  );

  /**
   * Reset the filter if the search value was changed
   */
  @Effect()
  resetFilterOnSearch$ = this.actions$.pipe(
    ofType(OrdersActionTypes.SUBMIT_SEARCH),
    map((action: SubmitSearch) => {
      if (action.payload.value !== INITIAL_ORDERS_STATE.search.value) {
        return new ClearFilter();
      } else {
        return new RetainFilter();
      }
    })
  );

  /**
   * Change the page to page 1 if a new search, a new filter or a new page limit is set
   */
  @Effect()
  searchFilterUpdate$ = merge(
    this.actions$.pipe(ofType(OrdersActionTypes.SUBMIT_SEARCH)),
    this.actions$.pipe(ofType(OrdersActionTypes.SUBMIT_FILTER)),
    this.actions$.pipe(ofType(OrdersActionTypes.CHANGE_PAGE_LIMIT)),
    this.actions$.pipe(ofType(OrdersActionTypes.CHANGE_SORT))
  ).pipe(
    map((action: any) => {
      return new ChangePage(INITIAL_ORDERS_STATE.pagination.current);
    })
  );

  constructor(private actions$: Actions, private ordersService: OrdersService) {}
}
