import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IOrderLine, IListingSearchState } from '@app/shared/shared.interfaces';

/**
 * Selects the Orders state from the root state object
 */
export const getOrdersState = createFeatureSelector<IListingSearchState<IOrderLine>>('orders');

/**
 * Retrieves the search
 */
export const getSearch = createSelector(
  getOrdersState,
  ordersState => ordersState.search
);

/**
 * Retrieves the filter
 */
export const getFilter = createSelector(
  getOrdersState,
  ordersState => ordersState.filter
);

/**
 * Retrieves the result
 */
export const getResult = createSelector(
  getOrdersState,
  ordersState => ordersState.items
);

/**
 * Retrieve the loading state of the page
 */
export const getLoading = createSelector(
  getOrdersState,
  ordersState => ordersState.loading
);

/**
 * Retrieve the error state of the page
 */
export const getError = createSelector(
  getOrdersState,
  ordersState => ordersState.error
);

/**
 * Retrieve the number of items that should show per page
 */
export const getLimit = createSelector(
  getOrdersState,
  ordersState => ordersState.pagination.limit
);

/**
 * Retrieve the number of items that should show per page
 */
export const getPage = createSelector(
  getOrdersState,
  ordersState => ordersState.pagination.current
);

/**
 * Retrieve the number of items that should show per page
 */
export const getTotalPages = createSelector(
  getOrdersState,
  ordersState => ordersState.pagination.total
);

/**
 * Retrieve the sorting of items
 */
export const getSort = createSelector(
  getOrdersState,
  ordersState => ordersState.sort
);

export const getSelectedIds = createSelector(
  getOrdersState,
  ordersState => ordersState.selectedIds
);

export const getSelectedOrderLines = createSelector(
  getSelectedIds,
  getResult,
  (ids, orderLines) => {
    if (!orderLines) {
      return [];
    }

    return orderLines.filter(orderLine => ids.includes(`${orderLine.salesOrderId}_${orderLine.lineItem}`));
  }
);

export const getHasNcnrOrderLines = createSelector(
  getOrdersState,
  orderLines => {
    return orderLines.items && orderLines.items.some(item => item.ncnrStatus === true);
  }
);
