import { HttpErrorResponse } from '@angular/common/http';
import { IOrderLineQueryRequest, IOrderLineQueryResponse } from '@app/core/orders/orders.interfaces';
import { IFilter, ISearchCriteria, ISortCriteron } from '@app/shared/shared.interfaces';
import { Action } from '@ngrx/store';

export enum OrdersActionTypes {
  SUBMIT_SEARCH = 'SUBMIT_SEARCH',
  SET_FILTER = 'SET_FILTER',
  SUBMIT_FILTER = 'SUBMIT_FILTERS',
  QUERY = 'QUERY',
  QUERY_COMPLETE = 'QUERY_COMPLETE',
  QUERY_ERROR = 'QUERY_ERROR',
  CHANGE_PAGE_LIMIT = 'CHANGE_PAGE_LIMIT',
  CHANGE_PAGE = 'CHANGE_PAGE',
  CHANGE_SORT = 'CHANGE_SORT',
  SELECT_ORDER_LINES = 'SELECT_ORDER_LINES',
  CLEAR_FILTER = 'CLEAR_FILTER',
  RETAIN_FILTER = 'RETAIN_FILTER',
  EMAIL_REPRESENTATIVE = 'EMAIL_REPRESENTATIVE',
  PUSH_PULL_ORDER = 'PUSH_PULL_ORDER',
  REQUEST_RETURN_ORDER = 'REQUEST_RETURN_ORDER',
  SUBMIT_REQUEST_RETURN_ORDER = 'SUBMIT_REQUEST_RETURN_ORDER',
}

/**
 * Submit the Search facets for the Query
 */
export class SubmitSearch implements Action {
  readonly type = OrdersActionTypes.SUBMIT_SEARCH;
  constructor(public payload: ISearchCriteria) {}
}

/**
 * Submit the filters facets for the Query
 */
export class SetFilter implements Action {
  readonly type = OrdersActionTypes.SET_FILTER;
  constructor(public payload: IFilter, public label: string = '', public track: boolean = true) {}
}

/**
 * Submit the filters facets for the Query
 */
export class SubmitFilter implements Action {
  readonly type = OrdersActionTypes.SUBMIT_FILTER;
  constructor(public payload: IFilter) {}
}

/**
 * Updates the Query with the provided facets
 */
export class Query implements Action {
  readonly type = OrdersActionTypes.QUERY;
  constructor(public payload: IOrderLineQueryRequest) {}
}

/**
 * Results of the Query
 */
export class QueryComplete implements Action {
  readonly type = OrdersActionTypes.QUERY_COMPLETE;
  constructor(public payload: IOrderLineQueryResponse) {}
}

/**
 * Thrown if the Query received an Error
 */
export class QueryError implements Action {
  readonly type = OrdersActionTypes.QUERY_ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

/**
 * Changes the limit on each page
 */
export class ChangePageLimit implements Action {
  readonly type = OrdersActionTypes.CHANGE_PAGE_LIMIT;
  constructor(public payload: number) {}
}

/**
 * Change the page number
 */
export class ChangePage implements Action {
  readonly type = OrdersActionTypes.CHANGE_PAGE;
  constructor(public payload: number) {}
}

/**
 * Changes the limit on each page
 */
export class ChangeSort implements Action {
  readonly type = OrdersActionTypes.CHANGE_SORT;
  constructor(public payload: Array<ISortCriteron>) {}
}

/**
 * Adds an OrderLine to the selection
 */
export class ToggleOrderLines implements Action {
  readonly type = OrdersActionTypes.SELECT_ORDER_LINES;
  constructor(public payload: Array<string>, public select: boolean) {}
}

export class ClearFilter implements Action {
  readonly type = OrdersActionTypes.CLEAR_FILTER;
  constructor() {}
}

export class RetainFilter implements Action {
  readonly type = OrdersActionTypes.RETAIN_FILTER;
  constructor() {}
}
export class EmailRepresentative implements Action {
  readonly type = OrdersActionTypes.EMAIL_REPRESENTATIVE;
  constructor(public POnumbers: Array<string>) {}
}
export class PushPullOrder implements Action {
  readonly type = OrdersActionTypes.PUSH_PULL_ORDER;
  constructor(public ProductNames: Array<string>) {}
}

export class RequestReturnOrder implements Action {
  readonly type = OrdersActionTypes.REQUEST_RETURN_ORDER;
  constructor() {}
}

export class SubmitRequestReturnOrder implements Action {
  readonly type = OrdersActionTypes.SUBMIT_REQUEST_RETURN_ORDER;
  constructor(public payload: string) {}
}

export type OrdersActions =
  | SubmitSearch
  | SetFilter
  | SubmitFilter
  | Query
  | QueryComplete
  | QueryError
  | ChangePageLimit
  | ChangePage
  | ChangeSort
  | ToggleOrderLines
  | ClearFilter
  | RetainFilter
  | EmailRepresentative
  | PushPullOrder
  | RequestReturnOrder
  | SubmitRequestReturnOrder;
