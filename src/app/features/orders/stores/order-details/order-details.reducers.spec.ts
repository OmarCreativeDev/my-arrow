import { IOrderState, IOrder, IOrderLine, IHttpException } from '@app/shared/shared.interfaces';
import { INITIAL_ORDER_DETAILS_STATE, orderDetailsReducers } from '@app/features/orders/stores/order-details/order-details.reducers';
import {
  Query,
  ToggleOrderLines,
  QueryError,
  QueryComplete,
  ConfirmAddToCart,
  ConfirmReset,
} from '@app/features/orders/stores/order-details/order-details.actions';
import { HttpErrorResponse } from '@angular/common/http';
import { OrderServiceHttpExceptionStatusEnum, IViewOrderRequest } from '@app/core/orders/orders.interfaces';

describe('Order Details Reducers', () => {
  let initialState: IOrderState;

  beforeEach(() => {
    initialState = {
      loading: true,
      value: {
        details: undefined,
        orderLines: [],
      },
      error: undefined,
      selectedIds: [],
    };
  });

  it(`SELECT_ORDER_LINES should add the provided orderLines to the selection`, () => {
    const salesOrderId = 'xxx';
    const lineItem = '1.2.2';
    const orderLineId = `${salesOrderId}_${lineItem}`;
    const orderLines = [orderLineId];
    const expectedIds = [`${salesOrderId}_${lineItem}`];
    const action = new ToggleOrderLines(orderLines, true);
    const result = orderDetailsReducers(initialState, action);
    expect(result.selectedIds).toEqual(expectedIds);
  });

  it(`SELECT_ORDER_LINES should remove the provided orderLines from the selection`, () => {
    const salesOrderId = 'xxx';
    const lineItem = '1.2.2';
    const orderLineId = `${salesOrderId}_${lineItem}`;
    const orderLines = [orderLineId];
    initialState.selectedIds = [orderLineId];
    const action = new ToggleOrderLines(orderLines, false);
    const result = orderDetailsReducers(initialState, action);
    expect(result.selectedIds).toEqual([]);
  });

  it(`SELECT_ORDER_LINES should only return unique values in the selectedIds array`, () => {
    const salesOrderId = 'xxx';
    const lineItem = '1.2.2';
    const orderLineId = `${salesOrderId}_${lineItem}`;
    const orderLines = [orderLineId];
    initialState.selectedIds = [orderLineId];
    const action = new ToggleOrderLines(orderLines, true);
    const result = orderDetailsReducers(initialState, action);
    expect(result.selectedIds.length).toEqual(orderLines.length);
    expect(result.selectedIds).toEqual(orderLines);
  });

  it(`QUERY should trigger the request`, () => {
    const params: IViewOrderRequest = {
      orderId: '123',
      salesHeaderId: 1234,
      billToSiteUseId: 1234,
    };
    const action = new Query(params);
    const result = orderDetailsReducers(initialState, action);

    expect(result.selectedIds).toEqual([]);
    expect(result.loading).toEqual(true);
    expect(result.error).toEqual(undefined);
  });

  it(`QUERY_COMPLETE should set the value of the state,
   change loading to false and not have an error property`, () => {
    const response: IOrder = {
      details: {
        id: 1123,
        salesOrderId: 123123,
        purchaseOrderNumber: 'foo',
        billToID: 123,
        billToAddress: {
          addressLine1: 'asd',
          addressLine2: 'asd',
          city: 'asd',
          region: 'adasd',
          zipCode: 'asd',
          country: 'asd',
        },
        shipToID: 123,
        shipToAddress: {
          addressLine1: 'asd',
          addressLine2: 'ads',
          city: 'asd',
          region: 'asd',
          zipCode: 'asd',
          country: '0asd',
        },
        shipMethod: 'asere',
        buyerName: 'ascda',
        carrierAccount: 'adasd',
        currencyCode: 'BTC',
      },
      orderLines: [{ purchaseOrderNumber: 'dummy-po-number' } as IOrderLine],
    };
    const action = new QueryComplete(response);
    const result = orderDetailsReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.value).toEqual(response);
  });

  it(`QUERY_ERROR orderLines should be undefined and error property should have an error property`, () => {
    const expectedError = {
      error: {
        error: 'foo',
        status: OrderServiceHttpExceptionStatusEnum.BAD_REQUEST,
      } as IHttpException,
    } as HttpErrorResponse;
    const action = new QueryError(expectedError);
    const result = orderDetailsReducers(initialState, action);
    expect(result.error).toEqual(expectedError);
    expect(result.value).toEqual(undefined);
  });

  it(`by default should return the state without transforming it`, () => {
    const result = orderDetailsReducers(undefined, {} as any);
    expect(result).toEqual(INITIAL_ORDER_DETAILS_STATE);
  });

  it(`CONFIRM_ADD_TO_CART should set the value of confirmed to true`, () => {
    const action = new ConfirmAddToCart();
    const result = orderDetailsReducers(initialState, action);

    expect(result.confirmed).toEqual(true);
  });

  it(`CONFIRM_RESET should set the value of confirmed to false`, () => {
    const action = new ConfirmReset();
    const result = orderDetailsReducers(initialState, action);

    expect(result.confirmed).toEqual(false);
  });
});
