import { IOrderState } from '@app/shared/shared.interfaces';
import { createFeatureSelector, createSelector } from '@ngrx/store';

/**
 * Selects the Orders state from the root state object
 */
export const getOrderDetailsState = createFeatureSelector<IOrderState>('orderDetails');

/**
 * Retrieve the loading state of the page
 */
export const getLoading = createSelector(
  getOrderDetailsState,
  orderDetailsState => orderDetailsState.loading
);

/**
 * Retrieve the error state of the page
 */
export const getError = createSelector(
  getOrderDetailsState,
  orderDetailsState => orderDetailsState.error
);

export const getSelectedIds = createSelector(
  getOrderDetailsState,
  orderDetailsState => orderDetailsState.selectedIds
);

export const getOrderLines = createSelector(
  getOrderDetailsState,
  orderDetailsState => (orderDetailsState.value ? orderDetailsState.value.orderLines : undefined)
);

export const getDetails = createSelector(
  getOrderDetailsState,
  orderDetailsState => (orderDetailsState.value ? orderDetailsState.value.details : undefined)
);

export const getOrder = createSelector(
  getOrderDetailsState,
  orderDetailsState => orderDetailsState.value
);

export const getSelectedOrderLines = createSelector(
  getSelectedIds,
  getOrderLines,
  (ids, orderLines) => {
    if (!orderLines) {
      return [];
    }

    return orderLines.filter(orderLine => ids.includes(`${orderLine.salesOrderId}_${orderLine.lineItem}`));
  }
);

export const getConfirmed = createSelector(
  getOrderDetailsState,
  orderDetailsState => orderDetailsState.confirmed
);
