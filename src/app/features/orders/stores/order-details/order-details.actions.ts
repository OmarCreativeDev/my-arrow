import { Action } from '@ngrx/store';
import { IOrder } from '@app/shared/shared.interfaces';
import { HttpErrorResponse } from '@angular/common/http';
import { IViewOrderRequest } from '@app/core/orders/orders.interfaces';

export enum OrderDetailsActionTypes {
  SELECT_ORDER_LINES = '[Order Details] Select Order Lines',
  QUERY = '[Order Details] Query',
  QUERY_COMPLETE = '[Order Details] Query Complete',
  QUERY_ORDERLINES_COMPLETE = '[Order Details] Query OrderLines Complete',
  QUERY_DETAILS_COMPLETE = '[Order Details] Query Details Complete',
  QUERY_ERROR = '[Order Details] Query Error',
  CONFIRM_ADD_TO_CART = '[Order Details] Confirm add to cart',
  CONFIRM_RESET = '[Order Details] Confirm reset',
}

/**
 * Adds an OrderLine to the selection
 */
export class ToggleOrderLines implements Action {
  readonly type = OrderDetailsActionTypes.SELECT_ORDER_LINES;

  constructor(public payload: Array<string>, public select: boolean) {}
}

/**
 * Updates the Query with the provided facets
 */
export class Query implements Action {
  readonly type = OrderDetailsActionTypes.QUERY;

  constructor(public payload: IViewOrderRequest) {}
}

/**
 * Results of the Query
 */
export class QueryComplete implements Action {
  readonly type = OrderDetailsActionTypes.QUERY_COMPLETE;
  constructor(public payload: IOrder) {}
}

/**
 * Thrown if the Query received an Error
 */
export class QueryError implements Action {
  readonly type = OrderDetailsActionTypes.QUERY_ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

/**
 * Confirm add to cart
 */
export class ConfirmAddToCart implements Action {
  readonly type = OrderDetailsActionTypes.CONFIRM_ADD_TO_CART;

  constructor() {}
}

/**
 * Confirm reset
 */
export class ConfirmReset implements Action {
  readonly type = OrderDetailsActionTypes.CONFIRM_RESET;

  constructor() {}
}

export type OrderDetailsActions = ToggleOrderLines | Query | QueryComplete | QueryError | ConfirmAddToCart | ConfirmReset;
