import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { OrdersService } from '@app/core/orders/orders.service';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { OrderDetailsActionTypes, Query, QueryComplete, QueryError } from '@app/features/orders/stores/order-details/order-details.actions';
import { IOrder } from '@app/shared/shared.interfaces';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class OrderDetailsEffects {
  /**
   * Side-effect for the Query being changed directly.
   */
  @Effect()
  queryOrderLines$: Observable<any> = this.actions$.pipe(
    ofType(OrderDetailsActionTypes.QUERY),
    switchMap((action: Query) => {
      return this.orderService.getOrderForId(action.payload).pipe(
        map((order: IOrder) => new QueryComplete(order)),
        catchError((errorResponse: HttpErrorResponse) => of(new QueryError(errorResponse)))
      );
    })
  );

  constructor(private actions$: Actions, private orderService: OrdersService) {}
}
