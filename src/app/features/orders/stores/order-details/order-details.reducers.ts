import { IOrderState } from '@app/shared/shared.interfaces';
import { OrderDetailsActions, OrderDetailsActionTypes } from './order-details.actions';
import without from 'lodash-es/without';
import uniq from 'lodash-es/uniq';

export const INITIAL_ORDER_DETAILS_STATE: IOrderState = {
  loading: true,
  error: undefined,
  value: undefined,
  selectedIds: [],
  confirmed: false,
};

/**
 * Mutates the state for given set of OrderDetailsActions
 * @param state
 * @param action
 */
export function orderDetailsReducers(state: IOrderState = INITIAL_ORDER_DETAILS_STATE, action: OrderDetailsActions): IOrderState {
  switch (action.type) {
    case OrderDetailsActionTypes.SELECT_ORDER_LINES: {
      let selectedIds = [...state.selectedIds];
      if (action.select) {
        selectedIds.push(...action.payload);
      } else {
        selectedIds = without(selectedIds, ...action.payload);
      }
      return {
        ...state,
        selectedIds: uniq(selectedIds),
      };
    }
    case OrderDetailsActionTypes.QUERY: {
      return {
        ...state,
        error: undefined,
        loading: true,
        value: undefined,
        selectedIds: [],
      };
    }
    case OrderDetailsActionTypes.QUERY_COMPLETE: {
      return {
        ...state,
        value: action.payload,
        loading: false,
      };
    }
    case OrderDetailsActionTypes.QUERY_ERROR: {
      return {
        ...state,
        error: action.payload,
        selectedIds: undefined,
        value: undefined,
        loading: false,
      };
    }
    case OrderDetailsActionTypes.CONFIRM_ADD_TO_CART: {
      return {
        ...state,
        confirmed: true,
      };
    }
    case OrderDetailsActionTypes.CONFIRM_RESET: {
      return {
        ...state,
        confirmed: false,
      };
    }
    default: {
      return state;
    }
  }
}
