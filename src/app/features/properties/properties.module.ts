import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { SharedModule } from '@app/shared/shared.module';
import { PropertiesEffects } from '@app/features/properties/store/properties.effects';
import { PropertiesService } from '@app/core/properties/properties.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    StoreDevtoolsModule.instrument({
      maxAge: 25,
    }),
    EffectsModule.forFeature([PropertiesEffects]),
  ],
  providers: [PropertiesService],
})
export class PropertiesModule {}
