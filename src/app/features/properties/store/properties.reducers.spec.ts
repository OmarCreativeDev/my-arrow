import { IPropertiesState } from '@app/core/properties/properties.interface';
import {
  GetCountryList,
  GetCountryListSuccess,
  GetCountryListFailed,
  GetPublicProperties,
  GetPublicPropertiesSuccess,
  GetPublicPropertiesFailed,
  GetPrivateProperties,
  GetPrivatePropertiesSuccess,
  GetPrivatePropertiesFailed,
} from '@app/features/properties/store/properties.actions';
import { propertiesReducers, INITIAL_RESULTS_STATE } from '@app/features/properties/store/properties.reducers';

describe('Trade Compliance Generate CountryList Reducers', () => {
  let initialState: IPropertiesState;
  let countryState: IPropertiesState;
  let publicState: IPropertiesState;
  let privateState: IPropertiesState;
  const domainProps: any = {
    key: 'value',
  };

  beforeEach(() => {
    initialState = {
      countryList: [],
      public: undefined,
      private: undefined,
      error: undefined,
      loading: false,
    };

    countryState = {
      countryList: [
        {
          code: 'US',
          name: 'United States',
        },
        {
          code: 'CR',
          name: 'Costa Rica',
        },
      ],
      public: undefined,
      private: undefined,
      error: undefined,
      loading: false,
    };

    publicState = {
      countryList: [],
      public: domainProps,
      private: undefined,
      error: undefined,
      loading: false,
    };

    privateState = {
      countryList: [],
      public: undefined,
      private: domainProps,
      error: undefined,
      loading: false,
    };
  });

  it(`GetCountryList should put loading to true and not have an error property`, () => {
    const initialPayload = { countryList: [], public: undefined, private: undefined, loading: true, error: undefined };
    const action = new GetCountryList();
    const result = propertiesReducers(initialState, action);
    expect(result).toEqual(initialPayload);
  });

  it(`GetCountryListSuccess should put loading to false, get the information and have an error property in false`, () => {
    const payload = {
      countryList: [
        {
          code: 'US',
          name: 'United States',
        },
        {
          code: 'CR',
          name: 'Costa Rica',
        },
      ],
    };
    const action = new GetCountryListSuccess(payload);
    const result = propertiesReducers(countryState, action);
    expect(result).toEqual({
      countryList: payload,
      public: undefined,
      private: undefined,
      loading: false,
      error: undefined,
    });
  });

  it(`GetCountryListFailed should put loading to false, get an error and have an error property in true`, () => {
    const payload = new Error('There was an error fetching the country list');
    const action = new GetCountryListFailed(payload);
    const result = propertiesReducers(initialState, action);
    expect(result).toEqual({
      countryList: [],
      public: undefined,
      private: undefined,
      loading: false,
      error: payload,
    });
  });

  it(`GetPublicProperties should put loading to true and not have an error property`, () => {
    const initialPayload = { countryList: [], public: undefined, private: undefined, loading: true, error: undefined };
    const action = new GetPublicProperties();
    const result = propertiesReducers(initialState, action);
    expect(result).toEqual(initialPayload);
  });

  it(`GetPublicPropertiesSuccess should put loading to false, get the information and have an error property in false`, () => {
    const payload = {
      key: 'value',
    };
    const action = new GetPublicPropertiesSuccess(payload);
    const result = propertiesReducers(publicState, action);
    expect(result).toEqual({
      countryList: [],
      public: payload,
      private: undefined,
      loading: false,
      error: undefined,
    });
  });

  it(`GetPublicPropertiesFailed should put loading to false, get an error and have an error property in true`, () => {
    const payload = new Error('There was an error fetching the country list');
    const action = new GetPublicPropertiesFailed(payload);
    const result = propertiesReducers(initialState, action);
    expect(result).toEqual({
      countryList: [],
      public: undefined,
      private: undefined,
      loading: false,
      error: payload,
    });
  });

  it(`GetPrivateProperties should put loading to true and not have an error property`, () => {
    const initialPayload = { countryList: [], public: undefined, private: undefined, loading: true, error: undefined };
    const action = new GetPrivateProperties('private/na');
    const result = propertiesReducers(initialState, action);
    expect(result).toEqual(initialPayload);
  });

  it(`GetPrivatePropertiesSuccess should put loading to false, get the information and have an error property in false`, () => {
    const payload = {
      key: 'value',
    };
    const action = new GetPrivatePropertiesSuccess(payload);
    const result = propertiesReducers(privateState, action);
    expect(result).toEqual({
      countryList: [],
      public: undefined,
      private: payload,
      loading: false,
      error: undefined,
    });
  });

  it(`GetPrivatePropertiesFailed should put loading to false, get an error and have an error property in true`, () => {
    const payload = new Error('There was an error fetching the country list');
    const action = new GetPrivatePropertiesFailed(payload);
    const result = propertiesReducers(initialState, action);
    expect(result).toEqual({
      countryList: [],
      public: undefined,
      private: undefined,
      loading: false,
      error: payload,
    });
  });

  it(`by default should return the state without transforming it`, () => {
    const result = propertiesReducers(undefined, {} as any);
    expect(result).toEqual(INITIAL_RESULTS_STATE);
  });
});
