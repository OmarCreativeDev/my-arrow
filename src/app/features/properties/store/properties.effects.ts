import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, catchError, switchMap } from 'rxjs/operators';
import { PropertiesService } from '@app/core/properties/properties.service';
import { ICountryList } from '@app/core/properties/properties.interface';
import {
  PropertiesActionTypes,
  GetCountryList,
  GetCountryListSuccess,
  GetCountryListFailed,
  GetPublicProperties,
  GetPublicPropertiesFailed,
  GetPublicPropertiesSuccess,
  GetPrivateProperties,
  GetPrivatePropertiesFailed,
  GetPrivatePropertiesSuccess,
} from '@app/features/properties/store/properties.actions';

@Injectable()
export class PropertiesEffects {
  constructor(private actions$: Actions, private propertiesService: PropertiesService) {}

  @Effect()
  GetCountryList$ = this.actions$.pipe(
    ofType(PropertiesActionTypes.GET_COUNTRY_LIST),
    switchMap((action: GetCountryList) => {
      return this.propertiesService.getCountryList().pipe(
        map((data: ICountryList) => new GetCountryListSuccess(data)),
        catchError(err => of(new GetCountryListFailed(err)))
      );
    })
  );

  @Effect()
  GetPublicProperties$ = this.actions$.pipe(
    ofType(PropertiesActionTypes.GET_PUBLIC_PROPERTIES),
    switchMap((action: GetPublicProperties) => {
      return this.propertiesService.getProperties('public').pipe(
        map(data => new GetPublicPropertiesSuccess(data)),
        catchError(err => of(new GetPublicPropertiesFailed(err)))
      );
    })
  );

  @Effect()
  GetPrivateProperties$ = this.actions$.pipe(
    ofType(PropertiesActionTypes.GET_PRIVATE_PROPERTIES),
    switchMap((action: GetPrivateProperties) => {
      return this.propertiesService.getProperties(action.payload).pipe(
        map(data => new GetPrivatePropertiesSuccess(data)),
        catchError(err => of(new GetPrivatePropertiesFailed(err)))
      );
    })
  );
}
