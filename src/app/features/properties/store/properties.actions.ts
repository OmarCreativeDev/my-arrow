import { Action } from '@ngrx/store';
import { ICountryList } from '@app/core/properties/properties.interface';

export enum PropertiesActionTypes {
  GET_COUNTRY_LIST = 'GET_COUNTRY_LIST',
  GET_COUNTRY_LIST_SUCCESS = 'GET_COUNTRY_LIST_SUCCESS',
  GET_COUNTRY_LIST_FAILED = 'GET_COUNTRY_LIST_FAILED',
  GET_PUBLIC_PROPERTIES = 'GET_PUBLIC_PROPERTIES',
  GET_PUBLIC_PROPERTIES_SUCCESS = 'GET_PUBLIC_PROPERTIES_SUCCESS',
  GET_PUBLIC_PROPERTIES_FAILED = 'GET_PUBLIC_PROPERTIES_FAILED',
  GET_PRIVATE_PROPERTIES = 'GET_PRIVATE_PROPERTIES',
  GET_PRIVATE_PROPERTIES_SUCCESS = 'GET_PRIVATE_PROPERTIES_SUCCESS',
  GET_PRIVATE_PROPERTIES_FAILED = 'GET_PRIVATE_PROPERTIES_FAILED',
}

/**
 * Get country list
 */
export class GetCountryList implements Action {
  readonly type = PropertiesActionTypes.GET_COUNTRY_LIST;
  constructor() {}
}

/**
 * Get cart items count sucess
 */
export class GetCountryListSuccess implements Action {
  readonly type = PropertiesActionTypes.GET_COUNTRY_LIST_SUCCESS;
  constructor(public payload: ICountryList) {}
}

/**
 * Get cart items count failed
 */
export class GetCountryListFailed implements Action {
  readonly type = PropertiesActionTypes.GET_COUNTRY_LIST_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Get public properties
 */
export class GetPublicProperties implements Action {
  readonly type = PropertiesActionTypes.GET_PUBLIC_PROPERTIES;
}

/**
 * Get public properties success
 */
export class GetPublicPropertiesSuccess implements Action {
  readonly type = PropertiesActionTypes.GET_PUBLIC_PROPERTIES_SUCCESS;
  constructor(public payload: any) {}
}

/**
 * Get public properties failed
 */
export class GetPublicPropertiesFailed implements Action {
  readonly type = PropertiesActionTypes.GET_PUBLIC_PROPERTIES_FAILED;
  constructor(public payload: Error) {}
}

/**
 * Get private properties
 */
export class GetPrivateProperties implements Action {
  readonly type = PropertiesActionTypes.GET_PRIVATE_PROPERTIES;
  constructor(public payload: string) {}
}

/**
 * Get private properties success
 */
export class GetPrivatePropertiesSuccess implements Action {
  readonly type = PropertiesActionTypes.GET_PRIVATE_PROPERTIES_SUCCESS;
  constructor(public payload: any) {}
}

/**
 * Get private properties failed
 */
export class GetPrivatePropertiesFailed implements Action {
  readonly type = PropertiesActionTypes.GET_PRIVATE_PROPERTIES_FAILED;
  constructor(public payload: Error) {}
}

export type PropertiesActions =
  | GetCountryList
  | GetCountryListSuccess
  | GetCountryListFailed
  | GetPublicProperties
  | GetPublicPropertiesSuccess
  | GetPublicPropertiesFailed
  | GetPrivateProperties
  | GetPrivatePropertiesSuccess
  | GetPrivatePropertiesFailed;
