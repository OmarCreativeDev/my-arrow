import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IPropertiesState } from '@app/core/properties/properties.interface';
import { get } from 'lodash-es';

export const getPropertiesState = createFeatureSelector<IPropertiesState>('properties');

export const getCountryListSelector = createSelector(
  getPropertiesState,
  propertiesState => propertiesState.countryList
);

export const getPublicPropertiesSelector = createSelector(
  getPropertiesState,
  propertiesState => propertiesState.public
);

export const getPrivatePropertiesSelector = createSelector(
  getPropertiesState,
  propertiesState => propertiesState.private
);

export const getPublicFeatureFlagsSelector = createSelector(
  getPropertiesState,
  propertiesState => get(propertiesState, 'public.flags', {})
);

export const getPrivateFeatureFlagsSelector = createSelector(
  getPropertiesState,
  propertiesState => get(propertiesState, 'private.featureFlags', {})
);

export const getPublicDomainsSelector = createSelector(
  getPropertiesState,
  propertiesState => get(propertiesState, 'public.domains', {})
);

export const getErrorSelector = createSelector(
  getPropertiesState,
  propertiesState => propertiesState.error
);
