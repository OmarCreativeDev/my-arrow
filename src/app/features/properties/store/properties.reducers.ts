import { IPropertiesState } from '@app/core/properties/properties.interface';
import { PropertiesActions, PropertiesActionTypes } from '@app/features/properties/store/properties.actions';

export const INITIAL_RESULTS_STATE: IPropertiesState = {
  countryList: [],
  public: undefined,
  private: undefined,
  error: undefined,
  loading: false,
};

/**
 * Mutates the state for given set of PropertiesActions
 * @param state
 * @param action
 */
export function propertiesReducers(state: IPropertiesState = INITIAL_RESULTS_STATE, action: PropertiesActions) {
  switch (action.type) {
    case PropertiesActionTypes.GET_COUNTRY_LIST: {
      return {
        ...state,
        loading: true,
        error: undefined,
        countryList: [],
      };
    }
    case PropertiesActionTypes.GET_COUNTRY_LIST_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: undefined,
        countryList: action.payload,
      };
    }
    case PropertiesActionTypes.GET_COUNTRY_LIST_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload,
        countryList: [],
      };
    }
    case PropertiesActionTypes.GET_PUBLIC_PROPERTIES: {
      return {
        ...state,
        loading: true,
        error: undefined,
        public: undefined,
      };
    }
    case PropertiesActionTypes.GET_PUBLIC_PROPERTIES_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: undefined,
        public: action.payload,
      };
    }
    case PropertiesActionTypes.GET_PUBLIC_PROPERTIES_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload,
        public: undefined,
      };
    }
    case PropertiesActionTypes.GET_PRIVATE_PROPERTIES: {
      return {
        ...state,
        loading: true,
        error: undefined,
        private: undefined,
      };
    }
    case PropertiesActionTypes.GET_PRIVATE_PROPERTIES_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: undefined,
        private: action.payload,
      };
    }
    case PropertiesActionTypes.GET_PRIVATE_PROPERTIES_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload,
        private: undefined,
      };
    }
    default: {
      return state;
    }
  }
}
