import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { TradeComplianceSearchComponent } from './pages/trade-compliance-search/trade-compliance-search.component';
import { TradeComplianceSearchResultsComponent } from './pages/trade-compliance-search-results/trade-compliance-search-results.component';
import { TradeComplianceRoutingModule } from './trade-compliance-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { TradeComplianceService } from '@app/core/trade-compliance/trade-compliance.service';
import { GenerateCertificateService } from '@app/core/generate-certificate/generate-certificate.service';
import { TradeComplianceSearchRouteGuard } from './pages/trade-compliance-search-results/trade-compliance-search-results.component.guard';
import { SearchResultsComponent } from './components/search-results/search-results.component';
import { GenerateCertificateModalMessageComponent } from './components/generate-certificate/generate-certificate-modal-message.component';
import { SearchResultsEffects } from './components/search-results/store/search-results.effects';
import { GenerateCertificateEffects } from './components/generate-certificate/store/generate-certificate.effects';

@NgModule({
  imports: [
    CommonModule,
    TradeComplianceRoutingModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    StoreDevtoolsModule.instrument({
      maxAge: 25,
    }),
    EffectsModule.forFeature([SearchResultsEffects, GenerateCertificateEffects]),
  ],
  declarations: [
    TradeComplianceSearchComponent,
    TradeComplianceSearchResultsComponent,
    SearchFormComponent,
    SearchResultsComponent,
    GenerateCertificateModalMessageComponent,
  ],
  providers: [TradeComplianceService, GenerateCertificateService, TradeComplianceSearchRouteGuard],
})
export class TradeComplianceModule {}
