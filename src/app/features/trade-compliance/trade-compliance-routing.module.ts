import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TradeComplianceSearchComponent } from './pages/trade-compliance-search/trade-compliance-search.component';
import { TradeComplianceSearchResultsComponent } from './pages/trade-compliance-search-results/trade-compliance-search-results.component';
import { TradeComplianceSearchRouteGuard } from './pages/trade-compliance-search-results/trade-compliance-search-results.component.guard';

const tradeComplianceRoutes: Routes = [
  { path: '', component: TradeComplianceSearchComponent, data: { title: 'Trade Compliance' } },
  {
    path: 'search',
    component: TradeComplianceSearchResultsComponent,
    canActivate: [TradeComplianceSearchRouteGuard],
    data: { title: 'Trade Compliance - Search' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(tradeComplianceRoutes)],
  exports: [RouterModule],
})
export class TradeComplianceRoutingModule {}
