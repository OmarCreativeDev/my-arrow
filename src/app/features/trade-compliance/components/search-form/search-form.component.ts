import { AfterContentInit, Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { ISelectableOption, IAppState } from '@app/shared/shared.interfaces';
import { TradeComplianceService } from '@app/core/trade-compliance/trade-compliance.service';
import {
  UpdateNaftaYear,
  UpdateSearchType,
  TradeComplianceSearch,
} from '@app/features/trade-compliance/components/search-results/store/search-results.actions';
import { CertificateType, ITradeComplianceSearchType } from '@app/core/trade-compliance/trade-compliance.interfaces';
import { trimEnd } from 'lodash-es';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss'],
})
export class SearchFormComponent implements AfterContentInit, OnInit {
  private _currentYear: number = new Date().getFullYear();
  public searchForm: FormGroup;
  public filterDropdownOptions: Array<ISelectableOption<any>> = [
    { label: 'NAFTA', value: 0 },
    { label: 'COO Information', value: 1 },
    { label: 'HTC/ECCN Information', value: 2 },
  ];

  @Input()
  set parsedValues(values: string) {
    /* istanbul ignore else */
    if (this.searchForm) {
      this.searchForm.patchValue({ partNumbers: values });
    }
  }

  constructor(private router: Router, private tradeComplianceService: TradeComplianceService, private store: Store<IAppState>) {}

  ngOnInit() {
    this.searchForm = new FormGroup({
      partNumbers: new FormControl(null, {
        validators: [Validators.required],
      }),
      searchFilter: new FormControl(null, {
        validators: [Validators.required],
      }),
      searchYear: new FormControl(null, {
        validators: [Validators.required],
      }),
    });
    this.store.dispatch(new UpdateNaftaYear(this._currentYear));
    this.store.dispatch(
      new UpdateSearchType(<ITradeComplianceSearchType>{
        label: CertificateType[0],
        value: 0,
      })
    );
  }

  ngAfterContentInit() {
    this.searchForm.setValue({
      partNumbers: '',
      searchFilter: 0, // NAFTA
      searchYear: this._currentYear,
    });
  }

  get partNumbers() {
    return this.searchForm.get('partNumbers');
  }

  getYearDropdownOptions() {
    let initialYear: number = this._currentYear - 4;
    const yearsList: Array<ISelectableOption<any>> = [];
    for (let i = 6; i > 0; i--) {
      yearsList.unshift({
        label: `${initialYear}`,
        value: initialYear,
      });
      initialYear++;
    }
    return yearsList;
  }

  changeSearchFilterValue(value) {
    this.searchForm.patchValue({ searchFilter: value });
    const searchTypeInfo = {
      label: CertificateType[value],
      value,
    };
    this.store.dispatch(new UpdateSearchType(searchTypeInfo));
  }

  changeSearchYearValue(value) {
    this.searchForm.patchValue({ searchYear: value });
    this.store.dispatch(new UpdateNaftaYear(value));
  }

  validateSearchForm() {
    const formGroup = this.searchForm;
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      control.markAsTouched({ onlySelf: true });
    });
    return formGroup.valid;
  }

  searchPartNumbers() {
    const certificateType = this.searchForm.value.searchFilter;
    const certificateTypeName = CertificateType[certificateType].toUpperCase();
    const year = this.searchForm.value.searchYear;
    const parts = this.searchForm.value.partNumbers;
    const tradeComplianceSearchInfo =
      certificateType === 0
        ? `${certificateTypeName}: ${year}: ${trimEnd(parts.trim(), ',')}`
        : `${certificateTypeName}: ${trimEnd(parts.trim(), ',')}`;
    this.tradeComplianceService.setData('year', year);
    this.tradeComplianceService.setData('parts', trimEnd(parts.trim(), ','));
    this.tradeComplianceService.setData('certificateType', certificateType);
    this.store.dispatch(new TradeComplianceSearch(tradeComplianceSearchInfo));
    this.router.navigateByUrl('/trade-compliance/search');
  }

  doSearch() {
    if (this.validateSearchForm()) {
      this.searchPartNumbers();
    }
  }
}
