import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';

import { TradeComplianceService } from '@app/core/trade-compliance/trade-compliance.service';
import { ApiService } from '@app/core/api/api.service';
import { SharedModule } from '@app/shared/shared.module';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { BackdropServiceMock } from '@app/app.component.spec';
import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { SearchFormComponent } from './search-form.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import {GenerateCertificateModalMessageComponent} from '@app/features/trade-compliance/components/generate-certificate/generate-certificate-modal-message.component';
import {GenerateCertificateService} from '@app/core/generate-certificate/generate-certificate.service';

import {Store, StoreModule} from '@ngrx/store';
import {tradeComplianceSearchReducers} from '@app/features/trade-compliance/components/search-results/store/search-results.reducers';
import {userReducers} from '@app/core/user/store/user.reducers';
import {generateCertificateReducers} from '@app/features/trade-compliance/components/generate-certificate/store/generate-certificate.reducers';
import {TradeComplianceSearchResultsComponent} from '@app/features/trade-compliance/pages/trade-compliance-search-results/trade-compliance-search-results.component';
import {SearchResultsComponent} from '@app/features/trade-compliance/components/search-results/search-results.component';
import {TradeComplianceSearchComponent} from '@app/features/trade-compliance/pages/trade-compliance-search/trade-compliance-search.component';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {TradeComplianceRoutingModule} from '@app/features/trade-compliance/trade-compliance-routing.module';
import {StoreMock} from '@app/features/products/pages/product-search/product-search.component.spec';

class MockRouter {
  navigateByUrl(url: string) {
    return url;
  }
}

describe('SearchFormComponent', () => {
  let component: SearchFormComponent;
  let fixture: ComponentFixture<SearchFormComponent>;
  let tradeComplianceService: TradeComplianceService;
  let router: Router;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [
          TradeComplianceSearchComponent,
          TradeComplianceSearchResultsComponent,
          SearchFormComponent,
          SearchResultsComponent,
          GenerateCertificateModalMessageComponent
        ],
        providers: [
          { provide: BackdropService, useClass: BackdropServiceMock },
          { provide: Router, useClass: MockRouter },
          { provide: Store, useClass: StoreMock },
          TradeComplianceService,
          GenerateCertificateService,
          ApiService,
          AuthTokenService,
        ],
        imports: [
          CommonModule,
          TradeComplianceRoutingModule,
          HttpClientModule,
          SharedModule,
          FormsModule,
          ReactiveFormsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          StoreModule.forRoot({
            user: userReducers,
            tradeCompliancePartNumbers: tradeComplianceSearchReducers,
            tradeComplianceCertificate: generateCertificateReducers
          }),
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFormComponent);
    router = TestBed.get(Router);
    component = fixture.componentInstance;
    fixture.detectChanges();
    tradeComplianceService = TestBed.get(TradeComplianceService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create service', () => {
    expect(tradeComplianceService).toBeTruthy();
  });

  it('partNumbers field validity', () => {
    let errors = {};
    const partNumbers = component.searchForm.controls['partNumbers'];
    expect(partNumbers.valid).toBeFalsy();

    errors = partNumbers.errors || {};
    expect(errors['required']).toBeTruthy();

    partNumbers.setValue('bav99');
    errors = partNumbers.errors || {};
    expect(errors['required']).toBeFalsy();

    partNumbers.setValue('');
    errors = partNumbers.errors || {};
    expect(errors['required']).toBeTruthy();
  });

  it('hide year selector when is not NAFTA', () => {
    const searchFilter = component.searchForm.controls['searchFilter'];

    expect(
      fixture.nativeElement.querySelector('[data-automation-ref="year"]') ===
        null
    ).toBeFalsy();

    searchFilter.setValue(1); // Set search filter to COO
    fixture.detectChanges();

    expect(
      fixture.nativeElement.querySelector('[data-automation-ref="year"]') ===
        null
    ).toBeTruthy();
  });

  it('should show errors when searching without input', () => {
    expect(
      fixture.nativeElement.querySelector(
        '[data-automation-ref="part-numbers-error"]'
      ) === null
    ).toBeTruthy();
    component.doSearch();
    fixture.detectChanges();
    expect(
      fixture.nativeElement.querySelector(
        '[data-automation-ref="part-numbers-error"]'
      ) === null
    ).toBeFalsy();
  });

  it('should call `validateSearchForm` when submiting form', () => {
    spyOn(component, 'validateSearchForm');
    component.doSearch();
    expect(component.validateSearchForm).toHaveBeenCalled();
  });

  it('should not call `searchPartNumbers` when submiting empty form', () => {
    spyOn(component, 'searchPartNumbers');
    component.doSearch();
    expect(component.searchPartNumbers).toHaveBeenCalledTimes(0);
  });

  it('should call `searchPartNumbers` when submiting valid form', () => {
    spyOn(component, 'searchPartNumbers');
    component.searchForm.controls['partNumbers'].setValue('ABC-DEFG');
    component.doSearch();
    expect(component.searchPartNumbers).toHaveBeenCalledTimes(1);
  });

  it('should populate partNumbers when passing parsedValues', () => {
    const partNumbers = '008,020,MK-009';
    expect(component.searchForm.controls['partNumbers'].value).toBe('');
    component.parsedValues = partNumbers;
    expect(component.searchForm.controls['partNumbers'].value).toBe(
      partNumbers
    );
  });

  it('should update `searchFilter` and `searchYear` through change handlers', () => {
    const searchFilter = 0;
    const currentYear = new Date().getFullYear();
    const searchYear = currentYear - 1;
    expect(component.searchForm.controls['searchFilter'].value).toBe(0);
    expect(component.searchForm.controls['searchYear'].value).toBe(currentYear);
    component.changeSearchFilterValue(searchFilter);
    component.changeSearchYearValue(searchYear);
    expect(component.searchForm.controls['searchFilter'].value).toBe(
      searchFilter
    );
    expect(component.searchForm.controls['searchYear'].value).toBe(searchYear);
  });

  it('should set values in the service before going to search', () => {
    const partNumbers = '008,020,MK-009';
    const searchYear = '2018';
    const searchFilter = 0;
    expect(tradeComplianceService.certificateType).toBeUndefined();
    expect(tradeComplianceService.parts).toBeUndefined();
    expect(tradeComplianceService.year).toBeUndefined();
    component.searchForm.patchValue({
      searchFilter,
      searchYear,
      partNumbers
    });
    component.doSearch();
    expect(tradeComplianceService.certificateType).toBe(searchFilter);
    expect(tradeComplianceService.parts).toBe(partNumbers);
    expect(tradeComplianceService.year).toBe(searchYear);
  });

  it('should redirect user to results page', () => {
    const spy = spyOn(router, 'navigateByUrl');
    component.searchPartNumbers();
    const url = spy.calls.first().args[0];
    expect(url).toBe('/trade-compliance/search');
  });
});
