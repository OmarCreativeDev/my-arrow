import { Component, EventEmitter, Input, Output, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { bind, get, findIndex, some } from 'lodash-es';
import { chain } from 'lodash';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { TradeComplianceService } from '@app/core/trade-compliance/trade-compliance.service';
import {
  ToggleCheckPartNumber,
  ToggleAllCheckPartNumbers,
  ToggleCheckCooPartNumber,
  ToggleAllCheckCooPartNumbers,
} from '@app/features/trade-compliance/components/search-results/store/search-results.actions';

import { IAppState } from '@app/shared/shared.interfaces';

import {
  CertificateType,
  CertificateTypeNames,
  ICOOPart,
  IHTCPart,
  INAFTAPart,
  IPart,
  ITradeComplianceSearchResults,
  ITradeComplianceSearchType,
} from '@app/core/trade-compliance/trade-compliance.interfaces';

import {
  GenerateCertificate,
  UpdateCertificateForm,
  GetUserInformation,
} from '@app/features/trade-compliance/components/generate-certificate/store/generate-certificate.actions';

import { CertificateNameType, ICertificate, IUserInformation } from '@app/core/generate-certificate/generate-certificate.interface';
import { IUser } from '@app/core/user/user.interface';

import {
  getNaftaYear,
  getPartNumberFound,
  getPartNumberNotFound,
  getSearchType,
  getCooPartNumberFound,
} from '@app/features/trade-compliance/components/search-results/store/search-results.selectors';
import { getUser } from '@app/core/user/store/user.selectors';
import {
  getCertificateSelector,
  getUserInformation,
  getIsModalMessageOpenSelector,
} from '@app/features/trade-compliance/components/generate-certificate/store/generate-certificate.selectors';
import { ToggleCertificateModal } from '@app/features/trade-compliance/components/generate-certificate/store/generate-certificate.actions';
import { ICountry } from '@app/core/properties/properties.interface';
import { getCountryListSelector } from '@app/features/properties/store/properties.selectors';
import { GetCountryList } from '@app/features/properties/store/properties.actions';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss'],
})
export class SearchResultsComponent implements OnInit, OnDestroy {
  prepareCertificateForm: FormGroup;
  countryCodeError: boolean = false;

  @Input()
  results: ITradeComplianceSearchResults;
  @Output()
  public hidePipelineNaftaFormModalChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  public allCheckboxesTicked: boolean = true;
  public allCooCheckboxesTicked: boolean = true;
  public certificateType: string = undefined;
  public certificateTypeNames = CertificateTypeNames;

  public searchResultsFound: Array<INAFTAPart | ICOOPart | IHTCPart> = [];
  public searchCooResultsFound: Array<ICOOPart> = [];
  public searchResultsNotFound: String[];

  public certificate$: Observable<ICertificate>;
  public userInformation$: Observable<IUserInformation>;
  public isCertificateModalOpen$: Observable<boolean>;
  public getSearchType$: Observable<ITradeComplianceSearchType>;
  public searchType: ITradeComplianceSearchType;

  public tradeCompliancePartNumbersFound$: Observable<Array<INAFTAPart | ICOOPart | IHTCPart>>;
  public tradeComplianceCooPartNumbers$: Observable<Array<ICOOPart>>;
  public tradeCompliancePartNumbersNotFound$: Observable<String[]>;
  public tradeComplianceNaftaYear$: Observable<number>;

  public userProfile$: Observable<IUser>;

  public certificate: ICertificate;
  public userInformation: IUserInformation;
  public isModalMessageOpen: boolean;

  public tradeCompliancePartNumbersFound: Array<INAFTAPart | ICOOPart | IHTCPart>;
  public tradeComplianceCooPartNumbers: Array<ICOOPart>;
  public tradeCompliancePartNumbersNotFound: String[];

  public tradeComplianceNaftaYear: number;
  public userProfile: IUser;

  public prepareCertificateModalVisible: boolean = false;

  public countryList$: Observable<Array<ICountry>>;
  public countryList: Array<ICountry>;
  public countryListFinal;

  private MAX_PART_NUMBERS: number = 29;

  public EMAIL_MESSAGE_TITLE: string = '';
  public EMAIL_MESSAGE: string = '';
  public EMAIL_ACTION_BTN_MESSAGE: string = '';

  public htcEmailMessageTitle: string = 'Email Information';
  public htcEmailMessage: string = 'The requested information has been sent to your email address.';
  public htcEmailActionButtonMessage: string = 'Close';

  public naftaEmailMessageTitle: string = 'Thank you';
  public naftaEmailMessage: string =
    'Thank you! You can now access your NAFTA certificate from your email account. If you’ve requested less than 30 line items, your certificate wlll be accessible from the resulting PDF window. To generate another NAFTA certificate, please select return to search on the NAFTA part list page.';
  public naftaEmailActionButtonMessage: string = 'Continue';

  public cooEmailMessage: string = 'The COO information has been sent to your email address';

  public subscription: Subscription = new Subscription();

  constructor(private formBuilder: FormBuilder, private tradeComplianceService: TradeComplianceService, private store: Store<IAppState>) {
    this.certificateType = CertificateType[this.tradeComplianceService.getData('certificateType')].toUpperCase();

    this.tradeCompliancePartNumbersFound$ = store.pipe(select(getPartNumberFound));
    this.tradeCompliancePartNumbersNotFound$ = store.pipe(select(getPartNumberNotFound));
    this.tradeComplianceCooPartNumbers$ = store.pipe(select(getCooPartNumberFound));
    this.tradeComplianceNaftaYear$ = store.pipe(select(getNaftaYear));
    this.getSearchType$ = store.pipe(select(getSearchType));

    this.certificate$ = store.pipe(select(getCertificateSelector));
    this.isCertificateModalOpen$ = store.pipe(select(getIsModalMessageOpenSelector));

    this.userInformation$ = store.pipe(select(getUserInformation));
    this.userProfile$ = store.pipe(select(getUser));
    this.countryList$ = store.pipe(select(getCountryListSelector));
  }

  ngOnInit(): void {
    this.countryListFinal = [
      {
        label: 'Select your country',
        value: '',
      },
    ];
    this.startSubscriptions();
    this.getUserInformation();
    this.createCertificateForm();
    this.getCountryListInformation();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeCertificate());
    this.subscription.add(this.subscribeIsCertificateModalOpen());
    this.subscription.add(this.subscribeTradeComplianceNaftaYear());
    this.subscription.add(this.subscribeTradeCompliancePartNumbersFound());
    this.subscription.add(this.subscribeTradeCompliancePartNumbersNotFound());
    this.subscription.add(this.subscribeTradeComplianceCooPartNumbers());
    this.subscription.add(this.subscribeUserProfile());
    this.subscription.add(this.subscribeUserInformation());
    this.subscription.add(this.subscribeCountryList());
    this.subscription.add(this.subscribeSearchType());
  }

  private subscribeCertificate(): Subscription {
    return this.certificate$.subscribe(certificate => {
      this.certificate = certificate;
      if (certificate.userInformation) {
        this.updateCertificateForm(certificate.userInformation);
      }
    });
  }

  private subscribeIsCertificateModalOpen(): Subscription {
    return this.isCertificateModalOpen$.subscribe(isModalMessageOpen => (this.isModalMessageOpen = isModalMessageOpen));
  }

  private subscribeTradeComplianceNaftaYear(): Subscription {
    return this.tradeComplianceNaftaYear$.subscribe(tradeComplianceNaftaYear => (this.tradeComplianceNaftaYear = tradeComplianceNaftaYear));
  }

  private subscribeTradeCompliancePartNumbersFound(): Subscription {
    return this.tradeCompliancePartNumbersFound$.subscribe(tradeCompliancePartNumbersFound => {
      // TODO: Temporal Fix to represent Part Numbers Found for other Certificates different than NAFTA
      this.searchResultsFound = tradeCompliancePartNumbersFound;
      this.tradeCompliancePartNumbersFound = tradeCompliancePartNumbersFound;
      this.allCheckboxesTicked = this.areAllPartNumbersChecked(tradeCompliancePartNumbersFound);
    });
  }

  private subscribeTradeCompliancePartNumbersNotFound(): Subscription {
    return this.tradeCompliancePartNumbersNotFound$.subscribe(tradeCompliancePartNumbersNotFound => {
      // TODO: Temporal Fix to represent Part Numbers Not Found for other Certificates different than NAFTA
      this.searchResultsNotFound = tradeCompliancePartNumbersNotFound;
      this.tradeCompliancePartNumbersNotFound = tradeCompliancePartNumbersNotFound;
    });
  }

  private subscribeTradeComplianceCooPartNumbers(): Subscription {
    return this.tradeComplianceCooPartNumbers$.subscribe(tradeComplianceCooPartNumbers => {
      // TODO: Temporal Fix to represent Part Numbers Found for other Certificates different than NAFTA
      this.searchCooResultsFound = tradeComplianceCooPartNumbers;
      this.tradeComplianceCooPartNumbers = tradeComplianceCooPartNumbers;
      this.allCooCheckboxesTicked = this.areAllPartNumbersChecked(tradeComplianceCooPartNumbers);
    });
  }

  private subscribeUserProfile(): Subscription {
    return this.userProfile$.subscribe(userProfile => (this.userProfile = userProfile));
  }

  private subscribeUserInformation(): Subscription {
    return this.userInformation$.subscribe(userInformation => (this.userInformation = userInformation));
  }

  private subscribeCountryList(): Subscription {
    return this.countryList$.subscribe(countryList => {
      this.transformCountryListInformation(countryList);
    });
  }

  private subscribeSearchType(): Subscription {
    return this.getSearchType$.subscribe(searchType => {
      this.searchType = searchType;
      if (searchType) {
        switch (searchType.label) {
          case 'htc':
            this.EMAIL_MESSAGE_TITLE = this.htcEmailMessageTitle;
            this.EMAIL_MESSAGE = this.htcEmailMessage;
            this.EMAIL_ACTION_BTN_MESSAGE = this.htcEmailActionButtonMessage;
            break;
          case 'nafta':
            this.EMAIL_MESSAGE_TITLE = this.naftaEmailMessageTitle;
            this.EMAIL_MESSAGE = this.naftaEmailMessage;
            this.EMAIL_ACTION_BTN_MESSAGE = this.naftaEmailActionButtonMessage;
            break;
          case 'coo':
            this.EMAIL_MESSAGE_TITLE = this.htcEmailMessageTitle;
            this.EMAIL_MESSAGE = this.cooEmailMessage;
            this.EMAIL_ACTION_BTN_MESSAGE = this.htcEmailActionButtonMessage;
            break;
        }
      }
    });
  }

  public getUserInformation(): void {
    this.store.dispatch(new GetUserInformation());
  }

  public areAllPartNumbersChecked(partNumbersFound): boolean {
    return !some(partNumbersFound, ['checked', false]);
  }

  public toggleAllFoundCheckboxes(): void {
    const _checked = !this.allCheckboxesTicked;

    const _partNumbersFound: Array<any> = this.tradeCompliancePartNumbersFound.map((partNumber: INAFTAPart | ICOOPart | IHTCPart) => ({
      ...partNumber,
      checked: _checked,
    }));

    this.store.dispatch(new ToggleAllCheckPartNumbers(_partNumbersFound));
  }

  public toggleCheckPartNumber(partNumber, checked = true): void {
    const index = findIndex(this.tradeCompliancePartNumbersFound, {
      partNumber,
    });

    this.store.dispatch(new ToggleCheckPartNumber({ index, checked: !checked }));
  }

  public toggleCooAllFoundCheckboxes(): void {
    const _checked = !this.allCooCheckboxesTicked;
    const _partNumbersFound: Array<any> = this.tradeComplianceCooPartNumbers.map((partNumber: ICOOPart) => ({
      ...partNumber,
      checked: _checked,
    }));

    this.store.dispatch(new ToggleAllCheckCooPartNumbers(_partNumbersFound));
  }

  public toggleCooCheckPartNumber(partNumber, checked = true): void {
    const index = findIndex(this.tradeComplianceCooPartNumbers, {
      partNumber,
    });

    this.store.dispatch(new ToggleCheckCooPartNumber({ index, checked: !checked }));
  }

  public createCertificateForm() {
    this.prepareCertificateForm = this.formBuilder.group({
      companyName: [this.userInformation.companyName, Validators.required],
      attentionOf: [this.userInformation.attentionOf, Validators.required],
      address: [this.userInformation.address, Validators.required],
      city: [this.userInformation.city, Validators.required],
      state: [this.userInformation.state, Validators.required],
      postalCode: [this.userInformation.postalCode, Validators.required],
      countryCode: [this.userInformation.countryCode, Validators.required],
      taxId: [this.userInformation.taxId, Validators.required],
    });
  }

  public updateCertificateForm(form: any) {
    if (this.prepareCertificateForm) {
      this.prepareCertificateForm.patchValue(form);
    }
  }

  public getCountryListInformation(): void {
    this.store.dispatch(new GetCountryList());
  }

  public displayCountryValue(value) {
    value === '' ? (this.countryCodeError = true) : (this.countryCodeError = false);
    this.prepareCertificateForm.patchValue({ countryCode: value });
    this.updateCertificateFormField('countryCode', value);
  }

  public transformCountryListInformation(countryList) {
    if (countryList.length > 1) {
      const tempList = countryList;

      this.countryListFinal = tempList.map(country => {
        return { label: country.name, value: country.code };
      });
      this.countryListFinal.unshift({
        label: 'Select your country',
        value: '',
      });
    }
  }

  public getSelectedPartNumberFound() {
    return <Array<string>>chain(this.tradeCompliancePartNumbersFound)
      .filter({ checked: true })
      .map((item: IPart) => item.partNumber)
      .value();
  }

  public getSelectedCooPartNumberFound() {
    return <Array<string>>chain(this.tradeComplianceCooPartNumbers)
      .filter({ checked: true })
      .map((item: IPart) => item.id)
      .value();
  }

  //TODO remove this and use only one method for NAFTA, COO and HTC.
  public getSelectedIdsFound() {
    return <Array<string>>chain(this.tradeCompliancePartNumbersFound)
      .filter({ checked: true })
      .map((item: IPart) => item.id)
      .value();
  }

  public consolidateCertificateNaftaInfo(): ICertificate {
    const selectedPartNumbers = this.getSelectedPartNumberFound();

    const isrEmail = this.userProfile.contact.salesRepEmail || null;
    const certificate: ICertificate = {
      parts: selectedPartNumbers,
      to: this.userProfile.email,
      userInformation: this.certificate.userInformation,
      year: this.tradeComplianceNaftaYear,
      certificateNameType: CertificateNameType.nafta,
      isrEmail,
    };
    return certificate;
  }

  public consolidateCertificateInfo(certificateType: CertificateNameType): ICertificate {
    const selectedPartNumbers =
      certificateType === CertificateNameType.nafta ? this.getSelectedPartNumberFound() : this.getSelectedIdsFound();

    const isrEmail = this.userProfile.contact.salesRepEmail || null;
    const certificate: ICertificate = {
      parts: selectedPartNumbers,
      to: this.userProfile.email,
      certificateNameType: certificateType,
      // TODO: THIS NEEDS TO BE ASSIGNED IN A BETTER WAY, USING OBSERVABLE CHAINED RESULTS
      userInformation: this.certificate.userInformation,
      isrEmail,
    };
    return certificate;
  }

  public consolidateCooCertificateInfo(): ICertificate {
    const selectedPartNumbers = this.getSelectedCooPartNumberFound();
    const isrEmail = this.userProfile.contact.salesRepEmail || null;
    const certificate: ICertificate = {
      parts: selectedPartNumbers,
      to: this.userProfile.email,
      certificateNameType: CertificateNameType.coo,
      userInformation: this.certificate.userInformation,
      isrEmail,
    };
    return certificate;
  }

  private openWindowAndDisplayPDF(base64PDF: string) {
    const windowCb = get(this, '_windowCb');
    if (windowCb) {
      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        windowCb.close();
        const byteCharacters = atob(base64PDF);
        const byteNumbers = new Array(byteCharacters.length);
        for (let i = 0; i < byteCharacters.length; i++) {
          byteNumbers[i] = byteCharacters.charCodeAt(i);
        }
        const byteArray = new Uint8Array(byteNumbers);
        const blob = new Blob([byteArray], {
          type: 'application/pdf',
        });
        window.navigator.msSaveOrOpenBlob(blob, 'NAFTA Certificate.pdf');
      } else {
        const embed = document.createElement('embed');
        embed.src = 'data:application/pdf;base64,' + base64PDF;
        embed.width = '100%';
        embed.height = '100%';
        const body = windowCb.document.body;
        windowCb.document.title = 'NAFTA Certificate';
        body.appendChild(embed);
      }
    }
  }

  public getPDFCallback() {
    const _windowCb = window.open('');
    return bind(this.openWindowAndDisplayPDF, { _windowCb });
  }

  public generateCertificateNafta(): void {
    const certificate = this.consolidateCertificateNaftaInfo();
    const _windowCb = certificate.parts.length <= this.MAX_PART_NUMBERS ? this.getPDFCallback() : () => {};
    this.store.dispatch(new GenerateCertificate({ certificate, _windowCb }));
    this.prepareCertificateModalVisible = false;
  }

  public updateCertificateFormField(fieldName, value): void {
    this.store.dispatch(new UpdateCertificateForm({ fieldName, value }));
  }

  public generateCertificateHtc(): void {
    const certificate = this.consolidateCertificateInfo(CertificateNameType.htc);
    this.store.dispatch(new GenerateCertificate({ certificate }));
    this.EMAIL_MESSAGE = this.htcEmailMessage;
  }

  public generateCertificateCoo(): void {
    const certificate = this.consolidateCertificateInfo(CertificateNameType.coo);
    this.store.dispatch(new GenerateCertificate({ certificate }));
    this.EMAIL_MESSAGE = this.cooEmailMessage;
  }

  public generateCertificateCooOnNotFound(): void {
    const certificate = this.consolidateCooCertificateInfo();
    this.store.dispatch(new GenerateCertificate({ certificate }));
    this.EMAIL_MESSAGE = this.cooEmailMessage;
  }

  public isValidFormField(propName) {
    if (this.prepareCertificateForm) {
      return (
        this.prepareCertificateForm.value[propName] === '' &&
        (this.prepareCertificateForm.controls[propName].touched || this.prepareCertificateForm.controls[propName].dirty)
      );
    } else {
      return null;
    }
  }

  public onCertificateModalClickOutside($event) {
    this.store.dispatch(new ToggleCertificateModal($event));
  }
}
