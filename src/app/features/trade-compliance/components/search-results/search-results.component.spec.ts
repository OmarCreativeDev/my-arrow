import { ICertificate } from '@app/core/generate-certificate/generate-certificate.interface.ts';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { ICOOPart, IHTCPart, INAFTAPart, ITradeComplianceSearchResults } from '@app/core/trade-compliance/trade-compliance.interfaces';
import { TradeComplianceService } from '@app/core/trade-compliance/trade-compliance.service';
import { SearchResultsComponent } from './search-results.component';
import { SharedModule } from '@app/shared/shared.module';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { BackdropServiceMock, ModalServiceMock } from '@app/app.component.spec';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GenerateCertificateService } from '@app/core/generate-certificate/generate-certificate.service';
import { Store, StoreModule } from '@ngrx/store';
import { ModalService } from '@app/shared/services/modal.service';
import { GenerateCertificateModalMessageComponent } from '@app/features/trade-compliance/components/generate-certificate/generate-certificate-modal-message.component';
import { CommonModule } from '@angular/common';
import { tradeComplianceSearchReducers } from '@app/features/trade-compliance/components/search-results/store/search-results.reducers';
import { userReducers } from '@app/core/user/store/user.reducers';
import { generateCertificateReducers } from '@app/features/trade-compliance/components/generate-certificate/store/generate-certificate.reducers';
import { HttpClientModule } from '@angular/common/http';
import { By } from '@angular/platform-browser';
import {
  SearchPartNumbers,
  ToggleAllCheckPartNumbers,
  ToggleCheckPartNumber,
  UpdateSearchType,
} from '@app/features/trade-compliance/components/search-results/store/search-results.actions';
import { ApiService } from '@app/core/api/api.service';
import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { SearchResultsEffects } from '@app/features/trade-compliance/components/search-results/store/search-results.effects';
import { EffectsModule } from '@ngrx/effects';
import { ICountryList } from '@app/core/properties/properties.interface';
import { PropertiesService } from '@app/core/properties/properties.service';
import { PropertiesModule } from '@app/features/properties/properties.module';
import { propertiesReducers } from '@app/features/properties/store/properties.reducers';
import { PropertiesEffects } from '@app/features/properties/store/properties.effects';
import { CertificateNameType } from '@app/core/generate-certificate/generate-certificate.interface';
import {
  GetUserInformation,
  GenerateCertificate,
  UpdateCertificateForm,
  ToggleCertificateModal,
} from '@app/features/trade-compliance/components/generate-certificate/store/generate-certificate.actions';
import { GetCountryList } from '@app/features/properties/store/properties.actions';
import { UserCustomerType } from '@app/core/user/user.interface';

const mockSearchResults: ITradeComplianceSearchResults = {
  NMC1210NPO472J50TRPLPF: [
    {
      itemType: 'NAFTA',
      partNumber: 'NMC1210NPO472J50TRPLPF',
      countryOfOrigin: 'Costa Rica',
      id: '1234',
    },
  ],
  '89037-102LF': [
    {
      itemType: 'NAFTA',
      partNumber: '89037-102LF',
      countryOfOrigin: 'Costa Rica',
      id: '12345',
    },
  ],
  MI400: [],
};

const mockCountryListResults: ICountryList = {
  countryList: [
    {
      code: 'US',
      name: 'United States',
    },
    {
      code: 'CR',
      name: 'Costa Rica',
    },
  ],
};

const mockUserInfo = {
  companyName: '',
  attentionOf: '',
  address: '',
  city: '',
  state: '',
  postalCode: '',
  countryCode: '',
  email: '',
  taxId: '',
};

const mockUserProfile = {
  accountId: 1,
  accountSiteID: 1,
  organizationPartyID: 1,
  accountNumber: 1,
  company: '',
  contact: {
    customerServiceEmail: '',
    customerServiceNumber: '',
    salesRepName: '',
    salesRepPhoneNumber: '',
    salesRepEmail: '',
  },
  country: '',
  customerType: UserCustomerType.CEM,
  currencyCode: '',
  currencyList: [''],
  defaultLanguage: '',
  email: '',
  firstName: '',
  internalUserId: 1,
  isTsAndCsAccepted: false,
  lastName: '',
  orgId: 1,
  paymentsTermDescription: '',
  paymentsTermId: 1,
  paymentsTermName: '',
  permissionList: [''],
  region: '',
  registrationInstance: '',
  selectedBillTo: 1,
  selectedShipTo: 1,
  userType: 1,
  warehouses: [
    {
      id: 1,
      code: '',
      arrowReel: false,
    },
  ],
  phoneNumber: '',
  userPartyID: 1,
};

const resultingNAFTACertificate: ICertificate = {
  parts: ['NMC1210NPO472J50TRPLPF', '89037-102LF'],
  to: '',
  isrEmail: null,
  userInformation: mockUserInfo,
  year: 2014,
  certificateNameType: CertificateNameType.nafta,
};

const resultingCertificate: ICertificate = {
  parts: ['1234', '12345'],
  to: '',
  isrEmail: null,
  certificateNameType: CertificateNameType.htc,
  userInformation: mockUserInfo,
};

export class PropertiesServiceMock {
  public getCountryList(): Observable<ICountryList> {
    return of(mockCountryListResults);
  }
}

export class TradeComplianceServiceMock {
  public getSearchResults(): Observable<ITradeComplianceSearchResults> {
    return of(mockSearchResults);
  }

  public getData(field: string): number {
    return 0;
  }
}

const mockGenerateCertificateResult: string = 'TASDAS34ADASDASSADSADASDSADAS';
const partNumbers = 'NMC1210NPO472J50TRPLPF,89037-102LF,MI400';

export class GenerateCertificateServiceMock {
  public generate(): Observable<string> {
    return of(mockGenerateCertificateResult);
  }
}

describe('SearchResultsComponent', () => {
  let component: SearchResultsComponent;
  let fixture: ComponentFixture<SearchResultsComponent>;
  let tradeComplianceStore: Store<any>;
  let tradeComplianceService: TradeComplianceService;
  let propertiesService: PropertiesService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchResultsComponent, GenerateCertificateModalMessageComponent],
      providers: [
        { provide: BackdropService, useClass: BackdropServiceMock },
        { provide: ModalService, useClass: ModalServiceMock },
        {
          provide: GenerateCertificateService,
          useClass: GenerateCertificateServiceMock,
        },
        Store,
        { provide: PropertiesService, useClass: PropertiesServiceMock },
        TradeComplianceService,
        ApiService,
        AuthTokenService,
      ],
      imports: [
        CommonModule,
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        PropertiesModule,
        StoreModule.forRoot({
          user: userReducers,
          tradeCompliancePartNumbers: tradeComplianceSearchReducers,
          tradeComplianceCertificate: generateCertificateReducers,
          properties: propertiesReducers,
        }),
        EffectsModule.forRoot([PropertiesEffects, SearchResultsEffects]),
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    tradeComplianceService = TestBed.get(TradeComplianceService);
    tradeComplianceService.setData('certificateType', 0);
    tradeComplianceService.setData('year', 2014);
    tradeComplianceService.setData('parts', partNumbers);
    tradeComplianceService.getSearchResults = (): Observable<ITradeComplianceSearchResults> => of(mockSearchResults);

    fixture = TestBed.createComponent(SearchResultsComponent);
    component = fixture.componentInstance;
    tradeComplianceStore = fixture.debugElement.injector.get(Store);
    tradeComplianceStore.dispatch(new SearchPartNumbers());
    tradeComplianceStore.dispatch(new UpdateSearchType({ label: '', value: 5 }));
    fixture.detectChanges();
    tradeComplianceService = TestBed.get(TradeComplianceService);
    propertiesService = TestBed.get(PropertiesService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create services', () => {
    expect(tradeComplianceService).toBeTruthy();
    expect(propertiesService).toBeTruthy();
  });

  it('should add results to `tradeCompliancePartNumbersFound` or `tradeCompliancePartNumbersNotFound` when data is received', () => {
    expect(component.tradeCompliancePartNumbersFound.length > 0).toBeTruthy();
    expect(component.tradeCompliancePartNumbersNotFound.length > 0).toBeTruthy();
  });

  it('prepareCertificateModal is visible when prepareCertificateModalVisible goes from false to true', () => {
    fixture.detectChanges();
    let flag = component.prepareCertificateModalVisible;

    const modal = fixture.debugElement.query(By.css('.prepare-certificate__modal'));

    expect(flag).toBeFalsy();
    expect(modal.attributes['ng-reflect-visible']).toBe('false');
    flag = true;
    expect(modal.attributes['ng-reflect-visible']).toBeTruthy();
  });

  it('transformCountryListInformation transforms the regularCountrylist into the countryListFinal', () => {
    const regularCountrylist = [
      {
        code: 'CR',
        name: 'Costa Rica',
      },
      {
        code: 'US',
        name: 'United States',
      },
    ];
    const countryListFinal = [
      {
        label: 'Select your country',
        value: '',
      },
      {
        label: 'Costa Rica',
        value: 'CR',
      },
      {
        label: 'United States',
        value: 'US',
      },
    ];
    component.transformCountryListInformation(regularCountrylist);
    expect(component.countryListFinal).toEqual(countryListFinal);
  });

  it('toggleAllFoundCheckboxes should dispatch the action ToggleAllCheckPartNumbers', () => {
    fixture.detectChanges();

    const _partNumbersFound: Array<INAFTAPart | ICOOPart | IHTCPart> = component.tradeCompliancePartNumbersFound.map(
      (partNumber: INAFTAPart | ICOOPart | IHTCPart) => ({
        ...partNumber,
        checked: false,
      })
    );

    spyOn(tradeComplianceStore, 'dispatch').and.callThrough();

    component.toggleAllFoundCheckboxes();

    const action = new ToggleAllCheckPartNumbers(_partNumbersFound);
    expect(tradeComplianceStore.dispatch).toHaveBeenCalledWith(action);
  });

  it('areAllPartNumbersChecked should return true when all Part Numbers are checked', () => {
    const _partNumbersFoundChecked: Array<INAFTAPart | ICOOPart | IHTCPart> = component.tradeCompliancePartNumbersFound.map(
      (partNumber: INAFTAPart | ICOOPart | IHTCPart) => ({
        ...partNumber,
        checked: true,
      })
    );

    const actual = component.areAllPartNumbersChecked(_partNumbersFoundChecked);
    expect(actual).toBeTruthy();
  });

  it('when htc is selected, then htc email message displayed', () => {
    tradeComplianceStore.dispatch(new UpdateSearchType({ label: 'htc', value: 2 }));
    fixture.detectChanges();
    expect(component.EMAIL_MESSAGE_TITLE).toBe(component.htcEmailMessageTitle);
    expect(component.EMAIL_MESSAGE).toBe(component.htcEmailMessage);
    expect(component.EMAIL_ACTION_BTN_MESSAGE).toBe(component.htcEmailActionButtonMessage);
  });

  it('when nafta is selected, then nafta email message displayed', () => {
    tradeComplianceStore.dispatch(new UpdateSearchType({ label: 'nafta', value: 0 }));
    fixture.detectChanges();
    expect(component.EMAIL_MESSAGE_TITLE).toBe(component.naftaEmailMessageTitle);
    expect(component.EMAIL_MESSAGE).toBe(component.naftaEmailMessage);
    expect(component.EMAIL_ACTION_BTN_MESSAGE).toBe(component.naftaEmailActionButtonMessage);
  });

  describe('Certificate Info', () => {
    it('getSelectedPartNumberFound should return all partNumber selected', () => {
      const result = component.getSelectedPartNumberFound();
      expect(result).toEqual(['NMC1210NPO472J50TRPLPF', '89037-102LF']);
    });

    it('consolidateCertificateInfo should return the certificate correctly filled', () => {
      component.certificate.userInformation = mockUserInfo;
      component.userProfile = mockUserProfile;

      const result = component.consolidateCertificateInfo(CertificateNameType.htc);
      expect(result).toEqual(resultingCertificate);
    });

    it('consolidateCertificateNaftaInfo should return the certificate correctly filled', () => {
      component.certificate.userInformation = mockUserInfo;
      component.userProfile = mockUserProfile;
      component.tradeComplianceNaftaYear = 2014;

      const result = component.consolidateCertificateNaftaInfo();
      expect(result).toEqual(resultingNAFTACertificate);
    });
  });

  describe('Search Result Actions', () => {
    let store: Store<any>;

    beforeEach(() => {
      store = TestBed.get(Store);
      spyOn(store, 'dispatch').and.callThrough();
      fixture.detectChanges();
    });

    it('getUserInformation should dispatch the correct action and get the User Information into the store', () => {
      component.getUserInformation();

      const action = new GetUserInformation();
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });

    it('toggleCheckPartNumber should dispatch ToggleCheckPartNumber action and update the checkPartNumber Information into the store', () => {
      component.toggleCheckPartNumber('NMC1210NPO472J50TRPLPF', true);

      const action = new ToggleCheckPartNumber({ index: 0, checked: false });
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });

    it('getCountryListInformation should dispatch GetCountryList action and update the CountryList Information into the store', () => {
      component.getCountryListInformation();

      const action = new GetCountryList();
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });

    it('updateCertificateFormField should dispatch UpdateCertificateForm with FieldName and Value', () => {
      const fieldName = 'fieldName';
      const value = 'value';
      component.updateCertificateFormField(fieldName, value);

      const action = new UpdateCertificateForm({ fieldName, value });
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });

    it('generateCertificateHtc should dispatch GenerateCertificate for HTC', () => {
      component.certificate.userInformation = mockUserInfo;
      component.userProfile = mockUserProfile;
      const certificate = component.consolidateCertificateInfo(CertificateNameType.htc);
      component.generateCertificateHtc();

      const action = new GenerateCertificate({ certificate });
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });

    it('generateCertificateCoo should dispatch GenerateCertificate for COO', () => {
      component.certificate.userInformation = mockUserInfo;
      component.userProfile = mockUserProfile;
      const certificate = component.consolidateCertificateInfo(CertificateNameType.coo);
      component.generateCertificateCoo();

      const action = new GenerateCertificate({ certificate });
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });

    it('onCertificateModalClickOutside should dispatch ToggleCertificateModal action to close the modal', () => {
      const mockEvent = false;
      component.onCertificateModalClickOutside(mockEvent);

      const action = new ToggleCertificateModal(false);
      expect(store.dispatch).toHaveBeenCalledWith(action);
    });
  });

  it('isValidFormField should return the  prepareCertificateForm attributes if prepareCertificateForm is defined', () => {
    const propName = 'PropName';
    const result = component.isValidFormField(propName);
    const mockResult =
      component.prepareCertificateForm.value[propName] === '' &&
      (component.prepareCertificateForm.controls[propName].touched || component.prepareCertificateForm.controls[propName].dirty);

    expect(result).toEqual(mockResult);
  });
});
