import { Action } from '@ngrx/store';
import {
  ICOOPart,
  IHTCPart,
  INAFTAPart,
  ITradeComplianceSearchResults,
  ITradeComplianceSearchType,
  ICheckPartNumber,
} from '@app/core/trade-compliance/trade-compliance.interfaces';

export enum TradeComplianceActionTypes {
  SEARCH_PART_NUMBERS = 'SEARCH_PART_NUMBERS',
  SEARCH_PART_NUMBERS_COMPLETE = 'SEARCH_PART_NUMBERS_COMPLETE',
  SEARCH_PART_NUMBERS_ERROR = 'SEARCH_PART_NUMBERS_ERROR',
  UPDATE_NAFTA_YEAR = 'UPDATE_NAFTA_YEAR',
  UPDATE_SEARCH_TYPE = 'UPDATE_SEARCH_TYPE',
  SET_PART_NUMBERS_FOUND = 'SET_PART_NUMBERS_FOUND',
  SET_PART_NUMBERS_NOT_FOUND = 'SET_PART_NUMBERS_NOT_FOUND',
  TOGGLE_CHECK_PART_NUMBER = 'TOGGLE_CHECK_PART_NUMBER',
  TOGGLE_ALL_CHECK_PART_NUMBERS = 'TOGGLE_ALL_CHECK_PART_NUMBERS',
  RESET_SEARCH_RESULTS = 'RESET_SEARCH_RESULTS',
  SET_COO_PART_NUMBERS_FOUND = 'SET_COO_PART_NUMBERS_FOUND',
  TOGGLE_ALL_CHECK_COO_PART_NUMBERS = 'TOGGLE_ALL_CHECK_COO_PART_NUMBERS',
  TOGGLE_CHECK_COO_PART_NUMBER = 'TOGGLE_CHECK_COO_PART_NUMBER',
  TRADE_COMPLIANCE_SEARCH = 'TRADE_COMPLIANCE_SEARCH',
  COMPLIANCE_CHECK_UPLOAD = 'COMPLIANCE_CHECK_UPLOAD',
}

/**
 * Triggers the searchPartNumbers
 */
export class SearchPartNumbers implements Action {
  readonly type = TradeComplianceActionTypes.SEARCH_PART_NUMBERS;
  constructor() {}
}

/**
 * Results of the searchPartNumbers
 */
export class SearchPartNumbersComplete implements Action {
  readonly type = TradeComplianceActionTypes.SEARCH_PART_NUMBERS_COMPLETE;
  constructor(public payload: ITradeComplianceSearchResults) {}
}

/**
 * Thrown if the searchPartNumbers received an Error
 */
export class SearchPartNumbersError implements Action {
  readonly type = TradeComplianceActionTypes.SEARCH_PART_NUMBERS_ERROR;
  constructor(public payload: Error) {}
}

/**
 * Triggers the update of the selected NAFTA year
 */
export class UpdateNaftaYear implements Action {
  readonly type = TradeComplianceActionTypes.UPDATE_NAFTA_YEAR;
  constructor(public payload: number) {}
}

/**
 * Triggers the update of the selected search type
 */
export class UpdateSearchType implements Action {
  readonly type = TradeComplianceActionTypes.UPDATE_SEARCH_TYPE;
  constructor(public payload: ITradeComplianceSearchType) {}
}

/**
 * Triggers the update of the part numbers found
 */
export class UpdatePartNumbersFound implements Action {
  readonly type = TradeComplianceActionTypes.SET_PART_NUMBERS_FOUND;
  constructor(public payload: Array<INAFTAPart | ICOOPart | IHTCPart>) {}
}

/**
 * Triggers the update of the part numbers found
 */
export class UpdatePartNumbersNotFound implements Action {
  readonly type = TradeComplianceActionTypes.SET_PART_NUMBERS_NOT_FOUND;
  constructor(public payload: Array<string>) {}
}

/**
 * Triggers the update of the coo part numbers found
 */
export class UpdateCooPartNumbersFound implements Action {
  readonly type = TradeComplianceActionTypes.SET_COO_PART_NUMBERS_FOUND;
  constructor(public payload: Array<ICOOPart>) {}
}

/**
 * Triggers the set check of the part number
 */
export class ToggleCheckPartNumber implements Action {
  readonly type = TradeComplianceActionTypes.TOGGLE_CHECK_PART_NUMBER;
  constructor(public payload: ICheckPartNumber) {}
}

/**
 * Triggers the set check of All the part numbers
 */
export class ToggleAllCheckPartNumbers implements Action {
  readonly type = TradeComplianceActionTypes.TOGGLE_ALL_CHECK_PART_NUMBERS;
  constructor(public payload: Array<INAFTAPart | ICOOPart | IHTCPart>) {}
}

/**
 * Triggers the set check of the part number
 */
export class ToggleCheckCooPartNumber implements Action {
  readonly type = TradeComplianceActionTypes.TOGGLE_CHECK_COO_PART_NUMBER;
  constructor(public payload: ICheckPartNumber) {}
}

/**
 * Triggers the set check of All the part numbers
 */
export class ToggleAllCheckCooPartNumbers implements Action {
  readonly type = TradeComplianceActionTypes.TOGGLE_ALL_CHECK_COO_PART_NUMBERS;
  constructor(public payload: Array<ICOOPart>) {}
}

export class ResetSearchResults implements Action {
  readonly type = TradeComplianceActionTypes.RESET_SEARCH_RESULTS;
  constructor() {}
}

export class TradeComplianceSearch implements Action {
  readonly type = TradeComplianceActionTypes.TRADE_COMPLIANCE_SEARCH;
  constructor(public payload: string) {}
}

export class ComplianceCheckUpload implements Action {
  readonly type = TradeComplianceActionTypes.COMPLIANCE_CHECK_UPLOAD;
  constructor(public payload: string) {}
}

export type TradeComplianceActions =
  | SearchPartNumbers
  | SearchPartNumbersComplete
  | SearchPartNumbersError
  | UpdateNaftaYear
  | UpdateSearchType
  | UpdatePartNumbersFound
  | UpdatePartNumbersNotFound
  | ToggleCheckPartNumber
  | ToggleAllCheckPartNumbers
  | UpdateCooPartNumbersFound
  | ResetSearchResults
  | ToggleCheckCooPartNumber
  | ToggleAllCheckCooPartNumbers
  | TradeComplianceSearch
  | ComplianceCheckUpload;
