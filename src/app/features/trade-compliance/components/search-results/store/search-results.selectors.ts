import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ITradeComplianceSearchState, ITradeComplianceSearchResults } from '@app/core/trade-compliance/trade-compliance.interfaces';

/**
 * Selects the trade compliance search state from the root state object
 */
export const getSearchResultsState = createFeatureSelector<ITradeComplianceSearchState<Array<ITradeComplianceSearchResults>>>(
  'tradeCompliancePartNumbers'
);

/**
 * Retrieves the result
 */
export const getResults = createSelector(getSearchResultsState, searchResultsState => searchResultsState.results);

/**
 * Retrieves the nafta year
 */
export const getNaftaYear = createSelector(getSearchResultsState, searchResultsState => searchResultsState.naftaYear);

/**
 * Retrieves the search type
 */
export const getSearchType = createSelector(getSearchResultsState, searchResultsState => searchResultsState.searchType);

/**
 * Retrieve the loading state of the page
 */
export const getLoading = createSelector(getSearchResultsState, searchResultsState => searchResultsState.loading);

/**
 * Retrieve the error state of the page
 */
export const getError = createSelector(getSearchResultsState, searchResultsState => searchResultsState.error);

/**
 * Retrieve the part number items found
 */
export const getPartNumberFound = createSelector(getSearchResultsState, searchResultsState => searchResultsState.partNumbersFound);

/**
 * Retrieve the part number items found
 */
export const getPartNumberNotFound = createSelector(getSearchResultsState, searchResultsState => searchResultsState.partNumbersNotFound);

/**
 * Retrieve the coo part number items found
 */
export const getCooPartNumberFound = createSelector(getSearchResultsState, searchResultsState => searchResultsState.cooPartNumbersFound);
