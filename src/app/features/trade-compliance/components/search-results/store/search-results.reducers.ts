import { cloneDeep } from 'lodash-es';
import { TradeComplianceActions, TradeComplianceActionTypes } from './search-results.actions';
import { ITradeComplianceSearchState, ITradeComplianceSearchResults } from '@app/core/trade-compliance/trade-compliance.interfaces';

export const INITIAL_RESULTS_STATE: ITradeComplianceSearchState<ITradeComplianceSearchResults> = {
  loading: false,
  error: undefined,
  results: undefined,
  naftaYear: undefined,
  partNumbersFound: [],
  cooPartNumbersFound: [],
  partNumbersNotFound: [],
  searchType: undefined,
};

/**
 * Mutates the state for given set of TradeComplianceActions
 * @param state
 * @param action
 */
export function tradeComplianceSearchReducers(
  state: ITradeComplianceSearchState<ITradeComplianceSearchResults> = INITIAL_RESULTS_STATE,
  action: TradeComplianceActions
): ITradeComplianceSearchState<ITradeComplianceSearchResults> {
  switch (action.type) {
    case TradeComplianceActionTypes.SEARCH_PART_NUMBERS: {
      return {
        ...state,
        loading: true,
        error: undefined,
        results: undefined,
      };
    }
    case TradeComplianceActionTypes.SEARCH_PART_NUMBERS_COMPLETE: {
      return {
        ...state,
        results: action.payload,
        loading: false,
        error: undefined,
      };
    }
    case TradeComplianceActionTypes.SEARCH_PART_NUMBERS_ERROR: {
      return {
        ...state,
        error: action.payload,
        results: undefined,
        loading: false,
      };
    }
    case TradeComplianceActionTypes.UPDATE_NAFTA_YEAR: {
      return {
        ...state,
        naftaYear: action.payload,
      };
    }
    case TradeComplianceActionTypes.UPDATE_SEARCH_TYPE: {
      return {
        ...state,
        searchType: action.payload,
      };
    }
    case TradeComplianceActionTypes.SET_PART_NUMBERS_FOUND: {
      return {
        ...state,
        partNumbersFound: action.payload,
      };
    }
    case TradeComplianceActionTypes.SET_PART_NUMBERS_NOT_FOUND: {
      return {
        ...state,
        partNumbersNotFound: action.payload,
      };
    }
    case TradeComplianceActionTypes.SET_COO_PART_NUMBERS_FOUND: {
      return {
        ...state,
        cooPartNumbersFound: action.payload,
      };
    }
    case TradeComplianceActionTypes.TOGGLE_CHECK_PART_NUMBER: {
      const { index, checked } = action.payload;
      const partNumber = cloneDeep(state.partNumbersFound[index]);

      return {
        ...state,
        partNumbersFound: [
          ...state.partNumbersFound.slice(0, index),
          {
            ...partNumber,
            checked,
          },
          ...state.partNumbersFound.slice(index + 1),
        ],
      };
    }
    case TradeComplianceActionTypes.TOGGLE_CHECK_COO_PART_NUMBER: {
      const { index, checked } = action.payload;
      const partNumber = cloneDeep(state.cooPartNumbersFound[index]);

      return {
        ...state,
        cooPartNumbersFound: [
          ...state.cooPartNumbersFound.slice(0, index),
          {
            ...partNumber,
            checked,
          },
          ...state.cooPartNumbersFound.slice(index + 1),
        ],
      };
    }
    case TradeComplianceActionTypes.TOGGLE_ALL_CHECK_PART_NUMBERS: {
      return {
        ...state,
        partNumbersFound: action.payload,
      };
    }
    case TradeComplianceActionTypes.TOGGLE_ALL_CHECK_COO_PART_NUMBERS: {
      return {
        ...state,
        cooPartNumbersFound: action.payload,
      };
    }
    case TradeComplianceActionTypes.RESET_SEARCH_RESULTS: {
      const { results, partNumbersFound, partNumbersNotFound, cooPartNumbersFound } = INITIAL_RESULTS_STATE;

      return {
        ...state,
        results,
        partNumbersFound,
        partNumbersNotFound,
        cooPartNumbersFound,
      };
    }
    case TradeComplianceActionTypes.TRADE_COMPLIANCE_SEARCH: {
      return {
        ...state,
      };
    }
    case TradeComplianceActionTypes.COMPLIANCE_CHECK_UPLOAD: {
      return {
        ...state,
      };
    }
    default: {
      return state;
    }
  }
}
