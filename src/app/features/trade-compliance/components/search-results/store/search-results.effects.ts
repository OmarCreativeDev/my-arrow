import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { switchMap, catchError } from 'rxjs/operators';
import { isEmpty, isString, toLower } from 'lodash-es';
import { chain } from 'lodash';

import { TradeComplianceService } from '@app/core/trade-compliance/trade-compliance.service';
import {
  CertificateType,
  ITradeComplianceSearchResults,
  CertificateTypeNames,
} from '@app/core/trade-compliance/trade-compliance.interfaces';

import {
  TradeComplianceActionTypes,
  SearchPartNumbersError,
  SearchPartNumbersComplete,
  SearchPartNumbers,
  UpdatePartNumbersFound,
  UpdatePartNumbersNotFound,
  UpdateCooPartNumbersFound,
} from '@app/features/trade-compliance/components/search-results/store/search-results.actions';
@Injectable()
export class SearchResultsEffects {
  /**
   * Side-effect for the Query being called directly.
   */
  @Effect()
  query$ = this.actions$.pipe(
    ofType(TradeComplianceActionTypes.SEARCH_PART_NUMBERS),
    switchMap((action: SearchPartNumbers) => {
      return this.tradeComplianceService.getSearchResults().pipe(
        switchMap((queryResponse: ITradeComplianceSearchResults) => {
          const partNumbersFound = this.formatPartNumbersFound(queryResponse);
          const partNumbersNotFound = this.formatPartNumbersNotFound(queryResponse);
          const cooPartNumbersFound = this.formatPartNumbersFoundForCoo(queryResponse);
          return [
            new SearchPartNumbersComplete(queryResponse),
            new UpdatePartNumbersFound(partNumbersFound),
            new UpdatePartNumbersNotFound(partNumbersNotFound),
            new UpdateCooPartNumbersFound(cooPartNumbersFound),
          ];
        }),
        catchError(err => of(new SearchPartNumbersError(err)))
      );
    })
  );

  constructor(private actions$: Actions, private tradeComplianceService: TradeComplianceService) {}

  private formatPartNumbersFound(partNumbersFound) {
    const certificateTypeName = this.tradeComplianceService.getEndpoint(this.tradeComplianceService.certificateType);
    return chain(partNumbersFound)
      .map(item => item)
      .flatMapDeep()
      .toArray()
      .filter(item => toLower(item.itemType) === toLower(certificateTypeName))
      .filter(item => (this.tradeComplianceService.certificateType === CertificateType.coo ? !isEmpty(item.countryOfOrigin) : item))
      .uniqBy('partNumber')
      .map(item => ({ ...item, checked: true }))
      .value();
  }

  private formatPartNumbersFoundForCoo(cooPartNumbersFound) {
    return chain(cooPartNumbersFound)
      .map(item => item)
      .flatMapDeep()
      .toArray()
      .filter(item => !isEmpty(item) && item.itemType === CertificateTypeNames.coo)
      .map(item => ({ ...item, checked: true }))
      .value();
  }

  private formatPartNumbersNotFound(partNumbersNotFound): Array<string> {
    return chain(partNumbersNotFound)
      .map((item, key) =>
        this.tradeComplianceService.certificateType === CertificateType.coo
          ? (isEmpty(item) || isEmpty(item[0].countryOfOrigin)) && key
          : isEmpty(item) && key
      )
      .filter(isString)
      .toArray()
      .uniq()
      .value();
  }
}
