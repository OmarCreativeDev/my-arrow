import { cloneDeep } from 'lodash-es';
import {
  SearchPartNumbers,
  SearchPartNumbersComplete,
  SearchPartNumbersError,
  UpdateNaftaYear,
  UpdateSearchType,
  UpdatePartNumbersFound,
  TradeComplianceSearch,
  ComplianceCheckUpload,
} from './search-results.actions';
import { tradeComplianceSearchReducers, INITIAL_RESULTS_STATE } from './search-results.reducers';
import {
  INAFTAPart,
  ICOOPart,
  IHTCPart,
  ITradeComplianceSearchState,
  ITradeComplianceSearchResults,
  ICheckPartNumber,
} from '@app/core/trade-compliance/trade-compliance.interfaces';
import {
  ToggleAllCheckPartNumbers,
  ToggleCheckPartNumber,
  UpdatePartNumbersNotFound,
  ToggleCheckCooPartNumber,
  ToggleAllCheckCooPartNumbers,
} from '@app/features/trade-compliance/components/search-results/store/search-results.actions';

describe('Trade Compliance Search Results Reducers', () => {
  let initialState: ITradeComplianceSearchState<ITradeComplianceSearchResults>;
  let partNumbersFound: Array<INAFTAPart | ICOOPart | IHTCPart> = [];
  let cooPartNumbersFound: Array<ICOOPart> = [];
  let partNumbersNotFound: Array<string> = [];

  beforeEach(() => {
    initialState = {
      loading: false,
      error: undefined,
      results: undefined,
      searchType: undefined,
      naftaYear: undefined,
      partNumbersFound: [],
      cooPartNumbersFound: [],
      partNumbersNotFound: [],
    };

    partNumbersNotFound = ['89037-1', '89037-2', '89037-3'];

    partNumbersFound = [
      {
        partNumber: 'NMC1210NPO472J50TRPLPF',
        checked: true,
      },
      {
        partNumber: '89037-102LF',
        checked: true,
      },
    ];
    cooPartNumbersFound = [
      {
        partNumber: 'COO_12345678',
        checked: true,
      },
      {
        partNumber: 'COO_23456789',
        checked: true,
      },
    ];
  });

  it(`SEARCH_PART_NUMBERS should change loading to true and not have an error property`, () => {
    const action = new SearchPartNumbers();
    const result = tradeComplianceSearchReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeUndefined();
    expect(result.results).toBeUndefined();
  });

  it(`SEARCH_PART_NUMBERS_COMPLETE should set the results of the state, change loading to false and not have an error property`, () => {
    const response: ITradeComplianceSearchResults = {
      'A-PART-NUMBER-01': [
        {
          partNumber: 'A-PART-NUMBER-01',
          partDescription: 'A description',
        },
      ],
    };
    const action = new SearchPartNumbersComplete(response);
    const result = tradeComplianceSearchReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.results).toEqual(response);
  });

  it(`SEARCH_PART_NUMBERS_ERROR orderLines should be undefined and error property should have an error property`, () => {
    const expectedError = new Error('foo');
    const action = new SearchPartNumbersError(expectedError);
    const result = tradeComplianceSearchReducers(initialState, action);
    expect(result.error).toEqual(expectedError);
    expect(result.results).toBeUndefined();
  });

  it(`by default should return the state without transforming it`, () => {
    const result = tradeComplianceSearchReducers(undefined, {} as any);
    expect(result).toEqual(INITIAL_RESULTS_STATE);
  });

  it(`UPDATE_NAFTA_YEAR should have the correct payload`, () => {
    const response = 2014;

    const action = new UpdateNaftaYear(response);
    const result = tradeComplianceSearchReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.naftaYear).toEqual(response);
  });

  it(`UPDATE_SEARCH_TYPE should have the correct payload`, () => {
    const response = {
      label: 'nafta',
      value: 0,
    };
    const action = new UpdateSearchType(response);
    const result = tradeComplianceSearchReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.searchType).toEqual(response);
  });

  it(`SET_PART_NUMBERS_FOUND should have the correct payload`, () => {
    const action = new UpdatePartNumbersFound(partNumbersFound);
    const result = tradeComplianceSearchReducers(initialState, action);

    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.partNumbersFound).toEqual(partNumbersFound);
  });

  it(`SET_PART_NUMBERS_NOT_FOUND should have the correct payload`, () => {
    const action = new UpdatePartNumbersNotFound(partNumbersNotFound);
    const result = tradeComplianceSearchReducers(initialState, action);

    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.partNumbersNotFound).toEqual(partNumbersNotFound);
  });

  it(`TOGGLE_CHECK_PART_NUMBER should have the correct payload`, () => {
    const targetPartNumber: ICheckPartNumber = {
      index: 1,
      checked: false,
    };
    const action = new ToggleCheckPartNumber(targetPartNumber);
    const _initialState = { ...cloneDeep(initialState), partNumbersFound };
    const result = tradeComplianceSearchReducers(_initialState, action);

    const _partNumbersFound = cloneDeep(partNumbersFound);
    _partNumbersFound[targetPartNumber.index].checked = targetPartNumber.checked;

    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.partNumbersFound).toEqual(_partNumbersFound);
  });

  it(`TOGGLE_ALL_CHECK_PART_NUMBERS should have the correct payload`, () => {
    const _partNumbersFound = partNumbersFound.map((partNumber: INAFTAPart | ICOOPart | IHTCPart) => ({ ...partNumber, checked: false }));

    const action = new ToggleAllCheckPartNumbers(_partNumbersFound);
    const result = tradeComplianceSearchReducers(initialState, action);

    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.partNumbersFound).toEqual(_partNumbersFound);
  });

  it(`TOGGLE_CHECK_COO_PART_NUMBER should have the correct payload`, () => {
    const targetPartNumber: ICheckPartNumber = {
      index: 1,
      checked: false,
    };
    const action = new ToggleCheckCooPartNumber(targetPartNumber);
    const _initialState = { ...cloneDeep(initialState), cooPartNumbersFound };
    const result = tradeComplianceSearchReducers(_initialState, action);

    const _cooPartNumbersFound = cloneDeep(cooPartNumbersFound);
    _cooPartNumbersFound[targetPartNumber.index].checked = targetPartNumber.checked;

    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.cooPartNumbersFound).toEqual(_cooPartNumbersFound);
  });

  it(`TOGGLE_ALL_CHECK_COO_PART_NUMBERS should have the correct payload`, () => {
    const _cooPartNumbersFound = cooPartNumbersFound.map((partNumber: ICOOPart) => ({ ...partNumber, checked: false }));

    const action = new ToggleAllCheckCooPartNumbers(_cooPartNumbersFound);
    const result = tradeComplianceSearchReducers(initialState, action);

    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.cooPartNumbersFound).toEqual(_cooPartNumbersFound);
  });

  it('`TRADE_COMPLIANCE_SEARCH` should have the correct payload', () => {
    const action = new TradeComplianceSearch('NAFTA: 2014: 10-527334-19R');
    const result = tradeComplianceSearchReducers(initialState, action);
    expect(result).toEqual(initialState);
  });

  it('`COMPLIANCE_CHECK_UPLOAD` should have the correct payload', () => {
    const action = new ComplianceCheckUpload('type');
    const result = tradeComplianceSearchReducers(initialState, action);
    expect(result).toEqual(initialState);
  });
});
