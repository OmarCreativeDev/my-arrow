import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Store } from '@ngrx/store';
import { StoreMock } from '@app/features/products/pages/product-search/product-search.component.spec';

import { BackdropService } from '@app/shared/services/backdrop.service';
import { ModalService } from '@app/shared/services/modal.service';
import { GenerateCertificateModalMessageComponent } from './generate-certificate-modal-message.component';
import { SharedModule } from '@app/shared/shared.module';

describe('GenerateCertificateModalMessageComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, HttpClientTestingModule, SharedModule],
      declarations: [GenerateCertificateModalMessageComponent],
      providers: [{ provide: Store, useClass: StoreMock }, BackdropService, ModalService],
    }).compileComponents();
  }));

  it('should render modal', async(() => {
    const fixture = TestBed.createComponent(GenerateCertificateModalMessageComponent);
    const modal = fixture.debugElement.componentInstance;
    expect(modal).toBeTruthy();
  }));
});
