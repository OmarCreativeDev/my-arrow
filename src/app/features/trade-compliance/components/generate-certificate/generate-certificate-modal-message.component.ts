import { Component, Input, ElementRef, OnChanges } from '@angular/core';
import { Store } from '@ngrx/store';

import { ToggleCertificateModal } from '@app/features/trade-compliance/components/generate-certificate/store/generate-certificate.actions';
import { IAppState } from '@app/shared/shared.interfaces';
import { ToggleComponent } from '@app/shared/classes/toggle-component.class';

@Component({
  selector: 'app-generate-certificate-modal-message',
  templateUrl: './generate-certificate-modal-message.html',
  styleUrls: ['./generate-certificate-modal-message.component.scss'],
})
export class GenerateCertificateModalMessageComponent extends ToggleComponent implements OnChanges {
  @Input()
  title: string = '';
  @Input()
  description: string = '';
  @Input()
  actionLabel: string = '';
  @Input()
  visible: boolean;

  constructor(private store: Store<IAppState>, public elementRef: ElementRef) {
    super(elementRef);
  }

  public onCertificateModalConfirmation(): void {
    this.store.dispatch(new ToggleCertificateModal(false));
  }

  public ngOnChanges(): void {
    this.visible ? super.show() : super.hide();
  }
}
