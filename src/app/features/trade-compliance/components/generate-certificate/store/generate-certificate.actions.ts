import { Action } from '@ngrx/store';

export enum GenerateCertificateActionTypes {
  GENERATE_CERTIFICATE = 'GENERATE_CERTIFICATE',
  GENERATE_CERTIFICATE_COMPLETE = 'GENERATE_CERTIFICATE_COMPLETE',
  GENERATE_CERTIFICATE_ERROR = 'GENERATE_CERTIFICATE_ERROR',
  GET_USER_INFORMATION = 'GET_USER_INFORMATION',
  GET_USER_INFORMATION_COMPLETE = 'GET_USER_INFORMATION_COMPLETE',
  GET_USER_INFORMATION_ERROR = 'GET_USER_INFORMATION_ERROR',
  TOGGLE_CERTIFICATE_MODAL = 'TOGGLE_CERTIFICATE_MODAL',
  UPDATE_CERTIFICATE_FORM = 'UPDATE_CERTIFICATE_FORM',
  CLEAR_CERTIFICATE_FORM = 'CLEAR_CERTIFICATE_FORM',
}

export class GenerateCertificate implements Action {
  readonly type = GenerateCertificateActionTypes.GENERATE_CERTIFICATE;
  constructor(public payload: any) {}
}

export class GenerateCertificateComplete implements Action {
  readonly type = GenerateCertificateActionTypes.GENERATE_CERTIFICATE_COMPLETE;
  constructor(public payload: any) {}
}

export class GenerateCertificateError implements Action {
  readonly type = GenerateCertificateActionTypes.GENERATE_CERTIFICATE_ERROR;
  constructor(public payload: Error) {}
}

export class GetUserInformation implements Action {
  readonly type = GenerateCertificateActionTypes.GET_USER_INFORMATION;
  constructor() {}
}

export class GetUserInformationComplete implements Action {
  readonly type = GenerateCertificateActionTypes.GET_USER_INFORMATION_COMPLETE;
  constructor(public payload: any) {}
}

export class GetUserInformationError implements Action {
  readonly type = GenerateCertificateActionTypes.GET_USER_INFORMATION_ERROR;
  constructor(public payload: Error) {}
}

export class ToggleCertificateModal implements Action {
  readonly type = GenerateCertificateActionTypes.TOGGLE_CERTIFICATE_MODAL;
  constructor(public payload: boolean) {}
}

export class UpdateCertificateForm implements Action {
  readonly type = GenerateCertificateActionTypes.UPDATE_CERTIFICATE_FORM;
  constructor(public payload: any) {}
}

export class ClearCertificateForm implements Action {
  readonly type = GenerateCertificateActionTypes.CLEAR_CERTIFICATE_FORM;
  constructor() {}
}

export type GenerateCertificateActions =
  | GenerateCertificate
  | GenerateCertificateComplete
  | GenerateCertificateError
  | GetUserInformation
  | GetUserInformationComplete
  | GetUserInformationError
  | ToggleCertificateModal
  | UpdateCertificateForm
  | ClearCertificateForm;
