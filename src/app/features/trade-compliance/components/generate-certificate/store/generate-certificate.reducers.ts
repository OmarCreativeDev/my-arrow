import { cloneDeep } from 'lodash-es';
import {
  GenerateCertificateActions,
  GenerateCertificateActionTypes,
} from './generate-certificate.actions';
import {
  ICertificate,
  ICertificateState,
  IUserInformation,
} from '@app/core/generate-certificate/generate-certificate.interface.ts';

export const INITIAL_USER_INFO_STATE: IUserInformation = {
  companyName: '',
  attentionOf: '',
  address: '',
  city: '',
  state: '',
  postalCode: '',
  countryCode: '',
  email: '',
  taxId: '',
};

export const INITIAL_RESULTS_STATE: ICertificateState = {
  loading: false,
  isModalMessageOpen: false,
  error: undefined,
  results: undefined,
  certificate: {
    userInformation: cloneDeep(INITIAL_USER_INFO_STATE),
    parts: [],
    to: '',
    year: 0,
  },
};

/**
 * Mutates the state for given set of TradeComplianceActions
 * @param state
 * @param action
 */
export function generateCertificateReducers(
  state: ICertificateState = cloneDeep(INITIAL_RESULTS_STATE),
  action: GenerateCertificateActions
): ICertificateState {
  switch (action.type) {
    case GenerateCertificateActionTypes.GENERATE_CERTIFICATE: {
      const certificate: ICertificate = action.payload.certificate;
      return {
        ...state,
        loading: true,
        error: undefined,
        results: undefined,
        certificate,
      };
    }
    case GenerateCertificateActionTypes.GENERATE_CERTIFICATE_COMPLETE: {
      return {
        ...state,
        results: action.payload,
        loading: false,
        error: undefined,
      };
    }
    case GenerateCertificateActionTypes.GENERATE_CERTIFICATE_ERROR: {
      return {
        ...state,
        error: action.payload,
        results: undefined,
        loading: false,
      };
    }
    case GenerateCertificateActionTypes.GET_USER_INFORMATION_COMPLETE: {
      return {
        ...state,
        certificate: {
          ...state.certificate,
          userInformation: action.payload,
        },
        loading: false,
        error: undefined,
      };
    }
    case GenerateCertificateActionTypes.GET_USER_INFORMATION_ERROR: {
      return {
        ...state,
        error: action.payload,
        results: undefined,
        loading: false,
      };
    }
    case GenerateCertificateActionTypes.TOGGLE_CERTIFICATE_MODAL: {
      return {
        ...state,
        isModalMessageOpen: action.payload,
      };
    }
    case GenerateCertificateActionTypes.UPDATE_CERTIFICATE_FORM: {
      const {fieldName, value} = action.payload;
      return {
        ...state,
        certificate: {
          ...state.certificate,
          userInformation: {
            ...state.certificate.userInformation,
            [fieldName]: value,
          },
        },
      };
    }
    case GenerateCertificateActionTypes.CLEAR_CERTIFICATE_FORM: {
      return {
        ...state,
        certificate: {
          ...state.certificate,
          userInformation: {
            ...INITIAL_USER_INFO_STATE,
          },
        },
      };
    }
    default: {
      return state;
    }
  }
}
