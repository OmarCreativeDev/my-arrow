import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { switchMap, catchError } from 'rxjs/operators';

import { GenerateCertificateService } from '@app/core/generate-certificate/generate-certificate.service';

import {
  GenerateCertificateActionTypes,
  GenerateCertificate,
  GenerateCertificateComplete,
  GenerateCertificateError,
  GetUserInformation,
  GetUserInformationComplete,
  GetUserInformationError,
  ToggleCertificateModal,
} from '@app/features/trade-compliance/components/generate-certificate/store/generate-certificate.actions';

@Injectable()
export class GenerateCertificateEffects {
  constructor(private actions$: Actions, private generateCertificateService: GenerateCertificateService) {}

  @Effect()
  query$ = this.actions$.pipe(
    ofType(GenerateCertificateActionTypes.GENERATE_CERTIFICATE),
    switchMap((action: GenerateCertificate) => {
      return this.generateCertificateService.generate(action.payload.certificate).pipe(
        switchMap(response => {
          if (response.body) {
            action.payload._windowCb(response.body);
          }
          return [new GenerateCertificateComplete(response), new ToggleCertificateModal(true)];
        }),
        catchError(err => of(new GenerateCertificateError(err)))
      );
    })
  );

  @Effect()
  getUserInformation$ = this.actions$.pipe(
    ofType(GenerateCertificateActionTypes.GET_USER_INFORMATION),
    switchMap((action: GetUserInformation) => {
      return this.generateCertificateService.getUserInformation().pipe(
        switchMap(response => {
          return [new GetUserInformationComplete(response)];
        }),
        catchError(err => of(new GetUserInformationError(err)))
      );
    })
  );
}
