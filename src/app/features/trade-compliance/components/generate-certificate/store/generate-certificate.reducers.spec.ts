import {
  GenerateCertificate,
  GenerateCertificateComplete,
  GenerateCertificateError,
  GetUserInformationComplete,
  GetUserInformationError,
  ToggleCertificateModal,
  UpdateCertificateForm,
  ClearCertificateForm,
} from './generate-certificate.actions';
import { generateCertificateReducers, INITIAL_RESULTS_STATE, INITIAL_USER_INFO_STATE } from './generate-certificate.reducers';
import { IUserInformation, ICertificate, ICertificateState } from '@app/core/generate-certificate/generate-certificate.interface';

describe('Trade Compliance Generate Certificate Reducers', () => {
  let initialState: ICertificateState;

  beforeEach(() => {
    initialState = {
      loading: false,
      error: undefined,
      results: undefined,
      isModalMessageOpen: false,
    };
  });

  it(`GENERATE_CERTIFICATE should change loading to true and not have an error property`, () => {
    const userInformation: IUserInformation = {
      companyName: 'Test Company',
      attentionOf: 'Test',
      address: 'Central Park',
      city: 'Manhattan',
      state: 'NY',
      postalCode: '81389',
      countryCode: 'US',
      email: 'user@example.com',
      taxId: '123',
    };
    const certificate: ICertificate = {
      parts: [''],
      to: '',
      userInformation: userInformation,
      year: 2014,
    };
    const payload = { certificate };
    const action = new GenerateCertificate(payload);
    const result = generateCertificateReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeUndefined();
    expect(result.results).toBeUndefined();
  });

  it(`GENERATE_CERTIFICATE_COMPLETE should set the results of the state, change loading to false and not have an error property`, () => {
    const payload = 'JVBERi0xLjQKJaqrrK0KMSAwIG9iago8PAovQ3JlYXRvciAoQXBh';
    const action = new GenerateCertificateComplete(payload);
    const result = generateCertificateReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.results).toBe(payload);
  });

  it(`GENERATE_CERTIFICATE_ERROR pdf response should be undefined and error property should have an error property`, () => {
    const expectedError = new Error('foo');
    const action = new GenerateCertificateError(expectedError);
    const result = generateCertificateReducers(initialState, action);
    expect(result.error).toEqual(expectedError);
    expect(result.results).toBeUndefined();
  });

  it(`GET_USER_INFORMATION_COMPLETE should set the results of the state, change loading to false and not have an error property`, () => {
    const userInformation: IUserInformation = {
      companyName: 'Test Company',
      attentionOf: 'Test',
      address: 'Central Park',
      city: 'Manhattan',
      state: 'NY',
      postalCode: '81389',
      countryCode: 'US',
      email: 'user@example.com',
      taxId: '123',
    };
    const certificate: ICertificate = {
      parts: [''],
      to: '',
      userInformation: userInformation,
      year: 2014,
    };

    initialState.certificate = certificate;

    const action = new GetUserInformationComplete(userInformation);
    const result = generateCertificateReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.certificate.userInformation).toEqual(userInformation);
  });

  it(`GET_USER_INFORMATION_ERROR user information response should be undefined and error property should have an error property`, () => {
    const expectedError = new Error('foo');
    const action = new GetUserInformationError(expectedError);
    const result = generateCertificateReducers(initialState, action);
    expect(result.error).toEqual(expectedError);
    expect(result.results).toBeUndefined();
  });

  it(`TOGGLE_CERTIFICATE_MODAL should set the results of the state, change isModalMessageOpen to different value and not have an error property`, () => {
    const payload: boolean = true;
    const action = new ToggleCertificateModal(payload);
    const result = generateCertificateReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.isModalMessageOpen).toEqual(payload);
  });

  it(`UPDATE_CERTIFICATE_FORM should set the results of the state, change isModalMessageOpen to different value and not have an error property`, () => {
    const userInformation: IUserInformation = {
      companyName: 'Test Company',
      attentionOf: 'Test',
      address: 'Central Park',
      city: 'Manhattan',
      state: 'NY',
      postalCode: '81389',
      countryCode: 'US',
      email: 'user@example.com',
      taxId: '123',
    };
    const certificate: ICertificate = {
      parts: [''],
      to: '',
      userInformation: userInformation,
      year: 2014,
    };

    initialState.certificate = certificate;

    const payload = {
      fieldName: 'countryCode',
      value: 'CR',
    };
    const action = new UpdateCertificateForm(payload);
    const result = generateCertificateReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.certificate).toBeDefined();
    expect(result.certificate.userInformation).toBeDefined();
    expect(result.certificate.userInformation.countryCode).toEqual(payload.value);
  });

  it(`CLEAR_CERTIFICATE_FORM should set the results of the state, change user information to default values and not have an error property`, () => {
    const action = new ClearCertificateForm();
    const result = generateCertificateReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeUndefined();
    expect(result.certificate.userInformation).toEqual(INITIAL_USER_INFO_STATE);
  });

  it(`by default should return the state without transforming it`, () => {
    const result = generateCertificateReducers(undefined, {} as any);
    expect(result).toEqual(INITIAL_RESULTS_STATE);
  });
});
