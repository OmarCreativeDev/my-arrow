import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ICertificateState } from '@app/core/generate-certificate/generate-certificate.interface';

export const getCertificateState = createFeatureSelector<ICertificateState>('tradeComplianceCertificate');

export const getCertificateSelector = createSelector(
  getCertificateState,
  certificateState => certificateState.certificate
);

export const getIsModalMessageOpenSelector = createSelector(
  getCertificateState,
  certificateState => certificateState.isModalMessageOpen
);

export const getUserInformation = createSelector(
  getCertificateState,
  certificateState => certificateState.certificate.userInformation
);

