import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';

import { TradeComplianceService } from '@app/core/trade-compliance/trade-compliance.service';

@Injectable()
export class TradeComplianceSearchRouteGuard implements CanActivate {
  constructor(private tradeComplianceService: TradeComplianceService, private router: Router) {}

  canActivate() {
    if (this.tradeComplianceService.getData('parts')) {
      return true;
    } else {
      this.router.navigate(['/trade-compliance']);
      return false;
    }
  }
}
