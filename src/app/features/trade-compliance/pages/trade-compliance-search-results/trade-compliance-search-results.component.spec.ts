import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule, combineReducers } from '@ngrx/store';
import { ApiService } from '@app/core/api/api.service';
import { TradeComplianceSearchResultsComponent } from './trade-compliance-search-results.component';
import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { SharedModule } from '@app/shared/shared.module';
import { tradeComplianceSearchReducers } from '../../components/search-results/store/search-results.reducers';
import { SearchPartNumbers } from '../../components/search-results/store/search-results.actions';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { userReducers } from '@app/core/user/store/user.reducers';
import { generateCertificateReducers } from '@app/features/trade-compliance/components/generate-certificate/store/generate-certificate.reducers';
import { StoreMock } from '@app/features/products/pages/product-search/product-search.component.spec';

describe('TradeComplianceSearchResultsComponent', () => {
  let component: TradeComplianceSearchResultsComponent;
  let fixture: ComponentFixture<TradeComplianceSearchResultsComponent>;
  let store: Store<any>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [TradeComplianceSearchResultsComponent],
        imports: [
          RouterTestingModule,
          SharedModule,
          StoreModule.forRoot({
            search: combineReducers(tradeComplianceSearchReducers),
            user: userReducers,
            tradeCompliancePartNumbers: tradeComplianceSearchReducers,
            tradeComplianceCertificate: generateCertificateReducers,
          }),
          HttpClientTestingModule,
        ],
        providers: [ApiService, AuthTokenService, { provide: Store, useClass: StoreMock }],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeComplianceSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should dispatch a SearchPartNumbers event on init`, () => {
    const action = new SearchPartNumbers();
    component.doSearch();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
});
