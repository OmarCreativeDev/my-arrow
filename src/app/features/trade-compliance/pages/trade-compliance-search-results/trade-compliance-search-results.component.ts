import { Component, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { ITradeComplianceSearchResults, ITradeComplianceSearchType } from '@app/core/trade-compliance/trade-compliance.interfaces';
import { getResults, getLoading, getError, getSearchType } from '../../components/search-results/store/search-results.selectors';
import { SearchPartNumbers, ResetSearchResults } from '../../components/search-results/store/search-results.actions';

@Component({
  selector: 'app-trade-compliance-search-results',
  templateUrl: './trade-compliance-search-results.component.html',
  styleUrls: ['./trade-compliance-search-results.component.scss'],
})
export class TradeComplianceSearchResultsComponent implements OnInit {
  public results$: Observable<Array<ITradeComplianceSearchResults>>;
  public loading$: Observable<boolean>;
  public error$: Observable<Error>;
  public searchType$: Observable<ITradeComplianceSearchType>;
  public searchType: ITradeComplianceSearchType;
  public subscription: Subscription = new Subscription();

  constructor(private store: Store<IAppState>) {
    this.results$ = store.pipe(select(getResults));
    this.loading$ = store.pipe(select(getLoading));
    this.error$ = store.pipe(select(getError));
    this.searchType$ = store.pipe(select(getSearchType));
  }

  doSearch() {
    this.store.dispatch(new SearchPartNumbers());
  }

  ngOnInit() {
    this.doSearch();
    this.startSubscriptions();
  }

  public startSubscriptions(): void {
    this.subscription.add(this.subscribeSearchType());
  }

  private subscribeSearchType(): Subscription {
    return this.searchType$.subscribe(searchType => (this.searchType = searchType));
  }

  public resetSearchResults(): void {
    this.store.dispatch(new ResetSearchResults());
  }
}
