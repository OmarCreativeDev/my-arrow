export interface IFileReaderEventTarget extends FileReader {
  result: ArrayBuffer | SharedArrayBuffer;
}

export interface IFileReaderEvent extends FileReaderProgressEvent {
  target: IFileReaderEventTarget;
  getMessage(): ArrayBuffer | SharedArrayBuffer;
}
