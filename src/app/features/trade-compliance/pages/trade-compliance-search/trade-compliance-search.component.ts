import { Component } from '@angular/core';
import * as XLSX from 'ts-xlsx';
import { PapaParseService } from 'ngx-papaparse';
import { IFileReaderEvent } from './file-reader-event.interface';
import { Store } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { ComplianceCheckUpload } from '@app/features/trade-compliance/components/search-results/store/search-results.actions';

@Component({
  selector: 'app-trade-compliance-search',
  templateUrl: './trade-compliance-search.component.html',
  styleUrls: ['./trade-compliance-search.component.scss'],
})
export class TradeComplianceSearchComponent {
  private _acceptedFileExtensions: Array<string> = ['.csv', '.xls', '.xlsx'];
  private INVALID_TYPE = 'invalid-type';

  showFileExtensionError: boolean = false;
  showFileEmptyError: boolean = false;
  showCorruptFileError: boolean = false;
  showUnreadableFileError: boolean = false;
  showCSVParsingError: boolean = false;

  parsedValues: string = '';

  get acceptedFileExtensions(): any {
    return this._acceptedFileExtensions.join(',');
  }

  // This polyfills the readAsBinaryString for IE
  IECheck(): void {
    if (FileReader.prototype.readAsBinaryString === undefined) {
      FileReader.prototype.readAsBinaryString = function(fileData) {
        const pt = this;
        const reader = new FileReader();
        reader.onload = function(e) {
          const blobURL = URL.createObjectURL(fileData);
          const xhr = new XMLHttpRequest();
          xhr.open('get', blobURL);
          xhr.overrideMimeType('text/plain; charset=x-user-defined');
          xhr.onload = function() {
            const g = { target: { result: xhr.response } };
            pt.onload(g);
          };
          xhr.send();
        };
        reader.readAsArrayBuffer(fileData);
      };
    }
  }

  constructor(private Papa: PapaParseService, private store: Store<IAppState>) {
    this.IECheck();
  }

  getFileExtensionFromMimeType(fileType: string): string {
    let fileExtension = this.INVALID_TYPE;
    switch (fileType) {
      case 'application/vnd.ms-excel':
        fileExtension = 'xls';
        break;
      case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
        fileExtension = 'xlsx';
        break;
      case 'text/csv':
        fileExtension = 'csv';
        break;
    }
    return fileExtension;
  }

  getFileExtensionFromName(fileName: string): string {
    const fileExtension = fileName
      .split('.')
      .pop()
      .toLowerCase();
    return this.acceptedFileExtensions.includes('.' + fileExtension) ? fileExtension : this.INVALID_TYPE;
  }

  getFileType(file: File) {
    let type = '';
    if (file.type) {
      type = this.getFileExtensionFromMimeType(file.type);
    } else {
      type = this.getFileExtensionFromName(file.name);
    }
    return type;
  }

  validateFile(file: File) {
    const that = this;
    const fileReader = new FileReader();
    const fileTypeByExtension = that.getFileType(file);
    if (fileTypeByExtension === that.INVALID_TYPE) {
      that.showFileExtensionError = true;
    } else {
      fileReader.onloadend = (e: IFileReaderEvent) => {
        const arr = new Uint8Array(e.target.result).subarray(0, 4);
        let header = '';
        for (let i = 0; i < arr.length; i++) {
          header += arr[i].toString(16);
        }
        switch (header) {
          case 'da': // Empty Magic Number
            that.showFileEmptyError = true;
            break;
          // CSV Magic Numbers
          case '22617364':
          case '31302d35':
          case '4d493130':
          case '50563137':
          case '50563138':
          case 'efbbbf50':
          case 'efbbbf53':
            that.parseCSV(file);
            break;
          // XLS Magic Number
          case 'd0cf11e0':
          case '504b34':
            that.parseXLS(file);
            break;
          default:
            that.showCorruptFileError = true;
            break;
        }
      };
      fileReader.readAsArrayBuffer(file);
    }
  }

  validateParsingResult(result: Array<any>): boolean {
    let isValidResult = true;
    result.forEach(element => {
      if (element.length !== 1 && typeof element[0] !== 'string') {
        isValidResult = false;
      }
    });
    return isValidResult;
  }

  writeParsedValues(values: Array<any>) {
    this.parsedValues = values.join(',').replace(/(^,)|(,$)/g, '');
  }

  parseCSV(file: File) {
    const options = {
      complete: results => {
        if (results && results.data) {
          const isValidResult = this.validateParsingResult(results.data);
          if (isValidResult) {
            this.writeParsedValues(results.data);
          } else {
            this.showUnreadableFileError = true;
          }
        }
      },
      error: error => {
        this.showCSVParsingError = true;
        throw new Error(error);
      },
    };

    this.Papa.parse(file, options);
  }

  parseXLS(file: File) {
    const fileReader: FileReader = new FileReader();
    fileReader.onload = (e: any) => {
      // pre-process data
      let binary = '';
      const bytes = new Uint8Array(e.target.result);
      const length = bytes.byteLength;
      for (let i = 0; i < length; i++) {
        binary += String.fromCharCode(bytes[i]);
      }
      // call 'xlsx' to read the file
      const oFile = XLSX.read(binary, { type: 'binary', cellDates: true, cellStyles: true });
      const sheetName = oFile.SheetNames[0];
      const ws = oFile.Sheets[sheetName];
      const result = <any>XLSX.utils.sheet_to_json(ws, { header: 1 });
      const isValidResult = this.validateParsingResult(result);
      if (isValidResult) {
        this.writeParsedValues(result);
      } else {
        this.showUnreadableFileError = true;
      }
    };
    fileReader.readAsArrayBuffer(file);
  }

  handleUpload(file: File) {
    this.showFileExtensionError = false;
    this.showFileEmptyError = false;
    this.showCorruptFileError = false;
    this.showUnreadableFileError = false;
    this.showCSVParsingError = false;
    this.parsedValues = '';

    this.store.dispatch(new ComplianceCheckUpload(this.getFileType(file)));
    this.validateFile(file);
  }
}
