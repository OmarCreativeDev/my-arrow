import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { PapaParseService } from 'ngx-papaparse';
import { RouterTestingModule } from '@angular/router/testing';

import { TradeComplianceSearchComponent } from './trade-compliance-search.component';
import { SearchFormComponent } from '../../components/search-form/search-form.component';
import { TradeComplianceService } from '../../../../core/trade-compliance/trade-compliance.service';
import { ApiService } from '../../../../core/api/api.service';

import { SharedModule } from '@app/shared/shared.module';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { BackdropServiceMock } from '@app/app.component.spec';
import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { userReducers } from '@app/core/user/store/user.reducers';
import { tradeComplianceSearchReducers } from '@app/features/trade-compliance/components/search-results/store/search-results.reducers';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import { generateCertificateReducers } from '@app/features/trade-compliance/components/generate-certificate/store/generate-certificate.reducers';
import { StoreMock } from '@app/features/products/pages/product-search/product-search.component.spec';

describe('TradeComplianceSearchComponent', () => {
  let component: TradeComplianceSearchComponent;
  let fixture: ComponentFixture<TradeComplianceSearchComponent>;
  let Papa;

  function getMockFile({ filename, mimeType }) {
    const name = filename || 'file.csv';

    function range() {
      return `50 56 31 38`;
    }

    const blob = new Blob([range()], { type: mimeType });
    blob['lastModifiedDate'] = new Date();
    blob['name'] = name;

    return <File>blob;
  }

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [TradeComplianceSearchComponent, SearchFormComponent],
        providers: [
          { provide: BackdropService, useClass: BackdropServiceMock },
          { provide: Store, useClass: StoreMock },
          PapaParseService,
          TradeComplianceService,
          ApiService,
          AuthTokenService,
        ],
        imports: [
          SharedModule,
          ReactiveFormsModule,
          RouterTestingModule,
          HttpClientTestingModule,
          StoreModule.forRoot({
            search: combineReducers(tradeComplianceSearchReducers),
            user: userReducers,
            tradeCompliancePartNumbers: tradeComplianceSearchReducers,
            tradeComplianceCertificate: generateCertificateReducers,
          }),
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();
      Papa = TestBed.get(PapaParseService);
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(TradeComplianceSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should validate file when a file is received', () => {
    spyOn(component, 'validateFile');
    const file = getMockFile({ filename: 'file.csv', mimeType: 'text/csv' });
    component.handleUpload(file);
    expect(component.validateFile).toHaveBeenCalledWith(file);
  });

  it('should call getFileType when validating file', () => {
    spyOn(component, 'getFileType');
    const file = getMockFile({ filename: 'file.csv', mimeType: 'text/csv' });
    component.validateFile(file);
    expect(component.getFileType).toHaveBeenCalledWith(file);
  });

  it('should call `getFileExtensionFromMimeType` when a file type is detected', () => {
    spyOn(component, 'getFileExtensionFromMimeType');
    const file = getMockFile({ filename: 'file.csv', mimeType: 'text/csv' });
    component.getFileType(file);
    expect(component.getFileExtensionFromMimeType).toHaveBeenCalledWith(file.type);
  });

  it('should call `getFileExtensionFromName` when a file type is not detected', () => {
    spyOn(component, 'getFileExtensionFromName');
    const file = getMockFile({ filename: 'file.csv', mimeType: '' });
    component.getFileType(file);
    expect(component.getFileExtensionFromName).toHaveBeenCalledWith(file.name);
  });

  it('should get correct file type when calling getFileType', () => {
    const csvFile = getMockFile({ filename: 'file.csv', mimeType: 'text/csv' });
    expect(component.getFileType(csvFile)).toBe('csv');
    const xlsFile = getMockFile({
      filename: 'file.xls',
      mimeType: 'application/vnd.ms-excel',
    });
    expect(component.getFileType(xlsFile)).toBe('xls');
    const jpgFile = getMockFile({
      filename: 'file.jpg',
      mimeType: 'image/jpeg',
    });
    expect(component.getFileType(jpgFile)).toBe('invalid-type');
  });

  it('should show error message for invalid file types', () => {
    expect(component.showFileExtensionError).toBeFalsy();
    const jpgFile = getMockFile({
      filename: 'file.jpg',
      mimeType: 'image/jpeg',
    });
    component.validateFile(jpgFile);
    expect(component.showFileExtensionError).toBeTruthy();
  });

  it('should validate parsing result accordingly', () => {
    const fakeValidResult = [['PV18-10RB-3K'], ['MID400']];
    const fakeInvalidResult = [['PV18-10RB-3K'], [['MID400'], ['MID402']]];
    expect(component.validateParsingResult(fakeValidResult)).toBeTruthy();
    expect(component.validateParsingResult(fakeInvalidResult)).toBeFalsy();
  });

  it('should updated the parsed values property when passing a result', () => {
    const fakeValidResult = [['PV18-10RB-3K'], ['MID400']];
    expect(component.parsedValues).toBe('');
    component.writeParsedValues(fakeValidResult);
    expect(component.parsedValues).toBe('PV18-10RB-3K,MID400');
  });

  it('should call `Papa.parse` when calling `parseCSV`', () => {
    spyOn(Papa, 'parse');
    const file = getMockFile({ filename: 'file.csv', mimeType: 'text/csv' });
    component.parseCSV(file);
    expect(Papa.parse).toHaveBeenCalled();
  });

  it('should call `parseXLS`', () => {
    const file = getMockFile({ filename: 'file.xls', mimeType: 'application/vnd.ms-excel' });
    component.parseXLS(file);
  });
});
