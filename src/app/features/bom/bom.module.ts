import { NgModule } from '@angular/core';
import { CoreModule } from '@arrow/bom/core';
import { RouterModule } from '@angular/router';

import { environment } from '@env/environment';
import { BomHooks } from '@app/features/bom/bom.hooks';
import { BomRoutingModule } from './bom-routing.module';

@NgModule({
  imports: [RouterModule, CoreModule.forRoot(environment.bomConfig, BomHooks), BomRoutingModule],
  declarations: [],
  providers: [],
})
export class BomModule {}
