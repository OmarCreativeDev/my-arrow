import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { filter, first, mergeMap } from 'rxjs/operators';
import { IAppState } from '@app/shared/shared.interfaces';
import { getPrivateFeatureFlagsSelector } from '../properties/store/properties.selectors';
import { isEmpty } from 'lodash-es';

@Injectable()
export class BomGuard {
  constructor(private store: Store<IAppState>) {}

  public canActivate(): Observable<boolean> {
    return this._getPrivateFeatureFlags().pipe(
      mergeMap(() => {
        return of(true);
      })
    );
  }

  public _getPrivateFeatureFlags(): any {
    return this.store.pipe(
      select(getPrivateFeatureFlagsSelector),
      filter(flags => !isEmpty(flags)),
      first()
    );
  }
}
