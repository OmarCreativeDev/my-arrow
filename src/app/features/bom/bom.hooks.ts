import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { filter } from 'rxjs/operators';

import { IAppState } from '@app/shared/shared.interfaces';
import { RefreshQuoteCart } from '@app/features/quotes/stores/quote-cart.actions';
import { getCurrencyCode } from '@app/core/user/store/user.selectors';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';
import { isEmpty } from 'lodash';

@Injectable()
export class BomHooks {
  constructor(private store: Store<IAppState>) {}

  public refresh() {
    this.store.dispatch(new RefreshQuoteCart());
  }

  public getCurrencyObservable(): Observable<string | null> {
    return this.store.pipe(select(getCurrencyCode));
  }

  public getFeatureFlags(): Observable<object | null> {
    return this.store.pipe(
      select(getPrivateFeatureFlagsSelector),
      filter(flags => {
        return !isEmpty(flags);
      })
    );
  }
}
