import { NgModule } from '@angular/core';
import { RouterModule, Route } from '@angular/router';

import { ManagementModule } from '@arrow/bom/management';
import { MappingModule } from '@arrow/bom/mapping';
import { ViewModule } from '@arrow/bom/view';

import { ManagementComponent } from '@arrow/bom/management';
import { MappingComponent } from '@arrow/bom/mapping';
import { ViewComponent } from '@arrow/bom/view';
import { FeatureFlagRoutingModule } from '@arrow/bom/feature-flag';
import { MaintenanceModule } from '@arrow/bom/maintenance';

export const routes: Route[] = [
  { path: '', component: ManagementComponent },
  { path: 'view/:id/:page', component: ViewComponent },
  { path: 'mapping', component: MappingComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes), ManagementModule, ViewModule, MappingModule, FeatureFlagRoutingModule, MaintenanceModule],
})
export class BomRoutingModule {}
