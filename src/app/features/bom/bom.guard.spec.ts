import { TestBed } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';
import { CoreModule } from '../../core/core.module';
import { userReducers } from '@app/core/user/store/user.reducers';
import { of } from 'rxjs';
import { BomGuard } from './bom.guard';

describe('BomGuard', () => {
  let bomGuard: BomGuard;
  let store: Store<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({ user: userReducers }), CoreModule],
      providers: [BomGuard],
    });
    bomGuard = TestBed.get(BomGuard);

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should be created', () => {
    expect(bomGuard).toBeTruthy();
  });

  it('should activate route if feature flags object is not empty', () => {
    spyOn(bomGuard, '_getPrivateFeatureFlags').and.returnValue(of({ testFlag: true }));
    expect(bomGuard.canActivate()).toBeTruthy();
  });
});
