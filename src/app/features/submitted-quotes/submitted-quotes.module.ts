import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { quotesAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.quotes';
import { quotesDetailsAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.quotes-details';
import { QuotesService } from '@app/core/quotes/quotes.service';
import { QuotesEffects } from '@app/features/submitted-quotes/stores/quotes/quotes.effects';
import { QuoteDetailsEffects } from '@app/features/submitted-quotes/stores/quote-details/quote-details.effects';
import { SharedModule } from '@app/shared/shared.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { SubmittedQuotesListingComponent } from './components/submitted-quotes-listing/submitted-quotes-listing.component';
import { SubmittedQuoteDetailsComponent } from './pages/submitted-quote-details/submitted-quote-details.component';
import { SubmittedQuotesComponent } from './pages/submitted-quotes/submitted-quotes.component';
import { SubmittedQuotesRoutingModule } from './submitted-quotes-routing.module';
import { SubmittedQuoteDetailsListingComponent } from './components/submitted-quote-details-listing/submitted-quote-details-listing.component';
import { AddToCartDialogComponent } from './components/add-to-cart-dialog/add-to-cart-dialog.component';
import { DialogService } from '@app/core/dialog/dialog.service';
import { NotesDialogComponent } from './components/notes-dialog/notes-dialog.component';
import { DownloadQuoteComponent } from './components/download-quote/download-quote.component';

@NgModule({
  imports: [
    CommonModule,
    SubmittedQuotesRoutingModule,
    EffectsModule.forFeature([QuotesEffects]),
    EffectsModule.forFeature([QuoteDetailsEffects]),
    StoreModule.forFeature('quotes', quotesAnalyticsMetaReducers),
    StoreModule.forFeature('quoteDetails', quotesDetailsAnalyticsMetaReducers),
    SharedModule,
    ReactiveFormsModule,
  ],
  declarations: [
    DownloadQuoteComponent,
    SubmittedQuotesComponent,
    SubmittedQuotesListingComponent,
    SubmittedQuoteDetailsComponent,
    SubmittedQuoteDetailsListingComponent,
    AddToCartDialogComponent,
    NotesDialogComponent,
  ],
  providers: [QuotesService, DialogService],
  entryComponents: [AddToCartDialogComponent, DownloadQuoteComponent, NotesDialogComponent],
})
export class SubmittedQuotesModule {}
