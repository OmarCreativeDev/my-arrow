import { DatePipe } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule } from '@ngrx/store';
import { SubmittedQuotesListingComponent } from './submitted-quotes-listing.component';
import { IsoDatePipe } from '@app/shared/pipes/iso-date/iso-date.pipe';
import { DateService } from '@app/shared/services/date.service';
import { ChangeSort } from '@app/features/submitted-quotes/stores/quotes/quotes.actions';
import { ISortCriteronOrderEnum } from '@app/shared/shared.interfaces';
import { quotesReducer } from '../../stores/quotes/quotes.reducers';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { QuoteStatus } from '@app/core/quotes/quotes.enum';

export const mockQuote = {
  itemsQuoted: 0,
  totalItems: 25,
  submittedDate: '2018-11-25',
  expiryDate: '2018-12-25',
  referenceNumber: 'Quote Factory',
  quoteNumber: '21811474',
  quoteHeaderId: 2815416,
  owner: null,
  status: 'Processing',
  externalComments: 'TESTING COMMENT',
};

describe('SubmittedQuotesListingComponent', () => {
  let store: Store<any>;
  let component: SubmittedQuotesListingComponent;
  let fixture: ComponentFixture<SubmittedQuotesListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, StoreModule.forRoot({ quotes: quotesReducer })],
      declarations: [SubmittedQuotesListingComponent, IsoDatePipe],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [DatePipe, DateService, { provide: DialogService, useClass: DialogServiceMock }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittedQuotesListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch `ChangeSort` to the store', () => {
    const sortObject = [
      {
        key: 'submittedDate',
        order: ISortCriteronOrderEnum.DESC,
      },
    ];

    spyOn(store, 'dispatch');
    component.sortChange(sortObject);
    expect(store.dispatch).toHaveBeenCalledWith(new ChangeSort(sortObject));
  });

  describe('#isLineItemProcessing()', () => {
    const testCases = [
      {
        data: QuoteStatus.CUSTOMER_ACCEPTED,
        expected: false,
      },
      {
        data: QuoteStatus.PROCESSING,
        expected: true,
      },
    ];

    for (const testCase of testCases) {
      it(`should return ${testCase.expected} when quote status is '${testCase.data}'`, () => {
        const result = component.isLineItemProcessing(testCase.data);
        expect(result).toEqual(testCase.expected);
      });
    }
  });
});
