import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { ISortCriteron, ISortCriteronOrderEnum } from '@app/shared/shared.interfaces';
import { IAppState } from '@app/shared/shared.interfaces';
import { getSort } from '@app/features/submitted-quotes/stores/quotes/quotes.selectors';
import { ChangeSort } from '@app/features/submitted-quotes/stores/quotes/quotes.actions';
import { QuoteStatus } from '@app/core/quotes/quotes.enum';

@Component({
  selector: 'app-submitted-quotes-listing',
  templateUrl: './submitted-quotes-listing.component.html',
  styleUrls: ['./submitted-quotes-listing.component.scss'],
})
export class SubmittedQuotesListingComponent implements OnInit, OnDestroy {
  @Input()
  quotes;
  @Input()
  featureFlags;

  public sort$: Observable<Array<ISortCriteron>>;
  public sortOrder: Map<string, ISortCriteronOrderEnum>;
  public privateFeatures$: Observable<any>;
  public subscription: Subscription = new Subscription();

  constructor(private store: Store<IAppState>) {
    this.sort$ = store.pipe(select(getSort));
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public startSubscriptions() {
    this.subscription.add(this.subscribeSort());
  }

  private subscribeSort(): Subscription {
    /* istanbul ignore next */
    return this.sort$.subscribe(updatedSort => {
      this.sortOrder = new Map();
      if (updatedSort.length) {
        updatedSort.forEach(updatedSortItem => {
          this.sortOrder.set(updatedSortItem.key, updatedSortItem.order);
        });
      }
    });
  }

  public sortChange(sortCriteria?: Array<ISortCriteron>): void {
    this.store.dispatch(new ChangeSort(sortCriteria));
  }

  public isLineItemProcessing(lineItemStatus: QuoteStatus): boolean {
    return lineItemStatus === QuoteStatus.PROCESSING;
  }
}
