import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';

import { DownloadQuoteDialogData, DownloadQuoteComponent } from './download-quote.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { SharedModule } from '@app/shared/shared.module';
import { QuotesService } from '@app/core/quotes/quotes.service';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { of, throwError } from 'rxjs';
import * as FileSaver from 'file-saver';
import { IFileTypeEnum, ISortableItem } from '@app/shared/shared.interfaces';
import { HttpHeaders } from '@angular/common/http';
import { userReducers } from '@app/core/user/store/user.reducers';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import { SendAnalyticsCustomEvent } from '@app/core/analytics/stores/analytics.actions';
import { EventType, EventCategory, EventAction } from '@app/core/analytics/analytics.enums';

class MockQuotesService {
  public downloadQuote() {}
}

describe('DownloadQuoteComponent', () => {
  let component: DownloadQuoteComponent;
  let fixture: ComponentFixture<DownloadQuoteComponent>;
  let quotesService: QuotesService;
  let dialog: Dialog<DownloadQuoteComponent>;
  let store: Store<any>;

  const mockColumns: ISortableItem[] = [
    {
      id: 0,
      label: 'Part Number',
      properties: ['partNumber'],
    },
    {
      id: 1,
      label: 'Manufacturer',
      properties: ['manufacturer'],
    },
  ];

  const quoteHeaderId = 234567890;
  const quoteNumber = '0000234567890';
  const data: DownloadQuoteDialogData = { quoteHeaderId, quoteNumber };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DownloadQuoteComponent],
      imports: [
        SharedModule,
        HttpClientTestingModule,
        StoreModule.forRoot({
          orders: combineReducers(userReducers),
        }),
      ],
      providers: [
        { provide: QuotesService, useClass: MockQuotesService },
        { provide: Dialog, useClass: DialogMock },
        { provide: APP_DIALOG_DATA, useValue: data },
      ],
    }).compileComponents();
    quotesService = TestBed.get(QuotesService);
    dialog = TestBed.get(Dialog);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadQuoteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the dialog when Cancel button is pressed', () => {
    spyOn(dialog, 'close');
    component.onClose();
    expect(dialog.close).toHaveBeenCalled();
  });

  describe('#canDownload', () => {
    const testCases = [
      {
        description: 'should return true if there are selectedColumns',
        selectedColumns: ['1', '2'],
        result: true,
      },
      {
        description: 'should return false if there are no selectedColumns',
        selectedColumns: [],
        result: false,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        component.selectedColumns = testCase.selectedColumns;
        const result = component.canDownload();
        expect(result).toEqual(testCase.result);
      });
    }
  });

  describe('#onDownload', () => {
    beforeEach(() => {
      spyOn(FileSaver, 'saveAs').and.callFake(() => {});
      spyOn(component, 'saveResponse').and.callThrough();
    });

    it('should call quotesService with quoteHeaderId, fileType and selectedColumns and invoke #saveResponse when successful', inject(
      [APP_DIALOG_DATA],
      (dialogData: { quoteHeaderId }) => {
        spyOn(quotesService, 'downloadQuote').and.callFake(() => of([]));
        const selectedFileType = IFileTypeEnum.XLSX;

        component.selectedFileType = selectedFileType;
        component.selectedColumns = mockColumns;
        component.onDownload();

        expect(quotesService.downloadQuote).toHaveBeenCalledWith({
          quoteHeaderId: dialogData.quoteHeaderId,
          fileType: component.selectedFileType,
          columns: mockColumns.map((column: ISortableItem) => {
            return column.properties[0];
          }),
        });
        expect(component.saveResponse).toHaveBeenCalled();
      }
    ));

    it('should dispatch a proper event to be tracked onDownload()', inject(
      [APP_DIALOG_DATA],
      (dialogData: { quoteHeaderId; quoteNumber }) => {
        spyOn(quotesService, 'downloadQuote').and.callFake(() => of([]));
        const selectedFileType = IFileTypeEnum.XLSX;

        component.selectedFileType = selectedFileType;
        component.selectedColumns = mockColumns;
        component.onDownload();

        expect(store.dispatch).toHaveBeenCalledWith(
          new SendAnalyticsCustomEvent({
            event: EventType.EVENT,
            category: EventCategory.QUOTE,
            action: EventAction.QUOTE_DOWNLOAD,
            label: dialogData.quoteNumber,
          })
        );
      }
    ));

    it('should invoke handleDownloadError if there is an error with the api', () => {
      spyOn(quotesService, 'downloadQuote').and.callFake(() => throwError('Error'));
      spyOn(component, 'handleError').and.callThrough();
      component.onDownload();
      expect(component.handleError).toHaveBeenCalled();
    });

    describe('#getFilenameFromHeaders', () => {
      it('should return the value of the filename in the Content-Disposition header', () => {
        const filename = 'dummy-filename.csv';
        const headers = new HttpHeaders({
          'Content-Disposition': `attachment; filename=${filename}`,
        });
        const result = component.getFilenameFromHeaders(headers);
        expect(result).toEqual(filename);
      });

      it('should return an empty string if no filename was found', () => {
        const headers = new HttpHeaders({
          'Content-Disposition': 'attachment',
        });
        const result = component.getFilenameFromHeaders(headers);
        expect(result).toEqual('');
      });

      it(`should return an empty string if no 'Content-Disposition' header was found`, () => {
        const headers = new HttpHeaders({});
        const result = component.getFilenameFromHeaders(headers);
        expect(result).toEqual('');
      });
    });
  });
});
