import { Component, Inject } from '@angular/core';
import { IDownloadColumnTypeEnum, IFileTypeEnum, IFileTypeOption, ISortableItem, IAppState } from '@app/shared/shared.interfaces';
import { saveAs } from 'file-saver';
import { HttpHeaders, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { QuotesService } from '@app/core/quotes/quotes.service';
import { Store } from '@ngrx/store';
import { SendAnalyticsCustomEvent } from '@app/core/analytics/stores/analytics.actions';
import { EventType, EventCategory, EventAction } from '@app/core/analytics/analytics.enums';

export interface DownloadQuoteDialogData {
  quoteHeaderId: number;
  quoteNumber: string;
}

@Component({
  selector: 'app-download-quote',
  templateUrl: './download-quote.component.html',
  styleUrls: ['./download-quote.component.scss'],
})
export class DownloadQuoteComponent {
  public fileTypeOptions: Array<IFileTypeOption> = [
    {
      label: '.xls',
      value: IFileTypeEnum.XLS,
    },
    {
      label: '.xlsx',
      value: IFileTypeEnum.XLSX,
    },
    {
      label: '.csv',
      value: IFileTypeEnum.CSV,
    },
  ];

  public selectedFileType = this.fileTypeOptions[0].value;

  // TODO: `label` strings should be localizable
  public columns: Array<ISortableItem> = [
    {
      id: 0,
      label: 'Quote Line Number',
      properties: ['quoteLineNumber'],
    },
    {
      id: 1,
      label: 'Part Number',
      properties: ['partNumber'],
    },
    {
      id: 2,
      label: 'Manufacturer',
      properties: ['manufacturer'],
    },
    {
      id: 3,
      label: 'Customer Part Number',
      properties: ['cpn'],
    },
    {
      id: 4,
      label: 'Inventory',
      properties: ['availability'],
    },
    {
      id: 5,
      label: 'Quantity',
      properties: ['quantity'],
      type: IDownloadColumnTypeEnum.String,
    },
    {
      id: 6,
      label: 'Target Price',
      properties: ['targetPrice'],
    },
    {
      id: 7,
      label: 'Quoted Price',
      properties: ['quotePrice'],
    },
    {
      id: 8,
      label: 'Total Price',
      properties: ['totalPrice'],
    },
    {
      id: 9,
      label: 'Lead Time',
      properties: ['leadTime'],
    },
    {
      id: 10,
      label: 'Status',
      properties: ['status'],
    },
  ];
  public selectedColumns = [];
  public downloadProcessing = false;
  public downloadError = undefined;

  constructor(
    private store: Store<IAppState>,
    private quoteService: QuotesService,
    private dialog: Dialog<DownloadQuoteComponent>,
    @Inject(APP_DIALOG_DATA) private data: DownloadQuoteDialogData
  ) {}

  public onClose(): void {
    if (!this.downloadProcessing) {
      this.dialog.close();
    }
  }

  private getSelectedColumns(): string[] {
    return this.selectedColumns.map((column: ISortableItem) => {
      return column.properties[0];
    });
  }
  public onDownload(): void {
    this.downloadProcessing = true;
    this.quoteService
      .downloadQuote({
        quoteHeaderId: this.data.quoteHeaderId,
        fileType: this.selectedFileType,
        columns: this.getSelectedColumns(),
      })
      .subscribe((response: HttpResponse<Blob>) => this.saveResponse(response), (err: HttpErrorResponse) => this.handleError(err));
  }

  public saveResponse(response: HttpResponse<Blob>): void {
    this.downloadProcessing = false;
    const filename = this.getFilenameFromHeaders(response.headers);
    this.store.dispatch(
      new SendAnalyticsCustomEvent({
        event: EventType.EVENT,
        category: EventCategory.QUOTE,
        action: EventAction.QUOTE_DOWNLOAD,
        label: this.data.quoteNumber,
      })
    );
    saveAs(response.body, filename);
    this.onClose();
  }

  public getFilenameFromHeaders(headers: HttpHeaders) {
    // From https://shekhargulati.com/2017/07/16/implementing-file-save-functionality-with-angular-4/
    const contentDisposition = {};
    if (headers) {
      const contentDispositionHeader: string = headers.get('Content-Disposition');
      if (contentDispositionHeader) {
        const contentDispositionParts: Array<string> = contentDispositionHeader.split(';');
        for (const contentDispositionPart of contentDispositionParts) {
          const keyValuePair = contentDispositionPart.split('=');
          const key = keyValuePair[0].trim();
          const value = (keyValuePair[1] || '').trim();
          contentDisposition[key] = value;
        }
      }
    }
    return contentDisposition['filename'] || '';
  }

  public handleError(err: HttpErrorResponse): void {
    this.downloadError = HttpErrorResponse;
    this.downloadProcessing = false;
  }

  public canDownload(): boolean {
    return this.selectedColumns.length > 0;
  }
}
