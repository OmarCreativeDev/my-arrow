import { Component, OnInit } from '@angular/core';

import { Dialog } from '@app/core/dialog/dialog.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { getCartMaxLineItems } from '@app/features/cart/stores/cart/cart.selectors';

@Component({
  selector: 'app-add-to-cart-dialog',
  templateUrl: './add-to-cart-dialog.component.html',
  styleUrls: ['./add-to-cart-dialog.component.scss'],
})
export class AddToCartDialogComponent implements OnInit {
  public cartMaxLineItems$: Observable<number>;

  constructor(public dialogService: Dialog<AddToCartDialogComponent>, public router: Router, public store: Store<IAppState>) {
    this.cartMaxLineItems$ = this.store.pipe(select(getCartMaxLineItems));
  }

  ngOnInit() {}

  public onDismiss(): void {
    this.dialogService.close();
  }

  public viewCart(): void {
    this.router.navigate(['/cart']);
    this.dialogService.close();
  }
}
