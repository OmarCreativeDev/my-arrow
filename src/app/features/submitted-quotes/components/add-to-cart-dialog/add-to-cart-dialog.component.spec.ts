import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToCartDialogComponent } from './add-to-cart-dialog.component';
import { Dialog } from '@app/core/dialog/dialog.service';
import { SharedModule } from '@app/shared/shared.module';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { Router } from '@angular/router';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { combineReducers, StoreModule } from '@ngrx/store';
import { cartReducers } from '@app/features/cart/stores/cart/cart.reducers';
import { RouterTestingModule } from '@angular/router/testing';

class DummyComponent {}

describe('AddToCartDialogComponent', () => {
  let component: AddToCartDialogComponent;
  let fixture: ComponentFixture<AddToCartDialogComponent>;
  let router: Router;
  const routes = [{ path: 'cart', component: DummyComponent }];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddToCartDialogComponent],
      providers: [{ provide: Dialog, useClass: DialogMock }],
      imports: [
        RouterTestingModule.withRoutes(routes),
        SharedModule,
        HttpClientTestingModule,
        StoreModule.forRoot({
          cart: combineReducers(cartReducers),
        }),
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToCartDialogComponent);
    component = fixture.componentInstance;
    router = TestBed.get(Router);
    spyOn(router, 'navigateByUrl').and.callThrough();
    component.ngOnInit();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('onDismiss should call the close method of dialog', () => {
    spyOn(component.dialogService, 'close');
    component.onDismiss();
    expect(component.dialogService.close).toHaveBeenCalled();
  });

  it('viewCart should navigate to cart and call the close method of dialog', () => {
    spyOn(component.dialogService, 'close');
    spyOn(component.router, 'navigate');
    component.viewCart();
    expect(component.router.navigate).toHaveBeenCalledWith(['/cart']);
    expect(component.dialogService.close).toHaveBeenCalled();
  });
});
