import { Component, OnInit, Inject, AfterViewInit } from '@angular/core';
import { Dialog, APP_DIALOG_DATA } from '@app/core/dialog/dialog.service';
import { INotesDialogData } from './notes-dialog.interface';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store } from '@ngrx/store';
import { SendAnalyticsCustomEvent } from '@app/core/analytics/stores/analytics.actions';
import { EventType, EventCategory, EventAction } from '@app/core/analytics/analytics.enums';

@Component({
  selector: 'app-notes-dialog',
  templateUrl: './notes-dialog.component.html',
  styleUrls: ['./notes-dialog.component.scss'],
})
export class NotesDialogComponent implements OnInit, AfterViewInit {
  public content: INotesDialogData;

  constructor(
    @Inject(APP_DIALOG_DATA) public data: INotesDialogData,
    private store: Store<IAppState>,
    public dialogService: Dialog<NotesDialogComponent>
  ) {}

  ngOnInit() {
    this.content = this.data;
  }

  public onDismiss(): void {
    this.dialogService.close();
  }

  ngAfterViewInit(): void {
    this.store.dispatch(
      new SendAnalyticsCustomEvent({
        event: EventType.EVENT,
        category: EventCategory.MODAL,
        action: EventAction.VIEW_QUOTES,
        label: this.data.label,
      })
    );
  }
}
