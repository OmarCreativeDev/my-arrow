import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NotesDialogComponent } from './notes-dialog.component';
import { APP_DIALOG_DATA, Dialog, DialogService } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import { NoteDialogType } from './notes-dialog.interface';
import { StoreModule, Store } from '@ngrx/store';
import { googleAnalyticsMetaReducers } from '@app/core/analytics/meta-reducers/analytics.custom-events';
import { OverlayContainer } from '@app/core/dialog/overlay-container';
import { IAppState } from '@app/shared/shared.interfaces';
import { EventType, EventCategory, EventAction } from '@app/core/analytics/analytics.enums';
import { SendAnalyticsCustomEvent } from '@app/core/analytics/stores/analytics.actions';

@Component({
  template: `
    <header>THIS IS A HEADER</header>
  `,
})
class DummyComponent {
  constructor(public dialogService: DialogService) {}
  public openDialog() {
    this.dialogService.open(NotesDialogComponent, {
      size: 'medium',
      data: {
        type: NoteDialogType.QUOTE_LINE_ITEM,
        title: 'TEST',
        subtitle: 'TEST',
        note: 'TEST',
      },
    });
  }
}

describe('NotesDialogComponent', () => {
  let dummyComponent: DummyComponent;
  let dummyFixture: ComponentFixture<DummyComponent>;

  let store: Store<IAppState>;

  let dialogService: DialogService;

  let component: NotesDialogComponent;
  let fixture: ComponentFixture<NotesDialogComponent>;
  let dialog: Dialog<NotesDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DummyComponent, NotesDialogComponent],
      imports: [StoreModule.forRoot({ googleAnalytics: googleAnalyticsMetaReducers })],
      providers: [
        DialogService,
        OverlayContainer,
        { provide: Dialog, useClass: DialogMock },
        {
          provide: APP_DIALOG_DATA,
          useValue: { label: NoteDialogType.QUOTE_HEADER, title: 'TESTING', subtitle: 'TEST SUBTITLE', note: 'TEST NOTE' },
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  describe('inside a Component', () => {
    beforeEach(() => {
      dummyFixture = TestBed.createComponent(DummyComponent);
      dummyComponent = dummyFixture.componentInstance;

      dialogService = TestBed.get(DialogService);
      dialog = TestBed.get(Dialog);

      dummyFixture.detectChanges();

      spyOn(dialogService, 'open');
      dummyComponent.openDialog();
    });

    it('should create a base Dummy component containing the Dialog call', () => {
      expect(dummyComponent).toBeTruthy();
    });

    it('should open the dialog correctly', () => {
      expect(dialogService.open).toHaveBeenCalledWith(NotesDialogComponent, {
        size: 'medium',
        data: {
          type: NoteDialogType.QUOTE_LINE_ITEM,
          title: 'TEST',
          subtitle: 'TEST',
          note: 'TEST',
        },
      });
    });
  });

  describe('on its own', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(NotesDialogComponent);
      component = fixture.componentInstance;

      store = TestBed.get(Store);

      dialog = TestBed.get(Dialog);
      fixture.detectChanges();
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    it('should call close onDismiss()', () => {
      spyOn(dialog, 'close');
      component.onDismiss();
      expect(dialog.close).toHaveBeenCalled();
    });

    it('should dispatch the corresponding Google Analytic Event', () => {
      spyOn(store, 'dispatch');
      component.ngAfterViewInit();
      expect(store.dispatch).toHaveBeenCalledWith(
        new SendAnalyticsCustomEvent({
          event: EventType.EVENT,
          category: EventCategory.MODAL,
          action: EventAction.VIEW_QUOTES,
          label: NoteDialogType.QUOTE_HEADER,
        })
      );
    });
  });
});
