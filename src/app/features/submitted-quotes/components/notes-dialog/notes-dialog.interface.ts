export enum NoteDialogType {
  QUOTE_LINE_ITEM = 'Line Item Notes Display',
  QUOTE_HEADER = 'Header Notes Display',
}

export interface INotesDialogData {
  label: NoteDialogType;
  title: string;
  subtitle: string;
  note: string;
}
