import { IQuoteLineItem } from './../../../../core/quotes/quotes.interfaces';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { getSort } from '@app/features/submitted-quotes/stores/quote-details/quote-details.selectors';
import {
  ChangeSort,
  ToggleAllQuotedDetailLineItems,
  ToggleCheckQuotedDetailLineItem,
} from '@app/features/submitted-quotes/stores/quote-details/quote-details.actions';
import { ISortCriteron, ISortCriteronOrderEnum } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { IAppState } from '@app/shared/shared.interfaces';
import { QuoteLineItemStatus, QuoteStatus, QuoteLineItemCartsStatus } from '@app/core/quotes/quotes.enum';
import { some } from 'lodash';
import { IProductDetails } from '@app/core/inventory/product-details.interface';
import { DialogService } from '@app/core/dialog/dialog.service';
import { NotesDialogComponent } from '../notes-dialog/notes-dialog.component';
import { NoteDialogType } from '../notes-dialog/notes-dialog.interface';
import { isTexasInstruments } from '@app/shared/enums/texas.enums';
import { UserCustomerType } from '@app/core/user/user.interface';

@Component({
  selector: 'app-submitted-quote-details-listing',
  templateUrl: './submitted-quote-details-listing.component.html',
  styleUrls: ['./submitted-quote-details-listing.component.scss'],
})
export class SubmittedQuoteDetailsListingComponent implements OnInit, OnDestroy {
  @Input()
  quoteLineItems;
  @Input()
  quoteStatus: string;
  @Input()
  featureFlags: any;
  @Input()
  customerType: string;

  public showTariffHeadline: boolean = false;
  public allCheckboxesTicked: boolean;
  public sort$: Observable<Array<ISortCriteron>>;
  public sortOrder: Map<string, ISortCriteronOrderEnum>;
  public subscription: Subscription = new Subscription();

  constructor(private store: Store<IAppState>, public dialogService: DialogService) {
    this.sort$ = store.pipe(select(getSort));
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.sortSubscription());
  }

  private sortSubscription(): Subscription {
    this.checkTariff();
    return this.sort$.subscribe(sort => this.setSort(sort));
  }

  public setSort(updatedSort: Array<ISortCriteron>): void {
    this.sortOrder = new Map();
    if (updatedSort.length) {
      updatedSort.forEach(updatedSortItem => {
        this.sortOrder.set(updatedSortItem.key, updatedSortItem.order);
      });
    }
  }

  public sortChange(sortCriteria?: Array<ISortCriteron>): void {
    this.store.dispatch(new ChangeSort(sortCriteria));
  }

  public isQuoteStatusExpired(): boolean {
    return this.quoteStatus === QuoteStatus.EXPIRED;
  }

  public isPricePositive(price: number): boolean {
    return price > 0;
  }

  public showPrice(lineItem, field): boolean {
    switch (lineItem.status) {
      case QuoteLineItemStatus.PROCESSING:
      case QuoteLineItemStatus.EXPIRED: {
        return false;
      }
      default: {
        return !this.isQuoteStatusExpired() && this.isPricePositive(lineItem[field]);
      }
    }
  }

  public getHidePublic(productDetail: IProductDetails): boolean {
    return !productDetail.suppAlloc || productDetail.suppAlloc.toLocaleLowerCase() === 'none';
  }

  public getStatus(lineItem: IQuoteLineItem): string {
    return this.quoteStatus !== QuoteStatus.EXPIRED
      ? lineItem.quoteLineStatus === QuoteLineItemCartsStatus.IN_CART
        ? QuoteLineItemStatus.IN_CART
        : lineItem.status
      : QuoteStatus.EXPIRED;
  }

  public getLineItemNotQuoted(lineItem): boolean {
    if (lineItem) {
      return (
        this.quoteStatus !== QuoteStatus.EXPIRED &&
        lineItem.status === QuoteLineItemStatus.QUOTED &&
        lineItem.quoteLineStatus === QuoteLineItemCartsStatus.QUOTED
      );
    } else {
      return false;
    }
  }

  public areSomeLineItemsQuoted(): boolean {
    return some(this.quoteLineItems, lineItem => lineItem.status === QuoteLineItemStatus.QUOTED);
  }

  public getAllQuotedLineItems(): Array<IQuoteLineItem> {
    return this.quoteLineItems.filter(lineItem => lineItem.status === QuoteLineItemStatus.QUOTED);
  }

  public areAllLineItemsChecked(): boolean {
    return this.getAllQuotedLineItems().every(lineItem => lineItem.checked === true);
  }

  public setCheckToAllValidLineItems(checked: boolean): Array<IQuoteLineItem> {
    return this.quoteLineItems.map(lineItem => {
      if (this.getLineItemNotQuoted(lineItem)) {
        return {
          ...lineItem,
          checked,
        };
      } else {
        return lineItem;
      }
    });
  }

  public toggleAllValidCheckboxes(): void {
    const checked = !this.allCheckboxesTicked;
    this.allCheckboxesTicked = !this.allCheckboxesTicked;

    const lineItemsToToggle: Array<IQuoteLineItem> = this.setCheckToAllValidLineItems(checked);
    this.store.dispatch(new ToggleAllQuotedDetailLineItems(lineItemsToToggle));
  }

  public findLineItemIndex(id: number): number {
    return this.quoteLineItems.findIndex(quoteLineItem => quoteLineItem.quoteLineId === id);
  }

  public toggleCheckQuotedLineItem(partNumber: IQuoteLineItem, checked = false): void {
    const id = partNumber.quoteLineId;
    const index: number = this.findLineItemIndex(id);
    this.store.dispatch(new ToggleCheckQuotedDetailLineItem({ index, checked: !checked }));
  }

  public checkTariff(): void {
    this.showTariffHeadline = some(this.quoteLineItems, lineItem => {
      return lineItem.tariffApplicable;
    });
  }

  public shouldShowNotesIcon(lineItem: IQuoteLineItem): boolean {
    const status = this.getStatus(lineItem);
    if (status !== QuoteLineItemStatus.EXPIRED && status !== QuoteLineItemStatus.PROCESSING) {
      if (lineItem.externalComments && lineItem.externalComments.length > 0) {
        return true;
      }
    }
    return false;
  }

  public showNotesDialog(lineItem: IQuoteLineItem): void {
    this.dialogService.open(NotesDialogComponent, {
      size: 'medium',
      data: {
        label: NoteDialogType.QUOTE_LINE_ITEM,
        title: lineItem.mpn + ' - Notes',
        subtitle: lineItem.manufacturer,
        note: lineItem.externalComments,
      },
    });
  }

  public brokerBanned(lineItem: IQuoteLineItem) {
    return isTexasInstruments(lineItem.manufacturer) && this.customerType === UserCustomerType.BROKER;
  }
}
