import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { SubmittedQuoteDetailsListingComponent } from './submitted-quote-details-listing.component';
import { FormattedPricePipe } from '@app/shared/pipes/formatted-price/formatted-price.pipe';
import { CurrencyPipe } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule } from '@ngrx/store';
import {
  ChangeSort,
  ToggleCheckQuotedDetailLineItem,
  ToggleAllQuotedDetailLineItems,
} from '@app/features/submitted-quotes/stores/quote-details/quote-details.actions';
import { ISortCriteronOrderEnum } from '@app/shared/shared.interfaces';
import { quoteDetailsReducer } from '@app/features/submitted-quotes/stores/quote-details/quote-details.reducers';
import { QuoteLineItemStatus, QuoteStatus, QuoteLineItemCartsStatus } from '@app/core/quotes/quotes.enum';
import { IQuoteLineItem } from '@app/core/quotes/quotes.interfaces';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { UserCustomerType } from '@app/core/user/user.interface';

export const mockQuoteLineItem: IQuoteLineItem = {
  quoteLineNumber: '81.1',
  quoteLineId: 40432326,
  docId: '1790_07219597',
  status: 'Quoted',
  cpn: '03-0569',
  mpn: '05-0569',
  foh: 0,
  moq: 0,
  multOrdQty: 0,
  notes: '',
  leadtime: '99',
  partNumber: '0',
  manufacturer: 'SILICON',
  quantity: 6000,
  targetPrice: 0,
  quotedPrice: 8.52,
  totalPrice: 51120,
  quoteLineStatus: 'QUOTED',
  tariffAmount: 377.42,
  tariffApplicable: true,
  externalComments: 'example comment',
  productDetail: {
    id: '7219597',
    itemId: 7219597,
    mpn: 'MT29C4G48MAZBBAKB-48 IT',
    manufacturer: 'Micron Technology',
    description: 'NAND Flash and Mobile LPDDR PoP',
    image: 'http://download.siliconexpert.com/pdfs/2011/7/28/10/17/3/439/tyc_/manual/not_valid_image.jpg',
    itemType: 'COMPONENTS',
    purchasable: true,
    compliance: { CHINA_ROHS: 'CHINA_ROHS_RHC', EU_ROHS: 'EU_ROHS_RHC' },
    warehouseId: 1790,
    arrowReel: false,
    minimumOrderQuantity: 1,
    multipleOrderQuantity: 1,
    availableQuantity: 0,
    bufferQuantity: 0,
    leadTime: '99',
    customerPartNumber: null,
    datasheet: 'http://download.siliconexpert.com/pdfs/2008/06/30/pemco_b/cs/hnw/ds/nods.pdf',
    endCustomerRecords: [{ cpn: '03-0569', endCustomers: [{ name: 'Medtronic Inc', endCustomerSiteId: 2275681 }] }],
    price: {
      priceTier: [
        { minQuantity: 1, maxQuantity: 66, price: 11.8511 },
        { minQuantity: 67, maxQuantity: 330, price: 11.53507 },
        { minQuantity: 331, maxQuantity: 660, price: 10.55037 },
        { minQuantity: 661, maxQuantity: 1321, price: 10.178 },
        { minQuantity: 1322, maxQuantity: 3302, price: 9.83102 },
        { minQuantity: 3303, maxQuantity: 13211, price: 9.61256 },
        { minQuantity: 13212, maxQuantity: 132117458, price: 9.50692 },
      ],
    },
    pipeline: { deliveryDate: null, quantity: 0 },
    spq: 1008,
    docId: '1790_07219597',
    specs: null,
    category: 'Semiconductor - IC > Memory > Multi-Chip Package Memory',
    suppAlloc: 'SUPPLIER ALLOCATED',
    ncnr: false,
  },
};

describe('SubmittedQuoteDetailsListingComponent', () => {
  let store: Store<any>;
  let component: SubmittedQuoteDetailsListingComponent;
  let fixture: ComponentFixture<SubmittedQuoteDetailsListingComponent>;
  let dialogService: DialogService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SubmittedQuoteDetailsListingComponent, FormattedPricePipe],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      providers: [CurrencyPipe, { provide: DialogService, useClass: DialogServiceMock }],
      imports: [RouterTestingModule, StoreModule.forRoot({ quoteDetails: quoteDetailsReducer })],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittedQuoteDetailsListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    dialogService = TestBed.get(DialogService);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch `ChangeSort` to the store', () => {
    const sortObject = [
      {
        key: 'status',
        order: ISortCriteronOrderEnum.ASC,
      },
    ];

    component.sortChange(sortObject);
    expect(store.dispatch).toHaveBeenCalledWith(new ChangeSort(sortObject));
  });

  it('should return true for showPrice() if status is not PROCESSING or EXPIRED, even if Quote is not Expired,  for a given LineItem', () => {
    const quoteLineItem = {
      ...mockQuoteLineItem,
      totalPrice: 100,
      status: QuoteLineItemStatus.QUOTED,
    };

    component.quoteStatus = QuoteStatus.PART_QUOTED;

    const quotedPrice = component.showPrice(quoteLineItem, 'totalPrice');
    expect(quotedPrice).toEqual(true);
  });

  it('should return false for showPrice() if status is PROCESSING, even if Quote is not Expired,  for a given LineItem', () => {
    const quoteLineItem = {
      ...mockQuoteLineItem,
      totalPrice: 100,
      status: QuoteLineItemStatus.PROCESSING,
    };

    component.quoteStatus = QuoteStatus.PART_QUOTED;

    const quotedPrice = component.showPrice(quoteLineItem, 'totalPrice');
    expect(quotedPrice).toEqual(false);
  });

  it('should return false for showPrice() if status is EXPIRED, even if Quote is not Expired, for a given LineItem', () => {
    const quoteLineItem = {
      ...mockQuoteLineItem,
      totalPrice: 100,
      status: QuoteLineItemStatus.EXPIRED,
    };

    component.quoteStatus = QuoteStatus.PART_QUOTED;

    const quotedPrice = component.showPrice(quoteLineItem, 'totalPrice');
    expect(quotedPrice).toEqual(false);
  });

  it('getStatus() should return "Expired" for this test lineitem if Quote status is Expired', () => {
    const quoteLineItem = {
      ...mockQuoteLineItem,
      totalPrice: 100,
      status: QuoteLineItemStatus.QUOTED,
    };

    component.quoteStatus = QuoteStatus.EXPIRED;

    const quotedPrice = component.getStatus(quoteLineItem);
    expect(quotedPrice).toContain(QuoteLineItemStatus.EXPIRED);
  });

  it('getStatus() should return "Quoted" for this test lineitem if Quote status is NOT Expired', () => {
    const quoteLineItem = {
      ...mockQuoteLineItem,
      totalPrice: 100,
      status: QuoteLineItemStatus.QUOTED,
    };

    component.quoteStatus = QuoteStatus.PROCESSING;
    const quotedPrice = component.getStatus(quoteLineItem);
    expect(quotedPrice).toContain(QuoteLineItemStatus.QUOTED);
  });

  describe('#getHidePublic()', () => {
    const testCases = [
      {
        data: {
          ...mockQuoteLineItem,
          productDetail: {
            ...mockQuoteLineItem.productDetail,
            suppAlloc: 'NoNe',
          },
        },
        expected: true,
      },
      {
        data: {
          ...mockQuoteLineItem,
          productDetail: {
            ...mockQuoteLineItem.productDetail,
            suppAlloc: null,
          },
        },
        expected: true,
      },
      {
        data: {
          ...mockQuoteLineItem,
          productDetail: {
            ...mockQuoteLineItem.productDetail,
            suppAlloc: '',
          },
        },
        expected: true,
      },
      {
        data: {
          ...mockQuoteLineItem,
          productDetail: {
            ...mockQuoteLineItem.productDetail,
            suppAlloc: undefined,
          },
        },
        expected: true,
      },
      {
        data: {
          ...mockQuoteLineItem,
          productDetail: {
            ...mockQuoteLineItem.productDetail,
            suppAlloc: 'ARROW ALLOCATED',
          },
        },
        expected: false,
      },
    ];

    for (const testCase of testCases) {
      it(`Should return ${testCase.expected} when suppAlloc is '${testCase.data.productDetail.suppAlloc}'`, () => {
        const result = component.getHidePublic(testCase.data.productDetail);
        expect(result).toEqual(testCase.expected);
      });
    }
  });

  it('toggleCheckQuotedLineItem() should call "findLineItemIndex()" and dispatch "ToggleCheckQuotedDetailLineItem" action', () => {
    spyOn(component, 'findLineItemIndex').and.callThrough();
    component.quoteLineItems = [mockQuoteLineItem];

    const action = new ToggleCheckQuotedDetailLineItem({ index: 0, checked: true });
    component.toggleCheckQuotedLineItem(mockQuoteLineItem);

    expect(component.findLineItemIndex).toHaveBeenCalledWith(mockQuoteLineItem.quoteLineId);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('toggleAllValidCheckboxes() should call "setCheckToAllValidLineItems()" and dispatch "ToggleAllQuotedDetailLineItems" action when item is Quotes', () => {
    spyOn(component, 'setCheckToAllValidLineItems').and.callThrough();

    const notQuotedLineItem = {
      ...mockQuoteLineItem,
      tariffApplicable: false,
      status: QuoteLineItemStatus.IN_CART,
      quoteLineStatus: QuoteLineItemCartsStatus.IN_CART,
    };

    component.quoteLineItems = [mockQuoteLineItem, notQuotedLineItem];
    component.quoteLineItems[0].checked = true;

    const action = new ToggleAllQuotedDetailLineItems([mockQuoteLineItem, notQuotedLineItem]);
    component.toggleAllValidCheckboxes();

    expect(component.setCheckToAllValidLineItems).toHaveBeenCalledWith(true);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('areAllLineItemsChecked() should call "getAllQuotedLineItems()"', () => {
    spyOn(component, 'getAllQuotedLineItems').and.callThrough();
    component.quoteLineItems = [mockQuoteLineItem];
    component.quoteLineItems[0].checked = true;

    component.areAllLineItemsChecked();

    expect(component.getAllQuotedLineItems).toHaveBeenCalled();
  });

  it('areSomeLineItemsQuoted() should return true if at least 1 line item is quoted', () => {
    component.quoteLineItems = [mockQuoteLineItem];

    const result = component.areSomeLineItemsQuoted();

    expect(result).toBeTruthy();
  });

  it('should show Tariff Head line for checkTariff() if tariffApplicable is true and tariffAmount greater than zero ', () => {
    component.quoteLineItems = [
      {
        ...mockQuoteLineItem,
        tariffApplicable: true,
        status: QuoteLineItemStatus.QUOTED,
      },
    ];

    component.checkTariff();
    expect(component.showTariffHeadline).toBeTruthy();
  });

  it('should hide Tariff Head line for checkTariff() if tariffApplicable is false and tariffAmount greater than zero ', () => {
    component.quoteLineItems = [
      {
        ...mockQuoteLineItem,
        tariffApplicable: false,
        status: QuoteLineItemStatus.QUOTED,
      },
    ];

    component.checkTariff();
    expect(component.showTariffHeadline).toBeFalsy();
  });

  it('should return `false` on a falsy lineItem for getLineItemNotQuoted()', () => {
    const falseExpected = component.getLineItemNotQuoted(undefined);
    expect(falseExpected).toBe(false);
  });

  describe('#shouldShowNotesIcon()', () => {
    const testCases = [
      {
        data: {
          ...mockQuoteLineItem,
        },
        expected: true,
      },
      {
        data: {
          ...mockQuoteLineItem,
          status: QuoteLineItemStatus.PROCESSING,
        },
        expected: false,
      },
      {
        data: {
          ...mockQuoteLineItem,
          status: QuoteLineItemStatus.EXPIRED,
        },
        expected: false,
      },
      {
        data: {
          ...mockQuoteLineItem,
          status: QuoteLineItemStatus.IN_CART,
        },
        expected: true,
      },
      {
        data: {
          ...mockQuoteLineItem,
          status: QuoteLineItemStatus.IN_CART,
          externalComments: null,
        },
        expected: false,
      },
      {
        data: {
          ...mockQuoteLineItem,
          status: QuoteLineItemStatus.IN_CART,
          externalComments: '',
        },
        expected: false,
      },
    ];

    for (const testCase of testCases) {
      it(`should return ${testCase.expected} when lineitem status is '${testCase.data.status}' and note is: '${
        testCase.data.externalComments
      }' `, () => {
        const result = component.shouldShowNotesIcon(testCase.data);
        expect(result).toEqual(testCase.expected);
      });
    }
  });

  it('should open a dialog on showNotesDialog()', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.showNotesDialog(mockQuoteLineItem);
    expect(dialogService.open).toHaveBeenCalled();
  });

  it('brokerBanned() should return true if is a Broker and is a banned manufacturer', () => {
    component.customerType = UserCustomerType.BROKER;
    const quoteLineItem = {
      ...mockQuoteLineItem,
      manufacturer: 'Texas Instruments',
      totalPrice: 100,
      status: QuoteLineItemStatus.QUOTED,
    };

    const isBanned = component.brokerBanned(quoteLineItem);
    expect(isBanned).toBe(true);
  });

  it('brokerBanned() should return true if is a Broker and is NOT a banned manufacturer', () => {
    component.customerType = UserCustomerType.BROKER;
    const quoteLineItem = {
      ...mockQuoteLineItem,
      manufacturer: 'I am not a banned Manufacturer',
      totalPrice: 100,
      status: QuoteLineItemStatus.QUOTED,
    };

    const isBanned = component.brokerBanned(quoteLineItem);
    expect(isBanned).toBe(false);
  });
});
