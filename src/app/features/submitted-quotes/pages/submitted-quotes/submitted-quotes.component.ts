import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IQuote } from '@app/core/quotes/quotes.interfaces';
import { getUserBillToAccount } from '@app/core/user/store/user.selectors';
import { ChangePage, ChangePageLimit, ChangeSort, SearchQuotes } from '@app/features/submitted-quotes/stores/quotes/quotes.actions';
import {
  getError,
  getLimit,
  getLoading,
  getPage,
  getQuotes,
  getSearchQuery,
  getSort,
  getTotalPages,
} from '@app/features/submitted-quotes/stores/quotes/quotes.selectors';
import { IAppState, ISortCriteron, ISortCriteronOrderEnum } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { filter, first } from 'rxjs/operators';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: 'app-submitted-quotes',
  templateUrl: './submitted-quotes.component.html',
  styleUrls: ['./submitted-quotes.component.scss'],
})
export class SubmittedQuotesComponent implements OnInit, OnDestroy {
  public limit$: Observable<number>;
  public page$: Observable<number>;
  public totalPages$: Observable<number>;
  public quotes$: Observable<Array<IQuote>>;
  public quotes: Array<IQuote>;
  public loading$: Observable<boolean>;
  public error$: Observable<Error>;
  public serverError: any = null;
  public billToAccount$: Observable<number>;
  public currentBillToAccount: number;
  public defaultSearch: string = '';
  public billToAccountSub: Subscription;
  public sort$: Observable<Array<ISortCriteron>>;
  public sortOrder: Map<string, ISortCriteronOrderEnum>;
  public form: FormGroup;
  public showError: boolean = false;
  public search$: Observable<string>;
  public submittedSearch: string;
  public privateFeatures$: Observable<any>;
  private subscription: Subscription = new Subscription();
  public featureFlags: any;

  constructor(public store: Store<IAppState>, public formBuilder: FormBuilder) {
    this.limit$ = store.pipe(select(getLimit));
    this.page$ = this.store.pipe(select(getPage));
    this.totalPages$ = this.store.pipe(select(getTotalPages));
    this.quotes$ = this.store.pipe(select(getQuotes));
    this.loading$ = this.store.pipe(select(getLoading));
    this.error$ = this.store.pipe(select(getError));
    this.billToAccount$ = this.store.pipe(select(getUserBillToAccount));
    this.sort$ = store.pipe(select(getSort));
    this.search$ = this.store.pipe(select(getSearchQuery));
    this.privateFeatures$ = this.store.pipe(select(getPrivateFeatureFlagsSelector));
  }

  ngOnInit() {
    this.setUpForm();
    this.setInitialStoreValues();
    this.getCurrentBillToAccount();
    this.reloadQuotesOnBillToChange();

    this.subscription.add(this.subscribePrivateFeatureFlags());
    this.subscription.add(this.subscribeQuotes());
    this.subscription.add(this.subscribeSearch());
    this.subscription.add(this.subscribeError());
  }

  private subscribeError(): Subscription {
    return this.error$.subscribe(error => {
      this.serverError = error;
    });
  }

  private subscribeSearch(): Subscription {
    return this.search$.subscribe(search => {
      this.submittedSearch = search;
    });
  }

  private subscribeQuotes(): Subscription {
    return this.quotes$.subscribe(quotes => {
      this.quotes = quotes;
    });
  }

  private subscribePrivateFeatureFlags(): Subscription {
    return this.privateFeatures$.subscribe(features => {
      this.featureFlags = features;
    });
  }

  ngOnDestroy() {
    this.billToAccountSub.unsubscribe();
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public setInitialStoreValues(): void {
    this.pageLimitChange(10);
    this.pageChange(1);
    this.quotesSearch(this.defaultSearch);

    this.store.dispatch(new ChangeSort([{ key: 'submittedDate', order: ISortCriteronOrderEnum.DESC }]));
  }

  public pageChange(page: number): void {
    this.store.dispatch(new ChangePage(page));
  }

  public pageLimitChange(pageLimit: number): void {
    this.store.dispatch(new ChangePageLimit(pageLimit));
  }

  public quotesSearch(searchQuery): void {
    this.store.dispatch(new SearchQuotes(searchQuery));
  }

  public getCurrentBillToAccount(): void {
    this.billToAccount$.pipe(first()).subscribe((billToAccount: number) => {
      this.setCurrentBillToAccount(billToAccount);
    });
  }

  public setCurrentBillToAccount(billToAccount: number): void {
    this.currentBillToAccount = billToAccount;
  }

  public reloadQuotesOnBillToChange(): void {
    this.billToAccountSub = this.billToAccount$.pipe(filter(billToAccount => billToAccount !== 0)).subscribe((billToAccount: number) => {
      if (this.currentBillToAccount !== billToAccount) {
        this.setCurrentBillToAccount(billToAccount);
        this.quotesSearch(this.defaultSearch);
        this.pageChange(1);
      }
    });
  }

  public setUpForm(): void {
    this.form = this.formBuilder.group({
      searchQuery: [null, Validators.compose([Validators.required])],
    });
  }

  public checkForm(): void {
    this.showError = !this.form.valid;
    this.serverError = null;

    /* istanbul ignore else */
    if (this.form.valid) {
      this.quotesSearch(this.form.get('searchQuery').value.trim());
      this.pageChange(1);
    }
  }

  public resetForm(): void {
    this.form.reset();
    this.quotesSearch('');
    this.pageChange(1);
  }
}
