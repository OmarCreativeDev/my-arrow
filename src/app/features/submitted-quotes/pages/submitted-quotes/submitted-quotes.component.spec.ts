import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { SubmittedQuotesComponent } from './submitted-quotes.component';
import { Observable, of } from 'rxjs';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { QuotesService } from '@app/core/quotes/quotes.service';
import { ChangePage, ChangePageLimit, SearchQuotes } from '@app/features/submitted-quotes/stores/quotes/quotes.actions';
import { ReactiveFormsModule } from '@angular/forms';

export class StoreMock {
  public dispatch(): void {}
  public select(): Observable<number> {
    return of(10);
  }
  public pipe() {
    return of({});
  }
}

export class QuotesServiceMock {
  public search(): Observable<any> {
    return of({});
  }
}

let store: Store<any>;

describe('SubmittedQuotesComponent', () => {
  let component: SubmittedQuotesComponent;
  let fixture: ComponentFixture<SubmittedQuotesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: Store, useClass: StoreMock }, { provide: QuotesService, useClass: QuotesServiceMock }],
      declarations: [SubmittedQuotesComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [ReactiveFormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittedQuotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`ChangePageLimit` action is dispatched on the store when `pageLimitChange()` method has fired', () => {
    spyOn(store, 'dispatch');
    component.pageLimitChange(10);
    expect(store.dispatch).toHaveBeenCalledWith(new ChangePageLimit(10));
  });

  it('`ChangePage` action is dispatched on the store when `pageChange()` method has fired', () => {
    spyOn(store, 'dispatch');
    component.pageChange(2);
    expect(store.dispatch).toHaveBeenCalledWith(new ChangePage(2));
  });

  it('`SearchQuotes` action is dispatched on the store when `quotesSearch()` method has fired', () => {
    spyOn(store, 'dispatch');
    component.quotesSearch('');
    expect(store.dispatch).toHaveBeenCalledWith(new SearchQuotes(''));
  });

  it('`getCurrentBillToAccount()` invokes `setCurrentBillToAccount()`', () => {
    spyOn(component, 'setCurrentBillToAccount');
    component.getCurrentBillToAccount();
    expect(component.setCurrentBillToAccount).toHaveBeenCalled();
  });

  it('`setCurrentBillToAccount()` sets `currentBillToAccount`', () => {
    component.setCurrentBillToAccount(33291);
    expect(component.currentBillToAccount).toBeDefined();
    expect(component.currentBillToAccount).toEqual(33291);
  });

  it('`reloadQuotesOnBillToChange()` invokes `quotesSearch()`, `setCurrentBillToAccount()` & `pageChange` if `billToAccount` has changed', () => {
    spyOn(component, 'setCurrentBillToAccount');
    spyOn(component, 'quotesSearch');
    spyOn(component, 'pageChange');

    component.currentBillToAccount = 32313;
    component.reloadQuotesOnBillToChange();

    expect(component.setCurrentBillToAccount).toHaveBeenCalled();
    expect(component.quotesSearch).toHaveBeenCalled();
    expect(component.pageChange).toHaveBeenCalled();
  });

  it('`checkForm` invokes `quotesSearch` & `pageChange` if the search query is valid', () => {
    spyOn(component, 'quotesSearch');
    spyOn(component, 'pageChange');

    component.form.controls['searchQuery'].setValue('bav9');
    component.checkForm();

    expect(component.quotesSearch).toHaveBeenCalled();
    expect(component.pageChange).toHaveBeenCalled();
  });

  it('`resetForm` invokes `quotesSearch` & `pageChange`', () => {
    spyOn(component, 'quotesSearch');
    spyOn(component, 'pageChange');

    component.resetForm();

    expect(component.quotesSearch).toHaveBeenCalled();
    expect(component.pageChange).toHaveBeenCalled();
  });
});
