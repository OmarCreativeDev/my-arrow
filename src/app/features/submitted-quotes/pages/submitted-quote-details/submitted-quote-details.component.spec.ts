import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SubmittedQuoteDetailsComponent } from './submitted-quote-details.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { of, from } from 'rxjs';
import { Store, ActionsSubject, StoreModule } from '@ngrx/store';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import {
  ChangePage,
  ChangePageLimit,
  SearchLineItems,
  RequestAddQuotedLineItemToShoppingCart,
} from '@app/features/submitted-quotes/stores/quote-details/quote-details.actions';
import { FormattedPricePipe } from '@app/shared/pipes/formatted-price/formatted-price.pipe';
import { CurrencyPipe, CommonModule } from '@angular/common';
import { IQuoteDetailsResponse } from '@app/core/quotes/quotes.interfaces';
import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { DialogService } from '@app/core/dialog/dialog.service';
import { OverlayContainer } from 'ngx-toastr';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { ToastService } from '@app/core/toast/toast.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { userReducers } from '@app/core/user/store/user.reducers';
import { quoteDetailsReducer } from '../../stores/quote-details/quote-details.reducers';
import { cartReducers } from '@app/features/cart/stores/cart/cart.reducers';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';
import { QuoteLineItemStatus } from '@app/core/quotes/quotes.enum';
import { propertiesReducers } from '@app/features/properties/store/properties.reducers';
import { mockPrivateProperties } from '@app/core/features/feature-flags.mock';

class MockToastService {
  public showToast() {}
}

let store: Store<any>;

export const mockedQuotedLineItem = [
  {
    quoteLineNumber: '1.1',
    quoteLineStatus: 'status',
    foh: 0,
    leadtime: '',
    notes: '',
    partNumber: '',
    quoteLineId: 62261238,
    itemId: 1965435,
    warehouseId: 323,
    docId: '0323_01965435',
    status: QuoteLineItemStatus.QUOTED,
    mpn: 'BZX55C10',
    manufacturer: 'ON Semiconductor',
    cpn: 'OBSNOSTOCK',
    quantity: 34000,
    moq: 34000,
    multOrdQty: 34000,
    leadTime: '313',
    currency: 'EUR',
    targetPrice: 0,
    quotedPrice: 0,
    totalPrice: 5,
    checked: true,
  },
  {
    quoteLineNumber: '1.1',
    quoteLineStatus: 'status',
    foh: 0,
    leadtime: '',
    notes: '',
    partNumber: '',
    quoteLineId: 62261239,
    itemId: 1965435,
    warehouseId: 323,
    docId: '0323_01965435',
    status: QuoteLineItemStatus.PROCESSING,
    mpn: 'BZX55C10',
    manufacturer: 'ON Semiconductor',
    cpn: 'OBSNOSTOCK',
    quantity: 34000,
    moq: 34000,
    multOrdQty: 34000,
    leadTime: '313',
    currency: 'EUR',
    targetPrice: 0,
    quotedPrice: 0,
    totalPrice: 5,
    checked: true,
  },
];

export const mockedQuoteDetails = {
  accountName: 'string',
  accountNumber: 'string',
  billTo: {
    addressLine1: 'string',
    addressLine2: 'string',
    addressLine3: 'string',
    city: 'string',
    country: 'string',
    id: 12345,
    name: 'string',
    postCode: 'string',
    state: 'string',
  },
  expiryDate: '12/12/2020',
  internalSalesRep: 'string',
  itemsQuoted: 12345,
  owner: 'string',
  quoteNumber: '12345',
  quoteType: 'string',
  quotedCurrency: 'EUR',
  referenceNumber: 'string',
  shipTo: {
    addressLine1: 'string',
    addressLine2: 'string',
    addressLine3: 'string',
    city: 'string',
    country: 'string',
    id: 12345,
    name: 'string',
    postCode: 'string',
    state: 'string',
  },
  status: 'string',
  submittedDate: 'string',
  terms: 'string',
  totalCost: 12345,
  totalItems: 12345,
};

export const mockWSResponse = {
  quoteHeaderId: 2472968,
  quoteNumber: 'string',
  quoteLineId: 62261239,
  quoteLineNumber: 'string',
  eventCode: 'string',
  eventMessage: 'Insertion exceeds max number',
};

const mockPropertiesReducers = () => ({
  ...propertiesReducers,
  private: mockPrivateProperties,
});

describe('SubmittedQuoteDetailsComponent', () => {
  let component: SubmittedQuoteDetailsComponent;
  let fixture: ComponentFixture<SubmittedQuoteDetailsComponent>;
  const mockQuoteHeaderId: number = 2472968;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        StoreModule.forRoot({
          cart: cartReducers,
          user: userReducers,
          properties: mockPropertiesReducers,
          quoteDetails: quoteDetailsReducer,
        }),
      ],
      declarations: [SubmittedQuoteDetailsComponent, FormattedPricePipe],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({
              quoteHeaderId: mockQuoteHeaderId,
            }),
          },
        },
        CurrencyPipe,
        ActionsSubject,
        AuthTokenService,
        OverlayContainer,
        { provide: WSSService, useClass: MockStompWSSService },
        { provide: ToastService, useClass: MockToastService },
        { provide: DialogService, useClass: DialogServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmittedQuoteDetailsComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);
    component.toastService.showToast = () => {
      return;
    };
    component.toastService.hideToast = () => {
      return;
    };
    component.toastService.showErrorToast = () => {
      return;
    };
    spyOn(store, 'dispatch');
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`ChangePageLimit` action is dispatched on the store when `pageLimitChange()` method has fired', () => {
    component.pageLimitChange(10);
    expect(store.dispatch).toHaveBeenCalledWith(new ChangePageLimit(10));
  });

  it('`ChangePage` action is dispatched on the store when `pageChange()` method has fired', () => {
    component.pageChange(2);
    expect(store.dispatch).toHaveBeenCalledWith(new ChangePage(2));
  });

  it('`SearchLineItems` action is dispatched on the store when `lineItemSearch()` method has fired', () => {
    component.lineItemSearch({ searchText: '', id: 6840542 });
    expect(store.dispatch).toHaveBeenCalledWith(new SearchLineItems({ searchText: '', id: 6840542 }));
  });

  it('`checkForm` invokes `lineItemSearch` & `pageChange` if the search query is valid', () => {
    spyOn(component, 'lineItemSearch');
    spyOn(component, 'pageChange');

    fixture.detectChanges();
    component.form.controls['searchQuery'].setValue('Yageo');
    component.checkForm();

    expect(component.lineItemSearch).toHaveBeenCalled();
    expect(component.pageChange).toHaveBeenCalled();
  });

  it('`resetForm` invokes `lineItemSearch` & `pageChange`', () => {
    spyOn(component, 'lineItemSearch');
    spyOn(component, 'pageChange');

    fixture.detectChanges();
    component.resetForm();

    expect(component.lineItemSearch).toHaveBeenCalled();
    expect(component.pageChange).toHaveBeenCalled();
  });

  it('handleActivatedRoute() should call setQuoteDetails() through quoteDetailsSubscription()', () => {
    const mockQuoteDetailsResponse = Array<IQuoteDetailsResponse>();

    const QuoteDetailResponse = {
      accountName: 'accountName',
      accountNumber: 'accountNumber',
      billTo: {
        addressLine1: 'addressLine1',
        addressLine2: 'addressLine2',
        addressLine3: 'addressLine3',
        city: 'city',
        country: 'country',
        id: 1,
        name: 'name',
        postCode: 'postCode',
        state: 'state',
      },
      expiryDate: 'expiryDate',
      internalSalesRep: 'internalSalesRep',
      itemsQuoted: 1,
      owner: 'owner',
      quoteNumber: 'quoteNumber',
      quoteType: 'quoteType',
      quotedCurrency: 'quotedCurrency',
      referenceNumber: 'referenceNumber',
      shipTo: {
        addressLine1: 'addressLine1',
        addressLine2: 'addressLine2',
        addressLine3: 'addressLine3',
        city: 'city',
        country: 'country',
        id: 1,
        name: 'name',
        postCode: 'postCode',
        state: 'state',
      },
      status: 'status',
      submittedDate: 'submittedDate',
      terms: 'terms',
      totalCost: 1,
      totalItems: 1,
    };

    mockQuoteDetailsResponse.push(QuoteDetailResponse);
    mockQuoteDetailsResponse.push(QuoteDetailResponse);
    component.quoteDetails$ = from(mockQuoteDetailsResponse);

    spyOn(component, 'setQuoteDetails').and.callThrough();
    component.handleActivatedRoute(mockQuoteHeaderId);
    expect(component.setQuoteDetails).toHaveBeenCalled();
  });

  it('shouldDisplayError() should return true if this.serverError.message is not equal to this.errorMessage', () => {
    component.serverError = { message: 'error 001' };
    component.regularErrorMessage = 'error 011';

    const result = component.shouldDisplayError();

    expect(result).toBeTruthy();
  });

  it('shouldDisplayError() should return false if this.serverError.message is equal to this.errorMessage', () => {
    component.serverError = { message: 'error 001' };
    component.regularErrorMessage = 'error 001';

    const result = component.shouldDisplayError();

    expect(result).toBeFalsy();
  });

  it('subscribeToWSTopic() should be called in the `ngOnInit`', () => {
    spyOn(component, 'subscribeToWSTopic');
    component.quoteHeaderId = mockQuoteHeaderId;
    fixture.detectChanges();

    expect(component.subscribeToWSTopic).toHaveBeenCalledWith(mockQuoteHeaderId);
  });

  describe('Creation of Request for Buy from Quotes', () => {
    beforeEach(async(() => {
      component.quoteHeaderId = mockQuoteHeaderId;
      component.quoteLineItems = mockedQuotedLineItem;
      component.quoteDetails = mockedQuoteDetails;
      component.billToId = 12345;
      component.currencyCode = 'US';
      component.shoppingCartId = '12345';
      component.userId = '12345';
    }));

    const mockedCuratedLineItems = [
      {
        quoteHeaderId: 2472968,
        quoteNumber: '12345',
        quoteLineId: 62261238,
      },
      {
        quoteHeaderId: 2472968,
        quoteNumber: '12345',
        quoteLineId: 62261239,
      },
    ];

    it('getQuoteLineItemsReadyForRequest should return the curated QuotedLineItems To Add into Shopping Cart', () => {
      const result = component.getQuoteLineItemsReadyForRequest();

      expect(result).toEqual(mockedCuratedLineItems);
    });

    it('buildAddToCartRequest() should return the correct data to be sent to the request', () => {
      const request = {
        billToId: 12345,
        currency: 'US',
        lineItems: mockedCuratedLineItems,
        shoppingCartId: '12345',
        userId: '12345',
      };

      const result = component.buildAddToCartRequest();

      expect(result).toEqual(request);
    });

    it('shouldAddToCart() should return true if items to add is lower or equal to remaining items of cart limit', () => {
      component.cartCurrentLineItems = 98;
      component.cartMaxLineItems = 100;
      component.quoteDetails.quotedCurrency = 'EUR';
      component.cartCurrency = 'EUR';

      const result = component.shouldAddToCart();

      expect(result).toBeTruthy();
    });

    it('shouldAddToCart() should return false if items to add is higher than the remaining items of cart limit', () => {
      component.cartCurrentLineItems = 100;
      component.cartMaxLineItems = 100;

      const result = component.shouldAddToCart();

      expect(result).toBeFalsy();
    });

    it('shouldDisplayExpirationAlert() should return false if quoteDetails exists and quote has not expired', () => {
      const result = component.shouldDisplayExpirationAlert();

      expect(result).toBeFalsy();
    });

    it('shouldDisplayExpirationAlert() should return false if quoteDetails does not exists', () => {
      component.quoteDetails = undefined;
      const result = component.shouldDisplayExpirationAlert();

      expect(result).toBeFalsy();
    });
  });

  describe('addToCart', function() {
    beforeEach(function() {
      component.quoteLineItems = mockedQuotedLineItem;
      component.cartMaxLineItems = 100;
      component.quoteDetails = mockedQuoteDetails;
      component.cartCurrency = 'EUR';
    });

    it('addToCart() should call `startRequestAddToCart()` if `shouldAddToCart()` returns true', () => {
      spyOn(component, 'startRequestAddToCart').and.callThrough();
      component.cartCurrentLineItems = 10;
      component.addToCart();

      expect(component.startRequestAddToCart).toHaveBeenCalled();
    });

    it('addToCart() should call `showLimitedItemsToAddDialog()` if `shouldAddToCart()` returns false', () => {
      spyOn(component.toastService, 'hideToast');
      spyOn(component.dialogService, 'open');
      component.cartCurrentLineItems = 100;
      component.addToCart();

      expect(component.toastService.hideToast).toHaveBeenCalled();
      expect(component.dialogService.open).toHaveBeenCalled();
    });

    it('startRequestAddToCart() should dispatch `RequestAddQuotedLineItemToShoppingCart` action', () => {
      spyOn(component, 'buildAddToCartRequest');
      spyOn(component.toastService, 'showToast');

      component.quoteDetails = mockedQuoteDetails;
      const request = component.buildAddToCartRequest();
      const action = new RequestAddQuotedLineItemToShoppingCart(request);

      component.startRequestAddToCart();

      expect(component.buildAddToCartRequest).toHaveBeenCalled();
      expect(store.dispatch).toHaveBeenCalledWith(action);
      expect(component.toastService.showToast).toHaveBeenCalled();
    });
  });

  describe('shouldDisableAddToCartButton', function() {
    beforeEach(function() {
      component.quoteLineItems = mockedQuotedLineItem;
    });

    it('shouldDisableAddToCartButton() should call `isAnyLineItemChecked()` if quoteLineItems is truthy', () => {
      spyOn(component, 'isAnyLineItemChecked');

      component.shouldDisableAddToCartButton();

      expect(component.isAnyLineItemChecked).toHaveBeenCalled();
    });

    it('shouldDisableAddToCartButton() should return true if quoteLineItems is falsy', () => {
      component.quoteLineItems = undefined;
      const result = component.shouldDisableAddToCartButton();

      expect(result).toBeTruthy();
    });

    it('isAnyLineItemChecked() should call `getAllQuotedLineItems()`', () => {
      spyOn(component, 'getAllQuotedLineItems').and.callThrough();

      component.isAnyLineItemChecked();

      expect(component.getAllQuotedLineItems).toHaveBeenCalled();
    });
  });
});
