import {
  RequestAddQuotedLineItemToShoppingCart,
  UpdateSubmitedLineItemsState,
  ToggleCheckQuotedDetailLineItem,
  RequestProductsLoggedOnAnalytics,
} from './../../stores/quote-details/quote-details.actions';

import { Component, OnInit, OnDestroy } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Client, Message, StompSubscription } from '@stomp/stompjs';

import {
  IQuoteLineItem,
  IQuoteDetailsResponse,
  IQuotedRequestToAdd,
  IQuotedLineItemToAdd,
  IQuoteLineItemsToAddWSResponse,
  IQuotedProductsToAdd,
} from '@app/core/quotes/quotes.interfaces';
import {
  ChangePage,
  ChangePageLimit,
  ChangeSort,
  SearchLineItems,
  RequestQuoteDetails,
  ReceiveWSAddToCartResponseFailed,
  ReceiveWSAddToCartResponseSuccess,
} from '@app/features/submitted-quotes/stores/quote-details/quote-details.actions';
import { IAppState, ISortCriteronOrderEnum } from '@app/shared/shared.interfaces';
import {
  getQuoteLineItems,
  getQuoteDetails,
  getLoading,
  getLimit,
  getPage,
  getTotalPages,
  getError,
  getSearchQuery,
} from '@app/features/submitted-quotes/stores/quote-details/quote-details.selectors';
import { QuoteLineItemStatus, QuoteLineItemCartsStatus, QuoteWSMessageEventCodes } from '@app/core/quotes/quotes.enum';

import { WSSService } from '@app/core/ws/wss.service';
import { WSSState } from '@app/core/ws/wss.interface';
import { ToastService } from '@app/core/toast/toast.service';
import { getShoppingCartId, getCartMaxLineItems, getCartItemsCount, getCartCurrency } from '@app/features/cart/stores/cart/cart.selectors';
import { getUserBillToAccount, getCurrencyCode, getUserEmail, getCustomerType } from '@app/core/user/store/user.selectors';
import { DialogService } from '@app/core/dialog/dialog.service';
import { AddToCartDialogComponent } from '../../components/add-to-cart-dialog/add-to-cart-dialog.component';
import { GetCarts } from '@app/features/cart/stores/cart/cart.actions';
import { skip, take } from 'rxjs/operators';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';
import { DownloadQuoteComponent, DownloadQuoteDialogData } from '../../components/download-quote/download-quote.component';

export enum Messages {
  PROCESSING = 'Your request is processing, please wait.',
  ERROR = "Can't complete your request, please try again.",
  SUCCESS = 'Your items were added to the cart.',
}

@Component({
  selector: 'app-submitted-quote-details',
  templateUrl: './submitted-quote-details.component.html',
  styleUrls: ['./submitted-quote-details.component.scss'],
})
export class SubmittedQuoteDetailsComponent implements OnInit, OnDestroy {
  public regularErrorMessage: string = `Can't complete your request, please try again.`;
  public currencyConflictErrorMessage: string = `Item(s) cannot be added to the shopping cart due to currency conflicts, before you try again make sure both have the same currency.`;
  public quoteLineItems$: Observable<Array<IQuoteLineItem>>;
  public quoteLineItems: Array<IQuoteLineItem>;
  public quoteDetails$: Observable<IQuoteDetailsResponse>;
  public quoteDetails: IQuoteDetailsResponse;
  public billToId$: Observable<number>;
  public billToId: number;
  public shoppingCartId$: Observable<string>;
  public shoppingCartId: string;
  public currencyCode$: Observable<string>;
  public currencyCode: string;
  public userId$: Observable<string>;
  public userId: string;
  public cartMaxLineItems$: Observable<number>;
  public cartMaxLineItems: number;
  public cartCurrentLineItems$: Observable<number>;
  public cartCurrentLineItems: number;
  public quoteStatus: string;
  public form: FormGroup;
  public quoteHeaderId: number = this.getQuoteHeaderId();
  public loading$: Observable<boolean>;
  public limit$: Observable<number>;
  public page$: Observable<number>;
  public totalPages$: Observable<number>;
  public error$: Observable<Error>;
  public serverError: any = null;
  public showError: boolean = false;
  public search$: Observable<any>;
  public submittedSearch: string;
  public privateFeatureFlags$: Observable<any>;
  public privateFeatureFlags: any;
  public cartCurrency$: Observable<string>;
  public cartCurrency: string;
  public customerType$: Observable<string>;
  public customerType: string;

  private subscription: Subscription = new Subscription();
  public wsLineItems: Client;
  public quoteWSTopic: StompSubscription;

  private wsCapCartErrorMessage: string = 'Insertion exceeds max number';
  public expirationLimit: number = 7;

  constructor(
    public store: Store<IAppState>,
    public formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private wssService: WSSService,
    public toastService: ToastService,
    public dialogService: DialogService
  ) {
    this.quoteLineItems$ = this.store.pipe(select(getQuoteLineItems));
    this.quoteDetails$ = this.store.pipe(select(getQuoteDetails));
    this.loading$ = this.store.pipe(select(getLoading));
    this.limit$ = store.pipe(select(getLimit));
    this.page$ = this.store.pipe(select(getPage));
    this.totalPages$ = this.store.pipe(select(getTotalPages));
    this.error$ = this.store.pipe(select(getError));
    this.search$ = this.store.pipe(select(getSearchQuery));
    this.privateFeatureFlags$ = store.pipe(select(getPrivateFeatureFlagsSelector));
    this.billToId$ = this.store.pipe(select(getUserBillToAccount));
    this.shoppingCartId$ = this.store.pipe(select(getShoppingCartId));
    this.currencyCode$ = this.store.pipe(select(getCurrencyCode));
    this.userId$ = this.store.pipe(select(getUserEmail));
    this.cartMaxLineItems$ = this.store.pipe(select(getCartMaxLineItems));
    this.cartCurrentLineItems$ = this.store.pipe(select(getCartItemsCount));
    this.cartCurrency$ = this.store.pipe(select(getCartCurrency));
    this.customerType$ = this.store.pipe(select(getCustomerType));
  }

  ngOnInit(): void {
    this.startSubscriptions();
    this.setUpForm();
  }

  ngOnDestroy(): void {
    this.stopSubscriptions();
  }

  private startSubscriptions(): void {
    this.subscription.add(this.featureFlagSubscription());
    this.subscription.add(this.activatedRouteSubscription());
    this.subscription.add(this.quoteLineItemsSubscription());
    this.subscription.add(this.errorSubscription());
    this.subscription.add(this.searchSubscription());
    this.subscription.add(this.billToIdSubscription());
    this.subscription.add(this.shoppingCartIdSubscription());
    this.subscription.add(this.currencyCodeSubscription());
    this.subscription.add(this.userIdSubscription());
    this.subscription.add(this.cartMaxLineItemsSubscription());
    this.subscription.add(this.cartCurrentLineItemsSubscription());
    this.subscription.add(this.cartCurrencySubscription());
    this.subscription.add(this.wsClientStateSubscription());
    this.subscription.add(this.customerTypeSubscription());
    this.subscription.add(this.cartCurrencySubscription());
    this.subscribeToWSTopic(this.quoteHeaderId);
  }

  private stopSubscriptions(): void {
    this.subscription.unsubscribe();
    this.unsubscribeToWSTopic();
  }

  private customerTypeSubscription(): Subscription {
    return this.customerType$.subscribe(customerType => this.setCustomerType(customerType));
  }

  private featureFlagSubscription(): Subscription {
    return this.privateFeatureFlags$.subscribe(featureFlags => this.setFeatureFlags(featureFlags));
  }

  private cartCurrencySubscription(): Subscription {
    return this.cartCurrency$.subscribe(cartCurrency => this.setCartCurrency(cartCurrency));
  }

  private activatedRouteSubscription(): Subscription {
    return this.activatedRoute.params.subscribe(value => this.handleActivatedRoute(value));
  }

  private quoteLineItemsSubscription(): Subscription {
    return this.quoteLineItems$.subscribe(quoteLineItems => {
      this.setQuoteLineItems(quoteLineItems);
    });
  }

  private quoteDetailsSubscription(): Subscription {
    return this.quoteDetails$
      .pipe(
        skip(1),
        take(1)
      )
      .subscribe(quoteDetails => {
        this.setQuoteDetails(quoteDetails);
        this.setQuoteStatus(quoteDetails.status);
      });
  }

  private errorSubscription(): Subscription {
    return this.error$.subscribe(error => this.setError(error));
  }

  private searchSubscription(): Subscription {
    return this.search$.subscribe(search => this.setSearch(search));
  }

  private billToIdSubscription(): Subscription {
    return this.billToId$.subscribe(billToId => this.setBillToId(billToId));
  }

  private shoppingCartIdSubscription(): Subscription {
    return this.shoppingCartId$.subscribe(shoppingCartId => this.setShoppingCartId(shoppingCartId));
  }

  private currencyCodeSubscription(): Subscription {
    return this.currencyCode$.subscribe(currencyCode => this.setCurrencyCode(currencyCode));
  }

  private userIdSubscription(): Subscription {
    return this.userId$.subscribe(userId => this.setUserId(userId));
  }

  private cartMaxLineItemsSubscription(): Subscription {
    return this.cartMaxLineItems$.subscribe(cartMaxLineItem => this.setCartMaxLineItems(cartMaxLineItem));
  }

  private cartCurrentLineItemsSubscription(): Subscription {
    return this.cartCurrentLineItems$.subscribe(cartCurrentLineItem => this.setCartCurrentLineItems(cartCurrentLineItem));
  }

  public handleActivatedRoute(value): void {
    this.subscription.add(this.quoteDetailsSubscription());

    this.pageLimitChange(10);
    this.pageChange(1);
    this.lineItemSearch({ searchText: '', id: this.quoteHeaderId });

    this.store.dispatch(new RequestQuoteDetails(this.quoteHeaderId));
    this.store.dispatch(new ChangeSort([{ key: '', order: ISortCriteronOrderEnum.ASC }]));
  }

  public setCustomerType(_customerType: string): void {
    this.customerType = _customerType;
  }

  public setFeatureFlags(featureFlag: object): void {
    this.privateFeatureFlags = featureFlag;
  }

  public setCartCurrency(cartCurrency: string): void {
    this.cartCurrency = cartCurrency;
  }

  public setQuoteLineItems(quoteLineItems: Array<IQuoteLineItem>): void {
    this.quoteLineItems = quoteLineItems;
  }

  public setQuoteDetails(quoteDetails: IQuoteDetailsResponse): void {
    this.quoteDetails = quoteDetails;
  }

  public setQuoteStatus(status: string): void {
    this.quoteStatus = status;
  }

  public setError(error: Error): void {
    this.serverError = error;
  }

  public setSearch(search: any): void {
    this.submittedSearch = search;
  }

  public setBillToId(billToId): void {
    this.billToId = billToId;
  }

  public setShoppingCartId(shoppingCartId): void {
    this.shoppingCartId = shoppingCartId;
  }

  public setCurrencyCode(currencyCode): void {
    this.currencyCode = currencyCode;
  }

  public setUserId(userId): void {
    this.userId = userId;
  }

  public setCartCurrentLineItems(cartCurrentLineItems): void {
    this.cartCurrentLineItems = cartCurrentLineItems;
  }

  public setCartMaxLineItems(cartMaxLineItems): void {
    this.cartMaxLineItems = cartMaxLineItems;
  }

  public pageChange(page: number): void {
    this.store.dispatch(new ChangePage(page));
  }

  public pageLimitChange(pageLimit: number): void {
    this.store.dispatch(new ChangePageLimit(pageLimit));
  }

  public lineItemSearch(searchQuery): void {
    this.store.dispatch(new SearchLineItems(searchQuery));
  }

  public wsClientStateSubscription() {
    return this.wssService.state$.subscribe((state: WSSState) => this.handleWSClientState(state));
  }

  public handleWSClientState(state: WSSState) {
    switch (state) {
      case WSSState.CONNECTED:
        this.onWSClientConnects(this.quoteHeaderId);
        break;
      case WSSState.DISCONNECTED:
        this.onWSClientDisconnects();
        break;
    }
  }

  public onWSClientConnects(headerId: number): void {
    this.subscribeToWSTopic(headerId);
  }

  public onWSClientDisconnects(): void {
    this.quoteWSTopic = undefined;
  }

  public subscribeToWSTopic(headerId: number) {
    if (this.wssService.isClientConnected() && headerId && !this.quoteWSTopic) {
      this.wsLineItems = this.wssService.client;
      this.quoteWSTopic = this.wssService.client.subscribe(`/quotes/queue/${headerId}/quoteEvent`, (message: Message) => {
        const quoteLineItem: IQuoteLineItemsToAddWSResponse = JSON.parse(message.body);
        this.handleQuotedCartItemInStore(quoteLineItem);
      });
    }
  }

  public unsubscribeToWSTopic() {
    if (this.wssService.isClientConnected() && this.quoteWSTopic) {
      this.quoteWSTopic.unsubscribe();
    }
  }

  public handleQuotedCartItemInStore(response: IQuoteLineItemsToAddWSResponse): void {
    if (this.shouldHandleWSMessageFailure(response)) {
      this.handleWSMessageFailure(response);
    } else {
      this.handleWSMessageSuccess(response);
    }
  }

  private shouldHandleWSMessageFailure(response: IQuoteLineItemsToAddWSResponse): boolean {
    return response.eventCode === QuoteWSMessageEventCodes.FAILED;
  }

  public handleWSMessageFailure(response: IQuoteLineItemsToAddWSResponse): void {
    const { eventMessage } = response;

    if (this.isWSCartCapError(eventMessage)) {
      this.showLimitedItemsToAddDialog();
    } else {
      this.handleWSMessageError();
    }
  }

  public handleWSMessageError(): void {
    const error = new Error(this.regularErrorMessage);

    this.showErrorToast(this.regularErrorMessage);
    this.store.dispatch(new ReceiveWSAddToCartResponseFailed(error));
  }

  private isWSCartCapError(message: string): boolean {
    return message.includes(this.wsCapCartErrorMessage);
  }

  private setLineItemStatusToInCart(lineItem: IQuoteLineItem): IQuoteLineItem {
    lineItem.status = QuoteLineItemStatus.IN_CART;
    lineItem.quoteLineStatus = QuoteLineItemCartsStatus.IN_CART;

    return lineItem;
  }

  private setLineItemStatusToPurchased(lineItem: IQuoteLineItem): IQuoteLineItem {
    lineItem.status = QuoteLineItemStatus.PURCHASED;
    lineItem.quoteLineStatus = QuoteLineItemCartsStatus.PURCHASED;

    return lineItem;
  }

  private findLineItemReturnedFromMessage(response: IQuoteLineItemsToAddWSResponse): IQuoteLineItem {
    return this.quoteLineItems.find(quoteLineItem => quoteLineItem.quoteLineId === response.quoteLineId);
  }

  private findIndexOfLineItemReturnedFromMessage(lineItem: IQuoteLineItem): number {
    return this.quoteLineItems.findIndex(quoteLineItem => quoteLineItem.quoteLineId === lineItem.quoteLineId);
  }

  public dispatchActionsAfterMessageSuccess(index: number, lineItem: IQuoteLineItem, response: IQuoteLineItemsToAddWSResponse): void {
    this.store.dispatch(new UpdateSubmitedLineItemsState({ index, lineItem }));
    this.store.dispatch(new ReceiveWSAddToCartResponseSuccess(response));
  }

  public buildLineItemWithUpdatedStatus(lineItem: IQuoteLineItem, status: string): IQuoteLineItem {
    switch (status) {
      case QuoteLineItemCartsStatus.IN_CART: {
        return this.setLineItemStatusToInCart(lineItem);
      }
      case QuoteLineItemCartsStatus.PURCHASED: {
        return this.setLineItemStatusToPurchased(lineItem);
      }

      default: {
        return lineItem;
      }
    }
  }

  public handleWSMessageSuccess(response: IQuoteLineItemsToAddWSResponse): void {
    const lineItem: IQuoteLineItem = this.findLineItemReturnedFromMessage(response);
    const index: number = this.findIndexOfLineItemReturnedFromMessage(lineItem);
    const updatedLineItem = this.buildLineItemWithUpdatedStatus(lineItem, response.eventCode);

    this.dispatchActionsAfterMessageSuccess(index, updatedLineItem, response);
    this.handleRefreshCartsAfterSubscription();
  }

  public handleRefreshCartsAfterSubscription(): void {
    this.store.dispatch(new GetCarts());
    this.refreshTable();
    this.showToast(Messages.SUCCESS);
  }

  private findLineItemIndex(id: number): number {
    return this.quoteLineItems.findIndex(quoteLineItem => quoteLineItem.quoteLineId === id);
  }

  private getAllLineItemsInCart(): Array<IQuoteLineItem> {
    return this.quoteLineItems.filter(lineItem => lineItem.status === QuoteLineItemStatus.IN_CART);
  }

  private uncheckLineItemsAddedToCart(lineItemsToUncheck: Array<IQuoteLineItem>): void {
    if (lineItemsToUncheck) {
      lineItemsToUncheck.forEach(lineItem => {
        if (lineItem.checked) {
          const id = lineItem.quoteLineId;
          const index: number = this.findLineItemIndex(id);
          this.store.dispatch(new ToggleCheckQuotedDetailLineItem({ index, checked: false }));
        }
      });
    }
  }

  public cleanRecentlyAddedLineItemStatus(): void {
    const lineItemsInCart = this.getAllLineItemsInCart();
    this.uncheckLineItemsAddedToCart(lineItemsInCart);
  }

  public refreshTable(): void {
    this.cleanRecentlyAddedLineItemStatus();
    this.subscription.add(this.quoteLineItemsSubscription());
  }

  public setUpForm(): void {
    this.form = this.formBuilder.group({
      searchQuery: [null, Validators.compose([Validators.required])],
    });
  }

  public checkForm(): void {
    this.showError = !this.form.valid;
    this.serverError = null;

    /* istanbul ignore else */
    if (this.form.valid) {
      this.lineItemSearch({ searchText: this.form.get('searchQuery').value.trim(), id: this.quoteHeaderId });
      this.pageChange(1);
    }
  }

  public resetForm(): void {
    this.form.reset();
    this.lineItemSearch({ searchText: '', id: this.quoteHeaderId });
    this.pageChange(1);
  }

  public getAllQuotedLineItems(): Array<IQuoteLineItem> {
    return this.quoteLineItems.filter(lineItem => lineItem.status === QuoteLineItemStatus.QUOTED);
  }

  public isAnyLineItemChecked(): boolean {
    return this.getAllQuotedLineItems().every(lineItem => !lineItem.hasOwnProperty('checked') || lineItem.checked === false);
  }

  public shouldDisableAddToCartButton(): boolean {
    if (this.quoteLineItems) {
      return this.isAnyLineItemChecked();
    } else {
      return true;
    }
  }

  public dateDiffInDays(date1, date2): number {
    const firstDate = new Date(date1);
    const secondDate = new Date(date2);
    return Math.floor(
      (Date.UTC(secondDate.getFullYear(), secondDate.getMonth(), secondDate.getDate()) -
        Date.UTC(firstDate.getFullYear(), firstDate.getMonth(), firstDate.getDate())) /
        (1000 * 60 * 60 * 24)
    );
  }

  public shouldDisplayExpirationAlert(): boolean {
    if (this.quoteDetails) {
      const expiry = new Date(this.quoteDetails.expiryDate);
      const current = new Date();
      const daysOfDifference = this.dateDiffInDays(current, expiry);
      return daysOfDifference < this.expirationLimit && daysOfDifference > 0;
    } else {
      return false;
    }
  }

  private getQuoteHeaderId(): number {
    const currentURL: string = window.location.href;
    return Number(currentURL.substr(currentURL.lastIndexOf('/') + 1));
  }

  public getQuoteLineItemsReadyForRequest(): Array<IQuotedLineItemToAdd> {
    return this.quoteLineItems
      .filter(lineItem => lineItem.checked)
      .map(lineItem => {
        return {
          quoteHeaderId: this.quoteHeaderId,
          quoteNumber: this.quoteDetails.quoteNumber,
          quoteLineId: lineItem.quoteLineId,
        };
      });
  }

  public buildAddToCartRequest(): IQuotedRequestToAdd {
    return {
      billToId: this.billToId,
      currency: this.currencyCode,
      lineItems: this.getQuoteLineItemsReadyForRequest(),
      shoppingCartId: this.shoppingCartId,
      userId: this.userId,
    };
  }

  public shouldAddToCart(): boolean {
    const itemsChecked = this.quoteLineItems.filter(lineItem => lineItem.checked).length;
    const totalItems = this.cartCurrentLineItems + itemsChecked;

    return totalItems <= this.cartMaxLineItems && this.doesCurrenciesMatch();
  }

  public addToCart(): void {
    this.shouldAddToCart() ? this.startRequestAddToCart() : this.showErrors();
  }

  public getQuoteLineItemsForRequestTotalPrice(): number {
    let total: number = 0;
    this.quoteLineItems
      .filter(lineItem => lineItem.checked)
      .forEach(lineItem => {
        total += lineItem.quotedPrice;
      });
    return total;
  }

  public buildAnalyticsRequest(): IQuotedProductsToAdd {
    return {
      quantity: this.getQuoteLineItemsReadyForRequest().length,
      totalPrice: this.getQuoteLineItemsForRequestTotalPrice(),
    };
  }

  public startRequestAddToCart(): void {
    const request: IQuotedRequestToAdd = this.buildAddToCartRequest();
    const analyticsRequest: IQuotedProductsToAdd = this.buildAnalyticsRequest();
    this.store.dispatch(new RequestAddQuotedLineItemToShoppingCart(request));
    this.store.dispatch(new RequestProductsLoggedOnAnalytics(analyticsRequest));
    this.showToast(Messages.PROCESSING);
  }

  public showToast(message: string): void {
    this.toastService.hideToast();
    this.toastService.showToast(message);
  }

  public showErrorToast(message: string): void {
    this.toastService.hideToast();
    this.toastService.showErrorToast(message);
  }

  public showLimitedItemsToAddDialog(): void {
    this.toastService.hideToast();
    this.dialogService.open(AddToCartDialogComponent, {
      size: 'small',
    });
  }

  public showErrors(): void {
    this.doesCurrenciesMatch() ? this.showLimitedItemsToAddDialog() : this.toastService.showErrorToast(this.currencyConflictErrorMessage);
  }

  public shouldDisplayError(): boolean {
    return this.serverError.message !== this.regularErrorMessage;
  }

  public doesCurrenciesMatch(): boolean {
    return this.quoteDetails.quotedCurrency === this.cartCurrency;
  }

  public openDownloadQuoteDialog() {
    const data: DownloadQuoteDialogData = {
      quoteHeaderId: this.quoteHeaderId,
      quoteNumber: this.quoteDetails.quoteNumber,
    };

    this.dialogService.open(DownloadQuoteComponent, { size: 'medium', data });
  }
}
