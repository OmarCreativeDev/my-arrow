import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubmittedQuotesComponent } from '@app/features/submitted-quotes/pages/submitted-quotes/submitted-quotes.component';
import { SubmittedQuoteDetailsComponent } from '@app/features/submitted-quotes/pages/submitted-quote-details/submitted-quote-details.component';

const routes: Routes = [
  {
    path: '',
    component: SubmittedQuotesComponent,
    data: { title: 'Quotes' },
  },
  {
    path: ':quoteId',
    component: SubmittedQuoteDetailsComponent,
    data: { title: 'View Quote' },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SubmittedQuotesRoutingModule {}
