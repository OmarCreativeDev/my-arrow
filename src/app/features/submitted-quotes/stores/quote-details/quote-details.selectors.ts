import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IQuoteDetailsState } from 'app/core/quotes/quotes.interfaces';

/**
 * Selects the quotes state from the root state object
 */
export const getQuoteDetailsState = createFeatureSelector<IQuoteDetailsState>('quoteDetails');

/**
 * Retrieve the number of items that should show per page
 */
export const getLimit = createSelector(getQuoteDetailsState, quoteDetailsState => quoteDetailsState.pagination.limit);

/**
 * Retrieve the current page
 */
export const getPage = createSelector(getQuoteDetailsState, quoteDetailsState => quoteDetailsState.pagination.current);

/**
 * Retrieve the number of total pages
 */
export const getTotalPages = createSelector(getQuoteDetailsState, quoteDetailsState => quoteDetailsState.pagination.total);

/**
 * Retrieve the quote line items
 */
export const getQuoteLineItems = createSelector(getQuoteDetailsState, quoteDetailsState => quoteDetailsState.lineItems);

/**
 * Retrieve the quote details
 */
export const getQuoteDetails = createSelector(getQuoteDetailsState, quoteDetailsState => quoteDetailsState.quoteDetails);

/**
 * Retrieves the loading state
 */
export const getLoading = createSelector(getQuoteDetailsState, quoteDetailsState => quoteDetailsState.loading);

/**
 * Retrieve the error state of the page
 */
export const getError = createSelector(getQuoteDetailsState, quoteDetailsState => quoteDetailsState.error);

/**
 * Retrieve the sorting of items
 */
export const getSort = createSelector(getQuoteDetailsState, quoteDetailsState => quoteDetailsState.sort);

/**
 * Retrieves the search
 */
export const getSearchQuery = createSelector(getQuoteDetailsState, quoteDetailsState => quoteDetailsState.searchText);
