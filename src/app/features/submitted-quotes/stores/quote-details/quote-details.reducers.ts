import { QuoteDetailsActionTypes, QuoteDetailsActions } from './quote-details.actions';
import { IQuoteDetailsState } from 'app/core/quotes/quotes.interfaces';
import { cloneDeep } from 'lodash';

export const INITIAL_QUOTE_DETAILS_STATE: IQuoteDetailsState = {
  loading: true,
  error: null,
  pagination: {
    limit: 10,
    current: 1,
    total: 0,
  },
  searchText: '',
  sort: [],
  lineItems: [],
  quoteDetails: null,
  quotesPurchased: [],
  productInformationRequestedToLog: null,
};

export function quoteDetailsReducer(state: IQuoteDetailsState = INITIAL_QUOTE_DETAILS_STATE, action: QuoteDetailsActions) {
  switch (action.type) {
    case QuoteDetailsActionTypes.CHANGE_PAGE_LIMIT: {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          limit: action.payload,
        },
      };
    }

    case QuoteDetailsActionTypes.CHANGE_PAGE: {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          current: action.payload,
        },
      };
    }

    case QuoteDetailsActionTypes.SEARCH_LINE_ITEMS: {
      return {
        ...state,
        searchText: action.payload,
      };
    }

    case QuoteDetailsActionTypes.QUERY: {
      return {
        ...state,
        error: false,
        loading: true,
        lineItems: null,
      };
    }

    case QuoteDetailsActionTypes.QUERY_APPEND_DETAILS: {
      return {
        ...state,
        lineItems: action.payload.lineItems,
      };
    }

    case QuoteDetailsActionTypes.QUERY_COMPLETE: {
      return {
        ...state,
        lineItems: action.payload.lineItems,
        pagination: {
          ...state.pagination,
          current: action.payload.pagination ? action.payload.pagination.page : INITIAL_QUOTE_DETAILS_STATE.pagination.current,
          total: action.payload.pagination ? action.payload.pagination.total : INITIAL_QUOTE_DETAILS_STATE.pagination.total,
        },
        loading: false,
        error: false,
      };
    }

    case QuoteDetailsActionTypes.QUERY_ERROR: {
      return {
        ...state,
        error: action.payload,
        loading: false,
        lineItems: null,
      };
    }

    case QuoteDetailsActionTypes.CHANGE_SORT: {
      return {
        ...state,
        sort: action.payload ? action.payload : [],
      };
    }

    case QuoteDetailsActionTypes.GET_QUOTE_DETAILS_SUCCESS: {
      return {
        ...state,
        quoteDetails: action.payload,
      };
    }

    case QuoteDetailsActionTypes.TOGGLE_CHECK_QUOTED_DETAIL_LINE_ITEM: {
      const { index, checked } = action.payload;
      const lineItem = cloneDeep(state.lineItems[index]);

      return {
        ...state,
        lineItems: [
          ...state.lineItems.slice(0, index),
          {
            ...lineItem,
            checked,
          },
          ...state.lineItems.slice(index + 1),
        ],
      };
    }

    case QuoteDetailsActionTypes.TOGGLE_ALL_QUOTED_DETAIL_LINE_ITEMS: {
      return {
        ...state,
        lineItems: action.payload,
      };
    }

    case QuoteDetailsActionTypes.REQUEST_ADD_QUOTED_LINE_ITEMS_TO_SHOPPING_CART: {
      return {
        ...state,
        error: null,
        loading: true,
      };
    }

    case QuoteDetailsActionTypes.REQUEST_ADD_QUOTED_LINE_ITEMS_TO_SHOPPING_CART_SUCCESS: {
      return {
        ...state,
        error: null,
        loading: false,
      };
    }

    case QuoteDetailsActionTypes.REQUEST_ADD_QUOTED_LINE_ITEMS_TO_SHOPPING_CART_FAILED: {
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    }

    case QuoteDetailsActionTypes.RECEIVE_WS_ADD_TO_CART_RESPONSE_FAILED: {
      return {
        ...state,
        error: action.payload,
      };
    }

    case QuoteDetailsActionTypes.RECEIVE_WS_ADD_TO_CART_RESPONSE_SUCCESS: {
      return {
        ...state,
        quotesPurchased: [...state.quotesPurchased, action.payload],
      };
    }

    case QuoteDetailsActionTypes.UPDATE_SUBMITED_LINE_ITEMS_STATE: {
      const { index, lineItem } = action.payload;
      return {
        ...state,
        lineItems: [
          ...state.lineItems.slice(0, index),
          {
            ...lineItem,
          },
          ...state.lineItems.slice(index + 1),
        ],
      };
    }

    case QuoteDetailsActionTypes.REQUEST_PRODUCTS_LOGGED_ON_ANALYTICS: {
      return {
        ...state,
        productInformationRequestedToLog: action.payload,
      };
    }

    default:
      return state;
  }
}
