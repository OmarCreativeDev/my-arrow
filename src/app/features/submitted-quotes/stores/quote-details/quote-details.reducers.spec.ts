import {
  ChangePageLimit,
  ChangePage,
  SearchLineItems,
  Query,
  QueryAppendDetails,
  QueryComplete,
  QueryError,
  ChangeSort,
  GetQuoteDetailsSuccess,
  ToggleCheckQuotedDetailLineItem,
  ToggleAllQuotedDetailLineItems,
  RequestAddQuotedLineItemToShoppingCart,
  RequestAddQuotedLineItemToShoppingCartSuccess,
  RequestAddQuotedLineItemToShoppingCartFailed,
  ReceiveWSAddToCartResponseSuccess,
  ReceiveWSAddToCartResponseFailed,
  UpdateSubmitedLineItemsState,
  RequestProductsLoggedOnAnalytics,
} from './quote-details.actions';

import { quoteDetailsReducer, INITIAL_QUOTE_DETAILS_STATE } from './quote-details.reducers';
import { IQuoteDetailsState } from '@app/core/quotes/quotes.interfaces';
import { HttpErrorResponse } from '@angular/common/http';

describe('Quote Details Reducer', () => {
  let initialState: IQuoteDetailsState;

  const mockedQuoteQueryRequest = {
    searchText: 'SearchText',
    id: 1,
  };

  const mockedQuoteLineItem = {
    cpn: 'cpn',
    mpn: 'mpn',
    docId: 'docId',
    foh: 1,
    leadtime: 'leadtime',
    manufacturer: 'manufacturer',
    moq: 1,
    multOrdQty: 1,
    notes: 'notes',
    partNumber: 'partNumber',
    quantity: 1,
    quotedPrice: 1,
    quoteLineId: 1,
    quoteLineStatus: 'quoteLineStatus',
    quoteLineNumber: 'quoteLineNumber',
    status: 'status',
    targetPrice: 1,
    totalPrice: 1,
    checked: true,
    quoteHeaderId: 12345,
    quoteNumber: '12345',
  };

  const mockedQuoteQueryResponseSuccess = {
    pagination: {
      page: 10,
      limit: 10,
      current: 10,
      total: 10,
    },
    lineItems: [mockedQuoteLineItem],
  };

  const mockedQuoteQueryResponseDefault = {
    pagination: undefined,
    lineItems: [mockedQuoteLineItem],
  };

  const mockedPaginationResponseSuccess = {
    limit: 10,
    current: 10,
    total: 10,
  };

  const mockedPaginationResponseDefault = {
    limit: 10,
    current: 1,
    total: 0,
  };

  const mockError = new HttpErrorResponse({});

  const mockedQuoteDetails = {
    accountName: 'string',
    accountNumber: 'string',
    billTo: {
      addressLine1: 'string',
      addressLine2: 'string',
      addressLine3: 'string',
      city: 'string',
      country: 'string',
      id: 12345,
      name: 'string',
      postCode: 'string',
      state: 'string',
    },
    expiryDate: '12/12/2018',
    internalSalesRep: 'string',
    itemsQuoted: 12345,
    owner: 'string',
    quoteNumber: '12345',
    quoteType: 'string',
    quotedCurrency: 'string',
    referenceNumber: 'string',
    shipTo: {
      addressLine1: 'string',
      addressLine2: 'string',
      addressLine3: 'string',
      city: 'string',
      country: 'string',
      id: 12345,
      name: 'string',
      postCode: 'string',
      state: 'string',
    },
    status: 'string',
    submittedDate: 'string',
    terms: 'string',
    totalCost: 12345,
    totalItems: 12345,
  };

  const mockedRequestToAdd = {
    billToId: 1,
    currency: 'string',
    lineItems: [mockedQuoteLineItem],
    shoppingCartId: 'string',
    userId: 'string',
  };

  const mockQuoteLineItemsToAddWSResponse = {
    quoteHeaderId: 1,
    quoteNumber: 'string',
    quoteLineId: 1,
    quoteLineNumber: 'string',
    eventCode: 'string',
    eventMessage: 'string',
  };

  beforeEach(() => {
    initialState = INITIAL_QUOTE_DETAILS_STATE;
  });

  it('`CHANGE_PAGE_LIMIT` should set pagination limit property according to the parameter sent on the action', () => {
    const action = new ChangePageLimit(10);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.pagination.limit).toBe(10);
  });

  it('`CHANGE_PAGE` should set pagination current property according to the parameter sent on the action', () => {
    const action = new ChangePage(10);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.pagination.current).toBe(10);
  });

  it('`SEARCH_LINE_ITEMS` should set pagination searchText property according to the parameter sent on the action', () => {
    const action = new SearchLineItems(mockedQuoteQueryRequest);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.searchText).toBe(mockedQuoteQueryRequest);
  });

  it('`QUERY` should set error property to false, loading property to true, and lineItems property to null', () => {
    const action = new Query(mockedQuoteQueryRequest);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeFalsy();
    expect(result.lineItems).toBeNull();
  });

  it('`QUERY_APPEND_DETAILS` should update lineItems', () => {
    const action = new QueryAppendDetails(mockedQuoteQueryResponseSuccess);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.lineItems).toEqual(mockedQuoteQueryResponseSuccess.lineItems);
  });

  it('`QUERY_COMPLETE` should update all query, set error and loading properties to false, and set pagination properties to parameters,', () => {
    const action = new QueryComplete(mockedQuoteQueryResponseSuccess);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.lineItems).toEqual(mockedQuoteQueryResponseSuccess.lineItems);
    expect(result.pagination).toEqual(mockedPaginationResponseSuccess);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeFalsy();
  });

  it('`QUERY_COMPLETE` should update all query, set error and loading properties to false, and set pagination properties to defaults', () => {
    const action = new QueryComplete(mockedQuoteQueryResponseDefault);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.lineItems).toEqual(mockedQuoteQueryResponseDefault.lineItems);
    expect(result.pagination).toEqual(mockedPaginationResponseDefault);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeFalsy();
  });

  it('`QUERY_ERROR` should return error, set loading on false, and error property to true', () => {
    const action = new QueryError(mockError);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.error).toEqual(mockError);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeTruthy();
  });

  it('`CHANGE_SORT` should set sort property according to parameter sent by action', () => {
    const action = new ChangeSort([]);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.sort).toEqual([]);
  });

  it('`CHANGE_SORT` should set sort property according to sort default', () => {
    const action = new ChangeSort(undefined);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.sort).toEqual([]);
  });

  it('`GET_QUOTE_DETAILS_SUCCESS` should update quote details', () => {
    const action = new GetQuoteDetailsSuccess(mockedQuoteDetails);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.quoteDetails).toEqual(mockedQuoteDetails);
  });

  it('`TOGGLE_CHECK_QUOTED_DETAIL_LINE_ITEM` should update quote details', () => {
    const mockCheckQuotedDetailLineItem = { index: 0, checked: true };
    const action = new ToggleCheckQuotedDetailLineItem(mockCheckQuotedDetailLineItem);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.quoteDetails).toEqual(null);
  });

  it('`TOGGLE_ALL_QUOTED_DETAIL_LINE_ITEMS` should update all line items', () => {
    const mockedLineItems = [mockedQuoteLineItem];
    mockedLineItems[0].status = 'Quoted';
    const action = new ToggleAllQuotedDetailLineItems(mockedLineItems);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.lineItems).toEqual(mockedLineItems);
  });

  it('`REQUEST_ADD_QUOTED_LINE_ITEMS_TO_SHOPPING_CART` should set error in false, and loading to true', () => {
    const action = new RequestAddQuotedLineItemToShoppingCart(mockedRequestToAdd);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeFalsy();
  });

  it('`REQUEST_ADD_QUOTED_LINE_ITEMS_TO_SHOPPING_CART_SUCCESS` should set error in false, and loading to false', () => {
    const action = new RequestAddQuotedLineItemToShoppingCartSuccess();
    const result = quoteDetailsReducer(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toBeFalsy();
  });

  it('`REQUEST_ADD_QUOTED_LINE_ITEMS_TO_SHOPPING_CART_FAILED` should update error, and set loading to false', () => {
    const action = new RequestAddQuotedLineItemToShoppingCartFailed(mockError);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.error).toEqual(mockError);
  });

  it('`RECEIVE_WS_ADD_TO_CART_RESPONSE_FAILED` should update error', () => {
    const action = new ReceiveWSAddToCartResponseFailed(mockError);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.error).toEqual(mockError);
  });

  it('`RECEIVE_WS_ADD_TO_CART_RESPONSE_SUCCESS` should update quotesPurchased', () => {
    const action = new ReceiveWSAddToCartResponseSuccess(mockQuoteLineItemsToAddWSResponse);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.quotesPurchased).toEqual([mockQuoteLineItemsToAddWSResponse]);
  });

  it('`UPDATE_SUBMITED_LINE_ITEMS_STATE` should update line items submitted', () => {
    const mockPayload = { index: 0, lineItem: mockedQuoteLineItem };
    const action = new UpdateSubmitedLineItemsState(mockPayload);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.lineItems).toEqual([mockedQuoteLineItem]);
  });

  it('`REQUEST_PRODUCTS_LOGGED_ON_ANALYTICS` should update productInformationRequestedToLog', () => {
    const mockPayload = { quantity: 1, totalPrice: 1.1 };
    const action = new RequestProductsLoggedOnAnalytics(mockPayload);
    const result = quoteDetailsReducer(initialState, action);
    expect(result.productInformationRequestedToLog).toEqual(mockPayload);
  });
});
