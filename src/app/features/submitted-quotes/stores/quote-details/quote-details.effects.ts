import { Injectable } from '@angular/core';
import { combineLatest, Observable, of } from 'rxjs';
import { debounceTime, switchMap, map, catchError, take } from 'rxjs/operators';
import { Effect, Actions, ofType } from '@ngrx/effects';
import {
  QuoteDetailsActionTypes,
  Query,
  QueryComplete,
  QueryError,
  RequestQuoteDetails,
  GetQuoteDetailsSuccess,
  GetQuoteDetailsFailed,
  QueryAppendDetails,
  RequestAddQuotedLineItemToShoppingCart,
  RequestAddQuotedLineItemToShoppingCartSuccess,
  RequestAddQuotedLineItemToShoppingCartFailed,
} from '@app/features/submitted-quotes/stores/quote-details/quote-details.actions';
import { QuotesService } from '@app/core/quotes/quotes.service';
import { HttpErrorResponse } from '@angular/common/http';
import { IQuoteLineItem } from '@app/core/quotes/quotes.interfaces';
import { InventoryService } from '@app/core/inventory/inventory.service';
import { QuoteStatus } from '@app/core/quotes/quotes.enum';

@Injectable()
export class QuoteDetailsEffects {
  /**
   * Side-effect that combines all side effects for these facets:
   *  - Search
   *  - Limit
   *  - Page
   */
  @Effect()
  facets$: Observable<any> = combineLatest(
    this.actions$.pipe(ofType(QuoteDetailsActionTypes.SEARCH_LINE_ITEMS)),
    this.actions$.pipe(ofType(QuoteDetailsActionTypes.CHANGE_PAGE)),
    this.actions$.pipe(ofType(QuoteDetailsActionTypes.CHANGE_PAGE_LIMIT)),
    this.actions$.pipe(ofType(QuoteDetailsActionTypes.CHANGE_SORT))
  ).pipe(
    debounceTime(50),
    map((actions: any) => {
      const payloads = actions.map(action => action.payload);

      return new Query({
        pagination: {
          page: payloads[1],
          limit: payloads[2],
        },
        searchText: payloads[0].searchText,
        sorting: {
          order: payloads[3][0].order,
          field: payloads[3][0].key,
        },
        id: payloads[0].id,
      });
    })
  );

  /**
   * Side-effect for the Query being changed directly. Trigger by the facets$ Side-effect
   */
  @Effect()
  query$: Observable<any> = this.actions$.pipe(
    ofType(QuoteDetailsActionTypes.QUERY),
    switchMap((action: Query) => {
      return this.quotesService.getQuoteLineItems(action.payload).pipe(
        map((queryResponse: any) => new QueryAppendDetails(queryResponse)),
        catchError((errorResponse: HttpErrorResponse) => of(new QueryError(errorResponse)))
      );
    })
  );

  @Effect()
  queryAppendDetails$: Observable<any> = this.actions$.pipe(
    ofType(QuoteDetailsActionTypes.QUERY_APPEND_DETAILS),
    map((action: QueryAppendDetails) => {
      action.payload.lineItems = action.payload.lineItems.map((lineItem: IQuoteLineItem) => {
        this.inventoryService
          .getProductDetails(lineItem.docId)
          .pipe(
            take(1),
            catchError((errorResponse: HttpErrorResponse) => {
              return of({
                availableQuantity: '- -',
                bufferQuantity: '- -',
                suppAlloc: '- -',
                pipeline: {
                  deliveryDate: null,
                },
                multipleOrderQuantity: '- -',
                ncnr: '- -',
                spq: '- -',
              });
            })
          )
          .subscribe((productDetail: any) => (lineItem.productDetail = productDetail));
        return lineItem;
      });
      return new QueryComplete(action.payload);
    })
  );

  @Effect()
  requestQuoteDetails$ = this.actions$.pipe(
    ofType(QuoteDetailsActionTypes.GET_QUOTE_DETAILS),
    switchMap((action: RequestQuoteDetails) => {
      return this.quotesService.getQuoteDetails(action.payload).pipe(
        map((quoteDetails: any) => {
          quoteDetails.totalCost = quoteDetails.status === QuoteStatus.FULLY_QUOTED ? quoteDetails.totalCost || 0 : 0;
          return new GetQuoteDetailsSuccess(quoteDetails);
        }),
        catchError(err => of(new GetQuoteDetailsFailed(err)))
      );
    })
  );

  @Effect()
  RequestAddQuotedLineItemToShoppingCart$ = this.actions$.pipe(
    ofType(QuoteDetailsActionTypes.REQUEST_ADD_QUOTED_LINE_ITEMS_TO_SHOPPING_CART),
    switchMap((action: RequestAddQuotedLineItemToShoppingCart) => {
      return this.quotesService.purchaseQuotes(action.payload).pipe(
        map(() => new RequestAddQuotedLineItemToShoppingCartSuccess()),
        catchError(err => of(new RequestAddQuotedLineItemToShoppingCartFailed(err)))
      );
    })
  );

  constructor(private actions$: Actions, private quotesService: QuotesService, private inventoryService: InventoryService) {}
}
