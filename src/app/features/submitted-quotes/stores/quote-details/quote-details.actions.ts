import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import {
  IQuotesQueryRequest,
  IQuotesQueryResponse,
  IQuoteDetailsResponse,
  ICheckQuotedDetailLineItem,
  IQuotedRequestToAdd,
  IQuoteLineItem,
  IQuoteLineItemsToAddWSResponse,
  IQuotedProductsToAdd,
} from '@app/core/quotes/quotes.interfaces';
import { ISortCriteron } from '@app/shared/shared.interfaces';

export enum QuoteDetailsActionTypes {
  CHANGE_PAGE_LIMIT = '[Quote Details] Change Page Limit',
  CHANGE_PAGE = '[Quote Details] Change page',
  SEARCH_LINE_ITEMS = '[Quote Details] Search Quote line items',
  QUERY = '[Quote Details] Query',
  QUERY_APPEND_DETAILS = '[Quote Details] Query Append Product Details',
  QUERY_COMPLETE = '[Quote Details] Query Complete',
  QUERY_ERROR = '[Quote Details] Query Error',
  CHANGE_SORT = '[Quote Details] Change Sort',
  GET_QUOTE_DETAILS = '[Quote Details] Get Quote Details',
  GET_QUOTE_DETAILS_FAILED = '[Quote Details] Get Quote Details Failed',
  GET_QUOTE_DETAILS_SUCCESS = '[Quote Details] Get Quote Details Success',
  TOGGLE_CHECK_QUOTED_DETAIL_LINE_ITEM = 'TOGGLE_CHECK_QUOTED_DETAIL_LINE_ITEM',
  TOGGLE_ALL_QUOTED_DETAIL_LINE_ITEMS = 'TOGGLE_ALL_QUOTED_DETAIL_LINE_ITEMS',
  REQUEST_ADD_QUOTED_LINE_ITEMS_TO_SHOPPING_CART = 'REQUEST_ADD_WS_QUOTED_LINE_ITEMS_TO_SHOPPING_CART',
  REQUEST_ADD_QUOTED_LINE_ITEMS_TO_SHOPPING_CART_SUCCESS = 'REQUEST_ADD_WS_QUOTED_LINE_ITEMS_TO_SHOPPING_CART_SUCCESS',
  REQUEST_ADD_QUOTED_LINE_ITEMS_TO_SHOPPING_CART_FAILED = 'REQUEST_ADD_WS_QUOTED_LINE_ITEMS_TO_SHOPPING_CART_FAILED',
  RECEIVE_WS_ADD_TO_CART_RESPONSE_SUCCESS = 'RECEIVE_WS_ADD_TO_CART_RESPONSE_SUCCESS',
  RECEIVE_WS_ADD_TO_CART_RESPONSE_FAILED = 'RECEIVE_WS_ADD_TO_CART_RESPONSE_FAILED',
  UPDATE_SUBMITED_LINE_ITEMS_STATE = 'UPDATE_SUBMITED_LINE_ITEMS_STATE',
  REQUEST_PRODUCTS_LOGGED_ON_ANALYTICS = 'REQUEST_PRODUCTS_LOGGED_ON_ANALYTICS',
}

/**
 * Changes the limit on each page
 */
export class ChangePageLimit implements Action {
  readonly type = QuoteDetailsActionTypes.CHANGE_PAGE_LIMIT;
  constructor(public payload: number) {}
}

/**
 * Change the page number
 */
export class ChangePage implements Action {
  readonly type = QuoteDetailsActionTypes.CHANGE_PAGE;
  constructor(public payload: number) {}
}

/**
 * Search for quote line items
 */
export class SearchLineItems implements Action {
  readonly type = QuoteDetailsActionTypes.SEARCH_LINE_ITEMS;
  constructor(public payload: { searchText: string; id: number }) {}
}

/**
 * Updates the Query with the provided facets
 */
export class Query implements Action {
  readonly type = QuoteDetailsActionTypes.QUERY;
  constructor(public payload: IQuotesQueryRequest) {}
}

/**
 * Append the Product Details to the Query result
 */
export class QueryAppendDetails implements Action {
  readonly type = QuoteDetailsActionTypes.QUERY_APPEND_DETAILS;
  constructor(public payload: IQuotesQueryResponse) {}
}

/**
 * Results of the Query
 */
export class QueryComplete implements Action {
  readonly type = QuoteDetailsActionTypes.QUERY_COMPLETE;
  constructor(public payload: IQuotesQueryResponse) {}
}

/**
 * Thrown if the Query received an Error
 */
export class QueryError implements Action {
  readonly type = QuoteDetailsActionTypes.QUERY_ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

/**
 * Update The sorting of the clicked column
 */
export class ChangeSort implements Action {
  readonly type = QuoteDetailsActionTypes.CHANGE_SORT;
  constructor(public payload: Array<ISortCriteron>) {}
}

/**
 * Request quote header details
 */
export class RequestQuoteDetails implements Action {
  readonly type = QuoteDetailsActionTypes.GET_QUOTE_DETAILS;
  constructor(public payload: number) {}
}

/**
 * Quote header details request failed
 */
export class GetQuoteDetailsFailed implements Action {
  readonly type = QuoteDetailsActionTypes.GET_QUOTE_DETAILS_FAILED;
  constructor(public payload: HttpErrorResponse) {}
}

/**
 * Quote header details request success
 */
export class GetQuoteDetailsSuccess implements Action {
  readonly type = QuoteDetailsActionTypes.GET_QUOTE_DETAILS_SUCCESS;
  constructor(public payload: IQuoteDetailsResponse) {}
}

/**
 * Triggers the set check of the line items
 */
export class ToggleCheckQuotedDetailLineItem implements Action {
  readonly type = QuoteDetailsActionTypes.TOGGLE_CHECK_QUOTED_DETAIL_LINE_ITEM;
  constructor(public payload: ICheckQuotedDetailLineItem) {}
}

/**
 * Triggers the set check of All the line items
 */
export class ToggleAllQuotedDetailLineItems implements Action {
  readonly type = QuoteDetailsActionTypes.TOGGLE_ALL_QUOTED_DETAIL_LINE_ITEMS;
  constructor(public payload: Array<IQuoteLineItem>) {}
}

/**
 * * Add Quoted LineItem request
 */

export class RequestAddQuotedLineItemToShoppingCart implements Action {
  readonly type = QuoteDetailsActionTypes.REQUEST_ADD_QUOTED_LINE_ITEMS_TO_SHOPPING_CART;
  constructor(public payload: IQuotedRequestToAdd) {}
}

/**
 * * Add Quoted LineItem request success
 */

export class RequestAddQuotedLineItemToShoppingCartSuccess implements Action {
  readonly type = QuoteDetailsActionTypes.REQUEST_ADD_QUOTED_LINE_ITEMS_TO_SHOPPING_CART_SUCCESS;
}

/**
 * * Add Quoted LineItem request success
 */
export class RequestAddQuotedLineItemToShoppingCartFailed implements Action {
  readonly type = QuoteDetailsActionTypes.REQUEST_ADD_QUOTED_LINE_ITEMS_TO_SHOPPING_CART_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Get Web Socket Response for add lineItems to Cart sucess
 */

export class ReceiveWSAddToCartResponseSuccess implements Action {
  readonly type = QuoteDetailsActionTypes.RECEIVE_WS_ADD_TO_CART_RESPONSE_SUCCESS;
  constructor(public payload: IQuoteLineItemsToAddWSResponse) {}
}

/**
 * * Get Web Socket Response for add lineItems to Cart failed
 */

export class ReceiveWSAddToCartResponseFailed implements Action {
  readonly type = QuoteDetailsActionTypes.RECEIVE_WS_ADD_TO_CART_RESPONSE_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Add Quoted LineItem state
 */

export class UpdateSubmitedLineItemsState implements Action {
  readonly type = QuoteDetailsActionTypes.UPDATE_SUBMITED_LINE_ITEMS_STATE;
  constructor(public payload: { index: number; lineItem: IQuoteLineItem }) {}
}

/**
 * * Add Quoted LineItem state
 */

export class RequestProductsLoggedOnAnalytics implements Action {
  readonly type = QuoteDetailsActionTypes.REQUEST_PRODUCTS_LOGGED_ON_ANALYTICS;
  constructor(public payload: IQuotedProductsToAdd) {}
}

export type QuoteDetailsActions =
  | SearchLineItems
  | Query
  | QueryAppendDetails
  | ChangePage
  | ChangePageLimit
  | QueryComplete
  | QueryError
  | ChangeSort
  | RequestQuoteDetails
  | GetQuoteDetailsFailed
  | GetQuoteDetailsSuccess
  | ToggleCheckQuotedDetailLineItem
  | ToggleAllQuotedDetailLineItems
  | RequestAddQuotedLineItemToShoppingCart
  | RequestAddQuotedLineItemToShoppingCartSuccess
  | RequestAddQuotedLineItemToShoppingCartFailed
  | ReceiveWSAddToCartResponseSuccess
  | ReceiveWSAddToCartResponseFailed
  | UpdateSubmitedLineItemsState
  | RequestProductsLoggedOnAnalytics;
