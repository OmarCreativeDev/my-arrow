import { QuotesActionTypes, QuotesActions } from './quotes.actions';
import { IQuotesState } from 'app/core/quotes/quotes.interfaces';

export const INITIAL_QUOTE_STATE: IQuotesState = {
  loading: true,
  error: null,
  pagination: {
    limit: 10,
    current: 1,
    total: 0,
  },
  searchText: '',
  sort: [],
  quotes: null,
};

export function quotesReducer(state: IQuotesState = INITIAL_QUOTE_STATE, action: QuotesActions) {
  switch (action.type) {
    case QuotesActionTypes.CHANGE_PAGE_LIMIT: {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          limit: action.payload,
        },
      };
    }

    case QuotesActionTypes.CHANGE_PAGE: {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          current: action.payload,
        },
      };
    }

    case QuotesActionTypes.SEARCH_QUOTES: {
      return {
        ...state,
        searchText: action.payload,
      };
    }

    case QuotesActionTypes.QUERY: {
      return {
        ...state,
        error: false,
        loading: true,
        quotes: null,
      };
    }

    case QuotesActionTypes.QUERY_COMPLETE: {
      return {
        ...state,
        quotes: action.payload.quotes,
        pagination: {
          ...state.pagination,
          current: action.payload.pagination ? action.payload.pagination.page : INITIAL_QUOTE_STATE.pagination.current,
          total: action.payload.pagination ? action.payload.pagination.total : INITIAL_QUOTE_STATE.pagination.total,
        },
        loading: false,
        error: false,
      };
    }

    case QuotesActionTypes.QUERY_ERROR: {
      return {
        ...state,
        error: action.payload,
        loading: false,
        quotes: null,
      };
    }

    case QuotesActionTypes.CHANGE_SORT: {
      return {
        ...state,
        sort: action.payload ? action.payload : [],
      };
    }

    default:
      return state;
  }
}
