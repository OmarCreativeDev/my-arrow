import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IQuotesState } from 'app/core/quotes/quotes.interfaces';

/**
 * Selects the quotes state from the root state object
 */
export const getQuotesState = createFeatureSelector<IQuotesState>('quotes');

/**
 * Retrieve the number of items that should show per page
 */
export const getLimit = createSelector(getQuotesState, quotesState => quotesState.pagination.limit);

/**
 * Retrieve the current page
 */
export const getPage = createSelector(getQuotesState, quotesState => quotesState.pagination.current);

/**
 * Retrieve the number of total pages
 */
export const getTotalPages = createSelector(getQuotesState, quotesState => quotesState.pagination.total);

/**
 * Retrieve quotes
 */
export const getQuotes = createSelector(getQuotesState, quotesState => quotesState.quotes);

/**
 * Retrieves the loading state
 */
export const getLoading = createSelector(getQuotesState, quotesState => quotesState.loading);

/**
 * Retrieve the error state of the page
 */
export const getError = createSelector(getQuotesState, quotesState => quotesState.error);

/**
 * Retrieve the sorting of items
 */
export const getSort = createSelector(getQuotesState, quotesState => quotesState.sort);

/**
 * Retrieves the search
 */
export const getSearchQuery = createSelector(getQuotesState, quotesState => quotesState.searchText);
