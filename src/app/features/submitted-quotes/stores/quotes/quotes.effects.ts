import { Injectable } from '@angular/core';
import { Observable, of, combineLatest } from 'rxjs';
import { debounceTime, switchMap, map, catchError } from 'rxjs/operators';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { QuotesActionTypes, Query, QueryComplete, QueryError } from '@app/features/submitted-quotes/stores/quotes/quotes.actions';
import { QuotesService } from '@app/core/quotes/quotes.service';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable()
export class QuotesEffects {
  /**
   * Side-effect that combines all side effects for these facets:
   *  - Search
   *  - Limit
   *  - Page
   */
  @Effect()
  facets$: Observable<any> = combineLatest(
    this.actions$.pipe(ofType(QuotesActionTypes.SEARCH_QUOTES)),
    this.actions$.pipe(ofType(QuotesActionTypes.CHANGE_PAGE)),
    this.actions$.pipe(ofType(QuotesActionTypes.CHANGE_PAGE_LIMIT)),
    this.actions$.pipe(ofType(QuotesActionTypes.CHANGE_SORT))
  ).pipe(
    debounceTime(50),
    map((actions: any) => {
      const payloads = actions.map(action => action.payload);
      return new Query({
        pagination: {
          page: payloads[1],
          limit: payloads[2],
        },
        searchText: payloads[0],
        sorting: {
          order: payloads[3][0].order,
          field: payloads[3][0].key,
        },
      });
    })
  );

  /**
   * Side-effect for the Query being changed directly. Trigger by the facets$ Side-effect
   */
  @Effect()
  query$: Observable<any> = this.actions$.pipe(
    ofType(QuotesActionTypes.QUERY),
    switchMap((action: Query) => {
      return this.quotesService.getQuotes(action.payload).pipe(
        map((queryResponse: any) => new QueryComplete(queryResponse)),
        catchError((errorResponse: HttpErrorResponse) => of(new QueryError(errorResponse)))
      );
    })
  );

  constructor(private actions$: Actions, private quotesService: QuotesService) {}
}
