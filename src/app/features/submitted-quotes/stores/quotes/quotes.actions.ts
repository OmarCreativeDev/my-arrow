import { Action } from '@ngrx/store';
import { HttpErrorResponse } from '@angular/common/http';
import { IQuotesQueryRequest, IQuotesQueryResponse } from '@app/core/quotes/quotes.interfaces';
import { ISortCriteron } from '@app/shared/shared.interfaces';

export enum QuotesActionTypes {
  CHANGE_PAGE_LIMIT = '[Quotes] Change Page Limit',
  CHANGE_PAGE = '[Quotes] Change page',
  SEARCH_QUOTES = '[Quotes] Search Quotes',
  QUERY = '[Quotes] Query',
  QUERY_COMPLETE = '[Quotes] Query Complete',
  QUERY_ERROR = '[Quotes] Query Error',
  CHANGE_SORT = '[Quotes] Change Sort',
}

/**
 * Changes the limit on each page
 */
export class ChangePageLimit implements Action {
  readonly type = QuotesActionTypes.CHANGE_PAGE_LIMIT;
  constructor(public payload: number) {}
}

/**
 * Change the page number
 */
export class ChangePage implements Action {
  readonly type = QuotesActionTypes.CHANGE_PAGE;
  constructor(public payload: number) {}
}

/**
 * Search for quotes
 */
export class SearchQuotes implements Action {
  readonly type = QuotesActionTypes.SEARCH_QUOTES;
  constructor(public payload: string) {}
}

/**
 * Updates the Query with the provided facets
 */
export class Query implements Action {
  readonly type = QuotesActionTypes.QUERY;
  constructor(public payload: IQuotesQueryRequest) {}
}

/**
 * Results of the Query
 */
export class QueryComplete implements Action {
  readonly type = QuotesActionTypes.QUERY_COMPLETE;
  constructor(public payload: IQuotesQueryResponse) {}
}

/**
 * Thrown if the Query received an Error
 */
export class QueryError implements Action {
  readonly type = QuotesActionTypes.QUERY_ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

/**
 * Update The sorting of the clicked column
 */
export class ChangeSort implements Action {
  readonly type = QuotesActionTypes.CHANGE_SORT;
  constructor(public payload: Array<ISortCriteron>) {}
}

export type QuotesActions = SearchQuotes | Query | ChangePage | ChangePageLimit | QueryComplete | QueryError | ChangeSort;
