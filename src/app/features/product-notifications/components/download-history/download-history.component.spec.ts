import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CoreModule } from '@app/core/core.module';
import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { StoreModule } from '@ngrx/store';

import { DownloadHistoryComponent } from './download-history.component';
import { ProductNotificationsService } from '@app/core/product-notifications/product-notifications.service';
import { ProductNotificationsServiceMock } from '@app/core/product-notifications/product-notifications.service.spec';
import {
  IProductNotificationsDownloadUserPreferences,
  IProductNotificationsUserPreferences,
} from '@app/core/product-notifications/product-notifications.interfaces';
import { Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { FileService } from '@app/shared/services/file.service';
import { BrowserService } from '@app/shared/services/browser.service';
import { productNotificationsReducers } from '@app/features/product-notifications/stores/product-notifications.reducers';
import { userReducers } from '@app/core/user/store/user.reducers';

const mockedPreferenceOptions: IProductNotificationsUserPreferences = {
  types: ['XLS', 'XLSX', 'CSV'],
  dates: [6, 12, 24],
  columns: [
    {
      column: 'date',
      text: 'Date',
    },
    {
      column: 'customerPartNumber',
      text: 'Cust Part #',
    },
    {
      column: 'manufacturerPartNumber',
      text: 'MFG Part Number',
    },
    {
      column: 'manufacturer',
      text: 'Manufacturer',
    },
    {
      column: 'type',
      text: 'Type',
    },
    {
      column: 'description',
      text: 'Description',
    },
  ],
};

const mockedUserPreferences: IProductNotificationsDownloadUserPreferences = {
  dateRange: 12,
  fileType: 'CSV',
  columns: ['date', 'customerPartNumber', 'manufacturerPartNumber'],
  userId: 'david.lane@oncorems.com',
};

const mockedPreferences: IProductNotificationsDownloadUserPreferences = {
  dateRange: 6,
  fileType: 'CSV',
  columns: ['date', 'customerPartNumber', 'manufacturerPartNumber', 'manufacturer', 'type', 'description'],
};

describe('DownloadHistoryComponent', () => {
  let component: DownloadHistoryComponent;
  let fixture: ComponentFixture<DownloadHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DownloadHistoryComponent],
      imports: [
        StoreModule.forRoot({
          productNotifications: productNotificationsReducers,
          user: userReducers,
        }),
        FormsModule,
        CoreModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: ProductNotificationsService, useClass: ProductNotificationsServiceMock },
        FileService,
        BrowserService,
        { provide: Dialog, useClass: DialogMock },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadHistoryComponent);
    component = fixture.componentInstance;
    component.userEmail = 'test@arrow.com';
    component.selectedColumns = mockedPreferences.columns;
    component.company = 'test company';
    component.fileType = 'CSV';
    component.custAccountId = 1234;
    component.downloadOptions$ = of(mockedPreferenceOptions);
    component.downloadPreferences$ = of(mockedUserPreferences);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onClose', () => {
    spyOn(component, 'onClose').and.callThrough();
    component.onClose();
    fixture.detectChanges();
    expect(component.onClose).toHaveBeenCalled();
  });

  // Todo figure out why switching to ngxx causes error
  // it('should call getPreferences', () => {
  //   spyOn(component, 'getPreferences').and.callThrough();
  //   component.getPreferences();
  //   fixture.detectChanges();
  //   expect(component.getPreferences).toHaveBeenCalled();
  // });

  it('should call getFileTypes', () => {
    spyOn(component, 'getFileTypes').and.callThrough();
    component.getFileTypes(mockedPreferenceOptions.types);
    fixture.detectChanges();
    expect(component.getFileTypes).toHaveBeenCalled();
  });

  it('should call getColumns', () => {
    spyOn(component, 'getColumns').and.callThrough();
    component.getColumns(mockedPreferenceOptions.columns, mockedPreferences.columns);
    fixture.detectChanges();
    expect(component.getColumns).toHaveBeenCalled();
  });

  // Todo figure out why switching to ngxx causes error
  // it('should call download', () => {
  //   spyOn(notificationsService, 'getProductNotificationsFile').and.callFake(() => of({ status: 200 }));
  //   spyOn(FileSaver, 'saveAs').and.callFake(() => {});
  //   spyOn(fileService, 'saveResponse').and.callThrough();
  //   component.download();
  //   expect(notificationsService.getProductNotificationsFile).toHaveBeenCalledWith(component.getDownloadParams());
  // });
  //
  // it('should invoke handleNoData when api returns 204', () => {
  //   spyOn(notificationsService, 'getProductNotificationsFile').and.callFake(() => of({ status: 204 }));
  //   spyOn(component, 'handleNoData').and.callThrough();
  //   component.download();
  //   expect(component.handleNoData).toHaveBeenCalled();
  // });
});
