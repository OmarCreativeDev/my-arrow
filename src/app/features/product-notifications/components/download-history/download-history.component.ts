import { Component, EventEmitter, Output, OnInit, OnDestroy } from '@angular/core';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import find from 'lodash-es/find';
import * as moment from 'moment';

import { ISortableItem, IDownloadColumnTypeEnum, IFileTypeEnum, IFileTypeOption } from '@app/shared/shared.interfaces';
import { IUser } from '@app/core/user/user.interface';
import { getUser } from '@app/core/user/store/user.selectors';

import {
  IProductNotificationFileTypes,
  IProductNotificationColumnTypes,
  IProductNotificationsDownloadUserPreferences,
  IProductNotificationsUserPreferences,
} from '@app/core/product-notifications/product-notifications.interfaces';
import { Dialog } from '@app/core/dialog/dialog.service';
import { AlertComponentEnum } from '@app/shared/shared.interfaces';
import {
  getProductNotificationDownloadOptionsSelector,
  getProductNotificationDownloadPreferencesSelector,
  getProductDownloadErrorSelector,
  getProductDownloadStatus,
} from '@app/features/product-notifications/stores/product-notifications.selectors';
import {
  GetNotificationDownloadOptions,
  GetNotificationDownloadPreferences,
  SetNotificationDownloadPreferences,
  GetNotificationDownloadFile,
  GetNotificationDownloadFileReset,
} from '@app/features/product-notifications/stores/product-notifications.actions';
import { IAppState } from '@app/shared/shared.interfaces';
import { errorsMessageHeader, errorsMessageBody } from '@app/features/errors/pages/errors.message';

@Component({
  selector: 'app-download-history',
  templateUrl: './download-history.component.html',
  styleUrls: ['./download-history.component.scss'],
})
export class DownloadHistoryComponent implements OnInit, OnDestroy {
  @Output()
  public close: EventEmitter<boolean> = new EventEmitter();
  @Output()
  initialColumns = new EventEmitter();

  DATE_FORMAT: string = 'YYYY-MM-DD';

  downloadModalVisible: boolean = false;
  downloadDateRange: Array<number> = [];
  fileTypeOptions: Array<IFileTypeOption> = [];

  columns: Array<ISortableItem> = [];
  dateRange = '6';
  fileType: IProductNotificationFileTypes = 'CSV';
  userEmail: string;
  selectedColumns = [];
  custAccountId: number;
  company: string;
  noData: boolean = false;
  alertComponentEnum = AlertComponentEnum;

  downloadOptions$: Observable<IProductNotificationsUserPreferences>;
  downloadPreferences$: Observable<IProductNotificationsDownloadUserPreferences>;
  userProfile$: Observable<IUser>;
  maxDate: number;
  loadNotificationDownloadsError$: Observable<any>;
  errors: object;
  errorHeader: string;
  errorBody: string;
  status$: Observable<any>;
  status: number;
  private subscription: Subscription = new Subscription();

  constructor(private store: Store<IAppState>, private dialog: Dialog<DownloadHistoryComponent>) {
    this.downloadOptions$ = store.pipe(select(getProductNotificationDownloadOptionsSelector));
    this.downloadPreferences$ = store.pipe(select(getProductNotificationDownloadPreferencesSelector));
    this.userProfile$ = store.pipe(select(getUser));
    this.loadNotificationDownloadsError$ = store.pipe(select(getProductDownloadErrorSelector));
    this.status$ = this.store.pipe(select(getProductDownloadStatus));
    this.errorHeader = errorsMessageHeader;
    this.errorBody = errorsMessageBody;
  }

  subAddUser(): Subscription {
    return this.userProfile$.subscribe(user => {
      if (user) {
        this.custAccountId = user.accountId;
        this.company = user.company;
        this.userEmail = user.email;
      }
    });
  }

  subGetErrors(): Subscription {
    return this.loadNotificationDownloadsError$.subscribe(error => {
      this.errors = error;
    });
  }

  subGetOptions(): Subscription {
    return combineLatest(this.downloadOptions$, this.downloadPreferences$).subscribe(latestValues => {
      if (latestValues[0].columns.length && latestValues[1].columns.length) {
        this.columns = this.getColumns(latestValues[0].columns, latestValues[1].columns);
        this.downloadDateRange = latestValues[0].dates;
        this.maxDate = Math.max(...latestValues[0].dates);
        this.fileTypeOptions = this.getFileTypes(latestValues[0].types);
        this.dateRange = String(latestValues[1].dateRange);
        this.fileType = latestValues[1].fileType;
      }
    });
  }

  subGetStatus(): Subscription {
    return this.status$.subscribe(status => {
      this.status = status;
      if (this.status === 204) {
        this.handleNoData();
      } else if (this.status === 200) {
        this.onClose();
      }
    });
  }

  ngOnInit(): void {
    this.getDownloadPreferences();
    this.subscription.add(this.subGetOptions());
    this.subscription.add(this.subAddUser());
    this.subscription.add(this.subGetErrors());
    this.subscription.add(this.subGetStatus());
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public getDownloadPreferences(): void {
    this.store.dispatch(new GetNotificationDownloadOptions());
    this.store.dispatch(new GetNotificationDownloadPreferences());
  }

  getColumns(columns, selectedColumns): Array<ISortableItem> {
    const unselected = columns.filter(x => !selectedColumns.includes(x.column));
    const selected = selectedColumns.filter(x => !columns.includes(x));
    let orderedColumns: Array<ISortableItem> = [];

    orderedColumns = selected.map((col, i) => ({
      id: i,
      label: find(columns, { column: col }).text,
      properties: [find(columns, { column: col }).column],
      type: IDownloadColumnTypeEnum.String,
      selected: true,
    }));

    let id = orderedColumns.length;

    const unselectedColumns = unselected.map(column => ({
      id: id++,
      label: column.text,
      properties: [column.column],
      type: IDownloadColumnTypeEnum.String,
      selected: false,
    }));

    orderedColumns = orderedColumns.concat(unselectedColumns);
    return orderedColumns;
  }

  getFileTypes(types): Array<any> {
    const fileTypes: Array<any> = [];
    types.forEach(function(type) {
      fileTypes.push({
        label: '.' + type.toLowerCase(),
        value: IFileTypeEnum[type],
      });
    });

    return fileTypes;
  }

  getSelectedColumns(): Array<IProductNotificationColumnTypes> {
    return this.selectedColumns.map(a => a.properties[0]);
  }

  getPreferences(): IProductNotificationsDownloadUserPreferences {
    const preferences: IProductNotificationsDownloadUserPreferences = {
      dateRange: Number(this.dateRange),
      fileType: this.fileType,
      columns: this.getSelectedColumns(),
      userId: this.userEmail,
    };

    return preferences;
  }

  getDownloadParams(): any {
    const startDate = moment(new Date())
      .subtract(this.dateRange, 'months')
      .format(this.DATE_FORMAT);
    const endDate = moment(new Date())
      .startOf('day')
      .format(this.DATE_FORMAT);
    const params: any = {
      companyName: this.company,
      fileType: this.fileType,
      columns: this.getSelectedColumns(),
      accountId: this.custAccountId,
      startDate: startDate,
      endDate: endDate,
    };

    return params;
  }

  public handleNoData(): void {
    this.noData = true;
  }

  download(): void {
    this.store.dispatch(new SetNotificationDownloadPreferences(this.getPreferences()));
    this.store.dispatch(new GetNotificationDownloadFile(this.getDownloadParams()));
  }

  onClose(): void {
    this.dialog.close();
    this.store.dispatch(new GetNotificationDownloadFileReset());
  }

  resetError(): void {
    this.noData = false;
  }
}
