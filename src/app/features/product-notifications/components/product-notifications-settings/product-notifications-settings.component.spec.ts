import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { FormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Store, StoreModule } from '@ngrx/store';
import { CommonModule } from '@angular/common';

import { ProductNotificationsSettingsComponent } from './product-notifications-settings.component';
import mockedProductNotifications from '@app/core/product-notifications/product-notifications-mock-response';
import { Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { productNotificationsReducers } from '@app/features/product-notifications/stores/product-notifications.reducers';

class MockStore {
  select() {
    return of();
  }
  dispatch() {}
  pipe() {
    return of();
  }
}

describe('ProductNotificationsSettingsComponent', () => {
  let component: ProductNotificationsSettingsComponent;
  let fixture: ComponentFixture<ProductNotificationsSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductNotificationsSettingsComponent],
      imports: [CommonModule, FormsModule, StoreModule.forRoot({ productNotifications: productNotificationsReducers })],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{ provide: Dialog, useClass: DialogMock }, { provide: Store, useClass: MockStore }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductNotificationsSettingsComponent);
    component = fixture.componentInstance;
    component.critical = mockedProductNotifications.critical;
    component.nonCritical = mockedProductNotifications.nonCritical;
    component.notificationsRequired = mockedProductNotifications.pcnNotifReq;
    component.OMfrequency = mockedProductNotifications.frequency;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call toggleAll', () => {
    spyOn(component, 'toggleAll').and.callThrough();
    component.toggleAll(true, 'critical', 'selectAllCritical');
    fixture.detectChanges();
    expect(component.critical[0].value).toEqual(true);
    expect(component.selectAllCritical).toEqual(true);
    expect(component.toggleAll).toHaveBeenCalled();
  });

  it('should call toggleCheckbox', () => {
    spyOn(component, 'toggleCheckbox').and.callThrough();
    component.toggleCheckbox(true, 'critical', 1);
    fixture.detectChanges();
    expect(component.toggleCheckbox).toHaveBeenCalled();
  });

  it('should call onClose', () => {
    spyOn(component, 'onClose').and.callThrough();
    component.onClose();
    fixture.detectChanges();
    expect(component.onClose).toHaveBeenCalled();
  });

  it('should call saveSettings', () => {
    spyOn(component, 'saveSettings').and.callThrough();
    spyOn(component, 'getPreferences').and.callThrough();
    component.saveSettings();
    fixture.detectChanges();
    expect(component.saveSettings).toHaveBeenCalled();
    expect(component.region).not.toBe(null);
    expect(component.getPreferences).toHaveBeenCalled();
  });
});
