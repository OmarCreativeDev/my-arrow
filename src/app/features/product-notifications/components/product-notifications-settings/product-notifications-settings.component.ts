import { OnDestroy } from '@angular/core';
import { Component, EventEmitter, Output, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';

import {
  IProductNotificationField,
  IProductNotificationsPreferences,
  ProductNotificationsFrequency,
} from '@app/core/product-notifications/product-notifications.interfaces';
import { Dialog } from '@app/core/dialog/dialog.service';
import {
  getProductNotificationPreferencesSelector,
  getProductNotificationPreferencesErrorSelector,
  getProductNotificationPreferencesSuccessSelector,
} from '@app/features/product-notifications/stores/product-notifications.selectors';
import {
  GetNotificationPreferences,
  SetNotificationPreferences,
} from '@app/features/product-notifications/stores/product-notifications.actions';
import { IAppState } from '@app/shared/shared.interfaces';
import { errorsMessageHeader, errorsMessageBody } from '@app/features/errors/pages/errors.message';
import { getAccountId, getAccountSiteID, getUserPartyID, getRegion } from '@app/core/user/store/user.selectors';

@Component({
  selector: 'app-notifications-settings',
  templateUrl: './product-notifications-settings.component.html',
  styleUrls: ['./product-notifications-settings.component.scss'],
})
export class ProductNotificationsSettingsComponent implements OnInit, OnDestroy {
  @Output()
  close: EventEmitter<boolean> = new EventEmitter();
  selectAllCritical: boolean = false;
  selectAllNonCritical: boolean = false;
  notificationsRequired: boolean = false;
  critical: IProductNotificationField[];
  nonCritical: IProductNotificationField[];
  OMfrequency: ProductNotificationsFrequency = 'Weekly';
  custAccountId: number;
  custContactId: number;
  custAcctSiteId: number;
  custPartyId: number;
  region: string;
  frequency: ProductNotificationsFrequency[] = ['Daily', 'Weekly'];
  notificationPreferencesError$: Observable<any>;
  notificationPreferencesSuccess$: Observable<any>;
  notificationPreferences$: Observable<any>;
  errors: object;
  errorHeader: string;
  errorBody: string;
  subscription: Subscription = new Subscription();

  constructor(private store: Store<IAppState>, private dialog: Dialog<ProductNotificationsSettingsComponent>) {
    this.notificationPreferences$ = store.pipe(select(getProductNotificationPreferencesSelector));
    this.errorHeader = errorsMessageHeader;
    this.errorBody = errorsMessageBody;
    this.notificationPreferencesError$ = store.pipe(select(getProductNotificationPreferencesErrorSelector));
    this.notificationPreferencesSuccess$ = store.pipe(select(getProductNotificationPreferencesSuccessSelector));
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public getNotificationPreferences(): void {
    this.store.dispatch(
      new GetNotificationPreferences({ customerId: this.custAccountId, custAcctSiteId: this.custAcctSiteId, custPartyId: this.custPartyId })
    );
  }

  private startSubscriptions() {
    this.subscription.add(
      this.notificationPreferences$.subscribe(preferences => {
        /* istanbul ignore next */
        if (preferences) {
          this.critical = preferences.critical;
          this.nonCritical = preferences.nonCritical;
          this.notificationsRequired = preferences.notificationsRequired;
          this.OMfrequency = preferences.OMfrequency ? preferences.OMfrequency : this.OMfrequency;
          this.custContactId = preferences.custContactId;
        }
      })
    );

    this.subscription.add(
      this.notificationPreferencesError$.subscribe(error => {
        this.errors = error;
      })
    );

    this.subscription.add(
      this.store.pipe(select(getAccountId)).subscribe(accountId => {
        this.custAccountId = accountId;
      })
    );

    this.subscription.add(
      this.store.pipe(select(getUserPartyID)).subscribe(userPartyId => {
        this.custPartyId = userPartyId;
      })
    );

    this.subscription.add(
      this.store.pipe(select(getAccountSiteID)).subscribe(accountSiteId => {
        this.custAcctSiteId = accountSiteId;
      })
    );

    this.subscription.add(
      this.store.pipe(select(getRegion)).subscribe(region => {
        this.region = region;
      })
    );

    this.getNotificationPreferences();
  }

  toggleAll(event: boolean, type: string, typeToggle: string): void {
    /* istanbul ignore else */
    if (this[type]) {
      this[type] = this[type].map(obj => ({
        description: obj.description,
        value: event,
        name: obj.name,
      }));
    }

    this[typeToggle] = event;
  }

  toggleCheckbox(event: boolean, type: string, i: number): void {
    this[type][i].value = event;
    /* istanbul ignore next */
    type === 'critical' ? (this.selectAllCritical = false) : (this.selectAllNonCritical = false);
  }

  onClose(): void {
    this.dialog.close();
  }

  getPreferences(): IProductNotificationsPreferences {
    const preferences = {
      custAccountId: this.custAccountId,
      custAcctSiteId: this.custAcctSiteId,
      custContactId: this.custContactId,
      custPartyId: this.custPartyId,
      frequency: this.OMfrequency,
      other: true,
      pcnFileType: 'PDF',
      pcnNotifReq: this.notificationsRequired,
      region: this.region,
    };

    this.critical.forEach(function(critical) {
      preferences[critical.name] = critical.value;
    });

    this.nonCritical.forEach(function(nonCritical) {
      preferences[nonCritical.name] = nonCritical.value;
    });

    return preferences;
  }

  saveSettings(): void {
    const preferences = this.getPreferences();
    this.store.dispatch(new SetNotificationPreferences(preferences));
    this.subscription.add(
      this.notificationPreferencesSuccess$.subscribe(res => {
        if (res) {
          this.onClose();
        }
      })
    );
  }
}
