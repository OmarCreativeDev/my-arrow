import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ProductNotificationTableComponent } from './product-notification-table.component';
import { MockedProductNotifications } from '@app/core/product-notifications/product-notifications.service.spec';

describe('ProductNotificationTableComponent', () => {
  let component: ProductNotificationTableComponent;
  let fixture: ComponentFixture<ProductNotificationTableComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ProductNotificationTableComponent],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductNotificationTableComponent);
    component = fixture.componentInstance;
    component.notifications = MockedProductNotifications;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call sortRows', () => {
    spyOn(component, 'sortRows').and.callThrough();
    const ascending = component.ascending;
    const collection = component.notifications;
    component.sortRows('type', 'notifications');
    fixture.detectChanges();
    expect(ascending).not.toEqual(component.ascending);
    expect(collection).not.toEqual(component.notifications);
    expect(component.sortRows).toHaveBeenCalled();
  });
});
