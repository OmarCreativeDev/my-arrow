import { Component, Input } from '@angular/core';

import { IProductNotification } from '@app/core/product-notifications/product-notifications.interfaces';
import orderBy from 'lodash-es/orderBy';

@Component({
  selector: 'app-product-notification-table',
  templateUrl: './product-notification-table.component.html',
  styleUrls: ['./product-notification-table.component.scss'],
})
export class ProductNotificationTableComponent {
  @Input()
  notifications: Array<IProductNotification>;
  @Input()
  header: string;
  @Input()
  description: string;
  @Input()
  table: string;
  @Input()
  selectedTable: string;

  ascending: boolean = true;

  sortRows(field, collection): void {
    /* istanbul ignore next */
    this[collection] = orderBy(this[collection], [fields => fields[field].toLowerCase()], this.ascending ? 'asc' : 'desc');
    this.ascending = !this.ascending;
  }
}
