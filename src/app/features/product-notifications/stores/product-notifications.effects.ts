import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, catchError, switchMap } from 'rxjs/operators';

import {
  ProductNotificationsActionTypes,
  GetNotifications,
  GetNotificationsSuccess,
  GetNotificationsFailed,
  GetNotificationPreferences,
  GetNotificationPreferencesSuccess,
  GetNotificationPreferencesFailed,
  SetNotificationPreferences,
  SetNotificationPreferencesSuccess,
  SetNotificationPreferencesFailed,
  GetNotificationDownloadPreferences,
  GetNotificationDownloadPreferencesSuccess,
  GetNotificationDownloadPreferencesFailed,
  SetNotificationDownloadPreferences,
  SetNotificationDownloadPreferencesSuccess,
  SetNotificationDownloadPreferencesFailed,
  GetNotificationDownloadOptions,
  GetNotificationDownloadOptionsSuccess,
  GetNotificationDownloadOptionsFailed,
  GetNotificationDownloadFile,
  GetNotificationDownloadFileSuccess,
  GetNotificationDownloadFileFailed,
} from '@app/features/product-notifications/stores/product-notifications.actions';

import { ProductNotificationsService } from '@app/core/product-notifications/product-notifications.service';
import { NotificationTypes } from '@app/core/product-notifications/product-notifications.interfaces';
import { FileService } from '@app/shared/services/file.service';
import { HttpResponse } from '@angular/common/http';

@Injectable()
export class ProductNotificationsEffects {
  constructor(private actions$: Actions, private notificationsService: ProductNotificationsService, private fileService: FileService) {}

  @Effect()
  GetProductNotifications$ = this.actions$.pipe(
    ofType(ProductNotificationsActionTypes.GET_NOTIFICATIONS),
    switchMap((action: GetNotifications) => {
      return this.notificationsService
        .getProductNotifications(action.payload.custAccountId, action.payload.startDate, action.payload.endDate)
        .pipe(
          map((data: any) => new GetNotificationsSuccess(this.populateProductNotifications(data))),
          catchError(err => of(new GetNotificationsFailed(err)))
        );
    })
  );

  @Effect()
  GetProductNotificationPreferences$ = this.actions$.pipe(
    ofType(ProductNotificationsActionTypes.GET_NOTIFICATION_PREFERENCES),
    switchMap((action: GetNotificationPreferences) => {
      return this.notificationsService
        .getProductNotificatonPreferences(action.payload.customerId, action.payload.custAcctSiteId, action.payload.custPartyId)
        .pipe(
          map((data: any) => new GetNotificationPreferencesSuccess(data)),
          catchError(err => of(new GetNotificationPreferencesFailed(err)))
        );
    })
  );

  @Effect()
  SetProductNotificationPreferences$ = this.actions$.pipe(
    ofType(ProductNotificationsActionTypes.SET_NOTIFICATION_PREFERENCES),
    switchMap((action: SetNotificationPreferences) => {
      return this.notificationsService.setProductNotificatonPreferences(action.payload).pipe(
        map((data: any) => new SetNotificationPreferencesSuccess()),
        catchError(err => of(new SetNotificationPreferencesFailed(err)))
      );
    })
  );

  @Effect()
  GetProductNotificationDownloadOptions$ = this.actions$.pipe(
    ofType(ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_OPTIONS),
    switchMap((action: GetNotificationDownloadOptions) => {
      return this.notificationsService.getProductNotificationsDownloadOptions().pipe(
        map((data: any) => new GetNotificationDownloadOptionsSuccess(data)),
        catchError(err => of(new GetNotificationDownloadOptionsFailed(err)))
      );
    })
  );

  @Effect()
  GetProductNotificationDownloadPreferences$ = this.actions$.pipe(
    ofType(ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_PREFERENCES),
    switchMap((action: GetNotificationDownloadPreferences) => {
      return this.notificationsService.getProductNotificationsDownloadUserPreferences().pipe(
        map((data: any) => new GetNotificationDownloadPreferencesSuccess(data)),
        catchError(err => of(new GetNotificationDownloadPreferencesFailed(err)))
      );
    })
  );

  @Effect()
  SetProductNotificationDownloadPreferences$ = this.actions$.pipe(
    ofType(ProductNotificationsActionTypes.SET_NOTIFICATION_DOWNLOAD_PREFERENCES),
    switchMap((action: SetNotificationDownloadPreferences) => {
      return this.notificationsService.setProductNotificationsDownloadOptions(action.payload).pipe(
        map((data: any) => new SetNotificationDownloadPreferencesSuccess(action.payload)),
        catchError(err => of(new SetNotificationDownloadPreferencesFailed(err)))
      );
    })
  );

  @Effect()
  GetProductNotificationDownloadFile$ = this.actions$.pipe(
    ofType(ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_FILE),
    switchMap((action: GetNotificationDownloadFile) => {
      return this.notificationsService.getProductNotificationsFile(action.payload).pipe(
        map((response: any) => new GetNotificationDownloadFileSuccess(this.handleDownload(response))),
        catchError(err => of(new GetNotificationDownloadFileFailed(err)))
      );
    })
  );

  populateProductNotifications(notifications) {
    if (notifications) {
      return notifications.reduce(
        (acc, notification) => {
          /* istanbul ignore next */
          switch (notification.category) {
            case NotificationTypes.MANUFACTURING:
              acc.pmProductNotifications.push(notification);
              break;
            case NotificationTypes.NON_FUNCTIONAL:
              acc.pnfProductNotifications.push(notification);
              break;
            case NotificationTypes.STATUS:
              acc.pscProductNotifications.push(notification);
              break;
          }
          return acc;
        },
        {
          pmProductNotifications: [],
          pnfProductNotifications: [],
          pscProductNotifications: [],
        }
      );
    } else {
      return {
        pmProductNotifications: [],
        pnfProductNotifications: [],
        pscProductNotifications: [],
      };
    }
  }

  handleDownload(response: HttpResponse<Blob>) {
    if (response.status === 200) {
      this.fileService.saveResponse(response);
      return 200;
    } else if (response.status === 204) {
      return 204;
    }
  }
}
