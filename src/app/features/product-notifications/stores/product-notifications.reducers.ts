import { ProductNotificationsActions, ProductNotificationsActionTypes } from './product-notifications.actions';
import {
  IProductNotificationsState,
  ProductNotificationsFrequency,
} from '@app/core/product-notifications/product-notifications.interfaces';

export const INITIAL_PRODUCT_NOTIFICATIONS_STATE: any = {
  notifications: {},
  loading: false,
  error: undefined,
  notificationPreferencesError: undefined,
  notificationPreferencesSuccess: undefined,
  downloadError: undefined,
  notificationPreferences: {
    critical: [],
    nonCritical: [],
    notificationsRequired: '',
    pcnNotifReq: false,
    custContactId: null,
    frequency: <ProductNotificationsFrequency>'Daily',
  },
  setNotificationPreferences: {
    custAccountId: null,
    custAcctSiteId: null,
    custContactId: null,
    custPartyId: null,
    frequency: <ProductNotificationsFrequency>'Daily',
    other: false,
    pcnFileType: '',
    pcnNotifReq: false,
  },
  downloadOptions: {
    dates: [],
    types: [],
    columns: [],
  },
  downloadPreferences: {
    dateRange: null,
    fileType: '',
    columns: [],
  },
  downloadStatus: undefined,
};

/**
 * Mutates the state for given set of notifications
 * @param state
 * @param action
 */
export function productNotificationsReducers(
  state: IProductNotificationsState = INITIAL_PRODUCT_NOTIFICATIONS_STATE,
  action: ProductNotificationsActions
) {
  switch (action.type) {
    case ProductNotificationsActionTypes.GET_NOTIFICATIONS: {
      return {
        ...state,
        loading: true,
        error: undefined,
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATIONS_SUCCESS: {
      return {
        ...state,
        loading: false,
        error: undefined,
        notifications: action.payload,
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATIONS_FAILED: {
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATION_PREFERENCES: {
      return {
        ...state,
        loading: true,
        notificationPreferencesError: undefined,
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATION_PREFERENCES_SUCCESS: {
      return {
        ...state,
        loading: false,
        notificationPreferencesError: undefined,
        notificationPreferences: {
          critical: action.payload.critical,
          nonCritical: action.payload.nonCritical,
          notificationsRequired: action.payload.pcnNotifReq,
          OMfrequency: action.payload.frequency,
          custContactId: action.payload.custContactId,
        },
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATION_PREFERENCES_FAILED: {
      return {
        ...state,
        loading: false,
        notificationPreferencesError: action.payload,
      };
    }
    case ProductNotificationsActionTypes.SET_NOTIFICATION_PREFERENCES: {
      return {
        ...state,
        loading: true,
        notificationPreferencesError: undefined,
        notificationPreferencesSuccess: undefined,
      };
    }
    case ProductNotificationsActionTypes.SET_NOTIFICATION_PREFERENCES_SUCCESS: {
      return {
        ...state,
        loading: false,
        notificationPreferencesError: undefined,
        notificationPreferencesSuccess: true,
      };
    }
    case ProductNotificationsActionTypes.SET_NOTIFICATION_PREFERENCES_FAILED: {
      return {
        ...state,
        loading: false,
        notificationPreferencesError: action.payload,
        notificationPreferencesSuccess: false,
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_OPTIONS: {
      return {
        ...state,
        loading: true,
        downloadError: undefined,
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_OPTIONS_SUCCESS: {
      return {
        ...state,
        loading: false,
        downloadError: undefined,
        downloadOptions: action.payload,
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_OPTIONS_FAILED: {
      return {
        ...state,
        loading: false,
        downloadError: action.payload,
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_PREFERENCES: {
      return {
        ...state,
        loading: true,
        downloadError: undefined,
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_PREFERENCES_SUCCESS: {
      return {
        ...state,
        loading: false,
        downloadError: undefined,
        downloadPreferences: action.payload,
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_PREFERENCES_FAILED: {
      return {
        ...state,
        loading: false,
        downloadError: action.payload,
      };
    }
    case ProductNotificationsActionTypes.SET_NOTIFICATION_DOWNLOAD_PREFERENCES: {
      return {
        ...state,
        loading: true,
        downloadError: undefined,
      };
    }
    case ProductNotificationsActionTypes.SET_NOTIFICATION_DOWNLOAD_PREFERENCES_SUCCESS: {
      return {
        ...state,
        loading: false,
        downloadError: undefined,
        downloadPreferences: {
          columns: action.payload.columns,
          fileType: action.payload.fileType,
          dateRange: action.payload.dateRange,
        },
      };
    }
    case ProductNotificationsActionTypes.SET_NOTIFICATION_DOWNLOAD_PREFERENCES_FAILED: {
      return {
        ...state,
        loading: false,
        downloadError: action.payload,
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_FILE: {
      return {
        ...state,
        loading: true,
        downloadError: undefined,
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_FILE_SUCCESS: {
      return {
        ...state,
        loading: false,
        downloadError: undefined,
        downloadStatus: action.payload,
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_FILE_FAILED: {
      return {
        ...state,
        loading: false,
        downloadError: action.payload,
      };
    }
    case ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_FILE_RESET: {
      return {
        ...state,
        loading: false,
        downloadError: undefined,
        downloadStatus: undefined,
      };
    }
    default: {
      return state;
    }
  }
}
