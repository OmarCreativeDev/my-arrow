import {
  GetNotifications,
  GetNotificationsSuccess,
  GetNotificationsFailed,
  GetNotificationPreferences,
  GetNotificationPreferencesSuccess,
  GetNotificationPreferencesFailed,
  SetNotificationPreferences,
  SetNotificationPreferencesSuccess,
  SetNotificationPreferencesFailed,
  GetNotificationDownloadPreferences,
  GetNotificationDownloadPreferencesSuccess,
  GetNotificationDownloadPreferencesFailed,
  SetNotificationDownloadPreferences,
  SetNotificationDownloadPreferencesSuccess,
  SetNotificationDownloadPreferencesFailed,
  GetNotificationDownloadOptions,
  GetNotificationDownloadOptionsSuccess,
  GetNotificationDownloadOptionsFailed,
  GetNotificationDownloadFile,
  GetNotificationDownloadFileSuccess,
  GetNotificationDownloadFileFailed,
} from '@app/features/product-notifications/stores/product-notifications.actions';

import {
  productNotificationsReducers,
  INITIAL_PRODUCT_NOTIFICATIONS_STATE,
} from '@app/features/product-notifications/stores/product-notifications.reducers';
import {
  IProductNotifications,
  IProductNotificationsState,
  IProductNotificationsSorted,
  ProductNotificationsFrequency,
  IProductNotificationsPreferences,
  IProductNotificationsDownloadUserPreferences,
  IProductNotificationsUserPreferences,
  IProductNotificationsDownloadParams,
} from '@app/core/product-notifications/product-notifications.interfaces';

describe('Product Notifications Reducer', () => {
  let initialState: IProductNotificationsState;

  beforeEach(() => {
    initialState = INITIAL_PRODUCT_NOTIFICATIONS_STATE;
  });

  const MockPreferences: IProductNotificationsPreferences = {
    alertRecall: false,
    assemblyProcess: false,
    assemblySite: false,
    custAccountId: 1234,
    custAcctSiteId: 1234,
    custContactId: 1234,
    custPartyId: 1234,
    endOfLife: false,
    environmentalData: false,
    eolReversal: false,
    formFitFun: false,
    frequency: <ProductNotificationsFrequency>'Daily',
    labellingPacking: false,
    marking: false,
    molding: false,
    nomenclatureChg: false,
    notForDesign: false,
    other: false,
    pcnFileType: '1234asdf',
    pcnNotifReq: false,
    recall: false,
    rfcbNcr: false,
    rfcbNcrReversal: false,
    shippingPacking: false,
    testProcess: false,
    testSite: false,
    waferProcess: false,
    waferSite: false,
  };

  const mockedProductNotificationsSorted: IProductNotificationsSorted = {
    pmProductNotifications: [],
    pnfProductNotifications: [],
    pscProductNotifications: [],
  };

  const mockedProductNotifications: IProductNotifications = {
    critical: [
      {
        value: false,
        description: 'test',
        name: 'test',
      },
    ],
    nonCritical: [
      {
        value: false,
        description: 'test',
        name: 'test',
      },
    ],
    custAccountId: 1234,
    custAcctSiteId: 1234,
    custContactId: 1234,
    custPartyId: 1234,
    pcnNotifReq: false,
    frequency: 'Daily',
    pcnFileType: 'PDF',
  };

  const mockedProductNotificationsDownloadUserPreferences: IProductNotificationsDownloadUserPreferences = {
    dateRange: 6,
    fileType: 'XLS',
    columns: ['date'],
  };

  const mockedUserPreferences: IProductNotificationsDownloadUserPreferences = {
    dateRange: 12,
    fileType: 'CSV',
    columns: ['date', 'customerPartNumber', 'manufacturerPartNumber', 'manufacturer', 'type', 'description'],
    userId: 'david.lane@oncorems.com',
  };

  const mockedProductNotificationsDownloadParams: IProductNotificationsDownloadParams = {
    accountId: 1234,
    companyName: 'test company',
    fileType: 'XLS',
    columns: ['date'],
  };

  const mockedDownloadOptions: IProductNotificationsUserPreferences = {
    types: ['XLS', 'XLSX', 'CSV'],
    dates: [6, 12, 24],
    columns: [
      {
        column: 'date',
        text: 'Date',
      },
      {
        column: 'customerPartNumber',
        text: 'Cust Part #',
      },
      {
        column: 'manufacturerPartNumber',
        text: 'MFG Part Number',
      },
      {
        column: 'manufacturer',
        text: 'Manufacturer',
      },
      {
        column: 'type',
        text: 'Type',
      },
      {
        column: 'description',
        text: 'Description',
      },
    ],
  };

  it('`GET_NOTIFICATIONS` should set `loading` to true and not have an error property', () => {
    const action = new GetNotifications({ custAccountId: 1305827, startDate: '2018-01-01', endDate: '2018-05-01' });
    const result = productNotificationsReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeUndefined();
  });

  it('`GET_NOTIFICATIONS_SUCCESS` should set `notifications`', () => {
    const action = new GetNotificationsSuccess(mockedProductNotificationsSorted);
    const result = <IProductNotificationsState>productNotificationsReducers(initialState, action);
    expect(result.notifications).toBeDefined();
  });

  it(`GET_NOTIFICATIONS_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new GetNotificationsFailed(error);
    const result = productNotificationsReducers(initialState, action);
    expect(result.error).toBeDefined();
  });

  it('`GET_NOTIFICATION_PREFERENCES` should set `loading` to true and not have an error property', () => {
    const action = new GetNotificationPreferences({ custAcctSiteId: 1305827, custPartyId: 10125599, customerId: 6239544 });
    const result = productNotificationsReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.error).toBeUndefined();
  });

  it('`GET_NOTIFICATION_PREFERENCES_SUCCESS` should set `notifications`', () => {
    const action = new GetNotificationPreferencesSuccess(mockedProductNotifications);
    const result = <IProductNotificationsState>productNotificationsReducers(initialState, action);
    expect(result.notifications).toBeDefined();
  });

  it(`GET_NOTIFICATION_PREFERENCES_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new GetNotificationPreferencesFailed(error);
    const result = productNotificationsReducers(initialState, action);
    expect(result.notificationPreferencesError).toBeDefined();
  });

  it('`SET_NOTIFICATION_PREFERENCES` should set `loading` to true and not have an error property', () => {
    const action = new SetNotificationPreferences(MockPreferences);
    const result = productNotificationsReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.notificationPreferencesError).toBeUndefined();
  });

  it('`SET_NOTIFICATION_PREFERENCES_SUCCESS` should set `preferences`', () => {
    const action = new SetNotificationPreferencesSuccess();
    const result = <IProductNotificationsState>productNotificationsReducers(initialState, action);
    expect(result.loading).toBeFalsy();
    expect(result.notificationPreferencesError).toBeUndefined();
  });

  it(`SET_NOTIFICATION_PREFERENCES_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new SetNotificationPreferencesFailed(error);
    const result = productNotificationsReducers(initialState, action);
    expect(result.notificationPreferencesError).toBeDefined();
  });

  it('`GET_NOTIFICATION_DOWNLOAD_OPTIONS` should set `loading` to true and not have an error property', () => {
    const action = new GetNotificationDownloadOptions();
    const result = productNotificationsReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.downloadError).toBeUndefined();
  });

  it('`GET_NOTIFICATION_DOWNLOAD_OPTIONS_SUCCESS` should set `downloadPreferences`', () => {
    const action = new GetNotificationDownloadOptionsSuccess(mockedDownloadOptions);
    const result = <IProductNotificationsState>productNotificationsReducers(initialState, action);
    expect(result.downloadOptions).toBeDefined();
  });

  it(`GET_NOTIFICATION_DOWNLOAD_OPTIONS_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new GetNotificationDownloadOptionsFailed(error);
    const result = productNotificationsReducers(initialState, action);
    expect(result.downloadError).toBeDefined();
  });

  it('`GET_NOTIFICATION_DOWNLOAD_PREFERENCES` should set `loading` to true and not have an error property', () => {
    const action = new GetNotificationDownloadPreferences();
    const result = productNotificationsReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.downloadError).toBeUndefined();
  });

  it('`GET_NOTIFICATION_DOWNLOAD_PREFERENCES_SUCCESS` should set `downloadPreferences`', () => {
    const action = new GetNotificationDownloadPreferencesSuccess(mockedProductNotificationsDownloadUserPreferences);
    const result = <IProductNotificationsState>productNotificationsReducers(initialState, action);
    expect(result.downloadPreferences).toBeDefined();
  });

  it(`GET_NOTIFICATION_DOWNLOAD_PREFERENCES_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new GetNotificationDownloadPreferencesFailed(error);
    const result = productNotificationsReducers(initialState, action);
    expect(result.downloadError).toBeDefined();
  });

  it('`SET_NOTIFICATION_DOWNLOAD_PREFERENCES` should set `loading` to true and not have an error property', () => {
    const action = new SetNotificationDownloadPreferences(mockedUserPreferences);
    const result = productNotificationsReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.downloadError).toBeUndefined();
  });

  it('`SET_NOTIFICATION_DOWNLOAD_PREFERENCES_SUCCESS` should set `preferences`', () => {
    const action = new SetNotificationDownloadPreferencesSuccess(mockedProductNotificationsDownloadUserPreferences);
    const result = <IProductNotificationsState>productNotificationsReducers(initialState, action);
    expect(result.downloadPreferences).toBeDefined();
  });

  it(`SET_NOTIFICATION_DOWNLOAD_PREFERENCES_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new SetNotificationDownloadPreferencesFailed(error);
    const result = productNotificationsReducers(initialState, action);
    expect(result.downloadError).toBeDefined();
  });

  it('`GET_NOTIFICATION_DOWNLOAD_FILE` should set `loading` to true and not have an error property', () => {
    const action = new GetNotificationDownloadFile(mockedProductNotificationsDownloadParams);
    const result = productNotificationsReducers(initialState, action);
    expect(result.loading).toBeTruthy();
    expect(result.downloadError).toBeUndefined();
  });

  it('`GetNotificationDownloadFileSuccess` should set `preferences`', () => {
    const action = new GetNotificationDownloadFileSuccess(200);
    const result = <IProductNotificationsState>productNotificationsReducers(initialState, action);
    expect(result.downloadStatus).toBeDefined();
  });

  it(`GET_NOTIFICATION_DOWNLOAD_FILE_FAILED error property should have an error property`, () => {
    const error = new Error('foo');
    const action = new GetNotificationDownloadFileFailed(error);
    const result = productNotificationsReducers(initialState, action);
    expect(result.downloadError).toBeDefined();
  });
});
