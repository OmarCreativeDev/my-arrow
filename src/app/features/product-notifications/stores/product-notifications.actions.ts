import { Action } from '@ngrx/store';

import {
  IProductNotifications,
  IProductNotificationsPreferences,
  IProductNotificationsDownloadUserPreferences,
  IProductNotificationsUserPreferences,
  IProductNotificationsDownloadParams,
  IProductNotificationsSorted,
} from '@app/core/product-notifications/product-notifications.interfaces';

export enum ProductNotificationsActionTypes {
  GET_NOTIFICATIONS = 'GET_NOTIFICATIONS',
  GET_NOTIFICATIONS_SUCCESS = 'GET_NOTIFICATIONS_SUCCESS',
  GET_NOTIFICATIONS_FAILED = 'GET_NOTIFICATIONS_FAILED',
  GET_NOTIFICATION_PREFERENCES = 'GET_NOTIFICATION_PREFERENCES',
  GET_NOTIFICATION_PREFERENCES_SUCCESS = 'GET_NOTIFICATION_PREFERENCES_SUCCESS',
  GET_NOTIFICATION_PREFERENCES_FAILED = 'GET_NOTIFICATION_PREFERENCES_FAILED',
  SET_NOTIFICATION_PREFERENCES = 'SET_NOTIFICATION_PREFERENCES',
  SET_NOTIFICATION_PREFERENCES_SUCCESS = 'SET_NOTIFICATION_PREFERENCES_SUCCESS',
  SET_NOTIFICATION_PREFERENCES_FAILED = 'SET_NOTIFICATION_PREFERENCES_FAILED',

  GET_NOTIFICATION_DOWNLOAD_OPTIONS = 'GET_NOTIFICATION_DOWNLOAD_OPTIONS',
  GET_NOTIFICATION_DOWNLOAD_OPTIONS_SUCCESS = 'GET_NOTIFICATION_DOWNLOAD_OPTIONS_SUCCESS',
  GET_NOTIFICATION_DOWNLOAD_OPTIONS_FAILED = 'GET_NOTIFICATION_DOWNLOAD_OPTIONS_FAILED',
  GET_NOTIFICATION_DOWNLOAD_PREFERENCES = 'GET_NOTIFICATION_DOWNLOAD_PREFERENCES',
  GET_NOTIFICATION_DOWNLOAD_PREFERENCES_SUCCESS = 'GET_NOTIFICATION_DOWNLOAD_PREFERENCES_SUCCESS',
  GET_NOTIFICATION_DOWNLOAD_PREFERENCES_FAILED = 'GET_NOTIFICATION_DOWNLOAD_PREFERENCES_FAILED',
  SET_NOTIFICATION_DOWNLOAD_PREFERENCES = 'SET_NOTIFICATION_DOWNLOAD_PREFERENCES',
  SET_NOTIFICATION_DOWNLOAD_PREFERENCES_SUCCESS = 'SET_NOTIFICATION_DOWNLOAD_PREFERENCES_SUCCESS',
  SET_NOTIFICATION_DOWNLOAD_PREFERENCES_FAILED = 'SET_NOTIFICATION_DOWNLOAD_PREFERENCES_FAILED',
  GET_NOTIFICATION_DOWNLOAD_FILE = 'GET_NOTIFICATION_DOWNLOAD_FILE',
  GET_NOTIFICATION_DOWNLOAD_FILE_SUCCESS = 'GET_NOTIFICATION_DOWNLOAD_FILE_SUCCESS',
  GET_NOTIFICATION_DOWNLOAD_FILE_FAILED = 'GET_NOTIFICATION_DOWNLOAD_FILE_FAILED',
  GET_NOTIFICATION_DOWNLOAD_FILE_RESET = 'GET_NOTIFICATION_DOWNLOAD_FILE_RESET',
}

/**
 * * Get Notifications
 */
export class GetNotifications implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATIONS;
  constructor(public payload: { custAccountId: number; startDate: string; endDate: string }) {}
}

/**
 * * Get Notifications Success
 */
export class GetNotificationsSuccess implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATIONS_SUCCESS;
  constructor(public payload: IProductNotificationsSorted) {}
}

/**
 * * Get Quote Carts Failed
 */
export class GetNotificationsFailed implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATIONS_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Get Notification Preferences
 */
export class GetNotificationPreferences implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATION_PREFERENCES;
  constructor(
    public payload: {
      custAcctSiteId: number;
      custPartyId: number;
      customerId: number;
    }
  ) {}
}

/**
 * * Get Notifications Success
 */
export class GetNotificationPreferencesSuccess implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATION_PREFERENCES_SUCCESS;
  constructor(public payload: IProductNotifications) {}
}

/**
 * * Get Quote Carts Failed
 */
export class GetNotificationPreferencesFailed implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATION_PREFERENCES_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Set Notification Preferences
 */
export class SetNotificationPreferences implements Action {
  readonly type = ProductNotificationsActionTypes.SET_NOTIFICATION_PREFERENCES;
  constructor(public payload: IProductNotificationsPreferences) {}
}

/**
 * * Set Notifications Success
 */
export class SetNotificationPreferencesSuccess implements Action {
  readonly type = ProductNotificationsActionTypes.SET_NOTIFICATION_PREFERENCES_SUCCESS;
  constructor() {}
}

/**
 * * Set Notifications Failed
 */
export class SetNotificationPreferencesFailed implements Action {
  readonly type = ProductNotificationsActionTypes.SET_NOTIFICATION_PREFERENCES_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Get Notification Download Preferences
 */
export class GetNotificationDownloadOptions implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_OPTIONS;
  constructor() {}
}

/**
 * * Get Notification Download Success
 */
export class GetNotificationDownloadOptionsSuccess implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_OPTIONS_SUCCESS;
  constructor(public payload: IProductNotificationsUserPreferences) {}
}

/**
 * * Get Notification Download Failed
 */
export class GetNotificationDownloadOptionsFailed implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_OPTIONS_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Get Notification Download Preferences
 */
export class GetNotificationDownloadPreferences implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_PREFERENCES;
  constructor() {}
}

/**
 * * Get Notification Download Success
 */
export class GetNotificationDownloadPreferencesSuccess implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_PREFERENCES_SUCCESS;
  constructor(public payload: IProductNotificationsDownloadUserPreferences) {}
}

/**
 * * Get Notification Download Failed
 */
export class GetNotificationDownloadPreferencesFailed implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_PREFERENCES_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Set Notification Download Preferences
 */
export class SetNotificationDownloadPreferences implements Action {
  readonly type = ProductNotificationsActionTypes.SET_NOTIFICATION_DOWNLOAD_PREFERENCES;
  constructor(public payload: IProductNotificationsDownloadUserPreferences) {}
}

/**
 * * Set Notification Download Success
 */
export class SetNotificationDownloadPreferencesSuccess implements Action {
  readonly type = ProductNotificationsActionTypes.SET_NOTIFICATION_DOWNLOAD_PREFERENCES_SUCCESS;
  constructor(public payload: IProductNotificationsDownloadUserPreferences) {}
}

/**
 * * Set Notification Download Failed
 */
export class SetNotificationDownloadPreferencesFailed implements Action {
  readonly type = ProductNotificationsActionTypes.SET_NOTIFICATION_DOWNLOAD_PREFERENCES_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Get Notification Download File
 */
export class GetNotificationDownloadFile implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_FILE;
  constructor(public payload: IProductNotificationsDownloadParams) {}
}

/**
 * * Get Notification Download File Success
 */
export class GetNotificationDownloadFileSuccess implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_FILE_SUCCESS;
  constructor(public payload: number) {}
}

/**
 * * Get Notification Download File Failed
 */
export class GetNotificationDownloadFileFailed implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_FILE_FAILED;
  constructor(public payload: Error) {}
}

/**
 * * Reset Notification Download File
 */
export class GetNotificationDownloadFileReset implements Action {
  readonly type = ProductNotificationsActionTypes.GET_NOTIFICATION_DOWNLOAD_FILE_RESET;
  constructor() {}
}

export type ProductNotificationsActions =
  | GetNotifications
  | GetNotificationsSuccess
  | GetNotificationsFailed
  | GetNotificationPreferences
  | GetNotificationPreferencesSuccess
  | GetNotificationPreferencesFailed
  | SetNotificationPreferences
  | SetNotificationPreferencesSuccess
  | SetNotificationPreferencesFailed
  | GetNotificationDownloadOptions
  | GetNotificationDownloadOptionsSuccess
  | GetNotificationDownloadOptionsFailed
  | GetNotificationDownloadPreferences
  | GetNotificationDownloadPreferencesSuccess
  | GetNotificationDownloadPreferencesFailed
  | SetNotificationDownloadPreferences
  | SetNotificationDownloadPreferencesSuccess
  | SetNotificationDownloadPreferencesFailed
  | GetNotificationDownloadFile
  | GetNotificationDownloadFileSuccess
  | GetNotificationDownloadFileFailed
  | GetNotificationDownloadFileReset;
