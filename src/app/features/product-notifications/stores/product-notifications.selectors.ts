import { createFeatureSelector, createSelector } from '@ngrx/store';
import { IProductNotificationsState } from '@app/core/product-notifications/product-notifications.interfaces';

/**
 * Selects product notifications state from the root state object
 */
export const getProductNotificationsState = createFeatureSelector<IProductNotificationsState>('productNotifications');

export const getProductNotificationsSelector = createSelector(
  getProductNotificationsState,
  productNotificationsState => productNotificationsState.notifications
);

export const getProductNotificationPreferencesSelector = createSelector(
  getProductNotificationsState,
  productNotificationsState => productNotificationsState.notificationPreferences
);

export const setProductNotificationPreferencesSelector = createSelector(
  getProductNotificationsState,
  productNotificationsState => productNotificationsState.setNotificationPreferences
);

export const getProductNotificationDownloadOptionsSelector = createSelector(
  getProductNotificationsState,
  productNotificationsState => productNotificationsState.downloadOptions
);

export const getProductNotificationDownloadPreferencesSelector = createSelector(
  getProductNotificationsState,
  productNotificationsState => productNotificationsState.downloadPreferences
);

export const getProductNotificationErrorSelector = createSelector(
  getProductNotificationsState,
  productNotificationsState => productNotificationsState.error
);

export const getProductNotificationPreferencesErrorSelector = createSelector(
  getProductNotificationsState,
  productNotificationsState => productNotificationsState.notificationPreferencesError
);

export const getProductNotificationPreferencesSuccessSelector = createSelector(
  getProductNotificationsState,
  productNotificationsState => productNotificationsState.notificationPreferencesSuccess
);

export const getProductDownloadErrorSelector = createSelector(
  getProductNotificationsState,
  productNotificationsState => productNotificationsState.downloadError
);

export const getProductDownloadStatus = createSelector(
  getProductNotificationsState,
  productNotificationsState => productNotificationsState.downloadStatus
);
