import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from '@app/shared/shared.module';
import { ProductNotificationsService } from '@app/core/product-notifications/product-notifications.service';
import { ProductNotificationsRoutingModule } from './product-notifications-routing.module';
import { ProductNotificationsComponent } from './pages/product-notifications/product-notifications.component';
import { ProductNotificationTableComponent } from '@app/features/product-notifications/components/product-notification-table/product-notification-table.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    ProductNotificationsRoutingModule,
    RouterModule,
  ],
  declarations: [ProductNotificationsComponent, ProductNotificationTableComponent, ProductNotificationTableComponent],
  providers: [ProductNotificationsService],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ProductNotificationsModule {}
