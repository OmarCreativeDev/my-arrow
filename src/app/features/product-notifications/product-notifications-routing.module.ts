import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductNotificationsComponent } from './pages/product-notifications/product-notifications.component';

const productNotificationRoutes: Routes = [{ path: '', component: ProductNotificationsComponent, data: { title: 'Notifications' } }];

@NgModule({
  imports: [RouterModule.forChild(productNotificationRoutes)],
  exports: [RouterModule],
})
export class ProductNotificationsRoutingModule {}
