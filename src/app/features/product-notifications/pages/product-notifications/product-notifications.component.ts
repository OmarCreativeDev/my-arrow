import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';

import { IProductNotification } from '@app/core/product-notifications/product-notifications.interfaces';
import { ISelectableOption } from '@app/shared/shared.interfaces';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DownloadHistoryComponent } from '@app/features/product-notifications/components/download-history/download-history.component';
import { ProductNotificationsSettingsComponent } from '@app/features/product-notifications/components/product-notifications-settings/product-notifications-settings.component';
import { getProductNotificationsSelector } from '@app/features/product-notifications/stores/product-notifications.selectors';
import { GetNotifications } from '@app/features/product-notifications/stores/product-notifications.actions';
import { IAppState } from '@app/shared/shared.interfaces';
import { getAccountId } from '@app/core/user/store/user.selectors';

import * as moment from 'moment';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-product-notifications',
  templateUrl: './product-notifications.component.html',
  styleUrls: ['./product-notifications.component.scss'],
})
export class ProductNotificationsComponent implements OnInit, OnDestroy {
  DATE_FORMAT: string = 'YYYY-MM-DD';
  showNotificationMessage: boolean = true;

  filterOptions: Array<ISelectableOption<any>> = [
    {
      label: 'Show All',
      value: 'All',
    },
    {
      label: 'Product Status Change',
      value: '1',
    },
    {
      label: 'Product Non-Functional Change',
      value: '2',
    },
    {
      label: 'Product Manufacturing Change',
      value: '3',
    },
  ];
  selectedTable: string = 'All';

  downloadDateRange: number[] = [6, 12, 24];
  custAccountId: number;
  startDate = moment(new Date())
    .subtract(1, 'months')
    .format(this.DATE_FORMAT);
  endDate = moment(new Date())
    .startOf('day')
    .format(this.DATE_FORMAT);

  pscProductNotifications: IProductNotification[] = [];
  pnfProductNotifications: IProductNotification[] = [];
  pmProductNotifications: IProductNotification[] = [];

  noProductNotifications = "There are no PCN's at this time";
  notifications$: any;

  private subscription: Subscription = new Subscription();

  accountId$: Observable<number>;

  constructor(private store: Store<IAppState>, private dialogService: DialogService) {
    this.notifications$ = store.pipe(select(getProductNotificationsSelector));
    this.accountId$ = this.store.pipe(select(getAccountId));
  }

  ngOnInit(): void {
    this.startSubscriptions();
    this.getNotifications();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public getNotifications(): void {
    this.store.dispatch(new GetNotifications({ custAccountId: this.custAccountId, startDate: this.startDate, endDate: this.endDate }));
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeAccountId());
    this.subscription.add(this.subscribeNotifications());
  }

  private subscribeAccountId(): Subscription {
    return this.accountId$.subscribe(accountId => {
      this.custAccountId = accountId;
    });
  }

  private subscribeNotifications(): Subscription {
    return this.notifications$.subscribe(notifications => {
      this.pscProductNotifications = notifications.pscProductNotifications;
      this.pnfProductNotifications = notifications.pnfProductNotifications;
      this.pmProductNotifications = notifications.pmProductNotifications;
    });
  }

  openSettingsModalDialog(): void {
    this.dialogService.open(ProductNotificationsSettingsComponent, {
      size: 'large',
    });
  }

  openDownloadModalDialog(): void {
    this.dialogService.open(DownloadHistoryComponent, {
      size: 'x-large',
    });
  }

  hideNotificationMessage(): void {
    this.showNotificationMessage = false;
  }
}
