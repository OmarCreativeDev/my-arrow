import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';
import { of, from } from 'rxjs';

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Dialog, DialogService } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { MockedProductNotifications } from '@app/core/product-notifications/product-notifications.service.spec';
import { ProductNotificationsComponent } from './product-notifications.component';
import { productNotificationsReducers } from '@app/features/product-notifications/stores/product-notifications.reducers';

class MockStore {
  select() {
    return of();
  }
  dispatch() {}
  pipe() {
    return of();
  }
}

describe('ProductNotificationsComponent', () => {
  let component: ProductNotificationsComponent;
  let fixture: ComponentFixture<ProductNotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductNotificationsComponent],
      imports: [StoreModule.forRoot({ productNotifications: productNotificationsReducers })],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: Dialog, useClass: DialogMock },
        { provide: DialogService, useClass: DialogServiceMock },
        { provide: Store, useClass: MockStore },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductNotificationsComponent);
    component = fixture.componentInstance;
    component.notifications$ = from(MockedProductNotifications);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
