import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SSOLoginComponent } from '@app/features/public-interstitials/pages/ssologin/ssologin.component';

import { SSOLoginGuard } from '@app/core/ssologin/ssologin.guard';

const routes: Routes = [{ path: 'ssologin', component: SSOLoginComponent, canActivate: [SSOLoginGuard] }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class UserInterstitialsRoutingModule {}
