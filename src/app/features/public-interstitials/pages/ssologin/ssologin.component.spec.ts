import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SSOLoginComponent } from './ssologin.component';
import { SharedModule } from '@app/shared/shared.module';
import { of } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { StoreModule, Store } from '@ngrx/store';
import { AuthService } from '@app/core/auth/auth.service';
import { SSOLoginState } from '@app/features/public-interstitials/pages/ssologin/ssologin.interface';
import { userReducers } from '@app/core/user/store/user.reducers';
import { IAppState } from '@app/shared/shared.interfaces';
import { UserAfterLoggedIn } from '@app/core/user/store/user.actions';

class ActivatedRouteMock {
  public get queryParams() {
    return of([]);
  }
}

class MockRouter {
  navigate(url: string) {
    return url;
  }

  navigateByUrl(url: string) {
    return url;
  }
}

class MockAuthService {
  loginWithClientId() {
    return of([]);
  }
}

describe('SSOLoginComponent', () => {
  let component: SSOLoginComponent;
  let fixture: ComponentFixture<SSOLoginComponent>;
  let store: Store<IAppState>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SSOLoginComponent],
      imports: [SharedModule, StoreModule.forRoot({ user: userReducers })],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteMock },
        { provide: AuthService, useClass: MockAuthService },
        { provide: Router, useClass: MockRouter },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SSOLoginComponent);
    store = TestBed.get(Store);
    component = fixture.componentInstance;

    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('isLoading should return true if state is equal to SSOLoginState.loading', () => {
    component.state = SSOLoginState.loading;
    const result = component.isLoading();
    expect(result).toBeTruthy();
  });

  it('isLoading should return false if state is not equal to SSOLoginState.loading', () => {
    component.state = SSOLoginState.error;
    const result = component.isLoading();
    expect(result).toBeFalsy();
  });

  describe('#login', () => {
    it('Should call UserAfterLoggedIn to log in an user', () => {
      const username = 'david.lane@oncorems.com';
      const password = 'Myarrow123';
      const accessId = 'Z17167';
      component.login(username, password, accessId);

      const action = new UserAfterLoggedIn();

      expect(store.dispatch).toHaveBeenCalledWith(action);
    });
  });
});
