import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { SSO_CLIENT_ID } from '@env/environment';
import { AuthService } from '@app/core/auth/auth.service.ts';
import { SSOLoginState } from '@app/features/public-interstitials/pages/ssologin/ssologin.interface';
import { IAppState } from '@app/shared/shared.interfaces';
import { UserAfterLoggedIn } from '@app/core/user/store/user.actions';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-ssologin',
  templateUrl: './ssologin.component.html',
  styleUrls: ['./ssologin.component.scss'],
})
export class SSOLoginComponent implements OnInit, OnDestroy {
  public state: SSOLoginState = SSOLoginState.loading;
  private subscription: Subscription = new Subscription();

  constructor(
    private activatedRoute: ActivatedRoute,
    private authService: AuthService,
    private router: Router,
    private store: Store<IAppState>
  ) {}

  ngOnInit() {
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public isLoading(): boolean {
    return this.state === SSOLoginState.loading;
  }

  public login(username: string, password: string, accessId: string): void {
    this.authService.loginWithClientId(username, password, SSO_CLIENT_ID, accessId).subscribe(
      () => this.store.dispatch(new UserAfterLoggedIn()),
      () => {
        this.state = SSOLoginState.error;
        this.router.navigate(['./'], { relativeTo: this.activatedRoute });
      }
    );
  }

  public startSubscriptions(): void {
    this.subscription.add(this.subscribeSctivatedRoute());
  }

  private subscribeSctivatedRoute(): Subscription {
    return this.activatedRoute.queryParams.subscribe((params: Params) => {
      const { cookieval, accessid } = params;
      return this.login(SSO_CLIENT_ID, cookieval, accessid);
    });
  }

  public sendToLogin() {
    this.authService.resetAuthTokensAndUserProfile();
  }
}
