import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SSOLoginComponent } from '@app/features/public-interstitials/pages/ssologin/ssologin.component';
import { RouterModule } from '@angular/router';
import { UserInterstitialsRoutingModule } from '@app/features/public-interstitials/public-interstitials-routing.module';
import { SharedModule } from '@app/shared/shared.module';
import { SSOLoginGuard } from '@app/core/ssologin/ssologin.guard';

@NgModule({
  imports: [CommonModule, RouterModule, UserInterstitialsRoutingModule, SharedModule],
  declarations: [SSOLoginComponent],
  providers: [SSOLoginGuard],
})
export class PublicInterstitialsModule {}
