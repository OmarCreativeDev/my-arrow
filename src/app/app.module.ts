import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { CoreModule } from './core/core.module';
import { LayoutModule } from './layout/layout.module';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TrimValueAccessorModule } from 'ng-trim-value-accessor';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '@env/environment';

import { ErrorsModule } from '@app/features/errors/errors.module';
import { ToastrModule, ToastNoAnimationModule, ToastContainerModule } from 'ngx-toastr';
import { ToastComponent } from '@app/shared/components/toast/toast.component';

import { reducers } from '@app/stores/reducers';
import { metaReducers } from '@app/stores/metareducers';
import { effects } from '@app/stores/effects';
import { PropertiesModule } from '@app/features/properties/properties.module';
import { TitleService } from '@app/shared/services/title.service';
import { WSSService } from '@app/core/ws/wss.service';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CoreModule,
    ErrorsModule,
    LayoutModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot(effects),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    TrimValueAccessorModule,
    PropertiesModule,
    ToastNoAnimationModule,
    ToastrModule.forRoot({
      positionClass: 'toast-top-center',
      toastComponent: ToastComponent,
    }),
    ToastContainerModule,
  ],
  providers: [TitleService, WSSService],
  bootstrap: [AppComponent],
})
export class AppModule {}
