import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule } from '@ngrx/store';
import { CoreModule } from '../../core/core.module';
import { SharedModule } from '../../shared/shared.module';
import { LoggedInComponent } from './logged-in.component';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { BackdropServiceMock } from '@app/app.component.spec';
import { userReducers } from '@app/core/user/store/user.reducers';
import { cartReducers } from '@app/features/cart/stores/cart/cart.reducers';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { RequestUserComplete } from '@app/core/user/store/user.actions';
import { IUser } from '@app/core/user/user.interface';
import { CustomerBroadcastService } from '@app/core/customer-broadcast/customer-broadcast.service';
import { of } from 'rxjs';
import { cloneDeep } from 'lodash-es';
import { ICustomerBroadcastMessage } from '@app/core/customer-broadcast/customer-broadcast.interface';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';
import { propertiesReducers } from '@app/features/properties/store/properties.reducers';
import { mockPrivateProperties } from '@app/core/features/feature-flags.mock';
import { GetPrivatePropertiesSuccess } from '@app/features/properties/store/properties.actions';

describe('LoggedInComponent', () => {
  let component: LoggedInComponent;
  let fixture: ComponentFixture<LoggedInComponent>;
  let dialogService: DialogService;
  let customerBroadcastService: CustomerBroadcastService;
  let store: Store<any>;

  const mockedCustomerBroadcastMessage: ICustomerBroadcastMessage = {
    id: 'id',
    title: 'title',
    content: 'content',
  };

  const mockPropertiesReducers = () => ({
    ...propertiesReducers,
    private: mockPrivateProperties,
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        RouterTestingModule,
        SharedModule,

        StoreModule.forRoot({
          user: userReducers,
          cart: cartReducers,
          properties: mockPropertiesReducers,
        }),
      ],
      declarations: [LoggedInComponent],
      providers: [
        CustomerBroadcastService,
        { provide: BackdropService, useClass: BackdropServiceMock },
        { provide: DialogService, useClass: DialogServiceMock },
        { provide: WSSService, useClass: MockStompWSSService },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoggedInComponent);
    component = fixture.componentInstance;
    dialogService = TestBed.get(DialogService);
    customerBroadcastService = TestBed.get(CustomerBroadcastService);
    spyOn(customerBroadcastService, 'getMessage').and.callFake(() => of(mockedCustomerBroadcastMessage));
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should use the Dialog Service to open a Bill To Accounts dialog', () => {
    spyOn(dialogService, 'open').and.callThrough();
    component.openBillToDialog();
    expect(dialogService.open).toHaveBeenCalled();
  });

  describe('when the customerBroadcast feature flag is active ', () => {
    beforeEach(() => {
      const mockProps = cloneDeep(mockPrivateProperties);
      mockProps.featureFlags.customerBroadcast = true;

      sessionStorage.clear();
      sessionStorage.setItem(component.SHOW_CUSTOMER_BROADCAST_DIALOG, 'true');
      store.dispatch(new GetPrivatePropertiesSuccess(mockProps));
      store.dispatch(
        new RequestUserComplete({
          defaultLanguage: 'en',
          region: 'arrowna',
        } as IUser)
      );
    });

    it('should call the getMessage method', fakeAsync(() => {
      component.privateFeaturesFlags$.subscribe(featureFlags => {
        expect(featureFlags.customerBroadcast).toBeTruthy();
        expect(customerBroadcastService.getMessage).toHaveBeenCalled();
      });
    }));

    it('should not call the getMessage method when the localStorage flag is false', () => {
      sessionStorage.setItem(component.SHOW_CUSTOMER_BROADCAST_DIALOG, 'false');
      fakeAsync(() => {
        component.user$.subscribe(user => {
          expect(customerBroadcastService.getMessage).not.toHaveBeenCalled();
        });
      });
    });

    it('should open a dialog when the getMessage method returns a message', fakeAsync(() => {
      spyOn(dialogService, 'open');
      component.subscribeToCustomerBroadcastService({
        defaultLanguage: 'en',
        region: 'arrowna',
      } as IUser);
      expect(dialogService.open).toHaveBeenCalled();
    }));
  });
});
