import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';

import { IAppState } from '@app/shared/shared.interfaces';
import { IUser } from '@app/core/user/user.interface';
import { getUser } from '@app/core/user/store/user.selectors';
import { getUserBillToAccount } from '@app/core/user/store/user.selectors';

import { DialogService } from '@app/core/dialog/dialog.service';
import { BillToAccountsComponent } from '@app/shared/components/bill-to-accounts/bill-to-accounts.component';

import { SESSION_TIMEOUT_MILLISECONDS } from '@env/environment.ts';
import { GetQuoteCarts } from '@app/features/quotes/stores/quote-cart.actions';
import { ICustomerBroadcastMessage } from '@app/core/customer-broadcast/customer-broadcast.interface';
import { CustomerBroadcastDialogComponent } from '@app/features/dashboard/components/customer-broadcast-dialog/customer-broadcast-dialog.component';
import { CustomerBroadcastService } from '@app/core/customer-broadcast/customer-broadcast.service';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';
import { WSSService } from '@app/core/ws/wss.service';
import { isEmpty } from 'lodash-es';
import { UserLoggedOut } from '@app/core/user/store/user.actions';
import { ReasonsForLogout } from '@app/core/analytics/meta-reducers/analytics.user.enum';

@Component({
  selector: 'app-logged-in',
  templateUrl: './logged-in.component.html',
  styleUrls: ['./logged-in.component.scss'],
})
export class LoggedInComponent implements OnInit, OnDestroy {
  public user$: Observable<IUser>;
  public privateFeaturesFlags$: Observable<any>;
  public billToAccount$: Observable<number>;
  public SHOW_CUSTOMER_BROADCAST_DIALOG: string = 'showCustomerBroadcastDialog';
  private inactivityIntervalId;
  private billToAccount: number;
  private subscription: Subscription = new Subscription();

  constructor(
    private store: Store<IAppState>,
    private dialogService: DialogService,
    private customerBroadcastService: CustomerBroadcastService,
    private wssService: WSSService
  ) {
    this.billToAccount$ = this.store.pipe(select(getUserBillToAccount));
    this.privateFeaturesFlags$ = this.store.pipe(select(getPrivateFeatureFlagsSelector));
    this.user$ = this.store.pipe(select(getUser));
    this.wssService.requestConnect();
  }

  ngOnInit() {
    this.store.dispatch(new GetQuoteCarts());
    this.startInactivityTimer();
    if (!sessionStorage.getItem(this.SHOW_CUSTOMER_BROADCAST_DIALOG)) {
      sessionStorage.setItem(this.SHOW_CUSTOMER_BROADCAST_DIALOG, 'true');
    }
    this.startSubscriptions();
  }

  ngOnDestroy() {
    this.cleanInactivityTimer();
    sessionStorage.removeItem(this.SHOW_CUSTOMER_BROADCAST_DIALOG);
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public startInactivityTimer() {
    this.inactivityIntervalId = setInterval(() => {
      this.store.dispatch(new UserLoggedOut(ReasonsForLogout.USER_INACTIVITY));
    }, SESSION_TIMEOUT_MILLISECONDS);
  }

  public cleanInactivityTimer() {
    if (this.inactivityIntervalId) {
      clearInterval(this.inactivityIntervalId);
    }
  }

  public restartInactivityTimer() {
    this.cleanInactivityTimer();
    this.startInactivityTimer();
  }

  public openBillToDialog(): void {
    this.dialogService.open(BillToAccountsComponent, {
      data: this.billToAccount,
    });
  }

  public subscribeToCustomerBroadcastService(user: IUser): Subscription {
    return this.customerBroadcastService.getMessage(user).subscribe((data: ICustomerBroadcastMessage) => {
      this.showCustomerBroadcastDialog(data);
    });
  }

  private showCustomerBroadcastDialog(data: ICustomerBroadcastMessage): void {
    if (data) {
      sessionStorage.setItem(this.SHOW_CUSTOMER_BROADCAST_DIALOG, 'false');
      this.dialogService.open(CustomerBroadcastDialogComponent, {
        size: 'medium',
        data,
      });
    }
  }

  private subscribeToFeatureFlag(): Subscription {
    return this.privateFeaturesFlags$.subscribe(privateFeatureFlags => {
      if (!isEmpty(privateFeatureFlags) && privateFeatureFlags.customerBroadcast) {
        this.subscription.add(this.subscribeToUserProfile());
      }
    });
  }

  private subscribeToUserProfile(): Subscription {
    return this.user$.subscribe(user => {
      if (user && JSON.parse(sessionStorage.getItem(this.SHOW_CUSTOMER_BROADCAST_DIALOG))) {
        this.subscription.add(this.subscribeToCustomerBroadcastService(user));
      }
    });
  }

  private subscribeToBillToAccount(): Subscription {
    return this.billToAccount$.subscribe(billToAccount => {
      this.billToAccount = billToAccount;
    });
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeToBillToAccount());
    this.subscription.add(this.subscribeToFeatureFlag());
  }
}
