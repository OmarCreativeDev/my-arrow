import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';

import { IAppState } from '@app/shared/shared.interfaces';
import { getUserLoggedIn, getUserEmail } from '@app/core/user/store/user.selectors';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
})
export class FooterComponent implements OnInit, OnDestroy {
  private getUserLoggedIn$: Observable<boolean>;
  public subscription: Subscription = new Subscription();
  public loggedIn: boolean = null;
  public userEmail: string;
  private userEmail$: Observable<string>;
  public year = new Date().getFullYear();

  constructor(private store: Store<IAppState>) {
    this.getUserLoggedIn$ = this.store.pipe(select(getUserLoggedIn));
    this.userEmail$ = this.store.pipe(select(getUserEmail));
  }

  public ngOnInit(): void {
    this.startSubscriptions();
  }

  public ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.assignLoggedIn());
    this.subscription.add(this.assignUserEmail());
  }

  assignUserEmail(): Subscription {
    return this.userEmail$.pipe(take(1)).subscribe(userEmail => {
      this.userEmail = userEmail;
    });
  }

  assignLoggedIn(): Subscription {
    return this.getUserLoggedIn$.subscribe(loggedIn => {
      this.loggedIn = loggedIn;
    });
  }
}
