import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store, StoreModule } from '@ngrx/store';

import { FooterComponent } from './footer.component';
import { userReducers } from '@app/core/user/store/user.reducers';
import { mockUser } from '@app/core/user/user.service.spec';
import { RequestUserComplete } from '@app/core/user/store/user.actions';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FooterComponent],
      imports: [StoreModule.forRoot({ user: userReducers })],
    }).compileComponents();
    store = TestBed.get(Store);
    store.dispatch(new RequestUserComplete(mockUser));
    spyOn(store, 'dispatch').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
