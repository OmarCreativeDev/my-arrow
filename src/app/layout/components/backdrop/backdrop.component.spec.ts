import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@app/core/core.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { BackdropComponent } from './backdrop.component';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { BackdropServiceMock, ModalServiceMock } from '@app/app.component.spec';
import { ModalService } from '@app/shared/services/modal.service';


describe('BackdropComponent', () => {
  let backdropService: BackdropService;
  let component: BackdropComponent;
  let fixture: ComponentFixture<BackdropComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule, CoreModule],
      declarations: [BackdropComponent],
      providers: [
        { provide: BackdropService, useClass: BackdropServiceMock },
        { provide: ModalService, useClass: ModalServiceMock }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    backdropService = TestBed.get(BackdropService);
    fixture = TestBed.createComponent(BackdropComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should hide backdrop when clicking on it if modal is not open', () => {
    component.modalVisible = false;
    spyOn(backdropService, 'hide');
    component.hideBackdrop();
    expect(backdropService.hide).toHaveBeenCalled();
  });
});
