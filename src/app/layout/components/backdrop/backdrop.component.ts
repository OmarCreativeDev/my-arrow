import { Component, OnDestroy, OnInit } from '@angular/core';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { Subscription } from 'rxjs';
import { ModalService } from '@app/shared/services/modal.service';

@Component({
  selector: 'app-backdrop',
  templateUrl: './backdrop.component.html',
  styleUrls: ['./backdrop.component.scss'],
})
export class BackdropComponent implements OnInit, OnDestroy {
  public backdropVisibleSubscription: Subscription;
  public backdropVisible: boolean = null;

  public modalVisibleSubscription: Subscription;
  public modalVisible: boolean = null;

  public subscription: Subscription = new Subscription();

  constructor(private backdropService: BackdropService, private modalService: ModalService) {}

  public ngOnInit(): void {
    this.startSubscriptions();
  }

  private startSubscriptions() {
    this.subscription.add(this.setupBackdropSubscription());
    this.subscription.add(this.setupModalSubscription());
  }

  public setupBackdropSubscription(): Subscription {
    return this.backdropService.isVisible.subscribe(value => {
      if (value) {
        document.body.classList.add('backdrop-open');
      } else {
        document.body.classList.remove('backdrop-open');
      }
      this.backdropVisible = value;
    });
  }

  public setupModalSubscription(): Subscription {
    return this.modalService.isVisible.subscribe(value => {
      this.modalVisible = value;
    });
  }

  public hideBackdrop(): void {
    /* istanbul ignore else */
    if (!this.modalVisible) {
      this.backdropService.hide();
    }
  }

  public ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }
}
