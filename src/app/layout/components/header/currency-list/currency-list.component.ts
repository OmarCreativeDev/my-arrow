import { Component, EventEmitter, Output, OnDestroy, OnInit } from '@angular/core';
import { UserActionTypes } from '@app/core/user/store/user.actions';
import { getCurrencyCode, getCurrencyList, getUserBillToAccount } from '@app/core/user/store/user.selectors';
import { getShoppingCartId, getQuotedItemCountSelector } from '@app/features/cart/stores/cart/cart.selectors';
import { ISelectableOption, IAppState } from '@app/shared/shared.interfaces';
import { ActionsSubject, Store, select } from '@ngrx/store';
import { getCurrencySymbol } from '@angular/common';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { PartialUpdateShoppingCart, GetQuotedItemCount } from '@app/features/cart/stores/cart/cart.actions';
import { ICartStatus } from '@app/core/cart/cart.interfaces';
import { getQuoteCartId } from '@app/features/quotes/stores/quote-cart.selectors';
import { UpdateQuoteCart } from '@app/features/quotes/stores/quote-cart.actions';
import { DialogService, Dialog } from '@app/core/dialog/dialog.service';
import { CurrencyChangeDialogComponent } from '../currency-change-dialog/currency-change-dialog.component';
import { take, filter, map } from 'rxjs/operators';
import { CurrencyWaitDialogComponent } from './currency-wait-dialog/currency-wait-dialog.component';

@Component({
  selector: 'app-currency-list',
  templateUrl: './currency-list.component.html',
  styleUrls: ['./currency-list.component.scss'],
})
export class CurrencyListComponent implements OnInit, OnDestroy {
  @Output()
  openChange = new EventEmitter<boolean>();

  public currencyCode$: Observable<string>;
  public currencyCode: string;
  public dropdownOptions$: Observable<ISelectableOption<any>[]>;
  private billToId$: Observable<number>;
  private shoppingCartId$: Observable<string>;
  private quoteCartId$: Observable<string>;
  public quotedItemCount$: Observable<number>;
  public quotedItemCount: number;
  public subscription: Subscription = new Subscription();
  public currencyWaitDialog: Dialog<CurrencyWaitDialogComponent>;
  public currencyChangeDialog: Dialog<CurrencyChangeDialogComponent>;

  constructor(private store: Store<IAppState>, private actionsSubj: ActionsSubject, private dialogService: DialogService) {
    this.shoppingCartId$ = store.pipe(select(getShoppingCartId));
    this.quoteCartId$ = store.pipe(select(getQuoteCartId));
    this.billToId$ = store.pipe(select(getUserBillToAccount));
    this.currencyCode$ = store.pipe(select(getCurrencyCode));
    this.quotedItemCount$ = store.pipe(select(getQuotedItemCountSelector));

    this.dropdownOptions$ = store.pipe(
      select(getCurrencyList),
      filter(x => typeof x !== 'undefined'),
      map(currencyList => {
        return currencyList.map((currency: string) => {
          return {
            label: `${currency} ${getCurrencySymbol(currency, 'narrow')}`,
            value: currency,
          };
        });
      })
    );
  }

  ngOnInit() {
    this.startSubscriptions();
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.getQuotedItemCount());
    this.subscription.add(this.getCurrencyCode());
  }

  public getCurrencyCode(): Subscription {
    return this.currencyCode$.subscribe(code => {
      this.currencyCode = code;
    });
  }

  public getQuotedItemCount(): Subscription {
    return this.shoppingCartId$.subscribe(shoppingCartId => {
      if (shoppingCartId) {
        this.store.dispatch(new GetQuotedItemCount({ shoppingCartId }));
      }
    });
  }

  public onSelect(value: string): void {
    this.subscription.add(this.checkQuotedItemCount(value));
  }

  public changeCurrency(value: string): void {
    if (this.currencyCode !== value) {
      this.subscription.add(this.subscribeCurrencyChangeAction());

      this.currencyWaitDialog = this.dialogService.open(CurrencyWaitDialogComponent, {
        data: value,
      });
    }
  }

  private subscribeCurrencyChangeAction(): Subscription {
    return this.actionsSubj
      .pipe(
        filter(action => action.type === UserActionTypes.USER_UPDATE_CURRENCY_CODE_COMPLETE),
        take(1)
      )
      .subscribe(() => {
        this.subscription.add(this.invalidateShoppingCart());
        this.subscription.add(this.invalidateQuoteCart());
        this.subscription.add(this.getQuotedItemCount());
      });
  }

  private invalidateShoppingCart(): Subscription {
    return combineLatest(this.billToId$, this.currencyCode$, this.shoppingCartId$).subscribe(latestValues => {
      const [billToId, currency, shoppingCartId] = latestValues;
      const updateShoppingCart = new PartialUpdateShoppingCart({
        billToId: billToId,
        currency: currency,
        shoppingCartId: shoppingCartId,
        validation: 'INVALID',
        status: ICartStatus.IN_PROGRESS,
      });

      this.store.dispatch(updateShoppingCart);
    });
  }

  private invalidateQuoteCart(): Subscription {
    return combineLatest(this.billToId$, this.currencyCode$, this.quoteCartId$).subscribe(latestValues => {
      const [billToId, currency, quoteCartId] = latestValues;
      const updateQuoteCart = new UpdateQuoteCart({
        billToId: billToId,
        currency: currency,
        cartId: quoteCartId,
      });

      this.store.dispatch(updateQuoteCart);
    });
  }

  public checkQuotedItemCount(value: string): Subscription {
    return this.quotedItemCount$.subscribe(quotedItemCount => {
      if (quotedItemCount > 0) {
        this.currencyChangeDialog = this.dialogService.open(CurrencyChangeDialogComponent, { size: 'medium' });
        this.currencyChangeDialog.afterClosed.pipe(take(1)).subscribe(selection => {
          if (selection.currencyChangeAccepted) {
            this.changeCurrency(value);
          }
        });
      } else {
        this.changeCurrency(value);
      }
    });
  }
}
