import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { CurrencyListComponent } from './currency-list.component';

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { mockUser } from '@app/core/user/user.service.spec';
import { userReducers } from '@app/core/user/store/user.reducers';
import { UpdateCurrencyCodeComplete } from '@app/core/user/store/user.actions';
import { StoreModule, Store } from '@ngrx/store';
import { RequestUserComplete } from '@app/core/user/store/user.actions';
import { cartReducers } from '@app/features/cart/stores/cart/cart.reducers';
import { quoteCartReducers } from '@app/features/quotes/stores/quote-cart.reducers';
import { CartService } from '@app/core/cart/cart.service';
import { PartialUpdateShoppingCart } from '@app/features/cart/stores/cart/cart.actions';
import { IShoppingCartPatchRequest, ICartStatus } from '@app/core/cart/cart.interfaces';
import { IQuoteCartUpdateRequest } from '@app/core/quote-cart/quote-cart.interfaces';
import { UpdateQuoteCart } from '@app/features/quotes/stores/quote-cart.actions';
import { QuoteCartService } from '@app/core/quote-cart/quote-cart.service';
import { SharedModule } from '@app/shared/shared.module';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of, Subscription } from 'rxjs';

export class BackdropServiceMock {
  public isVisible = of(true);
}

describe('CurrencyListComponent', () => {
  let component: CurrencyListComponent;
  let fixture: ComponentFixture<CurrencyListComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CurrencyListComponent],
      imports: [
        StoreModule.forRoot({
          user: userReducers,
          cart: cartReducers,
          quoteCart: quoteCartReducers,
        }),
        SharedModule,
        HttpClientTestingModule,
      ],
      providers: [
        {
          provide: BackdropService,
          useClass: BackdropServiceMock,
          CartService,
          QuoteCartService,
        },
        { provide: DialogService, useClass: DialogServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyListComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    store.dispatch(new RequestUserComplete(mockUser));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display correct currency', () => {
    component.currencyCode$.subscribe(currencyCode => {
      expect(currencyCode).toEqual('USD');
    });
  });

  it('should get quoted item count', () => {
    component.quotedItemCount$.subscribe(quotedItemCount => {
      expect(quotedItemCount).toEqual(0);
    });
  });

  it('should display correct currency dropdown options', () => {
    component.dropdownOptions$.subscribe(options => {
      expect(options).toEqual([{ label: 'EUR €', value: 'EUR' }, { label: 'GBP £', value: 'GBP' }, { label: 'USD $', value: 'USD' }]);
    });
  });

  it('should dispatch PartialUpdateShoppingCart if updating currency is successful', () => {
    const currencyVal = 'EUR';
    component.onSelect(currencyVal);
    store.dispatch(new UpdateCurrencyCodeComplete(currencyVal));
    const partialUpdateRequest: IShoppingCartPatchRequest = {
      billToId: 2171627,
      currency: 'EUR',
      shoppingCartId: undefined,
      validation: 'INVALID',
      status: ICartStatus.IN_PROGRESS,
    };
    expect(store.dispatch).toHaveBeenCalledWith(new PartialUpdateShoppingCart(partialUpdateRequest));
  });

  it('should dispatch UpdateQuoteCart if updating currency is successful', () => {
    const currencyVal = 'EUR';
    component.onSelect(currencyVal);
    store.dispatch(new UpdateCurrencyCodeComplete(currencyVal));
    const updateRequest: IQuoteCartUpdateRequest = {
      billToId: 2171627,
      currency: 'EUR',
      cartId: '',
    };
    expect(store.dispatch).toHaveBeenCalledWith(new UpdateQuoteCart(updateRequest));
  });

  it('#checkQuotedItemCount should return a subscription', () => {
    const subscription = new Subscription();
    spyOn(component, 'checkQuotedItemCount').and.returnValue(subscription);
  });
});
