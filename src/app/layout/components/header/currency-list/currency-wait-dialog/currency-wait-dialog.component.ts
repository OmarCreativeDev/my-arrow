import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { UpdateCurrencyCode } from '@app/core/user/store/user.actions';
import { getCurrencyCode } from '@app/core/user/store/user.selectors';
import { distinctUntilChanged, skip, take } from 'rxjs/operators';

@Component({
  selector: 'app-currency-wait-dialog',
  templateUrl: './currency-wait-dialog.component.html',
})
export class CurrencyWaitDialogComponent implements OnInit, OnDestroy {
  public currencyCode$: Observable<string>;
  public subscription: Subscription = new Subscription();
  public shouldRedirect: boolean = false;

  constructor(
    @Inject(APP_DIALOG_DATA) public data: string,
    public dialog: Dialog<CurrencyWaitDialogComponent>,
    private store: Store<IAppState>
  ) {
    this.currencyCode$ = store.pipe(select(getCurrencyCode));
  }

  public onClose(): void {
    this.dialog.close();
  }

  ngOnInit() {
    this.startSubscriptions();
    this.store.dispatch(new UpdateCurrencyCode(this.data));
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeCurrency());
  }

  public subscribeCurrency(): Subscription {
    return this.currencyCode$
      .pipe(
        skip(1),
        distinctUntilChanged(),
        take(1)
      )
      .subscribe(currencyCode => {
        this.dialog.close(currencyCode);
      });
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }
}
