import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { Store, StoreModule } from '@ngrx/store';
import { CurrencyPipe } from '@angular/common';
import { FormattedPricePipe } from '@app/shared/pipes/formatted-price/formatted-price.pipe';
import { Dialog, APP_DIALOG_DATA } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { CurrencyWaitDialogComponent } from './currency-wait-dialog.component';
import { UpdateCurrencyCode, RequestUserComplete } from '@app/core/user/store/user.actions';
import { userReducers } from '@app/core/user/store/user.reducers';
import { mockUser } from '@app/core/user/user.service.spec';
import { skip, distinctUntilChanged, take } from 'rxjs/operators';
import { EffectsModule } from '@ngrx/effects';
import { UserEffects } from '@app/core/user/store/user.effects';
import { AuthService } from '@app/core/auth/auth.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { ApiService } from '@app/core/api/api.service';
import { RouterTestingModule } from '@angular/router/testing';
import { CheckoutService } from '@app/core/checkout/checkout.service';
import { UserService } from '@app/core/user/user.service';
import { of } from 'rxjs';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';

const dialogData = 'EUR';

class DummyComponent {}

describe('CurrencyWaitDialogComponent', () => {
  let store: Store<any>;
  let component: CurrencyWaitDialogComponent;
  let fixture: ComponentFixture<CurrencyWaitDialogComponent>;
  let dialog: Dialog<CurrencyWaitDialogComponent>;
  let userService: UserService;

  const routes = [{ path: 'login', component: DummyComponent }];

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CurrencyWaitDialogComponent, FormattedPricePipe],
      imports: [
        RouterTestingModule.withRoutes(routes),
        HttpClientTestingModule,
        FormsModule,
        EffectsModule.forRoot([UserEffects]),
        StoreModule.forRoot({
          user: userReducers,
        }),
      ],
      providers: [
        AuthService,
        AuthTokenService,
        CurrencyPipe,
        ApiService,
        CheckoutService,
        UserService,
        { provide: Dialog, useClass: DialogMock },
        { provide: APP_DIALOG_DATA, useValue: { data: dialogData } },
        { provide: WSSService, useClass: MockStompWSSService },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    store = TestBed.get(Store);
    dialog = TestBed.get(Dialog);
    userService = TestBed.get(UserService);

    spyOn(store, 'dispatch').and.callThrough();
    spyOn(userService, 'updateProfile').and.returnValue(of({ accepted: true }));

    fixture = TestBed.createComponent(CurrencyWaitDialogComponent);
    component = fixture.componentInstance;
    component.data = dialogData;
    store.dispatch(new RequestUserComplete(mockUser));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dispatch UpdateCurrencyCode Action on ngOnInit()', () => {
    const action = new UpdateCurrencyCode(dialogData);
    component.ngOnInit();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('should set the new currency on UpdateCurrencyCode Action and close the dialog.', () => {
    component.ngOnInit();
    component.currencyCode$
      .pipe(
        skip(1),
        distinctUntilChanged(),
        take(1)
      )
      .subscribe(currency => {
        expect(currency).toBe(dialogData);
        expect(dialog.close).toHaveBeenCalled();
      });
  });

  it('should close the dialog', () => {
    spyOn(dialog, 'close');
    component.onClose();
    expect(dialog.close).toHaveBeenCalled();
  });
});
