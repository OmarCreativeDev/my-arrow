import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { StoreModule, Store } from '@ngrx/store';
import { ChatButtonComponent } from './chat-button.component';
import { ApiService } from '@app/core/api/api.service';
import { of } from 'rxjs';
import { WindowRefService } from '@app/core/window/window.service';
import { LiveChatService } from '@app/core/live-chat/live-chat.service';
import { RequestUserComplete } from '@app/core/user/store/user.actions';
import { userReducers } from '@app/core/user/store/user.reducers';
import { mockUser } from '@app/core/user/user.service.spec';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';

class ApiServiceMock {
  public get() {
    return of([]);
  }
}

describe('ChatButtonComponent', () => {
  let component: ChatButtonComponent;
  let fixture: ComponentFixture<ChatButtonComponent>;
  let windowRefService: WindowRefService;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChatButtonComponent],
      imports: [
        StoreModule.forRoot({
          user: userReducers,
        }),
      ],
      providers: [
        LiveChatService,
        { provide: ApiService, useClass: ApiServiceMock },
        { provide: DialogService, useClass: DialogServiceMock },
        WindowRefService,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChatButtonComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    windowRefService = TestBed.get(WindowRefService);
    windowRefService.nativeWindow.liveagent = {
      init() {},
      showWhenOnline() {},
      showWhenOffline() {},
      startOnlineChat() {},
    };
    store.dispatch(new RequestUserComplete(mockUser));
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
