import { Component, ElementRef, EventEmitter, OnDestroy, OnInit, Output, Renderer2, ViewChild } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { environment } from '@env/environment';
import { LiveChatService } from '@app/core/live-chat/live-chat.service';
import { IAppState } from '@app/shared/shared.interfaces';
import { IUser } from '@app/core/user/user.interface';
import { getUser } from '@app/core/user/store/user.selectors';
import { IPrechatData } from './chat-button.interface';
import { DialogService } from '@app/core/dialog/dialog.service';
import { OfflineChatComponent } from '@app/layout/components/header/offline-chat/offline-chat.component';

@Component({
  selector: 'app-chat-button',
  templateUrl: './chat-button.component.html',
  styleUrls: ['./chat-button.component.scss'],
})
export class ChatButtonComponent implements OnDestroy, OnInit {
  @ViewChild('online', { read: ElementRef })
  onlineButton: ElementRef;
  @ViewChild('offline', { read: ElementRef })
  offlineButton: ElementRef;
  public chatButtonId: string = environment.liveChatSettings.chatButtonId;
  private userProfile$: Observable<IUser>;
  private liveChatStarter$: Observable<any>;
  @Output()
  hideParent = new EventEmitter<void>();

  public subscription: Subscription = new Subscription();

  constructor(
    private store: Store<IAppState>,
    private liveChatService: LiveChatService,
    private renderer: Renderer2,
    private dialogService: DialogService
  ) {
    this.userProfile$ = this.store.pipe(select(getUser));
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeUserProfile());
  }

  private subscribeUserProfile(): Subscription {
    return this.userProfile$.pipe(take(1)).subscribe(userProfile => {
      const userInformation: IPrechatData = {
        ContactFirstName: userProfile.firstName,
        ContactLastName: userProfile.lastName,
        ContactCompany: userProfile.company,
        ContactEmail: userProfile.email,
        language: userProfile.defaultLanguage,
      };
      this.getLiveChatStarter(userInformation);
    });
  }

  private getLiveChatStarter(userInformation: IPrechatData) {
    this.liveChatStarter$ = this.liveChatService.getLiveChatStarter(
      userInformation,
      this.onlineButton.nativeElement,
      this.offlineButton.nativeElement
    );
    this.subscription.add(this.subscribeLiveChatStarter());
  }

  private subscribeLiveChatStarter(): Subscription {
    return this.liveChatStarter$.pipe(take(1)).subscribe((liveChatStarter: any) => {
      this.renderer.listen(this.onlineButton.nativeElement, 'click', evt => {
        evt.preventDefault();
        liveChatStarter(this.chatButtonId);
        this.hideParent.emit();
      });
    });
  }

  ngOnDestroy(): void {
    this.liveChatService.destroyLiveChat();
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public startOfflineChat() {
    this.hideParent.emit();
    this.dialogService.open(OfflineChatComponent, { size: 'large-x-large' });
  }
}
