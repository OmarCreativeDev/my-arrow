export interface IPrechatData {
  ContactFirstName: string;
  ContactLastName: string;
  ContactCompany: string;
  ContactEmail: string;
  language: string;
}
