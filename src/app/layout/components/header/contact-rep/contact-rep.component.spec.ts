import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactRepComponent } from './contact-rep.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { mockUser } from '@app/core/user/user.service.spec';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OfflineChatComponent } from '@app/layout/components/header/offline-chat/offline-chat.component';
import { CoreModule } from '@app/core/core.module';
import { DialogService } from '@app/core/dialog/dialog.service';
import { DialogServiceMock } from '@app/core/dialog/dialog.service.spec';

describe('ContactRepComponent', () => {
  let component: ContactRepComponent;
  let fixture: ComponentFixture<ContactRepComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule, FormsModule, ReactiveFormsModule],
      declarations: [ContactRepComponent, OfflineChatComponent],
      providers: [{ provide: DialogService, useClass: DialogServiceMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactRepComponent);
    component = fixture.componentInstance;
    component.user = mockUser;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
