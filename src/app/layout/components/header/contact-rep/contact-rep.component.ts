import { Component, Input, ElementRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { IUser } from '@app/core/user/user.interface';
import { ToggleComponent } from '@app/shared/classes/toggle-component.class';

@Component({
  selector: 'app-contact-rep',
  templateUrl: './contact-rep.component.html',
  styleUrls: ['../../../../shared/components/dropdown/dropdown.component.scss', './contact-rep.component.scss'],
})
export class ContactRepComponent extends ToggleComponent {
  @Input()
  user: IUser;
  public onlineChatEnabled = false;
  public contactEmailNorthAmerica = 'myarrow@arrow.com';
  public contactTelephone = '1-877-237-8621';
  public contactEmailEMEA = 'myarrowemea2@arrow.com';

  constructor(private sanitizer: DomSanitizer, public elementRef: ElementRef) {
    super(elementRef);
  }

  public sanatizeEmail(email: string): object {
    return this.sanitizer.bypassSecurityTrustUrl(`mailto:${email}`);
  }
}
