import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@app/core/core.module';
import { userReducers } from '@app/core/user/store/user.reducers';
import { UserService } from '@app/core/user/user.service';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { Store, StoreModule } from '@ngrx/store';
import { Observable, of } from 'rxjs';

import { HeaderComponent } from './header.component';

class BackdropServiceMock {
  hide() {}
  show() {}
}

class StoreMock {
  public select(): Observable<any> {
    return of({});
  }
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;
  let store: Store<any>;
  let backdropService: BackdropService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        CoreModule,
        StoreModule.forRoot({
          user: userReducers,
        }),
      ],
      declarations: [HeaderComponent],
      providers: [UserService, { provide: BackdropService, useClass: BackdropServiceMock }, { provide: Store, useClass: StoreMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    backdropService = TestBed.get(BackdropService);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not show backdrop on load', () => {
    spyOn(backdropService, 'hide');
    component.showBackdrop();
    expect(backdropService.hide).toHaveBeenCalled();
  });

  it('should show backdrop', () => {
    spyOn(backdropService, 'show');
    component.showBackdrop(true);
    expect(backdropService.show).toHaveBeenCalled();
  });

  it('should hide backdrop', () => {
    spyOn(backdropService, 'hide');
    component.showBackdrop(false);
    expect(backdropService.hide).toHaveBeenCalled();
  });

  it('should call `stickyHeader` when `stickyHeaderPosition` is called', () => {
    spyOn(component, 'stickyHeader');
    const positionToStickAt = 195;
    component.setStickyHeaderPosition(positionToStickAt);
    expect(component.stickyHeader).toHaveBeenCalled();
  });

  it('should return the header to the default style if the scroll position is less than the height of the header and nav', () => {
    component.stickyHeaderPosition = 195;
    const scrollPosition = 150;
    component.stickyHeader(scrollPosition);
    expect(component.showStickyHeader).toBeFalsy();
  });

  it('should make the header sticky if the scroll position is greater than the height of the header and nav', () => {
    component.stickyHeaderPosition = 195;
    const scrollPosition = 250;
    component.stickyHeader(scrollPosition);
    expect(component.showStickyHeader).toBeTruthy();
  });

  it('should update the showSearchBar variable when run', () => {
    component.updateSearchBarValue(true);
    expect(component.showSearchBar).toBeTruthy();
  });

  it('should scroll', () => {
    component.onWindowScroll();
    expect(component.showStickyHeader).toBeFalsy();
    expect(component.showSearchBar).toBeFalsy();
  });

  it('should emit the value passed up from the view account component: onBillToModalOpen()', () => {
    spyOn(component.openBillToModal, 'emit');
    const valuePassedFromViewAccount = true;
    component.onBillToModalOpen(valuePassedFromViewAccount);
    expect(component.openBillToModal.emit).toHaveBeenCalledWith(valuePassedFromViewAccount);
  });
});
