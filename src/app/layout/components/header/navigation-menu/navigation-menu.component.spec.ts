import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from '@app/core/core.module';
import { NavigationMenuComponent } from './navigation-menu.component';
import { Store, StoreModule } from '@ngrx/store';
import { quoteCartReducers } from '@app/features/quotes/stores/quote-cart.reducers';
import { FormattedCountPipe } from '@app/shared/pipes/formatted-count/formatted-count.pipe';
import { userReducers } from '@app/core/user/store/user.reducers';
import { propertiesReducers } from '@app/features/properties/store/properties.reducers';
import { mockPrivateProperties } from '@app/core/features/feature-flags.mock';

const mockPropertiesReducers = () => ({
  ...propertiesReducers,
  private: mockPrivateProperties,
});

describe('NavigationMenuComponent', () => {
  let component: NavigationMenuComponent;
  let fixture: ComponentFixture<NavigationMenuComponent>;
  let quoteCartStore: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          quoteCart: quoteCartReducers,
          user: userReducers,
          properties: mockPropertiesReducers,
        }),
        RouterTestingModule,
        CoreModule,
      ],
      declarations: [NavigationMenuComponent, FormattedCountPipe],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavigationMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    quoteCartStore = TestBed.get(Store);
    spyOn(quoteCartStore, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
