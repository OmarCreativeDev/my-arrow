import { Component, Input, Output, EventEmitter, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';

import { IAppState } from '@app/shared/shared.interfaces';
import { IUser, RegionUserEnum } from '@app/core/user/user.interface';
import { getLineItemCountSelector } from '@app/features/quotes/stores/quote-cart.selectors';
import { getRegion } from '@app/core/user/store/user.selectors';
import { getPrivateFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.scss'],
})
export class NavigationMenuComponent implements OnInit, OnDestroy {
  @Input()
  user: IUser;

  @Output()
  headerPosition = new EventEmitter<Object>();
  public quoteCartItemCount$: Observable<Number>;
  public region$: Observable<string>;
  public region: string;
  public naUser: string = RegionUserEnum.NAUSER;
  public privateFeatures$: Observable<any>;
  public privateFeatures: object;
  public subscription: Subscription = new Subscription();

  constructor(private element: ElementRef, private store: Store<IAppState>) {
    this.quoteCartItemCount$ = this.store.pipe(select(getLineItemCountSelector));
    this.region$ = this.store.pipe(select(getRegion));
    this.privateFeatures$ = this.store.pipe(select(getPrivateFeatureFlagsSelector));
  }

  ngOnInit() {
    // setting timeout to get the height of the element otherwise it is 0
    setTimeout(() => {
      this.headerPosition.emit({ position: this.element.nativeElement.offsetTop + this.element.nativeElement.offsetHeight });
    });
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public startSubscriptions() {
    this.subscription.add(this.subscribeRegion());
    this.subscription.add(this.subscribePrivateFeatureFlags());
  }

  private subscribeRegion(): Subscription {
    return this.region$.subscribe(region => {
      this.region = region;
    });
  }

  private subscribePrivateFeatureFlags(): Subscription {
    return this.privateFeatures$.subscribe(featureFlags => {
      this.privateFeatures = featureFlags;
    });
  }

  get featureFlags() {
    return this.privateFeatures;
  }
}
