import { Component } from '@angular/core';
import { AuthService } from '@app/core/auth/auth.service';
import { BackdropService } from '@app/shared/services/backdrop.service';

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.scss'],
})
export class UserPanelComponent {
  public loggingOut = false;

  constructor(private authService: AuthService, private backdropService: BackdropService) {}

  /**
   * Subscribe to AuthService logout method and reset panel on any outcome
   */
  public logout() {
    this.loggingOut = true;
    this.authService.logout().subscribe(
      () => {
        this.resetPanel();
      },
      err => {
        this.resetPanel();
      }
    );
  }

  private resetPanel() {
    this.backdropService.hide();
    this.loggingOut = false;
  }
}
