import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { StoreModule } from '@ngrx/store';
import { CoreModule } from '@app/core/core.module';
import { UserPanelComponent } from './user-panel.component';
import { AuthService } from '@app/core/auth/auth.service';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { BackdropServiceMock } from '@app/app.component.spec';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { of, throwError } from 'rxjs';
import { WSSService } from '@app/core/ws/wss.service';
import { MockStompWSSService } from '@app/core/ws/wss.service.mock';

describe('UserPanelComponent', () => {
  let component: UserPanelComponent;
  let fixture: ComponentFixture<UserPanelComponent>;
  let authService: AuthService;
  let backdropService: BackdropService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({}), RouterTestingModule, CoreModule],
      declarations: [UserPanelComponent],
      providers: [{ provide: BackdropService, useClass: BackdropServiceMock }, { provide: WSSService, useClass: MockStompWSSService }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserPanelComponent);
    authService = TestBed.get(AuthService);
    backdropService = TestBed.get(BackdropService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#logout', () => {
    it('should reset panel on logout success', () => {
      spyOn(authService, 'logout').and.returnValue(of({}));
      spyOn(backdropService, 'hide');
      component.logout();
      expect(backdropService.hide).toHaveBeenCalled();
    });

    it('should reset panel on logout failure', () => {
      spyOn(authService, 'logout').and.returnValue(throwError(new Error()));
      spyOn(backdropService, 'hide');
      component.logout();
      expect(backdropService.hide).toHaveBeenCalled();
    });

    it('should call authService.logout()', () => {
      spyOn(authService, 'logout').and.returnValue(of({}));
      component.logout();
      expect(authService.logout).toHaveBeenCalled();
    });
  });
});
