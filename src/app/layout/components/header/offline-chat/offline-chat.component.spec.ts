import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';
import { OfflineChatComponent } from './offline-chat.component';
import { mockUser } from '@app/core/user/user.service.spec';
import { CoreModule } from '@app/core/core.module';
import { of, throwError } from 'rxjs';
import { Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { Store, StoreModule } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';
import { OfflineLiveChatService } from '@app/core/offline-live-chat/offline-live-chat.service';

describe('OfflineChatComponent', () => {
  let component: OfflineChatComponent;
  let fixture: ComponentFixture<OfflineChatComponent>;
  let messageField: FormControl;
  let offlineLiveChatService: OfflineLiveChatService;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({ user: userReducers }), CoreModule, FormsModule, ReactiveFormsModule],
      declarations: [OfflineChatComponent],
      providers: [{ provide: Dialog, useClass: DialogMock }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfflineChatComponent);
    offlineLiveChatService = TestBed.get(OfflineLiveChatService);
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    spyOn(store, 'pipe').and.returnValue(of(mockUser));
    component = fixture.componentInstance;
    fixture.detectChanges();
    messageField = <FormControl>component.form.controls.message;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should submit message', () => {
    spyOn(offlineLiveChatService, 'createCase').and.returnValue(of({}));
    messageField.setValue('Description');
    component.submit();
    expect(component.submitted).toBeTruthy();
  });

  it('should capture error', () => {
    spyOn(offlineLiveChatService, 'createCase').and.returnValue(throwError(new Error()));
    messageField.setValue('Description');
    component.submit();
    expect(component.hasError).toBeTruthy();
  });

  it('should reset offline chat', () => {
    messageField.setValue('Description');
    component.reset();
    expect(messageField.value).toBeFalsy();
  });

  it('should reset form on dismiss', () => {
    spyOn(component, 'reset');
    component.dismiss();
    expect(component.reset).toHaveBeenCalled();
  });
});
