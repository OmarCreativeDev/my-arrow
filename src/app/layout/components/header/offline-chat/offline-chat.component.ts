import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Dialog } from '@app/core/dialog/dialog.service';
import { getUser } from '@app/core/user/store/user.selectors';
import { IUser } from '@app/core/user/user.interface';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { OfflineLiveChatService } from '@app/core/offline-live-chat/offline-live-chat.service';
import { IOfflineLiveChat } from '@app/core/offline-live-chat/offline-live-chat.interface';
import { environment } from '@env/environment';

@Component({
  selector: 'app-offline-chat',
  templateUrl: './offline-chat.component.html',
})
export class OfflineChatComponent implements OnInit, OnDestroy {
  public form: FormGroup;
  public submitted = false;
  public hasError = false;
  public user$: Observable<IUser>;
  public user: IUser;
  private subscription: Subscription = new Subscription();

  constructor(
    private dialog: Dialog<OfflineChatComponent>,
    private store: Store<IAppState>,
    private offlineLiveChatService: OfflineLiveChatService
  ) {}

  ngOnInit() {
    // This Store needs to wait, so dont move to constructor
    this.user$ = this.store.pipe(select(getUser));
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeUser());
  }

  private subscribeUser(): Subscription {
    return this.user$.subscribe(user => {
      this.user = user;
      this.form = new FormGroup(
        {
          name: new FormControl({ value: `${this.user.firstName} ${this.user.lastName}`, disabled: true }),
          email: new FormControl({ value: this.user.email, disabled: true }),
          message: new FormControl('', Validators.required),
        },
        { updateOn: 'submit' }
      );
    });
  }

  public submit() {
    /* istanbul ignore else */
    if (this.form.valid) {
      const offlineLiveChat: IOfflineLiveChat = {
        orgid: environment.liveChatSettings.orgId,
        firstName: this.user.firstName,
        origin: environment.liveChatSettings.origin,
        subject: environment.liveChatSettings.subject + this.user.firstName + ' ' + this.user.lastName,
        email: this.user.email,
        description: this.form.value.message,
        Website__c: environment.liveChatSettings.webSite__c,
        Communication_Type__c: environment.liveChatSettings.communicationType__c,
        elqFormName: environment.liveChatSettings.elqFormName,
        elqSiteId: environment.liveChatSettings.elqSiteId,
        sfdcUrl: environment.liveChatSettings.sfdcUrl,
        lastName: this.user.lastName,
        company: this.user.company,
        LeadSource: environment.liveChatSettings.leadSource,
        Lead_Source_Detail__c: environment.liveChatSettings.leadSourceDetail__c,
        Lead_Source_Reference__c: environment.liveChatSettings.leadSourceReference__c,
        uploadType: environment.liveChatSettings.uploadType,
        Campaign_ID: environment.liveChatSettings.campaign_ID,
        Category__c: environment.liveChatSettings.category__c,
      };
      this.offlineLiveChatService.createCase(offlineLiveChat).subscribe(
        () => {
          this.submitted = true;
        },
        error => {
          this.submitted = true;
          this.hasError = true;
        }
      );
    }
  }

  public reset() {
    this.form.controls.message.reset();
    this.submitted = false;
    this.hasError = false;
  }

  public dismiss() {
    this.reset();
    this.dialog.close();
  }
}
