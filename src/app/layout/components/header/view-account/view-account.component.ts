import { Component, Input, ElementRef, Output, EventEmitter } from '@angular/core';

import { IUser } from '@app/core/user/user.interface';

import { Subscription } from 'rxjs';
import { ToggleComponent } from '@app/shared/classes/toggle-component.class';

@Component({
  selector: 'app-view-account',
  templateUrl: './view-account.component.html',
  styleUrls: ['./view-account.component.scss'],
})
export class ViewAccountComponent extends ToggleComponent {
  public subscription: Subscription = new Subscription();
  public billToModalVisible: boolean = false;

  @Input()
  user: IUser;

  @Output()
  toggleBillToModal: EventEmitter<boolean> = new EventEmitter();
  constructor(public elementRef: ElementRef) {
    super(elementRef);
  }

  public doBillToOpen(): void {
    this.toggleBillToModal.emit(!this.billToModalVisible);
    this.toggle();
  }

  public onOpenAccount(): void {
    this.toggle();
  }
}
