import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewAccountComponent } from './view-account.component';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { BackdropServiceMock } from '@app/app.component.spec';
import { UserService } from '@app/core/user/user.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CoreModule } from '@app/core/core.module';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';

describe('ViewAccountComponent', () => {
  let component: ViewAccountComponent;
  let fixture: ComponentFixture<ViewAccountComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        CoreModule,
        StoreModule.forRoot({
          user: userReducers,
        }),
      ],
      declarations: [ViewAccountComponent],
      providers: [{ provide: BackdropService, useClass: BackdropServiceMock }, UserService],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle component when clicking on it: onOpenAccount()', () => {
    spyOn(component, 'toggle');
    component.onOpenAccount();
    expect(component.toggle).toHaveBeenCalled();
  });

  it('should toggle component and emit a true value to open bill-to modal when clicking on open account: doBillToOpen()', () => {
    spyOn(component, 'toggle');
    spyOn(component.toggleBillToModal, 'emit');
    component.doBillToOpen();
    expect(component.toggle).toHaveBeenCalled();
    expect(component.toggleBillToModal.emit).toHaveBeenCalledWith(true);
  });
});
