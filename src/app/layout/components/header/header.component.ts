import { Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { IUser } from '@app/core/user/user.interface';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import { getPublicFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  @Input()
  user: IUser;

  @Output()
  openBillToModal: EventEmitter<boolean> = new EventEmitter();

  public showSearchBar: boolean = false;
  public stickyHeaderPosition: number;
  public showStickyHeader: boolean = false;

  public publicFeatureFlags: any;
  public subscription: Subscription = new Subscription();
  private publicFeatureFlags$: Observable<any>;

  constructor(private backdropService: BackdropService, private store: Store<IAppState>) {
    this.publicFeatureFlags$ = this.store.pipe(select(getPublicFeatureFlagsSelector));
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribePublicFeatureFlags());
  }

  private subscribePublicFeatureFlags(): Subscription {
    return this.publicFeatureFlags$.subscribe(featureFlags => {
      this.publicFeatureFlags = featureFlags;
    });
  }

  public showBackdrop(show: boolean = false) {
    if (show) {
      this.backdropService.show();
    } else {
      this.backdropService.hide();
    }
  }

  @HostListener('window:scroll', [])
  onWindowScroll() {
    this.stickyHeader(window.pageYOffset);
  }

  public stickyHeader(scrollPosition) {
    if (scrollPosition > this.stickyHeaderPosition) {
      this.showStickyHeader = true;
    } else {
      this.showStickyHeader = false;
      this.showSearchBar = false;
    }
  }

  public setStickyHeaderPosition(headerPosition) {
    this.stickyHeaderPosition = headerPosition.position;

    this.stickyHeader(window.pageYOffset);
  }

  public updateSearchBarValue(value): void {
    this.showSearchBar = value;
  }

  public onBillToModalOpen(event) {
    this.openBillToModal.emit(event);
  }
}
