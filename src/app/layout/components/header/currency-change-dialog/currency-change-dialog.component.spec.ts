import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrencyChangeDialogComponent } from './currency-change-dialog.component';
import { Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { SharedModule } from '@app/shared/shared.module';
import { StoreModule } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('CurrencyChangeDialogComponent', () => {
  let component: CurrencyChangeDialogComponent;
  let fixture: ComponentFixture<CurrencyChangeDialogComponent>;
  let dialog: Dialog<CurrencyChangeDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SharedModule, StoreModule.forRoot({ user: userReducers }), HttpClientTestingModule, HttpClientModule],
      declarations: [CurrencyChangeDialogComponent],
      providers: [{ provide: Dialog, useClass: DialogMock }],
    }).compileComponents();
  }));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CurrencyChangeDialogComponent],
    }).compileComponents();

    dialog = TestBed.get(Dialog);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrencyChangeDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close the dialog', () => {
    spyOn(dialog, 'close');
    component.onClose(true);
    expect(dialog.close).toHaveBeenCalledWith({ currencyChangeAccepted: true });
  });
});
