import { Component } from '@angular/core';
import { Dialog } from '@app/core/dialog/dialog.service';

@Component({
  selector: 'app-currency-change-dialog',
  templateUrl: './currency-change-dialog.component.html',
  styleUrls: ['./currency-change-dialog.component.scss'],
})
export class CurrencyChangeDialogComponent {
  constructor(public dialog: Dialog<CurrencyChangeDialogComponent>) {}

  public onClose(currencyChangeAccepted: boolean): void {
    this.dialog.close({
      currencyChangeAccepted,
    });
  }
}
