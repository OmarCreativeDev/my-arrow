import { Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { getCartItemCountSelector } from '@app/features/cart/stores/cart/cart.selectors';
import { GetCarts } from '@app/features/cart/stores/cart/cart.actions';
import { IAppState } from '@app/shared/shared.interfaces';
import { Observable } from 'rxjs';
import { SimpleChanges } from '@angular/core/src/metadata/lifecycle_hooks';
import { Store, select } from '@ngrx/store';
import { ToggleComponent } from '@app/shared/classes/toggle-component.class';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent extends ToggleComponent implements OnInit, OnChanges {
  @Input()
  public showSearchBar: boolean;
  @Output()
  public toggleSearch = new EventEmitter<boolean>();

  public cartItemCount$: Observable<Number>;
  constructor(private store: Store<IAppState>, public elementRef: ElementRef) {
    super(elementRef);
    this.cartItemCount$ = this.store.pipe(select(getCartItemCountSelector));
  }

  ngOnInit() {
    this.store.dispatch(new GetCarts());
  }

  public toggleSearchBar(): void {
    this.toggle();
    this.toggleSearch.emit(this.isOpen);
  }

  ngOnChanges(changes: SimpleChanges) {
    /* istanbul ignore else */
    if (!changes.showSearchBar.currentValue) {
      this.isOpen = false;
    }
  }
}
