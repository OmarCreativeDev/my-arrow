import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { cartReducers } from '@app/features/cart/stores/cart/cart.reducers';
import { CoreModule } from '@app/core/core.module';
import { CUSTOM_ELEMENTS_SCHEMA, SimpleChange, SimpleChanges } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { Store, StoreModule } from '@ngrx/store';
import { ToolbarComponent } from './toolbar.component';
import { FormattedCountPipe } from '@app/shared/pipes/formatted-count/formatted-count.pipe';

describe('ToolbarComponent', () => {
  let component: ToolbarComponent;
  let fixture: ComponentFixture<ToolbarComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          cart: cartReducers,
        }),
        RouterTestingModule,
        CoreModule,
      ],
      declarations: [ToolbarComponent, FormattedCountPipe],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should toggle search bar when clicking on it: toggleSearchBar()', () => {
    spyOn(component, 'toggle');
    const toggleSearchSpy = spyOn(component.toggleSearch, 'emit');
    component.toggleSearchBar();
    expect(component.toggle).toHaveBeenCalled();
    expect(toggleSearchSpy).toHaveBeenCalledWith(component.isOpen);
  });

  it('should update `isOpen` when `showSearchBar` variable has changed', () => {
    const changesObj: SimpleChanges = {
      showSearchBar: new SimpleChange(undefined, false, false),
    };
    component.ngOnChanges(changesObj);
    expect(component.isOpen).toBeFalsy();
  });
});
