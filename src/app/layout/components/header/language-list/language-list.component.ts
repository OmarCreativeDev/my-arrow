import { Component, OnInit, LOCALE_ID, Inject, Output, EventEmitter, OnDestroy } from '@angular/core';

import { Observable, Subscription } from 'rxjs';
import { UserService } from '@app/core/user/user.service';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { LanguageSelect, LanguageDropdownClick } from '@app/layout/store/layout.actions';
import { getAccountId } from '@app/core/user/store/user.selectors';

export enum ToggleDropDownStatus {
  dropDownOpen = 'open',
  dropDownClose = 'close',
}

@Component({
  selector: 'app-language-list',
  templateUrl: './language-list.component.html',
})
export class LanguageListComponent implements OnInit, OnDestroy {
  @Output()
  openChange = new EventEmitter<boolean>();

  public labelMap = {
    ['en-US']: 'English',
    ['de']: 'Deutsche',
    ['fr']: 'Français',
  };

  public languages = this.userService.AVAILABLE_LOCALES.map(lang => {
    return {
      value: lang,
      label: this.labelMap[lang],
    };
  });

  public selectedLanguage;
  public currentLocale;
  public isOverlayVisible: boolean = false;
  public userId$: Observable<number>;
  public userId: number;
  public subscription: Subscription = new Subscription();
  private toggleDropDownStatus: ToggleDropDownStatus = ToggleDropDownStatus.dropDownOpen;

  constructor(@Inject(LOCALE_ID) protected localeId: string, private userService: UserService, private store: Store<IAppState>) {
    this.userId$ = this.store.pipe(select(getAccountId));
  }

  ngOnInit() {
    this.currentLocale = this.localeId;
    this.startSubscriptions();
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public startSubscriptions(): void {
    this.subscription.add(this.userSubscriptions());
  }

  private userSubscriptions() {
    this.userId$.subscribe(id => this.setUserId(id));
  }

  private setUserId(id) {
    this.userId = id;
  }

  public captureLanguageSelection() {
    const userId = this.userId ? this.userId.toString() : ' ';

    if (this.toggleDropDownStatus === ToggleDropDownStatus.dropDownOpen) {
      this.store.dispatch(new LanguageDropdownClick(userId));
      this.toggleDropDownStatus = ToggleDropDownStatus.dropDownClose;
    } else {
      this.toggleDropDownStatus = ToggleDropDownStatus.dropDownOpen;
    }
  }

  public changeLanguage(lang: string) {
    this.store.dispatch(new LanguageSelect(lang));
  }
}
