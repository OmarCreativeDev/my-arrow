import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CoreModule } from '@app/core/core.module';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { DropdownComponent } from '@app/shared/components/dropdown/dropdown.component';
import { LanguageListComponent } from './language-list.component';
import { UserService } from '@app/core/user/user.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { StoreModule, Store } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';
import { LanguageDropdownClick, LanguageSelect } from '@app/layout/store/layout.actions';

describe('LanguageListComponent', () => {
  let component: LanguageListComponent;
  let fixture: ComponentFixture<LanguageListComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({ user: userReducers }), CoreModule],
      providers: [BackdropService, UserService],
      declarations: [LanguageListComponent, DropdownComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  afterEach(() => {
    document.cookie = 'locale=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageListComponent);
    store = TestBed.get(Store);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOn(store, 'dispatch').and.callThrough();
    component.ngOnInit();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('default language should be en-US', () => {
    expect(component.currentLocale).toEqual('en-US');
  });

  it('captureLanguageSelection() should dispatch LanguageDropdownClick action', () => {
    const userId = '1305827';
    component.userId = 1305827;
    component.captureLanguageSelection();
    const action = new LanguageDropdownClick(userId);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });

  it('changeLanguage() should dispatch LanguageSelect action', () => {
    const userId = 'de';
    component.changeLanguage(userId);
    const action = new LanguageSelect(userId);
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
});
