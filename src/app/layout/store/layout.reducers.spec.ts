import { LanguageDropdownClick, LanguageSelect } from './layout.actions';
import { layoutReducers, INITIAL_RESULTS_STATE } from './layout.reducers';

describe('Layout Reducers', () => {
  let initialState = INITIAL_RESULTS_STATE;

  beforeEach(() => {
    initialState = {};
  });

  it('`LANGUAGE_DROPDOWN_CLICK` should have the correct payload', () => {
    const action = new LanguageDropdownClick('1305827');
    const result = layoutReducers(initialState, action);
    expect(result).toEqual({ state: {} });
  });

  it('`LANGUAGE_SELECT` should have the correct payload', () => {
    const action = new LanguageSelect('de');
    const result = layoutReducers(initialState, action);
    expect(result).toEqual({ state: {} });
  });
});
