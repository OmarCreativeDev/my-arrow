import { Action } from '@ngrx/store';

export enum LayoutActionTypes {
  LANGUAGE_DROPDOWN_CLICK = 'LANGUAGE_DROPDOWN_CLICK',
  LANGUAGE_SELECT = 'LANGUAGE_SELECT',
}

/**
 * Triggers when language dropdown is clicked
 */

export class LanguageDropdownClick implements Action {
  readonly type = LayoutActionTypes.LANGUAGE_DROPDOWN_CLICK;
  constructor(public payload: string) {}
}

/**
 * Triggers when language option is selected
 */

export class LanguageSelect implements Action {
  readonly type = LayoutActionTypes.LANGUAGE_SELECT;
  constructor(public payload: string) {}
}

export type LayoutActions = LanguageDropdownClick | LanguageSelect;
