import { UserService } from '@app/core/user/user.service';
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';

import { LayoutActionTypes, LanguageSelect } from '@app/layout/store/layout.actions';

@Injectable()
export class QuoteCartEffects {
  constructor(private actions$: Actions, private userService: UserService) {}

  @Effect()
  LanguageSelect$: Observable<any> = this.actions$.pipe(
    ofType<LanguageSelect>(LayoutActionTypes.LANGUAGE_SELECT),
    switchMap(data => {
      this.userService.changeLanguage(data.payload);
      return Observable;
    })
  );
}
