import { LayoutActions, LayoutActionTypes } from './layout.actions';

export const INITIAL_RESULTS_STATE = {};

/**
 * Mutates the state for given set of Layout Actions
 * @param state
 * @param action
 */
export function layoutReducers(state = INITIAL_RESULTS_STATE, action: LayoutActions) {
  switch (action.type) {
    case LayoutActionTypes.LANGUAGE_DROPDOWN_CLICK:
    case LayoutActionTypes.LANGUAGE_SELECT:
    default: {
      return {
        state,
      };
    }
  }
}
