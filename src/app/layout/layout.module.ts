import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';

import { BackdropComponent } from './components/backdrop/backdrop.component';
import { ContactRepComponent } from './components/header/contact-rep/contact-rep.component';
import { CurrencyListComponent } from './components/header/currency-list/currency-list.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { LanguageListComponent } from './components/header/language-list/language-list.component';
import { LoggedInComponent } from './logged-in/logged-in.component';
import { LoggedOutComponent } from './logged-out/logged-out.component';
import { NavigationMenuComponent } from './components/header/navigation-menu/navigation-menu.component';
import { OfflineChatComponent } from './components/header/offline-chat/offline-chat.component';
import { ToolbarComponent } from './components/header/toolbar/toolbar.component';
import { UserPanelComponent } from './components/header/user-panel/user-panel.component';
import { ViewAccountComponent } from './components/header/view-account/view-account.component';
import { InterstitialComponent } from './interstitial/interstitial.component';
import { ChatButtonComponent } from './components/header/chat-button/chat-button.component';
import { CurrencyChangeDialogComponent } from './components/header/currency-change-dialog/currency-change-dialog.component';
import { CustomerBroadcastDialogComponent } from '@app/features/dashboard/components/customer-broadcast-dialog/customer-broadcast-dialog.component';
import { CurrencyWaitDialogComponent } from './components/header/currency-list/currency-wait-dialog/currency-wait-dialog.component';

@NgModule({
  imports: [CommonModule, RouterModule, SharedModule, FormsModule, ReactiveFormsModule],
  declarations: [
    BackdropComponent,
    ContactRepComponent,
    CurrencyListComponent,
    FooterComponent,
    HeaderComponent,
    LanguageListComponent,
    LoggedInComponent,
    LoggedOutComponent,
    NavigationMenuComponent,
    OfflineChatComponent,
    ToolbarComponent,
    UserPanelComponent,
    ViewAccountComponent,
    InterstitialComponent,
    ChatButtonComponent,
    CurrencyChangeDialogComponent,
    CustomerBroadcastDialogComponent,
    CurrencyWaitDialogComponent,
  ],
  entryComponents: [OfflineChatComponent, CurrencyChangeDialogComponent, CustomerBroadcastDialogComponent, CurrencyWaitDialogComponent],
  exports: [BackdropComponent, CurrencyChangeDialogComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class LayoutModule {}
