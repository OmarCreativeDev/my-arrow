import { Component, OnDestroy, OnInit } from '@angular/core';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import { getPublicFeatureFlagsSelector } from '@app/features/properties/store/properties.selectors';
import { Subscription, Observable } from 'rxjs';

@Component({
  selector: 'app-logged-out',
  templateUrl: './logged-out.component.html',
  styleUrls: ['./logged-out.component.scss'],
})
export class LoggedOutComponent implements OnInit, OnDestroy {
  public publicFeatureFlags: any;
  public subscription: Subscription = new Subscription();
  private publicFeatureFlags$: Observable<any>;

  constructor(private backdropService: BackdropService, private store: Store<IAppState>) {
    this.publicFeatureFlags$ = this.store.pipe(select(getPublicFeatureFlagsSelector));
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribePublicFeatureFlags());
  }

  private subscribePublicFeatureFlags(): Subscription {
    return this.publicFeatureFlags$.subscribe(featureFlags => {
      this.publicFeatureFlags = featureFlags;
    });
  }

  public showBackdrop(show: boolean = false) {
    if (show) {
      this.backdropService.show();
    } else {
      this.backdropService.hide();
    }
  }
}
