export const TINames = ['Texas Instruments'];

export function isTexasInstruments(manufacturer: string) {
  return manufacturer !== undefined && TINames.some(value => manufacturer.toUpperCase() === value.toUpperCase());
}
