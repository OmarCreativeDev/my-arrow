import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';
import { add } from 'mathjs';

import { getCurrencyCode } from '@app/core/user/store/user.selectors';
import { IAppState } from '@app/shared/shared.interfaces';

@Component({
  selector: 'app-subtotal-display',
  templateUrl: './subtotal-display.component.html',
  styleUrls: ['./subtotal-display.component.scss'],
})
export class SubtotalDisplayComponent implements OnInit, OnDestroy {
  @Input()
  title: string;
  @Input()
  items: Array<any> = [];
  @Input()
  itemValueKey: string;
  @Input()
  currencyCode: string;

  public currencyCode$: Observable<string>;
  public subscription: Subscription = new Subscription();

  constructor(private store: Store<IAppState>) {
    this.currencyCode$ = this.store.pipe(select(getCurrencyCode));
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    /* istanbul ignore else */
    if (!this.currencyCode) {
      this.subscription.add(this.subscribeCurrencyCode());
    }
  }

  private subscribeCurrencyCode(): Subscription {
    return this.currencyCode$.pipe(filter(code => code !== undefined)).subscribe(code => {
      this.currencyCode = code;
    });
  }

  /**
   * Takes an array of items and calculates their total
   * @param items a array of items to calculate a total for
   * @param itemTotalKey the key for the value of each item
   */
  private calculateSubtotal(items: Array<any> = [], itemTotalKey: string): number {
    return items.reduce((subtotal, item) => {
      const itemTotal = item[itemTotalKey] || 0;
      return add(subtotal, itemTotal);
    }, 0);
  }

  public get subtotal(): number {
    return this.calculateSubtotal(this.items, this.itemValueKey);
  }
}
