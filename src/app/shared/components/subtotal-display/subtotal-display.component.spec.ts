import { CurrencyPipe } from '@angular/common';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { of } from 'rxjs';
import { FormattedPricePipe } from '@app/shared/pipes/formatted-price/formatted-price.pipe';
import { SubtotalDisplayComponent } from './subtotal-display.component';

class MockStore {
  select() {
    return of('BTC');
  }
  dispatch() {}
  pipe() {
    return of('BTC');
  }
}

describe('SubtotalDisplayComponent', () => {
  let component: SubtotalDisplayComponent;
  let fixture: ComponentFixture<SubtotalDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SubtotalDisplayComponent, FormattedPricePipe],
      providers: [{ provide: Store, useClass: MockStore }, CurrencyPipe],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubtotalDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#calculate Subtotal', () => {
    const testCases = [
      {
        description: `should return 0 if there are no items`,
        items: [],
        key: 'total',
        result: 0,
      },
      {
        description: `should return default item total to 0 if key not found`,
        items: [{ total: 1 }, { total: 2 }, {}],
        key: 'total',
        result: 3,
      },
      {
        description: `should total an array of lines`,
        items: [{ total: 1 }, { total: 2 }, { total: 3 }, { total: 4 }],
        key: 'total',
        result: 10,
      },
      {
        description: `should total an array of lines, including decimals`,
        items: [{ total: 1.09 }, { total: 2.17 }, { total: 3.32 }, { total: 4.94 }],
        key: 'total',
        result: 11.52,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        component.items = testCase.items;
        component.itemValueKey = testCase.key;
        expect(component.subtotal).toEqual(testCase.result);
      });
    }
  });
});
