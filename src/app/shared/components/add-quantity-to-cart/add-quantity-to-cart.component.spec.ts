import * as moment from 'moment';
import { AddQuantityToCartComponent } from './add-quantity-to-cart.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DialogService } from '@app/core/dialog/dialog.service';
import { Observable, of } from 'rxjs';
import { ReactiveFormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA, SimpleChange, SimpleChanges } from '@angular/core';
import { Store } from '@ngrx/store';

export class StoreMock {
  public select(): Observable<number> {
    return of(10);
  }
  public dispatch(): void {}
  public pipe() {
    return of({});
  }
}

export class MockDialogService {
  public open(): void {}
}

describe('AddQuantityToCartComponent', () => {
  let component: AddQuantityToCartComponent;
  let fixture: ComponentFixture<AddQuantityToCartComponent>;
  const mockEvent = {
    key: '2',
    keyCode: 50,
    target: {
      value: '2',
    },
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddQuantityToCartComponent],
      imports: [ReactiveFormsModule],
      providers: [{ provide: Store, useClass: StoreMock }, { provide: DialogService, useClass: MockDialogService }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddQuantityToCartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`setupForm()` should be invoked once components initialies', () => {
    spyOn(component, 'setupForm');
    component.ngOnInit();
    expect(component.setupForm).toHaveBeenCalled();
  });

  it('`ngOnChanges()` calls `setSelectedCustomerPartNumber()` when there is a selectedCustomerPartNumber', () => {
    const mockSimpleChanges: SimpleChanges = {
      selectedCustomerPartNumber: new SimpleChange(null, '2222-2222', null),
    };

    spyOn(component, 'setSelectedCustomerPartNumber');
    component.ngOnChanges(mockSimpleChanges);
    expect(component.setSelectedCustomerPartNumber).toHaveBeenCalled();
  });

  it('`ngOnChanges()` calls `setSelectedEndCustomerSiteId()` when there is a selectedEndCustomerSiteId', () => {
    const mockSimpleChanges: SimpleChanges = {
      selectedEndCustomerSiteId: new SimpleChange(null, 2222, null),
    };

    spyOn(component, 'setSelectedEndCustomerSiteId');
    component.ngOnChanges(mockSimpleChanges);
    expect(component.setSelectedEndCustomerSiteId).toHaveBeenCalled();
  });

  it('`ngOnChanges()` calls `setWarehouseId()` when there is a warehouseId', () => {
    const mockSimpleChanges: SimpleChanges = {
      warehouseId: new SimpleChange(null, 333, null),
    };

    spyOn(component, 'setWarehouseId');
    component.ngOnChanges(mockSimpleChanges);
    expect(component.setWarehouseId).toHaveBeenCalled();
  });

  it('`ngOnChanges()` calls `setDocId()` when there is a docId', () => {
    const mockSimpleChanges: SimpleChanges = {
      docId: new SimpleChange(null, '333-xxxx', null),
    };

    spyOn(component, 'setDocId');
    component.ngOnChanges(mockSimpleChanges);
    expect(component.setDocId).toHaveBeenCalled();
  });

  it('`ngOnChanges()` calls `setItemId()` when there is an itemId', () => {
    const mockSimpleChanges: SimpleChanges = {
      itemId: new SimpleChange(null, 22, null),
    };

    spyOn(component, 'setItemId');
    component.ngOnChanges(mockSimpleChanges);
    expect(component.setItemId).toHaveBeenCalled();
  });

  it('`setSelectedCustomerPartNumber()` should set selectedCustomerPartNumber in addToCartRequestItem', () => {
    component.selectedCustomerPartNumber = 'ABC-123';
    component.setSelectedCustomerPartNumber('ABC-123');
    expect(component.addToCartRequestItem.selectedCustomerPartNumber).toBe(component.selectedCustomerPartNumber);
  });

  it('`setSelectedEndCustomerSiteId()` should set selectedEndCustomerSiteId in addToCartRequestItem', () => {
    component.selectedEndCustomerSiteId = 123;
    component.setSelectedEndCustomerSiteId(123);
    expect(component.addToCartRequestItem.selectedEndCustomerSiteId).toBe(component.selectedEndCustomerSiteId);
  });

  it('`setWarehouseId()` should set warehouseId in addToCartRequestItem', () => {
    component.warehouseId = 665;
    component.setWarehouseId();
    expect(component.addToCartRequestItem.warehouseId).toBe(component.warehouseId);
  });

  it('`setDocId()` should set docId in addToCartRequestItem', () => {
    component.docId = '665-xxx';
    component.setDocId();
    expect(component.addToCartRequestItem.docId).toBe(component.docId);
  });

  it('`setItemId()` should set itemId in addToCartRequestItem', () => {
    component.itemId = 1234;
    component.setItemId();
    expect(component.addToCartRequestItem.itemId).toBe(component.itemId);
  });

  it('`setManufacturer()` should set manufacturer in addToCartRequestItem', () => {
    component.manufacturer = 'test manufacturer';
    component.setManufacturer();
    expect(component.addToCartRequestItem.manufacturer).toBe(component.manufacturer);
  });

  it('`setDescription()` should set description in addToCartRequestItem', () => {
    component.description = 'item description';
    component.setDescription();
    expect(component.addToCartRequestItem.description).toBe(component.description);
  });

  it('`setManufacturerPartNumber()` should set manufacturerPartNumber in addToCartRequestItem', () => {
    component.manufacturerPartNumber = 'aaa manufacturerPartNumber';
    component.setManufacturerPartNumber();
    expect(component.addToCartRequestItem.manufacturerPartNumber).toBe(component.manufacturerPartNumber);
  });

  it('`setupForm()` should set form with correct `FormGroup` type', () => {
    component.setupForm();
    expect(component.form).toBeDefined();
    expect(component.form.constructor.name).toBe('FormGroup');
  });

  it('`checkForm()` should invoke `setQuantity()` once form is valid', () => {
    spyOn(component, 'emitQuantity');
    component.form.controls['quantity'].setValue('2');
    component.checkForm(mockEvent);

    expect(component.enteredQuantity).toBeDefined();
    expect(component.enteredQuantity).toBe(2);
    expect(component.emitQuantity).toHaveBeenCalled();
  });

  it('`emitQuantity` should emit `setQuantity` and fire `setQuantityAndRequestDate`', () => {
    spyOn(component.setQuantity, 'emit');
    spyOn(component, 'setQuantityAndRequestDate');

    component.emitQuantity();
    expect(component.setQuantity.emit).toHaveBeenCalled();
    expect(component.setQuantityAndRequestDate).toHaveBeenCalled();
  });

  it('`setQuantityAndRequestDate()` should set quantity, requestDate in addToCartRequestItem', () => {
    const mockRequestDate = moment(new Date()).format('YYYY-MM-DD');
    component.enteredQuantity = 1;
    component.setQuantityAndRequestDate();

    expect(component.addToCartRequestItem.quantity).toBe(component.enteredQuantity);
    expect(component.addToCartRequestItem.requestDate).toBeDefined();
    expect(component.addToCartRequestItem.requestDate).toBe(mockRequestDate);
  });

  it('`addToCart()` should invoke `dialogService.open()`', () => {
    spyOn(component.dialogService, 'open');
    component.addToCart();
    expect(component.dialogService.open).toHaveBeenCalled();
  });

  it('should emit `showAddToCartCapDialog` when adding to cart and max cap has been reached', () => {
    spyOn(component.showAddToCartCapDialog, 'emit');
    component.cartLineItemsCount = 100;
    component.cartMaxLineItems = 100;
    component.addToCart();
    expect(component.showAddToCartCapDialog.emit).toHaveBeenCalled();
  });

  it('should emit `showAddToCartCapDialog` when adding to quote cart and max cap has been reached', () => {
    spyOn(component.showAddToCartCapDialog, 'emit');
    component.quoteCartLineItemsCount = 100;
    component.quoteCartMaxLineItems = 100;
    component.addToQuote();
    expect(component.showAddToCartCapDialog.emit).toHaveBeenCalled();
  });
});
