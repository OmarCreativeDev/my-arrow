import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as moment from 'moment';
import { IAddToCartRequestItem } from '@app/core/cart/cart.interfaces';
import { DialogService } from '@app/core/dialog/dialog.service';
import { IProductPriceTiers } from '@app/core/inventory/product-price.interface';
import { IAddToQuoteCartRequestItem } from '@app/core/quote-cart/quote-cart.interfaces';
import { AddToCartDialogComponent, CartType } from '@app/shared/components/add-to-cart-dialog/add-to-cart-dialog.component';

import { IQuantity } from './add-quantity-to-cart.interface';
import { IAppState } from '@app/shared/shared.interfaces';
import { QuoteCartLimit } from '@app/features/quotes/stores/quote-cart.actions';
import { IAddToCartCapData } from '@app/shared/components/add-to-cart-cap-dialog/add-to-cart-cap-dialog.interface';
import { ShoppingCartLimit } from '@app/features/cart/stores/cart/cart.actions';

@Component({
  selector: 'app-add-quantity-to-cart',
  templateUrl: './add-quantity-to-cart.component.html',
  styleUrls: ['./add-quantity-to-cart.component.scss'],
})
export class AddQuantityToCartComponent implements OnInit, OnChanges {
  public form: FormGroup;
  public addToCartRequestItem: IAddToCartRequestItem = {
    manufacturerPartNumber: null,
    docId: null,
    itemId: null,
    warehouseId: null,
    quantity: null,
    requestDate: null,
    manufacturer: null,
    description: null,
  };
  public enteredQuantity: number;

  @Input()
  public manufacturerPartNumber: string;
  @Input()
  public docId: string;
  @Input()
  public itemId: number;
  @Input()
  public minimumOrderQuantity: number;
  @Input()
  public selectedCustomerPartNumber: string;
  @Input()
  public selectedEndCustomerSiteId: number;
  @Input()
  public showDivider: boolean;
  @Input()
  public warehouseId: number;
  @Input()
  public price: number;
  @Input()
  public priceTiers: Array<IProductPriceTiers>;
  @Input()
  public purchasable: boolean;
  @Input()
  public selectForPricing: boolean;
  @Output()
  public setQuantity = new EventEmitter<IQuantity>();
  @Input()
  public quoteCartMaxLineItems: number;
  @Input()
  public quoteCartLineItemsCount: number;
  @Input()
  public cartMaxLineItems: number;
  @Input()
  public cartLineItemsCount: number;
  @Input()
  public manufacturer: string;
  @Input()
  public description: string;
  @Output()
  public showAddToCartCapDialog = new EventEmitter<IAddToCartCapData>();

  constructor(public dialogService: DialogService, public formBuilder: FormBuilder, private store: Store<IAppState>) {}

  public ngOnInit(): void {
    this.setupForm();
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.selectedCustomerPartNumber) {
      this.setSelectedCustomerPartNumber(changes.selectedCustomerPartNumber.currentValue);
    }

    if (changes.selectedEndCustomerSiteId) {
      this.setSelectedEndCustomerSiteId(changes.selectedEndCustomerSiteId.currentValue);
    }

    if (changes.warehouseId && changes.warehouseId.currentValue) {
      this.setWarehouseId();
    }

    if (changes.manufacturerPartNumber && changes.manufacturerPartNumber.currentValue) {
      this.setManufacturerPartNumber();
    }

    if (changes.docId && changes.docId.currentValue) {
      this.setDocId();
    }

    if (changes.itemId && changes.itemId.currentValue) {
      this.setItemId();
    }

    if (changes.warehouseId && changes.warehouseId.currentValue) {
      this.setWarehouseId();
    }

    if (changes.manufacturer && changes.manufacturer.currentValue) {
      this.setManufacturer();
    }

    if (changes.description && changes.description.currentValue) {
      this.setDescription();
    }
  }

  public setSelectedCustomerPartNumber(value: string): void {
    if (value) {
      this.addToCartRequestItem.selectedCustomerPartNumber = value;
    } else {
      delete this.addToCartRequestItem.selectedCustomerPartNumber;
    }
  }

  public setSelectedEndCustomerSiteId(value: number): void {
    if (value) {
      this.addToCartRequestItem.selectedEndCustomerSiteId = value;
    } else {
      delete this.addToCartRequestItem.selectedEndCustomerSiteId;
    }
  }

  public setWarehouseId(): void {
    this.addToCartRequestItem.warehouseId = this.warehouseId;
  }

  public setManufacturerPartNumber(): void {
    this.addToCartRequestItem.manufacturerPartNumber = this.manufacturerPartNumber;
  }

  public setDocId(): void {
    this.addToCartRequestItem.docId = this.docId;
  }

  public setItemId(): void {
    this.addToCartRequestItem.itemId = this.itemId;
  }

  public setManufacturer(): void {
    this.addToCartRequestItem.manufacturer = this.manufacturer;
  }

  public setDescription(): void {
    this.addToCartRequestItem.description = this.description;
  }

  public setupForm(): void {
    this.form = this.formBuilder.group({
      quantity: [null, Validators.compose([Validators.pattern('^[0-9]+$'), Validators.max(9999999), Validators.min(1)])],
    });
  }

  /**
   * Check if form is valid,
   * then emit quantity to parent
   * @param {string} $event
   */
  public checkForm($event): void {
    /* istanbul ignore else */
    if (this.form.valid) {
      this.enteredQuantity = parseInt($event.target.value, 10);
      this.emitQuantity();
    }
  }

  public emitQuantity(): void {
    this.setQuantity.emit({
      quantity: this.enteredQuantity,
    });

    this.setQuantityAndRequestDate();
  }

  public setQuantityAndRequestDate(): void {
    this.addToCartRequestItem.quantity = this.enteredQuantity;
    this.addToCartRequestItem.requestDate = moment(new Date()).format('YYYY-MM-DD');
  }

  public addToCart(): void {
    if (this.cartLineItemsCount + 1 > this.cartMaxLineItems) {
      this.showAddToCartCapDialog.emit({
        cap: this.cartMaxLineItems,
        cartType: CartType.SHOPPING_CART,
      });
      this.store.dispatch(new ShoppingCartLimit(this.cartMaxLineItems));
    } else {
      this.dialogService.open(AddToCartDialogComponent, {
        data: {
          cartType: CartType.SHOPPING_CART,
          items: [this.addToCartRequestItem],
        },
        size: 'large',
      });
    }
  }

  public addToQuote(): void {
    if (this.quoteCartLineItemsCount + 1 > this.quoteCartMaxLineItems) {
      const requestedItemsCount = this.quoteCartLineItemsCount + 1;
      this.showAddToCartCapDialog.emit({
        cap: this.quoteCartMaxLineItems,
        cartType: CartType.QUOTE_CART,
      });
      this.store.dispatch(new QuoteCartLimit(requestedItemsCount));
    } else {
      const addToQuoteCartRequestItem = this.addToCartRequestItem as IAddToQuoteCartRequestItem;
      addToQuoteCartRequestItem.targetPrice = null;

      this.dialogService.open(AddToCartDialogComponent, {
        data: {
          cartType: CartType.QUOTE_CART,
          items: [addToQuoteCartRequestItem],
        },
        size: 'large',
      });
    }
  }
}
