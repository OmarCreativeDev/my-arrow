import { Component, EventEmitter, forwardRef, Input, Output } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-checkbox',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true,
    },
  ],
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
})
export class CheckboxComponent implements ControlValueAccessor {
  private _value: any;

  @Input() error: boolean;
  @Input() label: string;
  @Input() disabled: boolean = false;
  @Output() valueChange = new EventEmitter<any>();

  get value(): any {
    return this._value;
  }

  @Input()
  set value(val: any) {
    this._value = val;
    this.onChange(val);
  }

  public writeValue(value: any): void {
    this._value = value;
  }

  constructor() {}

  public onChange(value: any) {}
  public onTouched() {}

  public registerOnChange(fn: (value: any) => void): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  public onClick() {
    this.value = !this.value;
    this.valueChange.emit(this.value);
  }

  public onBlur() {
    this.onTouched();
  }
}
