import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CheckboxComponent } from './checkbox.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('CheckboxComponent', () => {
  let component: CheckboxComponent;
  let fixture: ComponentFixture<CheckboxComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [CheckboxComponent],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(CheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit the value on click of input', () => {
    component.value = false;
    spyOn(component.valueChange, 'emit');
    component.onClick();
    expect(component.valueChange.emit).toHaveBeenCalledWith(true);
  });
});
