import { Component, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { ISortCriteronOrderEnum, ISortCriteron } from '@app/shared/shared.interfaces';

@Component({
  selector: '[app-sortable-column-header]',
  templateUrl: './sortable-column-header.component.html',
  styleUrls: ['./sortable-column-header.component.scss'],
})
export class SortableColumnHeaderComponent {
  @Input()
  keys: Array<string>;
  @Input()
  order: ISortCriteronOrderEnum;
  @Output()
  sortChange = new EventEmitter<Array<ISortCriteron>>();

  @HostListener('click')
  public _handleClick(): void {
    const newOrder = !this.order || this.order === ISortCriteronOrderEnum.DESC ? ISortCriteronOrderEnum.ASC : ISortCriteronOrderEnum.DESC;
    const orderCriteria: Array<ISortCriteron> = this.keys.map(key => {
      return {
        key: key,
        order: newOrder,
      };
    });
    this.sortChange.emit(orderCriteria);
  }
}
