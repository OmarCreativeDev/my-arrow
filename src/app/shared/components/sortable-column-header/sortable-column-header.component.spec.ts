import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SortableColumnHeaderComponent } from './sortable-column-header.component';
import { ISortCriteronOrderEnum } from '@app/shared/shared.interfaces';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('SortableColumnHeader', () => {
  let component: SortableColumnHeaderComponent;
  let fixture: ComponentFixture<SortableColumnHeaderComponent>;

  const sortKey = 'dummyKey';

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [SortableColumnHeaderComponent],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SortableColumnHeaderComponent);
    component = fixture.componentInstance;
    component.keys = [sortKey];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should emit an event with the order as ${
    ISortCriteronOrderEnum.ASC
  } if not set`, () => {
    spyOn(component.sortChange, 'emit');
    component.order = undefined;
    component._handleClick();
    expect(component.sortChange.emit).toHaveBeenCalledWith([
      {
        key: sortKey,
        order: ISortCriteronOrderEnum.ASC
      }
    ]);
  });

  it(`should emit an event with the order as ${
    ISortCriteronOrderEnum.ASC
  } if already ${ISortCriteronOrderEnum.DESC}`, () => {
    spyOn(component.sortChange, 'emit');
    component.order = ISortCriteronOrderEnum.DESC;
    component._handleClick();
    expect(component.sortChange.emit).toHaveBeenCalledWith([
      {
        key: sortKey,
        order: ISortCriteronOrderEnum.ASC
      }
    ]);
  });

  it(`should emit an event with the order as ${
    ISortCriteronOrderEnum.DESC
  } if already ${ISortCriteronOrderEnum.ASC}`, () => {
    spyOn(component.sortChange, 'emit');
    component.order = ISortCriteronOrderEnum.ASC;
    component._handleClick();
    expect(component.sortChange.emit).toHaveBeenCalledWith([
      {
        key: sortKey,
        order: ISortCriteronOrderEnum.DESC
      }
    ]);
  });
});
