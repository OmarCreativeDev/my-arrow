import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RohsIconComponent } from './rohs-icon.component';

describe('RohsIconComponent', () => {
  let component: RohsIconComponent;
  let fixture: ComponentFixture<RohsIconComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RohsIconComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RohsIconComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should keep null values if no compliance record is provided', () => {
    component.compliance = '';
    component.ngOnInit();

    expect(component.color).toBeUndefined();
    expect(component.symbol).toBeUndefined();
    expect(component.title).toBeUndefined();
  });

  it('should keep null values if an unrecognised compliance record is provided', () => {
    component.compliance = 'UNKOWN_COMPLIANCE_RECORD';
    component.ngOnInit();

    expect(component.color).toBeUndefined();
    expect(component.symbol).toBeUndefined();
    expect(component.title).toBeUndefined();
  });

  it('should select the China RoHS state `onInit` for compliance `CHINA_ROHS`', () => {
    component.compliance = 'CHINA_ROHS';
    component.ngOnInit();

    expect(component.color).toEqual('grey');
    expect(component.symbol).toEqual('recycle');
    expect(component.title).toEqual('China RoHS');
  });

  it('should select the China RoHS NRC state `onInit` for compliance `CHINA_ROHS_NRC`', () => {
    component.compliance = 'CHINA_ROHS_NRC';
    component.ngOnInit();

    expect(component.color).toEqual('grey');
    expect(component.symbol).toEqual('recycle');
    expect(component.title).toEqual('China RoHS - NRC');
  });

  it('should select the China RoHS RHC state `onInit` for compliance `CHINA_ROHS_RHC`', () => {
    component.compliance = 'CHINA_ROHS_RHC';
    component.ngOnInit();

    expect(component.color).toEqual('green');
    expect(component.symbol).toEqual('recycle-e');
    expect(component.title).toEqual('China RoHS - RHC');
  });

  it('should select the EU RoHS state `onInit` for compliance `EU_ROHS`', () => {
    component.compliance = 'EU_ROHS';
    component.ngOnInit();

    expect(component.color).toEqual('grey');
    expect(component.symbol).toEqual('leaf');
    expect(component.title).toEqual('EU RoHS');
  });

  it('should select the EU RoHS NRC state `onInit` for compliance `EU_ROHS_NRC`', () => {
    component.compliance = 'EU_ROHS_NRC';
    component.ngOnInit();

    expect(component.color).toEqual('grey');
    expect(component.symbol).toEqual('leaf');
    expect(component.title).toEqual('EU RoHS - NRC');
  });

  it('should select the EU RoHS REX state `onInit` for compliance `EU_ROHS_REX`', () => {
    component.compliance = 'EU_ROHS_REX';
    component.ngOnInit();

    expect(component.color).toEqual('grey');
    expect(component.symbol).toEqual('leaf');
    expect(component.title).toEqual('EU RoHS - REX');
  });

  it('should select the EU RoHS RHC state `onInit` for compliance `EU_ROHS_RHC`', () => {
    component.compliance = 'EU_ROHS_RHC';
    component.ngOnInit();

    expect(component.color).toEqual('green');
    expect(component.symbol).toEqual('leaf');
    expect(component.title).toEqual('EU RoHS - RHC');
  });

  it('should select the EU RoHS UNK state `onInit` for compliance `EU_ROHS_UNK`', () => {
    component.compliance = 'EU_ROHS_UNK';
    component.ngOnInit();

    expect(component.color).toEqual('red');
    expect(component.symbol).toEqual('no-sign');
    expect(component.title).toEqual('EU RoHS - UNK');
  });
});
