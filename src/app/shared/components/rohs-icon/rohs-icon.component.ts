import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-rohs-icon',
  templateUrl: './rohs-icon.component.html',
  styleUrls: ['./rohs-icon.component.scss'],
})
export class RohsIconComponent implements OnInit {
  @Input()
  public compliance: string = '';

  public color: string;
  public symbol: string;
  public title: string;

  public COMPLIANCE_STATES = {
    CHINA_ROHS: {
      color: 'grey',
      symbol: 'recycle',
      title: 'China RoHS',
    },
    CHINA_ROHS_NRC: {
      color: 'grey',
      symbol: 'recycle',
      title: 'China RoHS - NRC',
    },
    CHINA_ROHS_RHC: {
      color: 'green',
      symbol: 'recycle-e',
      title: 'China RoHS - RHC',
    },
    EU_ROHS: {
      color: 'grey',
      symbol: 'leaf',
      title: 'EU RoHS',
    },
    EU_ROHS_NRC: {
      color: 'grey',
      symbol: 'leaf',
      title: 'EU RoHS - NRC',
    },
    EU_ROHS_REX: {
      color: 'grey',
      symbol: 'leaf',
      title: 'EU RoHS - REX',
    },
    EU_ROHS_RHC: {
      color: 'green',
      symbol: 'leaf',
      title: 'EU RoHS - RHC',
    },
    EU_ROHS_UNK: {
      color: 'red',
      symbol: 'no-sign',
      title: 'EU RoHS - UNK',
    },
  };

  ngOnInit() {
    if (this.compliance && this.COMPLIANCE_STATES[this.compliance]) {
      const { color, symbol, title } = this.COMPLIANCE_STATES[this.compliance];
      this.color = color;
      this.symbol = symbol;
      this.title = title;
    }
  }
}
