import { Component, ApplicationRef } from '@angular/core';
import { ToastNoAnimation, ToastrService, ToastPackage } from 'ngx-toastr';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  styleUrls: ['./toast.component.scss'],
})
export class ToastComponent extends ToastNoAnimation {
  constructor(public toastrService: ToastrService, public toastPackage: ToastPackage, public appRef: ApplicationRef) {
    super(toastrService, toastPackage, appRef);
  }
}
