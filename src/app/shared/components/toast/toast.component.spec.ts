import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToastComponent } from './toast.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ToastrService, ToastPackage } from 'ngx-toastr';
import { of } from 'rxjs';

class MockToastrService {
  public success() {}
  public remove() {}
}

class MockToastPackage {
  public config = {};
  public toastRef = {
    afterActivate: () => of([]),
    manualClosed: () => of([]),
    timeoutReset: () => of([]),
  };
}

describe('ToastComponent', () => {
  let component: ToastComponent;
  let fixture: ComponentFixture<ToastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ToastComponent],
      providers: [{ provide: ToastrService, useClass: MockToastrService }, { provide: ToastPackage, useClass: MockToastPackage }],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
