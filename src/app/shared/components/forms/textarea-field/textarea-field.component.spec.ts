import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';
import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TextareaFieldComponent } from './textarea-field.component';

@Component({
  selector: 'app-textarea-field-mock-component',
  template: `
    <form #f="ngForm">
      <app-textarea-field [form]="f"></app-textarea-field>
      <button type="submit"></button>
    </form>
  `,
})
export class MockFieldComponent {}

describe('TextareaFieldComponent', () => {
  let component: TextareaFieldComponent;
  let formFieldComponent: TextareaFieldComponent;
  let fixture: ComponentFixture<TextareaFieldComponent>;
  let mockFixture: ComponentFixture<MockFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [TextareaFieldComponent, MockFieldComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextareaFieldComponent);
    component = fixture.componentInstance;
    mockFixture = TestBed.createComponent(MockFieldComponent);
    formFieldComponent = mockFixture.debugElement.children[0].children[0].componentInstance;
    formFieldComponent.control = new FormControl('');
    mockFixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
