import { Component, Input } from '@angular/core';
import { FormField } from '@app/shared/classes/form-field.class';

@Component({
  selector: 'app-textarea-field',
  templateUrl: './textarea-field.component.html'
})
export class TextareaFieldComponent extends FormField {
  @Input() public placeholder: string;
  @Input() public maxlength = '250';
}
