import { Component, Input } from '@angular/core';
import { FormField } from '@app/shared/classes/form-field.class';

@Component({
  selector: 'app-input-field',
  templateUrl: './input-field.component.html',
})
export class InputFieldComponent extends FormField {
  @Input()
  public type = 'text';
  @Input()
  public placeholder: string;
  @Input()
  public maxlength = '100';
  @Input()
  public disabled: boolean = false;
  @Input()
  public showErrorAfter: boolean = false;
}
