import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';
import { Component, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormControl, Validators } from '@angular/forms';
import { InputFieldComponent } from './input-field.component';
import { CustomValidators } from '@app/shared/classes/custom-validators';

@Component({
  selector: 'app-input-field-mock-component',
  template: `
    <form #f="ngForm">
      <app-input-field [form]="f"></app-input-field>
      <button type="submit"></button>
    </form>
  `,
})
class MockInputFieldComponent {}

describe('InputFieldComponent', () => {
  let inputFieldComponent: InputFieldComponent;
  let mockFixture: ComponentFixture<MockInputFieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [InputFieldComponent, MockInputFieldComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    mockFixture = TestBed.createComponent(MockInputFieldComponent);
    inputFieldComponent = mockFixture.debugElement.children[0].children[0].componentInstance;
    inputFieldComponent.control = new FormControl('', CustomValidators.email);
    mockFixture.detectChanges();
  });

  it('should create', () => {
    expect(inputFieldComponent).toBeTruthy();
  });

  it('should mark field as touched when form is submitted', fakeAsync(() => {
    const button = mockFixture.nativeElement.querySelector('button');
    button.click();
    expect(inputFieldComponent.form.submitted).toBeTruthy();
    expect(inputFieldComponent.control.touched).toBeTruthy();
  }));

  it(`should set correct 'email' validation error message on blank email`, () => {
    inputFieldComponent.control = new FormControl('', Validators.required);
    inputFieldComponent.ngOnInit();
    expect(inputFieldComponent.control.errors.required).toBeTruthy();
  });

  it(`should set correct 'email' validation error message on incorrect email`, () => {
    inputFieldComponent.control = new FormControl('not.an.email.com', CustomValidators.email);
    inputFieldComponent.ngOnInit();
    expect(inputFieldComponent.control.errors.email).toBeTruthy();
  });

  it(`should set correct 'required' validation error message on required field`, () => {
    inputFieldComponent.control = new FormControl('', Validators.required);
    inputFieldComponent.ngOnInit();
    expect(inputFieldComponent.control.errors.required).toBeTruthy();
  });

  it(`should set correct 'password' validation error message on incorrect password`, () => {
    inputFieldComponent.control = new FormControl('1234', CustomValidators.password);
    inputFieldComponent.ngOnInit();
    expect(inputFieldComponent.control.errors.password).toBeTruthy();
  });
});
