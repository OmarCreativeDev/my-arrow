import { Component, Input } from '@angular/core';
import { FormField } from '@app/shared/classes/form-field.class';

@Component({
  selector: 'app-form-item',
  templateUrl: './form-item.component.html',
})
export class FormItemComponent {
  @Input()
  public formField: FormField;
  @Input()
  public showErrorAfter: boolean = false;
}
