export interface IPricePostData {
  cpn: string;
  endCustomerSiteId?: number;
  selectForPricing?: boolean;
}
