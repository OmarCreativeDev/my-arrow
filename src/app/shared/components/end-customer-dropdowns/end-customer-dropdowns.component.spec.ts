import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, SimpleChange, SimpleChanges } from '@angular/core';
import { EndCustomerDropdownsComponent } from './end-customer-dropdowns.component';

export const singleCpnAndSingleEndCustomerListMock = [
  {
    cpn: '1111 1111',
    endCustomers: [
      {
        name: 'Siemens Industry Inc',
        endCustomerSiteId: 2182694,
      },
    ],
  },
];

export const endCustomerMock = [
  {
    name: 'Siemens Industry Inc',
    endCustomerSiteId: 2182694,
  },
];

export const multipleEndCustomersListMock = [
  {
    name: 'Siemens Industry Inc',
    endCustomerSiteId: 2182694,
  },
  {
    name: 'Google',
    endCustomerSiteId: 2147294,
  },
];

export const multipleCpnAndEndCustomersListMock = [
  {
    cpn: '1111 1111',
    endCustomers: [
      {
        name: 'Siemens Industry Inc',
        endCustomerSiteId: 2182694,
      },
      {
        name: 'Google',
        endCustomerSiteId: 2147294,
      },
    ],
  },
  {
    cpn: '2222 2222',
    endCustomers: [
      {
        name: 'Intel',
        endCustomerSiteId: 2182764,
      },
      {
        name: 'Apple',
        endCustomerSiteId: 2147984,
      },
    ],
  },
];

describe('EndCustomerDropdownsComponent', () => {
  let component: EndCustomerDropdownsComponent;
  let fixture: ComponentFixture<EndCustomerDropdownsComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [EndCustomerDropdownsComponent],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(EndCustomerDropdownsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should invoke both `setCpns` & `cpnSearchQueryMatch` on `searchQuery` change', () => {
    const mockSimpleChanges: SimpleChanges = {
      searchQuery: new SimpleChange(null, '1111 1111', true),
    };

    spyOn(component, 'setCpns');
    spyOn(component, 'cpnSearchQueryMatch');

    component.ngOnChanges(mockSimpleChanges);

    expect(component.setCpns).toHaveBeenCalled();
    expect(component.cpnSearchQueryMatch).toHaveBeenCalled();
  });

  it('should set `selectedCpn` to `searchQuery` & invoke `getEndCustomerRecord`', () => {
    const mockSimpleChanges: SimpleChanges = {
      searchQuery: new SimpleChange(null, '1111 1111', true),
    };
    component.data = singleCpnAndSingleEndCustomerListMock;

    spyOn(component, 'getEndCustomerRecord');

    component.ngOnChanges(mockSimpleChanges);

    component.setCpns();
    component.cpnSearchQueryMatch();

    expect(component.getEndCustomerRecord).toHaveBeenCalled();
  });

  it('should set both `endCustomerList` & `endCustomerRecord` & invoke `checkEndCustomerLength`', () => {
    component.data = singleCpnAndSingleEndCustomerListMock;
    component.selectedCpn = '1111 1111';

    spyOn(component, 'checkEndCustomerLength');

    component.getEndCustomerRecord();

    expect(component.checkEndCustomerLength).toHaveBeenCalled();
  });

  it('`onCpnChange` should set the selected cpn and invoke `getEndCustomerRecord()` and `emitCpnChange`', () => {
    spyOn(component, 'getEndCustomerRecord');

    component.data = multipleCpnAndEndCustomersListMock;
    component.onCpnChange('1111 1111');

    expect(component.selectedCpn).toEqual('1111 1111');
    expect(component.getEndCustomerRecord).toHaveBeenCalled();
  });

  it('should emit dropdownsSelectionChange event when `onDropdownsSelectionChange` is invoked', () => {
    spyOn(component, 'onEndCustomerChange');
    component.data = multipleCpnAndEndCustomersListMock;
    component.selectedEndCustomer = 'Google';
    component.onEndCustomerChange('Google');
    expect(component.onEndCustomerChange).toHaveBeenCalled();
  });

  it('should invoke `emitDropdownsSelectionChange` and return false when there are no end customers', () => {
    component.endCustomers = [];

    spyOn(component, 'emitDropdownsSelectionChange');
    component.emitDropdownsSelectionChange();

    expect(component.emitDropdownsSelectionChange).toHaveBeenCalled();
  });

  it('should check end customer length & invoke both `emitPriceRequest` & `initEndCustomerDropdown`', () => {
    component.endCustomers = endCustomerMock;

    spyOn(component, 'emitDropdownsSelectionChange');
    spyOn(component, 'initEndCustomerDropdown');
    component.checkEndCustomerLength();

    expect(component.emitDropdownsSelectionChange).toHaveBeenCalled();
    expect(component.initEndCustomerDropdown).toHaveBeenCalled();
  });

  it('should set `selectedEndCustomer` to `initialDropdownValue` & invoke `initEndCustomerDropdown`', () => {
    component.endCustomers = multipleEndCustomersListMock;

    spyOn(component, 'initEndCustomerDropdown');
    component.checkEndCustomerLength();

    expect(component.initEndCustomerDropdown).toHaveBeenCalled();
  });

  it('should invoke `initEndCustomerDropdown` with a single end customer', () => {
    component.endCustomers = endCustomerMock;

    component.initEndCustomerDropdown();
  });

  it('should invoke `initEndCustomerDropdown` with multiple end customers', () => {
    component.endCustomers = multipleEndCustomersListMock;

    component.initEndCustomerDropdown();
  });

  it('should set `selectedEndCustomer` when `defaultEndCustomerId` is passed', () => {
    component.defaultEndCustomerId = multipleEndCustomersListMock[1].endCustomerSiteId;
    component.endCustomers = multipleEndCustomersListMock;
    component.initEndCustomerDropdown();
    expect(component.selectedEndCustomer).toBe(multipleEndCustomersListMock[1].name);
  });

  it('should trigger `getEndCustomerRecord` with a default cpn', () => {
    spyOn(component, 'getEndCustomerRecord');
    component.data = singleCpnAndSingleEndCustomerListMock;
    component.defaultCpn = '1111 1111';
    component.ngOnInit();
    expect(component.getEndCustomerRecord).toHaveBeenCalledWith();
  });
});
