import find from 'lodash-es/find';
import { Component, OnChanges, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { IEndCustomer } from '@app/core/inventory/product-search.interface';
import { ISelectableOption } from '@app/shared/shared.interfaces';
import { IPricePostData } from '@app/shared/components/end-customer-dropdowns/end-customer-dropdowns.interface';

@Component({
  selector: 'app-end-customer-dropdowns',
  templateUrl: './end-customer-dropdowns.component.html',
  styleUrls: ['./end-customer-dropdowns.component.scss'],
})
export class EndCustomerDropdownsComponent implements OnChanges, OnInit {
  @Input()
  public data;
  @Input()
  public defaultCpn: string = null;
  @Input()
  public defaultEndCustomerId: number = null;
  @Input()
  public searchQuery: string = null;

  @Output()
  public dropdownsSelectionChange: EventEmitter<IPricePostData> = new EventEmitter<IPricePostData>(true);

  public initialDropdownValue = { label: null, value: null };

  public cpnList: ISelectableOption<any>[] = [this.initialDropdownValue];
  public disabledCPNDropdown: boolean = false;
  public disabledECDropdown: boolean = false;
  public endCustomerList: ISelectableOption<any>[] = [this.initialDropdownValue];
  public endCustomers: IEndCustomer[] = null;
  public selectedCpn: string = null;
  public selectedEndCustomer: string = null;
  public showEndCustomers: boolean = false;

  public ngOnInit() {
    if (this.defaultCpn) {
      this.selectedCpn = this.defaultCpn;
      this.getEndCustomerRecord();
    }
  }

  public ngOnChanges(changes: SimpleChanges): void {
    /* istanbul ignore else */
    if (changes.searchQuery) {
      this.setCpns();
      this.searchQuery = changes.searchQuery.currentValue;
      this.cpnSearchQueryMatch();
    }
  }

  public setCpns(): void {
    for (const record of this.data) {
      const cpn = {
        label: record.cpn,
        value: record.cpn,
      };
      if (!find(this.cpnList, cpn)) {
        this.cpnList.push(cpn);
      }
    }
  }

  public getEndCustomerRecord(): void {
    // Reset the list
    this.endCustomerList = [this.initialDropdownValue];

    // find end customer
    const endCustomerRecord = this.data.find(o => o.cpn === this.selectedCpn);

    // set the end customers (and clean out any dirty data)
    if (endCustomerRecord && endCustomerRecord.endCustomers) {
      this.endCustomers = endCustomerRecord.endCustomers.filter(ec => {
        return ec.endCustomerSiteId !== null;
      });
    } else {
      this.endCustomers = [];
    }

    // undertake further actions
    this.checkEndCustomerLength();
  }

  public checkEndCustomerLength() {
    // There are no End Customers
    if (!this.endCustomers || !this.endCustomers.length) {
      this.showEndCustomers = false;
      this.emitDropdownsSelectionChange();
      return false;
      // There is only one End Customer
    } else if (this.endCustomers && this.endCustomers.length < 2) {
      this.disabledECDropdown = true;
      this.selectedEndCustomer = this.endCustomers[0].name;
      this.emitDropdownsSelectionChange();
    } else {
      this.endCustomerList = [this.initialDropdownValue];
    }

    this.initEndCustomerDropdown();
  }

  public initEndCustomerDropdown(): void {
    let defaultEndCustomer = null;
    // Show the End Customer dropdown
    this.showEndCustomers = true;

    // Set the options
    for (const endCustomer of this.endCustomers) {
      this.endCustomerList.push({
        label: endCustomer.name,
        value: endCustomer.name,
      });
      if (endCustomer.endCustomerSiteId === this.defaultEndCustomerId) {
        defaultEndCustomer = endCustomer.name;
      }
    }
    if (this.endCustomerList.length === 2) {
      this.selectedEndCustomer = this.endCustomerList[1].label;
    } else if (defaultEndCustomer) {
      this.selectedEndCustomer = defaultEndCustomer;
    } else {
      this.selectedEndCustomer = this.endCustomerList[0].label;
      this.emitDropdownsSelectionChange();
    }
  }

  public cpnSearchQueryMatch(): void {
    for (const cpn of this.cpnList) {
      if (this.searchQuery === cpn.label) {
        this.selectedCpn = this.searchQuery;
        this.getEndCustomerRecord();
      }
    }
  }

  public onCpnChange($event) {
    this.selectedCpn = $event;
    this.getEndCustomerRecord();
  }

  public onEndCustomerChange($event) {
    this.emitDropdownsSelectionChange();
  }

  public getDropdownsSelection(): IPricePostData {
    // set cpn
    const priceRequestData = {
      cpn: this.selectedCpn,
      endCustomerSiteId: null,
      selectForPricing: false,
    };

    // If more than one end customer set a flag
    if (this.endCustomers.length > 1 && !this.selectedEndCustomer) {
      priceRequestData['selectForPricing'] = true;
    }

    // set endCustomerSiteId (if available)
    if (this.endCustomers && this.endCustomers.length) {
      const selectedRecord = this.endCustomers.find(o => o.name === this.selectedEndCustomer);
      const endCustomerSiteId = selectedRecord ? selectedRecord.endCustomerSiteId : null;

      if (endCustomerSiteId) {
        priceRequestData['endCustomerSiteId'] = endCustomerSiteId;
      }
    }

    return priceRequestData;
  }

  public emitDropdownsSelectionChange(): void {
    this.dropdownsSelectionChange.emit(this.getDropdownsSelection());
  }
}
