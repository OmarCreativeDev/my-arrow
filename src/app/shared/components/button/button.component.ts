import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  @Input()
  label: string;
  @Input()
  loading: boolean;
  @Input()
  disabled: boolean;
  @Input()
  type: string = 'button';
  @Input()
  cssClass: string;
  @Output()
  buttonClick = new EventEmitter();

  public onClick($event: MouseEvent): void {
    if (!this.loading) {
      this.buttonClick.emit($event);
    }
  }
}
