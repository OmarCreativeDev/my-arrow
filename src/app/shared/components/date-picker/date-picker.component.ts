import { Overlay, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';
import {
  Component,
  ComponentRef,
  ElementRef,
  EventEmitter,
  forwardRef,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  Output,
  Renderer2,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { IExcludeDates } from '@app/shared/components/calendar/calendar.component';
import { DatePickerOverlayActions } from '@app/shared/components/date-picker/date-picker-overlay/date-picker-overlay.interface';
import { DATE_PICKER_DATA } from '@app/shared/components/date-picker/date-picker.constants';
import { DatePickerDisplayStatus, IDateInputConfig } from '@app/shared/components/date-picker/date-picker.interfaces';
import { Subscription } from 'rxjs';

import { DatePickerOvelayComponent } from './date-picker-overlay/date-picker-overlay.component';
import { cloneDeep } from 'lodash-es';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatePickerComponent),
      multi: true,
    },
  ],
})
export class DatePickerComponent implements OnInit, OnDestroy, ControlValueAccessor {
  @ViewChild('datePickerForm')
  form: ElementRef;

  @Input()
  actions: Array<DatePickerOverlayActions> = [
    DatePickerOverlayActions.RESET,
    DatePickerOverlayActions.GO_TO_TODAY,
    DatePickerOverlayActions.SUBMIT,
  ];
  @Input()
  dates: Array<Date> = [];
  @Input()
  dateInputs: Array<IDateInputConfig>;
  @Input()
  minDate: Date;
  @Input()
  maxDate: Date;
  @Input()
  calendarCount: number;
  @Input()
  submitLabel: string;
  @Input()
  showActions: boolean = true;
  @Input()
  exclusions: IExcludeDates;
  @Input()
  disabled: boolean = false;
  @Input()
  fixedPosition: number;
  @Output()
  datesChange = new EventEmitter<Array<Date>>();
  @Output()
  closed = new EventEmitter<void>();

  public actionSubscription: Subscription;
  public componentRef: ComponentRef<DatePickerOvelayComponent>;
  public currentDateIndex: number;
  public currentDateIndexSubscription: Subscription;
  public handler;
  public displayStatus: DatePickerDisplayStatus = DatePickerDisplayStatus.HIDE;
  public overlayRef: OverlayRef;
  private prevDates: Array<Date> = [];

  public onChangeFunction: Function = (_: any) => {};
  public onTouchedFunction: Function = () => {};

  constructor(private elementRef: ElementRef, private overlay: Overlay, private renderer: Renderer2, private injector: Injector) {}

  ngOnInit() {
    this.overlayRef = this.overlay.create({
      positionStrategy: this.overlay
        .position()
        .connectedTo(this.elementRef, { originX: 'start', originY: 'bottom' }, { overlayX: 'start', overlayY: 'top' }),
      scrollStrategy: this.overlay.scrollStrategies.reposition(),
    });
    this.writeValue(this.dates);
    this.updateInitialPrevDates();
  }

  ngOnDestroy() {
    this.overlayRef.dispose();
  }

  private updateInitialPrevDates(): void {
    for (let i = 0; i < this.calendarCount; i++) {
      this.prevDates[i] = undefined;
    }
  }

  public handleActionSubscription(action: DatePickerOverlayActions) {
    if (action === DatePickerOverlayActions.SUBMIT) {
      this.submitDates();
      this.hide();
    }
  }

  public handleCurrentDateIndexSubscription(currentDateIndex: number) {
    this.currentDateIndex = currentDateIndex;
  }

  public open(index) {
    this.currentDateIndex = index;

    /* istanbul ignore else */
    if (!this.disabled) {
      this.displayStatus = DatePickerDisplayStatus.SHOW;

      if (this.overlayRef.hasAttached()) {
        this.componentRef.instance.focusDateIndex(this.currentDateIndex);
      } else {
        this.handler = this.renderer.listen(document, 'click', $event => {
          if (
            this.overlayRef.hasAttached() &&
            !this.form.nativeElement.contains($event.target) &&
            !this.overlayRef.overlayElement.contains($event.target)
          ) {
            this.writeValue(this.prevDates);
            this.close();
          }
        });

        setTimeout(() => {
          const { actions, calendarCount, dates, exclusions, maxDate, minDate, showActions, submitLabel, fixedPosition } = this;
          const injector = this.createInjector({
            actions,
            calendarCount,
            dates,
            exclusions,
            maxDate,
            minDate,
            showActions,
            submitLabel,
            fixedPosition,
          });
          const filePreviewPortal = new ComponentPortal(DatePickerOvelayComponent, null, injector);
          this.componentRef = this.overlayRef.attach(filePreviewPortal);

          this.componentRef.instance.focusDateIndex(this.currentDateIndex);
          this.actionSubscription = this.componentRef.instance.action.subscribe((action: DatePickerOverlayActions) =>
            this.handleActionSubscription(action)
          );
          this.currentDateIndexSubscription = this.componentRef.instance.currentDateIndexChanged.subscribe((currentDateIndex: number) =>
            this.handleCurrentDateIndexSubscription(currentDateIndex)
          );

          setTimeout(() => {
            const el = this.overlayRef.overlayElement;
            const pushVertical = window.innerHeight < el.offsetTop + el.clientHeight;
            const pushHorizontal = window.innerWidth < el.offsetLeft + el.clientWidth;
            if (pushVertical) {
              const pos = this.fixedPosition >= 0 ? this.fixedPosition : el.clientHeight - (window.innerHeight - el.offsetTop);
              this.renderer.setStyle(el, 'transform', `translateY(-${pos}px)`);
            }

            if (pushHorizontal) {
              const pos = this.fixedPosition >= 0 ? this.fixedPosition : el.clientWidth - (window.innerWidth - el.offsetLeft);
              this.renderer.setStyle(el, 'transform', `translateX(-${pos}px)`);
            }
          });
        });
      }
    }
  }

  public close() {
    this.currentDateIndex = undefined;
    this.renderer.removeStyle(this.overlayRef.overlayElement, 'transform');
    this.overlayRef.detach();
    this.actionSubscription.unsubscribe();
    this.currentDateIndexSubscription.unsubscribe();
    this.handler();
    this.displayStatus = DatePickerDisplayStatus.HIDE;
    this.closed.emit();
  }

  public hide() {
    this.overlayRef.overlayElement.style.visibility = 'hidden';

    setTimeout(() => {
      this.close();
      this.overlayRef.overlayElement.style.visibility = '';
    }, 300);
  }

  // From ControlValueAccessor
  public writeValue(value: Array<Date>): void {
    if (value) {
      this.dates = value;
    } else {
      this.dates = [];
    }
    this.prevDates = cloneDeep(this.dates);
    this.onChangeFunction(this.dates);
  }

  // From ControlValueAccessor
  public registerOnChange(fn: any): void {
    this.onChangeFunction = fn;
  }

  // From ControlValueAccessor
  public registerOnTouched(fn: any): void {
    this.onTouchedFunction = fn;
  }

  // From ControlValueAccessor
  public setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  /**
   * Emits the values and triggers the callbacks to indicate a change in value
   */
  public submitDates(): void {
    this.datesChange.emit(this.dates);
    this.writeValue(this.dates);
    this.onTouchedFunction();
  }

  public createInjector(data: any): PortalInjector {
    const injectionTokens = new WeakMap();

    injectionTokens.set(DATE_PICKER_DATA, data);

    return new PortalInjector(this.injector, injectionTokens);
  }

  public isOpen(): boolean {
    return this.displayStatus === DatePickerDisplayStatus.SHOW;
  }

  public isActive(index: number): boolean {
    return this.isOpen() && index === this.currentDateIndex;
  }

  public isInactive(index: number): boolean {
    return this.isOpen() && index !== this.currentDateIndex;
  }
}
