import { InjectionToken } from '@angular/core';

export const DATE_PICKER_DATA = new InjectionToken<any>('DATE_PICKER_DATA');
