export interface IDateMove {
  index: number;
  delta: number;
}

export enum DatePickerOverlayActions {
  GO_TO_TODAY = 'GO_TO_TODAY',
  RESET = 'RESET',
  SUBMIT = 'SUBMIT',
}
