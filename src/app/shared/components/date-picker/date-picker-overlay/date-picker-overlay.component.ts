import { Component, forwardRef, Inject, Input, OnChanges, OnInit, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import * as moment from 'moment';
import { IExcludeDates } from '@app/shared/components/calendar/calendar.component';
import { CalendarDisplayState, CalendarMonth, DatePickerHelper } from '@app/shared/components/date-picker/date-picker.classes';
import { DATE_PICKER_DATA } from '@app/shared/components/date-picker/date-picker.constants';
import { DatePickerOverlayActions, IDateMove } from '@app/shared/components/date-picker/date-picker-overlay/date-picker-overlay.interface';

@Component({
  selector: 'app-date-picker-overlay',
  templateUrl: './date-picker-overlay.component.html',
  styleUrls: ['./date-picker-overlay.component.scss'],
  providers: [
    DatePickerHelper,
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DatePickerOvelayComponent),
      multi: true,
    },
  ],
})
export class DatePickerOvelayComponent implements OnInit, OnChanges {
  @Input()
  actions: Array<DatePickerOverlayActions> = [];
  @Input()
  calendarCount: number = 1;
  @Input()
  dates: Array<Date> = [new Date()];
  @Input()
  exclusions: IExcludeDates;
  @Input()
  maxDate: Date;
  @Input()
  minDate: Date;
  @Input()
  showActions: boolean = true;
  @Input()
  submitLabel: string;
  @Output()
  action: EventEmitter<DatePickerOverlayActions> = new EventEmitter<DatePickerOverlayActions>();
  @Output()
  currentDateIndexChanged: EventEmitter<number> = new EventEmitter<number>();

  public calendarDisplayState: CalendarDisplayState;
  public currentDateIndex = this.calendarCount > 1 ? -1 : 0;
  public datePickerOverlayActions = DatePickerOverlayActions;
  public hoverDate: Date;
  public months: Array<CalendarMonth> = [];
  public rootDate: moment.Moment = moment();

  constructor(private datePickerHelper: DatePickerHelper, @Inject(DATE_PICKER_DATA) public datePickerData: any) {
    this.goToToday();

    this.actions = datePickerData.actions;
    this.calendarCount = datePickerData.calendarCount;
    this.dates = datePickerData.dates;
    this.exclusions = datePickerData.exclusions;
    this.maxDate = datePickerData.maxDate;
    this.minDate = datePickerData.minDate;
    this.showActions = datePickerData.showActions;
    this.submitLabel = datePickerData.submitLabel;
  }

  ngOnInit() {
    this.updateCalendars();
  }

  ngOnChanges(changes: SimpleChanges) {
    /* istanbul ignore else */
    if (changes.dates) {
      this.updateCalendars();
    }
  }

  private updateCalendars() {
    /* istanbul ignore else */
    if (this.dates && this.dates.length) {
      const activeDate = this.dates[this.currentDateIndex];
      if (activeDate !== undefined) {
        this.rootDate = moment(activeDate);
        this.months = this.updateMonths(this.rootDate, this.calendarCount);
      } else {
        this.goToToday();
      }
      this.updateCalendarDisplayState();
    }
  }

  /**
   * Changes the calendars to show today's day
   */
  public goToToday() {
    this.rootDate = moment().startOf('month');
    this.months = this.updateMonths(this.rootDate, this.calendarCount);
  }

  /**
   * Focusses a provided index for selection
   * @param index the index to focus
   */
  public focusDateIndex(index: number) {
    this.setCurrentDateIndex(index);
    this.show();
  }

  /**
   * Triggered when a date is clicked on a calendar; will focussed the date if the date has already been selected, or make the selection for the current index
   * @param date the date selected
   */
  public handleDateSelection(date: Date) {
    // If the date has already been selected, change the currentDateIndex so that it can be changed
    const selectedIndex = this.datePickerHelper.getDateSelectionIndex(date, this.dates);
    // Date is already selected, focus on that date
    if (selectedIndex >= 0) {
      this.setCurrentDateIndex(selectedIndex);
    } else {
      // Set the date
      this.dates[this.currentDateIndex] = date;
      // Check and adjust pre/post dates
      this.dates = this.adjustSelectedDates(date, this.dates, this.currentDateIndex);
      // Increment the currentDateIndex
      this.setCurrentDateIndex(this.currentDateIndex + 1);
      // Check we aren't selecting an index that is out of range
      /* istanbul ignore else */
      if (this.currentDateIndex > this.dates.length - 1) {
        this.setCurrentDateIndex(this.dates.length - 1);
      }
    }
    this.updateCalendarDisplayState();
    /* istanbul ignore else */
    if (!this.showActions) {
      this.submitDates();
    }
  }

  /**
   * Moves to the next month
   */
  public next(): void {
    this.changeRootDateByMonthDelta(1);
  }

  /**
   * Moves to the previous month
   */
  public previous(): void {
    this.changeRootDateByMonthDelta(-1);
  }

  /**
   * Changes the root date and updates the calendars
   * @param delta the amount of months to change the rootDate by
   */
  private changeRootDateByMonthDelta(delta: number) {
    if (delta > 0) {
      this.rootDate.add(Math.abs(1), 'month');
    } else {
      this.rootDate.subtract(Math.abs(delta), 'month');
    }
    this.months = this.updateMonths(this.rootDate, this.calendarCount);
  }

  /**
   * Overrides the super `show` to select and focus the first date
   */
  public show(): void {
    /* istanbul ignore else */
    if (this.currentDateIndex < 0) {
      this.setCurrentDateIndex(0);
    }
    this.updateCalendars();
  }

  /**
   * Overrides the super `hide` to reset the date index
   */
  public hide(): void {
    this.setCurrentDateIndex(-1);

    this.action.emit(DatePickerOverlayActions.SUBMIT);
  }

  /**
   * Resets the dates and moves the calendars back to today's month
   */
  public reset(): void {
    for (let i = 0; i < this.calendarCount; i++) {
      this.dates[i] = undefined;
    }

    this.setCurrentDateIndex(0);
    this.updateCalendarDisplayState();
    this.goToToday();
  }

  /**
   * Emits the values and triggers the callbacks to indicate a change in value
   */
  public submitDates(): void {
    this.hide();
  }

  /**
   * Determines if the currently selected values can be submitted
   */
  public canSubmit(): boolean {
    const selection = this.dates.filter(date => date !== undefined);
    return selection.length === this.dates.length;
  }

  /**
   * Determines if the currently selected values can be reset
   */
  public canReset(): boolean {
    const selection = this.dates.filter(date => date !== undefined);
    return selection.length > 0;
  }

  /**
   * Sets the current hover date; used by calculations in each calendar about which dates are highlighted
   * @param date the date that has been entered
   */
  public handleDateEnter(date: Date): void {
    /* istanbul ignore else */
    if (this.dates.length > 1) {
      this.hoverDate = date;
      this.updateCalendarDisplayState();
    }
  }

  public handleDateLeave(): void {
    this.hoverDate = undefined;
    this.updateCalendarDisplayState();
  }

  /**
   * Determines if today's day is visible
   */
  public todayVisible(): boolean {
    return this.dateInView(new Date(), this.months);
  }

  private updateCalendarDisplayState() {
    this.calendarDisplayState = new CalendarDisplayState(
      this.dates.map(date => {
        return date ? new Date(date) : undefined;
      }),
      this.currentDateIndex,
      this.hoverDate
    );
  }

  /**
   * Given a date, moves dates which are before and/or after, depending on the current ordinance
   * @param date the date around which the shifts should occur
   */
  private adjustSelectedDates(date: Date, dates: Array<Date>, activeDateIndex: number): Array<Date> {
    const dateMoves = [
      ...this.getDatesMovesBeforeDate(date, dates, activeDateIndex),
      ...this.getDatesMovesAfterDate(date, dates, activeDateIndex),
    ];
    // Calculate and perform shits on the dates before and after
    return this.performDateShifts(date, dateMoves, dates);
  }

  /**
   * Gets a list of moves for all dates from the datePool before the specified index
   * @param date the comparison date
   * @param datePool the pool of dates to potentially move
   * @param dateIndex the index to start from
   */
  private getDatesMovesBeforeDate(date: Date, datePool: Array<Date>, dateIndex: number): Array<IDateMove> {
    const beforeMoves: Array<IDateMove> = [];
    // Only possible to check dates if the date for comparison is not the first
    /* istanbul ignore else */
    if (dateIndex > 0) {
      // Get all dates from the pool before the comparison date
      const datesBefore = datePool.slice(0, dateIndex);
      // Cache the index; this will be iterated backwards for each date
      let datesBeforeIndex = datesBefore.length;
      // Iterate back through the dates
      while (datesBeforeIndex--) {
        // Cache the date
        const beforeDate = datesBefore[datesBeforeIndex];
        // Compare against the provided date
        /* istanbul ignore else */
        if (beforeDate && beforeDate.getTime() >= date.getTime()) {
          // If it was before the provided date, we need move the date n number of days before the comparison date
          beforeMoves.push({
            index: datesBeforeIndex,
            delta: datesBeforeIndex - dateIndex,
          });
        }
      }
    }
    // Return all of the date moves for dates before the provided dates
    return beforeMoves;
  }

  /**
   * Gets a list of moves for all dates from the datePool after the specified index
   * @param date the comparison date
   * @param datePool the pool of dates to potentially move
   * @param dateIndex the index to start from
   */
  private getDatesMovesAfterDate(date: Date, datePool: Array<Date>, dateIndex: number): Array<IDateMove> {
    const afterMoves: Array<IDateMove> = [];
    // Only possible to move dates if the provided date isn't the last one
    /* istanbul ignore else */
    if (dateIndex < datePool.length - 1) {
      // Cache the start index; this represents the first date after the provided date
      const startIndex = dateIndex + 1;
      // All dates after the provided index
      const datesAfter = datePool.slice(startIndex);
      // Iterate through each date after the provided date
      datesAfter.forEach((afterDate: Date, index: number) => {
        // If the date is before the provided date
        /* istanbul ignore else */
        if (afterDate && afterDate.getTime() <= date.getTime()) {
          // Need to move the date by a calculated n number of days, based on its distance from the provided date's index
          afterMoves.push({
            index: startIndex + index,
            delta: startIndex + index - dateIndex,
          });
        }
      });
    }
    // Return the list of moves for the dates after the provided date
    return afterMoves;
  }

  /**
   * Performs a list of moves on dates
   * @param fromDate date that we're moving dates because of
   * @param dateMoves a list of moves that we need to make because of the fromDate
   */
  private performDateShifts(fromDate: Date, dateMoves: Array<IDateMove>, dates: Array<Date>): Array<Date> {
    // Create a moment to shift from
    const startDate = moment(fromDate);
    // Iterate through each date move
    for (const dateMove of dateMoves) {
      let dateMoveMoment: moment.Moment;
      // If the date move is positive, days need adding
      if (dateMove.delta > 0) {
        dateMoveMoment = startDate.add(Math.abs(dateMove.delta), 'days');
      } else {
        // If the date move is negative, days need subtracting
        dateMoveMoment = startDate.subtract(Math.abs(dateMove.delta), 'days');
      }
      // Assign the Date to the specified index
      dates[dateMove.index] = dateMoveMoment.toDate();
    }
    return dates;
  }

  /**
   * Sets the calendars from a provided rootDate
   */
  private updateMonths(rootDate: moment.Moment, calendarCount: number): Array<CalendarMonth> {
    // Blank the calendars
    const months = [];
    // Create and push a rootDate for each calendar
    for (let i = 0, l = calendarCount; i < l; i++) {
      // Set the root for the month
      const calendarRootDate = moment(rootDate).add(i, 'month');
      // Create the month
      const month = new CalendarMonth(calendarRootDate);
      // Add to the array of calendars
      months.push(month);
    }
    return months;
  }

  /**
   * Determines if a provided date is in any of the current calendars
   * @param date the date to find
   */
  private dateInView(date: Date, months: Array<CalendarMonth>): boolean {
    const stamp = date.getTime();
    const dateMonth = months.find(month => {
      const end = moment(month.start)
        .endOf('month')
        .toDate();
      return month.start.toDate().getTime() <= stamp && end.getTime() >= stamp;
    });
    return dateMonth !== undefined;
  }

  private setCurrentDateIndex(currentDateIndex: number): void {
    this.currentDateIndex = currentDateIndex;

    this.currentDateIndexChanged.emit(this.currentDateIndex);
  }
}
