import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatePickerComponent } from './date-picker.component';
import { CUSTOM_ELEMENTS_SCHEMA, ElementRef, Renderer2, Injector } from '@angular/core';
import { IsoDatePipe } from '@app/shared/pipes/iso-date/iso-date.pipe';
import { DatePipe } from '@angular/common';
import {
  Overlay,
  ScrollStrategyOptions,
  ScrollDispatcher,
  ViewportRuler,
  OverlayContainer,
  OverlayPositionBuilder,
  OverlayKeyboardDispatcher,
} from '@angular/cdk/overlay';
import { Platform } from '@angular/cdk/platform';
import { DateService } from '@app/shared/services/date.service';

class MockElementRef {
  public nativeElement = {};
}

describe('DatePickerComponent', () => {
  let component: DatePickerComponent;
  let fixture: ComponentFixture<DatePickerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DatePickerComponent, IsoDatePipe],
      providers: [
        DatePipe,
        DateService,
        { provide: ElementRef, useClass: MockElementRef },
        Injector,
        Overlay,
        OverlayContainer,
        OverlayKeyboardDispatcher,
        OverlayPositionBuilder,
        Platform,
        Renderer2,
        ScrollStrategyOptions,
        ScrollDispatcher,
        ViewportRuler,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatePickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    spyOn(component, 'onChangeFunction').and.callThrough();
    spyOn(component, 'onTouchedFunction').and.callThrough();
    expect(component).toBeTruthy();
    component.onChangeFunction();
    expect(component.onChangeFunction).toHaveBeenCalled();
    component.onTouchedFunction();
    expect(component.onTouchedFunction).toHaveBeenCalled();
  });

  describe('Reactive Forms', () => {
    it('should register the provided OnChange and OnTouched functions', () => {
      // OnChange
      const onChangeFunctionSpy = jasmine.createSpy('on-change');
      component.registerOnChange(onChangeFunctionSpy);
      component.onChangeFunction(undefined);
      expect(component.onChangeFunction).toEqual(onChangeFunctionSpy);
      expect(onChangeFunctionSpy).toHaveBeenCalled();
      // OnTouched
      const onTouchedFunctionSpy = jasmine.createSpy('on-touched');
      component.registerOnTouched(onTouchedFunctionSpy);
      component.onTouchedFunction();
      expect(component.onTouchedFunction).toEqual(onTouchedFunctionSpy);
      expect(onTouchedFunctionSpy).toHaveBeenCalled();
    });

    it('should set the Disabled state to the provided value', () => {
      const disabledValue = false;
      component.setDisabledState(disabledValue);
      expect(component.disabled).toEqual(disabledValue);
    });
  });
});
