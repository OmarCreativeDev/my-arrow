import { ICalendarDate } from '@app/shared/components/date-picker/date-picker.interfaces';
import { max, min, without } from 'lodash';
import * as moment from 'moment';

const DAYS_IN_WEEK = 7;

export class CalendarMonth {
  public weeks: Array<Array<ICalendarDate>> = [];

  constructor(public start: moment.Moment) {
    // Blank the weeks
    this.weeks = [];
    // Ensure the Rootdate is at the start of the month
    start.startOf('month');
    // Find out how many days are in the month
    const daysLength = start.daysInMonth();
    // Loop through each day, filling at array with an ICalendarDate for each
    for (let dayIndex = 0; dayIndex < daysLength; dayIndex++) {
      // Get the day and the day's index
      const day = moment(start).add(dayIndex, 'days');
      const weekIndex = day.get('day');
      // Get the week
      let week: Array<ICalendarDate>;

      if (weekIndex % DAYS_IN_WEEK === 0) {
        // First Day of the Week
        week = [];
        this.weeks.push(week);
      } else {
        week = this.weeks[this.weeks.length - 1];
        if (!week) {
          week = [];
          this.weeks.push(week);
        }
      }
      week.push({
        value: day.toDate(),
      });
    }

    // First Week
    this.fillFirstWeek(start);
    this.fillLastWeek(start);
  }

  private fillFirstWeek(date: moment.Moment): void {
    // First Week
    const firstWeek = this.weeks[0];
    const firstWeekDaysNeeded = DAYS_IN_WEEK - firstWeek.length;
    /* istanbul ignore else */
    if (firstWeekDaysNeeded) {
      const bufferDates = [];
      const previousMonth = moment(date).subtract(1, 'month');
      const daysInPreviousMonth = previousMonth.daysInMonth();
      for (let i = 0, l = firstWeekDaysNeeded; i < l; i++) {
        const dateIndex = daysInPreviousMonth - firstWeekDaysNeeded + i;
        const bufferDate = moment(previousMonth).add(dateIndex, 'days');
        bufferDates.push({
          value: bufferDate.toDate(),
          isBuffer: true,
        });
      }
      firstWeek.unshift(...bufferDates);
    }
  }

  private fillLastWeek(date: moment.Moment): void {
    // Last Week
    const lastWeek = this.weeks[this.weeks.length - 1];
    const lastWeekDaysNeeded = DAYS_IN_WEEK - lastWeek.length;
    /* istanbul ignore else */
    if (lastWeekDaysNeeded) {
      const bufferDates = [];
      const nextMonth = moment(date).add(1, 'month');
      for (let lastWeekDaysNeededIndex = 0; lastWeekDaysNeededIndex < lastWeekDaysNeeded; lastWeekDaysNeededIndex++) {
        const bufferDate = moment(nextMonth).add(lastWeekDaysNeededIndex, 'days');
        bufferDates.push({
          value: bufferDate.toDate(),
          isBuffer: true,
        });
      }
      lastWeek.push(...bufferDates);
    }
  }
}

export class DatePickerHelper {
  /**
   * Gets the index of a provided date in the selection
   * @param date the date to find an index for
   */
  // TODO: Can be private when we find a way to test private methods
  public getDateSelectionIndex(date: Date, dates: Array<Date>): number {
    // Start with a number out of range
    let selectedIndex = -1;
    // Loop through the dates and find if the provided date aleady exists in the selection
    for (let i = 0, l = dates.length; i < l; i++) {
      const selectedDate = dates[i];
      if (selectedDate && selectedDate.getTime) {
        // Assign the matching index and break the loop
        if (selectedDate.getTime() === date.getTime()) {
          selectedIndex = i;
          break;
        }
      }
    }
    // Return the index for the supplied date
    return selectedIndex;
  }
}

export class CalendarDisplayState {
  public highlightRange: Array<number> = [];
  public activeDate?: number;
  public selection?: Array<number> = [];

  constructor(public dates: Array<Date>, activeIndex: number, private hoverDate: Date) {
    if (dates && dates.length) {
      this.selection = this.getSelectionStamps(dates);
      this.highlightRange = this.getHighlightRange(this.selection, this.hoverDate, activeIndex);
      this.activeDate = this.getActiveDateStamp(dates, activeIndex);
    }
  }

  private getSelectionStamps(selectionDates: Array<Date>): Array<number> {
    return selectionDates.map(date => {
      if (date) {
        return new Date(date).getTime();
      } else {
        return undefined;
      }
    });
  }

  private getHighlightRange(selectionRange: Array<number>, hoverDate: Date, activeIndex: number): Array<number> {
    if (selectionRange.length === 1) {
      return [];
    }
    // Don't show a highlight if the active date is the only selected date
    const selectedDates = without(selectionRange, undefined);
    const activeDate = selectionRange[activeIndex];
    if (selectedDates.length === 1 && selectedDates[0] === activeDate) {
      return [];
    }
    // A highlight needs to be shown
    const highlightRange = [...selectionRange];
    if (this.isDateOrdinate(hoverDate, highlightRange, activeIndex)) {
      // Add to the array of dates
      highlightRange.push(hoverDate.getTime());
    }
    // Get the first date
    const firstStamp = min(highlightRange);
    // Get the last date
    const lastStamp = max(highlightRange);
    return [firstStamp, lastStamp];
  }

  private getActiveDateStamp(dates: Array<Date>, dateIndex: number): number {
    const activeDate = dates[dateIndex];
    if (this.isDate(activeDate)) {
      return activeDate.getTime();
    }
    return;
  }

  /**
   * Checks if the value of a date is ordinate; that an earlier date hasn't been selected for a later date index, and vice versa
   * @param date the date to check
   * @param dateRange the range of dates to check
   * @param activeDateIndex the index of the active date
   */
  private isDateOrdinate(date: Date, dateRangeStamps: Array<number>, activeDateIndex: number): boolean {
    // First date + hover is after the last day OR
    // Last date + hover is before the first day
    return (
      this.isDate(date) &&
      dateRangeStamps !== undefined &&
      dateRangeStamps.length > 0 &&
      ((activeDateIndex === 0 && !this.dateAfterLastDateOfRange(date, dateRangeStamps)) ||
        (activeDateIndex === dateRangeStamps.length - 1 && !this.dateBeforeFirstDateOfRange(date, dateRangeStamps)))
    );
  }

  private dateBeforeFirstDateOfRange(date: Date, dateRangeStamps: Array<number>): boolean {
    const firstDate = dateRangeStamps[0];
    return date.getTime() < firstDate;
  }

  private dateAfterLastDateOfRange(date: Date, dateRangeStamps: Array<number>): boolean {
    const lastDate = dateRangeStamps[dateRangeStamps.length - 1];
    return date.getTime() > lastDate;
  }

  private isDate(date: Date): boolean {
    return date instanceof Date;
  }
}
