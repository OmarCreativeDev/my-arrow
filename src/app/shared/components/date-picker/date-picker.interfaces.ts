export interface IDateInputConfig {
  label?: string;
  placeholder: string;
}

export interface ICalendarDate {
  value: Date;
  isBuffer?: boolean;
}

export enum DatePickerDisplayStatus {
  HIDE = 'HIDE',
  SHOW = 'SHOW',
}
