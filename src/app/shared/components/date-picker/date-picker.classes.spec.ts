import { CalendarDisplayState } from '@app/shared/components/date-picker/date-picker.classes';

describe('CalendarDisplayState', () => {
  const testCases = [
    {
      description:
        'should be no highlight, no selection and no active date if all undefined',
      dates: [],
      activeIndex: 0,
      result: {
        highlightRange: [],
        selection: []
      },
      hoverDate: undefined
    },
    {
      description:
        'should have no highlight when single date is selected and hover exists',
      dates: [new Date('2018-01-01T00:00:00Z')],
      activeIndex: 0,
      result: {
        highlightRange: [],
        selection: [new Date('2018-01-01T00:00:00Z').getTime()],
        activeDate: new Date('2018-01-01T00:00:00Z').getTime()
      },
      hoverDate: new Date('2019-01-01T00:00:00Z')
    },
    {
      description:
        'should have highlight and selection of one and no active date if selection has been made',
      dates: [new Date('2018-01-01T00:00:00Z'), undefined],
      activeIndex: 1,
      result: {
        highlightRange: [
          new Date('2018-01-01T00:00:00Z').getTime(),
          new Date('2018-01-01T00:00:00Z').getTime()
        ],
        selection: [new Date('2018-01-01T00:00:00Z').getTime(), undefined]
      },
      hoverDate: undefined
    },
    {
      description:
        'should have highlight of first selection and hover date if selection of one has been made',
      dates: [new Date('2018-01-01T00:00:00Z'), undefined],
      activeIndex: 1,
      result: {
        highlightRange: [
          new Date('2018-01-01T00:00:00Z').getTime(),
          new Date('2019-01-01T00:00:00Z').getTime()
        ],
        selection: [new Date('2018-01-01T00:00:00Z').getTime(), undefined]
      },
      hoverDate: new Date('2019-01-01T00:00:00Z')
    },
    {
      description:
        'should have same highlight and selection if two selection have been made and no hover date',
      dates: [
        new Date('2018-01-01T00:00:00Z'),
        new Date('2019-01-01T00:00:00Z')
      ],
      activeIndex: 1,
      result: {
        highlightRange: [
          new Date('2018-01-01T00:00:00Z').getTime(),
          new Date('2019-01-01T00:00:00Z').getTime()
        ],
        selection: [
          new Date('2018-01-01T00:00:00Z').getTime(),
          new Date('2019-01-01T00:00:00Z').getTime()
        ]
      },
      hoverDate: undefined
    },
    {
      description:
        'should extend highlight to hover date if ordinally in-sequence and before the first date',
      dates: [
        new Date('2018-01-01T00:00:00Z'),
        new Date('2019-01-01T00:00:00Z')
      ],
      activeIndex: 0,
      result: {
        highlightRange: [
          new Date('2017-01-01T00:00:00Z').getTime(),
          new Date('2019-01-01T00:00:00Z').getTime()
        ],
        selection: [
          new Date('2018-01-01T00:00:00Z').getTime(),
          new Date('2019-01-01T00:00:00Z').getTime()
        ]
      },
      hoverDate: new Date('2017-01-01T00:00:00Z')
    },
    {
      description:
        'should extend highlight to hover date if ordinally in-sequence and after the last date',
      dates: [
        new Date('2018-01-01T00:00:00Z'),
        new Date('2019-01-01T00:00:00Z')
      ],
      activeIndex: 1,
      result: {
        highlightRange: [
          new Date('2018-01-01T00:00:00Z').getTime(),
          new Date('2020-01-01T00:00:00Z').getTime()
        ],
        selection: [
          new Date('2018-01-01T00:00:00Z').getTime(),
          new Date('2019-01-01T00:00:00Z').getTime()
        ]
      },
      hoverDate: new Date('2020-01-01T00:00:00Z')
    },
    {
      description:
        'should have same highlight and selection should match if hover date is before first selected date and active date is ordinally after it',
      dates: [
        new Date('2018-01-01T00:00:00Z'),
        new Date('2019-01-01T00:00:00Z')
      ],
      activeIndex: 1,
      result: {
        highlightRange: [
          new Date('2018-01-01T00:00:00Z').getTime(),
          new Date('2019-01-01T00:00:00Z').getTime()
        ],
        selection: [
          new Date('2018-01-01T00:00:00Z').getTime(),
          new Date('2019-01-01T00:00:00Z').getTime()
        ]
      },
      hoverDate: new Date('2017-01-01T00:00:00Z')
    },
    {
      description:
        'should have same highlight and selection should match if hover date is after last selected date and active date is ordinally before it',
      dates: [
        new Date('2018-01-01T00:00:00Z'),
        new Date('2019-01-01T00:00:00Z')
      ],
      activeIndex: 0,
      result: {
        highlightRange: [
          new Date('2018-01-01T00:00:00Z').getTime(),
          new Date('2019-01-01T00:00:00Z').getTime()
        ],
        selection: [
          new Date('2018-01-01T00:00:00Z').getTime(),
          new Date('2019-01-01T00:00:00Z').getTime()
        ]
      },
      hoverDate: new Date('2020-01-01T00:00:00Z')
    },
    {
      description:
        'should have no highlight if the active date is the only selected date',
      dates: [new Date('2018-01-01T00:00:00Z'), undefined],
      activeIndex: 0,
      result: {
        highlightRange: [],
        selection: [new Date('2018-01-01T00:00:00Z').getTime(), undefined]
      },
      hoverDate: new Date('2020-01-01T00:00:00Z')
    }
  ];
  for (const testCase of testCases) {
    it(`${testCase.description}`, () => {
      const displayState = new CalendarDisplayState(
        testCase.dates,
        testCase.activeIndex,
        testCase.hoverDate
      );
      expect(displayState).toEqual(jasmine.objectContaining(testCase.result));
    });
  }
});
