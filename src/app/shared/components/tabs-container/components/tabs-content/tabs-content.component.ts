import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-tabs-content',
  templateUrl: './tabs-content.component.html',
  styleUrls: ['./tabs-content.component.scss']
})
export class TabsContentComponent {
  public isActive = false;

  @Input()
  value: number | string;
}
