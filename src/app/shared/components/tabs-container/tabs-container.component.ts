import { Component, Input, Output, EventEmitter, ContentChildren, QueryList } from '@angular/core';
import { TabsContentComponent } from '@app/shared/components/tabs-container/components/tabs-content/tabs-content.component';

@Component({
  selector: 'app-tabs-container',
  templateUrl: './tabs-container.component.html',
  styleUrls: ['./tabs-container.component.scss'],
})
export class TabsContainerComponent {
  @Input()
  tabs: object[];

  @Output()
  selectedTab: EventEmitter<any> = new EventEmitter();

  @ContentChildren(TabsContentComponent, { descendants: true, read: TabsContentComponent })
  tabContent: QueryList<TabsContentComponent>;

  onTabChange($event) {
    this.tabContent.toArray().forEach((tab, index) => {
      tab.isActive = false;
      if ($event.value === tab.value) {
        this.selectedTab.emit(tab);
        tab.isActive = true;
      }
    });
  }
}
