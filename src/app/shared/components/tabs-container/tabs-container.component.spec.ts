import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsContainerComponent } from './tabs-container.component';
import { CUSTOM_ELEMENTS_SCHEMA, Component } from '@angular/core';
import { TabsContentComponent } from '@app/shared/components/tabs-container/components/tabs-content/tabs-content.component';

@Component({
  selector: 'app-tabs-container-mock-component',
  template: `
    <app-tabs-container>
      <app-tabs-content [value]="0"></app-tabs-content>
      <app-tabs-content [value]="1"></app-tabs-content>
      <app-tabs-content [value]="2"></app-tabs-content>
    </app-tabs-container>
  `,
})
export class TestHostTabsContainerComponent {}

describe('TabsContainerComponent', () => {
  let component: TabsContainerComponent;
  let tabsContainerComponent: TabsContainerComponent;
  let fixture: ComponentFixture<TabsContainerComponent>;
  let fixture2: ComponentFixture<TestHostTabsContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabsContainerComponent, TabsContentComponent, TestHostTabsContainerComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsContainerComponent);
    fixture2 = TestBed.createComponent(TestHostTabsContainerComponent);
    component = fixture.componentInstance;
    tabsContainerComponent = fixture2.debugElement.children[0].componentInstance;
    fixture.detectChanges();
    fixture2.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show the correct content when changing tabs', () => {
    const tabContentMock = tabsContainerComponent.tabContent;
    component.tabContent = tabContentMock;

    tabContentMock.toArray().forEach((tab, index) => {
      if (1 === tab.value) {
        tab.isActive = true;
      }
    });

    component.onTabChange({ value: 1 });
    expect(component.tabContent).toEqual(tabContentMock);
  });
});
