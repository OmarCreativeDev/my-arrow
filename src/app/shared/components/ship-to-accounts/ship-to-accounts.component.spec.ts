import { async, ComponentFixture, fakeAsync, TestBed, inject } from '@angular/core/testing';

import { ShipToAccountsComponent } from './ship-to-accounts.component';
import { CoreModule } from '@app/core/core.module';
import { userReducers, INITIAL_USER_STATE } from '@app/core/user/store/user.reducers';
import { ActionsSubject, Store, StoreModule } from '@ngrx/store';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { of } from 'rxjs';
import { IShipTo } from '@app/core/checkout/checkout.interfaces';
import { UpdateShipTo, RequestUserComplete, RequestAvailableShipTo } from '@app/core/user/store/user.actions';
import { Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { FormattedAddressPipe } from '@app/shared/pipes/formatted-address/formatted-address.pipe';
import { cartReducers, INITIAL_CART_STATE } from '@app/features/cart/stores/cart/cart.reducers';
import { mockUser } from '@app/core/user/user.service.spec';
import { ApiService } from '@app/core/api/api.service';
import { CheckoutService } from '@app/core/checkout/checkout.service';
import { HttpErrorResponse } from '@angular/common/http';

describe('ShipToAccountsComponent', () => {
  let component: ShipToAccountsComponent;
  let fixture: ComponentFixture<ShipToAccountsComponent>;
  let store: Store<any>;
  let actionsSubj: ActionsSubject;
  let apiService: ApiService;

  const shipToAccounts: IShipTo[] = [
    {
      id: 123,
      address1: 'MILPITAS SITE 335',
      address2: '5802 BOB BULLOCK C1',
      address3: 'PMB 014-507',
      address4: '',
      city: 'LAREDO',
      county: '',
      state: 'TX',
      country: 'United States',
      name: 'FLEXTRONICS INTERNATIONAL USA',
      postCode: '78041',
      primaryFlag: true,
      selected: true,
    },
    {
      id: 24667766,
      address1: 'SITE 998',
      address2: '5902 ROB BULLOCK C1',
      address3: 'PMB 014-537',
      address4: '',
      city: 'DENVER',
      county: '',
      state: 'COL',
      country: 'United States',
      name: 'FLEXTRONICS INTERNATIONAL INC',
      postCode: '78061',
      primaryFlag: true,
      selected: false,
    },
    {
      id: 24667799,
      address1: 'SITE 999',
      address2: '123 SSH',
      address3: 'SSH 987',
      address4: '',
      city: 'SJ',
      county: '',
      state: 'SJ',
      country: 'Costa Rica',
      name: 'FLEXTRONICS INTERNATIONAL CR',
      postCode: '78061',
      primaryFlag: true,
      selected: false,
    },
  ];

  const initialState = {
    user: {
      ...INITIAL_USER_STATE,
      profile: mockUser,
    },
    cart: INITIAL_CART_STATE,
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        StoreModule.forRoot(
          {
            user: userReducers,
            cart: cartReducers,
          },
          { initialState: initialState }
        ),
      ],
      declarations: [ShipToAccountsComponent, FormattedAddressPipe],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [{ provide: Dialog, useClass: DialogMock }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShipToAccountsComponent);
    component = fixture.componentInstance;
    apiService = TestBed.get(ApiService);
    actionsSubj = TestBed.get(ActionsSubject);
    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();

    fixture.detectChanges();
    store.dispatch(new RequestUserComplete(mockUser));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get a list of ship to accounts', () => {
    inject([CheckoutService], (service: CheckoutService) => {
      spyOn(apiService, 'get').and.returnValue(of(shipToAccounts));
      store.dispatch(new RequestAvailableShipTo());

      component.ngOnInit();
      expect(component.shipToAccounts.items).toEqual(shipToAccounts);
      expect(component.selectedShipToAccountId).toEqual(mockUser.selectedShipTo);
    });
  });

  it('should capture shipToAccount error from service', fakeAsync(() => {
    inject([CheckoutService], (service: CheckoutService) => {
      spyOn(apiService, 'get').and.returnValue(of(HttpErrorResponse));
      store.dispatch(new RequestAvailableShipTo());
      component.ngOnInit();
      expect(component.shipToAccounts.error).toBeTruthy();
    });
  }));

  it('should dismiss the dialog if the selected ship to account is the same as the original ship to', () => {
    component.originalShipToAccountId = 222;
    component.selectedShipToAccountId = 222;
    spyOn(component, 'dismiss').and.callThrough();
    component.confirm();
    expect(component.submitting).toBeFalsy();
    expect(component.dismiss).toHaveBeenCalled();
  });

  it('should dispatch the new selected value to the store', () => {
    component.originalShipToAccountId = 333;
    component.selectedShipToAccountId = 444;
    component.confirm();
    expect(component.submitting).toBeTruthy();
    expect(store.dispatch).toHaveBeenCalledWith(new UpdateShipTo(component.selectedShipToAccountId));
  });

  it('should update the store and dismiss the dialog', () => {
    component.originalShipToAccountId = 555;
    component.selectedShipToAccountId = 666;
    spyOn(actionsSubj, 'pipe').and.returnValue(of([]));
    component.confirm();
    expect(component.submitting).toBeFalsy();
  });
});
