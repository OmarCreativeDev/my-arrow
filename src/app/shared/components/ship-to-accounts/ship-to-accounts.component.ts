import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

import { IListingState, IAppState } from '@app/shared/shared.interfaces';
import { IShipTo } from '@app/core/checkout/checkout.interfaces';
import { getUserShipToAccount, getUserBillToAccount, getAvailableShipTo } from '@app/core/user/store/user.selectors';
import { ActionsSubject, Store, select } from '@ngrx/store';
import { UpdateShipTo, RequestAvailableShipTo } from '@app/core/user/store/user.actions';
import { Dialog } from '@app/core/dialog/dialog.service';
import { find } from 'lodash-es';

@Component({
  selector: 'app-ship-to-accounts',
  templateUrl: './ship-to-accounts.component.html',
  styleUrls: ['./ship-to-accounts.component.scss'],
})
export class ShipToAccountsComponent implements OnInit, OnDestroy {
  public shipToAccounts$: Observable<IListingState<IShipTo>>;
  readonly tariffCountry: string = 'United States';
  public shipToAccounts: IListingState<IShipTo> = {
    loading: true,
    error: null,
    items: [],
  };

  public originalShipToAccountId: number;
  public selectedShipToAccountId: number;
  public submitting: boolean = false;
  public shipToId$: Observable<number>;
  public billTo$: Observable<number>;
  private subscription: Subscription = new Subscription();

  constructor(private store: Store<IAppState>, private actionsSubj: ActionsSubject, private dialog: Dialog<ShipToAccountsComponent>) {
    this.billTo$ = this.store.pipe(select(getUserBillToAccount));
    this.shipToId$ = this.store.pipe(select(getUserShipToAccount));
    this.shipToAccounts$ = store.pipe(select(getAvailableShipTo));

    this.store.dispatch(new RequestAvailableShipTo());
  }

  ngOnInit() {
    this.startSubscriptions();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeShipToAccountId());
    this.subscription.add(this.subscribeShipToAddresses());
  }

  private subscribeShipToAddresses(): Subscription {
    return this.shipToAccounts$.subscribe((data: IListingState<IShipTo>) => {
      this.shipToAccounts = data;
    });
  }

  private subscribeShipToAccountId(): Subscription {
    return this.shipToId$.subscribe(shipToAccount => {
      this.originalShipToAccountId = this.selectedShipToAccountId = shipToAccount;
    });
  }

  public confirm(): void {
    if (this.selectedShipToAccountId === this.originalShipToAccountId) {
      this.dismiss();
      return;
    }

    this.submitting = true;
    this.store.dispatch(new UpdateShipTo(this.selectedShipToAccountId));
    this.actionsSubj.pipe(filter(action => action.type === 'USER_UPDATE_SHIP_TO_COMPLETE')).subscribe(() => {
      this.submitting = false;
      const selectedAddress = find(this.shipToAccounts.items, { id: { shipToID: this.selectedShipToAccountId } });
      this.dismiss(selectedAddress);
    });
  }

  public dismiss(data = null) {
    this.dialog.close(data);
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private showTariffWarningsBasedOnRules(includeTariff: boolean = false): boolean {
    const originalAddress = find(this.shipToAccounts.items, { id: { shipToID: this.originalShipToAccountId } });
    const selectedAddress = find(this.shipToAccounts.items, { id: { shipToID: this.selectedShipToAccountId } });
    if (this.originalShipToAccountId !== this.selectedShipToAccountId && originalAddress && selectedAddress) {
      return includeTariff
        ? originalAddress.country !== this.tariffCountry && selectedAddress.country === this.tariffCountry
        : originalAddress.country === this.tariffCountry && selectedAddress.country !== this.tariffCountry;
    }
    return false;
  }

  get showTariffInclusionWarning(): boolean {
    return this.showTariffWarningsBasedOnRules(true);
  }

  get showTariffRemovalWarning(): boolean {
    return this.showTariffWarningsBasedOnRules(false);
  }
}
