import { IEndCustomerRecord } from '@app/core/cart/cart.interfaces';

export interface ILineItemCpnInfo {
  endCustomerRecords?: Array<IEndCustomerRecord>;
  enteredCpn?: string;
  selectedCustomerPartNumber?: string;
  selectedEndCustomer?: number;
}
