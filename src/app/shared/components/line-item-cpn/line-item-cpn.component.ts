import { Component, Input, OnDestroy, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';
import { find, get } from 'lodash-es';
import { ILineItemCpnInfo } from '@app/shared/components/line-item-cpn/line-item-cpn.interfaces';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-line-item-cpn',
  templateUrl: './line-item-cpn.component.html',
  styleUrls: ['./line-item-cpn.component.scss'],
})
export class LineItemCpnComponent implements OnInit, OnDestroy {
  @Input()
  public data: ILineItemCpnInfo;
  @Input()
  public readOnly: boolean = false;
  @Input()
  public disabled: boolean = false;
  @Output()
  public customCpnChange: EventEmitter<string> = new EventEmitter<string>(true);
  public customCpnControl = new FormControl();
  public subscription: Subscription = new Subscription();

  ngOnInit(): void {
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeCustomCpnCtrlSub());
  }

  private subscribeCustomCpnCtrlSub(): Subscription {
    return this.customCpnControl.valueChanges.pipe(debounceTime(1000)).subscribe(newValue => this.customCpnChange.emit(newValue));
  }

  get endCustomerName(): string {
    let name: string = null;
    if (this.data.selectedCustomerPartNumber && this.data.selectedEndCustomer && this.data.endCustomerRecords) {
      const cpn = find(this.data.endCustomerRecords, record => record.cpn === this.data.selectedCustomerPartNumber);
      if (get(cpn, 'endCustomers', null)) {
        const ec = find(cpn.endCustomers, endCustomer => endCustomer.endCustomerSiteId === this.data.selectedEndCustomer);
        name = get(ec, 'name', null);
      }
    }
    return name;
  }
}
