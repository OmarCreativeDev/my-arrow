import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { LineItemCpnComponent } from './line-item-cpn.component';

describe('LineItemCpnComponent', () => {
  let component: LineItemCpnComponent;
  let fixture: ComponentFixture<LineItemCpnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LineItemCpnComponent],
      imports: [ReactiveFormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineItemCpnComponent);
    component = fixture.componentInstance;
    component.data = {
      endCustomerRecords: [
        { cpn: 'SVR-115-0000-004', endCustomers: [{ name: 'Siemens Industry Inc', endCustomerSiteId: 493535 }] },
        { cpn: 'SVR-115-0000-005', endCustomers: [{ name: 'Apple', endCustomerSiteId: 87973 }] },
      ],
      selectedCustomerPartNumber: 'SVR-115-0000-004',
      selectedEndCustomer: 493535,
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('endCustomerName should return null when selectedEndCustomer is not present', () => {
    component.data = { selectedCustomerPartNumber: 'MYAR-1987' };
    const result = component.endCustomerName;
    expect(result).toEqual(null);
  });

  it('endCustomerName should return a value different to null', () => {
    const result = component.endCustomerName;
    expect(result).toEqual('Siemens Industry Inc');
  });

  it('should detects values changes from customCpnControl', () => {
    spyOn(component, 'ngOnInit').and.callThrough();
    component.customCpnControl.setValue('12134');
  });
});
