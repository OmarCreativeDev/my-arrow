import { IEndCustomerRecord, IShoppingCartReel } from '@app/core/cart/cart.interfaces';

export interface IReelItem {
  availableQuantity: number;
  billToId: number;
  businessCost?: number;
  cpn?: string;
  currency: string;
  cartLineId?: string;
  endCustomerId?: number;
  endCustomerRecords?: Array<IEndCustomerRecord>;
  itemId: number;
  partNumber: string;
  reel?: IShoppingCartReel;
  warehouseId: number;
}

export interface IReelDialog {
  readOnlyCpn?: boolean;
  reelItem: IReelItem;
}

export interface IReelCallbackResponse {
  cartLineId?: string;
  itemId: number;
  pricePerItem: number;
  reel: IShoppingCartReel;
  tariffValue?: number;
  selectedCustomerPartNumber?: string;
  selectedEndCustomerSiteId?: number;
}
