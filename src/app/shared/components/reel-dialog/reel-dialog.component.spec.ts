import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { userReducers } from '@app/core/user/store/user.reducers';
import { FormattedPricePipe } from '@app/shared/pipes/formatted-price/formatted-price.pipe';
import { Store, combineReducers, StoreModule } from '@ngrx/store';

import { ReelDialogComponent } from './reel-dialog.component';
import { reelReducers } from '@app/core/reel/stores/reel.reducers';
import { Observable, of } from 'rxjs';
import { ClearReelState } from '@app/core/reel/stores/reel.actions';

export class StoreMock {
  public dispatch(): void {}
  public select(): Observable<any> {
    return of([]);
  }
  public pipe() {
    return of({});
  }
}

const dialogData = {
  reelItem: {
    availableQuantity: 12345,
    billToId: 233323,
    currency: 'USD',
    itemId: 123456,
    partNumber: 'BAV99',
    reel: {
      itemsPerReel: 1223,
      numberOfReels: 2,
    },
    warehouseId: 2772,
  },
};

describe('ReelDialogComponent', () => {
  let component: ReelDialogComponent;
  let dialog: Dialog<ReelDialogComponent>;
  let fixture: ComponentFixture<ReelDialogComponent>;
  let store: Store<any>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ReelDialogComponent, FormattedPricePipe],
      imports: [
        StoreModule.forRoot({
          reel: combineReducers(userReducers, reelReducers),
        }),
      ],
      providers: [
        { provide: Dialog, useClass: DialogMock },
        { provide: APP_DIALOG_DATA, useValue: { data: dialogData } },
        { provide: Store, useClass: StoreMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReelDialogComponent);
    component = fixture.componentInstance;
    component.reelItem = dialogData.reelItem;
    fixture.detectChanges();

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    dialog = TestBed.get(Dialog);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set `reelPrice` onReelCalculated', () => {
    component.onReelCalculated({
      noOfReels: 20,
      partsInReel: 100,
      totalParts: 2000,
      price: {
        unit: 2,
        total: 4000,
      },
    });

    expect(component.reelPrice).toEqual({
      noOfReels: 20,
      partsInReel: 100,
      totalParts: 2000,
      price: {
        unit: 2,
        total: 4000,
      },
    });
  });

  describe('#onClose()', () => {
    it('should close the dialog without passing data if `persist` is undefined', () => {
      spyOn(dialog, 'close');
      component.onClose();
      expect(dialog.close).toHaveBeenCalled();
    });

    it('should dispatch an action to clear the reel state', () => {
      component.onClose();
      expect(store.dispatch).toHaveBeenCalledWith(new ClearReelState());
    });

    it('should close the dialog passing data if `persist` is true and `reelPrice` is defined', () => {
      spyOn(dialog, 'close');
      component.reelPrice = {
        noOfReels: 20,
        partsInReel: 100,
        totalParts: 2000,
        price: {
          unit: 2,
          total: 4000,
        },
        tariffValue: 200.2,
      };

      component.onClose(true);
      expect(dialog.close).toHaveBeenCalledWith({
        itemId: 123456,
        pricePerItem: 2,
        reel: {
          itemsPerReel: 100,
          numberOfReels: 20,
        },
        tariffValue: 200.2,
      });
    });

    it('should close the dialog passing data with endCustomerSiteId, customerPartNumber and cartLineId if `persist` is true and `reelPrice` is defined', () => {
      spyOn(dialog, 'close');
      component.reelItem = {
        ...dialogData.reelItem,
        cartLineId: 'uniqueid',
      };
      component.reelPrice = {
        noOfReels: 20,
        partsInReel: 100,
        totalParts: 2000,
        endCustomerSiteId: 4567,
        customerPartNumber: 'CPN1',
        price: {
          unit: 2,
          total: 4000,
        },
        tariffValue: 200.2,
      };

      component.onClose(true);
      expect(dialog.close).toHaveBeenCalledWith({
        itemId: 123456,
        pricePerItem: 2,
        selectedEndCustomerSiteId: 4567,
        selectedCustomerPartNumber: 'CPN1',
        cartLineId: 'uniqueid',
        reel: {
          itemsPerReel: 100,
          numberOfReels: 20,
        },
        tariffValue: 200.2,
      });
    });
  });
});
