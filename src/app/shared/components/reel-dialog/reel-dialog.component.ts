import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { IReelPrice } from '@app/core/reel/reel.interfaces';
import { ClearReelState } from '@app/core/reel/stores/reel.actions';
import { getCurrencyCode, getUserBillToAccount } from '@app/core/user/store/user.selectors';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import { get } from 'lodash-es';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

import { IReelCallbackResponse, IReelDialog, IReelItem } from './reel-dialog.interfaces';

@Component({
  selector: 'app-reel-dialog',
  templateUrl: './reel-dialog.component.html',
})
export class ReelDialogComponent implements OnInit, OnDestroy {
  public reelItem: IReelItem;

  public billToId$: Observable<number>;
  public billToId: number;
  public currencyCode$: Observable<string>;
  public currencyCode: string;
  public reelPrice: IReelPrice;

  public subscription: Subscription = new Subscription();

  constructor(
    public dialog: Dialog<ReelDialogComponent>,
    @Inject(APP_DIALOG_DATA) public data: IReelDialog,
    private store: Store<IAppState>
  ) {
    this.reelItem = data.reelItem;
    this.billToId$ = store.pipe(select(getUserBillToAccount));
    this.currencyCode$ = store.pipe(select(getCurrencyCode));
  }

  ngOnInit(): void {
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeBillToId());
    this.subscription.add(this.subscribeCurrencyCode());
  }

  private subscribeBillToId(): Subscription {
    return this.billToId$.pipe(take(1)).subscribe(billToId => {
      this.billToId = billToId;
    });
  }

  private subscribeCurrencyCode(): Subscription {
    return this.currencyCode$.pipe(take(1)).subscribe(currencyCode => {
      this.currencyCode = currencyCode;
    });
  }

  public get isUpdateReel(): boolean {
    return get(this.reelItem, 'reel.itemsPerReel', 0) > 0 && get(this.reelItem, 'reel.numberOfReels', 0) > 0;
  }

  public onReelCalculated(reelPrice: IReelPrice) {
    this.reelPrice = reelPrice;
  }

  public onClose(persist?: boolean): void {
    if (persist && this.reelPrice) {
      const { customerPartNumber, endCustomerSiteId, noOfReels, partsInReel, price, tariffValue } = this.reelPrice;
      const reelData: IReelCallbackResponse = {
        itemId: this.reelItem.itemId,
        pricePerItem: price.unit,
        tariffValue: tariffValue,
        reel: {
          ...this.reelItem.reel,
          itemsPerReel: partsInReel,
          numberOfReels: noOfReels,
        },
        ...(customerPartNumber && { selectedCustomerPartNumber: customerPartNumber }),
        ...(endCustomerSiteId && { selectedEndCustomerSiteId: endCustomerSiteId }),
        ...(this.reelItem.cartLineId && { cartLineId: this.reelItem.cartLineId }),
      };
      this.dialog.close(reelData);
    } else {
      this.dialog.close();
    }
    this.store.dispatch(new ClearReelState());
  }
}
