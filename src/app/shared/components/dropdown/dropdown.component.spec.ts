import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownComponent } from './dropdown.component';
import { ElementRef, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ToggleComponent } from '@app/shared/classes/toggle-component.class';
import { ISelectableOption } from '@app/shared/shared.interfaces';

class MockElementRef {
  public nativeElement = {};
}

class MockToggleComponent {
  constructor() {}
}

describe('DropdownComponent', () => {
  let component: DropdownComponent;
  let fixture: ComponentFixture<DropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DropdownComponent],
      providers: [{ provide: ElementRef, useClass: MockElementRef }, { provide: ToggleComponent, useClass: MockToggleComponent }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownComponent);
    component = fixture.componentInstance;
    component.options = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit valueChange and select when an option is selected', () => {
    spyOn(component.valueChange, 'emit');
    spyOn(component.select, 'emit');
    const option = {
      label: 'label',
      value: 1,
    };
    component.isOpen = true;
    component.selectOption(new MouseEvent(undefined), option);
    expect(component.isOpen).toBeFalsy();
    expect(component.valueChange.emit).toHaveBeenCalledWith(option.value);
    expect(component.select.emit).toHaveBeenCalledWith(option);
  });

  describe('#getSelectedOptionLabel', () => {
    it('should find the option for the current value', () => {
      const dummyOption: ISelectableOption<any> = {
        label: 'foo',
        value: 'bar',
      };
      component.options = [dummyOption];
      component.value = dummyOption.value;
      const result = component.selectedOptionLabel;
      expect(result).toEqual(dummyOption.label);
    });

    it('should return undefined if no option for the current value is found', () => {
      const dummyOption: ISelectableOption<any> = {
        label: 'foo',
        value: 'bar',
      };
      component.options = [dummyOption];
      component.value = 'dummy-value';
      const result = component.selectedOptionLabel;
      expect(result).toBeUndefined();
    });

    it('should truncate the label it is too longer than 5 chars', () => {
      const dummyOption: ISelectableOption<any> = {
        label: 'foobar',
        value: 'bar',
      };
      component.options = [dummyOption];
      component.truncate = 5;
      component.value = dummyOption.value;
      const result = component.selectedOptionLabel;
      expect(result).toEqual('fooba…');
    });
  });
});
