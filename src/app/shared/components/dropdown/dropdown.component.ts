import {
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ToggleComponent } from '@app/shared/classes/toggle-component.class';

import { ISelectableOption } from '../../shared.interfaces';

@Component({
  selector: 'app-dropdown',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DropdownComponent),
      multi: true,
    },
  ],
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DropdownComponent extends ToggleComponent implements ControlValueAccessor, OnInit {
  private _value: any;
  @Input()
  options: Array<ISelectableOption<any>>;
  @Input()
  size: string = '';
  @Input()
  error: boolean;
  @Input()
  elemClass: string;
  @Input()
  truncate: number = 0;
  @Input()
  placeholder: string = '';
  @Input()
  disabled: boolean;
  @Input()
  closedIcon: string = 'down-arrow';
  @Input()
  openedIcon: string = 'up-arrow';
  @Input()
  searchable: boolean = false;
  // Retained to enable two-way binding, if necessary
  @Output()
  valueChange = new EventEmitter<any>();
  @Output()
  select = new EventEmitter<ISelectableOption<any>>();

  get value(): any {
    return this._value;
  }
  @Input()
  set value(val: any) {
    this._value = val;
    this.onChange(val);
    this.onTouched();
  }

  @ContentChild('optionLayout')
  optionLayoutTmpl: TemplateRef<any>;

  public filteredOptions: Array<ISelectableOption<any>>;

  public writeValue(value: any): void {
    this._value = value;
  }

  /* tslint:disable:no-unused-variable */
  constructor(private elementRef: ElementRef) {
    super(elementRef);
  }

  ngOnInit() {
    this.filteredOptions = this.options;
  }

  public open() {
    if (!this.disabled) {
      super.show();
      this.elementRef.nativeElement.querySelector('input').value = '';
      this.filteredOptions = this.options;
    }
  }

  public filterOptions(val) {
    if (this.searchable)
      if (val) {
        this.filteredOptions = this.options.filter(option => option.label.toLowerCase().startsWith(val.toLowerCase()));
      } else {
        this.filteredOptions = this.options;
      }
  }

  /**
   * Triggered any time an option is clicked
   * @param option The selected option
   */
  public selectOption($event: MouseEvent, option: ISelectableOption<any>) {
    $event.stopPropagation();
    this.toggle();
    this.select.emit(option);
    this.valueChange.emit(option.value);
    this.onChange(option.value);
  }

  /**
   * Gets the label of the selected option, based on value
   */
  public get selectedOptionLabel(): string {
    const selectedOption = this.options.find(option => option.value === this._value);
    let label: string;
    if (selectedOption) {
      label = selectedOption.label;
      if (this.truncate > 0 && label && label.length > this.truncate) {
        label = label.substring(0, this.truncate) + '…';
      }
    }
    return label;
  }

  public onChange(value: any) {}

  public onTouched() {}

  public registerOnChange(fn: (value: any) => void): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  public isDirty(): boolean {
    return this.value !== undefined;
  }
}
