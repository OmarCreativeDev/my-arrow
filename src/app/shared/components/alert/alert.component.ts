import { Component, Input } from '@angular/core';
import { AlertComponentEnum } from '@app/shared/shared.interfaces';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss'],
})
export class AlertComponent {
  @Input()
  type: AlertComponentEnum = AlertComponentEnum.Information;
  @Input()
  icon: string;
}
