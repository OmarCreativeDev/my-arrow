import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

import { Store, select } from '@ngrx/store';
import * as moment from 'moment';

import { Observable, Subscription } from 'rxjs/';
import { skip, take } from 'rxjs/operators';

import { Dialog, APP_DIALOG_DATA } from '@app/core/dialog/dialog.service';
import { IAppState, IListingState } from '@app/shared/shared.interfaces';
import { GetBomListing, AddPartsToBom, CreateBom } from '@app/core/boms/store/boms.actions';
import { getBomListing, getLastModifiedBom, isBomBusy, getBomError, getLastCreatedBom } from '@app/core/boms/store/boms.selectors';
import { IBom, IModifiedBom, IAddToBomPart } from '@app/core/boms/boms.interface';
import { InventoryService } from '@app/core/inventory/inventory.service';
import { IProductValidateResponseItem } from '@app/core/inventory/product-details.interface';

@Component({
  selector: 'app-add-to-bom-dialog',
  templateUrl: './add-to-bom-dialog.component.html',
  styleUrls: ['./add-to-bom-dialog.component.scss'],
})
export class AddToBomDialogComponent implements OnInit, OnDestroy {
  public subscription: Subscription = new Subscription();

  public createBomForm: FormGroup;
  public newNameControl: FormControl;
  public addToExistingBomForm: FormGroup;
  public existingBomsControl: FormControl;

  public existingBoms$: Observable<IListingState<IBom>>;
  public lastModified$: Observable<IModifiedBom>;
  public lastCreated$: Observable<IBom>;
  public hasError$: Observable<HttpErrorResponse>;
  public isBusy$: Observable<boolean>;

  public notification: IModifiedBom;
  public createdBom: IBom;
  public existingBom: IBom;
  public displayNotification = false;

  public invalidParts: Array<IProductValidateResponseItem>;
  public validationError = false;
  public parts: Array<IAddToBomPart>;
  public validating = true;

  constructor(
    private store: Store<IAppState>,
    private dialog: Dialog<AddToBomDialogComponent>,
    private router: Router,
    private inventoryService: InventoryService,
    @Inject(APP_DIALOG_DATA) public data: Array<IAddToBomPart>
  ) {
    /**
     * Store slices
     */
    this.existingBoms$ = this.store.pipe(select(getBomListing));
    this.lastModified$ = this.store.pipe(select(getLastModifiedBom));
    this.lastCreated$ = this.store.pipe(select(getLastCreatedBom));
    this.isBusy$ = this.store.pipe(select(isBomBusy));
    this.hasError$ = this.store.pipe(select(getBomError));
  }

  ngOnInit() {
    /**
     * Validate MOQ
     */
    this.validate();

    /**
     * Dispatch the bom listing
     */
    this.store.dispatch(new GetBomListing());

    /**
     * Set new Bom controls
     */
    this.newNameControl = new FormControl('', Validators.required);
    this.createBomForm = new FormGroup({
      newBomName: this.newNameControl,
    });

    /**
     * Set new exisitng BOM controls
     */
    this.existingBomsControl = new FormControl('', Validators.required);
    this.addToExistingBomForm = new FormGroup({
      existingBoms: this.existingBomsControl,
    });

    this.startSubscriptions();
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeExistingBom());
    this.subscription.add(this.subscribeLastModified());
    this.subscription.add(this.subscribeLastCreated());
  }

  private subscribeExistingBom(): Subscription {
    return this.existingBomsControl.valueChanges.subscribe(() => {
      this.existingBom = undefined;
    });
  }

  private subscribeLastModified(): Subscription {
    return this.lastModified$.pipe(skip(1)).subscribe(lastModified => {
      if (lastModified.isNew) {
        this.newNameControl.disable();
        this.createdBom = lastModified;
      } else {
        this.existingBom = lastModified;
      }
      this.displayNotification = true;
    });
  }

  private subscribeLastCreated(): Subscription {
    return this.lastCreated$
      .pipe(
        skip(1),
        take(1)
      )
      .subscribe(bom => {
        this.store.dispatch(new AddPartsToBom({ id: bom.id, name: bom.name, parts: this.parts, isNew: true }));
      });
  }

  public createBom() {
    const name = this.newNameControl.value;
    this.store.dispatch(new CreateBom(name));
  }

  public addPartsToBom() {
    /* istanbul ignore else */
    if (this.addToExistingBomEnabled) {
      const bom = this.existingBomsControl.value;
      this.store.dispatch(
        new AddPartsToBom({
          id: bom.id,
          name: bom.name,
          parts: this.parts,
          isNew: false,
        })
      );
    }
  }

  public resetExisitingBomForm() {
    this.addToExistingBomForm.reset();
  }

  public resetCreateBomForm() {
    if (!this.createdBom) {
      this.createBomForm.reset();
    }
  }

  public goToNewBom() {
    this.goToBom(this.createdBom.id);
  }

  public goToExistingBom() {
    this.goToBom(this.existingBom.id);
  }

  public dismiss() {
    this.dialog.close();
  }

  private goToBom(id) {
    this.router.navigate(['/bom/view', id, 1]);
    this.dismiss();
  }

  get addToExistingBomEnabled() {
    return this.existingBomsControl.valid;
  }

  get addToNewBomEnabled() {
    return this.newNameControl.valid;
  }

  private validate() {
    const validationItems = this.data.map(part => {
      return {
        ...part,
        requestDate: moment(new Date()).format('YYYY-MM-DD'),
      };
    });
    this.inventoryService.validateItems(validationItems).subscribe(
      result => {
        if (result.invalidItems.length) {
          this.invalidParts = result.invalidItems;
        } else {
          this.parts = this.data;
        }
        this.validating = false;
      },
      () => {
        this.validationError = true;
        this.validating = false;
      }
    );
  }

  public acceptInvalidParts() {
    this.parts = this.data.map(part => {
      const invalidPart = this.invalidParts.find(invalidItem => {
        return invalidItem.docId === part.docId;
      });
      if (invalidPart) {
        return {
          ...part,
          quantity: invalidPart.proposedQty,
        };
      } else {
        return part;
      }
    });
    this.invalidParts = undefined;
  }
}
