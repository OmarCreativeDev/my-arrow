import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { Location } from '@angular/common';
import { AddToBomDialogComponent } from './add-to-bom-dialog.component';

import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormControl } from '@angular/forms';
import { CoreModule } from '@app/core/core.module';
import { Store, StoreModule } from '@ngrx/store';
import { bomReducers } from '@app/core/boms/store/boms.reducers';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { RouterTestingModule } from '@angular/router/testing';
import { CreateBom } from '@app/core/boms/store/boms.actions';
import { mockBoms } from '@app/core/boms/boms.service.spec';

class DummyComponent {}
const routes = [{ path: 'bom/view/:bomId/1', component: DummyComponent }];

describe('AddToBomDialogComponent', () => {
  let component: AddToBomDialogComponent;
  let fixture: ComponentFixture<AddToBomDialogComponent>;
  let store: Store<any>;
  let nameField: FormControl;
  let location: Location;
  let dialog: Dialog<AddToBomDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          boms: bomReducers,
        }),
        RouterTestingModule.withRoutes(routes),
        CoreModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      providers: [
        { provide: APP_DIALOG_DATA, useValue: [{ mpn: 'aaa', quantity: 100 }, { mpn: 'bbb', quantity: 200 }] },
        { provide: Dialog, useClass: DialogMock },
      ],
      declarations: [AddToBomDialogComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToBomDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    dialog = TestBed.get(Dialog);
    location = TestBed.get(Location);
    nameField = <FormControl>component.createBomForm.controls.newBomName;
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    spyOn(dialog, 'close');
    component.dismiss();
    expect(dialog.close).toHaveBeenCalled();
  });

  it('should dipatch CreateBom action on createBom method', () => {
    nameField.setValue('New Bom Name');
    expect(component.addToNewBomEnabled).toBeTruthy();
    component.createBom();
    expect(store.dispatch).toHaveBeenCalledWith(new CreateBom('New Bom Name'));
  });

  it('should navigate to existing BOM', fakeAsync(() => {
    component.existingBom = mockBoms[0];
    component.goToExistingBom();
    tick(10);
    expect(location.path()).toEqual('/bom/view/1236236/1');
  }));

  it('should navigate to new BOM', fakeAsync(() => {
    component.createdBom = mockBoms[0];
    component.goToNewBom();
    tick(10);
    expect(location.path()).toEqual('/bom/view/1236236/1');
  }));
});
