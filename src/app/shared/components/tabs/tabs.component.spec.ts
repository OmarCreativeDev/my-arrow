import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabsComponent } from './tabs.component';
import { ISelectableOption } from '@app/shared/shared.interfaces';

describe('TabsComponent', () => {
  let component: TabsComponent;
  let fixture: ComponentFixture<TabsComponent>;
  const tabs: ISelectableOption<any>[] = [
    {
      label: 'Recent',
      value: 0,
    },
    {
      label: 'Open',
      value: 1,
    },
    {
      label: 'Shipped',
      value: 2,
    },
  ];

  const mockEvent = new Event('click');

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabsComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabsComponent);
    component = fixture.componentInstance;
    component.tabs = tabs;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should update the selected tab when clicking on a tab', () => {
    component.onClick(mockEvent, component.tabs[1]);
    expect(component.selectedTab).toEqual(component.tabs[1]);
  });

  it('should select first tab if  NO selected tab is passed in', () => {
    component.selectedTab = null;
    component.ngOnInit();
    expect(component.selectedTab).toEqual(tabs[0]);
  });
});
