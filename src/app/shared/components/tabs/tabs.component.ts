import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { ISelectableOption } from '@app/shared/shared.interfaces';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements OnInit {
  @Input()
  tabs: ISelectableOption<any>[];

  @Input()
  selectedTab: ISelectableOption<any>;

  @Input()
  cssClass: string;

  @Output()
  tabChange: EventEmitter<ISelectableOption<any>> = new EventEmitter<ISelectableOption<any>>();

  constructor() {}

  ngOnInit() {
    /* istanbul ignore else */
    if (!this.selectedTab) {
      this.selectedTab = this.tabs[0];
      this.tabChange.emit(this.selectedTab);
    }
  }

  public onClick($event, tab: ISelectableOption<any>) {
    this.selectedTab = tab;
    this.tabChange.emit(this.selectedTab);
  }
}
