import { Component, OnInit, EventEmitter, Input, Output, Self, Optional, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { ControlValueAccessor, NgControl } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-radio',
  templateUrl: './radio.component.html',
  styleUrls: ['./radio.component.scss'],
})
export class RadioComponent implements ControlValueAccessor, OnInit, OnDestroy {
  private _value: any;

  @Input() radioValue: string;
  @Input() error: boolean;
  @Input() label: string;
  @Input() disabled: boolean = false;
  @Output() valueChange = new EventEmitter<any>();

  public subscription: Subscription = new Subscription();

  get value(): any {
    return this._value;
  }

  @Input()
  set value(val: any) {
    this._value = val;
    this.onChange(val);
  }

  public writeValue(value: any): void {
    this._value = value;
  }

  constructor(
    @Self()
    @Optional()
    public ngControl: NgControl,
    private ref: ChangeDetectorRef
  ) {
    if (this.ngControl != null) {
      this.ngControl.valueAccessor = this;
    }
  }

  ngOnInit() {
    if (this.ngControl !== null && typeof this.ngControl.control !== 'undefined') {
      this.startSubscriptions();
    }
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  public onChange(value: any) {}
  public onTouched() {}

  private startSubscriptions() {
    this.subscription.add(this.subscribeValueChanges());
  }

  private subscribeValueChanges(): Subscription {
    return this.ngControl.control.valueChanges.subscribe(val => {
      this.ngControl.control.setValue(val, { emitEvent: false });
      this.ref.markForCheck();
    });
  }

  public registerOnChange(fn: (value: any) => void): void {
    this.onChange = fn;
  }

  public registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  public onClick() {
    this.value = this.radioValue;
    this.valueChange.emit(this.value);
  }

  public onBlur() {
    this.onTouched();
  }

  public isSelected(): boolean {
    return this.radioValue !== undefined && this.value !== undefined && this.radioValue === this.value;
  }
}
