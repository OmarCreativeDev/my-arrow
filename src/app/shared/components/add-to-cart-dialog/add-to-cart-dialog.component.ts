import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { AddToCart, AddToQuote } from '@app/features/cart/stores/cart/cart.actions';
import { IAppState } from '@app/shared/shared.interfaces';
import { IAddToCartRequestItem } from '@app/core/cart/cart.interfaces';
import { Store, select } from '@ngrx/store';
import { ToastService } from '@app/core/toast/toast.service';
import { IAddToQuoteCartRequestItem } from '@app/core/quote-cart/quote-cart.interfaces';
import { getQuoteCartId, getLineItemCountSelector } from '@app/features/quotes/stores/quote-cart.selectors';
import { getShoppingCartId, getCartItemsCount, getError } from '@app/features/cart/stores/cart/cart.selectors';

import { Observable, combineLatest, Subscription } from 'rxjs';
import { skip, take } from 'rxjs/operators';
import { InventoryService } from '@app/core/inventory/inventory.service';
import { IProductValidateResponse, IProductValidateResponseItem } from '@app/core/inventory/product-details.interface';
import { getUserBillToAccount, getCurrencyCode } from '@app/core/user/store/user.selectors';
import { cloneDeep, find } from 'lodash';

export enum CartType {
  SHOPPING_CART = 'SHOPPING_CART',
  QUOTE_CART = 'QUOTE_CART',
}

export interface IAddToCartDataObject {
  cartType: string;
  items: Array<IAddToCartRequestItem> | Array<IAddToQuoteCartRequestItem>;
}

@Component({
  selector: 'app-add-to-cart-dialog',
  templateUrl: './add-to-cart-dialog.component.html',
  styleUrls: ['./add-to-cart-dialog.component.scss'],
})
export class AddToCartDialogComponent implements OnInit, OnDestroy {
  public error: Error;
  public loading: boolean = true;
  public cartType;
  public billToId$: Observable<number>;
  public currencyCode$: Observable<string>;
  public shoppingCartId$: Observable<string>;
  public quoteCartId$: Observable<string>;
  public cartItemCount$: Observable<number>;
  public quoteCartLineItemCount$: Observable<number>;
  public error$: Observable<Error>;
  public invalidItems: Array<IProductValidateResponseItem>;
  private initialCartItems: Array<IAddToCartRequestItem>;
  private invalidCartMsg: string = 'Invalid cartType value';
  public cartItemCounts$;
  public subscription: Subscription = new Subscription();

  constructor(
    public dialog: Dialog<AddToCartDialogComponent>,
    @Inject(APP_DIALOG_DATA) public data: IAddToCartDataObject,
    private toastService: ToastService,
    private store: Store<IAppState>,
    private inventoryService: InventoryService
  ) {
    this.cartType = CartType;
    this.billToId$ = this.store.pipe(select(getUserBillToAccount));
    this.currencyCode$ = this.store.pipe(select(getCurrencyCode));
    this.shoppingCartId$ = this.store.pipe(select(getShoppingCartId));
    this.quoteCartId$ = this.store.pipe(select(getQuoteCartId));
    this.cartItemCount$ = this.store.pipe(select(getCartItemsCount));
    this.quoteCartLineItemCount$ = this.store.pipe(select(getLineItemCountSelector));
    this.error$ = this.store.pipe(select(getError));
  }

  ngOnInit() {
    this.startSubscriptions();

    switch (this.data.cartType) {
      case CartType.SHOPPING_CART:
        this.initialCartItems = cloneDeep(this.data.items) as Array<IAddToCartRequestItem>;
        this.validate(this.initialCartItems);
        return;
      case CartType.QUOTE_CART:
        this.addToQuoteCart();
        return;
      default:
        throw this.invalidCartMsg;
    }
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeCartItemCounts());
    this.subscription.add(this.subscribeError());
  }

  private subscribeCartItemCounts(): Subscription {
    // If the count changes when component is active we have successfully added to cart
    return (this.cartItemCounts$ = combineLatest(this.cartItemCount$, this.quoteCartLineItemCount$)
      .pipe(
        skip(1),
        take(1)
      )
      .subscribe(() => {
        this.handleAddToCartSuccess();
      }));
  }

  private subscribeError(): Subscription {
    // If error is thrown when component is active then something went wrong
    return this.error$
      .pipe(
        skip(1),
        take(1)
      )
      .subscribe(err => {
        this.handleError(err);
      });
  }

  private validate(items: Array<IAddToCartRequestItem>): void {
    this.inventoryService.validateItems(items).subscribe(result => {
      if (result.invalidItems.length) {
        this.onInvalidResult(result, items);
      } else {
        this.addToCart(items);
      }
    }, this.handleError.bind(this));
  }

  private onInvalidResult(result: IProductValidateResponse, items: Array<IAddToCartRequestItem>) {
    this.loading = false;
    this.invalidItems = result.invalidItems;
    for (let i = 0; i < this.invalidItems.length; i++) {
      const initialItem: IAddToCartRequestItem = find(this.initialCartItems, o => {
        return o.docId === this.invalidItems[i].docId;
      });
      initialItem.quantity = this.invalidItems[i].proposedQty;
    }
  }

  public addToQuoteCart() {
    this.quoteCartId$.pipe(take(1)).subscribe(quoteCartId => {
      this.store.dispatch(
        new AddToQuote({
          lineItems: this.data.items as Array<IAddToQuoteCartRequestItem>,
          quoteCartId,
        })
      );
    });
  }

  public addToCart(items: Array<IAddToCartRequestItem>): void {
    combineLatest(this.billToId$, this.currencyCode$, this.shoppingCartId$)
      .pipe(take(1))
      .subscribe(results => {
        const data = {
          billToId: results[0],
          currency: results[1],
          lineItems: items,
          shoppingCartId: results[2],
        };
        this.store.dispatch(new AddToCart(data));
      });
  }

  private handleAddToCartSuccess(): void {
    this.dialog.close();
    this.toastService.showToast(
      `${this.data.items.length} new items have been added to your ${
        this.data.cartType === CartType.SHOPPING_CART ? 'shopping' : 'quote'
      } cart.`
    );
  }

  private handleError(err: Error): void {
    this.loading = false;
    this.error = err;
  }

  public onConfirmProposed(): void {
    this.loading = true;
    this.addToCart(this.initialCartItems);
  }

  public onDismiss(): void {
    const dialogCancelled = true;

    this.cartItemCounts$.unsubscribe();
    this.close(dialogCancelled);
  }

  public close(dialogCancelled?: boolean): void {
    this.dialog.close(dialogCancelled);
  }
}
