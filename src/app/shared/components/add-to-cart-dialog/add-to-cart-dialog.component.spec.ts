import { AddToCartDialogComponent, CartType, IAddToCartDataObject } from './add-to-cart-dialog.component';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Store, StoreModule } from '@ngrx/store';
import { ToastService } from '@app/core/toast/toast.service';
import { InventoryService } from '@app/core/inventory/inventory.service';
import {
  AddToCart,
  GetCartItemCountSuccess,
  AddToCartFailed,
  AddToQuote,
  AddToQuoteFailed,
} from '@app/features/cart/stores/cart/cart.actions';
import { cartReducers } from '@app/features/cart/stores/cart/cart.reducers';
import { userReducers } from '@app/core/user/store/user.reducers';
import { quoteCartReducers } from '@app/features/quotes/stores/quote-cart.reducers';
import { IAddToQuoteCartRequestItem } from '@app/core/quote-cart/quote-cart.interfaces';

class MockToastService {
  public showToast() {}
}

export class InventoryServiceMock {
  public validateItems(): Observable<any> {
    return of({ invalidItems: [], validItems: [] });
  }
}

describe('AddToCartDialogComponent', () => {
  let component: AddToCartDialogComponent;
  let fixture: ComponentFixture<AddToCartDialogComponent>;
  let store: Store<any>;
  let dialog: Dialog<AddToCartDialogComponent>;
  let toastService: ToastService;
  const cartData: IAddToCartDataObject = {
    cartType: CartType.SHOPPING_CART,
    items: [],
  };
  const quoteCartData: IAddToCartDataObject = {
    cartType: CartType.QUOTE_CART,
    items: [
      {
        docId: '0323_01960980',
        itemId: 1960980,
        warehouseId: 323,
        quantity: 5,
        requestDate: '2018-06-15',
        targetPrice: null,
        manufacturerPartNumber: 'BAV99',
      },
    ],
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddToCartDialogComponent],
      imports: [StoreModule.forRoot({ cart: cartReducers, quoteCart: quoteCartReducers, user: userReducers })],
      providers: [
        { provide: Dialog, useClass: DialogMock },
        { provide: APP_DIALOG_DATA, useValue: cartData },
        { provide: ToastService, useClass: MockToastService },
        { provide: InventoryService, useClass: InventoryServiceMock },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToCartDialogComponent);
    component = fixture.componentInstance;

    toastService = TestBed.get(ToastService);
    dialog = TestBed.get(Dialog);
    fixture.detectChanges();
  });

  it('should create', () => {
    spyOn(dialog, 'close');
    component.close();
    expect(dialog.close).toHaveBeenCalled();
  });

  it('should discard proposed chages', () => {
    spyOn(dialog, 'close');
    component.onDismiss();
    expect(dialog.close).toHaveBeenCalledWith(true);
  });

  it('should confirm proposed chages', () => {
    component.onConfirmProposed();
    expect(component.loading).toBeTruthy();
  });

  it('should add to cart', () => {
    component.billToId$ = of(2131422);
    component.currencyCode$ = of('EUR');
    component.shoppingCartId$ = of('5b080807fe03e20001c28a2b');
    const items = [
      {
        manufacturerPartNumber: 'BAV99',
        docId: '0323_00078406',
        itemId: 78406,
        warehouseId: 323,
        quantity: 3000,
        requestDate: '2018-06-14',
      },
    ];
    const expectedCartRequest = {
      billToId: 2131422,
      currency: 'EUR',
      lineItems: items,
      shoppingCartId: '5b080807fe03e20001c28a2b',
    };
    component.addToCart(items);
    expect(store.dispatch).toHaveBeenCalledWith(new AddToCart(expectedCartRequest));
  });

  it('should handle success when line items are added to store', () => {
    spyOn(dialog, 'close');
    spyOn(toastService, 'showToast');
    store.dispatch(new GetCartItemCountSuccess({ lineItemCount: 5, lineItemMax: 100, remainingLineItems: 0 }));
    expect(dialog.close).toHaveBeenCalled();
    expect(toastService.showToast).toHaveBeenCalled();
  });

  it('should handle error on add to cart', () => {
    store.dispatch(new AddToCartFailed(new Error()));
    expect(component.error).toBeTruthy();
  });

  it('should call addToQuote cart method onInit if CartType is QUOTE_CART', () => {
    spyOn(component, 'addToQuoteCart');
    component.data = quoteCartData;
    component.ngOnInit();
    expect(component.addToQuoteCart).toHaveBeenCalled();
  });

  it('should call call AddToQuote action with correct params when adding calling addToQuoteCart()', () => {
    component.data = quoteCartData;
    component.quoteCartId$ = of('5b080807fe03e20001c28a2b');
    component.addToQuoteCart();
    expect(store.dispatch).toHaveBeenCalledWith(
      new AddToQuote({
        quoteCartId: '5b080807fe03e20001c28a2b',
        lineItems: quoteCartData.items as Array<IAddToQuoteCartRequestItem>,
      })
    );
  });

  it('should handle error on add to quote', () => {
    store.dispatch(new AddToQuoteFailed(new Error()));
    expect(component.error).toBeTruthy();
  });
});
