import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogPanelComponent } from './dialog-panel.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { combineReducers, Store, StoreModule } from '@ngrx/store';
import { userReducers } from '@app/core/user/store/user.reducers';

describe('DialogPanelComponent', () => {
  let component: DialogPanelComponent;
  let fixture: ComponentFixture<DialogPanelComponent>;
  let store: Store<any>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [DialogPanelComponent],
        imports: [
          StoreModule.forRoot({
            orders: combineReducers(userReducers),
          }),
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit a close even when the close button is clicked', () => {
    spyOn(component.close, 'emit');
    component.closeClick();
    expect(component.close.emit).toHaveBeenCalled();
  });
});
