import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { IAppState } from '@app/shared/shared.interfaces';
import { Observable, Subscription } from 'rxjs';
import { getUserLoggedIn } from '@app/core/user/store/user.selectors';

@Component({
  selector: 'app-dialog-panel',
  templateUrl: './dialog-panel.component.html',
  styleUrls: ['./dialog-panel.component.scss'],
})
export class DialogPanelComponent implements OnInit, OnDestroy {
  @Input()
  showCloseButton = true;

  @Output()
  close = new EventEmitter<void>();

  private getUserLoggedIn$: Observable<boolean>;
  public subscription: Subscription = new Subscription();
  private loggedIn: boolean = null;

  constructor(private store: Store<IAppState>) {
    this.getUserLoggedIn$ = this.store.pipe(select(getUserLoggedIn));
  }

  public ngOnInit(): void {
    this.startSubscriptions();
  }

  public ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeGetUserLoggedIn());
  }

  private subscribeGetUserLoggedIn(): Subscription {
    return this.getUserLoggedIn$.subscribe(loggedIn => {
      if (this.loggedIn == null) {
        this.loggedIn = loggedIn;
      }
      if (this.loggedIn !== loggedIn) {
        this.close.emit();
      }
    });
  }

  closeClick() {
    this.close.emit();
  }
}
