import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { BackdropServiceMock, ModalServiceMock } from '@app/app.component.spec';
import { ModalComponent } from './modal.component';
import { ModalService } from '@app/shared/services/modal.service';
import { SimpleChange, SimpleChanges } from '@angular/core';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('ModalComponent', () => {
  let backdropService: BackdropService;
  let component: ModalComponent;
  let fixture: ComponentFixture<ModalComponent>;
  let modalService: ModalService;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ModalComponent],
        providers: [
          { provide: BackdropService, useClass: BackdropServiceMock },
          { provide: ModalService, useClass: ModalServiceMock }
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    backdropService = TestBed.get(BackdropService);
    fixture = TestBed.createComponent(ModalComponent);
    modalService = TestBed.get(ModalService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('close() sets `visible` to false and emits an event', () => {
    spyOn(component.visibleChange, 'emit');
    component.hideModal();

    expect(component.visible).toBeFalsy();
    expect(component.visibleChange.emit).toHaveBeenCalledWith(false);
  });

  it('`ngOnChanges()` calls `backdropService.show()` and `modalService.show()` when `visible` is true', () => {
    const mockSimpleChanges: SimpleChanges = {
      visible: new SimpleChange(null, true, null)
    };

    spyOn(backdropService, 'show');
    spyOn(modalService, 'show');

    component.ngOnChanges(mockSimpleChanges);

    expect(backdropService.show).toHaveBeenCalled();
    expect(modalService.show).toHaveBeenCalled();
  });

  it('`ngOnChanges()` calls `backdropService.hide()` and `modalService.hide()` when `visible` is false', () => {
    const mockSimpleChanges: SimpleChanges = {
      visible: new SimpleChange(null, false, null)
    };

    spyOn(backdropService, 'hide');
    spyOn(modalService, 'hide');

    component.ngOnChanges(mockSimpleChanges);

    expect(backdropService.hide).toHaveBeenCalled();
    expect(modalService.hide).toHaveBeenCalled();
  });
});
