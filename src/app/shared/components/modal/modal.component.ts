import { Component, EventEmitter, Input, OnChanges, OnDestroy, Output, SimpleChanges } from '@angular/core';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { ModalService } from '@app/shared/services/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
})
export class ModalComponent implements OnChanges, OnDestroy {
  @Input()
  closable: boolean = true;
  @Input()
  size: string = '';
  @Input()
  visible: boolean;
  @Output()
  visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private backdropService: BackdropService, private modalService: ModalService) {}

  public hideModal(): void {
    this.visible = false;
    this.backdropService.hide();
    this.visibleChange.emit(this.visible);
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (changes.visible.currentValue) {
      this.modalService.show();
      this.backdropService.show();
      document.body.classList.add('modal-open');
    } else {
      if (changes.visible.previousValue === undefined) {
        return;
      }
      this.modalService.hide();
      this.backdropService.hide();
      document.body.classList.remove('modal-open');
    }
  }

  public ngOnDestroy(): void {
    this.hideModal();
  }
}
