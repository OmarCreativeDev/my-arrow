import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { InformationBoxComponent } from './information-box.component';

describe('InformationBoxComponent', () => {
  let component: InformationBoxComponent;
  let fixture: ComponentFixture<InformationBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [InformationBoxComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformationBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
