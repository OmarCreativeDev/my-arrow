import { CurrencyPipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, SimpleChange, SimpleChanges } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { InventoryService } from '@app/core/inventory/inventory.service';
import { IProductPrice } from '@app/core/inventory/product-price.interface';
import { RequestUserComplete } from '@app/core/user/store/user.actions';
import { userReducers } from '@app/core/user/store/user.reducers';
import { mockUser } from '@app/core/user/user.service.spec';
import { FormattedPricePipe } from '@app/shared/pipes/formatted-price/formatted-price.pipe';
import { ProductPriceComponent } from './product-price.component';
import { Observable, of } from 'rxjs';
import { Store, StoreModule } from '@ngrx/store';

export class InventoryServiceMock {
  public price(): Observable<any> {
    return of({});
  }
}

describe('ProductPriceComponent', () => {
  let component: ProductPriceComponent;
  let fixture: ComponentFixture<ProductPriceComponent>;
  let store: Store<any>;

  // const apiFailureMessage = {
  //   message: '500 error',
  // };

  const priceMock: IProductPrice = { price: 1212 };

  const priceTiersMock: IProductPrice = {
    priceTiers: [
      {
        minQuantity: 1,
        maxQuantity: 1,
        price: 1.221,
      },
      {
        minQuantity: 100,
        maxQuantity: 100,
        price: 0.821,
      },
    ],
  };

  const messageMock: IProductPrice = {
    message: 'No price found',
  };

  const bufferQuantityMock: IProductPrice = {
    bufferQuantity: 232323,
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StoreModule.forRoot({ user: userReducers })],
      declarations: [ProductPriceComponent, FormattedPricePipe],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        {
          provide: InventoryService,
          useClass: InventoryServiceMock,
        },
        CurrencyPipe,
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductPriceComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
    store.dispatch(new RequestUserComplete(mockUser));
    jasmine.clock().uninstall();
    jasmine.clock().install();
    fixture.detectChanges();
  });

  afterEach(function() {
    jasmine.clock().uninstall();
  });

  it('`bufferQuantity` should be initially set as null', () => {
    expect(component.bufferQuantity).toBeNull();
  });

  it('`error` should be initially set as null', () => {
    expect(component.error).toBeFalsy();
  });

  it('`loading` should be initially set as false', () => {
    expect(component.loading).toBeFalsy();
  });

  it('`message` should be initially set as null', () => {
    expect(component.message).toBeNull();
  });

  it('`priceTiers` should be initially set as null', () => {
    expect(component.priceTiers).toBeNull();
  });

  it('`ngOnChanges()` calls `setWebPrice()`, `cpnSet()` and `endCustomerSiteIdSet()`', () => {
    const mockSimpleChanges: SimpleChanges = {};

    spyOn(component, 'cpnSet');
    spyOn(component, 'endCustomerSiteIdSet');

    component.ngOnChanges(mockSimpleChanges);

    expect(component.cpnSet).toHaveBeenCalled();
    expect(component.endCustomerSiteIdSet).toHaveBeenCalled();
  });

  it('`ngOnChanges()` calls `setWebPrice()`, `cpnSet()` and `endCustomerSiteIdSet()`', () => {
    const mockSimpleChanges: SimpleChanges = {};

    this.selectForPricing = undefined;

    spyOn(component, 'cpnSet');
    spyOn(component, 'endCustomerSiteIdSet');

    component.ngOnChanges(mockSimpleChanges);

    expect(component.cpnSet).toHaveBeenCalled();
    expect(component.endCustomerSiteIdSet).toHaveBeenCalled();
  });

  it('`ngOnChanges()` calls `removePrice()` if `selectForPricing` is `true`', () => {
    const mockSimpleChanges: SimpleChanges = {};
    component.selectForPricing = true;

    spyOn(component, 'removePrice');

    component.ngOnChanges(mockSimpleChanges);

    expect(component.removePrice).toHaveBeenCalled();
  });

  it('`cpnSet()` calls `getPrice()` when `cpn` is set and there is no `endCustomerSiteId`', () => {
    const mockSimpleChanges: SimpleChanges = {
      cpn: new SimpleChange(null, '1111-1111', null),
    };

    spyOn(component, 'getPrice');

    component.cpnSet(mockSimpleChanges);
    jasmine.clock().tick(150);
    expect(component.getPrice).toHaveBeenCalled();
  });

  it('`cpnSet()` calls `getPrice()` when the blank `cpn` is selected', () => {
    const mockSimpleChanges: SimpleChanges = {
      cpn: new SimpleChange(null, null, null),
    };

    spyOn(component, 'getPrice');

    component.cpnSet(mockSimpleChanges);
    jasmine.clock().tick(150);
    expect(component.getPrice).toHaveBeenCalled();
  });

  it('`endCustomerSiteIdSet()` calls `getPrice()` when `endCustomerSiteId` is set and there is no `cpn`', () => {
    const mockSimpleChanges: SimpleChanges = {
      endCustomerSiteId: new SimpleChange(null, '2222-2222', null),
    };
    spyOn(component, 'getPrice');

    component.endCustomerSiteIdSet(mockSimpleChanges);
    jasmine.clock().tick(150);
    expect(component.getPrice).toHaveBeenCalled();
  });

  it('`apiSuccess()` calls several setterMethods and sets `error` and `loading` variables', () => {
    spyOn(component, 'setPrice');
    spyOn(component, 'setPriceTiers');
    spyOn(component, 'setMessage');
    spyOn(component, 'setBufferQuantity');

    component.apiSuccess(priceMock);

    expect(component.setPrice).toHaveBeenCalled();
    expect(component.setPriceTiers).toHaveBeenCalled();
    expect(component.setMessage).toHaveBeenCalled();
    expect(component.setBufferQuantity).toHaveBeenCalled();
    expect(component.error).toBeFalsy();
    expect(component.loading).toBeFalsy();
  });

  it('`setPrice()` sets `price`', () => {
    component.setPrice(priceMock);
    expect(component.price).toBeDefined();
    expect(component.price).toBe(priceMock.price);
  });

  it('`setPrice()` with null `price`', () => {
    component.setPrice({});
    expect(component.price).toBeDefined();
    expect(component.price).toBe(null);
  });

  it('`setPriceTiers()` sets `priceTiers`', () => {
    component.setPriceTiers(priceTiersMock);

    expect(component.priceTiers).toBeDefined();
    expect(component.priceTiers).toBe(priceTiersMock.priceTiers);
    expect(component.priceTiers.length).toBe(priceTiersMock.priceTiers.length);
  });

  it('`setPriceTiers()` sets null `priceTiers`', () => {
    component.setPriceTiers({});

    expect(component.priceTiers).toBeDefined();
    expect(component.priceTiers).toBe(null);
  });

  it('`shouldHighlight` returns true if input is between record `minQuantity` & `maxQuantity`', () => {
    component.quantity = 8;
    component.shouldHighlight({ minQuantity: 1, maxQuantity: 9, price: 1.221 });
  });

  it('`shouldHighlight` returns false if input is not between record `minQuantity` & `maxQuantity`', () => {
    component.quantity = 11;
    component.shouldHighlight({ minQuantity: 1, maxQuantity: 9, price: 1.221 });
  });

  it('`setMessage()` sets `message`', () => {
    component.setMessage(messageMock);
    expect(component.message).toBeDefined();
    expect(component.message).toBe(messageMock.message);
  });

  it('`setMessage()` sets null `message`', () => {
    component.setMessage({});
    expect(component.message).toBeDefined();
    expect(component.message).toBe(null);
  });

  it('`setBufferQuantity()` sets `bufferQuantity` and emits event if `bufferQuantity` is set', () => {
    spyOn(component.bufferQuantityChange, 'emit');
    component.setBufferQuantity(bufferQuantityMock);

    expect(component.bufferQuantity).toBeDefined();
    expect(component.bufferQuantity).toBe(bufferQuantityMock.bufferQuantity);
    expect(component.bufferQuantityChange.emit).toHaveBeenCalled();
  });

  it('`setBufferQuantity()` sets null `bufferQuantity` and does not emit event', () => {
    spyOn(component.bufferQuantityChange, 'emit');

    component.setBufferQuantity({});
    expect(component.bufferQuantity).toBeDefined();
    expect(component.bufferQuantity).toBe(null);
    expect(component.bufferQuantityChange.emit).not.toHaveBeenCalled();
  });

  it('`apiFailure()` sets `price`, `error` and `loading`', () => {
    component.apiFailure();
    expect(component.price).toBeNull();
    expect(component.error).toBe(true);
    expect(component.loading).toBeFalsy();
  });
});
