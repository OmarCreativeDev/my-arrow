import { Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { filter, take } from 'rxjs/operators';
import { InventoryService } from '@app/core/inventory/inventory.service';
import { IProductPrice, IProductPriceTiers } from '@app/core/inventory/product-price.interface';
import { getCurrencyCode, getUserBillToAccount } from '@app/core/user/store/user.selectors';
import { IAppState } from '@app/shared/shared.interfaces';
import { debounce } from 'lodash-es';

@Component({
  selector: 'app-product-price',
  templateUrl: './product-price.component.html',
  styleUrls: ['./product-price.component.scss'],
})
export class ProductPriceComponent implements OnChanges, OnInit, OnDestroy {
  @Input()
  public cpn: string = null;
  @Input()
  public endCustomerSiteId: number = null;
  @Input()
  public itemId: number = null;
  @Input()
  public selectForPricing: boolean;
  @Input()
  public price: number = null;
  @Input()
  public priceTiers: Array<IProductPriceTiers> = null;
  @Input()
  public quantity: number = 0;
  @Input()
  public warehouseId: number = null;

  @Output()
  bufferQuantityChange: EventEmitter<number> = new EventEmitter<number>();
  @Output()
  priceChange: EventEmitter<number> = new EventEmitter<number>();
  @Output()
  priceTiersChange: EventEmitter<Array<IProductPriceTiers>> = new EventEmitter<Array<IProductPriceTiers>>();
  @Output()
  tariffChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output()
  purchasableChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  public bufferQuantity: number = null;
  public currencyCode$: Observable<string>;
  public currencyCode: string;
  public error: boolean = false;
  public loading: boolean = false;
  public message: string = null;
  public webPrice: IProductPrice = null;
  public currentBillToAccount: number;
  public billToAccount$: Observable<number>;

  public subscription: Subscription = new Subscription();
  /*
    TODO: MYAR20-6486 This was brought in to stop multiple pricing calls firing, but needs
    to be refactored so that the service call is removed and NGRX'd
  */
  public getPrice = debounce(
    () => {
      this.loading = true;
      return this.subscribePrice();
    },
    100,
    { leading: false, trailing: true }
  );

  constructor(private store: Store<IAppState>, public inventoryService: InventoryService) {
    this.currencyCode$ = this.store.pipe(select(getCurrencyCode));
    this.billToAccount$ = this.store.pipe(select(getUserBillToAccount));
  }

  public ngOnChanges(changes: SimpleChanges): void {
    if (!this.selectForPricing || this.selectForPricing === undefined) {
      this.setWebPrice();
      this.cpnSet(changes);
      this.endCustomerSiteIdSet(changes);
    } else {
      this.removePrice();
    }
  }

  public ngOnInit(): void {
    this.startSubscriptions();
  }

  public ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeBillToAccount());
    this.subscription.add(this.subscribeCurrency());
  }

  private subscribePrice(): Subscription {
    return this.inventoryService
      .price(this.itemId, this.warehouseId, this.cpn || null, this.endCustomerSiteId || null)
      .pipe(take(1))
      .subscribe((data: IProductPrice) => this.apiSuccess(data), () => this.apiFailure());
  }

  private subscribeBillToAccount(): Subscription {
    return this.billToAccount$.pipe(filter(billToAccount => billToAccount !== 0)).subscribe((billToAccount: number) => {
      /* istanbul ignore else */
      if (billToAccount && this.currentBillToAccount !== billToAccount) {
        this.currentBillToAccount = billToAccount;
        this.onUpdatePrice();
      }
    });
  }

  private subscribeCurrency(): Subscription {
    return this.currencyCode$.pipe(filter(currencyCode => currencyCode !== '')).subscribe((currencyCode: string) => {
      /* istanbul ignore else */
      if (currencyCode && this.currencyCode !== currencyCode) {
        this.currencyCode = currencyCode;
        this.onUpdatePrice();
      }
    });
  }

  public setWebPrice(): void {
    if (this.webPrice === null) {
      this.webPrice = { price: this.price, priceTiers: this.priceTiers };
    }
  }

  public cpnSet(changes: SimpleChanges): void {
    /* istanbul ignore else */
    if (changes.cpn) {
      this.getPrice();
    }
  }

  public endCustomerSiteIdSet(changes: SimpleChanges): void {
    /* istanbul ignore else */
    if (!changes.cpn && changes.endCustomerSiteId) {
      this.getPrice();
    }
  }

  public apiSuccess(data: IProductPrice): void {
    this.setPrice(data);
    this.setPriceTiers(data);
    this.setMessage(data);
    this.setBufferQuantity(data);

    this.purchasableChange.emit(data.purchasable);
    this.tariffChange.emit(data.tariffApplicable);
    this.error = false;
    this.loading = false;
  }

  public setPrice(data: IProductPrice): void {
    this.price = data['price'] || null;
    if (this.price) {
      this.priceChange.emit(this.price);
    }
  }

  public setPriceTiers(data: IProductPrice): void {
    this.priceTiers = data['priceTiers'] || null;
    if (this.priceTiers) {
      this.priceTiersChange.emit(this.priceTiers);
    }
  }

  public shouldHighlight(record: IProductPriceTiers) {
    return this.quantity >= record.minQuantity && this.quantity <= record.maxQuantity ? true : false;
  }

  public setMessage(data: IProductPrice): void {
    this.message = data['message'] || null;
  }

  public setBufferQuantity(data: IProductPrice): void {
    this.bufferQuantity = data['bufferQuantity'] || null;

    /* istanbul ignore else */
    if (this.bufferQuantity) {
      this.bufferQuantityChange.emit(this.bufferQuantity);
    }
  }

  public apiFailure(): void {
    this.removePrice();
    this.error = true;
    this.loading = false;
  }

  public removePrice(): void {
    this.price = null;
    this.priceTiers = null;
  }

  public onUpdatePrice(): void {
    this.removePrice();
    this.setWebPrice();
    this.getPrice();
  }
}
