import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalendarComponent } from './calendar.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CalendarDisplayState } from '@app/shared/components/date-picker/date-picker.classes';

describe('CalendarComponent', () => {
  let component: CalendarComponent;
  let fixture: ComponentFixture<CalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#isDisabled', () => {
    const testCases = [
      {
        description: `should return true if the date is earlier than the minimum date allowed`,
        date: new Date('2017-01-01T00:00:00Z'),
        bounds: {
          min: new Date('2018-01-01T00:00:00Z'),
          max: new Date('2018-01-02T00:00:00Z'),
        },
        result: true,
      },
      {
        description: `should return true if the date is later than the maximum date allowed`,
        date: new Date('2019-01-01T00:00:00Z'),
        bounds: {
          min: new Date('2018-01-01T00:00:00Z'),
          max: new Date('2018-01-02T00:00:00Z'),
        },
        result: true,
      },
      {
        description: `should return false if the date is within min/max dates`,
        date: new Date('2018-01-01T00:00:00Z'),
        bounds: {
          min: new Date('2017-01-01T00:00:00Z'),
          max: new Date('2019-01-01T00:00:00Z'),
        },
        result: false,
      },
      {
        description: `should return false if min/max dates are not set`,
        date: new Date('2018-01-01T00:00:00Z'),
        bounds: {
          min: undefined,
          max: undefined,
        },
        result: false,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        component.minDate = testCase.bounds.min;
        component.maxDate = testCase.bounds.max;
        const result = component.isDisabled({
          value: testCase.date,
        });
        expect(result).toEqual(testCase.result);
      });
    }
  });

  describe('#isSelected', () => {
    const testCases = [
      {
        description: `should return false if dateRange is empty`,
        selection: [],
        date: new Date('2018-01-01T00:00:00Z'),
        result: false,
      },
      {
        description: `should return false if no matching date was found in the dateRange array`,
        selection: [new Date('1970-01-01T00:00:00Z').getTime()],
        date: new Date('2018-01-01T00:00:00Z'),
        result: false,
      },
      {
        description: `should return true if a matching date was found in the dateRange array`,
        selection: [new Date('2018-01-01T00:00:00Z').getTime()],
        date: new Date('2018-01-01T00:00:00Z'),
        result: true,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        component.displayState = {
          selection: testCase.selection,
        } as CalendarDisplayState;
        const result = component.isSelected({
          value: new Date(testCase.date),
        });
        expect(result).toEqual(testCase.result);
      });
    }
  });

  describe('#isActiveDate', () => {
    const testCases = [
      {
        description: `should return false if the provided date is not the active date`,
        date: new Date('2018-01-01T00:00:00Z'),
        activeDateStamp: new Date('1970-01-01T00:00:00Z').getTime(),
        result: false,
      },
      {
        description: `should return false if the active date is undefined`,
        date: new Date('2018-01-01T00:00:00Z'),
        activeDateStamp: undefined,
        result: false,
      },
      {
        description: `should return true if the provided date is the active date`,
        date: new Date('2018-01-01T00:00:00Z'),
        activeDateStamp: new Date('2018-01-01T00:00:00Z').getTime(),
        result: true,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        component.displayState = {
          activeDate: testCase.activeDateStamp,
        } as CalendarDisplayState;
        const result = component.isActiveDate({
          value: testCase.date,
        });
        expect(result).toEqual(testCase.result);
      });
    }
  });

  describe('#isToday', () => {
    const testCases = [
      {
        description: `should return false if the provided date is not the current date`,
        today: new Date('2018-01-01T00:00:00Z'),
        date: new Date('1970-01-01T00:00:00Z'),
        result: false,
      },
      {
        description: `should return false if the provided date is not the current date`,
        today: new Date('2018-01-01T00:00:00Z'),
        date: new Date('2018-01-01T00:00:00Z'),
        result: true,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        component.today = testCase.today;
        const result = component.isToday({
          value: testCase.date,
        });
        expect(result).toEqual(testCase.result);
      });
    }
  });

  describe('#isHighlightStart', () => {
    const testCases = [
      {
        description: `should return false if highlight start date is undefined`,
        date: new Date('2018-01-01T00:00:00Z'),
        highlightRange: [undefined, undefined],
        result: false,
      },
      {
        description: `should return false if date doesn't match the highlight start date`,
        date: new Date('2018-01-01T00:00:00Z'),
        highlightRange: [new Date('1970-01-01T00:00:00Z').getTime(), undefined],
        result: false,
      },
      {
        description: `should return true if date matches the highlight start date`,
        date: new Date('2018-01-01T00:00:00Z'),
        highlightRange: [new Date('2018-01-01T00:00:00Z').getTime(), undefined],
        result: true,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        component.displayState = {
          highlightRange: testCase.highlightRange,
        } as CalendarDisplayState;
        const result = component.isHighlightStart({ value: testCase.date });
        expect(result).toEqual(testCase.result);
      });
    }
  });

  describe('#isInSelectionRange', () => {
    const testCases = [
      {
        description: `should return false if a date range is an empty array`,
        date: new Date('2018-01-01T00:00:00Z'),
        selection: [],
        result: false,
      },
      {
        description: `should return false if first selection date is undefined`,
        selection: [undefined, new Date('2019-01-01T00:00:00Z').getTime()],
        date: new Date('2018-01-01T00:00:00Z'),
        result: false,
      },
      {
        description: `should return false if second selection date is undefined`,
        selection: [new Date('2017-01-01T00:00:00Z').getTime(), undefined],
        date: new Date('2018-01-01T00:00:00Z'),
        result: false,
      },
      {
        description: `should return false if date is outside selection`,
        selection: [new Date('2017-01-01T00:00:00Z').getTime(), new Date('2018-01-01T00:00:00Z').getTime()],
        date: new Date('1970-01-01T00:00:00Z'),
        result: false,
      },
      {
        description: `should return true if date is within selection`,
        selection: [new Date('2017-01-01T00:00:00Z').getTime(), new Date('2019-01-01T00:00:00Z').getTime()],
        date: new Date('2018-01-01T00:00:00Z'),
        result: true,
      },
      {
        description: `should return true if date is first selection date`,
        selection: [new Date('2018-01-01T00:00:00Z').getTime(), new Date('2019-01-01T00:00:00Z').getTime()],
        date: new Date('2018-01-01T00:00:00Z'),
        result: true,
      },
      {
        description: `should return true if date is last selection date`,
        selection: [new Date('2017-01-01T00:00:00Z').getTime(), new Date('2018-01-01T00:00:00Z').getTime()],
        date: new Date('2018-01-01T00:00:00Z'),
        result: true,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        component.displayState = {
          selection: testCase.selection,
        } as CalendarDisplayState;
        const result = component.isInSelectionRange({ value: testCase.date });
        expect(result).toEqual(testCase.result);
      });
    }
  });

  describe('#isHighlightEnd', () => {
    const testCases = [
      {
        description: `should return false date range only has one date`,
        date: new Date('2018-01-01T00:00:00Z'),
        highlightRange: [undefined],
        result: false,
      },
      {
        description: `should return false if last date in range is undefined`,
        date: new Date('2018-01-01T00:00:00Z'),
        highlightRange: [undefined, undefined],
        result: false,
      },
      {
        description: `should return false if last date in range is not provided date`,
        date: new Date('2018-01-01T00:00:00Z'),
        highlightRange: [undefined, new Date('2017-01-01T00:00:00Z').getTime()],
        result: false,
      },
      {
        description: `should return true if last date in range is provided date`,
        date: new Date('2018-01-01T00:00:00Z'),
        highlightRange: [undefined, new Date('2018-01-01T00:00:00Z').getTime()],
        result: true,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        component.displayState = {
          highlightRange: testCase.highlightRange,
        } as CalendarDisplayState;
        const result = component.isHighlightEnd({ value: testCase.date });
        expect(result).toEqual(testCase.result);
      });
    }
  });

  describe('#isDateOrdinate', () => {
    const testCases = [
      {
        description: `should return false if first date is active`,
        dateRange: [],
        hoverDate: undefined,
        activeDateIndex: 0,
        result: false,
      },
      {
        description: `should return true if first dateRange is undefined`,
        dateRange: [undefined, undefined],
        hoverDate: new Date('2018-01-01T00:00:00Z'),
        activeDateIndex: 1,
        result: true,
      },
      {
        description: `should return false if hoverDate is undefined`,
        dateRange: [new Date('2018-01-01T00:00:00Z'), new Date('2019-01-01T00:00:00Z')],
        hoverDate: undefined,
        activeDateIndex: 1,
        result: false,
      },
      {
        description: `should return true if hoverDate is greater than first date in date range`,
        dateRange: [new Date('2018-01-01T00:00:00Z'), undefined],
        hoverDate: new Date('2019-01-01T00:00:00Z'),
        activeDateIndex: 1,
        result: true,
      },
      {
        description: `should return true if hoverDate is the first date in date range`,
        dateRange: [new Date('2018-01-01T00:00:00Z'), undefined],
        hoverDate: new Date('2018-01-01T00:00:00Z'),
        activeDateIndex: 1,
        result: true,
      },
      {
        description: `should return false if hoverDate is before the first date in date range and the active date index is not 0`,
        dateRange: [new Date('2018-01-01T00:00:00Z'), new Date('2019-01-01T00:00:00Z')],
        hoverDate: new Date('2017-01-01T00:00:00Z'),
        activeDateIndex: 1,
        result: false,
      },
      {
        description: `should return false if hoverDate is after the last date in date range and the active date index is not the last item`,
        dateRange: [new Date('2018-01-01T00:00:00Z'), new Date('2019-01-01T00:00:00Z')],
        hoverDate: new Date('2020-01-01T00:00:00Z'),
        activeDateIndex: 0,
        result: false,
      },
      {
        description: `should return true if hoverDate is before the first date and the active date index is the first item`,
        dateRange: [new Date('2018-01-01T00:00:00Z'), new Date('2019-01-01T00:00:00Z')],
        hoverDate: new Date('2017-01-01T00:00:00Z'),
        activeDateIndex: 0,
        result: true,
      },
      {
        description: `should return true if hoverDate is after the last date and the active date index is the last item`,
        dateRange: [new Date('2018-01-01T00:00:00Z'), new Date('2019-01-01T00:00:00Z')],
        hoverDate: new Date('2020-01-01T00:00:00Z'),
        activeDateIndex: 1,
        result: true,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        // const result = component.isDateOrdinate(
        //   testCase.hoverDate,
        //   testCase.dateRange,
        //   testCase.activeDateIndex
        // );
        // expect(result).toEqual(testCase.result);
      });
    }
  });

  describe('#isHighlighted', () => {
    const testCases = [
      {
        description: `should return false date range dates are undefined`,
        date: new Date('2018-01-01T00:00:00Z'),
        highlightRange: [undefined, undefined],
        result: false,
      },
      {
        description: `should return false only one date is set`,
        date: new Date('2018-01-01T00:00:00Z'),
        highlightRange: [new Date('2018-01-01T00:00:00Z').getTime(), undefined],
        result: false,
      },
      {
        description: `should return false if all dates are set but date is not in bounds`,
        date: new Date('2017-01-01T00:00:00Z'),
        highlightRange: [new Date('2018-01-01T00:00:00Z').getTime(), new Date('2019-01-01T00:00:00Z').getTime()],
        result: false,
      },
      {
        description: `should return true if all dates are set and date is in bounds`,
        date: new Date('2018-01-01T00:00:00Z'),
        highlightRange: [new Date('2017-01-01T00:00:00Z').getTime(), new Date('2019-01-01T00:00:00Z').getTime()],
        result: true,
      },
      {
        description: `should return true if date is first date`,
        date: new Date('2018-01-01T00:00:00Z'),
        highlightRange: [new Date('2018-01-01T00:00:00Z').getTime(), new Date('2019-01-01T00:00:00Z').getTime()],
        result: true,
      },
      {
        description: `should return true if date is last date`,
        date: new Date('2018-01-01T00:00:00Z'),
        highlightRange: [new Date('2017-01-01T00:00:00Z').getTime(), new Date('2018-01-01T00:00:00Z').getTime()],
        result: true,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        component.displayState = {
          highlightRange: testCase.highlightRange,
        } as CalendarDisplayState;
        const result = component.isHighlighted({ value: testCase.date });
        expect(result).toEqual(testCase.result);
      });
    }
  });
});
