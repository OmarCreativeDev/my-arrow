import { Component, EventEmitter, Input, Output } from '@angular/core';
import * as moment from 'moment';
import { WeekdaysMap } from '@app/shared/locale/i18n.maps';
import { CalendarDisplayState, CalendarMonth, DatePickerHelper } from '@app/shared/components/date-picker/date-picker.classes';
import { ICalendarDate } from '@app/shared/components/date-picker/date-picker.interfaces';

export interface IExcludeDates {
  weekdays?: Array<number>;
  ranges?: Array<any>; //we may want to add in a future ranges of dates to exclude
}

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss'],
  providers: [DatePickerHelper],
})
export class CalendarComponent {
  @Input()
  date: Date;
  @Input()
  dateRange: Array<Date>;
  @Input()
  minDate: Date;
  @Input()
  maxDate: Date;
  @Input()
  month: CalendarMonth;
  @Input()
  activeDateIndex: number;
  @Input()
  hoverDateRange: Array<Date>;
  @Input()
  hoverDate: Date;
  @Input()
  displayState: CalendarDisplayState;
  @Output()
  selectDate = new EventEmitter<Date>();
  @Output()
  enterDate = new EventEmitter<Date>();
  @Output()
  leaveDate = new EventEmitter<Date>();
  @Input()
  exclusions: IExcludeDates;

  public weeks: Array<Array<ICalendarDate>> = [];
  public dayNames: Array<string> = ['0', '1', '2', '3', '4', '5', '6'];
  public weekdaysMap;
  public today = moment()
    .startOf('day')
    .toDate();

  constructor() {
    this.weekdaysMap = WeekdaysMap.initials;
  }

  public isDisabled(date: ICalendarDate): boolean {
    const startOfDate = moment(date.value)
      .startOf('day')
      .toDate();

    /* istanbul ignore else */
    if (this.minDate && this.minDate > startOfDate) {
      return true;
    }

    /* istanbul ignore else */
    if (this.maxDate && this.maxDate < startOfDate) {
      return true;
    }

    if (
      this.exclusions &&
      this.exclusions.weekdays.find(element => {
        return element === startOfDate.getDay();
      }) !== undefined
    ) {
      return true;
    }
    return false;
  }

  public isSelected(date: ICalendarDate): boolean {
    return this.displayState.selection.indexOf(date.value.getTime()) >= 0;
  }

  public isActiveDate(date: ICalendarDate): boolean {
    return this.displayState.activeDate === date.value.getTime();
  }

  public isToday(date: ICalendarDate): boolean {
    return date.value.getTime() === this.today.getTime();
  }

  public isHighlightStart(date: ICalendarDate) {
    return this.displayState.highlightRange[0] === date.value.getTime();
  }

  public isHighlightEnd(date: ICalendarDate) {
    return this.displayState.highlightRange[this.displayState.highlightRange.length - 1] === date.value.getTime();
  }

  /**
   * Checks if a provided date is highlighted; that it is within the bounds of date range and/or hover
   * @param date the date to check if it is highlighted
   */
  public isHighlighted(date: ICalendarDate) {
    return (
      date.value.getTime() >= this.displayState.highlightRange[0] &&
      date.value.getTime() <= this.displayState.highlightRange[this.displayState.highlightRange.length - 1]
    );
  }

  public isInSelectionRange(date: ICalendarDate): boolean {
    return (
      date.value.getTime() >= this.displayState.selection[0] &&
      date.value.getTime() <= this.displayState.selection[this.displayState.selection.length - 1]
    );
  }
}
