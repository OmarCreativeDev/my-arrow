import { Component } from '@angular/core';
import { Input } from '@angular/core';

@Component({
  selector: 'app-listing-message',
  templateUrl: './listing-message.component.html',
  styleUrls: ['./listing-message.component.scss'],
})
export class ListingMessageComponent {
  @Input() label: string;
  @Input() icon: string;
  @Input() elemClass: string;
  @Input() error: boolean;
  @Input() noBorder: boolean = false;
}
