import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListingMessageComponent } from '@app/shared/components/listing-message/listing-message.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('ListingMessageComponent', () => {
  let component: ListingMessageComponent;
  let fixture: ComponentFixture<ListingMessageComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [ListingMessageComponent],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ListingMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
