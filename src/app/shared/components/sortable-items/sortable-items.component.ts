import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { uniqueId } from 'lodash-es';
import { DragulaService } from 'ng2-dragula';
import { merge } from 'rxjs';

import { LineSelectionComponent } from '@app/shared/components/line-selection/line-selection.component';
import { ISortableItem } from '@app/shared/shared.interfaces';

@Component({
  selector: 'app-sortable-items',
  templateUrl: './sortable-items.component.html',
  styleUrls: ['./sortable-items.component.scss'],
})
export class SortableItemsComponent extends LineSelectionComponent implements OnChanges {
  @Input()
  items: Array<ISortableItem>;
  @Output()
  stateChange = new EventEmitter<Array<ISortableItem>>();

  public sortableItemsListReference: string = uniqueId(`sortable-item-list-`);
  public itemIndexKey: string = 'id';

  constructor(private dragulaService: DragulaService) {
    super();
    this.initInteractionSubscriptions();
  }

  ngOnChanges(changes: SimpleChanges) {
    /* istanbul ignore else */
    // If items has been set
    if (changes && changes.items && changes.items.currentValue.length) {
      // Find the items that should be auto-selected
      const preSelectedItems = changes.items.currentValue.filter(item => item.selected === true);
      if (preSelectedItems.length) {
        // Auto-select those that are configured
        preSelectedItems.forEach(item => this.toggleLine(item.id, item.selected));
      } else {
        // Otherwise, pre-select all lines
        this.toggleAllLines(true);
      }
    }
  }

  private initInteractionSubscriptions() {
    merge(this.dragulaService.drop, this.selectionChange).subscribe(this.handleStateChange.bind(this));
  }

  private handleStateChange(): void {
    /* istanbul ignore else */
    if (this.items && this.items.length) {
      const orderdSelectedItems = this.items.filter(item => {
        return this.selectedIds.includes(item.id);
      });
      this.stateChange.emit(orderdSelectedItems);
    }
  }
}
