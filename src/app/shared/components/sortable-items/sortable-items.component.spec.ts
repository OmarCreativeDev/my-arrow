import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SortableItemsComponent } from './sortable-items.component';
import { NO_ERRORS_SCHEMA, SimpleChanges, EventEmitter } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { ISortableItem } from '@app/shared/shared.interfaces';

class MockDragulaService {
  public drop = new EventEmitter();
}

describe('SortableItemsComponent', () => {
  let component: SortableItemsComponent;
  let fixture: ComponentFixture<SortableItemsComponent>;
  let dragulaService: DragulaService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SortableItemsComponent],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [{ provide: DragulaService, useClass: MockDragulaService }],
    }).compileComponents();
    dragulaService = TestBed.get(DragulaService);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortableItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('constructor should setup a merged subscription to drag-and-drop and selection', () => {
    const dummyItems = [{ id: 0 } as ISortableItem];
    fixture = TestBed.createComponent(SortableItemsComponent);
    component = fixture.componentInstance;
    const stateChangeSpy = spyOn(component.stateChange, 'emit').and.callFake(() => {});
    fixture.detectChanges();
    component.items = dummyItems;
    component.selectedIds = [0];
    dragulaService.drop.emit();
    component.selectionChange.emit();
    expect(stateChangeSpy).toHaveBeenCalledWith(dummyItems);
  });

  describe('#handleStateChange', () => {
    it('should build an ordered selection and emit to stateChange if the order or selection is changed', () => {
      const stateChangeSpy = spyOn(component.stateChange, 'emit').and.callFake(() => {});
      component.items = [{ id: 0 } as ISortableItem, { id: 1 } as ISortableItem, { id: 2 } as ISortableItem];
      component.selectedIds = [0, 2];
      dragulaService.drop.emit();
      component.selectionChange.emit();
      expect(stateChangeSpy).toHaveBeenCalledWith([{ id: 0 } as ISortableItem, { id: 2 } as ISortableItem]);
    });

    it('should do nothing if there are no items', () => {
      const stateChangeSpy = spyOn(component.stateChange, 'emit').and.callFake(() => {});
      component.items = [];
      dragulaService.drop.emit();
      component.selectionChange.emit();
      expect(stateChangeSpy).not.toHaveBeenCalled();
    });
  });

  describe('#ngOnChanges', () => {
    it('should select all lines if items were changed', () => {
      const toggleAllLinesSpy = spyOn(component, 'toggleAllLines');
      const changes = {
        items: {
          currentValue: [{ id: 0 } as ISortableItem],
          previousValue: [],
          firstChange: false,
          isFirstChange: () => true,
        },
      };
      component.ngOnChanges(changes);
      expect(toggleAllLinesSpy).toHaveBeenCalledWith(true);
    });

    it('should select all items lines if any configured to be pre-selected', () => {
      const toggleLineSpy = spyOn(component, 'toggleLine');
      const preSelectedItem = { label: '0', id: 0, selected: true } as ISortableItem;
      const preSelectedItems = [preSelectedItem];
      const dummyItems = [...preSelectedItems, { label: '1', id: 1, selected: false } as ISortableItem];
      const changes = {
        items: {
          currentValue: dummyItems,
          previousValue: [],
          firstChange: false,
          isFirstChange: () => true,
        },
      };
      component.ngOnChanges(changes as SimpleChanges);
      expect(toggleLineSpy).toHaveBeenCalledWith(preSelectedItem.id, preSelectedItem.selected);
    });
  });
});
