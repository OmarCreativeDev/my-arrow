import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA, Component, DebugElement } from '@angular/core';
import { IsoDatePipe } from '@app/shared/pipes/iso-date/iso-date.pipe';
import { SecureTextboxComponent } from './secure-textbox.component';
import { By } from '@angular/platform-browser';
import { FormGroup, FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

@Component({
  template: `
    <form [formGroup]="testForm">
      <app-secure-textbox
        class="secure_input"
        [formControl]="testForm.controls.testInput"
        placeholder="Placeholder Text"
        [maxlength]="16"
        [value]="1234567890"
      >
      </app-secure-textbox>
    </form>
  `,
})
class TestSecureTextboxComponent {
  public testForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    const fields = {
      testInput: [
        {
          value: '1234567890',
          disabled: false,
        },
      ],
    };
    this.testForm = this.formBuilder.group(fields);
  }
}

describe('SecureTextboxComponent', () => {
  let component: TestSecureTextboxComponent;
  let fixture: ComponentFixture<TestSecureTextboxComponent>;
  let inputElement: DebugElement;
  let showIconElement: DebugElement;
  let inputComponent: SecureTextboxComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [TestSecureTextboxComponent, SecureTextboxComponent, IsoDatePipe],
      providers: [],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(TestSecureTextboxComponent);
    component = fixture.componentInstance;
    inputElement = fixture.debugElement.query(By.css('.secure-textbox__input'));
    showIconElement = fixture.debugElement.query(By.css('.icon-link__icon'));

    inputComponent = inputElement.injector.get(SecureTextboxComponent);

    fixture.detectChanges();

    spyOn(inputComponent, 'writeValue').and.callThrough();
  });

  it('should create test component', () => {
    expect(component).toBeTruthy();
  });

  it('should set input value, and plainValue to 1234567890', () => {
    expect(inputComponent.plainValue).toBe('1234567890');
    expect(inputComponent.value).toBe('1234567890');
  });

  it('should set empty string if value is undefined on writevalue', () => {
    inputComponent.writeValue(undefined);
    expect(inputComponent.value).toBe('');
  });

  it('should mask the value by default if value is larger than 0', () => {
    expect((<Element>inputElement.nativeElement).classList.contains('shadow-text')).toBe(true);
  });

  it('should unmask the value if the show icon have the mouse over it', () => {
    const mouseEnter = new Event('mouseenter');
    showIconElement.nativeElement.dispatchEvent(mouseEnter);
    expect(inputComponent.value.length).toBeGreaterThan(0);
    expect((<Element>inputElement.nativeElement).classList.contains('shadow-text')).toBe(false);
  });

  it('should mask the value if the show icon does not have the mouse over it', () => {
    const mouseLeave = new Event('mouseleave');
    showIconElement.nativeElement.dispatchEvent(mouseLeave);
    expect(inputComponent.value.length).toBeGreaterThan(0);
    expect((<Element>inputElement.nativeElement).classList.contains('shadow-text')).toBe(true);
  });

  it('should call writeValue on Input', () => {
    const inputEvent = new Event('input');
    inputElement.nativeElement.dispatchEvent(inputEvent);
    expect(inputComponent.writeValue).toHaveBeenCalled();
  });

  it('should set proper disabled State', () => {
    inputComponent.setDisabledState(true);
    expect(inputComponent.disabled).toBe(true);
  });
});
