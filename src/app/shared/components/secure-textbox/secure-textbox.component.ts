import { Component, ElementRef, EventEmitter, forwardRef, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-secure-textbox',
  templateUrl: './secure-textbox.component.html',
  styleUrls: ['./secure-textbox.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SecureTextboxComponent),
      multi: true,
    },
  ],
})
export class SecureTextboxComponent implements OnInit, OnDestroy, ControlValueAccessor {
  @ViewChild('container')
  container: ElementRef;

  private shadowedFieldRef: HTMLInputElement;
  private unmaskIcon: Element;

  @Input()
  placeholder: string = '';
  @Input()
  public maxlength: number = 99999;
  @Input()
  disabled: boolean = false;
  @Output()
  valueChange = new EventEmitter<string>();

  public plainValue: string;

  public actionSubscription: Subscription;

  get value(): string {
    return this.plainValue;
  }

  @Input()
  set value(val: string) {
    this.writeValue(String(val));
    this.valueChange.emit(this.plainValue);
  }

  ngOnInit() {
    const container = <HTMLElement>this.container.nativeElement;
    this.shadowedFieldRef = <HTMLInputElement>container.getElementsByClassName('secure-textbox__input')[0];
    this.unmaskIcon = container.getElementsByClassName('secure-textbox__show-icon')[0];
    this.attachListeners();
  }

  private attachListeners() {
    this.attachOnInputListener(this.handleInput);
    this.attachOnMouseListener(this.handleMouseOver);
  }

  private attachOnMouseListener(handler) {
    this.unmaskIcon.addEventListener('mouseenter', handler.bind(this));
    this.unmaskIcon.addEventListener('mouseleave', handler.bind(this));
  }

  private attachOnInputListener(handler) {
    this.shadowedFieldRef.addEventListener('input', handler.bind(this));
  }

  private handleMouseOver(event: KeyboardEvent) {
    switch (event.type) {
      case 'mouseenter':
        /* istanbul ignore else */
        if (this.shadowedFieldRef.classList.contains('shadow-text')) {
          this.shadowedFieldRef.classList.remove('shadow-text');
        }
        break;

      case 'mouseleave':
        /* istanbul ignore else */
        if (this.plainValue.length > 0) {
          this.shadowedFieldRef.classList.add('shadow-text');
        }
        break;
    }
  }

  private handleInput() {
    this.writeValue(this.shadowedFieldRef.value);
  }

  ngOnDestroy() {}

  // From ControlValueAccessor
  public writeValue(value: string): void {
    if (value) {
      this.plainValue = value;
    } else {
      this.plainValue = '';
    }
    this.onChangeFunction(this.plainValue);
  }

  // From ControlValueAccessor
  public registerOnChange(fn: any): void {
    this.onChangeFunction = fn;
  }

  // From ControlValueAccessor
  public registerOnTouched(fn: any): void {
    this.onTouchedFunction = fn;
  }

  // From ControlValueAccessor
  public setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  public onChangeFunction: Function = (_: any) => {};
  public onTouchedFunction: Function = () => {};
}
