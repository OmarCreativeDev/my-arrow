import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
})
export class InventoryComponent {
  @Input()
  public availableQuantity: number = null;
  @Input()
  public bufferQuantity: number = null;
  @Input()
  public multipleOrderQuantity: number = null;
  @Input()
  public spq: number = null;
  @Input()
  public supplierAllocated: string;
  @Input()
  public quotable: boolean;
}
