import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InventoryComponent } from './inventory.component';

describe('InventoryComponent', () => {
  let component: InventoryComponent;
  let fixture: ComponentFixture<InventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('`availableQuantity` is inititially set as null', () => {
    expect(component.availableQuantity).toBeNull();
  });

  it('`bufferQuantity` is inititially set as null', () => {
    expect(component.bufferQuantity).toBeNull();
  });

  it('`multipleOrderQuantity` is inititially set as null', () => {
    expect(component.multipleOrderQuantity).toBeNull();
  });

  it('`spq` is inititially set as null', () => {
    expect(component.spq).toBeNull();
  });
});
