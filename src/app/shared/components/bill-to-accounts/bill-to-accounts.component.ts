import { Component, OnInit, OnDestroy } from '@angular/core';
import { IListingState, IAppState } from '@app/shared/shared.interfaces';
import { IBillTo } from '@app/core/user/user.interface';
import { getUserBillToAccount, getCurrencyCode, getUserBillToAccounts } from '@app/core/user/store/user.selectors';
import { getShoppingCartId, getQuotedItemCountSelector } from '@app/features/cart/stores/cart/cart.selectors';
import { ActionsSubject, Store, select } from '@ngrx/store';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { filter, take, tap } from 'rxjs/operators';
import { UpdateBillTo, UserActionTypes, RequestBillToAccounts } from '@app/core/user/store/user.actions';
import { Dialog } from '@app/core/dialog/dialog.service';
import { AlertComponentEnum } from '@app/shared/shared.interfaces';
import { find } from 'lodash-es';
import { PartialUpdateShoppingCart, GetQuotedItemCount } from '@app/features/cart/stores/cart/cart.actions';
import { ICartStatus, ICartValidationStatus } from '@app/core/cart/cart.interfaces';
import { getQuoteCartId } from '@app/features/quotes/stores/quote-cart.selectors';
import { UpdateQuoteCart } from '@app/features/quotes/stores/quote-cart.actions';

@Component({
  selector: 'app-bill-to-accounts',
  templateUrl: './bill-to-accounts.component.html',
  styleUrls: ['./bill-to-accounts.component.scss'],
})
export class BillToAccountsComponent implements OnInit, OnDestroy {
  public billToAccounts: IListingState<IBillTo> = {
    loading: true,
    error: null,
    items: [],
  };
  public originalBillToAccountId: number;
  public selectedBillToAccountId: number;
  public showConfirmation: boolean = false;
  public updating: boolean = false;
  public errorSelectingBillTo: boolean = false;
  public AlertComponentEnum = AlertComponentEnum;
  public billToAccounts$: Observable<IListingState<IBillTo>>;
  private billToId$: Observable<number>;
  private currencyCode$: Observable<string>;
  private shoppingCartId$: Observable<string>;
  private getSelectedBillToSub$: Observable<number>;
  private quoteCartId$: Observable<string>;
  public quotedItemCount$: Observable<number>;
  public quotedItemCount: number;
  public subscription: Subscription = new Subscription();
  public name: string;

  constructor(private store: Store<IAppState>, private actionsSubj$: ActionsSubject, private dialog: Dialog<BillToAccountsComponent>) {
    this.shoppingCartId$ = store.pipe(select(getShoppingCartId));
    this.billToId$ = store.pipe(select(getUserBillToAccount));
    this.currencyCode$ = store.pipe(select(getCurrencyCode));
    this.billToAccounts$ = store.pipe(select(getUserBillToAccounts));
    this.quoteCartId$ = store.pipe(select(getQuoteCartId));
    this.quotedItemCount$ = store.pipe(select(getQuotedItemCountSelector));
    this.getSelectedBillToSub$ = this.store.pipe(select(getUserBillToAccount));
  }

  ngOnInit() {
    this.startSubscriptions();
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.getSelectedBillTo());
    this.subscription.add(this.requestBillToAccounts());
    this.subscription.add(this.getQuotedItemCount());
    this.subscription.add(this.checkQuotedItemCount());
  }

  public getSelectedBillTo(): Subscription {
    return this.getSelectedBillToSub$.subscribe(billToAccountId => {
      this.originalBillToAccountId = this.selectedBillToAccountId = billToAccountId;
    });
  }

  public requestBillToAccounts(): Subscription {
    return this.billToAccounts$
      .pipe(
        tap(billToAccounts => {
          if (!billToAccounts) {
            this.store.dispatch(new RequestBillToAccounts());
          }
        }),
        filter(billToAccounts => billToAccounts !== undefined),
        take(1)
      )
      .subscribe(billToAccounts => {
        this.billToAccounts = billToAccounts;
      });
  }

  public getQuotedItemCount(): Subscription {
    return this.shoppingCartId$.subscribe(shoppingCartId => {
      if (shoppingCartId) {
        this.store.dispatch(new GetQuotedItemCount({ shoppingCartId }));
      }
    });
  }

  public checkQuotedItemCount(): Subscription {
    return this.quotedItemCount$.subscribe(quotedItemCount => {
      this.quotedItemCount = quotedItemCount;
    });
  }

  public invalidateShoppingCart(): Subscription {
    return combineLatest(this.billToId$, this.currencyCode$, this.shoppingCartId$).subscribe(latestValues => {
      const [billToId, currency, shoppingCartId] = latestValues;
      const updateShoppingCart = new PartialUpdateShoppingCart({
        billToId: billToId,
        currency: currency,
        shoppingCartId: shoppingCartId,
        validation: ICartValidationStatus.INVALID,
        status: ICartStatus.IN_PROGRESS,
      });

      this.store.dispatch(updateShoppingCart);
    });
  }

  public invalidateQuoteCart(): Subscription {
    return combineLatest(this.billToId$, this.currencyCode$, this.quoteCartId$).subscribe(latestValues => {
      const [billToId, currency, quoteCartId] = latestValues;
      const updateQuoteCart = new UpdateQuoteCart({
        billToId: billToId,
        currency: currency,
        cartId: quoteCartId,
      });

      this.store.dispatch(updateQuoteCart);
    });
  }

  public updateBillTo(): void {
    const selectedShipToAccount = find(this.billToAccounts.items, { billToId: this.selectedBillToAccountId });
    this.store.dispatch(new UpdateBillTo(selectedShipToAccount.ebsPersonId));
  }

  public invalidateCarts(): void {
    this.actionsSubj$.pipe(filter(action => action.type === UserActionTypes.USER_UPDATE_BILL_TO_COMPLETE)).subscribe(() => {
      this.updating = false;
      this.originalBillToAccountId = this.selectedBillToAccountId;
      this.subscription.add(this.invalidateShoppingCart());
      this.subscription.add(this.invalidateQuoteCart());
      this.dismiss();
    });
  }

  public handleBillToChangeErrors(): void {
    this.actionsSubj$.pipe(filter(action => action.type === UserActionTypes.USER_UPDATE_BILL_TO_FAILED)).subscribe(() => {
      this.updating = false;
      this.showConfirmation = false;
      this.errorSelectingBillTo = true;
    });
  }

  public select(): void {
    if (this.selectedBillToAccountId === this.originalBillToAccountId) {
      this.dismiss();
    } else {
      this.showConfirmation = true;
    }
  }

  public confirm(): void {
    this.updating = true;
    this.errorSelectingBillTo = false;
    this.updateBillTo();
    this.invalidateCarts();
    this.handleBillToChangeErrors();
  }

  public dismiss(): void {
    this.dialog.close();
  }
}
