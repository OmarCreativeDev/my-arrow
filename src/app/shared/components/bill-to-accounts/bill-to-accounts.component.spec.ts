import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpErrorResponse } from '@angular/common/http';
import { Store, StoreModule } from '@ngrx/store';
import { BillToAccountsComponent } from './bill-to-accounts.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IBillTo, IUser } from '@app/core/user/user.interface';
import { userReducers, INITIAL_USER_STATE } from '@app/core/user/store/user.reducers';
import { UserService } from '@app/core/user/user.service';
import { CoreModule } from '@app/core/core.module';
import { of } from 'rxjs';
import { UpdateBillTo, UpdateBillToComplete, UpdateBillToFailed, RequestBillToAccounts } from '@app/core/user/store/user.actions';
import { Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { FormattedAddressPipe } from '@app/shared/pipes/formatted-address/formatted-address.pipe';
import { ApiService } from '@app/core/api/api.service';
import { mockUser } from '@app/core/user/user.service.spec';
import { cartReducers, INITIAL_CART_STATE } from '@app/features/cart/stores/cart/cart.reducers';
import { quoteCartReducers, INITIAL_QUOTE_CART_STATE } from '@app/features/quotes/stores/quote-cart.reducers';
import { CartService } from '@app/core/cart/cart.service';
import { PartialUpdateShoppingCart } from '@app/features/cart/stores/cart/cart.actions';
import { IShoppingCartPatchRequest, ICartStatus } from '@app/core/cart/cart.interfaces';
import { IListingState } from '@app/shared/shared.interfaces';

class ApiServiceMock {
  get() {
    return of([]);
  }
}

export const mockBillToAccounts: IListingState<IBillTo> = {
  loading: false,
  error: undefined,
  items: [
    {
      name: 'B & C ELECTRONIC ENGINEERING',
      accountId: 1305704,
      accountNumber: 1067644,
      ebsPersonId: 7054454,
      billToId: 2171627,
      orgId: 241,
      address1: ' ',
      address2: '185 W Louisiana Avenue',
      address3: '',
      address4: '',
      city: 'Denver',
      state: 'CO',
      province: '',
      country: 'US',
      countryDescription: 'United States',
      postalCode: '80223',
      selected: null,
    },
    {
      name: 'B & C ELECTRONIC ENGINEERING',
      accountId: 123,
      accountNumber: 1067644,
      ebsPersonId: 1234,
      billToId: 1365393,
      orgId: 241,
      address1: ' ',
      address2: '185 W Louisiana Avenue',
      address3: '',
      address4: '',
      city: 'Denver',
      state: 'CO',
      province: '',
      country: 'US',
      countryDescription: 'United States',
      postalCode: '80223',
      selected: null,
    },
  ],
};

const initialState = {
  user: {
    ...INITIAL_USER_STATE,
    profile: mockUser,
    billToAccounts: mockBillToAccounts,
  },
  cart: INITIAL_CART_STATE,
  quoteCart: INITIAL_QUOTE_CART_STATE,
};

describe('BillToAccountsComponent', () => {
  let store: Store<any>;
  let component: BillToAccountsComponent;
  let userService: UserService;
  let fixture: ComponentFixture<BillToAccountsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        CoreModule,
        StoreModule.forRoot(
          {
            user: userReducers,
            cart: cartReducers,
            quoteCart: quoteCartReducers,
          },
          { initialState: initialState }
        ),
      ],
      providers: [UserService, CartService, { provide: Dialog, useClass: DialogMock }, { provide: ApiService, useClass: ApiServiceMock }],
      declarations: [BillToAccountsComponent, FormattedAddressPipe],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BillToAccountsComponent);
    userService = TestBed.get(UserService);

    component = fixture.componentInstance;
    fixture.detectChanges();
    store = TestBed.get(Store);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set billToAccounts property onInit', () => {
    spyOn(store, 'dispatch');
    spyOn(userService, 'getBillToAccounts').and.returnValue(of(mockBillToAccounts));
    component.ngOnInit();
    component.billToAccounts = mockBillToAccounts;
  });

  it('should dispatch RequestBillToAccounts() action if billToAccounts is undefined in store', () => {
    spyOn(store, 'dispatch');
    spyOn(userService, 'getBillToAccounts').and.returnValue(of(mockBillToAccounts));
    component.billToAccounts$ = of(undefined);
    component.ngOnInit();
    component.billToAccounts = mockBillToAccounts;
    expect(store.dispatch).toHaveBeenCalledWith(new RequestBillToAccounts());
    component.billToAccounts = mockBillToAccounts;
  });

  it('should dismiss the dialog if the original and selected bill to accounts are the same', () => {
    component.originalBillToAccountId = 123;
    component.selectedBillToAccountId = 123;
    spyOn(component, 'dismiss').and.callThrough();
    component.select();
    expect(component.showConfirmation).toBeFalsy();
    expect(component.dismiss).toHaveBeenCalled();
  });

  it('should ask the user to confirm changing their Bill-To account', () => {
    component.originalBillToAccountId = 123;
    component.selectedBillToAccountId = 2171627;
    component.select();
    expect(component.showConfirmation).toBeTruthy();
  });

  it('should dispatch the new selected value to the store', () => {
    spyOn(store, 'dispatch');
    component.billToAccounts = mockBillToAccounts;
    component.originalBillToAccountId = 123;
    component.selectedBillToAccountId = 2171627;
    component.confirm();
    expect(component.updating).toBeTruthy();
    expect(store.dispatch).toHaveBeenCalledWith(new UpdateBillTo(7054454));
  });

  it('should update the store and close the modal if updating billTo is successful', () => {
    const user: IUser = { ...mockUser, selectedBillTo: 1365393 };
    spyOn(component, 'dismiss').and.callThrough();
    component.billToAccounts = mockBillToAccounts;
    component.originalBillToAccountId = 123;
    component.selectedBillToAccountId = 1365393;
    component.confirm();
    store.dispatch(new UpdateBillToComplete(user));
    expect(component.updating).toBeFalsy();
    expect(component.originalBillToAccountId).toEqual(1365393);
    expect(component.dismiss).toHaveBeenCalled();
  });

  it('should not update originalBillToAccountId and should show error if updating billTo is unsuccessful', () => {
    spyOn(component, 'dismiss').and.callThrough();
    component.billToAccounts = mockBillToAccounts;
    component.originalBillToAccountId = 123;
    component.selectedBillToAccountId = 1365393;
    component.confirm();
    store.dispatch(new UpdateBillToFailed(new HttpErrorResponse({ status: 500 })));
    expect(component.updating).toBeFalsy();
    expect(component.errorSelectingBillTo).toBeTruthy();
    expect(component.originalBillToAccountId).toEqual(123);
    expect(component.dismiss).not.toHaveBeenCalled();
  });

  it('should dispatch UpdateShoppingCartAfterBillToAccount if updating billTo is successful', () => {
    const user: IUser = { ...mockUser, selectedBillTo: 1365393 };
    spyOn(store, 'dispatch').and.callThrough();
    spyOn(store, 'select').and.returnValue(of('111'));
    component.billToAccounts = mockBillToAccounts;
    component.originalBillToAccountId = 123;
    component.selectedBillToAccountId = 1365393;
    component.confirm();
    store.dispatch(new UpdateBillToComplete(user));
    const partialUpdateRequest: IShoppingCartPatchRequest = {
      billToId: 1365393,
      currency: 'USD',
      shoppingCartId: undefined,
      validation: 'INVALID',
      status: ICartStatus.IN_PROGRESS,
    };
    expect(component.updating).toBeFalsy();
    expect(store.dispatch).toHaveBeenCalledWith(new PartialUpdateShoppingCart(partialUpdateRequest));
  });
});
