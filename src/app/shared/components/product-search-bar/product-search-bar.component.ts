import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-search-bar',
  templateUrl: './product-search-bar.component.html',
  styleUrls: ['./product-search-bar.component.scss'],
})
export class ProductSearchBarComponent implements OnInit {
  public form: FormGroup;

  @Output() isSearching = new EventEmitter<boolean>();

  constructor(public formBuilder: FormBuilder, public router: Router) {}

  public ngOnInit(): void {
    this.setupForm();
  }

  public setupForm(): void {
    this.form = this.formBuilder.group({
      searchQuery: ['', [Validators.required]],
    });
  }

  public checkForm(): void {
    /* istanbul ignore else */
    if (this.form.valid) {
      this.submitForm();
      this.form.reset();
    }
  }

  public submitForm(): void {
    this.isSearching.emit(true);
    const searchQuery = this.form.controls.searchQuery.value.trim();
    this.router.navigate(['/products/search', searchQuery], { queryParams: { q: searchQuery } });
  }
}
