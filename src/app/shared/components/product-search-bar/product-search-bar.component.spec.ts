import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { Location } from '@angular/common';
import { ProductSearchBarComponent } from './product-search-bar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserService } from '@app/shared/services/browser.service';

export const ALLOWED_CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789 -_=+/';

class DummyComponent {}

describe('ProductSearchBarComponent', () => {
  const routes = [{ path: 'products/search/:searchQuery', component: DummyComponent }];
  let component: ProductSearchBarComponent;
  let fixture: ComponentFixture<ProductSearchBarComponent>;
  let location: Location;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule.withRoutes(routes), ReactiveFormsModule, FormsModule],
        declarations: [ProductSearchBarComponent],
        providers: [BrowserService],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSearchBarComponent);
    location = TestBed.get(Location);

    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should check form, submit if valid and reset form', () => {
    const submitFormSpy = spyOn(component, 'submitForm');
    const resetFormSpy = spyOn(component.form, 'reset');
    expect(component.form.valid).toBeFalsy();
    component.form.controls['searchQuery'].setValue('Product A');
    expect(component.form.valid).toBeTruthy();
    component.checkForm();
    expect(submitFormSpy).toHaveBeenCalled();
    expect(resetFormSpy).toHaveBeenCalled();
  });

  it('should trim searchQuery once submitted', () => {
    const routerSpy = spyOn(component.router, 'navigate');
    component.form.controls['searchQuery'].setValue(' Product ');
    component.submitForm();
    expect(routerSpy).toHaveBeenCalledWith(['/products/search', 'Product'], { queryParams: { q: 'Product' } });
  });

  it(
    'should redirect user to search page with their search, in addition populate query param for GA site search',
    fakeAsync(() => {
      component.form.controls['searchQuery'].setValue('Product[A]');
      component.submitForm();
      tick(50);
      expect(location.path()).toBe('/products/search/Product%5BA%5D?q=Product%5BA%5D');
    })
  );
});
