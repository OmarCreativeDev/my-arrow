import { CurrencyPipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { IReelPriceRequest } from '@app/core/reel/reel.interfaces';
import { RequestReelPrice, OpenReelModal } from '@app/core/reel/stores/reel.actions';
import { reelReducers } from '@app/core/reel/stores/reel.reducers';
import { IPricePostData } from '@app/shared/components/end-customer-dropdowns/end-customer-dropdowns.interface';
import { ReelCalculationComponent } from '@app/shared/components/reel-calculation/reel-calculation.component';
import { FormattedPricePipe } from '@app/shared/pipes/formatted-price/formatted-price.pipe';
import { IProduct } from '@app/shared/shared.interfaces';
import { Store, StoreModule } from '@ngrx/store';
import { Router } from '@angular/router';

class MockRouter {
  url: string = '/test';
}

describe('ReelCalculationComponent', () => {
  let store: Store<any>;
  let component: ReelCalculationComponent;
  let fixture: ComponentFixture<ReelCalculationComponent>;

  const productMock: IProduct = {
    docId: 'xxxx_xxxx',
    itemId: 1,
    mpn: 'JAYZ',
    manufacturer: 'orange ltd',
    description: 'microphone',
    image: 'data:image/gif;base64,R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs=',
    itemType: 'COMPONENTS',
    compliance: { EU_ROHS: 'EU_ROHS_RHC' },
    warehouseId: 145,
    quotable: false,
    arrowReel: true,
    minimumOrderQuantity: 100,
    availableQuantity: 10,
    bufferQuantity: 200,
    leadTime: 1,
    customerPartNumber: 'LOB1',
    price: 22.99,
    pipeline: {
      deliveryDate: '03/06/2018',
      quantity: 7000,
    },
    spq: 578,
    multipleOrderQuantity: 75,
    id: '1234_12345678',
    specs: [
      {
        name: 'EU RoHS Compliant',
        value: 'ON OFFF',
      },
    ],
  };

  const reelCpnChangeEvent: IPricePostData = {
    cpn: 'newCPN',
    endCustomerSiteId: 12345,
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        StoreModule.forRoot({
          reel: reelReducers,
        }),
      ],
      declarations: [ReelCalculationComponent, FormattedPricePipe],
      providers: [CurrencyPipe, { provide: Router, useClass: MockRouter }],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReelCalculationComponent);
    component = fixture.componentInstance;
    component.billToId = 1;
    component.currencyCode = 'USD';
    component.product = productMock;
    fixture.detectChanges();
    store = TestBed.get(Store);
    spyOn(store, 'dispatch').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('#onDropdownsSelectionChange', () => {
    expect(component.form.dirty).toBeFalsy();
    expect(component.form.controls['cpn'].value).toBeNull();
    expect(component.form.controls['endCustomerId'].value).toBeNull();

    component.onDropdownsSelectionChange(reelCpnChangeEvent);

    expect(component.form.dirty).toBeTruthy();
    expect(component.form.controls['cpn'].value).toEqual('newCPN');
    expect(component.form.controls['endCustomerId'].value).toEqual(12345);
  });

  it('should dispatch `RequestReelPrice` on `getReelPrice()`', () => {
    const reelRequest: IReelPriceRequest = {
      billToId: 1,
      currency: 'USD',
      itemId: 1,
      noOfReels: 10,
      partsInReel: 50,
      warehouseId: 145,
    };

    component.form.controls['itemsPerReel'].setValue(50);
    component.form.controls['numberOfReels'].setValue(10);

    component.getReelPrice();
    expect(store.dispatch).toHaveBeenCalledWith(new RequestReelPrice(reelRequest));
  });

  it('should dispatch `RequestReelPrice` on `getReelPrice()` with Customer Part Number and End Customer', () => {
    const reelRequest: IReelPriceRequest = {
      billToId: 1,
      currency: 'USD',
      itemId: 1,
      noOfReels: 10,
      partsInReel: 50,
      warehouseId: 145,
      cpn: 'newCPN',
      endCustomerSiteId: 12345,
    };

    component.form.controls['itemsPerReel'].setValue(50);
    component.form.controls['numberOfReels'].setValue(10);
    component.onDropdownsSelectionChange(reelCpnChangeEvent);

    component.getReelPrice();
    expect(store.dispatch).toHaveBeenCalledWith(new RequestReelPrice(reelRequest));
  });
  it('should dispatch OpenReelModal when the component inits', () => {
    const action = new OpenReelModal({ url: '/test' });
    component.ngOnInit();
    expect(store.dispatch).toHaveBeenCalledWith(action);
  });
});
