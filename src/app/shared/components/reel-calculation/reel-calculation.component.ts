import { Component, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IReelPrice, IReelPriceRequest } from '@app/core/reel/reel.interfaces';
import { RequestReelPrice, ClearReelPrice, OpenReelModal } from '@app/core/reel/stores/reel.actions';
import { getReelError, getReelLoadingStatus, getReelPrice } from '@app/core/reel/stores/reel.selectors';
import { CustomValidators } from '@app/shared/classes/custom-validators';
import { IPricePostData } from '@app/shared/components/end-customer-dropdowns/end-customer-dropdowns.interface';
import { IAppState } from '@app/shared/shared.interfaces';
import { Store, select } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reel-calculation',
  templateUrl: './reel-calculation.component.html',
  styleUrls: ['./reel-calculation.component.scss'],
})
export class ReelCalculationComponent implements OnInit, OnDestroy {
  @Input()
  public billToId: number;
  @Input()
  public currencyCode: string;
  @Input()
  public product;
  @Input()
  public calculateOnInit: boolean = false;

  @Output()
  public reelCalculated: EventEmitter<IReelPrice> = new EventEmitter<IReelPrice>();

  public subscription: Subscription = new Subscription();
  public reelPrice$: Observable<IReelPrice>;
  public reelPrice: IReelPrice;
  public calculating$: Observable<boolean>;
  public form: FormGroup;
  public error$: Observable<Error>;

  constructor(private formBuilder: FormBuilder, private store: Store<IAppState>, private router: Router) {
    this.reelPrice$ = this.store.pipe(select(getReelPrice));
    this.calculating$ = this.store.pipe(select(getReelLoadingStatus));
    this.error$ = this.store.pipe(select(getReelError));
  }

  ngOnInit() {
    this.setupForm();
    this.startSubscriptions();

    if (this.calculateOnInit) this.getReelPrice();

    this.store.dispatch(new OpenReelModal({ url: this.router.url }));
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeReelPrice());
    this.subscription.add(this.subscribeFormValueChanges());
  }

  private subscribeReelPrice(): Subscription {
    return this.reelPrice$.subscribe((reelPrice: IReelPrice) => {
      this.reelPrice = reelPrice;
      this.reelCalculated.emit(reelPrice);
    });
  }

  private subscribeFormValueChanges(): Subscription {
    return this.form.valueChanges.subscribe(val => {
      this.store.dispatch(new ClearReelPrice());
    });
  }

  public setupForm(): void {
    const itemsPerReel = this.product.reel ? this.product.reel.itemsPerReel : undefined;
    const numberOfReels = this.product.reel ? this.product.reel.numberOfReels : undefined;

    this.form = this.formBuilder.group({
      cpn: this.product.cpn,
      endCustomerId: this.product.endCustomerId,
      itemsPerReel: [itemsPerReel, Validators.compose([Validators.required, Validators.min(1), CustomValidators.integer])],
      numberOfReels: [
        numberOfReels,
        Validators.compose([Validators.required, CustomValidators.integer, Validators.pattern('[0-9]+$'), Validators.min(1)]),
      ],
    });
  }

  public onDropdownsSelectionChange(event: IPricePostData): void {
    this.form.patchValue({
      cpn: event.cpn,
      endCustomerId: event.endCustomerSiteId,
    });
    this.form.markAsDirty();
  }

  public getReelPrice(): void {
    /* istanbul ignore else */
    if (this.form.valid) {
      const { cpn, endCustomerId, itemsPerReel, numberOfReels } = this.form.controls;

      const reelRequest: IReelPriceRequest = {
        billToId: this.billToId,
        currency: this.currencyCode,
        itemId: this.product.itemId,
        noOfReels: parseInt(numberOfReels.value, 10),
        partsInReel: parseInt(itemsPerReel.value, 10),
        warehouseId: this.product.warehouseId,
        ...(cpn.value && { cpn: cpn.value }),
        ...(endCustomerId.value && { endCustomerSiteId: endCustomerId.value }),
      };

      this.store.dispatch(new RequestReelPrice(reelRequest));
    }
  }
}
