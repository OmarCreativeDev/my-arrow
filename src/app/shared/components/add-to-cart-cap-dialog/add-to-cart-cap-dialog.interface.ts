import { CartType } from '@app/shared/components/add-to-cart-dialog/add-to-cart-dialog.component';

export interface IAddToCartCapData {
  cap: number;
  cartType: CartType;
}
