import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { CartType } from '@app/shared/components/add-to-cart-dialog/add-to-cart-dialog.component';
import { IAddToCartCapData } from '@app/shared/components/add-to-cart-cap-dialog/add-to-cart-cap-dialog.interface';

@Component({
  selector: 'app-add-to-cart-cap-dialog',
  templateUrl: './add-to-cart-cap-dialog.component.html',
})
export class AddToCartCapDialogComponent {
  public readonly shouldRedirect: boolean = true;
  public addingToQuotes: boolean = true;

  constructor(
    public dialog: Dialog<AddToCartCapDialogComponent>,
    private router: Router,
    @Inject(APP_DIALOG_DATA) public data: IAddToCartCapData
  ) {
    this.addingToQuotes = this.data.cartType === CartType.QUOTE_CART;
  }

  public onClose(shouldRedirect: boolean = false): void {
    this.dialog.close();
    if (shouldRedirect) {
      this.router.navigateByUrl(this.addingToQuotes ? '/quotes' : '/cart');
    }
  }
}
