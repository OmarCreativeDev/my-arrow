import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { CartType } from '@app/shared/components/add-to-cart-dialog/add-to-cart-dialog.component';
import { AddToCartCapDialogComponent } from './add-to-cart-cap-dialog.component';

class MockRouter {
  navigateByUrl(url: string) {
    return url;
  }
}

const dialogData = {
  cap: 100,
  cartType: CartType.SHOPPING_CART,
};

describe('AddToCartCapDialogComponent', () => {
  let component: AddToCartCapDialogComponent;
  let fixture: ComponentFixture<AddToCartCapDialogComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [AddToCartCapDialogComponent],
      providers: [
        { provide: APP_DIALOG_DATA, useValue: { data: dialogData } },
        { provide: Dialog, useClass: DialogMock },
        { provide: Router, useClass: MockRouter },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToCartCapDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    router = TestBed.get(Router);
    spyOn(router, 'navigateByUrl').and.callThrough();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should navigate to '/cart' if cart type is ${CartType.SHOPPING_CART}`, () => {
    component.onClose(true);
    expect(router.navigateByUrl).toHaveBeenCalledWith('/cart');
  });

  it(`should navigate to '/quotes' if cart type is ${CartType.QUOTE_CART}`, () => {
    component.addingToQuotes = true;
    component.onClose(true);
    expect(router.navigateByUrl).toHaveBeenCalledWith('/quotes');
  });

  it('should close the modal without navigating if user cancels the dialog', () => {
    component.onClose();
    expect(router.navigateByUrl).not.toHaveBeenCalled();
  });
});
