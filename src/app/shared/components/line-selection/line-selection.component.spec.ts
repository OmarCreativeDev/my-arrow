import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineSelectionComponent } from './line-selection.component';

describe('LineSelectionComponent', () => {
  let component: LineSelectionComponent;
  let fixture: ComponentFixture<LineSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LineSelectionComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#allSelected', () => {
    it('should return false if items is empty', () => {
      // Missing Order
      component.items = [];
      const result1 = component.allSelected();
      expect(result1).toBeFalsy();
    });

    const testCases = [
      {
        description: 'should return true if all ids are selected',
        lineIds: ['1', '2', '3'],
        selectedIds: ['1', '2', '3'],
        result: true,
      },
      {
        description: 'should return false if all ids are not selected',
        lineIds: ['1', '2', '3'],
        selectedIds: ['1'],
        result: false,
      },
      {
        description: 'should return false if selected id is not found',
        lineIds: ['1'],
        selectedIds: ['100'],
        result: false,
      },
    ];

    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        spyOn(component, 'getAllLineIds').and.returnValue(testCase.lineIds);
        component.items = [{}];
        component.selectedIds = testCase.selectedIds;
        const result = component.allSelected();
        expect(result).toEqual(testCase.result);
      });
    }
  });

  describe('#isSelected', () => {
    const testCases = [
      {
        description: 'should return true if the specified orderLines is selected',
        item: { id: '1' },
        selectedIds: ['1'],
        result: true,
      },
      {
        description: 'should return true if the specified orderLines is selected',
        item: { id: '0' },
        selectedIds: ['1'],
        result: false,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        component.selectedIds = testCase.selectedIds;
        const result = component.isSelected(testCase.item.id);
        expect(result).toEqual(testCase.result);
      });
    }
  });

  describe('#getAllLineIds', () => {
    it('should return a map of the invoice numbers of all orderLines', () => {
      const mockItemId = 'mock-item-number';
      const expectedResult = [mockItemId];
      component.items = [{ id: mockItemId }];
      component.itemIndexKey = 'id';
      const result = component.getAllLineIds();
      expect(result).toEqual(expectedResult);
    });
    it('should return an empty array if no invoices have been loaded', () => {
      component.items = undefined;
      const result = component.getAllLineIds();
      expect(result).toEqual([]);
    });
  });

  describe('toggleLines', () => {
    it('#toggleLine should add/remove the invoiceNumber to the selectedIds', () => {
      const mockItemId = '1';
      const mockItem = { id: mockItemId };
      component.toggleLine(mockItem.id, true);
      expect(component.selectedIds).toContain(mockItemId);
      component.toggleLine(mockItem.id, false);
      expect(component.selectedIds).not.toContain(mockItemId);
    });

    it('#toggleLine should add/remove multiple invoiceNumber to the selectedIds', () => {
      const mockItemIds = ['1', '2'];
      spyOn(component, 'getAllLineIds').and.returnValue(mockItemIds);
      component.toggleAllLines(true);
      expect(component.selectedIds).toContain(mockItemIds[0]);
      expect(component.selectedIds).toContain(mockItemIds[1]);
      component.toggleAllLines(false);
      expect(component.selectedIds).not.toContain(mockItemIds[0]);
      expect(component.selectedIds).not.toContain(mockItemIds[1]);
    });

    it('#toggleLine should not add duplicates to the selectedIds', () => {
      const mockItemId = '1';
      component.selectedIds = [mockItemId];
      component.toggleLine(mockItemId, true);
      expect(component.selectedIds).toEqual([mockItemId]);
    });
  });

  describe('#getSelectedItems', () => {
    const includedItem1 = { id: '1' };
    const includedItem2 = { id: '2' };
    const excludedItem1 = { id: 'should-not-be-included' };
    const testCases = [
      {
        description: 'should return all items whose ids are selected',
        items: [includedItem1, includedItem2, excludedItem1],
        ids: ['1', '2'],
        result: [includedItem1, includedItem2],
      },
      {
        description: 'should return an empty array if no matching items found',
        items: [excludedItem1],
        ids: [],
        result: [],
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        component.items = testCase.items;
        const result = component.getSelectedItems(testCase.ids);
        expect(result).toEqual(testCase.result);
      });
    }
  });
});
