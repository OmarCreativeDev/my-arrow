import { Component, Input, EventEmitter, Output } from '@angular/core';
import { uniq, pullAll, difference } from 'lodash';

@Component({
  selector: 'app-line-selection',
  template: '<div></div>',
})
export class LineSelectionComponent {
  @Input()
  items: Array<any>;
  @Input()
  itemIndexKey: string = 'id';
  @Output()
  selectionChange = new EventEmitter<Array<string>>();

  public selectedIds = [];

  constructor() {
    this.toggleAllLines(true);
  }

  /**
   * Determines if all lines have been selected
   */
  public allSelected() {
    if (this.items && this.items.length) {
      const allLineIds = this.getAllLineIds();
      return difference(allLineIds, this.selectedIds).length <= 0;
    } else {
      return false;
    }
  }

  /**
   * Determines if an OrderLine is selected
   * @param orderLine the OrderLine to find out the selection status for
   */
  public isSelected(id: string): boolean {
    return this.selectedIds.includes(id);
  }

  /**
   * Toggles an OrderLine selection
   * @param orderLine the OrderLine to toggle
   * @param isChecked whether the OrderLine should be selected or unselected
   */
  public toggleLine(id: string, isChecked: boolean): void {
    this.toggleLineIds([id], isChecked);
  }

  /**
   * Toggles all OrderLines
   * @param isChecked whether to select or unselected all lines
   */
  public toggleAllLines(isChecked: boolean): void {
    this.toggleLineIds(this.getAllLineIds(), isChecked);
  }

  /**
   * Toggles all lines specified by Id
   * @param lineIds the linedIds to toggle
   * @param isChecked whether to select or unselect the specified lines
   */
  public toggleLineIds(lineIds: Array<string>, isChecked: boolean): void {
    if (isChecked) {
      this.selectedIds = [...this.selectedIds, ...lineIds];
    } else {
      this.selectedIds = [...pullAll(this.selectedIds, lineIds)];
    }
    this.selectedIds = uniq(this.selectedIds);
    this.selectionChange.emit(this.selectedIds);
  }

  public getSelectedItems(ids: Array<string>): Array<any> {
    return this.items.filter(item => {
      const itemIndexValue = this.getItemIndexValue(item);
      return ids.includes(itemIndexValue);
    });
  }

  protected getItemIndexValue(item: any): string {
    return item[this.itemIndexKey];
  }

  /**
   * Gets the Ids of all lines
   */
  public getAllLineIds(): Array<string> {
    if (this.items && this.items.length && this.itemIndexKey) {
      return this.items.map(item => item[this.itemIndexKey]);
    } else {
      return [];
    }
  }
}
