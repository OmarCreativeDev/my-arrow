import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';

import { ISelectableOption } from '@app/shared/shared.interfaces';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss'],
})
export class PaginationComponent implements OnInit {
  get currentPage(): number {
    return this._currentPage;
  }

  @Input()
  set currentPage(value: number) {
    this._currentPage = value;
    /* instabul ignore else */
    if (this.form) {
      this.form.controls['pageNumber'].setErrors(null);
    }
  }
  @Input()
  pageLimit: number;
  @Input()
  totalPages: number = 0;

  @Output()
  pageLimitChange = new EventEmitter<number>();
  @Output()
  pageChange = new EventEmitter<number>();

  private _currentPage: number;
  public form: FormGroup;

  public pageLimitOptions: Array<ISelectableOption<any>> = [
    { label: '10 Results', value: 10 },
    { label: '25 Results', value: 25 },
    { label: '50 Results', value: 50 },
  ];

  constructor(public formBuilder: FormBuilder) {}

  public ngOnInit(): void {
    this.form = this.formBuilder.group({
      pageNumber: [null, Validators.compose([Validators.pattern('^[0-9]+$'), Validators.min(1), this.maxValidator()])],
    });
  }

  public move(delta: number) {
    let newPage = this._currentPage + delta;
    if (newPage < 1) {
      newPage = 1;
    } else if (newPage > this.totalPages) {
      newPage = this.totalPages;
    }
    this.pageChange.emit(newPage);
  }

  public onPageLimitChange(limit: number) {
    this.pageLimitChange.emit(limit);
    // TODO: Removed Orders dependency on the `pageChange` Action, but Search still needs it. Once Search has been improved, this line can also be removed
    this.pageChange.emit(1);
  }

  public onSubmit() {
    const page = parseInt(this.form.controls.pageNumber.value, 10);
    if (this.form.valid && !isNaN(page) && page <= this.totalPages) {
      this.jumpTo(page);
    }

    return false;
  }

  public jumpTo(page: number) {
    this.pageChange.emit(page);
  }

  maxValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const pass = control.value <= this.totalPages;
      return pass ? null : { exceedsTotalPages: { value: control.value } };
    };
  }
}
