import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';

import { BackdropServiceMock } from '@app/app.component.spec';
import { DropdownComponent } from '@app/shared/components/dropdown/dropdown.component';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { PaginationComponent } from './pagination.component';

describe('PaginationComponent', () => {
  let component: PaginationComponent;
  let fixture: ComponentFixture<PaginationComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [PaginationComponent, DropdownComponent],
        imports: [
          ReactiveFormsModule
        ],
        providers: [
          { provide: BackdropService, useClass: BackdropServiceMock }
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  const testCases = [
    {
      description:
        '#move should emit an event with a page number affected by a positive delta',
      current: 10,
      total: 50,
      delta: 1,
      expected: 11
    },
    {
      description:
        '#move should emit an event with a page number affected by a negative delta',
      current: 10,
      total: 50,
      delta: -1,
      expected: 9
    },
    {
      description:
        '#move should emit an event with a page number affected by a negative delta',
      current: 1,
      total: 50,
      delta: -1,
      expected: 1
    },
    {
      description:
        '#move should emit an event with a page number affected by a positive delta',
      current: 50,
      total: 50,
      delta: 1,
      expected: 50
    }
  ];

  for (const testCase of testCases) {
    it(`${testCase.description}`, () => {
      spyOn(component.pageChange, 'emit');
      component.currentPage = testCase.current;
      component.totalPages = testCase.total;
      component.move(testCase.delta);
      // These values shouldn't have changed
      expect(component.currentPage).toEqual(testCase.current);
      expect(component.totalPages).toEqual(testCase.total);
      // Now ensure that the pageChange was emitted
      expect(component.pageChange.emit).toHaveBeenCalledWith(testCase.expected);
    });
  }

  it('should call `pageChange()` with 1 and `pageLimitChange`() on `onPageLimitChange()`', () => {
    spyOn(component.pageChange, 'emit');
    spyOn(component.pageLimitChange, 'emit');

    component.onPageLimitChange(5);
    expect(component.pageChange.emit).toHaveBeenCalledWith(1);
    expect(component.pageLimitChange.emit).toHaveBeenCalledWith(5);
  });

  it('should call `pageChange()` on `onSubmit()` when `page` is a numeric value', () => {
    spyOn(component.pageChange, 'emit');
    component.currentPage = 1;
    component.totalPages = 10;

    component.form.controls['pageNumber'].setValue('5');
    component.onSubmit();
    expect(component.pageChange.emit).toHaveBeenCalledWith(5);

    component.form.controls['pageNumber'].setValue('1');
    component.onSubmit();
    expect(component.pageChange.emit).toHaveBeenCalledWith(1);

    component.form.controls['pageNumber'].setValue(10);
    component.onSubmit();
    expect(component.pageChange.emit).toHaveBeenCalledWith(10);
  });

  it('should not call `pageChange()` on `onSubmit()` when `page` is above `totalPages`', () => {
    spyOn(component.pageChange, 'emit');
    component.currentPage = 1;
    component.totalPages = 10;

    component.form.controls['pageNumber'].setValue('11');
    component.onSubmit();
    expect(component.pageChange.emit).not.toHaveBeenCalled();
  });

  it('should not call `pageChange()` on `onSubmit()` when `page` is below 1', () => {
    spyOn(component.pageChange, 'emit');
    component.currentPage = 1;
    component.totalPages = 10;

    component.form.controls['pageNumber'].setValue('0');
    component.onSubmit();
    expect(component.pageChange.emit).not.toHaveBeenCalled();

    component.form.controls['pageNumber'].setValue('-1');
    component.onSubmit();
    expect(component.pageChange.emit).not.toHaveBeenCalled();
  });

  it('should not call `pageChange()` on `onSubmit()` when `page` not a number', () => {
    spyOn(component.pageChange, 'emit');
    component.currentPage = 1;
    component.totalPages = 10;

    component.form.controls['pageNumber'].setValue('Hello');
    component.onSubmit();
    expect(component.pageChange.emit).not.toHaveBeenCalled();

    component.form.controls['pageNumber'].setValue('');
    component.onSubmit();
    expect(component.pageChange.emit).not.toHaveBeenCalled();

    component.form.controls['pageNumber'].setValue(' ');
    component.onSubmit();
    expect(component.pageChange.emit).not.toHaveBeenCalled();

    component.form.controls['pageNumber'].setValue('.0');
    component.onSubmit();
    expect(component.pageChange.emit).not.toHaveBeenCalled();
  });
});
