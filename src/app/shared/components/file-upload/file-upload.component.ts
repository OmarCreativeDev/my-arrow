import { Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';

let nextUniqueId = 0;

@Component({
  selector: 'app-file-upload-component',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
})
export class FileUploadComponent {
  private _file: File = null;
  readonly inputId = `file-upload__control-${++nextUniqueId}`;

  @Input()
  acceptedFileExtensions: string;

  @Output()
  actionButtonClick: EventEmitter<any> = new EventEmitter<any>();

  @ViewChild('fileElement')
  fileElement: ElementRef;

  get isFileReady(): boolean {
    return this._file !== null;
  }

  get fileTitle(): string {
    return this._file ? this._file.name : '';
  }

  onFileChange(event: any) {
    const target = event.target || event.srcElement;
    const files = target.files;

    /* istanbul ignore else */
    if (files.length > 1) {
      throw new Error('Cannot use multiple files');
    }

    const file = files[0];

    /* istanbul ignore else */
    if (file) {
      this._file = file;
    }
  }

  sendFile() {
    this.actionButtonClick.emit(this._file);
    this._file = null;

    /* istanbul ignore else */
    if (this.fileElement) {
      this.fileElement.nativeElement.value = '';
    }
  }
}
