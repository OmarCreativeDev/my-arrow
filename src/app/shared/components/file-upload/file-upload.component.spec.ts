import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FileUploadComponent } from './file-upload.component';

describe('FileUploadComponent', () => {
  let component: FileUploadComponent;
  let fixture: ComponentFixture<FileUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FileUploadComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FileUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('file title should change when a file is selected', () => {
    expect(component.fileTitle).toBe('');
    component.onFileChange({target: {files: [{name: 'a-csv-file.csv'}]} });
    expect(component.fileTitle).toBe('a-csv-file.csv');
  });

  it('should detect that there\'s a file selected', () => {
    expect(component.isFileReady).toBeFalsy();
    component.onFileChange({target: {files: [{name: 'a-csv-file.csv'}]} });
    expect(component.isFileReady).toBeTruthy();
  });

  it('should send the selected file', () => {
    spyOn(component.actionButtonClick, 'emit');
    const file = {name: 'a-csv-file.csv'};
    component.onFileChange({target: {files: [file]} });
    component.sendFile();
    expect(component.actionButtonClick.emit).toHaveBeenCalledWith(file);
  });
});
