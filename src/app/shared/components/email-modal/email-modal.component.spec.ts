import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { of, throwError } from 'rxjs';
import { HttpClient, HttpHandler, HttpErrorResponse } from '@angular/common/http';

import { AuthTokenService } from '@app/core/auth/auth-token.service';
import { ApiService } from '@app/core/api/api.service';
import { EmailService } from '@app/core/email/email.service';
import { ISelectableOption } from '@app/shared/shared.interfaces';
import { EmailModalComponent, DialogData } from './email-modal.component';
import { DialogMock } from '@app/core/dialog/dialog.service.spec';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { Store } from '@ngrx/store';

class StoreMock {
  select() {
    return of([]);
  }
  dispatch() {}
  pipe() {
    return of();
  }
}

class EmailServiceMock {
  sendTemplatedEmail() {}
  sendEmail() {}
}

describe('EmailModalComponent', () => {
  let component: EmailModalComponent;
  let fixture: ComponentFixture<EmailModalComponent>;
  let emailService: EmailService;

  const filterOptions: Array<ISelectableOption<string>> = [
    {
      label: 'email@arrow.com',
      value: 'email@arrow.com',
    },
  ];

  const header = 'Email Your Sales Representative';
  const headerDescription =
    'This e-mail will be sent to your Sales and Marketing Rep. Please provide additional details in the space provided. The "From" email address will receive a copy of the final email as well.';
  const fromEmail = 'test@test.com';
  const recipients = filterOptions;
  const payload = {
    orderLines: [],
  };
  const subject = 'testing email';

  const data: DialogData = { header, headerDescription, fromEmail, recipients, subject, payload };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmailModalComponent],
      providers: [
        ApiService,
        HttpClient,
        HttpHandler,
        AuthTokenService,
        { provide: Dialog, useClass: DialogMock },
        { provide: APP_DIALOG_DATA, useValue: data },
        { provide: Store, useClass: StoreMock },
        { provide: EmailService, useClass: EmailServiceMock },
      ],
      imports: [ReactiveFormsModule],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailModalComponent);
    component = fixture.componentInstance;
    emailService = TestBed.get(EmailService);
    component.header = data.header;
    component.headerDescription = data.headerDescription;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call addRecipient', () => {
    spyOn(component, 'addRecipient').and.callThrough();
    spyOn(component, 'addItem').and.callThrough();
    component.filterOptions = [{ value: 'dummy-value', label: 'dummy-label' }];
    component.addItem();
    expect(component.recipients.controls.length).toEqual(1);
    expect(component.addItem).toHaveBeenCalled();
    expect(component.addRecipient).toHaveBeenCalled();
  });

  it('should call sendEmailMessage', () => {
    spyOn(emailService, 'sendEmail').and.returnValue(of({}));
    spyOn(component, 'sendEmailMessage').and.callThrough();
    spyOn(component, 'validateEmailForm').and.callThrough();
    spyOn(component, 'dismiss').and.callThrough();
    spyOn(component, 'addItem').and.callThrough();
    component.filterOptions = [{ value: 'dummy-value', label: 'dummy-label' }];
    component.addItem();
    component.emailForm.patchValue({ message: 'test message' });
    component.sendEmailMessage();
    expect(component.sendEmailMessage).toHaveBeenCalled();
    expect(component.validateEmailForm).toHaveBeenCalled();
    expect(component.dismiss).toHaveBeenCalled();
  });

  it('should call removeRecipient', () => {
    spyOn(component, 'addItem').and.callThrough();
    spyOn(component, 'removeRecipient').and.callThrough();
    component.filterOptions = [{ value: 'dummy-value', label: 'dummy-label' }];
    component.addItem();
    component.removeRecipient(0);
    expect(component.recipients.controls.length).toEqual(0);
    expect(component.removeRecipient).toHaveBeenCalled();
  });

  describe('#sendEmailMessage', () => {
    const mockRecipients = ['recipient@example.com'];
    const mockSender = 'sender@example.com';
    const mockSubject = 'Dummy subject';
    const mockContent = 'Lorem ipsum';
    const mockPayload = { message: mockContent, region: 'NA' };
    const mockTemplateName = 'basic';
    const mockSalutation = 'hello';
    const mockFirstname = 'John';

    beforeEach(() => {
      component.fromEmail = mockSender;
      component.subject = mockSubject;
      component.message.setValue(mockContent);
      spyOn(component, 'getRecipients').and.callFake(() => mockRecipients);
    });

    it('should not send an email if the form is invalid', () => {
      const sendEmailSpy = spyOn(emailService, 'sendEmail');
      const sendTemplatedEmailSpy = spyOn(emailService, 'sendTemplatedEmail');
      spyOn(component, 'validateEmailForm').and.callFake(() => false);
      component.sendEmailMessage();
      expect(sendEmailSpy).not.toHaveBeenCalled();
      expect(sendTemplatedEmailSpy).not.toHaveBeenCalled();
    });

    it('should send a plain text email with the email data if no template is specified', () => {
      const sendEmailSpy = spyOn(emailService, 'sendEmail').and.callFake(() => of([]));
      spyOn(component, 'validateEmailForm').and.callFake(() => true);
      component.templateName = undefined;
      component.sendEmailMessage();
      expect(sendEmailSpy).toHaveBeenCalledWith({
        to: mockRecipients,
        from: mockSender,
        subject: mockSubject,
        content: mockContent,
      });
    });

    xit('should send a cc copy to the user if `ccSelf` is `true`', () => {
      const sendEmailSpy = spyOn(emailService, 'sendEmail').and.callFake(() => of([]));
      spyOn(component, 'validateEmailForm').and.callFake(() => true);
      component.templateName = undefined;
      component.sendEmailMessage();
      expect(sendEmailSpy).toHaveBeenCalledWith({
        to: mockRecipients,
        from: mockSender,
        subject: mockSubject,
        content: mockContent,
        cc: [mockSender],
      });
    });

    it('should attach the message to the payload that is sent to the API', () => {
      const templateName = 'basic';
      const expectedPayload = { ...{ foo: 'bar' }, ...mockPayload };
      const sendTemplatedEmailSpy = spyOn(emailService, 'sendTemplatedEmail').and.callFake(() => of({}));
      spyOn(component, 'validateEmailForm').and.callFake(() => true);
      component.templateName = templateName;
      component.data.payload = expectedPayload;
      component.sendEmailMessage();
      expect(sendTemplatedEmailSpy).toHaveBeenCalledWith(
        {
          to: mockRecipients,
          from: mockSender,
          subject: mockSubject,
          payload: expectedPayload,
        },
        mockTemplateName
      );
    });

    it('should attach the `salutation` and `firstname` values to the request if provided before sending to the API', () => {
      const sendTemplatedEmailSpy = spyOn(emailService, 'sendTemplatedEmail').and.callFake(() => of({}));
      spyOn(component, 'validateEmailForm').and.callFake(() => true);
      component.templateName = mockTemplateName;
      component.data.payload = undefined;
      component.templateSalutation = mockSalutation;
      component.templateFirstName = mockFirstname;
      component.sendEmailMessage();
      expect(sendTemplatedEmailSpy).toHaveBeenCalledWith(
        {
          to: mockRecipients,
          from: mockSender,
          subject: mockSubject,
          salutation: mockSalutation,
          firstname: mockFirstname,
          payload: mockPayload,
        },
        mockTemplateName
      );
    });

    it('should closed the modal if a message was sent successfully', () => {
      const dismissSpy = spyOn(component, 'dismiss').and.callFake(() => {});
      spyOn(component, 'validateEmailForm').and.callFake(() => true);
      spyOn(emailService, 'sendEmail').and.callFake(() => of({}));
      component.sendEmailMessage();
      expect(dismissSpy).toHaveBeenCalled();
    });

    it('should catch any error resulting from sending a message and store the error', () => {
      const expectedResponse = new HttpErrorResponse({ error: new Error('mock error') });
      spyOn(component, 'validateEmailForm').and.callFake(() => true);
      spyOn(emailService, 'sendEmail').and.callFake(() => throwError(expectedResponse));
      component.sendEmailMessage();
      expect(component.error).toBe(expectedResponse);
    });
  });
});
