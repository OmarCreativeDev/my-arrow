import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailOrdersComponent } from './email-orders.component';
import { IOrderLineStatus } from '@app/shared/shared.interfaces';

describe('EmailOrdersComponent', () => {
  let component: EmailOrdersComponent;
  let fixture: ComponentFixture<EmailOrdersComponent>;
  const mockOrderLines = [
    {
      buyerName: 'Tracie D.',
      carrier: 'FEDEX',
      committed: new Date('2018-07-30'),
      customerPartNumber: 'BAV99',
      entered: new Date('2018-07-24'),
      extResale: 1745,
      id: 1,
      invoiced: new Date('2018-01-21'),
      lineItem: '1.2.1',
      manufacturerName: 'Vishay',
      manufacturerPartNumber: 'SMC8903124',
      purchaseOrderNumber: 'PO234098',
      qtyOrdered: 3,
      qtyReadyToShip: 1,
      qtyRemaining: 850,
      qtyShipped: 2,
      requested: new Date('2018-07-21'),
      salesOrderId: '5952303',
      shipmentTrackingDate: new Date('2018-01-01'),
      shipmentTrackingReference: 'UPS190321-GB',
      status: <IOrderLineStatus>'OPEN',
      unitResale: 0,
      shipmentTrackingUrl: 'http://www.fedex.com/123',
    },
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmailOrdersComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailOrdersComponent);
    component = fixture.componentInstance;
    component.orderLines = mockOrderLines;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
