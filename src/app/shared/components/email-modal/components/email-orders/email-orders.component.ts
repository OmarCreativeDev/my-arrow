import { Component, Input } from '@angular/core';

import { Status } from '@app/shared/locale/i18n.maps';

@Component({
  selector: 'app-email-orders',
  templateUrl: './email-orders.component.html',
  styleUrls: ['./email-orders.component.scss'],
})
export class EmailOrdersComponent {
  @Input()
  orderLines: any;
  public orderLineStatusMap: any = Status.orderLineStatusMap;
}
