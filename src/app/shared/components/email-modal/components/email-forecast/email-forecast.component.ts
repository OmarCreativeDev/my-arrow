import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-email-forecast',
  templateUrl: './email-forecast.component.html',
  styleUrls: ['./email-forecast.component.scss'],
})
export class EmailForecastComponent {
  @Input()
  forecastLines: any;
}
