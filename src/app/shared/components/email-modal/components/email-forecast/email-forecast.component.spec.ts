import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailForecastComponent } from './email-forecast.component';

describe('EmailForecastComponent', () => {
  let component: EmailForecastComponent;
  let fixture: ComponentFixture<EmailForecastComponent>;

  const forecastLines = [
    {
      custItemId: '26-1016-0006-7',
      mfrItemId: 'OP184ESZ-REEL7',
      mfrName: 'ADI',
      quantityPublic: 0,
      mult: 1000,
      selected: false,
    },
    {
      custItemId: '26-1014-7590-8',
      mfrItemId: 'AD8402ARUZ10',
      mfrName: 'ADI',
      quantityPublic: 660,
      mult: 192,
      selected: false,
    },
    {
      custItemId: '26-1014-7590-9',
      mfrItemId: 'AD8402ARDRTY',
      mfrName: 'ADI',
      quantityPublic: 660,
      mult: 224,
      selected: false,
    },
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmailForecastComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailForecastComponent);
    component = fixture.componentInstance;
    component.forecastLines = forecastLines;
    fixture.detectChanges();
  });

  it('should create component', () => {
    expect(component).toBeTruthy();
  });
});
