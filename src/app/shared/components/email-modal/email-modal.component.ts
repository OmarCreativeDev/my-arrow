import { IForecastPartList } from '@app/core/forecast/forecast.interfaces';
import { Component, Inject, OnInit, OnDestroy } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';

import { ISelectableOption } from '@app/shared/shared.interfaces';
import { APP_DIALOG_DATA, Dialog } from '@app/core/dialog/dialog.service';
import { IOrderLine, IAppState } from '@app/shared/shared.interfaces';
import { EmailService } from '@app/core/email/email.service';
import { IEmail, EmailTemplates } from '@app/core/email/email.interfaces';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Store, select } from '@ngrx/store';
import { getSalesRepEmails, getUserEmail, getRegion } from '@app/core/user/store/user.selectors';
import { EmailRepresentative } from '@app/features/orders/stores/orders/orders.actions';
import { take } from 'rxjs/operators';

export interface DialogData {
  header: string;
  headerDescription: string;
  fromEmail: string;
  recipients: Array<ISelectableOption<string>>;
  ccSelf?: boolean;
  orderLines?: Array<IOrderLine>;
  receivedDate?: string;
  forecastPartList?: Array<IForecastPartList>;
  subject: string;
  templateName?: EmailTemplates; // set to use templated email
  templateFirstName?: string;
  templateSalutation?: string;
  payload?: any;
}

export const DEFAULT_MODAL_TITLE = 'Email Your Sales Representative';
export const DEFAULT_MODAL_INTRO =
  'This e-mail will be sent to your Sales and Marketing Rep. Please provide additional details in the space provided. The "From" email address will receive a copy of the final email as well.';

@Component({
  selector: 'app-email-modal',
  templateUrl: './email-modal.component.html',
  styleUrls: ['./email-modal.component.scss'],
})
export class EmailModalComponent implements OnInit, OnDestroy {
  public emailForm: FormGroup;
  public emailItems: FormArray;
  public orderLines: Array<IOrderLine>;
  public receivedDate: string;
  public forecastPartList: Array<IForecastPartList>;
  public salesRep: string;
  public salesPhone: string;
  public salesMail: string;
  public filterOptions: Array<ISelectableOption<string>>;
  public recipients: FormArray;
  public ccRecipients: Array<string> = [];
  public ccSelf: boolean = false;
  public message = new FormControl('', Validators.required);
  public fromEmail: string;
  public header: string;
  public headerDescription: string;
  public emailData: IEmail;
  public subject: string;
  public templateName: EmailTemplates;
  public templateFirstName: string;
  public templateSalutation: string;
  public error: HttpErrorResponse;
  public sending = false;

  public region$: Observable<string>;
  public region: string;

  public userEmail$: Observable<string>;
  public salesRepEmails$: Observable<string[]>;

  private subscription: Subscription = new Subscription();

  constructor(
    private dialog: Dialog<EmailModalComponent>,
    private emailService: EmailService,
    private formBuilder: FormBuilder,
    private store: Store<IAppState>,
    @Inject(APP_DIALOG_DATA) public data?: DialogData
  ) {
    this.orderLines = this.data.payload.orderLines;
    this.receivedDate = this.data.payload.receivedDate;
    this.forecastPartList = this.data.payload.forecastPartList;
    this.salesRep = this.data.payload.salesRep;
    this.salesPhone = this.data.payload.receivedDate;
    this.salesMail = this.data.payload.salesMail;
    this.header = this.data.header;
    this.headerDescription = this.data.headerDescription;
    this.subject = this.data.subject;
    this.templateName = this.data.templateName;
    this.templateFirstName = this.data.templateFirstName;
    this.templateSalutation = this.data.templateSalutation;
    this.ccSelf = this.data.ccSelf ? this.data.ccSelf : this.ccSelf;

    this.region$ = store.pipe(select(getRegion));
    this.userEmail$ = store.pipe(select(getUserEmail));
    this.salesRepEmails$ = store.pipe(select(getSalesRepEmails));
  }

  ngOnInit(): void {
    this.startSubscriptions();

    this.emailForm = new FormGroup({
      message: this.message,
      recipients: this.formBuilder.array([], Validators.required),
    });
  }

  ngOnDestroy() {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.regionSubscription());
    this.subscription.add(this.userEmailSubscription());
    this.subscription.add(this.salesRepEmailsSubscription());
  }

  private regionSubscription(): Subscription {
    return this.region$.subscribe(region => this.setRegion(region));
  }

  private userEmailSubscription(): Subscription {
    return this.userEmail$.pipe(take(1)).subscribe(userEmails => this.setUserEmail(userEmails));
  }

  private salesRepEmailsSubscription(): Subscription {
    return this.salesRepEmails$.pipe(take(1)).subscribe(salesRepEmails => this.setSalesRepEmails(salesRepEmails));
  }

  public setRegion(region: string): void {
    this.region = region;
  }

  public setUserEmail(userEmails: string): void {
    this.fromEmail = userEmails;
    if (this.ccSelf) {
      this.ccRecipients.push(userEmails);
    }
  }

  public setSalesRepEmails(salesRepEmails: Array<string>): void {
    this.filterOptions = salesRepEmails.map(salesRepEmail => {
      return {
        value: salesRepEmail,
        label: salesRepEmail,
      };
    });
  }

  public dismiss(): void {
    this.dialog.close();
  }

  public addRecipient(): FormControl {
    return new FormControl(this.filterOptions[0].label, Validators.required);
  }

  addItem(): void {
    this.recipients = this.emailForm.get('recipients') as FormArray;
    this.recipients.push(this.addRecipient());
  }

  public removeRecipient(i): void {
    this.recipients = this.emailForm.get('recipients') as FormArray;
    this.recipients.controls.splice(i, 1);
  }

  validateEmailForm() {
    const formGroup = this.emailForm;
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      control.markAsTouched({ onlySelf: true });
    });
    return formGroup.valid;
  }

  getRecipients() {
    const recipients: Array<string> = [];
    this.recipients.controls.forEach(function(recipient) {
      recipients.push(recipient.value);
    });
    return recipients;
  }

  getEmailData(): void {
    this.emailData = {
      to: this.getRecipients(),
      from: this.fromEmail,
      subject: this.subject,
    };
    if (this.ccRecipients.length) this.emailData.cc = this.ccRecipients;
  }

  public sendEmailMessage(): Subscription {
    const payloadRegion = this.region === 'arrowne' || this.region === 'arrowce' || this.region === 'arrowse' ? 'EMEA' : 'NA';

    /* istanbul ignore else */
    if (this.validateEmailForm()) {
      this.error = undefined;
      let observableRequest: Observable<HttpResponse<any>>;
      this.getEmailData();
      if (this.templateName) {
        this.emailData.payload = {
          message: this.message.value,
          region: payloadRegion,
        };
        // TODO: Consolidate these properties into a single payload
        if (this.templateSalutation && this.templateFirstName) {
          this.emailData.salutation = this.templateSalutation;
          this.emailData.firstname = this.templateFirstName;
        }
        if (this.data.payload) {
          this.emailData.payload = { ...this.emailData.payload, ...this.data.payload };
        }
        observableRequest = this.emailService.sendTemplatedEmail(this.emailData, this.templateName);
      } else {
        this.emailData.content = this.message.value;
        observableRequest = this.emailService.sendEmail(this.emailData);
      }
      this.sending = true;
      return observableRequest.subscribe(
        () => {
          this.onEmailSuccess();
        },
        err => {
          this.sending = false;
          this.error = err;
        }
      );
    }
  }

  public onEmailSuccess() {
    if (this.orderLines) {
      const ordersId = this.orderLines.map(order => order.purchaseOrderNumber);
      this.store.dispatch(new EmailRepresentative(ordersId));
    }
    this.sending = false;
    this.dismiss();
  }
}
