import { Component, HostBinding, OnChanges, Input, ElementRef } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
/**
 * Inspiration taken from: https://plnkr.co/edit/MPdoKCxS3rW3t7BIrYR6?p=preview
 * and: https://stackoverflow.com/questions/47315256/angular-animation-for-dynamically-changing-height
 */

@Component({
  selector: 'app-animated-height',
  template: `<ng-content></ng-content>`,
  styles: [
    `
      :host {
        display: block;
        overflow: hidden;
      }
    `,
  ],
  animations: [
    trigger('resize', [
      transition(':enter', [style({ height: 0 }), animate('1000ms cubic-bezier(0.19, 1, 0.22, 1)', style({ height: '*' }))]),
      transition(':leave', [style({ height: '*' }), animate('1000ms cubic-bezier(0.19, 1, 0.22, 1)', style({ height: 0 }))]),
      transition(
        '* <=> *',
        [style({ height: '{{startHeight}}px' }), animate('1000ms cubic-bezier(0.19, 1, 0.22, 1)', style({ height: '*' }))],
        { params: { startHeight: 0 } }
      ),
    ]),
  ],
})
export class AnimatedHeightComponent implements OnChanges {
  @Input()
  state: string;
  public startHeight: number = 0;

  @HostBinding('@resize')
  get resize() {
    return { value: this.state, params: { startHeight: this.startHeight } };
  }

  constructor(private element: ElementRef) {}

  ngOnChanges(changes) {
    this.startHeight = this.element.nativeElement.clientHeight;
  }
}
