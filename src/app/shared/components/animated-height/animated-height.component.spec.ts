import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimatedHeightComponent } from './animated-height.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('AnimatedHeightComponent', () => {
  let component: AnimatedHeightComponent;
  let fixture: ComponentFixture<AnimatedHeightComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        imports: [BrowserAnimationsModule],
        declarations: [AnimatedHeightComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimatedHeightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
