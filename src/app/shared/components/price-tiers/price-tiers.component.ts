import { Component, Input } from '@angular/core';
import { IPriceTier } from './price-tiers.interface';

@Component({
  selector: 'app-price-tiers',
  templateUrl: './price-tiers.component.html',
  styleUrls: ['./price-tiers.component.scss'],
})
export class PriceTiersComponent {
  @Input() priceTiers: Array<IPriceTier>;
  @Input() quantity: number;
  @Input() currencyCode?: String;

  public paintColumns(quantity, quantityFrom, quantityTo) {
    return quantity >= quantityFrom && (quantity <= quantityTo || quantityTo === null);
  }
}
