import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PriceTiersComponent } from '@app/shared/components/price-tiers/price-tiers.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CurrencyPipe } from '@angular/common';
import { FormattedPricePipe } from '@app/shared/pipes/formatted-price/formatted-price.pipe';

describe('PriceTiersComponent', () => {
  let component: PriceTiersComponent;
  let fixture: ComponentFixture<PriceTiersComponent>;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [PriceTiersComponent, FormattedPricePipe],
        providers: [CurrencyPipe],
        schemas: [CUSTOM_ELEMENTS_SCHEMA],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(PriceTiersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should return true if quantity is greater than quantityFrom and lower than quantityTo', () => {
    const result = component.paintColumns(3, 1, 4);
    expect(result).toBeTruthy();
  });

  it('should return true if quantity is greater than quantityFrom and quantityTo equals null', () => {
    const result = component.paintColumns(3, 1, null);
    expect(result).toBeTruthy();
  });

  it('should return false if quantity is lower than quantityFrom and greater than quantityTo', () => {
    const result = component.paintColumns(3, 4, 1);
    expect(result).toBeFalsy();
  });
});
