export interface IPriceTier {
  quantityFrom: number;
  quantityTo: number;
  pricePerItem: number;
}
