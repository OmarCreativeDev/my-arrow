import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { PasswordInformationBoxComponent } from './password-information-box.component';

describe('PasswordInformationBoxComponent', () => {
  let component: PasswordInformationBoxComponent;
  let fixture: ComponentFixture<PasswordInformationBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PasswordInformationBoxComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasswordInformationBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('#isPasswordValidOn()', () => {
    const testCases = [
      {
        text: 'asdf',
        check: 'length',
        expected: 'error',
      },
      {
        text: 'sasdfasdf',
        check: 'number',
        expected: 'error',
      },
      {
        text: 'sasdfasdf',
        check: 'uppercase',
        expected: 'error',
      },
      {
        text: 'KJHKJHGKJH',
        check: 'lowercase',
        expected: 'error',
      },
      {
        text: 'QWEqwe1234',
        check: 'lowercase',
        expected: 'ok',
      },
      {
        text: 'QWEqwe1234',
        check: 'uppercase',
        expected: 'ok',
      },
      {
        text: 'QWEqwe1234',
        check: 'length',
        expected: 'ok',
      },
      {
        text: 'QWEqwe1234',
        check: 'number',
        expected: 'ok',
      },
    ];
    testCases.forEach(test => {
      it(`should return '${test.expected}' for '${test.text}' when checking  '${test.check}'`, () => {
        component.value = test.text;
        expect(component.isPasswordValidOn(test.check)).toBe(test.expected);
      });
    });
  });
});
