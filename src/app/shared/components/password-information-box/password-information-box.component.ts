import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-password-information-box',
  templateUrl: './password-information-box.component.html',
  styleUrls: ['./password-information-box.scss'],
})
export class PasswordInformationBoxComponent {
  @Input()
  value: string;

  public isPasswordValidOn(toCheck: string): string {
    if (!this.value || this.value.length === 0) return 'empty';
    switch (toCheck) {
      case 'length': {
        return this.value.length >= 8 && this.value.length <= 25 ? 'ok' : 'error';
      }
      case 'lowercase': {
        return /[a-z]/g.test(this.value) ? 'ok' : 'error';
      }
      case 'uppercase': {
        return /[A-Z]/g.test(this.value) ? 'ok' : 'error';
      }
      case 'number': {
        return /[0-9]/g.test(this.value) ? 'ok' : 'error';
      }
    }
  }
}
