import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-icon',
  template: `
    <app-svg-icon src="/assets/icons/{{symbol}}.svg"
                  class="svg-icon"></app-svg-icon>`,
  styleUrls: ['./icon.component.scss'],
})
export class IconComponent {
  @Input() symbol: string;
}
