import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class BackdropService {
  public isVisible: BehaviorSubject<any> = new BehaviorSubject<any>(false);

  public show(): void {
    this.isVisible.next(true);
  }

  public hide(): void {
    this.isVisible.next(false);
  }
}
