import { Injectable } from '@angular/core';

@Injectable()
export class BrowserService {
  constructor() {}

  /**
   * Calculates if the current browser supports the `msSaveOrOpenBlob` method (IE only)
   */
  public canTriggerIEDownload(_window: Window): boolean {
    return _window && _window.navigator && _window.navigator.msSaveOrOpenBlob !== undefined;
  }
}
