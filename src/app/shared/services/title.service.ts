import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';

const APP_TITLE = 'MyArrow';
const SEPERATOR = ' - ';

@Injectable()
export class TitleService {
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private titleService: Title) {}

  public init() {
    this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => this.activatedRoute),
        map(route => route.root)
      )
      .subscribe(root => {
        const breadcrumbs = this.buildBreadCrumb(root);
        const title = breadcrumbs.length ? `${APP_TITLE}${SEPERATOR}${breadcrumbs.join(SEPERATOR)}` : APP_TITLE;
        this.titleService.setTitle(title);
      });
  }

  public buildBreadCrumb(route: ActivatedRoute, breadcrumbs: Array<string> = []) {
    if (route.snapshot.data.title) {
      breadcrumbs = [...breadcrumbs, route.snapshot.data.title];
    }

    if (route.firstChild) {
      return this.buildBreadCrumb(route.firstChild, breadcrumbs);
    }

    return breadcrumbs;
  }
}
