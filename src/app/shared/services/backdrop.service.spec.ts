import { TestBed } from '@angular/core/testing';

import { BackdropService } from './backdrop.service';

describe('BackdropService', () => {
  let backdropService: BackdropService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BackdropService],
    });
    backdropService = TestBed.get(BackdropService);
  });

  it('should be created', () => {
    expect(backdropService).toBeTruthy();
  });

  it('should set isVisible true on show()', () => {
    backdropService.show();
    expect(backdropService.isVisible.value).toBeTruthy();
  });

  it('should set isVisible false on hide()', () => {
    backdropService.hide();
    expect(backdropService.isVisible.value).toBeFalsy();
  });
});
