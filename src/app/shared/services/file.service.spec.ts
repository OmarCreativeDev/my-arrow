import { TestBed, inject } from '@angular/core/testing';

import { FileService } from './file.service';
import { ApiService } from '@app/core/api/api.service';
import { of, throwError } from 'rxjs';
import { BrowserService } from '@app/shared/services/browser.service';
import { HttpResponse } from '@angular/common/http';

class ApiServiceMock {
  get() {}
}

const fakeWindow = {
  document: {
    title: '',
    body: {
      style: {},
      appendChild: () => {},
    },
    createElement: () => ({}),
  },
  close: () => {},
};

describe('FileService', () => {
  let fileService: FileService;
  let apiService: ApiService;
  let browserService: BrowserService;

  beforeAll(() => {
    spyOn(window, 'open').and.callFake(() => {
      return fakeWindow;
    });
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FileService, BrowserService, { provide: ApiService, useClass: ApiServiceMock }],
    });
    fileService = TestBed.get(FileService);
    apiService = TestBed.get(ApiService);
    browserService = TestBed.get(BrowserService);
  });

  it('should be created', inject([FileService], (service: FileService) => {
    expect(service).toBeTruthy();
  }));

  describe('#getAndOpenFile', () => {
    it('should open a window and return the instance', () => {
      const getSpy = spyOn(apiService, 'get').and.callFake(() => {
        return of(
          new HttpResponse({
            body: { pdf: '' },
          })
        );
      });
      const dummyUrl = 'dummy url';
      const dummyTitle = 'dummy title';
      fileService.getAndOpenFile(dummyUrl, dummyTitle);
      expect(getSpy).toHaveBeenCalledWith(dummyUrl, undefined, undefined, 'response');
    });

    it('should handle any error in retrieving the file', () => {
      spyOn(apiService, 'get').and.callFake(() => {
        return throwError('Error');
      });
      try {
        fileService.getAndOpenFile('', '');
      } catch (e) {
        throw new Error('Expected no unhandled exceptions to be thrown');
      }
    });

    it('should force download if the browser is IE', () => {
      spyOn(apiService, 'get').and.callFake(() => {
        return of(
          new HttpResponse({
            body: { pdf: '' },
          })
        );
      });
      spyOn(browserService, 'canTriggerIEDownload').and.returnValue(true);
      const downloadFileStub = spyOn(fileService, 'downloadFile').and.callFake(() => {});
      fileService.getAndOpenFile('', undefined, 'text/plain');
      expect(downloadFileStub).toHaveBeenCalled();
    });

    it('should not auto-close a window if the browser is IE', () => {
      const successCb = jasmine.createSpy('successCb');
      const errorCb = jasmine.createSpy('errorCb');

      spyOn(apiService, 'get').and.callFake(() => {
        return throwError('Error');
      });

      spyOn(browserService, 'canTriggerIEDownload').and.returnValue(true);
      const request = fileService.getAndOpenFile('', undefined, 'text/plain');
      request.subscribe(successCb, errorCb);
      expect(successCb).not.toHaveBeenCalled();
      expect(errorCb).toHaveBeenCalled();
    });
  });
});
