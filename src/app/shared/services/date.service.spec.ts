import { TestBed, inject } from '@angular/core/testing';

import { DateService } from './date.service';

describe('DateService', () => {

  let dateService: DateService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DateService]
    });
    dateService = TestBed.get(DateService);
  });

  it('should be created', inject([DateService], (service: DateService) => {
    expect(service).toBeTruthy();
  }));

  describe('#getMsForDays', () => {

    it('should return the correct number of MS for 1 day', () => {
      const days = 1;
      const expected = days * 86400000;
      const result = dateService.getMsForDays(days);
      expect(result).toEqual(expected);
    });

    it('should return 0 if no arguments were provided', () => {
      const result = dateService.getMsForDays();
      expect(result).toEqual(0);
    });

  });

  describe('#getDate', () => {

    it('should return a date for the provided delta', () => {
      const now = new Date();
      jasmine.clock().mockDate(now);
      const target = new Date();
      const offset = 1000;
      target.setMilliseconds(target.getMilliseconds() + offset);
      const result = dateService.getDate(offset);
      expect(result).toBe(target.toISOString());
    });

    it('should return the current time/date if no offset was provided', () => {
      const now = new Date();
      jasmine.clock().mockDate(now);
      const result = dateService.getDate();
      expect(result).toBe(now.toISOString());
    });
  });

});
