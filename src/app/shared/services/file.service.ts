import { Injectable } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { saveAs } from 'file-saver';

import { ApiService } from '@app/core/api/api.service';
import { BrowserService } from '@app/shared/services/browser.service';
import { shareReplay } from 'rxjs/operators';

export interface GetFileResponse {
  pdf: string;
}

@Injectable()
export class FileService {
  constructor(private apiService: ApiService, private browserService: BrowserService) {}

  /**
   * Opens a window and retrieves a file over XHR; then handles the response or, if there was an error, closes the opened window
   * @param url url of the file
   * @param title title of the file
   * @param type type of the file
   */
  public getAndOpenFile(url: string, title: string = '', type?: string): Observable<any> {
    let target: Window;
    // Open the window if not IE; this is important to do immediately to bypass the popup blocker
    if (!this.browserService.canTriggerIEDownload(window)) {
      target = window.open('');
      target.document.title = title;
    }
    // Get the base64 string of the file
    const request = this.apiService.get<GetFileResponse>(url, undefined, undefined, 'response').pipe(shareReplay());
    // Subscribe to the request to handle the response and/or close the automatically-opened window
    request.subscribe(
      response => {
        // Get the filename from the hdears
        const filename = this.getFilenameFromHeaders(response.headers);
        this.handleFileResponse(response.body.pdf, title, filename, type, target);
      },
      err => {
        // Close the previously opened window if there was a problem opening the file
        if (!this.browserService.canTriggerIEDownload(target)) {
          target.close();
        }
      }
    );
    // Return the request to allow any component-level error handling
    return request;
  }

  /**
   * Handles a base64 string: IE -> trigger download, Everything else -> open in a new tab
   * @param base64 string
   * @param target window that the base64 string renders to
   * @param title title of the window/name of the file
   * @param type type of the file
   */
  private handleFileResponse(
    base64: string,
    title: string,
    filename: string = title,
    type: string = 'application/pdf',
    target: Window = window
  ): void {
    // If IE, Use IE's prop. mechanism to force a file download
    if (this.browserService.canTriggerIEDownload(target)) {
      this.downloadFile(base64, filename, type);
    } else {
      this.renderFileInWindow(base64, target, title, type);
    }
  }

  /**
   * Triggers a file download for a base64 string
   * N.B. Taken from the trade compliance search results component
   * TODO: We need to find a way for jasmine to spawn a IE browser (if available) in order to test this IE-only code
   * @param base64 string to convert into a downloaded file
   * @param filename intended filename of the downloaded file
   * @param type type of the file
   */
  public downloadFile(base64: string, filename: string, type: string): void {
    if (this.browserService.canTriggerIEDownload(window)) {
      const byteCharacters = atob(base64);
      const byteNumbers = new Array(byteCharacters.length);
      for (let i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      const blob = new Blob([byteArray], { type });
      window.navigator.msSaveOrOpenBlob(blob, filename);
    }
  }

  /**
   * Render the base64 string representing a file into a separate, existing window
   * @param base64 string to render
   * @param target window to render the file in
   * @param title title of the window
   * @param type type of the file
   */
  private renderFileInWindow(base64: string, target: Window, title: string, type: string): void {
    // Open the window and cache the necessary element hook
    const body = target.document.body;
    // Create an embed element
    const embed = document.createElement('embed');
    embed.width = '100%';
    embed.height = '100%';
    // Assign the source of the embed a data string
    embed.src = `data:${type};base64,${base64}`;
    // Blank the margins of the body so that the PDF fills the screen
    body.style.margin = '0px';
    target.document.title = title;
    // Move the embed to the body of the new window
    body.appendChild(embed);
  }

  public getFilenameFromHeaders(headers: HttpHeaders) {
    // From https://shekhargulati.com/2017/07/16/implementing-file-save-functionality-with-angular-4/
    const contentDisposition = {};
    if (headers) {
      const contentDispositionHeader: string = headers.get('Content-Disposition');
      if (contentDispositionHeader) {
        const contentDispositionParts: Array<string> = contentDispositionHeader.split(';');
        for (const contentDispositionPart of contentDispositionParts) {
          const keyValuePair = contentDispositionPart.split('=');
          const key = keyValuePair[0].trim();
          let value = (keyValuePair[1] || '').trim();
          if (value[value.length - 1] === '"') {
            value = value.substring(0, value.length);
          }
          if (value[0] === '"') {
            value = value.substring(1, value.length - 1);
          }
          contentDisposition[key] = value;
        }
      }
    }
    return contentDisposition['filename'] || '';
  }

  public saveResponse(response: HttpResponse<Blob>): void {
    const filename = this.getFilenameFromHeaders(response.headers);
    saveAs(response.body, filename);
  }
}
