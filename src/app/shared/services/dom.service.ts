import { Injectable } from '@angular/core';

@Injectable()
export class DomService {
  /**
   * Get all of an element's parent elements up the DOM tree
   * @param  {Node}   elem     The element
   * @param  {String} selector Selector to match against [optional]
   * @return {Array}           The parent elements
   */
  // From: https://gomakethings.com/climbing-up-and-down-the-dom-tree-with-vanilla-javascript/
  public getParents = function(elem, selector) {
    // Element.matches() polyfill
    if (!Element.prototype.matches) {
      Element.prototype.matches =
        Element.prototype['matchesSelector'] ||
        Element.prototype['mozMatchesSelector'] ||
        Element.prototype.msMatchesSelector ||
        Element.prototype['oMatchesSelector'] ||
        Element.prototype.webkitMatchesSelector ||
        function(s) {
          const matches = (
            this.document || this.ownerDocument
          ).querySelectorAll(s);
          let i = matches.length;
          while (--i >= 0 && matches.item(i) !== this) {}
          return i > -1;
        };
    }

    // Setup parents array
    const parents = [];

    // Get matching parent elements
    for (; elem && elem !== document; elem = elem.parentNode) {
      // Add matching parents to array
      if (selector) {
        if (elem.matches(selector)) {
          parents.push(elem);
        }
      } else {
        parents.push(elem);
      }
    }

    return parents;
  };
}
