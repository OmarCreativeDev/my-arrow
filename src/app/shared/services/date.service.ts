import { Injectable } from '@angular/core';

@Injectable()
export class DateService {
  constructor() {}

  public getMsForDays(numberOfDays: number = 0): number {
    return numberOfDays * 24 * 60 * 60 * 1000;
  }

  public getDate(offset: number = 0): string {
    return new Date(Date.now() + offset).toISOString();
  }

  public getIsoDateFormatString(): string {
    return 'yyyy-MM-dd';
  }
}
