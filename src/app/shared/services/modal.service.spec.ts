import { TestBed } from '@angular/core/testing';

import { ModalService } from './modal.service';

describe('ModalService', () => {

  let modalService: ModalService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ModalService]
    });
    modalService = TestBed.get(ModalService);
  });

  it('should be created', () => {
    expect(modalService).toBeTruthy();
  });

  it('should set isVisible true on show()', () => {
    modalService.show();
    expect(modalService.isVisible.value).toBeTruthy();
  });

  it('should set isVisible false on hide()', () => {
    modalService.hide();
    expect(modalService.isVisible.value).toBeFalsy();
  });

});
