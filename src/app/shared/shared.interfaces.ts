import { HttpErrorResponse } from '@angular/common/http';
import { ICartState } from '@app/core/cart/cart.interfaces';
import { ICheckoutState } from '@app/core/checkout/checkout.interfaces';
import { ICertificateState } from '@app/core/generate-certificate/generate-certificate.interface';
import { IProductPriceTiers } from '@app/core/inventory/product-price.interface';
import { IEndCustomerRecord, IPipeline, IProductSearchListingState } from '@app/core/inventory/product-search.interface';
import { OrderServiceHttpExceptionStatusEnum } from '@app/core/orders/orders.interfaces';
import { IProductNotificationsState } from '@app/core/product-notifications/product-notifications.interfaces';
import { IQuoteCartState } from '@app/core/quote-cart/quote-cart.interfaces';
import { IReelState } from '@app/core/reel/reel.interfaces';
import { ITradeComplianceSearchResults, ITradeComplianceSearchState } from '@app/core/trade-compliance/trade-compliance.interfaces';
import { IUserState, UserServiceHttpExceptionStatusEnum } from '@app/core/user/user.interface';
import { IDashboardState } from '@app/features/dashboard/stores/dashboard.interfaces';
import { IPropertiesState } from '@app/core/properties/properties.interface';
import { IRegistrationState } from '@app/core/registration/registration.interfaces';

export interface ISelectableOption<T> {
  label: string;
  value: any;
  payload?: T;
  track?: boolean;
}

export interface IAddress {
  addressLine1: string;
  addressLine2: string;
  city: string;
  region: string;
  zipCode: string;
  country: string;
}

/**
 * Orders interfaces
 */

export interface IOrderDetails {
  id: number;
  salesOrderId: number;
  purchaseOrderNumber: string;
  billToID: number;
  shipToID: number;
  billToAddress: IAddress;
  shipToAddress: IAddress;
  shipMethod: string;
  buyerName: string;
  carrierAccount: string;
  currencyCode: string;
  enteringBranch?: string;
  operatingUnit?: string;
}

export enum IOrderLineStatus {
  Open = 'OPEN',
  Closed = 'CLOSED',
  Shipped = 'SHIPPED',
  Pending = 'PENDING',
  PreparingShipping = 'PREPARING_FOR_SHIPPING',
  Cancelled = 'CANCELLED',
}

export enum IPurchaseOrderStatus {
  Open = 'OPEN',
  Closed = 'CLOSED',
  Pending = 'PENDING',
  Shipped = 'SHIPPED',
}

export interface IOrderLine {
  id: number;
  purchaseOrderNumber: string;
  // status of entire order or PO
  purchaseOrderStatus: IPurchaseOrderStatus;
  lineItem: string;
  customerPartNumber: string;
  manufacturerPartNumber: string;
  manufacturerName: string;
  qtyOrdered: number;
  qtyShipped: number;
  qtyReadyToShip: number;
  qtyRemaining: number;
  requested: Date;
  entered: Date;
  committed: Date;
  billToSiteUseId: number;

  // line status
  status: IOrderLineStatus;
  buyerName: string;
  salesOrderId: string;
  salesHeaderId: number;
  unitResale: number;
  extResale: number;
  currencyCode?: string;
  docId: string;
  itemId: number;
  warehouseId: number;
  shipments: Array<IShipment>;
  invoices: Array<IInvoice>;
  shipmentTrackingDate?: Date;
  ncnrStatus?: Boolean;
  facilityCode?: string;
  orderType?: string;
  specialHandlingCode?: string;
  shipToAccountNumber?: string;
}

export interface IShipment {
  shipmentTrackingDate: Date;
  shipmentTrackingReference: string;
  shipmentTrackingUrl: string;
  carrier: string;
  qtyShipped: number;
  deliveryId?: number;
  countryOfOrigin?: string;
  dateCode?: string;
}

export interface IOrder {
  details: IOrderDetails;
  orderLines: Array<IOrderLine>;
}

/**
 * Search
 */

export interface IApiRequestState {
  loading?: boolean;
  error?: HttpErrorResponse;
}

export interface ISearchCriteria {
  value: string;
  type: string;
}

export enum IFilterCriteronOperatorEnum {
  Equals = 'EQ',
  GreaterThan = 'GT',
  LessThan = 'LT',
  GreaterThanOrEqualTo = 'GTE',
  LessThanOrEqualTo = 'LTE',
}

export interface IFilterCriteron {
  key: string;
  value: string;
  operator: IFilterCriteronOperatorEnum;
  editable?: boolean;
}

export interface IFilter {
  value: number;
  rules: Array<IFilterRule>;
}
export interface IFilterRule {
  criteria: Array<IFilterCriteron>;
}

/**
 * Listing States
 */

export interface IListingState<ItemType> extends IApiRequestState {
  items: Array<ItemType>;
  selectedIds?: Array<string>;
  pagination?: IPaginationState;
  sort?: Array<ISortCriteron>;
}

export interface IObjectState<ItemType> extends IApiRequestState {
  value: ItemType;
}

export interface IOrderState extends IObjectState<IOrder> {
  selectedIds?: Array<string>;
  confirmed?: boolean;
}

export interface IPaginationState {
  limit: number;
  total: number;
  current: number;
}

export interface IListingSearchState<ItemType> extends IListingState<ItemType> {
  search: ISearchCriteria;
  filter: IFilter;
}

export interface IForecastDetails {
  name: string;
  eceivedDate: string;
  lastArrowUpdateDate: string;
  weeksInHorizon: number;
  linesSubmitted: number;
  partsCovered: number;
  potentialShortages: number;
}

export interface IForecastSearchState<ItemType> extends IListingSearchState<ItemType> {
  summaryDetails: IForecastDetails;
  shortagesOnly: boolean;
  potentialShortages: number;
}

export interface ISortCriteron {
  key: string;
  order: ISortCriteronOrderEnum;
}

export enum ISortCriteronOrderEnum {
  ASC = 'ASC',
  DESC = 'DESC',
}

/**
 * Products
 */

export interface IProduct {
  arrowReel: boolean;
  availableQuantity: number;
  bufferQuantity: number;
  businessCost?: number;
  compliance: IComplianceState;
  customerPartNumber?: string;
  datasheet?: string;
  description: string;
  docId: string;
  endCustomerRecords?: Array<IEndCustomerRecord>;
  endCustomerSiteId?: number;
  id: string;
  image: string;
  itemId: number;
  itemType: string;
  leadTime: number;
  manufacturer: string;
  minimumOrderQuantity: number;
  mpn: string;
  multipleOrderQuantity: number;
  pipeline?: IPipeline;
  price: number;
  priceTiers?: Array<IProductPriceTiers>;
  quotable: boolean;
  specs: Array<IProductSpecification>;
  spq: number;
  warehouseId: number;
  htsFlag?: boolean;
  supplierAllocated?: string;
}

export interface IComplianceState {
  CHINA_ROHS?: string;
  EU_ROHS?: string;
}

export interface IProductSpecification {
  name: string;
  value: string;
  uom?: string;
}

export interface IAcceptanceResponse {
  accepted: boolean;
}

export interface IInvoice {
  number: string;
  date: string;
  quantity: number;
  amount: number;
  url: string;
}

export interface ISelectableItem {
  id: any;
}

export interface ISortableItem extends ISelectableItem {
  label: string;
  [propName: string]: any;
}

export interface IDownloadCriteria {
  search: ISearchCriteria;
  filter: Array<IFilterRule>;
  sort: Array<ISortCriteron>;
  fileType: IFileTypeEnum;
  columns: Array<IDownloadUrlColumn | IBasicDownloadColumn>;
  title: string;
}

export interface IQuoteDownloadCriteria {
  quoteHeaderId: number;
  fileType: IFileTypeEnum;
  columns: string[];
}

export interface IDownloadColumn {
  type: IDownloadColumnTypeEnum;
  label: string;
}

export enum IDownloadColumnTypeEnum {
  Url = 'URL',
  Date = 'DATE',
  String = 'STRING',
}

export interface IDownloadUrlColumn extends IDownloadColumn {
  id: number;
  value: string;
  url: string;
}

export interface IBasicDownloadColumn extends IDownloadColumn {
  properties: Array<string>;
  format: string;
}

export interface IFileTypeOption {
  value: IFileTypeEnum;
  label: string;
}

export enum IFileTypeEnum {
  XLS = 'XLS',
  XLSX = 'XLSX',
  CSV = 'CSV',
}

export interface IException extends Error {
  error: IHttpException;
}

export interface IHttpException {
  error: string;
  timestamp: string;
  status: OrderServiceHttpExceptionStatusEnum | UserServiceHttpExceptionStatusEnum;
  exception: string;
  message?: string;
  messages: Array<string>;
  path: string;
}

/**
 * Alert
 */

export enum AlertComponentEnum { // TODO: rename -> AlertComponentTypeEnum
  Error = 'error',
  Information = 'information',
  Success = 'success',
}

/**
 * App store state
 */

export interface IAppState {
  user: IUserState;
  cart: ICartState;
  checkout: ICheckoutState;
  tradeCompliancePartNumbers: ITradeComplianceSearchState<ITradeComplianceSearchResults>;
  tradeComplianceCertificate: ICertificateState;
  properties: IPropertiesState;
  quoteCart: IQuoteCartState;
  productSearch?: IProductSearchListingState;
  productNotifications: IProductNotificationsState;
  reel: IReelState;
  orders?: IListingSearchState<IOrderLine>;
  orderDetails?: IOrderState;
  dashboard?: IDashboardState;
  features?: object;
  registration?: IRegistrationState;
}

export enum IFormFieldStatus {
  Disabled = 'DISABLED',
  Enabled = 'ENABLED',
}

export enum orderReturnCategory {
  RMA = 'RMA',
  FQR = 'FQR',
}

export enum orderReturnReason {
  shortShip = 'Short Ship',
  overShip = 'Over Ship',
  defectiveParts = 'Defective',
  damagedParts = 'Damaged Parts',
  packagingIssue = 'Packaging Issue',
  wrongParts = 'Wrong Parts',
  anotherCustomersParts = "Another Customer's Parts",
  earlyShipment = 'Early Shipment',
  courtesyReturn = 'Courtesy Return/No Longer Needed',
  customerRequirementNotFollowed = 'Customer Requirement not Followed',
  issueNotListed = 'Issue Not Listed',
}

export interface IOrderReturnFormHeader {
  capa: boolean;
  capaNumber: string;
  description: string;
  partsNo: boolean;
  partsYes: boolean;
  repair?: boolean;
  failure?: boolean;
  credit?: boolean;
}

export interface IOrderReturnFormLineItem {
  dateCodeUser?: string;
  quantityAffected?: number;
  quantityReceived?: number;
  partNumberReceived?: string;
  attachments?: Array<IOrderReturnFormLineAttachment>;
}

export interface IOrderReturnFormLineAttachment {
  id: string;
  name: string;
}

export interface IRmaSubmitRequest {
  creditRequired: boolean;
  repairReplacement: boolean;
  failureAnalysisRequired: boolean;
  capaRequired: boolean;
  branchTransferOrder: boolean;
  returningParts: boolean;
  problemDescription: string;
  region: string;
  customerCapaReference: string;
  customerQualityContactName: string;
  contactPhone: string;
  contactEmail: string;
  salesOrderNumber: string;
  customerPONumber: string;
  arrowSalesContactEmail: string;
  oracleAccountNumber: string;
  customerName: string;
  enteringBranch: string;
  billToAccount: string;
  shipToAccount: string;
  issueType: string;
  operatingUnit: string;
  problemCategory: string;
  lineItems: Array<IRmaLineItemRequest>;
}

export interface IRmaLineItemRequest {
  quantityAffected: number;
  quantityReceived: number;
  quantityOrdered: number;
  partNumberReceived: string;
  dateCodeUser: string;
  lineItem: string;
  partNumberOrdered: string;
  customerPartNumber: string;
  warehouseCode: string;
  stockNumber: string;
  orderLineType: string;
  manufacturerName: string;
  manufacturerPartNumber: string;
  facilityCode: string;
  specialHandlingCode: string;
  shipToAccount: string;
  deliveries: Array<IRmaShipmentRequest>;
  attachments: Array<IRmaAttachmentRequest>;
}

export interface IRmaShipmentRequest {
  invoiceNumber: string;
  deliveryId: string;
  dateCodeSystem: string;
  carrier: string;
  shipDate: string;
  trackingNumber: string;
  countryOfOrigin: string;
}

export interface IRmaAttachmentRequest {
  id: string;
  name: string;
}
