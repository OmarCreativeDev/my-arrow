import { AbstractControl, ValidatorFn, ValidationErrors } from '@angular/forms';
import * as moment from 'moment';
import get from 'lodash-es/get';

/**
 * Provides a set of custom validators used by form controls.
 */
export class CustomValidators {
  /**
   * Validator that performs password validation
   * Passwords must be at between 8-25 characters with at least one number, uppercase letter and lowercase letter
   */
  static password(control: AbstractControl): ValidationErrors | null {
    const regEx = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d$@$!%*?&]{8,25}$/;
    return regEx.test(control.value) ? null : { password: true };
  }

  /**
   * FormGroup validator that requires two controls to have the same values
   */
  static equal(control1: AbstractControl, control2: AbstractControl, errorType?: string): ValidatorFn {
    return () => {
      return control2.value && control1.value !== control2.value ? (errorType ? { [errorType]: true } : { matching: true }) : null;
    };
  }

  /**
   * FormGroup validator that requires input value to be greater than or equal to minimunQuantity
   */
  static greaterThan(minimunQuantity: number, errorType: string): ValidatorFn {
    return (input: AbstractControl) => {
      return input.value >= minimunQuantity ? null : { [errorType]: true };
    };
  }

  /**
   * FormControl validator that requires a value to be an integer
   * @param control
   */
  static integer(control: AbstractControl): ValidationErrors {
    return Number.isInteger(+control.value) ? null : { valid: false };
  }

  /**
   * FormGroup validator that requires a Date to be valid and either today or a day in the future.
   * Works with the DatePicker which has an array of Dates as its value.
   * Only validates the first date.
   */
  static date(control: AbstractControl): ValidationErrors | null {
    const dateMoment = moment(get(control, 'value', [''])[0]);
    return dateMoment.isValid() && dateMoment.diff(moment(), 'days') >= 0 ? null : { invalidDate: true };
  }
  /**
   * FormGroup validator that requires input value to be module of multipleQuantity
   */
  static multipleOf(multipleQuantity: number, errorType: string): ValidatorFn {
    return (input: AbstractControl) => {
      if (input.value != null) {
        return input.value % multipleQuantity === 0 ? null : { [errorType]: true };
      }
    };
  }

  /**
   * Validator that performs First or Last Name validation
   * No number or special characters are allowed
   */
  static firstOrLastNames(control: AbstractControl): ValidationErrors | null {
    const regEx = /^[a-zA-Z\s']+$/;
    return regEx.test(control.value.trim() || '') ? null : { validValue: true };
  }

  /**
   * Validator that performs phone number validation
   * Only Numbers and white spaces are allowed
   */
  static phoneNumber(control: AbstractControl): ValidationErrors | null {
    const regEx = /^[\d\s]+$/;
    return regEx.test(control.value.trim() || '') ? null : { validValue: true };
  }

  /**
   * Validator that performs dropdow required validation
   */
  static dropwdownRequired(control: AbstractControl): ValidatorFn | null {
    return () => {
      return control.value ? null : { invalid: true };
    };
  }

  /**
   * Validator that performs email validation
   */
  static email(control: AbstractControl): ValidationErrors | null {
    const regEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regEx.test(control.value) ? null : { email: true };
  }

  /**
   * Validator that performs only letters and spaces validation
   */
  static onlyLettersAndSpaces(control: AbstractControl): ValidationErrors | null {
    const regEx = /^[a-zA-Z\s]*$/;
    return regEx.test(control.value) ? null : { onlyLettersAndSpaces: true };
  }
}
