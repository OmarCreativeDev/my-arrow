import { OnInit, Input, OnDestroy } from '@angular/core';
import { NgForm, FormControl } from '@angular/forms';
import uniqueId from 'lodash-es/uniqueId';
import { Subscription } from 'rxjs';

export abstract class FormField implements OnInit, OnDestroy {
  @Input() public form: NgForm;
  @Input() public control: FormControl;
  @Input() public label: string;

  public id: string;
  public subscription: Subscription = new Subscription();

  constructor() {
    // set unique id to use for field/labels associations
    this.id = uniqueId(`form-field-`);
  }

  ngOnInit() {
    this.startSubscriptions();
  }

  ngOnDestroy(): void {
    if (this.subscription && !this.subscription.closed) this.subscription.unsubscribe();
  }

  private startSubscriptions() {
    this.subscription.add(this.subscribeSubmit());
  }

  private subscribeSubmit(): Subscription {
    // mark control as touched once form subitted to handle error behaviour
    return this.form.ngSubmit.subscribe(() => {
      this.control.markAsTouched();
    });
  }

  /**
   * Returns true if control should be displayed in error state
   */
  get hasError() {
    return this.control.invalid && this.control.touched;
  }
}
