import { ToggleComponent } from '@app/shared/classes/toggle-component.class';
import { ElementRef } from '@angular/core';

const mockNativeElement = {
  contains: () => {},
};

class MockElementRef {
  public nativeElement = mockNativeElement;
}

class MockToggleComponent extends ToggleComponent {
  constructor(public elementRef: ElementRef) {
    super(elementRef);
  }
}

describe('ToggleComponent', () => {
  let mockToggleComponent: ToggleComponent;

  beforeEach(() => {
    mockToggleComponent = new MockToggleComponent(new MockElementRef());
  });

  it('should create', () => {
    expect(mockToggleComponent).toBeTruthy();
  });

  it('toggle should invert the current isOpen value and emit the value on openChange', () => {
    spyOn(mockToggleComponent.openChange, 'emit');
    mockToggleComponent.isOpen = false;
    mockToggleComponent.toggle();
    expect(mockToggleComponent.isOpen).toBeTruthy();
    expect(mockToggleComponent.openChange.emit).toHaveBeenCalledWith(true);
  });

  it('should toggle the component if open and the clicked element was not a child', () => {
    spyOn(mockNativeElement, 'contains').and.returnValue(false);
    spyOn(mockToggleComponent, 'hide');
    mockToggleComponent.isOpen = true;
    mockToggleComponent._handleDocumentClick(new MouseEvent(undefined));
    expect(mockToggleComponent.hide).toHaveBeenCalled();
  });

  it('should not toggle the component if open and the clicked element was a child', () => {
    spyOn(mockNativeElement, 'contains').and.returnValue(true);
    spyOn(mockToggleComponent, 'toggle');
    mockToggleComponent.isOpen = true;
    mockToggleComponent._handleDocumentClick(new MouseEvent(undefined));
    expect(mockToggleComponent.toggle).not.toHaveBeenCalled();
  });

  it('should not toggle the component if the component was closed, even if the clicked element was not a child', () => {
    spyOn(mockNativeElement, 'contains').and.returnValue(false);
    spyOn(mockToggleComponent, 'toggle');
    mockToggleComponent.isOpen = false;
    mockToggleComponent._handleDocumentClick(new MouseEvent(undefined));
    expect(mockToggleComponent.toggle).not.toHaveBeenCalled();
  });

  it('should not change isOpen or emit on openChange if the component is disabled', () => {
    spyOn(mockToggleComponent.openChange, 'emit');
    const expectedIsOpen = false;
    mockToggleComponent.disabled = true;
    mockToggleComponent.isOpen = expectedIsOpen;
    mockToggleComponent.toggle();
    expect(mockToggleComponent.isOpen).toEqual(expectedIsOpen);
    expect(mockToggleComponent.openChange.emit).not.toHaveBeenCalled();
  });
});
