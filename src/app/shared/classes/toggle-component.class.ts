import { Output, EventEmitter, Input } from '@angular/core';
import { HostListener } from '@angular/core';
import { ElementRef } from '@angular/core';

export abstract class ToggleComponent {
  public isOpen: boolean = false;

  @Input() disabled: boolean = false;
  @Output() openChange = new EventEmitter<boolean>();

  /**
   * Listens for a global click event and, if the clicked element was not contained in this element, close the menu
   * @param $event
   */
  @HostListener('document:click', ['$event'])
  _handleDocumentClick($event: MouseEvent) {
    if (
      this.isOpen &&
      !this._elementRef.nativeElement.contains($event.target)
    ) {
      this.hide();
    }
  }

  // necessary to init with the reference to the element (used in the _handleDocumentClick method)
  constructor(private _elementRef: ElementRef) {}

  /**
   * Toggles the component
   * @param $event
   */
  public toggle(): void {
    if (!this.disabled) {
      if (this.isOpen) {
        this.hide();
      } else {
        this.show();
      }
    }
  }

  public hide(): void {
    this.isOpen = false;
    this.openChange.emit(this.isOpen);
  }

  public show(): void {
    this.isOpen = true;
    this.openChange.emit(this.isOpen);
  }
}
