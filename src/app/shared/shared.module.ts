import { AddQuantityToCartComponent } from '@app/shared/components/add-quantity-to-cart/add-quantity-to-cart.component';
import { AddToCartDialogComponent } from './components/add-to-cart-dialog/add-to-cart-dialog.component';
import { AnimatedHeightComponent } from './components/animated-height/animated-height.component';
import { BillToAccountsComponent } from '@app/shared/components/bill-to-accounts/bill-to-accounts.component';
import { CalendarComponent } from './components/calendar/calendar.component';
import { CheckboxComponent } from './components/checkbox/checkbox.component';
import { CommonModule, CurrencyPipe, DatePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { DialogPanelComponent } from './components/dialog-panel/dialog-panel.component';
import { DomService } from '@app/shared/services/dom.service';
import { DownloadHistoryComponent } from '@app/features/product-notifications/components/download-history/download-history.component';
import { DragulaModule } from 'ng2-dragula';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { EmailModalComponent } from '@app/shared/components/email-modal/email-modal.component';
import { EmailOrdersComponent } from '@app/shared/components/email-modal/components/email-orders/email-orders.component';
import { EndCustomerDropdownsComponent } from './components/end-customer-dropdowns/end-customer-dropdowns.component';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { FormattedPricePipe } from '@app/shared/pipes/formatted-price/formatted-price.pipe';
import { FormattedAddressPipe } from '@app/shared/pipes/formatted-address/formatted-address.pipe';
import { FormItemComponent } from '@app/shared/components/forms/form-item/form-item.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IconComponent } from './components/icon/icon.component';
import { InputFieldComponent } from './components/forms/input-field/input-field.component';
import { InventoryComponent } from './components/inventory/inventory.component';
import { IsoDatePipe } from './pipes/iso-date/iso-date.pipe';
import { LineSelectionComponent } from './components/line-selection/line-selection.component';
import { ListingMessageComponent } from '@app/shared/components/listing-message/listing-message.component';
import { LoaderComponent } from './components/loader/loader.component';
import { ModalComponent } from './components/modal/modal.component';
import { ProductNotificationsSettingsComponent } from '@app/features/product-notifications/components/product-notifications-settings/product-notifications-settings.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { PapaParseModule } from 'ngx-papaparse';
import { PriceTiersComponent } from '@app/shared/components/price-tiers/price-tiers.component';
import { ProductPriceComponent } from './components/product-price/product-price.component';
import { ProductSearchBarComponent } from './components/product-search-bar/product-search-bar.component';
import { RadioComponent } from './components/radio/radio.component';
import { RouterModule } from '@angular/router';
import { ScrollDuringDragDirective } from './directives/scroll-during-drag.directive';
import { ScrollToElementDirective } from './directives/scroll-to-element/scroll-to-element.directive';
import { ShipToAccountsComponent } from './components/ship-to-accounts/ship-to-accounts.component';
import { SortableColumnHeaderComponent } from './components/sortable-column-header/sortable-column-header.component';
import { SortableItemsComponent } from './components/sortable-items/sortable-items.component';
import { SubtotalDisplayComponent } from './components/subtotal-display/subtotal-display.component';
import { SvgIconModule } from '@app/shared/svg-icon/svg-icon.module';
import { TabsComponent } from './components/tabs/tabs.component';
import { TabsContainerComponent } from './components/tabs-container/tabs-container.component';
import { TabsContentComponent } from './components/tabs-container/components/tabs-content/tabs-content.component';
import { TextareaFieldComponent } from './components/forms/textarea-field/textarea-field.component';
import { ToastComponent } from './components/toast/toast.component';
import { FileService } from '@app/shared/services/file.service';
import { BrowserService } from '@app/shared/services/browser.service';
import { AlertComponent } from './components/alert/alert.component';
import { ButtonComponent } from './components/button/button.component';
import { ReelDialogComponent } from './components/reel-dialog/reel-dialog.component';
import { FormattedCountPipe } from '@app/shared/pipes/formatted-count/formatted-count.pipe';
import { OnlyPositiveNumbersDirective } from './directives/only-positive-numbers.directive';
import { OnlyAlphaCharactersDirective } from './directives/only-alpha-characters.directive';
import { OnlyPhoneNumberDirective } from './directives/phone-number.directive';
import { CurrencyFormatDirective } from './directives/currency-format.directive';
import { NoLeadingZerosDirective } from './directives/no-leading-zeros.directive';
import { LineItemCpnComponent } from './components/line-item-cpn/line-item-cpn.component';
import { NoWhitespaceDirective } from './directives/no-whitespace.directive';
import { ReelCalculationComponent } from '@app/shared/components/reel-calculation/reel-calculation.component';
import { TooltipComponent } from './components/tooltip/tooltip.component';
import { RohsIconComponent } from './components/rohs-icon/rohs-icon.component';
import { TooltipDirective } from './directives/tooltip.directive';
import { InformationBoxComponent } from './components/information-box/information-box.component';
import { PasswordInformationBoxComponent } from './components/password-information-box/password-information-box.component';
import { AddToCartCapDialogComponent } from './components/add-to-cart-cap-dialog/add-to-cart-cap-dialog.component';
import { ScrollShadowDirective } from '@app/shared/directives/scroll-shadow';
import { WINDOW_PROVIDERS } from '@app/shared/services/window.service';
import { ForecastDownloadComponent } from '@app/features/forecast/components/forecast-download/forecast-download.component';
import { StatusBarComponent } from './components/status-bar/status-bar.component';
import { MultipleShippingComponent } from '@app/features/orders/components/multiple-shipping/multiple-shipping.component';

import { OverlayModule } from '@angular/cdk/overlay';
import { DatePickerOvelayComponent } from '@app/shared/components/date-picker/date-picker-overlay/date-picker-overlay.component';
import { FocusFirstInvalidDirective } from './directives/focus-first-invalid.directive';
import { ScrollToEndSpyDirective } from './directives/scroll-to-end-spy.directive';
import { EmailForecastComponent } from '@app/shared/components/email-modal/components/email-forecast/email-forecast.component';
import { AddToBomDialogComponent } from './components/add-to-bom-dialog/add-to-bom-dialog.component';
import { PreventEnterKeyDirective } from '@app/shared/directives/prevent-enter-key.directive';
import { StickyHeaderDirective } from '@app/shared/directives/sticky-header.directive';
import { SecureTextboxComponent } from './components/secure-textbox/secure-textbox.component';

@NgModule({
  imports: [
    CommonModule,
    DragulaModule,
    FormsModule,
    PapaParseModule,
    ReactiveFormsModule,
    RouterModule,
    OverlayModule,
    SvgIconModule.forRoot('/assets/icons/fallback.svg'),
  ],
  declarations: [
    SecureTextboxComponent,
    StickyHeaderDirective,
    AddQuantityToCartComponent,
    AddToCartDialogComponent,
    AnimatedHeightComponent,
    BillToAccountsComponent,
    CalendarComponent,
    CheckboxComponent,
    DatePickerComponent,
    DatePickerOvelayComponent,
    DialogPanelComponent,
    DownloadHistoryComponent,
    DropdownComponent,
    EmailModalComponent,
    EmailOrdersComponent,
    EndCustomerDropdownsComponent,
    FileUploadComponent,
    FormattedCountPipe,
    FormattedPricePipe,
    FormattedAddressPipe,
    FormItemComponent,
    IconComponent,
    InputFieldComponent,
    InventoryComponent,
    IsoDatePipe,
    LineSelectionComponent,
    ListingMessageComponent,
    LoaderComponent,
    ModalComponent,
    ProductNotificationsSettingsComponent,
    PaginationComponent,
    PriceTiersComponent,
    ProductPriceComponent,
    ProductSearchBarComponent,
    RadioComponent,
    ScrollDuringDragDirective,
    ScrollToElementDirective,
    ShipToAccountsComponent,
    SortableColumnHeaderComponent,
    SortableItemsComponent,
    SubtotalDisplayComponent,
    TabsComponent,
    TabsContainerComponent,
    TabsContentComponent,
    TextareaFieldComponent,
    ToastComponent,
    AlertComponent,
    ButtonComponent,
    ReelDialogComponent,
    OnlyPositiveNumbersDirective,
    OnlyAlphaCharactersDirective,
    OnlyPhoneNumberDirective,
    CurrencyFormatDirective,
    NoLeadingZerosDirective,
    LineItemCpnComponent,
    NoWhitespaceDirective,
    ReelCalculationComponent,
    TooltipComponent,
    RohsIconComponent,
    TooltipDirective,
    InformationBoxComponent,
    PasswordInformationBoxComponent,
    AddToCartCapDialogComponent,
    ScrollShadowDirective,
    FocusFirstInvalidDirective,
    ScrollToEndSpyDirective,
    ForecastDownloadComponent,
    EmailForecastComponent,
    AddToBomDialogComponent,
    StatusBarComponent,
    PreventEnterKeyDirective,
    MultipleShippingComponent,
  ],
  exports: [
    SecureTextboxComponent,
    AddQuantityToCartComponent,
    AnimatedHeightComponent,
    BillToAccountsComponent,
    CalendarComponent,
    CheckboxComponent,
    DatePickerComponent,
    DatePickerOvelayComponent,
    DialogPanelComponent,
    DropdownComponent,
    EmailModalComponent,
    EmailOrdersComponent,
    EndCustomerDropdownsComponent,
    FileUploadComponent,
    FormattedCountPipe,
    FormattedPricePipe,
    FormattedAddressPipe,
    FormItemComponent,
    IconComponent,
    InputFieldComponent,
    InventoryComponent,
    IsoDatePipe,
    LineSelectionComponent,
    ListingMessageComponent,
    LoaderComponent,
    ModalComponent,
    PaginationComponent,
    PriceTiersComponent,
    ProductPriceComponent,
    ProductSearchBarComponent,
    RadioComponent,
    ScrollDuringDragDirective,
    ScrollToElementDirective,
    ShipToAccountsComponent,
    SortableColumnHeaderComponent,
    SortableItemsComponent,
    SubtotalDisplayComponent,
    SvgIconModule,
    TabsComponent,
    TabsContainerComponent,
    TabsContentComponent,
    TextareaFieldComponent,
    ToastComponent,
    AlertComponent,
    ButtonComponent,
    ReelDialogComponent,
    OnlyPositiveNumbersDirective,
    OnlyAlphaCharactersDirective,
    OnlyPhoneNumberDirective,
    CurrencyFormatDirective,
    NoLeadingZerosDirective,
    LineItemCpnComponent,
    NoWhitespaceDirective,
    ReelCalculationComponent,
    TooltipComponent,
    RohsIconComponent,
    TooltipDirective,
    InformationBoxComponent,
    PasswordInformationBoxComponent,
    AddToCartCapDialogComponent,
    ScrollShadowDirective,
    FocusFirstInvalidDirective,
    ScrollToEndSpyDirective,
    EmailForecastComponent,
    AddToBomDialogComponent,
    StatusBarComponent,
    PreventEnterKeyDirective,
    StickyHeaderDirective,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [DatePipe, DomService, CurrencyPipe, FileService, BrowserService, WINDOW_PROVIDERS],
  entryComponents: [
    AddToCartDialogComponent,
    BillToAccountsComponent,
    DownloadHistoryComponent,
    EmailModalComponent,
    ProductNotificationsSettingsComponent,
    ShipToAccountsComponent,
    ToastComponent,
    ReelDialogComponent,
    DatePickerOvelayComponent,
    ForecastDownloadComponent,
    AddToBomDialogComponent,
    AddToCartCapDialogComponent,
    MultipleShippingComponent,
  ],
})
export class SharedModule {}
