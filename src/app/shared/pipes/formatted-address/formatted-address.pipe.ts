import { Pipe, PipeTransform } from '@angular/core';
import { at, reject, isNil } from 'lodash-es';
import { IShipTo } from '@app/core/checkout/checkout.interfaces';
import { IBillTo } from '@app/core/user/user.interface';

@Pipe({ name: 'formattedAddress' })
export class FormattedAddressPipe implements PipeTransform {
  transform(value: IShipTo | IBillTo, showName: boolean = true): String {
    const addressProps = ['address1', 'address2', 'address3', 'address4', 'city', 'state', 'postCode', 'postalCode'];
    if (showName) addressProps.unshift('name');
    let address = at(value, addressProps);
    address = reject(address, line => {
      return isNil(line) || !/\S/.test(line);
    });
    if (value) {
      return address.join('<br>');
    } else {
      return '';
    }
  }
}
