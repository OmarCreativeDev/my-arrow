import { FormattedAddressPipe } from './formatted-address.pipe';
import { IShipTo } from '@app/core/checkout/checkout.interfaces';

const completeAddress: IShipTo = {
  id: 24667766,
  name: 'ONCORE MANUFACTURING SERVICES',
  address1: 'Line 1',
  address2: 'Line 2',
  address3: 'Line 3',
  address4: 'Line 4',
  city: 'Chatsworth',
  state: 'CA',
  postCode: '91311',
  country: 'US',
  county: 'Los Angeles',
  primaryFlag: true,
};

const partialAddress: IShipTo = {
  id: 24667766,
  name: 'ONCORE MANUFACTURING SERVICES',
  address1: null,
  address2: 'Line 2',
  address3: 'Line 3',
  address4: null,
  city: 'Chatsworth',
  state: 'CA',
  postCode: null,
  country: 'US',
  county: 'Los Angeles',
  primaryFlag: true,
};

describe('FormattedAddressPipe', () => {
  let pipe: FormattedAddressPipe;

  beforeEach(() => {
    pipe = new FormattedAddressPipe();
  });

  it('display the desired formatted address when the value is a full address', () => {
    expect(pipe.transform(completeAddress)).toEqual(
      'ONCORE MANUFACTURING SERVICES<br>Line 1<br>Line 2<br>Line 3<br>Line 4<br>Chatsworth<br>CA<br>91311'
    );
  });

  it('display the desired formatted address when the value is a partial address', () => {
    expect(pipe.transform(partialAddress)).toEqual('ONCORE MANUFACTURING SERVICES<br>Line 2<br>Line 3<br>Chatsworth<br>CA');
  });

  it('display just the empty string even when the value is null', () => {
    expect(pipe.transform(null)).toEqual('');
  });
});
