import { Pipe, PipeTransform } from '@angular/core';
import * as mathjs from 'mathjs';
import { CurrencyPipe } from '@angular/common';
export const PRICE_DECIMALS_DEFAULT = '0.2-6';
export const MAX_SUBTOTAL_DECIMALS = 2;
export const MAX_FULL_DECIMALS = 5;

@Pipe({ name: 'formattedPrice' })
export class FormattedPricePipe implements PipeTransform {
  constructor(private currencyPipe: CurrencyPipe) {}
  transform(value: any, currency: string, display?: 'full' | 'subTotal'): String {
    if (value) {
      const roundValue =
        display === 'subTotal' ? mathjs.round(value, MAX_SUBTOTAL_DECIMALS) : this.nonRoundDecimalTrim(value, MAX_FULL_DECIMALS);
      return this.currencyPipe.transform(roundValue, currency, 'symbol', PRICE_DECIMALS_DEFAULT);
    } else {
      return '0';
    }
  }

  private nonRoundDecimalTrim(value: number, maxDecimals: number): number {
    const _value: string = value.toString();
    const [number, decimals] = _value.indexOf('.') > 0 ? _value.split('.') : [_value, '0'];
    const _decimals = decimals.slice(0, maxDecimals);
    return Number(number.concat('.', _decimals));
  }
}
