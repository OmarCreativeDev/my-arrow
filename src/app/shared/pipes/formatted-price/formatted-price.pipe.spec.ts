import { FormattedPricePipe } from './formatted-price.pipe';
import { CurrencyPipe } from '@angular/common';

describe('FormattedPricePipe', () => {
  let pipe: FormattedPricePipe;

  beforeEach(() => {
    pipe = new FormattedPricePipe(new CurrencyPipe('EN'));
  });

  it('display rounded value with the desired number of decimals', () => {
    expect(pipe.transform(1000.12355, 'USD', 'subTotal')).toEqual('$1,000.12');
  });

  it("display just the requested decimals even when those doesn't exceed the numbers to be picked", () => {
    expect(pipe.transform(1000.12, 'USD', 'subTotal')).toEqual('$1,000.12');
  });

  it('display just the requested decimals even when there are no decimals', () => {
    expect(pipe.transform(1000, 'USD')).toEqual('$1,000.00');
  });
  it('display `0` even when there value is null', () => {
    expect(pipe.transform(null, 'USD')).toEqual('0');
  });
});
