import { FormattedCountPipe } from './formatted-count.pipe';

describe('FormattedCountPipe', () => {
  let pipe: FormattedCountPipe;

  beforeEach(() => {
    pipe = new FormattedCountPipe();
  });

  it('display empty string when value is 0', () => {
    expect(pipe.transform(0, 100)).toEqual('');
  });

  it('display string equal to value when value is below max', () => {
    expect(pipe.transform(10, 100)).toEqual('10');
  });

  it('display string equal to value when value is equal to max', () => {
    expect(pipe.transform(100, 100)).toEqual('100');
  });

  it('display string equal to value appending a + symbol when value is above max', () => {
    expect(pipe.transform(101, 100)).toEqual('100+');
  });
});
