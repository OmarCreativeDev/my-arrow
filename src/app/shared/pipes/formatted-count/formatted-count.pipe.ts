import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'formattedCount' })
export class FormattedCountPipe implements PipeTransform {
  constructor() {}

  transform(value: any, max: number): String {
    if (value > max) {
      return `${max}+`;
    } else if (value > 0) {
      return value.toString();
    } else {
      return '';
    }
  }
}
