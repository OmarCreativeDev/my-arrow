import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';
import { DateService } from '@app/shared/services/date.service';

@Pipe({
  name: 'isoDate'
})
export class IsoDatePipe implements PipeTransform {
  constructor(private datePipe: DatePipe, private dateService: DateService) {}

  transform(value: any, args?: any): any {
    return this.datePipe.transform(
      value,
      this.dateService.getIsoDateFormatString()
    );
  }
}
