import { Directive, HostListener } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { DomService } from '@app/shared/services/dom.service';

export const AUTO_SCROLL_SPEED = 5;
export const HOT_ZONE_PERCENT = 0.1;
export const MAX_SCROLL_SPEED = 8;

@Directive({
  selector: '[appScrollDuringDrag]',
})
export class ScrollDuringDragDirective {
  public dragging = false;
  public draggingElement: Element;

  @HostListener('document:mousemove', ['$event'])
  public move($event: MouseEvent) {
    // Only handle the drag when the dragging flag is truthy
    if (this.dragging) {
      this.handleDrag($event, this.draggingElement);
    }
  }

  constructor(private dragulaService: DragulaService, private domService: DomService) {
    this.subscribeToDragEvents();
  }

  /**
   * Subscribes to any drag events emitted from the dragulaService
   */
  private subscribeToDragEvents() {
    // Start a drag when a drag event is registered
    this.dragulaService.drag.subscribe(value => {
      const elements: Array<any> = value.slice(1);
      this.draggingElement = elements[0];
      this.dragging = true;
    });
    // Cancel the drag when a drop is registered
    this.dragulaService.drop.subscribe(() => {
      this.dragging = false;
      this.draggingElement = undefined;
    });
  }

  /**
   * Calculates thresholds for scroll, then percent above the thresholds to scroll the parent scrollable area
   * i.e. if the drag is just above the threshold when dragging up, it will move more slowly than if the drag is a long way above the threshold; and vice versa
   * @param $event the mouse move event that continues the drag
   */
  public handleDrag($event: MouseEvent, element: Element): void {
    let scrollEl: Element | Window;
    // Get first scrollable parent
    const scrollingParents = this.domService.getParents(element, '.scroll');
    if (!scrollingParents.length) {
      scrollEl = window;
    } else {
      scrollEl = scrollingParents[0];
    }
    const scrollElBoundingBox: ClientRect = this.getBoundingBoxForElement(scrollEl);
    // Establish some constants for drag calculation
    const dragY = $event.clientY;
    const scrollElHeight = scrollElBoundingBox.height;
    const scrollElYPosition = scrollElBoundingBox.top;
    const thesholdHeight = scrollElHeight * HOT_ZONE_PERCENT;
    // Find start and end thresholds - these are the points - above and below respectively - that we start scroll automatically
    const topTheshold = scrollElYPosition + thesholdHeight;
    const bottomTheshold = scrollElYPosition + scrollElHeight - thesholdHeight;
    const delta = this.getScrollDeltaForDrag(dragY, thesholdHeight, topTheshold, bottomTheshold);
    // Apply the resulting delta to the scrollable area
    if (window === scrollEl) {
      window.scrollTo(0, window.pageYOffset + Math.round(delta));
    } else {
      (scrollEl as Element).scrollTop = (scrollEl as Element).scrollTop + Math.round(delta);
    }
  }

  /**
   * Calculates the bounding box for a given element
   * @param element the element to find the bounding box for
   */
  private getBoundingBoxForElement(element: Element | Window): ClientRect {
    // Determines if the element that needs the bounding box calculating is an Element or the Window
    // Necessitates the `as` syntax to ensure the compiler is happy
    if ((element as Element).getBoundingClientRect) {
      return (element as Element).getBoundingClientRect();
    } else {
      return {
        height: (element as Window).innerHeight,
        width: (element as Window).innerWidth,
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
      };
    }
  }

  /**
   * Gets the delta to adjust the scroll position by
   * @param dragY the Y position of the darg
   * @param thresholdHeight the calculated height of the top and bototm thresholds
   * @param topThreshold the threshold at the top of the page
   * @param bottomThreshold the threshold at the bototm of the page
   */
  private getScrollDeltaForDrag(dragY: number, thresholdHeight: number, topThreshold: number, bottomThreshold: number): number {
    let delta = 0;
    // Calculate how far past these thresholds the drag is; this controls the speed of the scroll
    // If drag is above the top threshold
    if (dragY < topThreshold) {
      const topThresholdPercent = (dragY - topThreshold) / thresholdHeight;
      delta = MAX_SCROLL_SPEED * topThresholdPercent;
      if (delta < -MAX_SCROLL_SPEED) {
        delta = -MAX_SCROLL_SPEED;
      }
    } else if (dragY > bottomThreshold) {
      // If the drag is below the bottom threshold
      const bottomThresholdPercent = (dragY - bottomThreshold) / thresholdHeight;
      delta = MAX_SCROLL_SPEED * bottomThresholdPercent;
      if (delta > MAX_SCROLL_SPEED) {
        delta = MAX_SCROLL_SPEED;
      }
    }
    // Return the caluclated delta
    return delta;
  }
}
