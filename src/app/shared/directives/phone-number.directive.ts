import { Directive, ElementRef, HostListener } from '@angular/core';
import { includes } from 'lodash-es';

@Directive({
  selector: '[appOnlyPhoneNumber]',
})
export class OnlyPhoneNumberDirective {
  private el: HTMLInputElement;
  // prettier-ignore
  private allowedKeys = [
    8, 46, // backspace and delete
    9, // tab
    13, // enter
    27, // escape
    35, 36, 37, 38, 39, 40, // end, home and arrows
    48, 49, 50, 51, 52, 53, 54, 55, 56, 57, // numbers
    96, 97, 98, 99, 100, 101, 102, 103, 104, 105, // numpad numbers
  ];

  constructor(public elementRef: ElementRef) {
    this.el = this.elementRef.nativeElement;
  }

  private applyCleanStringFilter() {
    let result = '';
    const filtered = (<string>this.el.value).trim();
    if (/^[\+]/.test(filtered)) {
      result += '+';
    }
    result += filtered.replace(/[\D]/gi, '');
    this.el.value = result;
  }

  @HostListener('blur')
  onBlur() {
    this.applyCleanStringFilter();
  }

  @HostListener('keydown', ['$event'])
  onKeydown(event) {
    const keyCode = event.which || event.keyCode;
    if (
      event.altKey ||
      event.keyCode === 229 ||
      (event.shiftKey && event.key !== '+') ||
      !(includes(this.allowedKeys, keyCode) || /[\+0-9]/.test(event.key))
    ) {
      event.preventDefault();
    }
  }
}
