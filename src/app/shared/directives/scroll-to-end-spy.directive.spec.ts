import { ElementRef } from '@angular/core';
import { ScrollToEndSpyDirective } from './scroll-to-end-spy.directive';

describe('ScrollToEndSpyDirective', () => {
  it('should create an instance', () => {
    const directive = new ScrollToEndSpyDirective({} as ElementRef);
    expect(directive).toBeTruthy();
  });

  it(`should return false if haven't got to the bottom`, () => {
    const directive = new ScrollToEndSpyDirective({} as ElementRef);
    expect(directive.reachedTheEnd).toBeFalsy();
  });
});
