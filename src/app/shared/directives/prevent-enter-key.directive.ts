import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appPreventEnterKeyDirective]',
})
export class PreventEnterKeyDirective {
  constructor() {}

  @HostListener('keydown', ['$event'])
  onKeydown(event) {
    const keyCode = event.which || event.keyCode;
    if (keyCode === 13) {
      // enter key
      event.preventDefault();
    }
  }
}
