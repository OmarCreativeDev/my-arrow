import { Directive, Input, HostListener } from '@angular/core';
import { NgForm } from '@angular/forms';

@Directive({
  selector: '[appFocusFirstInvalid]',
})
export class FocusFirstInvalidDirective {
  @Input()
  appFocusFirstInvalid: NgForm;
  @HostListener('submit', ['$event'])
  onSubmit(event) {
    if (!this.appFocusFirstInvalid.valid) {
      const firstInvalidElem: HTMLElement = <HTMLElement>(<any>event.target.querySelector('.ng-invalid'));
      if (firstInvalidElem) {
        firstInvalidElem.scrollIntoView(true);
        firstInvalidElem.focus();
      }
    }
  }
}
