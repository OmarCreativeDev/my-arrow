import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { CurrencyFormatDirective } from './currency-format.directive';

@Component({
  template: `
    <input type="text" appCurrencyFormat />
  `,
})
class TestCurrencyComponent {}

describe('Directive: OnlyNumbers', () => {
  let fixture: ComponentFixture<TestCurrencyComponent>;
  let inputEl: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestCurrencyComponent, CurrencyFormatDirective],
    });
    fixture = TestBed.createComponent(TestCurrencyComponent);
    inputEl = fixture.debugElement.query(By.css('input'));
  });

  it('should accept numbers', () => {
    const event: any = document.createEvent('CustomEvent');
    event.which = 49; // '49' represents number 1
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputEl.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalledTimes(0);
  });

  it('should not accept letters testing branch event.Keycode', () => {
    const event: any = document.createEvent('CustomEvent');
    event.keyCode = 40;
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    const element = <HTMLInputElement>inputEl.nativeElement;
    element.value = '0';
    spyOn(event, 'preventDefault');
    inputEl.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('should not accept letters', () => {
    const event: any = document.createEvent('CustomEvent');
    event.which = 88; // '88' represents letter x
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputEl.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it(`should filter content properly on blur`, () => {
    const event: any = document.createEvent('CustomEvent');
    event.initEvent('blur', true, true);
    const element = <HTMLInputElement>inputEl.nativeElement;
    element.value = '123123,312.98';
    inputEl.nativeElement.dispatchEvent(event);
    expect(element.value).toBe('123,123,312.98');
  });

  it(`should filter content properly with no dots`, () => {
    const event: any = document.createEvent('CustomEvent');
    event.initEvent('blur', true, true);
    const element = <HTMLInputElement>inputEl.nativeElement;
    element.value = '123123,312';
    inputEl.nativeElement.dispatchEvent(event);
    expect(element.value).toBe('123,123,312.0');
  });

  it(`should format properly if no number`, () => {
    const event: any = document.createEvent('CustomEvent');
    event.initEvent('blur', true, true);
    const element = <HTMLInputElement>inputEl.nativeElement;
    element.value = undefined;
    inputEl.nativeElement.dispatchEvent(event);
    expect(element.value).toBe('0.0');
  });
});
