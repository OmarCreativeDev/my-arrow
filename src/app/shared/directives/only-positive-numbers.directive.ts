import { Directive, ElementRef, HostListener } from '@angular/core';
import { includes } from 'lodash-es';
import { toNumber } from 'lodash';

@Directive({
  selector: '[appOnlyPositiveNumbers]',
})
export class OnlyPositiveNumbersDirective {
  private el: HTMLInputElement;
  // prettier-ignore
  private allowedKeys = [
    8, 46, // backspace and delete
    9, // tab
    13, // enter
    27, // escape
    35, 36, 37, 38, 39, 40, // end, home and arrows
    48, 49, 50, 51, 52, 53, 54, 55, 56, 57, // numbers
    96, 97, 98, 99, 100, 101, 102, 103, 104, 105, // numpad numbers
  ];

  constructor(public elementRef: ElementRef) {
    this.el = this.elementRef.nativeElement;
  }

  public static ensureNumber(value): number {
    return Math.abs(toNumber(String(value).replace(/,/gi, '')));
  }

  private applyCleanStringFilter() {
    this.el.value = (<string>this.el.value).trim().replace(/[^\d]/gi, '');
  }

  private isPastingWithKeyboard(event): boolean {
    return (event.metaKey || event.ctrlKey) && event.key === 'v';
  }

  @HostListener('blur')
  onBlur() {
    this.applyCleanStringFilter();
  }

  @HostListener('keydown', ['$event'])
  onKeydown(event) {
    const keyCode = event.which || event.keyCode;
    if (
      !this.isPastingWithKeyboard(event) &&
      (event.shiftKey ||
        event.altKey ||
        event.key === 'Dead' ||
        !includes(this.allowedKeys, keyCode) ||
        (parseInt(this.el.value, 10) === 0 && keyCode === 40))
    ) {
      event.preventDefault();
    }
  }
}
