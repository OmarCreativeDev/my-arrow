import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { NoLeadingZerosDirective } from './no-leading-zeros.directive';

@Component({
  template: `
    <input type="text" appNoLeadingZeros />
  `,
})
class TestNoLeadingZerosComponent {}

describe('Directive: NoLeadingZeros', () => {
  let fixture: ComponentFixture<TestNoLeadingZerosComponent>;
  let inputEl: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestNoLeadingZerosComponent, NoLeadingZerosDirective],
    });
    fixture = TestBed.createComponent(TestNoLeadingZerosComponent);
    inputEl = fixture.debugElement.query(By.css('input'));
  });

  it('should remove leading zeros from the input', () => {
    inputEl.nativeElement.value = '999';
    expect(inputEl.nativeElement.value).toBe('999');
    inputEl.nativeElement.value = '00999';
    inputEl.triggerEventHandler('keyup', null);
    fixture.detectChanges();
    expect(inputEl.nativeElement.value).toBe('999');
  });

  it('should allow trailing zeros in the input', () => {
    inputEl.nativeElement.value = '999';
    expect(inputEl.nativeElement.value).toBe('999');
    inputEl.nativeElement.value = '99900';
    inputEl.triggerEventHandler('keyup', null);
    fixture.detectChanges();
    expect(inputEl.nativeElement.value).toBe('99900');
  });

  it('should allow empty value in the input', () => {
    inputEl.nativeElement.value = '999';
    expect(inputEl.nativeElement.value).toBe('999');
    inputEl.nativeElement.value = undefined;
    inputEl.triggerEventHandler('keyup', null);
    fixture.detectChanges();
    expect(inputEl.nativeElement.value).toBe('');
  });

  it('should not allow a zero when the caret is in the first position', () => {
    const event: any = document.createEvent('CustomEvent');
    event.keyCode = 48; // '48' represents 0
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');

    inputEl.nativeElement.value = '';
    inputEl.nativeElement.dispatchEvent(event);

    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('should allow a zero when the caret is past the first position', () => {
    const event: any = document.createEvent('CustomEvent');
    event.keyCode = 48; // '48' represents 0
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');

    inputEl.nativeElement.value = '1';
    inputEl.nativeElement.selectionStart = 1;
    inputEl.nativeElement.dispatchEvent(event);

    expect(event.preventDefault).toHaveBeenCalledTimes(0);
  });
});
