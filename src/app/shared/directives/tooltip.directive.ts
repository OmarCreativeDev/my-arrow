import { Directive, ElementRef, HostListener, Input, OnChanges, OnDestroy, Renderer2, SimpleChanges } from '@angular/core';
import { forOwn } from 'lodash-es';

@Directive({
  selector: '[appTooltip]',
})
export class TooltipDirective implements OnChanges, OnDestroy {
  @Input()
  appTooltip: string;
  @Input()
  placement: string;
  @Input()
  delay: string;
  @Input()
  fixed: boolean = false;
  @Input()
  visible: boolean = true;
  @Input()
  customStyles: any;
  private tooltip: HTMLElement;
  private offset: number = 10;

  private get tooltipTitle(): string {
    return this.appTooltip;
  }

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  ngOnChanges(changes: SimpleChanges) {
    this.validateChangesOnVisible(changes);
    this.validateChangesOnFixed(changes);
  }

  private validateChangesOnVisible(changes: SimpleChanges): void {
    if (changes.visible) {
      if (this.fixed && changes.visible.currentValue && !changes.visible.previousValue) {
        this.show();
      } else if (!changes.visible.currentValue && changes.visible.previousValue) {
        this.hide();
      }
    }
  }

  private validateChangesOnFixed(changes: SimpleChanges): void {
    if (changes.fixed) {
      if (this.visible && changes.fixed.currentValue && !changes.fixed.previousValue) {
        this.show();
      } else if (!changes.fixed.currentValue && changes.fixed.previousValue) {
        this.hide();
      }
    }
  }

  ngOnDestroy() {
    if (this.tooltip) {
      this.renderer.removeChild(document.body, this.tooltip);
    }
  }

  @HostListener('mouseenter')
  onMouseEnter() {
    if (this.fixed) {
      return;
    }

    if (!this.tooltip && this.visible) {
      this.show();
    }
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    if (this.fixed) {
      return;
    }
    if (this.tooltip && this.visible) {
      this.hide();
    }
  }

  private show(): void {
    if (this.visible && this.tooltip) {
      return;
    }
    this.create();
    this.setPosition();
    this.renderer.addClass(this.tooltip, 'ng-tooltip-show');
  }

  private hide(): void {
    if (this.delay) {
      window.setTimeout(() => this.destroy(), parseInt(this.delay, 10));
    } else {
      this.destroy();
    }
  }

  private create(): void {
    this.tooltip = this.renderer.createElement('span');

    this.renderer.appendChild(
      this.tooltip,
      this.renderer.createText(this.tooltipTitle) // textNode
    );

    this.renderer.appendChild(document.body, this.tooltip);
    // this.renderer.appendChild(this.el.nativeElement, this.tooltip);

    this.renderer.addClass(this.tooltip, 'ng-tooltip');
    this.renderer.addClass(this.tooltip, `ng-tooltip-${this.placement}`);

    // delay
    this.renderer.setStyle(this.tooltip, '-webkit-transition', `opacity ${this.delay}ms`);
    this.renderer.setStyle(this.tooltip, '-moz-transition', `opacity ${this.delay}ms`);
    this.renderer.setStyle(this.tooltip, '-o-transition', `opacity ${this.delay}ms`);
    this.renderer.setStyle(this.tooltip, 'transition', `opacity ${this.delay}ms`);

    // Custom Styles
    if (this.customStyles) {
      forOwn(this.customStyles, (value, key) => {
        this.renderer.setStyle(this.tooltip, key, value);
      });
    }
  }

  private setPosition(): void {
    const hostPos = this.el.nativeElement.getBoundingClientRect();

    // tooltip
    const tooltipPos = this.tooltip.getBoundingClientRect();

    // window scroll top
    // getBoundingClientRect
    const scrollPos = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    let top, left;

    if (this.placement === 'top') {
      top = hostPos.top - tooltipPos.height - this.offset;
      left = hostPos.left + (hostPos.width - tooltipPos.width) / 2;
    }

    if (this.placement === 'bottom') {
      top = hostPos.bottom + this.offset;
      left = hostPos.left + (hostPos.width - tooltipPos.width) / 2;
    }

    if (this.placement === 'left') {
      top = hostPos.top + (hostPos.height - tooltipPos.height) / 2;
      left = hostPos.left - tooltipPos.width - this.offset;
    }

    if (this.placement === 'right') {
      top = hostPos.top + (hostPos.height - tooltipPos.height) / 2;
      left = hostPos.right + this.offset;
    }

    this.renderer.setStyle(this.tooltip, 'top', `${top + scrollPos}px`);
    this.renderer.setStyle(this.tooltip, 'left', `${left}px`);
  }

  private destroy(): void {
    if (this.tooltip) {
      this.renderer.removeClass(this.tooltip, 'ng-tooltip-show');
      this.renderer.removeChild(document.body, this.tooltip);
      this.tooltip = null;
    }
  }
}
