import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appScrollToEndSpy]',
  exportAs: 'appScrollToEndSpy',
})
export class ScrollToEndSpyDirective {
  reachedTheEnd: boolean = false;

  constructor(private elemRef: ElementRef) {}

  @HostListener('scroll')
  onScrollEvent() {
    const top = this.elemRef.nativeElement.scrollTop;
    const offSetHeight = this.elemRef.nativeElement.offsetHeight;
    const scrollHeight = this.elemRef.nativeElement.scrollHeight;
    /* istanbul ignore else */
    if (top > scrollHeight - offSetHeight - 1) {
      this.reachedTheEnd = true;
    }
  }
}
