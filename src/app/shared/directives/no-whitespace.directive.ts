import { Directive, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appNoWhitespace]',
})
export class NoWhitespaceDirective {
  constructor(private controlRef: NgControl) {}

  @HostListener('keydown', ['$event'])
  onKeydown(event) {
    const keyCode = event.which || event.keyCode;
    if (keyCode === 32) {
      // space key
      event.preventDefault();
    }
  }

  @HostListener('paste', ['$event'])
  onPaste(event) {
    event.stopPropagation();
    setTimeout(() => {
      // Removes whitespace on pasting
      this.controlRef.control.setValue(event.target.value.replace(/\s/g, ''));
    }, 1);
  }
}
