import { TooltipDirective } from './tooltip.directive';
import { Component, DebugElement } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

@Component({
  template: `
    <span appTooltip="Lorem ipsum." placement="top" [visible]="isVisible">Mock</span>
  `,
})
class TestTooltipComponent {
  public isVisible: boolean = true;
}

describe('TooltipDirective', () => {
  let component: TestTooltipComponent;
  let fixture: ComponentFixture<TestTooltipComponent>;
  let directive: TooltipDirective;
  let spanElement: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestTooltipComponent, TooltipDirective],
    });
    fixture = TestBed.createComponent(TestTooltipComponent);
    component = fixture.componentInstance;
    spanElement = fixture.debugElement.query(By.css('span'));
    directive = spanElement.injector.get(TooltipDirective);
    fixture.detectChanges();
  });

  it('should initialize a mock Component', () => {
    expect(component).toBeTruthy();
  });

  it('should have ToolTipDirective present', () => {
    expect(directive).toBeTruthy();
  });

  it('should display the tooltip when mouse enter the element', () => {
    const mouseEnter = new Event('mouseenter');
    spyOn(directive, 'onMouseEnter');
    spanElement.nativeElement.dispatchEvent(mouseEnter);
    expect(directive.onMouseEnter).toHaveBeenCalledTimes(1);
  });

  it('should hide the tooltip when mouse leaves the element', () => {
    const mouseLeave = new Event('mouseleave');
    spyOn(directive, 'onMouseLeave');
    spanElement.nativeElement.dispatchEvent(mouseLeave);
    expect(directive.onMouseLeave).toHaveBeenCalledTimes(1);
  });

  it('should listen to changes on the properties', () => {
    spyOn(directive, 'ngOnChanges');
    component.isVisible = false;
    fixture.detectChanges();
    expect(directive.ngOnChanges).toHaveBeenCalledTimes(1);
  });
});
