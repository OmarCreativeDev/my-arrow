import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { NgControl } from '@angular/forms';
import { NoWhitespaceDirective } from './no-whitespace.directive';

@Component({
  template: `
    <input type="text" appNoWhitespace />
  `,
})
class TestNoWhitespaceComponent {}

describe('NoWhitespaceDirective', () => {
  let fixture: ComponentFixture<TestNoWhitespaceComponent>;
  let inputEl: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestNoWhitespaceComponent, NoWhitespaceDirective],
      providers: [NgControl],
    });
    fixture = TestBed.createComponent(TestNoWhitespaceComponent);
    inputEl = fixture.debugElement.query(By.css('input'));
  });

  it('should not accept spaces', () => {
    const event: any = document.createEvent('CustomEvent');
    event.which = 32; // '32' represents space
    event.preventDefault = () => undefined;
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputEl.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalledTimes(1);
  });
});
