import { ScrollToElementDirective } from './scroll-to-element.directive';
import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

@Component({
  template: `
    <div class="parent" style="position: relative; height: 100px; overflow: scroll;">
      <div style="height: 200px;">Child 1</div>
      <div class="child" style="height: 300px;"><span parentElement=".parent" childElement=".child" appScrollToElement>Child 2</span></div>
    </div>
  `,
})
class TestScrollToElementComponent {}

describe('ScrollToElementDirective', () => {
  let fixture: ComponentFixture<TestScrollToElementComponent>;
  let parentEl: DebugElement;
  let directiveEl: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestScrollToElementComponent, ScrollToElementDirective],
    });
    fixture = TestBed.createComponent(TestScrollToElementComponent);
    parentEl = fixture.debugElement.query(By.css('div.parent'));
    directiveEl = fixture.debugElement.query(By.css('span[appScrollToElement]'));
  });

  it('should scroll the parent element so the child element is at the top when clicked', fakeAsync(() => {
    fixture.detectChanges();
    expect(parentEl.nativeElement.scrollTop).toBe(0);
    directiveEl.triggerEventHandler('click', null);
    tick();
    expect(parentEl.nativeElement.scrollTop).toBe(200);
  }));
});
