import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appScrollToElement]'
})
export class ScrollToElementDirective {
  @Input() public parentElement: string;
  @Input() public childElement: string;

  @HostListener('click')
  public scrollToElement() {
    const parentElement = document.querySelector(this.parentElement);
    const scrollToPosition = document.querySelector(this.childElement)['offsetTop'];

    // setting timeout to get the height of the element otherwise it is 0
    setTimeout(() => {
      parentElement.scrollTop = scrollToPosition;
    });
  }

}
