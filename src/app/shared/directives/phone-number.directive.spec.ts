import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { OnlyPhoneNumberDirective } from './phone-number.directive';

@Component({
  template: `
    <input type="text" appOnlyPhoneNumber />
  `,
})
class TestOnlyPhoneNumberComponent {}

describe('Directive: OnlyPhoneNumberDirective', () => {
  let fixture: ComponentFixture<TestOnlyPhoneNumberComponent>;
  let inputEl: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestOnlyPhoneNumberComponent, OnlyPhoneNumberDirective],
    });
    fixture = TestBed.createComponent(TestOnlyPhoneNumberComponent);
    inputEl = fixture.debugElement.query(By.css('input'));
  });

  it('should accept numbers', () => {
    const event: any = document.createEvent('CustomEvent');
    event.which = 49; // '49' represents number 1
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputEl.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalledTimes(0);
  });

  it('should not accept letters', () => {
    const event: any = document.createEvent('CustomEvent');
    event.which = 88; // '88' represents letter x
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputEl.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('should not accept letters testing branch event.Keycode', () => {
    const event: any = document.createEvent('CustomEvent');
    event.keyCode = 88; // '88' represents letter x
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputEl.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('should not accept shift other than keyCode 40', () => {
    const event: any = document.createEvent('CustomEvent');
    event.shiftKey = true;
    event.key = '7';
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputEl.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it(`should filter content on blur '+1234567sdfgh345678' should be '+1234567345678'`, () => {
    const event: any = document.createEvent('CustomEvent');
    event.initEvent('blur', true, true);
    const element = <HTMLInputElement>inputEl.nativeElement;
    element.value = '+1234567sdfgh345678';
    inputEl.nativeElement.dispatchEvent(event);
    expect(element.value).toBe('+1234567345678');
  });

  it(`should filter content on blur '1234567sdfgh345678' should be '1234567345678'`, () => {
    const event: any = document.createEvent('CustomEvent');
    event.initEvent('blur', true, true);
    const element = <HTMLInputElement>inputEl.nativeElement;
    element.value = '1234567sdfgh345678';
    inputEl.nativeElement.dispatchEvent(event);
    expect(element.value).toBe('1234567345678');
  });
});
