import { Directive, ElementRef, HostListener, AfterViewInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { WINDOW } from '../services/window.service';

@Directive({
  selector: '[appStickyHeader]',
})
export class StickyHeaderDirective implements AfterViewInit {
  public table: Element;
  public headerCreated: Element;
  public tableHeaderHeight;

  public thead: Element;
  public tbody: Element;
  public headRow: Element;

  private error: Error;

  constructor(@Inject(DOCUMENT) private document: Document, @Inject(WINDOW) public window: Window, private elemRef: ElementRef) {}

  ngAfterViewInit(): void {
    this.table = this.elemRef.nativeElement;
    this.thead = this.table.querySelector('thead');
    this.tbody = this.table.querySelector('tbody');
    try {
      this.structureRefactor();
    } catch (e) {}
  }

  @HostListener('window:scroll', [])
  onScrollEvent() {
    if (!this.error) {
      try {
        const tableBodyPos = this.getPos(this.table);
        const stickyClassSet = this.headerCreated.classList.contains('thead-sticky');

        const tableHeight = parseInt(this.window.getComputedStyle(this.table, null).height, 10);
        this.tableHeaderHeight = parseInt(this.window.getComputedStyle(this.headerCreated, null).height, 10);

        const tableEndLimit = tableBodyPos.y + tableHeight - 55 - this.tableHeaderHeight + (stickyClassSet ? this.tableHeaderHeight : 0);

        if (tableBodyPos.y <= this.headerCreated.scrollHeight + 55 && tableEndLimit > 0) {
          this.fixHeader();
        } else {
          if (stickyClassSet) {
            this.releaseHeader();
          }
        }
      } catch (e) {
        this.error = e;
        console.log('STICKY HEADER ERROR: ', e);
      }
    }
  }

  private structureRefactor() {
    this.table.removeChild(this.table.querySelector('thead'));
    this.headerCreated = this.document.createElement('table');
    this.setInitialStyle();
    Array.from(this.table.classList).forEach(classElement => {
      this.headerCreated.classList.add(classElement);
    });

    this.headerCreated.appendChild(this.thead);
    this.table.insertAdjacentElement('beforebegin', this.headerCreated);
    this.headRow = this.headerCreated.querySelector('thead>tr');
  }

  private setInitialStyle() {
    this.headerCreated.classList.add('thead-normal');
    this.table.classList.add('sticky-body');
  }

  private fixHeader() {
    const totalHeaderWidth = this.window.getComputedStyle(this.table, null).width;
    this.headerCreated.setAttribute('style', 'width: ' + totalHeaderWidth + '; left: ' + this.tbody.getBoundingClientRect().left + 'px');
    this.headerCreated.classList.remove('thead-normal');
    this.headerCreated.classList.add('thead-sticky');
    this.table.setAttribute('style', 'margin-top: ' + (this.tableHeaderHeight + 15) + 'px; ');
  }

  private releaseHeader() {
    this.headerCreated.classList.add('thead-normal');
    this.headerCreated.classList.remove('thead-sticky');
    this.table.removeAttribute('style');
  }

  private getPos(el: Element) {
    const rect = el.getBoundingClientRect();
    return { x: rect.left, y: rect.top };
  }
}
