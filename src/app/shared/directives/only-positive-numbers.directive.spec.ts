import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { OnlyPositiveNumbersDirective } from './only-positive-numbers.directive';

@Component({
  template: `
    <input type="text" appOnlyPositiveNumbers />
  `,
})
class TestOnlyPositiveNumbersComponent {}

describe('Directive: OnlyNumbers', () => {
  let fixture: ComponentFixture<TestOnlyPositiveNumbersComponent>;
  let inputEl: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestOnlyPositiveNumbersComponent, OnlyPositiveNumbersDirective],
    });
    fixture = TestBed.createComponent(TestOnlyPositiveNumbersComponent);
    inputEl = fixture.debugElement.query(By.css('input'));
  });

  it('should accept numbers', () => {
    const event: any = document.createEvent('CustomEvent');
    event.which = 49; // '49' represents number 1
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputEl.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalledTimes(0);
  });

  it('should not accept letters testing branch event.Keycode', () => {
    const event: any = document.createEvent('CustomEvent');
    event.keyCode = 88; // '88' represents letter x
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputEl.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('should not accept letters testing branch event.Keycode', () => {
    const event: any = document.createEvent('CustomEvent');
    event.keyCode = 40;
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    const element = <HTMLInputElement>inputEl.nativeElement;
    element.value = '0';
    spyOn(event, 'preventDefault');
    inputEl.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('should not accept letters.', () => {
    const event: any = document.createEvent('CustomEvent');
    event.which = 88; // '88' represents letter x
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputEl.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it(`should format properly on blur`, () => {
    const event: any = document.createEvent('CustomEvent');
    event.initEvent('blur', true, true);
    const element = <HTMLInputElement>inputEl.nativeElement;
    element.value = '-12313';
    inputEl.nativeElement.dispatchEvent(event);
    expect(element.value).toBe('12313');
  });

  it(`should format properly ensureNumber()`, () => {
    const testing = OnlyPositiveNumbersDirective.ensureNumber('13123,123,12313.312');
    expect(testing).toBe(1312312312313.312);
  });
});
