import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { OnlyAlphaCharactersDirective } from './only-alpha-characters.directive';

@Component({
  template: `
    <input class="normal" type="text" appOnlyAlphaCharacters /> <input class="titlecase" titlecase appOnlyAlphaCharacters />
  `,
})
class TestOnlyAlphaCharactersComponent {}

describe('Directive: OnlyAlphaCharacters', () => {
  let fixture: ComponentFixture<TestOnlyAlphaCharactersComponent>;
  let inputElNormal: DebugElement;
  let inputElTitlecase: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestOnlyAlphaCharactersComponent, OnlyAlphaCharactersDirective],
    });
    fixture = TestBed.createComponent(TestOnlyAlphaCharactersComponent);
    inputElNormal = fixture.debugElement.query(By.css('.normal'));
    inputElTitlecase = fixture.debugElement.query(By.css('.titlecase'));
  });

  it('should accept letters', () => {
    const event: any = document.createEvent('CustomEvent');
    event.key = 'x'; // number 1
    event.which = 88; // '88' represents letter x
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputElNormal.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalledTimes(0);
  });

  it('should not accept numbers', () => {
    const event: any = document.createEvent('CustomEvent');
    event.key = '1'; // number 1
    event.which = 49; // '49' represents number 1
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputElNormal.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalled();
  });

  it('should accept letters testing branch event.Keycode', () => {
    const event: any = document.createEvent('CustomEvent');
    event.keyCode = 88; // '88' represents letter x
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputElNormal.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalledTimes(0);
  });

  it('should accept ALT + N for Ñ', () => {
    const event: any = document.createEvent('CustomEvent');
    event.altKey = true;
    event.key = 'n';
    event.preventDefault = () => {};
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputElNormal.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalledTimes(0);
  });

  it(`should filter content on blur 'this   is ña 1234 test' should be filtered as 'this is ña test'`, () => {
    const event: any = document.createEvent('CustomEvent');
    event.initEvent('blur', true, true);
    const element = <HTMLInputElement>inputElNormal.nativeElement;
    element.value = 'this   is ña 1234 test';
    inputElNormal.nativeElement.dispatchEvent(event);
    expect(element.value).toBe('this is ña test');
  });

  it(`should filter and titlecase content on blur 'this   is a 1234 test' should be filtered as 'This Is A Test'`, () => {
    const event: any = document.createEvent('CustomEvent');
    event.initEvent('blur', true, true);
    const element = <HTMLInputElement>inputElTitlecase.nativeElement;
    element.value = 'this   is a 1234 test';
    inputElTitlecase.nativeElement.dispatchEvent(event);
    expect(element.value).toBe('This Is A Test');
  });
});
