import { Directive, ElementRef, HostListener } from '@angular/core';
import * as math from 'mathjs';

@Directive({
  selector: '[appNoLeadingZeros]',
})
export class NoLeadingZerosDirective {
  private el: HTMLInputElement;

  private zeroKeys: Array<number> = [
    48, // numbers
    96, // numpad numbers
  ];

  constructor(public elementRef: ElementRef) {
    this.el = this.elementRef.nativeElement;
  }

  @HostListener('keydown', ['$event'])
  onKeydown(event) {
    const keyCode = event.which || event.keyCode;
    /* istanbul ignore else */
    if (this.el.selectionStart === 0 && this.zeroKeys.includes(keyCode)) {
      event.preventDefault();
    }
  }

  @HostListener('keyup')
  onKeyup() {
    // Using math.bignumber to prevent exponential notation in the input field
    this.el.value = this.el.value && !isNaN(this.el.value as any) ? math.bignumber(this.el.value).toFixed() : '';
  }
}
