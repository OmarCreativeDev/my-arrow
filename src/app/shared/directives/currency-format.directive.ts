import { Directive, ElementRef, HostListener } from '@angular/core';
import { includes } from 'lodash-es';
import { toNumber } from 'lodash';

@Directive({
  selector: '[appCurrencyFormat]',
})
export class CurrencyFormatDirective {
  private static COMMA_REGEX = /,/gi;
  private static NO_DIGIT_OR_DOT = /[^\d\.]/gi;
  private el: HTMLInputElement;

  // prettier-ignore
  private allowedKeys = [
    8, 46, // backspace and delete
    9, // tab
    13, // enter
    27, // escape
    35, 36, 37, 38, 39, 40, // end, home and arrows
    48, 49, 50, 51, 52, 53, 54, 55, 56, 57, // numbers
    96, 97, 98, 99, 100, 101, 102, 103, 104, 105, // numpad numbers
    188, // comma
    190, // dot
  ];

  constructor(public elementRef: ElementRef) {
    this.el = this.elementRef.nativeElement;
  }

  private static ensureNumber(value): number {
    return Math.abs(toNumber(String(value).replace(CurrencyFormatDirective.COMMA_REGEX, '')));
  }

  private static removeAllButLastDot(value: string): string {
    const parts = value.split('.');
    if (parts[1] === undefined) {
      return value;
    } else {
      return parts.slice(0, -1).join('') + '.' + parts.slice(-1);
    }
  }

  private static applyCleanStringFilter(value: string) {
    return value.trim().replace(CurrencyFormatDirective.NO_DIGIT_OR_DOT, '');
  }

  static moneyFormatNumber(input: string): string {
    let filtered = this.removeAllButLastDot(input);
    filtered = this.applyCleanStringFilter(filtered);
    let numberToFormat = this.ensureNumber(filtered);

    if (!numberToFormat || numberToFormat <= 0) {
      numberToFormat = 0;
    }
    return new Intl.NumberFormat('en-EN', { style: 'decimal', minimumFractionDigits: 1, maximumFractionDigits: 5 }).format(numberToFormat);
  }

  @HostListener('blur')
  @HostListener('change')
  doFormat() {
    this.el.value = CurrencyFormatDirective.moneyFormatNumber(this.el.value);
  }

  @HostListener('keydown', ['$event'])
  onKeydown(event) {
    const keyCode = event.which || event.keyCode;
    if (
      event.shiftKey ||
      event.altKey ||
      event.key === 'Dead' ||
      !includes(this.allowedKeys, keyCode) ||
      (parseInt(this.el.value, 10) === 0 && keyCode === 40)
    ) {
      event.preventDefault();
    }
  }
}
