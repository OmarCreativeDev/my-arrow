import { Directive, ElementRef, HostListener } from '@angular/core';
import { includes } from 'lodash-es';

@Directive({
  selector: '[appOnlyAlphaCharacters]',
})
export class OnlyAlphaCharactersDirective {
  private static WORD_OR_SPACES_REGEX = /\w\S*/g;
  private static ALPHA_AND_SPACE_REGEX = /[^a-z\sñç']+/gi;
  private static SINGLE_ALPHA_AND_SPACE_REGEX = /[a-z\sñç']/i;
  private static TWO_OR_MORE_SPACES_REGEX = /\s{2,}/g;
  private static C_N_REGEX = /[cn]/i;

  private el: HTMLInputElement;
  // prettier-ignore
  private allowedKeys = [
    8, 46, // backspace and delete
    9, // tab
    13, // enter
    27, // escape
    32, // space
    35, 36, 37, 38, 39, 40, // end, home and arrows
  ];

  constructor(public elementRef: ElementRef) {
    this.el = this.elementRef.nativeElement;
  }

  public static toTitleCase(str) {
    return str.replace(OnlyAlphaCharactersDirective.WORD_OR_SPACES_REGEX, function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  }

  private applyTitleCaseFilter() {
    this.el.value = OnlyAlphaCharactersDirective.toTitleCase(this.el.value);
  }

  private applyCleanStringFilter() {
    this.el.value = (<string>this.el.value)
      .trim()
      .replace(OnlyAlphaCharactersDirective.ALPHA_AND_SPACE_REGEX, '')
      .replace(OnlyAlphaCharactersDirective.TWO_OR_MORE_SPACES_REGEX, ' ');
  }

  @HostListener('blur')
  onBlur() {
    if (this.el.hasAttribute('titlecase')) {
      this.applyTitleCaseFilter();
    }
    this.applyCleanStringFilter();
  }

  @HostListener('keydown', ['$event'])
  onKeydown(event) {
    const keyCode = event.which || event.keyCode;
    if (
      !(
        includes(this.allowedKeys, keyCode) ||
        (event.altKey && OnlyAlphaCharactersDirective.C_N_REGEX.test(event.key)) ||
        OnlyAlphaCharactersDirective.SINGLE_ALPHA_AND_SPACE_REGEX.test(event.key)
      )
    ) {
      event.preventDefault();
    }
  }
}
