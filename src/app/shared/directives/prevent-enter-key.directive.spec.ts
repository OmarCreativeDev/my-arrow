import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { NgControl } from '@angular/forms';
import { PreventEnterKeyDirective } from './prevent-enter-key.directive';

@Component({
  template: `
    <input type="text" appPreventEnterKeyDirective />
  `,
})
class TestNoEnterKeyComponent {}

describe('NoWhitespaceDirective', () => {
  let fixture: ComponentFixture<TestNoEnterKeyComponent>;
  let inputEl: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestNoEnterKeyComponent, PreventEnterKeyDirective],
      providers: [NgControl],
    });
    fixture = TestBed.createComponent(TestNoEnterKeyComponent);
    inputEl = fixture.debugElement.query(By.css('input'));
  });

  it('should not accept enter key', () => {
    const event: any = document.createEvent('CustomEvent');
    event.which = 13; // '13' represents enter
    event.preventDefault = () => undefined;
    event.initEvent('keydown', true, true);
    spyOn(event, 'preventDefault');
    inputEl.nativeElement.dispatchEvent(event);
    expect(event.preventDefault).toHaveBeenCalledTimes(1);
  });
});
