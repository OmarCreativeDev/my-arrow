import { ScrollDuringDragDirective, HOT_ZONE_PERCENT } from './scroll-during-drag.directive';
import { Component, DebugElement } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DomService } from '@app/shared/services/dom.service';

const ELEMENT_HEIGHT = 10000;

@Component({
  template: `<div style="height: 200px; overflow-y: scroll;" class="scroll">
      <div style="height: ${ELEMENT_HEIGHT}px" id="scroll-element" appScrollDuringDrag></div>
    </div>`,
})
class TestScrollDuringDragComponent {}

describe('ScrollDuringDragDirective', () => {
  let directive: ScrollDuringDragDirective;
  let dragulaService: DragulaService;

  describe('window', () => {
    beforeEach(() => {
      dragulaService = new DragulaService();
      directive = new ScrollDuringDragDirective(dragulaService, new DomService());
      document.body.style.height = '10000px';
    });

    it('should create an instance', () => {
      expect(directive).toBeTruthy();
    });

    it('should only handle the mouse move if an object is being dragged', () => {
      const handleDragSpy = spyOn(directive, 'handleDrag');
      directive.dragging = false;
      directive.move({} as MouseEvent);
      expect(handleDragSpy).not.toHaveBeenCalled();
    });

    const testCases = [
      {
        description: `should not scroll if starting scroll position is 0 and drag moves up`,
        dragY: 0,
        scrollY: 0,
        scrollDelta: 0,
      },
      {
        description: `should not scroll if starting scroll position is at the bottom and move event was 1px before the top threshold`,
        dragY: window.innerHeight * HOT_ZONE_PERCENT + 1,
        scrollY: window.innerHeight,
        scrollDelta: 0,
      },
      {
        description: `should not scroll if starting scroll position is at the top and move event was 1px before the bottom threshold`,
        dragY: window.innerHeight - window.innerHeight * HOT_ZONE_PERCENT - 1,
        scrollY: 0,
        scrollDelta: 0,
      },
      {
        description: `should scroll by max speed if starting scroll position is at the bottom and move event was at the top`,
        dragY: 0,
        scrollY: window.innerHeight,
        scrollDelta: -8,
      } /**,
      {
        description: `should scroll by max speed if starting scroll position is at the top and move event was at the bottom`,
        dragY: window.innerHeight,
        scrollY: 0,
        scrollDelta: 8,
      },**/,
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        window.scrollTo(0, testCase.scrollY);
        directive.dragging = true;
        directive.move({ clientY: testCase.dragY } as MouseEvent);
        const scrollTopAfter = window.scrollY;
        expect(scrollTopAfter - testCase.scrollY).toEqual(testCase.scrollDelta);
      });
    }

    it('should set `dragging` to true and cache the dragged element', () => {
      const element = document.querySelector('.something');
      const dragParams = [undefined, [element, undefined]];
      dragulaService.drag.emit(dragParams);
      expect(directive.dragging).toBeTruthy();
    });

    it('should set `dragging` to false and unset the dragged element', () => {
      directive.dragging = true;
      directive.draggingElement = {} as Element;
      dragulaService.drop.emit();
      expect(directive.dragging).toBeFalsy();
      expect(directive.draggingElement).toBeUndefined();
    });
  });

  describe('element', () => {
    let component: TestScrollDuringDragComponent;
    let fixture: ComponentFixture<TestScrollDuringDragComponent>;
    let scrollElement: DebugElement;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [TestScrollDuringDragComponent, ScrollDuringDragDirective],
        providers: [DragulaService, DomService],
      });
      fixture = TestBed.createComponent(TestScrollDuringDragComponent);
      component = fixture.componentInstance;
      dragulaService = TestBed.get(DragulaService);
      scrollElement = fixture.debugElement.query(By.directive(ScrollDuringDragDirective));
    });

    it('should create', () => {
      expect(component).toBeTruthy();
    });

    const testCases = [
      {
        description: `should scroll the parent .scroll element if starting scroll position is at the bottom and drag moves up`,
        dragY: 0,
        scrollY: ELEMENT_HEIGHT,
        scrollDelta: -8,
      },
      {
        description: `should scroll the parent .scroll element if starting scroll position is at the top and drag moves down`,
        dragY: window.innerHeight,
        scrollY: 0,
        scrollDelta: 8,
      },
    ];
    for (const testCase of testCases) {
      it(`${testCase.description}`, () => {
        const directiveInstance = scrollElement.injector.get(ScrollDuringDragDirective);
        const scrollingElement = fixture.nativeElement.children[0];
        scrollingElement.scrollTop = testCase.scrollY;
        const scrollTopBefore = scrollingElement.scrollTop;
        // Now emit data through drag
        dragulaService.drag.emit([undefined, scrollElement.nativeElement, undefined]);
        // dispatch mousemove
        const move = new MouseEvent('mousemove', {
          clientY: testCase.dragY,
        });
        spyOn(scrollingElement, 'getBoundingClientRect').and.callFake(() => {
          return {
            bottom: 0,
            height: 100,
            left: 0,
            right: 0,
            top: 0,
            width: 0,
          };
        });
        directiveInstance.handleDrag(move, scrollingElement);
        fixture.detectChanges();
        // calc diff
        const scrollTopAfter = scrollingElement.scrollTop;
        expect(scrollTopAfter - scrollTopBefore).toEqual(testCase.scrollDelta);
      });
    }
  });
});
