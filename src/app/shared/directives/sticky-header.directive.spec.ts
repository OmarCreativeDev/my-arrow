import { Component, DebugElement } from '@angular/core';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DOCUMENT } from '@angular/common';
import { DomService } from '@app/shared/services/dom.service';
import { StickyHeaderDirective } from './sticky-header.directive';
import { WINDOW } from '../services/window.service';

@Component({
  template: `
    <header>THIS IS A HEADER</header>
    <table style="height:1000px" appStickyHeader class="mockClass" id="testingTable">
      <thead>
        <tr>
          <th>Title1</th>
          <th>Title2</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>Content1.1</td>
          <td>Content1.2</td>
        </tr>
        <tr>
          <td>Content2.1</td>
          <td>Content2.2</td>
        </tr>
        <tr>
          <td>Content2.1</td>
          <td>Content2.2</td>
        </tr>
        <tr>
          <td>Content2.1</td>
          <td>Content2.2</td>
        </tr>
        <tr>
          <td>Content2.1</td>
          <td>Content2.2</td>
        </tr>
        <tr>
          <td>Content2.1</td>
          <td>Content2.2</td>
        </tr>
        <tr>
          <td>Content2.1</td>
          <td>Content2.2</td>
        </tr>
        <tr>
          <td>Content2.1</td>
          <td>Content2.2</td>
        </tr>
      </tbody>
    </table>
  `,
})
class TestStickyHeaderDirectiveComponent {}

describe('StickyHeaderDirective', () => {
  let directive: StickyHeaderDirective;
  let stickyElement: DebugElement;
  let component: TestStickyHeaderDirectiveComponent;
  let fixture: ComponentFixture<TestStickyHeaderDirectiveComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TestStickyHeaderDirectiveComponent, StickyHeaderDirective],
      providers: [DomService, { provide: WINDOW, useValue: window }, { provide: DOCUMENT, useValue: document }],
    });

    fixture = TestBed.createComponent(TestStickyHeaderDirectiveComponent);
    component = fixture.componentInstance;
    stickyElement = fixture.debugElement.query(By.directive(StickyHeaderDirective));
    directive = stickyElement.injector.get(StickyHeaderDirective);
  });

  it('should initialize a mock Component', () => {
    expect(component).toBeTruthy();
  });

  it('should have a Table with appStickyHeader attached', () => {
    expect(stickyElement).toBeTruthy();
  });

  describe('should set important variables on ngAfterViewInit(): ', () => {
    beforeEach(() => {
      directive.ngAfterViewInit();
    });

    const varsToCheck = ['table', 'headerCreated', 'thead', 'tbody', 'headRow'];

    varsToCheck.forEach(variable => {
      it(`directive.${variable} should  be defined`, () => {
        expect(directive[variable]).toBeDefined();
      });
    });
  });

  it('should call onScrollEvent on window scroll', () => {
    const spyEvent = spyOn(directive, 'onScrollEvent');
    window.dispatchEvent(new UIEvent('scroll', { detail: 400 }));
    expect(spyEvent).toHaveBeenCalled();
  });

  describe('should handle scroll events correctly: ', () => {
    let spyEvent: jasmine.Spy;

    beforeEach(() => {
      directive.ngAfterViewInit();
    });

    it('Short scroll and release fix header', () => {
      spyEvent = spyOn(directive, 'onScrollEvent').and.callThrough();
      directive.onScrollEvent();
    });

    it('Large Scroll large document and fix header', () => {
      spyEvent = spyOn(directive, 'onScrollEvent').and.callThrough();
      window.document.body.style.height = '10000px';

      window.dispatchEvent(new UIEvent('scroll', { detail: 500 }));
      window.scrollTo(0, 500);
      window.dispatchEvent(new UIEvent('scroll', { detail: 1000 }));
      window.scrollTo(0, 1000);

      directive.onScrollEvent();
    });

    afterEach(() => {
      expect(spyEvent).toHaveBeenCalled();
    });
  });
});
