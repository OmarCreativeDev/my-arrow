import { Directive, ElementRef, Inject, Renderer2 } from '@angular/core';
import { fromEvent } from 'rxjs';
import { WINDOW } from '@app/shared/services/window.service';

@Directive({
  selector: '[appScrollShadow]',
})
export class ScrollShadowDirective {
  constructor(@Inject(WINDOW) public window: any, public hostElement: ElementRef, public renderer: Renderer2) {
    const htmlEl = hostElement.nativeElement;
    const scroll$ = fromEvent(htmlEl, 'scroll');
    scroll$.subscribe(($event: Event) => {
      const el = hostElement.nativeElement;
      const childEl = hostElement.nativeElement.children[0];
      const elValue = el.getBoundingClientRect().x;
      const childElValue = childEl.getBoundingClientRect().x;
      // ev stays same, cv goes down
      // check if there is space to scroll left
      if (elValue > childElValue) {
        renderer.addClass(el.parentNode, 'show-right');
        renderer.addClass(el.parentNode, 'show-left');
      } else {
        renderer.removeClass(el.parentNode, 'show-left');
      }
      if (childEl.getBoundingClientRect().right - elValue - 1 <= el.getBoundingClientRect().width) {
        renderer.removeClass(el.parentNode, 'show-right');
      }
    });
  }
}
