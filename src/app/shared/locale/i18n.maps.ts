export class WeekdaysMap {
  public static initials: any = {
    '0': 'S',
    '1': 'M',
    '2': 'T',
    '3': 'W',
    '4': 'T',
    '5': 'F',
    '6': 'S',
  };
}

export class Status {
  public static orderLineStatusMap: any = {
    OPEN: 'Open',
    CLOSED: 'Closed',
    SHIPPED: 'Shipped',
    PENDING: 'Pending',
    PREPARING_FOR_SHIPPING: 'Preparing For Shipping',
    CANCELLED: 'Cancelled',
  };

  public static purchaseOrderStatusMap: any = {
    OPEN: 'Open',
    CLOSED: 'Closed',
    PENDING: 'Pending',
    SHIPPED: 'Shipped',
  };
}
