import { of, Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, finalize, map, share, tap } from 'rxjs/operators';
/** temp fork from  https://github.com/czeckd/angular-svg-icon **/
import { Inject, Optional, SkipSelf } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class SvgIconRegistryService {
  private iconsByUrl = new Map<string, SVGElement>();
  private iconsLoadingByUrl = new Map<string, Observable<SVGElement>>();

  constructor(private http: HttpClient, private fallbackIconUrl: string) {}

  loadSvg(url: string): Observable<SVGElement> {
    if (this.iconsByUrl.has(url)) {
      return of(this.iconsByUrl.get(url));
    } else if (this.iconsLoadingByUrl.has(url)) {
      return this.iconsLoadingByUrl.get(url);
    } else {
      const o = this.http.get(url, { responseType: 'text' }).pipe(
        map(svg => {
          const div = document.createElement('DIV');
          div.innerHTML = svg;
          return <SVGElement>div.querySelector('svg');
        }),
        catchError(error => {
          if (this.fallbackIconUrl) {
            return this.loadSvg(this.fallbackIconUrl);
          } else {
            return observableThrowError(error);
          }
        }),
        tap(svg => {
          this.iconsByUrl.set(url, svg);
        }),
        finalize(() => {
          this.iconsLoadingByUrl.delete(url);
        }),
        share()
      );
      this.iconsLoadingByUrl.set(url, o);
      return o;
    }
  }

  unloadSvg(url: string) {
    if (this.iconsByUrl.has(url)) {
      this.iconsByUrl.delete(url);
    }
  }
}

export function SVG_ICON_REGISTRY_PROVIDER_FACTORY(parentRegistry: SvgIconRegistryService, http: HttpClient, fallBackIconUrl: string) {
  return parentRegistry || new SvgIconRegistryService(http, fallBackIconUrl);
}

export const SVG_ICON_REGISTRY_PROVIDER = {
  provide: SvgIconRegistryService,
  deps: [[new Optional(), new SkipSelf(), SvgIconRegistryService], HttpClient, [new Inject('FALLBACK_ICON')]],
  useFactory: SVG_ICON_REGISTRY_PROVIDER_FACTORY,
};
