/** temp fork from  https://github.com/czeckd/angular-svg-icon **/
import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvgIconComponent } from '@app/shared/svg-icon/svg-icon.component';
import { SVG_ICON_REGISTRY_PROVIDER } from '@app/shared/svg-icon/svg-icon-registry.service';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [SvgIconComponent],
  providers: [
    {provide: 'FALLBACK_ICON', useValue: null},
    SVG_ICON_REGISTRY_PROVIDER],
  exports: [SvgIconComponent],
})
export class SvgIconModule {
  static forRoot(fallbackIcon: string): ModuleWithProviders {
    return {
      ngModule: SvgIconModule,
      providers: [
        {provide: 'FALLBACK_ICON', useValue: fallbackIcon},
        SVG_ICON_REGISTRY_PROVIDER],
    };
  }
}
