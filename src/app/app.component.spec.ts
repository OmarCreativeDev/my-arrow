import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NavigationEnd, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { BackdropComponent } from '@app/layout/components/backdrop/backdrop.component';
import { BackdropService } from '@app/shared/services/backdrop.service';
import { ModalService } from '@app/shared/services/modal.service';
import { TitleService } from '@app/shared/services/title.service';
import { AppComponent } from './app.component';

export class BackdropServiceMock {
  public isVisible = of(true);
  public hide(): void {}
  public show(): void {}
}

export class ModalServiceMock {
  public isVisible = of(true);
  public hide(): void {}
  public show(): void {}
}

class MockServices {
  public events = of(new NavigationEnd(0, 'http://localhost:4200/', 'http://localhost:4200/dashboard'));
}

class MockToastrService {
  public overlayContainer;
}

class TitleServiceMock {
  public init(): void {}
}

describe('AppComponent', () => {
  let app: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let router: Router;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([])],
      declarations: [AppComponent, BackdropComponent],
      providers: [
        { provide: BackdropService, useClass: BackdropServiceMock },
        { provide: ModalService, useClass: ModalServiceMock },
        { provide: Router, useClass: MockServices },
        { provide: ToastrService, useClass: MockToastrService },
        { provide: TitleService, useClass: TitleServiceMock },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;
    router = fixture.debugElement.injector.get(Router);
    fixture.detectChanges();
  });

  it('should create the app', async(() => {
    expect(app).toBeTruthy();
  }));

  it('should scroll to top of page when it changes route', () => {
    spyOn(router, 'events').and.returnValue(of([new NavigationEnd(1, '/', '/dashboard')]));
    spyOn(app, 'scrollToTop');
    app.ngOnInit();
    expect(app.scrollToTop).toHaveBeenCalled();
  });

  it(' it should not scroll to top of page if is not a NavigationEnd event', () => {
    spyOn(router, 'events').and.returnValue(of([new Event(``)]));
    spyOn(app, 'scrollToTop');
    app.ngOnInit();
    expect(app.scrollToTop).toHaveBeenCalled();
  });
});
