import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExampleDialogComponent, StyleGuideComponent } from './pages/style-guide/style-guide.component';
import { SharedModule } from '../shared/shared.module';
import { StyleGuideRoutingModule } from './style-guide-routing.module';
import { FormsModule } from '@angular/forms';
import { CoreModule } from '@app/core/core.module';
import { LayoutModule } from '../layout/layout.module';
import { RouterModule } from '@angular/router';
import { StyleGuideGuard } from './style-guide.guard';

@NgModule({
  imports: [CommonModule, LayoutModule, CoreModule, SharedModule, FormsModule, StyleGuideRoutingModule, RouterModule],
  declarations: [StyleGuideComponent, ExampleDialogComponent],
  entryComponents: [ExampleDialogComponent],
  providers: [StyleGuideGuard],
})
export class StyleGuideModule {}
