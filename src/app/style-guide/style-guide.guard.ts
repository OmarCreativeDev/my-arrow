import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { environment } from '@env/environment';

@Injectable()
export class StyleGuideGuard implements CanActivate {
  constructor(private router: Router) {}

  public canActivate(): Observable<boolean> {
    if (!environment.production) {
      return of(true);
    } else {
      this.router.navigateByUrl('/login');
      return of(false);
    }
  }
}
