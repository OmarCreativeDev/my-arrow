/** temp fork from  https://github.com/czeckd/angular-svg-icon **/
import { Component, Inject, ViewChild } from '@angular/core';
import { IPaginationState, ISortableItem, ISortCriteron, ISelectableOption } from '../../../shared/shared.interfaces';
import * as moment from 'moment';
import { DatePickerComponent } from '@app/shared/components/date-picker/date-picker.component';
import { APP_DIALOG_DATA, Dialog, DialogService } from '@app/core/dialog/dialog.service';
import { EmailModalComponent, DEFAULT_MODAL_INTRO, DEFAULT_MODAL_TITLE } from '@app/shared/components/email-modal/email-modal.component';
import { IOrderLine, IOrderLineStatus, IPurchaseOrderStatus } from '@app/shared/shared.interfaces';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'app-example-dialog',
  template: `
    <app-dialog-panel (close)="onDismiss()">
      <h2>Dialog Title</h2>
      <div class="form">
        <div class="form-item">
          <div class="form-item__label"><label class="label">Change it:</label></div>
          <div class="form-item__control"><input type="text" class="input" [(ngModel)]="data" /></div>
        </div>
      </div>
      <div class="cta-container">
        <ul class="cta-container__list justify-flex-end">
          <li class="cta-container__list-item"><button (click)="onDismiss()" class="btn btn-secondary" i18n>Cancel</button></li>
          <li class="cta-container__list-item"><button (click)="onConfirm()" class="btn btn-primary" i18n>Confirm</button></li>
        </ul>
      </div>
    </app-dialog-panel>
  `,
})
export class ExampleDialogComponent {
  constructor(public dialog: Dialog<ExampleDialogComponent>, @Inject(APP_DIALOG_DATA) public data: string) {}

  onConfirm(): void {
    this.dialog.close(this.data);
  }

  onDismiss(): void {
    this.dialog.close();
  }
}

@Component({
  selector: 'app-style-guide',
  templateUrl: './style-guide.component.html',
  styleUrls: ['./style-guide.component.scss'],
})
export class StyleGuideComponent {
  @ViewChild('inactiveDatePicker')
  inactiveDatePicker: DatePickerComponent;

  constructor(private dialogService: DialogService) {}

  public sections = [
    {
      label: 'Color Mappings',
      slug: 'color-mappings',
    },
    {
      label: 'Grids',
      slug: 'grids',
    },
    {
      label: 'Headings',
      slug: 'headings',
    },
    {
      label: 'Icons',
      slug: 'icons',
    },
    {
      label: 'Buttons and CTAs',
      slug: 'buttons',
    },
    {
      label: 'Links',
      slug: 'links',
    },
    {
      label: 'Form Checkbox/Radios',
      slug: 'checkbox-radios',
    },
    {
      label: 'Form Inputs',
      slug: 'inputs',
    },
    {
      label: 'Tabs',
      slug: 'tabs',
    },
    {
      label: 'Pagination',
      slug: 'pagination',
    },
    {
      label: 'Tables',
      slug: 'tables',
    },
    {
      label: 'Modals/Dialogs',
      slug: 'modals-dialogs',
    },
    {
      label: 'Messaging',
      slug: 'messaging',
    },
    {
      label: 'Loader',
      slug: 'loader',
    },
    {
      label: 'Sortable Lists',
      slug: 'sortable-lists',
    },
    {
      label: 'Subtotals',
      slug: 'subtotals',
    },
    {
      label: 'Property Lists',
      slug: 'property-lists',
    },
    {
      label: 'Tooltip',
      slug: 'tooltip',
    },
  ];

  public colorMappings = [
    'text-color',
    'outline-color',
    'error-color',
    'information-color',
    'purchase-color',
    'placeholder-color',
    'listing-bg-alt',
    'active-color',
    'active-bg-color',
    'inactive-color',
    'inactive-bg-color',
    'disabled-text-color',
    'disabled-bg-color',
    'disabled-outline-color',
    'content-heading-color',
    'content-heading-bg-color',
    'primary-action-color',
    'primary-action-button-text-color',
    'primary-action-focus-color',
    'primary-action-focus-button-text-color',
    'secondary-action-color',
    'secondary-action-button-text-color',
    'secondary-action-focus-color',
    'secondary-action-focus-button-text-color',
    'purchase-action-color',
    'purchase-action-button-text-color',
    'purchase-action-focus-color',
    'purchase-action-focus-button-text-color',
    'wo-action-color',
    'wo-action-text-color',
    'wo-action-focus-color',
    'wo-action-focus-text-color',
    'listing-message-text-color',
    'listing-message-outline-color',
    'listing-message-bg-color',
  ];

  public emailModalVisible: boolean = false;

  public tabs: Array<ISelectableOption<any>> = [{ label: 'One', value: 0 }, { label: 'Two', value: 1 }, { label: 'Three', value: 2 }];

  public dropdownOptions: Array<ISelectableOption<any>> = [
    { label: 'An option', value: 0 },
    { label: 'Another option', value: 1 },
    { label: 'A really really long third option', value: 2 },
    { label: 'Last option', value: 3 },
  ];

  public countryList = [
    { label: 'Albania', value: 'AL' },
    { label: 'Algeria', value: 'DZ' },
    { label: 'Andorra', value: 'AD' },
    { label: 'Anguilla', value: 'AI' },
    { label: 'Antarctica', value: 'AQ' },
    { label: 'Argentina', value: 'AR' },
    { label: 'Armenia', value: 'AM' },
    { label: 'Aruba', value: 'AW' },
    { label: 'Australia', value: 'AU' },
    { label: 'Austria', value: 'AT' },
    { label: 'Azerbaijan', value: 'AZ' },
    { label: 'Bahamas', value: 'BS' },
    { label: 'Bahrain', value: 'BH' },
    { label: 'Bangladesh', value: 'BD' },
    { label: 'Barbados', value: 'BB' },
    { label: 'Belarus', value: 'BY' },
    { label: 'Belgium', value: 'BE' },
    { label: 'Belize', value: 'BZ' },
    { label: 'Benin', value: 'BJ' },
    { label: 'Bermuda', value: 'BM' },
    { label: 'Bhutan', value: 'BT' },
    { label: 'Bolivia', value: 'BO' },
    { label: 'Botswana', value: 'BW' },
    { label: 'Brazil', value: 'BR' },
    { label: 'Bulgaria', value: 'BG' },
    { label: 'Burundi', value: 'BI' },
    { label: 'Cambodia', value: 'KH' },
    { label: 'Cameroon', value: 'CM' },
    { label: 'Canada', value: 'CA' },
    { label: 'Cape Verde', value: 'CV' },
    { label: 'Chad', value: 'TD' },
    { label: 'Chile', value: 'CL' },
    { label: 'China', value: 'CN' },
    { label: 'Colombia', value: 'CO' },
    { label: 'Comoros', value: 'KM' },
    { label: 'Congo', value: 'CG' },
    { label: 'Costa Rica', value: 'CR' },
    { label: 'Croatia', value: 'HR' },
    { label: 'Cyprus', value: 'CY' },
    { label: 'Czech Republic', value: 'CZ' },
    { label: 'Denmark', value: 'DK' },
    { label: 'Djibouti', value: 'DJ' },
    { label: 'Dominica', value: 'DM' },
    { label: 'Dominican Republic', value: 'DO' },
    { label: 'Ecuador', value: 'EC' },
    { label: 'Egypt', value: 'EG' },
    { label: 'El Salvador', value: 'SV' },
    { label: 'Eritrea', value: 'ER' },
    { label: 'Estonia', value: 'EE' },
    { label: 'Ethiopia', value: 'ET' },
    { label: 'Fiji', value: 'FJ' },
    { label: 'Finland', value: 'FI' },
    { label: 'France', value: 'FR' },
    { label: 'Gabon', value: 'GA' },
    { label: 'Gambia', value: 'GM' },
    { label: 'Georgia', value: 'GE' },
    { label: 'Germany', value: 'DE' },
    { label: 'Ghana', value: 'GH' },
    { label: 'Great Britain', value: 'UK' },
    { label: 'Greece', value: 'GR' },
    { label: 'Greenland', value: 'GL' },
    { label: 'Grenada', value: 'GD' },
    { label: 'Guadeloupe', value: 'GP' },
    { label: 'Guam', value: 'GU' },
    { label: 'Guatemala', value: 'GT' },
    { label: 'Guinea', value: 'GN' },
    { label: 'Guyana', value: 'GY' },
    { label: 'Haiti', value: 'HT' },
    { label: 'Honduras', value: 'HN' },
    { label: 'Hong Kong', value: 'HK' },
    { label: 'Hungary', value: 'HU' },
    { label: 'Iceland', value: 'IS' },
    { label: 'India', value: 'IN' },
    { label: 'Indonesia', value: 'ID' },
    { label: 'Ireland', value: 'IE' },
    { label: 'Israel', value: 'IL' },
    { label: 'Italy', value: 'IT' },
    { label: 'Jamaica', value: 'JM' },
    { label: 'Japan', value: 'JP' },
    { label: 'Jordan', value: 'JO' },
    { label: 'Kazakhstan', value: 'KZ' },
    { label: 'Kenya', value: 'KE' },
    { label: 'Kiribati', value: 'KI' },
    { label: 'Kuwait', value: 'KW' },
    { label: 'Kyrgyzstan', value: 'KG' },
    { label: 'Laos', value: 'LA' },
    { label: 'Latvia', value: 'LV' },
    { label: 'Lebanon', value: 'LB' },
    { label: 'Lesotho', value: 'LS' },
    { label: 'Lithuania', value: 'LT' },
    { label: 'Luxembourg', value: 'LU' },
    { label: 'Macao', value: 'MO' },
    { label: 'Macedonia', value: 'MK' },
    { label: 'Madagascar', value: 'MG' },
    { label: 'Malawi', value: 'MW' },
    { label: 'Malaysia', value: 'MY' },
    { label: 'Maldives', value: 'MV' },
    { label: 'Mali', value: 'ML' },
    { label: 'Malta', value: 'MT' },
    { label: 'Martinique', value: 'MQ' },
    { label: 'Mauritania', value: 'MR' },
    { label: 'Mauritius', value: 'MU' },
    { label: 'Mayotte', value: 'YT' },
    { label: 'Mexico', value: 'MX' },
    { label: 'Micronesia', value: 'FM' },
    { label: 'Moldova', value: 'MD' },
    { label: 'Monaco', value: 'MC' },
    { label: 'Mongolia', value: 'MN' },
    { label: 'Montserrat', value: 'MS' },
    { label: 'Morocco', value: 'MA' },
    { label: 'Mozambique', value: 'MZ' },
    { label: 'Namibia', value: 'NA' },
    { label: 'Nauru', value: 'NR' },
    { label: 'Nepal', value: 'NP' },
    { label: 'Netherlands', value: 'NL' },
    { label: 'New Caledonia', value: 'NC' },
    { label: 'New Zealand', value: 'NZ' },
    { label: 'Nicaragua', value: 'NI' },
    { label: 'Niger', value: 'NE' },
    { label: 'Nigeria', value: 'NG' },
    { label: 'Niue', value: 'NU' },
    { label: 'Norway', value: 'NO' },
    { label: 'Oman', value: 'OM' },
    { label: 'Pakistan', value: 'PK' },
    { label: 'Palau', value: 'PW' },
    { label: 'Panama', value: 'PA' },
    { label: 'Paraguay', value: 'PY' },
    { label: 'Peru', value: 'PE' },
    { label: 'Philippines', value: 'PH' },
    { label: 'Pitcairn', value: 'PN' },
    { label: 'Poland', value: 'PL' },
    { label: 'Portugal', value: 'PT' },
    { label: 'Puerto Rico', value: 'PR' },
    { label: 'Qatar', value: 'QA' },
    { label: 'Reunion', value: 'RE' },
    { label: 'Romania', value: 'RO' },
    { label: 'Russia', value: 'RU' },
    { label: 'Saint Helena', value: 'SH' },
    { label: 'Saint Lucia', value: 'LC' },
    { label: 'Samoa', value: 'WS' },
    { label: 'San Marino', value: 'SM' },
    { label: 'Saudi Arabia', value: 'SA' },
    { label: 'Senegal', value: 'SN' },
    { label: 'Seychelles', value: 'SC' },
    { label: 'Sierra Leone', value: 'SL' },
    { label: 'Singapore', value: 'SG' },
    { label: 'Slovakia', value: 'SK' },
    { label: 'Slovenia', value: 'SI' },
    { label: 'South Africa', value: 'ZA' },
    { label: 'South Korea', value: 'KR' },
    { label: 'Spain', value: 'ES' },
    { label: 'Sri Lanka', value: 'LK' },
    { label: 'Suriname', value: 'SR' },
    { label: 'Swaziland', value: 'SZ' },
    { label: 'Sweden', value: 'SE' },
    { label: 'Switzerland', value: 'CH' },
    { label: 'Taiwan', value: 'TW' },
    { label: 'Tajikistan', value: 'TJ' },
    { label: 'Tanzania', value: 'TZ' },
    { label: 'Thailand', value: 'TH' },
    { label: 'The Democratic Republic Of Congo', value: 'CD' },
    { label: 'Timor-Leste', value: 'TL' },
    { label: 'Togo', value: 'TG' },
    { label: 'Tokelau', value: 'TK' },
    { label: 'Tonga', value: 'TO' },
    { label: 'Trinidad and Tobago', value: 'TT' },
    { label: 'Tunisia', value: 'TN' },
    { label: 'Turkey', value: 'TR' },
    { label: 'Turkmenistan', value: 'TM' },
    { label: 'Tuvalu', value: 'TV' },
    { label: 'Uganda', value: 'UG' },
    { label: 'Ukraine', value: 'UA' },
    { label: 'United Arab Emirates', value: 'AE' },
    { label: 'United Kingdom', value: 'GB' },
    { label: 'United States', value: 'US' },
    { label: 'Uruguay', value: 'UY' },
    { label: 'Uzbekistan', value: 'UZ' },
    { label: 'Vanuatu', value: 'VU' },
    { label: 'Venezuela', value: 'VE' },
    { label: 'Vietnam', value: 'VN' },
    { label: 'Yemen', value: 'YE' },
    { label: 'Zambia', value: 'ZM' },
    { label: 'Zimbabwe', value: 'ZW' },
  ];
  public dummyCountryListValue: string = '';

  public readonly icons: Array<string> = [
    'alert',
    'back',
    'bell',
    'calendar',
    'cart',
    'cross',
    'document-add',
    'document',
    'down-arrow',
    'down-sort',
    'download',
    'envelope',
    'eye',
    'flag',
    'forward',
    'gear',
    'i-bubble',
    'laptop',
    'leaf',
    'left-arrow',
    'line',
    'link',
    'list-sort',
    'list',
    'load',
    'lock',
    'menu',
    'pencil',
    'person',
    'phone',
    'play-bubble',
    'plus',
    'recycle',
    'refresh',
    'return',
    'right-arrow',
    'search',
    'share',
    'sort',
    'speech-bubble',
    'text',
    'tick-bubble',
    'tick',
    'trash',
    'up-arrow',
    'up-sort',
    'upload',
    'printer',
    'broken icon',
  ];

  public iconsColorGroup = 'black';

  public dummyPaginationFirstPageState: IPaginationState = {
    current: 1,
    total: 10,
    limit: 10,
  };

  public dummyPaginationLastPageState: IPaginationState = {
    current: 10,
    total: 10,
    limit: 10,
  };

  public dummyPaginationMidPageState: IPaginationState = {
    current: 5,
    total: 10,
    limit: 10,
  };

  public dummyInactiveDatePickerDates = [undefined, undefined];
  public dummyInactiveDatePickerDatesConfig = [
    {
      label: 'From',
      placeholder: 'Start date',
    },
    {
      label: 'To',
      placeholder: 'End date',
    },
  ];

  public dummyActiveDatePickerDates = [
    moment()
      .startOf('day')
      .format(),
  ];
  public dummyActiveDatePickerDatesConfig = [
    {
      label: 'From',
      placeholder: 'Select date',
    },
  ];

  public dummyBoundedDatePickerDates = [undefined];
  public dummyBoundedDatePickerDatesConfig = [
    {
      label: 'From',
      placeholder: 'Select date',
    },
  ];

  public dummyBoundedDatePickerMinDate = moment()
    .startOf('day')
    .add(-10, 'days')
    .format();

  public dummyBoundedDatePickerMaxDate = moment()
    .startOf('day')
    .add(10, 'days')
    .format();

  public dummyTableSort: Array<ISortCriteron> = [
    {
      key: 'dummySort',
      order: undefined,
    },
  ];

  public dummySortableList: Array<ISortableItem> = [
    {
      label: 'Item 1',
      id: 'item-1',
    },
    {
      label: 'Item 2',
      id: 'item-2',
    },
    {
      label: 'Item 3',
      id: 'item-3',
    },
    {
      label: 'Item 1',
      id: 'item-1',
    },
    {
      label: 'Item 2',
      id: 'item-2',
    },
    {
      label: 'Item 3',
      id: 'item-3',
    },
    {
      label: 'Item 1',
      id: 'item-1',
    },
    {
      label: 'Item 2',
      id: 'item-2',
    },
    {
      label: 'Item 3',
      id: 'item-3',
    },
    {
      label: 'Item 1',
      id: 'item-1',
    },
    {
      label: 'Item 2',
      id: 'item-2',
    },
    {
      label: 'Item 3',
      id: 'item-3',
    },
    {
      label: 'Item 1',
      id: 'item-1',
    },
    {
      label: 'Item 2',
      id: 'item-2',
    },
    {
      label: 'Item 3',
      id: 'item-3',
    },
    {
      label: 'Item 1',
      id: 'item-1',
    },
    {
      label: 'Item 2',
      id: 'item-2',
    },
    {
      label: 'Item 3',
      id: 'item-3',
    },
  ];

  public dummySortableListState = [];

  public dialogInput = '';
  public dialogOutput: string;

  public dummySubtotalItems1 = [];

  public dummySubtotalItems2 = [{ total: 100.76 }, { total: 24.22 }, { total: 91.49 }, { total: 314.01 }];

  public buttonLoading: boolean;
  public groupedCheckbox1Value = false;
  public groupedCheckbox2Value = false;
  public groupedCheckbox3Value = false;

  public dummyDefaultCheckboxValue = false;
  public dummyCheckedCheckboxValue = true;
  public dummyMultilineCheckboxValue = false;
  public dummyDisabledCheckboxValue = false;
  public dummyErrorCheckboxValue = false;

  public dummyRadioDefaultValue = -1;
  public dummyCheckedRadioValue = 0;
  public dummyRadioMultilineValue = -1;
  public dummyRadioDisabledValue = -1;
  public dummyRadioErrorValue = -1;
  public dummyGroupedRadiosValue = -1;

  public dummyDefaultDropdownValue;
  public dummyActiveDropdownValue = 1;
  public dummyDisabledDropdownValue;
  public dummyErrorDropdownValue;
  public dummyCustomDropdownValue;

  public dummyInlineFormDropdownValue;

  public openInactiveDatePicker($event: MouseEvent): void {
    // This is needed to ensure the popup doesn't auto-close because of the document:click!
    $event.stopPropagation();
    this.inactiveDatePicker.open(0);
  }

  public openDialog() {
    const dialog = this.dialogService.open(ExampleDialogComponent, {
      size: 'large',
      data: this.dialogInput,
    });
    dialog.afterClosed.subscribe(result => {
      this.dialogOutput = result;
    });
  }

  public openEmailModalDialog(): void {
    const filterOptions: Array<ISelectableOption<string>> = [
      {
        label: 'myarrowuserna@gmail.com',
        value: 'myarrowuserna@gmail.com',
      },
      {
        label: 'myarrowuserse@gmail.com',
        value: 'myarrowuserse@gmail.com',
      },
      {
        label: 'myarrowuserne@gmail.com',
        value: 'myarrowuserne@gmail.com',
      },
      {
        label: 'myarrowuserce@gmail.com',
        value: 'myarrowuserce@gmail.com',
      },
      {
        label: 'test@arrow.com',
        value: 'test@arrow.com',
      },
    ];

    const mockOrderLines: Array<IOrderLine> = [
      {
        id: 1234,
        purchaseOrderNumber: '1234',
        purchaseOrderStatus: <IPurchaseOrderStatus>'OPEN',
        lineItem: '1.2.1',
        customerPartNumber: 'BAV99',
        manufacturerPartNumber: 'SMC8903124',
        manufacturerName: 'Vishay',
        qtyOrdered: 3,
        qtyShipped: 3,
        qtyReadyToShip: 1,
        qtyRemaining: 1,
        requested: new Date('2018-07-30'),
        entered: new Date('2018-07-30'),
        committed: new Date('2018-07-30'),
        status: <IOrderLineStatus>'OPEN',
        buyerName: 'Tracie D.',
        salesOrderId: '5952303',
        salesHeaderId: 7438563,
        billToSiteUseId: 1234,
        unitResale: 0,
        extResale: 1745,
        currencyCode: 'USD',
        docId: '555_123456',
        itemId: 123456,
        warehouseId: 555,
        shipments: [
          {
            shipmentTrackingDate: new Date('2018-07-30'),
            shipmentTrackingReference: 'UPS190321-GB',
            carrier: 'FEDEX',
            shipmentTrackingUrl: 'http://www.fedex.com/123',
            qtyShipped: 100,
          },
        ],
        invoices: [],
      },
      {
        id: 1234,
        purchaseOrderNumber: '1234',
        purchaseOrderStatus: <IPurchaseOrderStatus>'OPEN',
        lineItem: '1.2.1',
        customerPartNumber: 'BAV99',
        manufacturerPartNumber: 'SMC8903124',
        manufacturerName: 'Vishay',
        qtyOrdered: 3,
        qtyShipped: 3,
        qtyReadyToShip: 1,
        qtyRemaining: 1,
        requested: new Date('2018-07-30'),
        entered: new Date('2018-07-30'),
        committed: new Date('2018-07-30'),
        status: <IOrderLineStatus>'OPEN',
        buyerName: 'Tracie D.',
        salesOrderId: '5952303',
        salesHeaderId: 7638563,
        billToSiteUseId: 1234,
        unitResale: 0,
        extResale: 1745,
        currencyCode: 'USD',
        docId: '0912309',
        itemId: 13,
        warehouseId: 555,
        shipments: [
          {
            shipmentTrackingDate: new Date('2018-07-30'),
            shipmentTrackingReference: 'UPS190321-GB',
            carrier: 'FEDEX',
            shipmentTrackingUrl: 'http://www.fedex.com/123',
            qtyShipped: 100,
          },
        ],
        invoices: [],
      },
      {
        id: 1234,
        purchaseOrderNumber: '1234',
        purchaseOrderStatus: <IPurchaseOrderStatus>'OPEN',
        lineItem: '1.2.1',
        customerPartNumber: 'BAV99',
        manufacturerPartNumber: 'SMC8903124',
        manufacturerName: 'Vishay',
        qtyOrdered: 3,
        qtyShipped: 3,
        qtyReadyToShip: 1,
        qtyRemaining: 1,
        requested: new Date('2018-07-30'),
        entered: new Date('2018-07-30'),
        committed: new Date('2018-07-30'),
        status: <IOrderLineStatus>'OPEN',
        buyerName: 'Tracie D.',
        salesOrderId: '5952303',
        salesHeaderId: 7458563,
        billToSiteUseId: 1234,
        unitResale: 0,
        extResale: 1745,
        currencyCode: 'USD',
        docId: '0912309',
        itemId: 42,
        warehouseId: 555,
        shipments: [
          {
            shipmentTrackingDate: new Date('2018-07-30'),
            shipmentTrackingReference: 'UPS190321-GB',
            carrier: 'FEDEX',
            shipmentTrackingUrl: 'http://www.fedex.com/123',
            qtyShipped: 100,
          },
        ],
        invoices: [],
      },
    ];

    this.dialogService.open(EmailModalComponent, {
      size: 'large',
      data: {
        header: DEFAULT_MODAL_TITLE,
        headerDescription: DEFAULT_MODAL_INTRO,
        fromEmail: 'david.lane@oncorems.com',
        recipients: filterOptions,
        subject: 'test email',
        payload: {
          orderLines: mockOrderLines,
        },
      },
    });
  }

  public loadSomething() {
    this.buttonLoading = true;
    of(false)
      .pipe(delay(1500))
      .subscribe(val => {
        this.buttonLoading = val;
      });
  }
  /*
   * Necessary because: https://github.com/angular/angular/issues/6595
   */
  public goToFragment(fragment: string) {
    const element = document.querySelector('#' + fragment);
    if (element) {
      setTimeout(() => {
        element.scrollIntoView({ behavior: 'instant', block: 'start', inline: 'nearest' });
      }, 0);
    }
  }
}
