import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StyleGuideComponent } from './pages/style-guide/style-guide.component';
import { StyleGuideGuard } from './style-guide.guard';

const routes: Routes = [
  {
    path: '',
    component: StyleGuideComponent,
    canActivate: [StyleGuideGuard],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class StyleGuideRoutingModule {}
