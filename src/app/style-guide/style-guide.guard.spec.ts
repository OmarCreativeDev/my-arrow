import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { StyleGuideGuard } from './style-guide.guard';
import { environment } from '@env/environment';
import { CoreModule } from '@app/core/core.module';
import { Router } from '@angular/router';

class DummyComponent {}

describe('StyleGuideGuard', () => {
  const routes = [{ path: 'dashboard', component: DummyComponent }, { path: 'login', component: DummyComponent }];
  let router: Router;
  let styleGuideGuard: StyleGuideGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CoreModule, RouterTestingModule.withRoutes(routes)],
      providers: [StyleGuideGuard],
    });
    styleGuideGuard = TestBed.get(StyleGuideGuard);
    router = TestBed.get(Router);
  });

  it('should be created', () => {
    expect(styleGuideGuard).toBeTruthy();
  });

  it('should guard Style Guide if environment is production', () => {
    environment.production = true;
    spyOn(styleGuideGuard, 'canActivate').and.callThrough();
    spyOn(router, 'navigateByUrl');
    styleGuideGuard.canActivate().subscribe(canActivate => {
      expect(canActivate).toBeFalsy();
      expect(router.navigateByUrl).toHaveBeenCalledWith('/login');
    });
  });

  it('should let pass Style Guide if environment is NOT production', () => {
    environment.production = false;
    spyOn(styleGuideGuard, 'canActivate').and.callThrough();
    spyOn(router, 'navigateByUrl');
    styleGuideGuard.canActivate().subscribe(canActivate => {
      expect(canActivate).toBeTruthy();
      expect(router.navigateByUrl).not.toHaveBeenCalled();
    });
  });
});
