export interface ILiveChatSettings {
  chatButtonId: string;
  deploymentId: string;
  deploymentUrl: string;
  initUrl: string;
  orgId: string;
  offlinePostUrl: string;
  sfdcUrl: string;
  origin: string;
  subject: string;
  webSite__c: string;
  communicationType__c: string;
  elqFormName: string;
  elqSiteId: string;
  leadSource: string;
  leadSourceDetail__c: string;
  leadSourceReference__c: string;
  uploadType: string;
  campaign_ID: string;
  category__c: string;
}

export interface IPermissions {
  public: any;
  private: any;
}

export interface Environment {
  production: boolean;
  baseUrls: {
    serviceBoms: string;
    serviceCheckout: string;
    serviceEmail: string;
    serviceEventNotifier?: string;
    serviceForecasts?: string;
    serviceOrderHistory: string;
    serviceOrders: string;
    servicePOUpload?: string;
    servicePayment?: string;
    serviceProductDetails: string;
    serviceProductNotification: string;
    serviceProductSearch: string;
    serviceProductsCatalog: string;
    serviceProperties: string;
    serviceQuoteCart: string;
    serviceQuotes: string;
    serviceSecurity: string;
    serviceShoppingCart: string;
    serviceTradeCompliance: string;
    serviceUser: string;
    pushPullOrder: string;
    serviceCustomerBroadcast: string;
  };
  liveChatSettings: ILiveChatSettings;
  bomConfig: any;
}
