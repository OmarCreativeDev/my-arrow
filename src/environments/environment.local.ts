import { Environment } from './environment.interface';
import { ClientId } from '@app/core/user/user.interface';

export const CLIENT_ID = ClientId.MYARROWUI;
export const SSO_CLIENT_ID = ClientId.MYADMINUI;
export const SESSION_TIMEOUT_MILLISECONDS = 60 * 60 * 1000; // 1 hour
export const WS_DEBUG = false;
export const WS_RECONNECT_DELAY = 30000; // milliseconds
export const WS_RECONNECT_TRIES = 3;
export const WS_HEARTBEAT_INCOMING = 4000;
export const WS_HEARTBEAT_OUTGOING = 4000;

const API_GATEWAY_HTTP = 'http://localhost';
const API_GATEWAY_WS = 'ws://localhost';

export const environment: Environment = {
  production: false,
  baseUrls: {
    serviceBoms: 'https://dev-design.arrow.com/bom/v2',
    serviceCheckout: API_GATEWAY_HTTP + ':9086/checkout',
    serviceEmail: API_GATEWAY_HTTP + ':9098/emails',
    serviceEventNotifier: API_GATEWAY_WS + ':9082/eventnotifier',
    serviceOrderHistory: API_GATEWAY_HTTP + ':9091/orderhistory',
    serviceOrders: API_GATEWAY_HTTP + ':9081/orders',
    servicePOUpload: API_GATEWAY_HTTP + ':9085/poupload',
    serviceProductDetails: API_GATEWAY_HTTP + ':9099/productdetails',
    serviceProductNotification: API_GATEWAY_HTTP + ':9083/notifications',
    serviceProductSearch: API_GATEWAY_HTTP + ':9095/products',
    serviceProductsCatalog: API_GATEWAY_HTTP + ':9088/productcatalog',
    serviceProperties: API_GATEWAY_HTTP + ':9081/properties',
    serviceQuoteCart: API_GATEWAY_HTTP + `:9087/quotecarts`,
    serviceQuotes: API_GATEWAY_HTTP + '/quotes',
    serviceSecurity: API_GATEWAY_HTTP + ':9090/security',
    serviceShoppingCart: API_GATEWAY_HTTP + ':9093/shoppingcarts',
    serviceTradeCompliance: API_GATEWAY_HTTP + ':9096/tradecompliance',
    serviceUser: API_GATEWAY_HTTP + ':9094/users',
    pushPullOrder: API_GATEWAY_HTTP + ':9094/pushpull',
    serviceCustomerBroadcast: API_GATEWAY_HTTP + ':9191/customerbroadcast',
  },
  liveChatSettings: {
    chatButtonId: '573230000000080',
    deploymentId: '572230000008ONy',
    deploymentUrl: 'https://c.la3-c2cs-chi.salesforceliveagent.com/content/g/js/43.0/deployment.js',
    initUrl: 'https://d.la3-c2cs-chi.salesforceliveagent.com/chat',
    orgId: '00D23000000CpWs',
    offlinePostUrl: '/proxy/eloqua/?params=/e/f2.aspx?elqFormName=MyarrowNaEnDesignCenterEngineerProfile',
    sfdcUrl: 'https://cs28.salesforce.com/servlet/servlet.WebToCase',
    origin: 'MyArrow - Offline Chat',
    subject: 'MyArrow 2.0 Offline Chat Request - ',
    webSite__c: 'MyArrow.com',
    communicationType__c: 'Web',
    elqFormName: 'MyarrowNaEnDesignCenterEngineerProfile',
    elqSiteId: '600830862',
    leadSource: 'ecommerce',
    leadSourceDetail__c: 'MyArrow',
    leadSourceReference__c: 'Design Center',
    uploadType: 'Form Submission',
    campaign_ID: '70170000001645b',
    category__c: 'Engineering',
  },
  bomConfig: {
    target: 'myarrow',
    clientId: CLIENT_ID,
    apis: {
      security: { protocol: 'https', domain: 'arrow.com', subdomain: 'dev-myarrow-api', path: 'security' },
      cart: { protocol: 'https', domain: 'arrow.com', subdomain: 'dev-myarrow-api', path: 'shoppingcarts' },
      user: { protocol: 'https', domain: 'arrow.com', subdomain: 'dev-myarrow-api', path: 'users' },
      bom: { domain: 'arrow.{topDomain}', subdomain: 'dev-design', path: 'bom/v2' },
    },
  },
};
