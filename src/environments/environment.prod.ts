import { Environment } from './environment.interface';
import { ClientId } from '@app/core/user/user.interface';

export const CLIENT_ID = ClientId.MYARROWUI;
export const SSO_CLIENT_ID = ClientId.MYADMINUI;
export const SESSION_TIMEOUT_MILLISECONDS = 60 * 60 * 1000; // 1 hour
export const WS_DEBUG = false;
export const WS_RECONNECT_DELAY = 30000; // milliseconds
export const WS_RECONNECT_TRIES = 3;
export const WS_HEARTBEAT_INCOMING = 4000;
export const WS_HEARTBEAT_OUTGOING = 4000;

const API_GATEWAY_HTTP = 'https://my.arrow.com/api';
const API_GATEWAY_WS = 'wss://my.arrow.com/api';

export const environment: Environment = {
  production: true,
  baseUrls: {
    serviceBoms: 'https://design.arrow.com/bom/v2',
    serviceCheckout: API_GATEWAY_HTTP + '/checkout',
    serviceEmail: API_GATEWAY_HTTP + '/emails',
    serviceEventNotifier: API_GATEWAY_WS + '/eventnotifier',
    serviceForecasts: API_GATEWAY_HTTP + '/forecasts',
    serviceOrderHistory: API_GATEWAY_HTTP + '/orderhistory',
    serviceOrders: API_GATEWAY_HTTP + '/orders',
    servicePOUpload: API_GATEWAY_HTTP + '/poupload',
    servicePayment: API_GATEWAY_HTTP + '/payment',
    serviceProductDetails: API_GATEWAY_HTTP + '/productdetails',
    serviceProductNotification: API_GATEWAY_HTTP + '/notifications',
    serviceProductSearch: API_GATEWAY_HTTP + '/products',
    serviceProductsCatalog: API_GATEWAY_HTTP + '/productcatalog',
    serviceProperties: API_GATEWAY_HTTP + '/properties',
    serviceQuoteCart: API_GATEWAY_HTTP + '/quotecarts',
    serviceQuotes: API_GATEWAY_HTTP + '/quotes',
    serviceSecurity: API_GATEWAY_HTTP + '/security',
    serviceShoppingCart: API_GATEWAY_HTTP + '/shoppingcarts',
    serviceTradeCompliance: API_GATEWAY_HTTP + '/tradecompliance',
    serviceUser: API_GATEWAY_HTTP + '/users',
    pushPullOrder: API_GATEWAY_HTTP + '/pushpull',
    serviceCustomerBroadcast: API_GATEWAY_HTTP + '/customerbroadcast',
  },
  liveChatSettings: {
    chatButtonId: '5730g000000TNNN',
    deploymentId: '57270000000XZBo',
    deploymentUrl: 'https://c.la4-c2-dfw.salesforceliveagent.com/content/g/js/43.0/deployment.js',
    initUrl: 'https://d.la4-c2-dfw.salesforceliveagent.com/chat',
    orgId: '00D70000000JhFy',
    offlinePostUrl: '/proxy/eloqua/?params=/e/f2.aspx?elqFormName=MyarrowNaEnDesignCenterEngineerProfile',
    sfdcUrl: 'https://www.salesforce.com/servlet/servlet.WebToCase',
    origin: 'MyArrow - Offline Chat',
    subject: 'MyArrow 2.0 Offline Chat Request - ',
    webSite__c: 'MyArrow.com',
    communicationType__c: 'Web',
    elqFormName: 'MyarrowNaEnDesignCenterEngineerProfile',
    elqSiteId: '600830862',
    leadSource: 'ecommerce',
    leadSourceDetail__c: 'MyArrow',
    leadSourceReference__c: 'Design Center',
    uploadType: 'Form Submission',
    campaign_ID: '70170000001645b',
    category__c: 'Engineering',
  },
  bomConfig: {
    target: 'myarrow',
    clientId: CLIENT_ID,
    apis: {
      security: { protocol: 'https', domain: 'arrow.com', subdomain: 'my', path: 'api/security' },
      cart: { protocol: 'https', domain: 'arrow.com', subdomain: 'my', path: 'api/shoppingcarts' },
      user: { protocol: 'https', domain: 'arrow.com', subdomain: 'my', path: 'api/users' },
      bom: { domain: 'arrow.{topDomain}', subdomain: 'design', path: 'bom/v2' },
    },
  },
};
