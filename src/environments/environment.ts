import { Environment } from './environment.interface';
import { ClientId } from '@app/core/user/user.interface';

export const CLIENT_ID = ClientId.MYARROWUI;
export const SSO_CLIENT_ID = ClientId.MYADMINUI;
export const SESSION_TIMEOUT_MILLISECONDS = 60 * 60 * 1000; // 1 hour
export const WS_DEBUG = false;
export const WS_RECONNECT_DELAY = 30000; // milliseconds
export const WS_RECONNECT_TRIES = 3;
export const WS_HEARTBEAT_INCOMING = 4000;
export const WS_HEARTBEAT_OUTGOING = 4000;

export const environment: Environment = {
  production: false,
  baseUrls: {
    serviceBoms: 'https://llk8ckrlp5dgxsj4b-mock.stoplight-proxy.io/myarrow',
    serviceCheckout: 'http://ndhkwct4nabzd2ncj-mock.stoplight-proxy.io/checkout',
    serviceEmail: 'http://mw7cztnzqzpjakchx-mock.stoplight-proxy.io/myarrow',
    serviceEventNotifier: 'ws://localhost:9082/eventnotifier',
    serviceOrderHistory: 'https://a7ofedmhud9qrtdgp-mock.stoplight-proxy.io/myarrow',
    serviceOrders: 'https://a7ofedmhud9qrtdgp-mock.stoplight-proxy.io/myarrow',
    servicePOUpload: 'http://localhost:9085/poupload',
    serviceProductDetails: 'https://ohgpbyrw2fxvjrucj-mock.stoplight-proxy.io/myarrow',
    serviceProductNotification: 'http://localhost:9083/notifications',
    serviceProductSearch: 'https://mqzlbfpq6mlcedycb-mock.stoplight-proxy.io/myarrow',
    serviceProductsCatalog: 'https://wsprh46jnrvtthnbz-mock.stoplight-proxy.io/myarrow',
    serviceProperties: 'https://baatyrsxzqbzaurrd-mock.stoplight-proxy.io/myarrow',
    serviceQuoteCart: 'https://9zjyumoqrzi3ywdsz-mock.stoplight-proxy.io',
    serviceQuotes: 'http://gfq8ytn8tstg7ktkb-mock.stoplight-proxy.io/myarrow',
    serviceSecurity: 'https://tmdfrmgbbngxvd6s6-mock.stoplight-proxy.io/myarrow',
    serviceShoppingCart: 'https://f28nea6xrgwmojwrm-mock.stoplight-proxy.io/myarrow',
    serviceTradeCompliance: 'http://4vwq99hghsl4aeetb-mock.stoplight-proxy.io/myarrow',
    serviceUser: 'https://vybbzkee7ehykkyxe-mock.stoplight-proxy.io/myarrow',
    pushPullOrder: 'https://jvMbE6oWW7SYfrwG4-mock.stoplight-proxy.io/myarrow/pushpull',
    serviceCustomerBroadcast: 'https://dev-myarrow-api.arrow.com/customerbroadcast',
  },
  liveChatSettings: {
    chatButtonId: '573230000000080',
    deploymentId: '572230000008ONy',
    deploymentUrl: 'https://c.la3-c2cs-chi.salesforceliveagent.com/content/g/js/43.0/deployment.js',
    initUrl: 'https://d.la3-c2cs-chi.salesforceliveagent.com/chat',
    orgId: '00D23000000CpWs',
    offlinePostUrl: '/proxy/eloqua/?params=/e/f2.aspx?elqFormName=MyarrowNaEnDesignCenterEngineerProfile',
    sfdcUrl: 'https://cs28.salesforce.com/servlet/servlet.WebToCase',
    origin: 'MyArrow - Offline Chat',
    subject: 'MyArrow 2.0 Offline Chat Request - ',
    webSite__c: 'MyArrow.com',
    communicationType__c: 'Web',
    elqFormName: 'MyarrowNaEnDesignCenterEngineerProfile',
    elqSiteId: '600830862',
    leadSource: 'ecommerce',
    leadSourceDetail__c: 'MyArrow',
    leadSourceReference__c: 'Design Center',
    uploadType: 'Form Submission',
    campaign_ID: '70170000001645b',
    category__c: 'Engineering',
  },
  bomConfig: {
    target: 'myarrow',
    clientId: CLIENT_ID,
    apis: {
      security: { protocol: 'https', domain: 'arrow.com', subdomain: 'dev-myarrow-api', path: 'security' },
      cart: { protocol: 'https', domain: 'arrow.com', subdomain: 'dev-myarrow-api', path: 'shoppingcarts' },
      user: { protocol: 'https', domain: 'arrow.com', subdomain: 'dev-myarrow-api', path: 'users' },
      bom: { domain: 'arrow.{topDomain}', subdomain: 'dev-design', path: 'bom/v2' },
    },
  },
};
